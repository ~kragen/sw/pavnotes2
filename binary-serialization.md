I wrote a [simple VNC server][0] in Golang, which worked out to about
300 lines of code.  The VNC protocol has a lot of binary message
formats, and one of the nice things about Golang for this was that I
could define a message format like this:

    type PIXEL_FORMAT struct {
            Bits_per_pixel, Depth, Big_endian_flag, True_color_flag byte
            Red_max, Green_max, Blue_max                            uint16
            Red_shift, Green_shift, Blue_shift                      uint8
            _, _, _ byte
    }

Then I could instantiate it like this:

    var pixelFormat24Bit = PIXEL_FORMAT{
            Bits_per_pixel: 32, Depth: 24,
            Big_endian_flag: 1,
            True_color_flag: 1,

            Red_max: 255, Green_max: 255, Blue_max: 255,
            Red_shift: 16, Green_shift: 8, Blue_shift: 0,
    }

Write it onto the wire like this:

    w.WriteBigEndian(ServerInit{
            width: 640, height: 480,
            pixelFormat: pixelFormat24Bit,
            nameLength:  uint32(len(name)),
    }, "ServerInit")

Read it off the wire like this:

    w.ReadBigEndian(&pixel_format, "SetPixelFormat decoding")

And access its fields like this:

    if format.Big_endian_flag != 0 { ... }

[0]: http://canonical.org/~kragen/sw/dev3/vncs.go

Here ReadBigEndian and WriteBigEndian are methods in my code that call
the Write and Read functions from the Golang “encoding/binary”
standard library module.  So you define the message format once, the
compiler calculates the field offsets for you; you can initialize,
read, and write its fields by name; and you can serialize or
deserialize it (with user-chosen endianness!) with a simple function
call.

In Golang this depends on runtime reflection for encoding/binary to
find out what fields exist in the struct, what order they’re in, and I
think some optional annotation data I’m not using.  But how could I
get such a facility in, for example, C?  What I want to avoid here is
the potential for desynchronization bugs, where I add a field to a
message type, or change its definition, but have a bug because I
didn’t make the corresponding change to some other piece of code.

C structs don’t carry enough information for reflection
-------------------------------------------------------

We could try defining these record formats as C structs, as we did in
Golang.  But to serialize one of these messages as anything other than
its ABI-dependent form (possibly including uninitialized data in
padding), we have to iterate over its fields.  And there’s no way to
iterate over the fields of a struct in C.

It’s not type-safe to do it at runtime in C
-------------------------------------------

Instead of defining a struct, you could imagine building up some kind
of data structure that the serialization or deserialization code
traverses at runtime to figure out the sizes, offsets, and types of
the fields.  Unfortunately, there isn’t really any way to gateway the
runtime-determined types back into C’s static type system; you’d end
up having to do something like this:

    if (get_uint32(format, "big_endian_flag")) { ... }

and only detecting any type mismatches at runtime.  So you’re back to
having desynchronization bugs.

A solution using an extra code generation build step
----------------------------------------------------

Whatever language you want to use, it’s fairly straightforward to do
this by writing a code generator; here’s [a simple example in
Python][1], which got a lot better for this kind of thing when it
recently got quasiquoted f-strings:

    def generate_c(formats):
        yield '#include <stdbool.h>\n'
        yield '#include <stdint.h>\n'
        yield '#include "binmsg.h"\n\n'

        for format in formats:
            yield f'typedef struct {format["name"]} {{\n'
            for field_name, field_type in format['fields']:
                if field_name != '_':
                    yield f'    {field_type} {field_name};\n'
            yield f'}} {format["name"]};\n\n'

        for how in ['big_endian', 'little_endian']:
            for op in ['read', 'write']:
                for format in formats:
                    yield from generate_c_function(how, op, format)

    def generate_c_function(how, op, format):
        yield f'bool {op}_{format["name"]}_{how}(stream out, '
        yield f'struct {format["name"]} *obj) {{\n'
        for field_name, field_type in format['fields']:
            if field_name == '_':
                yield (f'    if (!{op}_dummy_{field_type}_{how}(out)) ' +
                       'return false;\n')
            else:
                yield (f'    if (!{op}_{field_type}_{how}(out, ' +
                       f'&obj->{field_name})) return false;\n')

        yield '    return true;\n}\n\n'

[1]: http://canonical.org/~kragen/sw/dev3/binmsg.py

This generates serialization functions that look like this:

    bool write_ServerInit_big_endian(stream out, struct ServerInit *obj) {
        if (!write_u16_big_endian(out, &obj->width)) return false;
        if (!write_u16_big_endian(out, &obj->height)) return false;
        if (!write_PIXEL_FORMAT_big_endian(out, &obj->pixelFormat)) return false;
        if (!write_u32_big_endian(out, &obj->nameLength)) return false;
        return true;
    }

This permits the field types to be previously defined message types,
thus permitting nested fields.  It assumes that functions like
`write_u32_big_endian` are defined elsewhere and return a boolean
indicating success or failure (perhaps storing a status on the stream
to permit better error reporting).

This is not super efficient, involving a subroutine call and some
conditionals for every field, but perfectly acceptable.  And if the
primitive subroutines are properly declared for compilation inline, we
can probably get good code out of it.  But can we do something like
this without the extra build step and the dependencies on unstable,
resource-intensive things like Python?

A solution using X-macros in the C preprocessor
-----------------------------------------------

It turns out that the C preprocessor is just barely powerful enough to
do this.  Given [this source file][2]:

    #include <stdint.h>
    #include <stdbool.h>
    #include "binmsg_cpp.h"

    #define KeyEvent_fields(field, padding)               \
      field(u8, down_flag)                                \
      padding(u8)                                         \
      padding(u8)                                         \
      field(u32, keysym)

    MESSAGE_TYPE(KeyEvent)

[2]: http://canonical.org/~kragen/sw/dev3/binmsg_cpp.c

The C preprocessor can turn it into this (reformatted and leading
declarations removed):

    typedef struct KeyEvent {
        u8 down_flag;
        u32 keysym;
    } KeyEvent;

    _Bool read_KeyEvent_big_endian(stream out, KeyEvent * obj)
    {
        if (!read_u8_big_endian(out, &obj->down_flag))
            return 0;
        if (!read_dummy_u8(out))
            return 0;
        if (!read_dummy_u8(out))
            return 0;
        if (!read_u32_big_endian(out, &obj->keysym))
            return 0;
        return 1;
    }

    _Bool write_KeyEvent_big_endian(stream out, KeyEvent * obj)
    {
        if (!write_u8_big_endian(out, &obj->down_flag))
            return 0;
        if (!write_dummy_u8(out))
            return 0;
        if (!write_dummy_u8(out))
            return 0;
        if (!write_u32_big_endian(out, &obj->keysym))
            return 0;
        return 1;
    }

To make this work, we write [a header file][3] containing these macro
definitions:

    #define MESSAGE_TYPE(type)                                       \
      DECLARE_STRUCT(type)                                           \
      STRUCT_READER(type)                                            \
      STRUCT_WRITER(type)

    #define DECLARE_STRUCT(type)                                     \
      typedef struct type {                                          \
        type##_fields(STRUCT_FIELD, IGNORE)                          \
      } type;
    #define IGNORE(type)
    #define STRUCT_FIELD(type, name) type name;

    #define STRUCT_READER(type)                                      \
      bool read_##type##_big_endian(stream out, type *obj) {         \
        type##_fields(READ_FIELD_BIG_ENDIAN, READ_PADDING)           \
        return true;                                                 \
      }
    #define READ_FIELD_BIG_ENDIAN(type, name)                        \
      if (!read_##type##_big_endian(out, &obj->name)) return false;
    #define READ_PADDING(type)                                       \
      if (!read_dummy_##type(out)) return false;

    #define STRUCT_WRITER(type)                                      \
      bool write_##type##_big_endian(stream out, type *obj) {        \
        type##_fields(WRITE_FIELD_BIG_ENDIAN, WRITE_PADDING)         \
        return true;                                                 \
      }
    #define WRITE_FIELD_BIG_ENDIAN(type, name)                       \
      if (!write_##type##_big_endian(out, &obj->name)) return false;
    #define WRITE_PADDING(type)                                      \
      if (!write_dummy_##type(out)) return false;

[3]: http://canonical.org/~kragen/sw/dev3/binmsg_cpp.h

Essentially the `KeyEvent_fields` macro is used to store a sequence of
fields which can then be traversed repeatedly by the `MESSAGE_TYPE`
macro to perform different operations on those fields, such as
declaring them, emitting code to read them, emitting code to write
them, etc.  You could reasonably include a pretty-printer for human
readability, and this can be straightforwardly extended to do
little-endian like the Python, compute the serialized size of the
structures, and so on.

This is an application of a macro inversion-of-control technique known
as “X macros”, because canonically the “field” or “padding” parameter
is called “X”; it’s documented [in Wikipedia][4], in [the C
Programming WikiBook][5] (and in particular [as being useful for
serialization][6]), in [a Dr. Dobb’s article by Randy Meyers][7], in
[an article by Walter Bright][8], [in a Stack Overflow answer][9], and
in [uzimonkey explaining why they stopped using them][10].

[4]: https://en.wikipedia.org/wiki/X_macro
[5]: https://en.wikibooks.org/wiki/C_Programming/Preprocessor_directives_and_macros#X-Macros
[6]: https://en.wikibooks.org/wiki/C_Programming/Serialization
[7]: https://www.drdobbs.com/the-new-c-x-macros/184401387
[8]: https://digitalmars.com/articles/b51.html
[9]: https://stackoverflow.com/a/650729
[10]: https://old.reddit.com/r/C_Programming/comments/8jzo5w/favourite_preprocessor_tricks/dz4htps/

This may be harder to explain than the Python solution, and also
harder to debug if it goes wrong, but it’s only about 25 lines of code
(the full Python code including the parser is 46 lines and is arguably
not that straightforward either) and doesn’t require any extra build
steps.

A messier approach
------------------

You of course don’t have to define the structs at all.  If you’re
happy to just parse the protocol stream and act on it, you can do
something like this:

    if (expect_byte(conn, '\4')) {  // KeyEvent
        int down_flag;
        if (!read_uint8(conn, &down_flag)) return 0;
        if (!read_padding(conn, 2)) return 0;
        unsigned keysym;
        if (!read_uint32_big_endian(conn, &keysym)) return 0;
        commit_reads(conn);         // We’ve read the whole event
        handle_key(down_flag, keysym);
        return 1;
    } else if (expect_byte(conn, '\5')) {  // PointerEvent
        ...

The idea is that `expect_byte` only consumes a byte if it sees the one
it expects, and things like `read_uint8` return false if they reach
the end of the buffer, so we can come back and restart the read later.
`commit_reads` marks the point from which restarting will happen.
This approach can definitely be made to work reliably, but I think
it’s worse for three reasons:

1. It’s much more difficult to see if the message format definition in
   that code is correct, and whether we’ve maybe slipped in some
   irreversible side effects in the middle of some parsing that may
   have to be backtracked in the rare case of a short packet.
   
2. In that form it only handles reads, not writes.  You’d have to
   write a similar piece of parallel code for the client.  Since it
   uses pointers, you can genericize it so that it reads or writes
   depending on the nature of the stream it’s invoked on, but that
   involves putting the down_`flag` and `keysym` into a struct.
   
3. You get away without defining the struct type, but only by virtue
   of defining a function whose arguments are the contents of the
   struct.  And then you still can’t do things like store the structs.

For more complicated data languages, this kind of control structure
may actually be better, but you’re still better off using a parser
generator to generate it.
