I've written previously about powder-bed flux-deposition processes and
their inverse.  In a flux-deposition process, a powder bed is spread
layer by layer, and a flux is deposited selectively on each layer
which lowers the powder's melting point.  The flux on each layer forms
a three-dimensional shape embedded inside the powder bed.  After the
last layer is deposited, the powder bed is baked in a kiln; the
unfluxed powder is unaffected, but the fluxed powder sinters or even
melts, supported by the solid powder around it.  After cooling, the
now-solid fluxed form is depowdered.

In the inverse process, the selectively deposited additive reacts with
the powder to *raise* the powder's melting point instead of lowering
it, so that in the kiln all the untouched powder melts away.

As far as I know nobody is using either of these processes yet.

A particularly interesting form of the second just occurred to me.
Suppose the powder is aluminum or titanium and the additive is an
oxidizer such as a nitrate, chlorate, perchlorate, perborate, or
percarbonate of sodium or potassium.  Could you thus 3-D print
sapphire or rutile?

Titanium carbide is another interesting target due to its abundant
ingredients, its extreme hardness, and its high melting point.  You
could selectively deposit carbon sources such as proteins or other
thermoset polymers into a titanium powder bed, or even a rutile powder
bed, carbothermally "reducing" the rutile to titanium carbide.

Silicon carbide, similarly, could be printed in a bed of silicon
powder.

Of course, all these metal powders are a pretty serious fire risk.

I'm not sure what ingredients could plausibly combine to produce boron
nitride or boron carbide in this way.
