[The 169.4–169.8125 MHz band][0], about 1.8 meters, was formerly used
in the EU by the ERMES paging system at 6250 bits per second and has
now been reallocated for ISM service at up to 500 milliwatts.
[Radiocrafts is promoting it for LPWAN use][3], claiming 3–4km range
in urban environments.  [You probably want a half-meter-long antenna
and a reflecting ground plane][2] for efficient coupling.  [In the US,
it is not an ISM band,][1] at least as of 02022.  I don’t know what
it’s allocated to in Argentina.

[0]: https://en.wikipedia.org/wiki/Wize_technology
[1]: https://e2e.ti.com/support/wireless-connectivity/sub-1-ghz-group/sub-1-ghz/f/sub-1-ghz-forum/1162892/cc1120em-169-rd-is-169mhz-permitted-by-the-fcc-in-the-usa
[2]: https://www.ead-ltd.com/news/169-mhz-ism-band-antennas-reality-check
[3]: https://radiocrafts.com/uploads/WP011_LPWAN_At_169MHz.pdf

169MHz should go through buildings without much difficulty, but not
through the ground, and you won’t get groundwave propagation.  But
3–4km line of sight [only requires the antenna to be 1 meter off the
ground][4], assuming a spherical earth.  [433MHz is commonly used in
the 1–2km range][5].

[4]: https://www.ti.com/lit/an/swra479a/swra479a.pdf
[5]: https://dl.ifip.org/db/conf/ifip6-6/eunice2012/MilankovichLIS12.pdf
