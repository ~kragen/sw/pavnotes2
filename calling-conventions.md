I feel like I finally have a good understanding of the design of
calling conventions.

Call-preserved (“caller-saved”) registers are necessary for subroutine
calls to work at all.  Minimally the subroutine needs to restore the
PC to point into the caller; that’s what returning is.  If the caller
is to be reentrant, some kind of context pointer such as the stack
pointer is also required, so the caller can store its return address.
But if those are the only preserved registers, all your local
variables have to live on the stack in non-leaf subroutines, which is
expensive.  Declaring some general-purpose registers call-preserved
allows you to store local variables in them across subroutine calls,
at the cost of having to save them on entry.  If you want to support
thread-local data and relocatable globals, you need two
super-call-preserved registers for those, a thread pointer TP and a
globals pointer GP: not just restored on return but also passed
unchanged to all callees.  If they can be used from interrupts, you
can never use them for something else even temporarily, unless you
have interrupts disabled.  You can legitimately declare all registers
call-preserved, except for any unavoidably lost by the calling
mechanism, such as LR and IP on ARM.

On the other hand, making all registers call-preserved means
subroutines cannot return a value in registers, and you have to pay
the cost of saving some of them on entry to any subroutine that needs
to compute a temporary result whose lifetime includes no further
subroutine calls, and saving all of them on a cooperative context
switch.  And it seems perverse (usually) to require subroutines to
preserve their own parameters.  By declaring some registers (and all
the argument registers) call-clobbered, you can use them freely for
expression evaluation.  Experience with the MuP21/F21/F18A Forth chips
suggests that you may need as many as 10 registers for this across all
subroutines, but usually 3-4 per subroutine is plenty.

So, to avoid great sacrifices, you need at least four call-preserved
registers (PC, SP, TP, GP) and at least two call-clobbered registers.
From there you can get more efficiency by adding more of each, but in
each case there’s a point of diminishing returns, maybe after 4-6
call-clobbered registers and 10 call-preserved registers (6 for local
variables).  If you have more than 16 registers, then, the question is
whether allocating them as call-preserved or call-clobbered does less
harm to the abundant code that doesn’t use them.

Interrupt handlers can carefully avoid clobbering registers that are
normally call-clobbered, but if they call subroutines of their own,
they must save them.  Simoilar comments apply to preemptive context
switches.  These are harms of having more registers which don’t depend
on whether those registers are clobbered or not.
