FIPS-137 is LPC-10, from 01984.  It is supposed to describe how to
encode understandable speech at 2400 baud in terms even a military man
can understand.  But it’s only 10 pages long.  What gives?

Some of the algorithms are described completely; for example, the
voice pre-emphasis block is specified as 1 - 0.9375 *z*⁻¹.  But the
heart of the LPC algorithm is computing the ten reflection
coefficients, estimating the pitch, and distinguishing voiced from
unvoiced frames (which are specified as 22.5ms).  The FIPS standard
specifies how you encode these values in 54-byte LPC-10 frames once
you’ve computed them, but doesn’t say anything about how to derive
them.

Campbell and Tremain of the NSA published “Voiced/Unvoiced
Classification of Speech With Applications to the U.S.Government
LPC-1OE Algorithm” in 01986, which is in the public domain.  Tremain
apparently published the original LPC-10 algorithm in 01982 and in an
earlier form in 01976.

There’s a [very nice Jupyter notebook by jhagesaw][0] which presents
LPC-10 using his mp5.py, which is [about 100 lines of Numpy code][1]
that students have to write.  It doesn’t quite explain how to
implement the whole thing, but it shows you what the result of each
step is supposed to look like.

[0]: http://www.isle.illinois.edu/speech_web_lg/coursematerials/ece401/fa2020/mp5overview.html
[1]: http://courses.engr.illinois.edu/ece401/fa2020/ece401_20fall_mp5.zip

There’s a [Fortran implementation from 01993][2] linked from the [CMU
AI repo][3] which is a mirror of
svr-ftp.eng.cam.ac.uk:/pub/comp.speech/sources/lpc10-1.0.tar.Z.  It’s
impossibly verbose.  This code, passed through f2c and hacked by Andy
Fingerhut (jaf) at WUSTL, forms part of, I think, sox, Asterisk, and
linphone today.

[2]: https://www.cs.cmu.edu/afs/cs/project/ai-repository/ai/areas/speech/systems/lpc/
[3]: https://www.cs.cmu.edu/afs/cs/project/ai-repository/ai/areas/speech/systems/lpc/0.html
