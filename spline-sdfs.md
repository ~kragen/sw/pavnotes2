In 3-D videogames there’s a [popular SDF-based letterform-decal-shader
technique due to Chris Green of Casual Numerics][2]: the font texture
contains the sampled SDF of the letterforms (the distance that the
center of each pixel is outside the letter boundaries) and this SDF is
linearly interpolated by the GPU’s texture mapper at each
perspective-projected screen pixel.  Then you simply threshold this
value to determine whether the pixel is black or transparent; a bit of
[smoothstep][0] on that threshold gives you antialiasing.  The
contours of the interpolated SDF make straight lines across each
square between four adjacent texture points, so this gives you a
piecewise-linear approximation to the letterform with somewhere around
40 texels per letter, which looks a hell of a lot better than 40
blurred-out pixels per letter.  [Sometimes you’d prefer to put the
outline’s corners somewhere else, though;][1] by default these fonts
have a bit of a Comic Sans look.

[0]: https://iquilezles.org/articles/smoothstepintegral/
[1]: https://computergraphics.stackexchange.com/questions/306/sharp-corners-with-signed-distance-fields-fonts
[2]: http://web.archive.org/web/20120217211316/https://www.valvesoftware.com/publications/2007/SIGGRAPH2007_AlphaTestedMagnification.pdf "Improved Alpha-Tested Magnification for Vector Textures and Special Effects"

If you were to interpolate the SDF with quadratic splines (requiring 9
texture samples as input) rather than linearly, which as far as I know
is not something GPU texture mapping units can manage, your
letterforms would have piecewise-quadratic outlines, changing
curvature at texel boundaries, instead of piecewise-linear.  Cubic
splines would similarly give you piecewise-cubic outlines.

Let’s rule out perspective mapping for a moment, considering only
affine transforms.  If you’re computing a scanline sample by sample
across such a cubically-interpolated texture, the SDF along that
scanline will also be piecewise-cubic.  You can tabulate a cubic
polynomial at regular intervals by applying a third-order additive
[prefix sum][3] to a constant function; a piecewise-cubic spline is
the third-order additive prefix sum of a piecewise-constant function.
There are [logarithmic-time parallel algorithms][4] for computing
this, but in serial hardware it only takes constant space and constant
time per sample (three additions and four registers).

[3]: https://en.wikipedia.org/wiki/Prefix_sum
[4]: https://www.cs.cmu.edu/~guyb/papers/Ble93.pdf "Prefix Sums and Their Applications, Guy Blelloch"

The underlying cubic spline surface can be fitted to the SDF of the
letterforms offline ahead of time, as with Green’s decal shader
approach.  A brute-force least-squares fit is one possibility.  This
representation can then be linearly transformed into third-order
differences ahead of time.  This suggests a pipeline with which a
simple raster CRT controller could support scalable, stretchable,
rotatable, shearable fonts with a pipeline costing roughly an *n*-bit
adder delay per pixel, though rotation and shearing might require
recomputing the weights ahead of time.  In its crude, unpipelined
form, the algorithm looks like this at each pixel:

    ttl--;
    if (ttl < 0) {
        ttl = ttlbuf[texindex];
        diff3 = diffbuf[texindex];
        texindex++;
    }
    diff2 += diff3;        // diff2 is the second derivative of the SDF
    diff += diff2;         // diff is the derivative of the SDF
    sdf += diff;
    frag_color = palette[sdf >> 4];  // or alternatively just the sign bit

Of course in hardware we can pipeline all these operations, so we are
doing three additions, three table lookups, an increment, a decrement,
a sign-bit test, and three two-way muxes (for the next values of
`texindex`, `ttl`, and `diff3` all at the same time, and the additions
are the slowest of these.  At the beginning of each scan line, we must
reset `ttl` and `texindex` and initialize `diff2` and `sdf`, and load
the first entries into `ttlbuf` and `diffbuf`; `diff3` will get
initialized itself when ttl goes negative.  Further values can get
loaded into `ttlbuf` and `diffbuf` concurrently with the scan line
being drawn; without dual-ported memory, this requires an arbitrator
that can delay a write until after satisfying a read.

I think these registers need to be somewhere around 8 bits, and we
probably want lookahead carry on at least the `sdf` adder, though the
others could use carry-save representation.

`ttlbuf` and `diffbuf` can in fact be FIFOs rather than random-access
memories; only `palette` is really random-access, and it can be quite
small, like 16 bits, though anything up to about 8192 bits would be
useful.

Multiple font weights can be provided from the same sampled letterform
SDFs in a crude fashion by loading different contents into `palette`,
as can outlines (for hollow letters, edge enhancement, or
nondirectional soft shadows).  If the sign bit of `diff` is also
available to index `palette`, shadows can be directional in *x*,
though not in *y*, which would require knowing the vertical gradient
of the SDF.  (That’s easy in a GPU pixel shader, though.)

The `ttlbuf` and `diffbuf` memories are relatively large; `diffbuf` in
particular needs to be on the order of 6 samples per letterform, and
plausibly 6 bits per sample, so probably the number of bits required
is over 9000.  Under some circumstances `ttlbuf` and `diffbuf` might
actually be single-entry buffers or two-entry FIFOs that get refilled
from some kind of external memory whenever there’s a vacancy.  Even a
shift-register memory could work.

All in all, this should require about 500 two-input NAND gates or 2000
transistors.

It might be desirable to be able to reset `sdf`, `diff`, `diff2`, and
`diff3` to fresh values at a transition between two tiles (font
glyphs, say); this permits loading a scan line of (non-rotated,
non-vertically-sheared) text into the buffers simply by copying the
relevant samples into the scanline buffer in time, rather than by
doing several dot products.  Resetting `diff2` and `diff3` at
preprogrammed points can also be used to get sharp corners on the
otherwise unavoidably smooth outlines.

Without rotation or vertical shearing, the intervals along the scan
line between crossings between texels are constant, eliminating the
need for `ttlbuf` as well.  This still permits scaling, including
nonuniform scaling (different scales in *x* and *y*) and horizontal
shearing.

The SDF-thresholding approach can also be used for drawing line
drawings with thick lines, but in general this requires computing at
least an approximation of the SDF for the whole scene.  [There’s a
well-known linear-time sampled SDF-computation algorithm][5], which
also provides [skeletonization][6], which unfortunately has quite
unpredictable memory access patterns; it accesses the pixels in the
order of their SDF through local relaxation.  I think you can derive
an efficient blockwise variant of the algorithm with predictable
memory access if you’re willing to either access the pixel blocks in a
similarly random order or to settle for an SDF approximation that is
bounded by, say, twice the block size.  This should be fine if the
desired line thickness is smaller than that.

[5]: http://canonical.org/~kragen/sw/dev3/circlegrid.html
[6]: https://en.wikipedia.org/wiki/Topological_skeleton

I think that this approach can also serve to do various kinds of
three-dimensional drawings by storing not only an SDF but also a
Z-depth for each pixel.  In particular:

- Wireframes drawn with black pixels are more visually understandable
  when background lines are cut where they cross foreground lines,
  leaving a white gap.  This can be achieved by a similar kind of
  local-propagation rule in which pixel distances to foreground lines
  (those with lower Z values) are considered "closer" than distances
  to background lines for purposes of the local propagation, and the
  pixel distances are then used for outlined rendering similar to the
  outlined-text trick.
- A pointcloud can perhaps provide a reasonable simulacrum of a solid
  object if we color each pixel according to its nearest projected
  cloud point, adjusting “nearest” to be shorter in directions similar
  to the surface normal, and to allow points with sufficiently lower Z
  values to defeat more distant points.  This involves three steps:

    1. Project each point in the cloud onto the screen, getting a
       Z-value for each.
    2. Perform the linear-time local propagation algorithm to find the
       “closest” point to each pixel.
    3. Color each pixel according to its “closest” pointcloud point,
       unless that point is too far away.

    This is almost certainly slower than traditional triangle-based
    approaches, but it might have interestingly different artifacts.
    
