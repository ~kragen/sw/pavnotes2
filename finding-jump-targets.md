My current bytecode design for Hadix does not provide information
about basic-block starts (jump targets), so we have to add *all* the
bytecode instructions to a table of possible jump targets as we
compile them, not just the actual jump targets.  A pretty wide variety
of simple optimizations I’ve thought of require this basic-block start
information; this could be obtained with an initial pass over the
bytecode to identify the jump targets, which would be relatively
simple and fast, or perhaps I could redefine the bytecode to provide
that information explicitly, as wasm does.

Branchlessly, this initial pass might look something like this:

    1: ldrb r0, [r6]               @ load next bytecode
       ldr r1, [r8, r0]            @ load length of this bytecode from table
       adds r3, r1                 @ compute pointer to next bytecode
       ldrsb r2, [r6, #1]          @ load possible operand byte
       adds r2, r3                 @ compute jump target, if any
       stmia r10, {r2, r6}         @ store jump origin and target
       ldrb r2, [r9, r0]           @ load whether or not to keep them (8 or 0)
       adds r10, r2                @ bump jump table pointer if so
       mov r6, r3
       cmp r6, r11                 @ are we done with the subroutine?
       bmi 1b                      @ if not, go on to the next bytecode

This is 11 instructions, 6 memory references, and a jump, so probably
20 cycles per bytecode, which is quite substantial.  5 of those
instructions and 4 of the memory references are only useful in the
case where the bytecode *is* a jump, which it usually isn’t; branching
around that would be a better idea.

    1: ldrb r0, [r6], #1           @ load next bytecode and increment
       ldrb r2, [r9, r0]           @ load whether or not we have an operand byte
       cbnz r2, 2f                 @ if so, skip to operand-byte-handling code
       cmp r6, r11                 @ have we finished?
       bmi 1b                      @ if not, go on to the next byte
       b 1f                        @ skip out of loop
    2: cmp r2, #8                  @ is it a jump instruction?
       beq 2f
       add r6, r2                  @ just add the operand size and continue
       b 1b
    2: ldrsb r2, [r6], #1          @ load the operand byte
       add r2, r6                  @ calculate jump target
       str r6, [r2, r10]           @ convert to table offset and store
       b 1b
    1:

The inner loop here is 5 instructions, 2 memory references, and a jump
in the usual case: 10 cycles per bytecode, twice as fast.  (This is
assuming our target CPU doesn’t do proper branch prediction, just
speculatively executes instructions after the branch instruction at
one cycle per instruction, so it’s important to keep the usual-case
code together.)  And even handling of jump bytecodes is only 12
instructions, 4 memory references, and 3 taken jumps, so it’s 25
cycles instead of 20.

I don’t think conditional instructions can compete with that because
there would need to be less than 2 of them.

However, my thinking about this table is confused.  I’m trying to
store the jump origin in it as a bytecode address, but that’s
impossible: there can be more than one jump origin for the same jump
target.  And the store into that table is going to be unaligned, which
means either terrible performance or a crash, and in any case the
address I’m storing is going to overlap other addresses.  And I’m not
counting the time to initialize the table to zeroes.  So the right
thing to do is something more like this:

    2: ldrsb r2, [r6], #1          @ load the operand byte
       add r2, r6                  @ calculate jump target
       strb r0, [r10, r2]          @ store nonzero in table
       b 1b

This way the table is one byte per entry, there’s no misalignment
penalty or overlapping, and we only need a 0.3 cycles per entry to
zero it.  We’re supposing the opcode byte is nonzero because 0 doesn’t
happen to be a jump bytecode, but otherwise we can probably arrange to
have a nonzero byte in some register ready for storing.

This table permits a simple constant-time query (3 cycles) to find out
if a given bytecode address is a jump target:

        ldrb r0, [r10, r6]         @ supposing r6 points at the query bytecode
        cbz r0, 1f                 @ skip to jump-target handling if new bb

As for the loop, if bytecodes with operands are reasonably common, a
hybrid approach might be better, adding the table entry to the
bytecode pointer unconditionally and then leaping out to jump handling
in the case of jumps.
