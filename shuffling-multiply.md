I was writing [a Tetris in ARM assembly][0] and ran into a troublesome
problem with rotating pieces.  I represent the board as a bitmap (one
word per row, leftmost 22 bits unused) and the falling piece as a
starting offset and a four-word bitmap in the same format.  This makes
things like drawing the board and collision testing fairly simple, but
it isn’t a particularly good representation for piece rotation.

[0]: http://canonical.org/~kragen/sw/dev3/tetris.S

Alexia Massalin suggested in her dissertation that a perfect-shuffle
instruction would be useful for this, especially for things like
bitplane displays.

But current CPUs have fast multiplication, which can make many copies
of a bit in different places; can we use that to shuffle?

In Python I just tried this:

    >>> ['{:04b}'.format(((n * 0x8421) >> 12) & 0xf) for n in [0x1110, 0x0111, 0x1011, 0x1101]]
    ['0111', '1110', '1101', '1011']

The low-order bits of the four nibbles are brought together in reverse
order.  This seems potentially useful for my problem: a 4x4 bit matrix
fits into a 16-bit or 32-bit register, and an AND with 0x1111 extracts
the low bits of each nibble.  But I’m not sure it’s a win; I think it
ends up being something like this:

        movw r2, #0x8421          @ preload multiplier
        movs r3, #0               @ initialize output
        movs r4, #4               @ initialize loop counter
    loop:
        ands r1, r0, #0x1111      @ select four input bits
        mul r1, r2                @ bring them together
        ubfx r1, r1, #12, #4      @ extract 4-bit field
        orrs r3, r1, r3, lsl #4   @ append it to result
        lsrs r0, #1               @ shift input right to next bits
        subs r4, #1               @ decrement loop counter
        bne loop

That’s 10 static instructions, using 5 registers, and 31 dynamic
instructions, which is almost two instructions per bit thus shuffled.
The 64-bit equivalent is maybe a little more appealing (though of
course Tetris pieces are at most 4x4, not 8x8): 8 iterations of almost
the same 7 instructions and 3 of input gives less than 1 instruction
per bit.

A perfect-shuffle and perfect-shuffle-high instruction pair (analogous
to multiply and multiply-high, *e.g.*, mul and umulh) would be a lot
better; for this case you only need the low half:

        psh r0, r0, r0, lsr #8  @ make 8 bit pairs
        psh r0, r0, r0, lsr #8  @ make 4 nibbles

You’re not done, though; the nibbles are in the wrong order.

In an architecture like the ARM where you have a shift available only
on the second operand, you might also want a `rpsh`,
reverse-perfect-shuffle, analogous to the ARM’s `rsb`, reverse
subtract.
