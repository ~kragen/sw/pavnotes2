I was watching [John Plant’s Primitive Technology video on his new
unidirectional blower design][0], which is itself pretty interesting
(see file `plant-blower.md`), and which mostly uses unfired mud to
hold things in place.  I was struck by the thought that adobe is
pretty rigid, and rigidity is commonly the most demanding part of
building machine tools, often achieved by using really unreasonable
amounts of cast iron, whose Young’s modulus [is reported as 66–157GPa
depending on grade and strain][4], though malleable iron is in the
162–170GPa range.  At times people have instead built concrete lathes,
and so-called “epoxy granite” (quartz-sand-filled epoxy) is a popular
material today.  But epoxy is expensive and toxic; even brick and
concrete are significantly more expensive than mud.  Could you use
adobe?

[0]: https://youtu.be/bS4_K5_tHbg
[4]: https://amesweb.info/Materials/Youngs-Modulus-of-Iron.aspx

In 02011, [Ioannou et al.][1] bought a bunch of adobe bricks,
oven-dried them, and made up some adobe walls from them, then tested
them after 3 and 43 weeks of curing; they report a compressive Young’s
modulus of 135 MPa from their experimental test.  However, they also
report easy plastic deformation, and strengths in the range of
0.2–1.6MPa using a 1.5% deformation criterion, which they consider
better than the 1.6–12.3MPa they got using a
maximum-engineering-stress criterion because the deformation there was
about 33%.

> The results of the loading-unloading test (Fig. 7) indicate that the
> behaviour of adobe masonry under compression is non-linear and is
> characterized by intense plasticity and deformability. The parts of
> the stress–strain diagram that correspond to the application of
> loading show that the material exhibits progressive hardening.  The
> form of the unloading branches reveals that when the exerted load is
> released, induced deformation is only partially removed, while
> considerable inelastic deformations remain present. Plastic
> deformations start to develop even when the specimen is subjected to
> low compressive stresses at the region of 0.08 MPa. This behaviour
> can be attributed to the fact that the sliding and displacement
> mechanisms, which take place between the soil grains due to the
> application of pressure, are non-reversible.

[1]: https://www.researchgate.net/profile/Ioannis-Ioannou-3/publication/271492466_A_study_of_the_mechanical_behaviour_of_adobe_masonry/links/57fb783f08ae280dd0c4b542/A-study-of-the-mechanical-behaviour-of-adobe-masonry.pdf "A study of the mechanical behaviour of adobe masonry, R. Illampas, I. Ioannou & D. C. Charmpis, DOI 10.2495/STR110401"

This suggests that maybe both some pretensioning and some compacting
would be a good idea if you want to try to use adobe’s rigidity for
machine tools.  Compacting seems straightforward (Factor E Farm’s
compressed-earth brick press comes to mind) but pretensioning seems
like it could be more difficult.

They also report variations of more than a factor of 3 in adobes from
different manufacturers, even within Cyprus.

Other researchers have reported Young’s moduli of around 100MPa or
170MPa or 200–220MPa ([Blondet and Vargas 01978 as reported by Nicola
Tarque][2]), 59–211MPa ([G. A. Jokhio et al. 02017][3]), 143–145MPa
([Parisi et al.][5]), etc.

These numbers all suggest that you need to use on the order of 1024
times as much volume of adobe as cast iron, and on the order of 256
times as much mass, to reach the same rigidity.  So if a conventional
Monarch engine lathe made of iron castings weighs 1500kg (190ℓ of
iron), an adobe lathe might weigh 400 tonnes (50kℓ of adobe), if you
could make it work at all.  This suggests that probably just using
ordinary adobe bricks is not a practical solution; the adobe required
will not fit into the space available.  That is, even if the entire
volume of the lathe is filled with adobe, it still wouldn’t be quite
stiff enough, and because of the usual scaling laws, making it larger
makes the problem worse.

The adobe is only too floppy by about one order of magnitude, though,
so by the same token, you ought to be able to get enough rigidity out
of adobe at a smaller scale, like maybe a tabletop lathe.

[2]: http://nicolatarque.weebly.com/uploads/1/2/6/9/12699783/tarque_-_2011_-_chapter_3.pdf
[3]: https://iopscience.iop.org/article/10.1088/1757-899X/318/1/012030/pdf "Compressive and Flexural Tests on Adobe Samples Reinforced with Wire Mesh, by G. A. Jokhio, Y. M. Y. Al-Tawil, S. M. Syed Mohsin, Y. Gul, and N. I. Ramli, IOP Conf. Series: Materials Science and Engineering 318 (02018) 012030, DOI 10.1088/1757-899X/318/1/012030, CC-BY"
[5]: https://iris.unica.it/retrieve/e2f56eda-d3e8-3eaf-e053-3a05fe0a5d97/Comp.%20Str..pdf "Experimental characterization of Italian composite adobe bricks reinforced with straw fibers, by Fulvio Parisi, Domenico Asprone, Luigi Fenu, and Andrea Prota, DOI 10.1016/j.compstruct.2014.11.060, CC-BY-NC-ND"

If you wanted to increase the rigidity of adobe at the expense of
strength, you could probably incorporate more gravel into it.
Quartz’s Young’s modulus is about 70–100GPa, three orders of magnitude
higher than the values reported for adobe and in the same range as
cast iron; feldspar is similar.  I think fired-clay bricks and
concrete blocks have Young’s moduli in the range of 15–50GPa, so they
should serve nearly as well as cast iron while being very much
cheaper.  Perhaps even lime-cement blocks would serve adequately, or
adobe grouted with a small amount of lime.  And even wood is supposed
to have on the order of 10GPa of Young’s-modulus stiffness, and is of
course the traditional material for wood-cutting lathes.

