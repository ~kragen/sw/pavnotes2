- ICP plasma cutting torches: by using inductively coupled plasma you
  can generate plasma to cut nonconductive materials.
- SIMD broadcast terminals/processors (in a token ring?  systolic
  array processor?)
- aluminum-fueled diesel: aluminum yield 559 kJ/mol O, water steals
  284 kJ/mol O and yields hydrogen, which upon mixing with air yields
  those 284 back.  of course silicon, magnesium, titanium, and
  probably zinc or calcium would also work.  and turbine and rocket
  engines too.
- gibbsite-cemented ruby?
- sulfur-cemented sand?  “Sulfur concrete”, analogous to blacktop.
  65% JSC-1 lunar regolith simulant and 35% sulfur: “compressive
  strength…33.8 MPa…tensile strength…3.7 MPa. Addition of 2% metal
  fiber increase[s] the compressive strength to 43.0 MPa”.  Sulfur
  cost US$90 per tonne in 02021, up from US$24 in 02020, US$51 in
  02019, US$81 in 02018, and US$46 in 02017.  This is cheaper than
  organic thermoplastics by at least an order of magnitude.
- oh interesting, hydrogen reduces iron from ilmenite at only 800°!
- arbitrarily rotated non-antialiased pixel ellipses are just sheared
  paraxial ellipses
