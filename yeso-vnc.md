I’ve written a simple graphics library called Yeso, for which I
currently have frontends in (C)Python, Lua(JIT), and C, and backends
for X11, the Linux framebuffer, and its own graphics protocol called
Wercam.  Right now, I want to add the ability to use Yeso to write
servers accessed over VNC.  (Other notes about this include file
`nonblocking-vnc.md` and file `binary-serialization.md`.)

So this file contains a bunch of my notes on VNC and on Yeso, somewhat
organized.  The VNC protocol is defined in [RFC 6143][5], which is
only 38 pages, and more recently in [rfbproto.rst][18], which is 98
pages.

[18]: https://github.com/rfbproto/rfbproto/blob/master/rfbproto.rst

Yeso strategies
---------------

In some sense I’d like to maximize the product of the number of Yeso’s
frontends and its backends, on the theory that its value increases
proportional to that product.  The other backends I’ve thought about
are Windows GDI, network-remote X11, SDL, OpenGL, svgalib, VNC,
Quartz, Wayland, SurfaceFlinger, and WebAssembly.  I’ve written in
file `language-bindings.md` about the possibility of adding more
frontends, including C#, GForth, PHP, C++, assembly, Java, Golang,
Perl, Rust, OCaml, D, and Objective-C.

VNC server scenarios
--------------------

There are a couple of different scenarios for writing VNC servers with
Yeso:

1. I want to compile a regular Yeso application, which normally opens
   a window on the X11 screen or whatever, to instead run as a VNC
   server.  This is tricky because the regular Yeso application
   doesn’t have a way to configure VNC parameters like display number
   (added to port 5900), listening IP addresses (usually just
   localhost), a frame rate limit, usernames and passwords, TLS
   configuration, making outgoing connections to viewers in “listening
   mode” on port 5500, etc..  The normal way for the application to
   decide whether to keep consuming CPU to update the display when
   there are no events (calling `yw_wait` with 0 or not) should work.
   The single application most likely to be used in this way is the
   Wercam server.
2. I want to write a new application that uses VNC to display to
   clients — potentially multiple clients, potentially seeing
   different views and authorized in different ways.  Not only could
   you do Wercam this way, you could also do things like non-massively
   multiplayer online role-playing games, collaborative document
   editing, pair programming, board games, or chat programs.  So the
   program uses Yeso for drawing internally, but it isn’t simply a
   regular Yeso graphical application calling `yw_open`.

### Collaborative shared desktops ###

As one example, you could imagine a collaborative windowing
environment in which the virtual desktop is potentially much bigger
than what’s visible to any one client, and each window has at most one
client’s keyboard focused on it; if another user focuses your window,
you lose focus and your keystrokes start going nowhere.
Alternatively, it might raise a flag saying they’d *like* to focus
your window, a request you could accept, reject, or leave pending.
This would be potentially more useful for pair programming.  You could
even imagine more elaborate permission schemes.

Panning the virtual desktop by, for example, quarter screenfuls, would
make it easy to avoid the situation where you accidentally scroll a
few pixels of your window off the screen and it becomes unreadable
until you carefully scroll them back.  This could also reduce the
amount of update traffic (though CopyRect might be adequate for such
scrolling updates) and the amount of data that has to go to the
client’s physical framebuffer.

(There’s a section below about relative vs. absolute mouse motion
which suggests that it actually might be best to have buttons to press
to scroll around the desktop.)

Some kind of audio sharing would also be useful for pair programming
in particular.  There seem to be some audio extensions that are likely
useful for this.

Conventional windowed programs would work well in this environment
without requiring any special handling of, for example, multiple
cursors — enormously better than in conventional shared VNC viewers,
where one user’s Alt key might screw up another user’s F or E
keystroke.  Programs that are written to support multiple cursors
might be able to accept the focus of multiple clients, but for most
programs, a single input focus per window is likely adequate.

Updating the framebuffer without bufferbloat
--------------------------------------------

In VNC, the server *cannot* send framebuffer updates without the
client requesting them; as [the RFC says][5]:

> The update protocol is demand-driven by the client.  That is, an
> update is only sent from the server to the client in response to an
> explicit request from the client.  This gives the protocol an
> adaptive quality.  The slower the client and the network are, the
> lower the rate of updates.  With typical applications, changes to
> the same area of the framebuffer tend to happen soon after one
> another. With a slow client or network, transient states of the
> framebuffer can be ignored, resulting in less network traffic and
> less drawing for the client. (...) The server must not send
> unsolicited updates.  An update must only be sent in response to a
> request from the client.  When several requests from the client are
> outstanding, a single update from the server may satisfy all of
> them.

I thought not ([my existing vncs.go test server][7] only calls
`sendFramebufferUpdate` when it receives a framebuffer update request,
and the observed behavior is that the screen in the VNC client updates
all the time anyway), but I wasn’t sure.  So the client is in control
of the frame rate, and in particular can avoid requesting updates
faster than it’s receiving them, so bufferbloat shouldn’t be a
problem.  The server chooses which rectangles to send updates for, so
it can send a response with no updated rectangles if nothing has
changed (or maybe one tiny one).

This does result in a framerate issue where you only get one frame
drawn per round trip time, so [people say][23] things like

> VNC always felt slow and laggy to me, even locally over gigabit.

Probably the Yeso-VNC server state ought to include a framebuffer to
satisfy client framebuffer update requests from; the Yeso API doesn’t
include a way to request the drawing of a particular part of the
window.  If the server remembers the last framebuffer it sent a
certain client, it can potentially avoid sending areas that haven’t
changed, though at the expense of having to do the comparison.

It still might be useful to limit the update rate or bandwidth
artificially for cellphone or datacenter billing purposes.

I don’t think the VNC protocol has a way for the *server* to detect
when there’s high round-trip latency due to bufferbloat.  And, because
the server can send an update in response to multiple update requests,
the client can’t always reliably determine this either, because there
isn’t a request ID in the framebuffer update message, just a message
type and a number of rectangles.

All this is leading up to saying I was thinking I might have to
[`setsockopt` with `SO_SNDBUF`][0] to limit the amount of queued data
on the local machine.  On this MicroPC,
`/proc/sys/net/core/wmem_default` is 212992, which at 64kbps would be
33 seconds of latency.  But I think that won’t be a problem.

[0]: https://man7.org/linux/man-pages/man7/socket.7.html
[7]: http://canonical.org/~kragen/sw/dev3/vncs.go
[23]: https://old.reddit.com/r/linuxquestions/comments/10winzq/how_do_i_fix_first_person_mouse_movement_in_a_vnc/

The right solution to this is some kind of sliding-window protocol
where each framebuffer update has a serial number (included as a
pseudo-encoding) and the client requests updates up to a given serial
number (with an alternative to the FramebufferUpdateRequest message,
if the server somehow advertises the capability to receive that
alternative).  Because to a significant extent we’re talking about
isochronous data here, the client request should also contain some
kind of frame rate limit, either a maximum number of bytes per second
or a minimum number of milliseconds per update.  This would make it
possible to see smooth movement despite network lag, which is
important for improving the comprehensibility of animations.

VNC browser clients
-------------------

One option is [noVNC][20], including Pierre Ossman as a contributor.
It uses a WebSocket proxy.

[20]: https://novnc.com/info.html

VNC extensions for TLS
----------------------

TLS support is potentially important.  The VNC client I’m using on
Android is [AVNC, which supports][1] AnonTLS and VeNCrypt as well as
VNC over ssh.  [MultiVNC also supports][2] AnonTLS and VeNCrypt.

VNC extensions for better bandwidth usage, such as “tight” encoding
-------------------------------------------------------------------

Both also support “tight” encoding, which suggests that it might be
important; [it’s from 02001][3], and [although it apparently uses
JPEG, QEMU has a PNG variant][4].  [The TightVNC author][8] offers
commercial licensing terms for his encoder code and has [a page
documenting the original TightVNC compression experiment results from
02001][9], comparing to Hextile and Zlib encodings.  It transmits
11–75% less data than Zlib.

VNC proxies
-----------

The TightVNC compression experiment results use data recorded with
[Tim Waugh’s rfbproxy][15] (originally [on SourceForge][16]), which
provides recording and playback of VNC sessions, though only if the
client is willing to accept the same pixel format (it doesn’t
translate pixel formats).

Another potentially useful thing to do with VNC proxies, incidentally,
might be recording and replaying user interactions, but rfbproxy
doesn’t support that, just recording and replaying the screen data.

Also, of course, logging of data for debugging the VNC protocol, and
for measuring latency.

For some purposes, periodic screenshots and activity logging would be
useful; I could use this, for example, to figure out what time to bill
a client for.

A proxy that adds authentication onto an existing VNC server would be
useful.  Or better compression.

Exporting existing X-Windows displays over VNC
----------------------------------------------

It seems like the active fork of TightVNC is [TigerVNC][12], which
[includes a way to export an existing X-Windows display][13] called
x0vncserver, using XDamage if supported.  rfbproxy’s documentation
says you can also just `Load "vnc"` in your XF86Config with an `Option
"passwordFile".` And there’s also something called `x11vnc` which can
export even a single window.

[1]: https://play.google.com/store/apps/details?id=com.gaurav.avnc&hl=en_IN
[2]: https://f-droid.org/packages/com.coboltforge.dontmind.multivnc/
[3]: https://www.researchgate.net/publication/3936130_VNC_tight_encoder-data_compression_for_VNC
[4]: https://wiki.qemu.org/Features/VNC_Tight_PNG
[8]: https://www.tightvnc.com/decoder.php
[9]: https://www.tightvnc.com/archive/compare.html
[12]: https://tigervnc.org/
[13]: https://tigervnc.org/doc/x0vncserver.html
[15]: https://github.com/ribasushi/rfbproxy
[16]: https://rfbproxy.sourceforge.net/

More about AVNC
---------------

AVNC, incidentally, is fantastic; aside from “tight encoding”, it has
a pretty reasonable way to deal with touchscreens, a way which is
preferable in many cases to native touchscreen interactions, where you
can’t see what you’re tapping on, and where the keyboard takes up most
of your screen.  It supports two-finger zooming and has a “touchpad
mode” for the mouse cursor where you drag anywhere on the screen to
move the mouse cursor around, possibly panning the display, tap on the
screen to click, and long-press to right-click.

AVNC also has a mode where you can compose a line of text in a
client-side buffer (permitting the use of autocorrect) and then send
it all to the server when you’re done editing it.

I don’t know how to emulate the mousewheel in AVNC’s UI.  Two-finger
drag just pans the screen.

Alternative transports
----------------------

VNC is almost a pure byte-stream protocol, so it’s reasonable to
contemplate running it over channels other than plain TCP, although I
don’t think any existing VNC clients support this.  For example, you
could imagine running it over a bidirectional SSH pipe, or over a
serial port.  The only exception is that there is supposed to be some
way to detect when the connection has been opened or closed, which
might be tricky over a serial port without handshaking lines; how do
you recover if the server sends the initial handshake message before
the client has started, for example?

At least [TigerVNC does support Unix-domain sockets][13].

The VNC protocol’s design errors
--------------------------------

### Coddling the client ###

A kind of annoying thing about the VNC protocol is that, although in
normal use the client is necessarily running on a big expensive
computer (because it has a full-color display) while the server could
be a stupid microcontroller, the protocol puts most of the burden on
the server; [as RFC 6143 explains][5]:

> The emphasis in the design of the RFB protocol is to make very few
> requirements of the client.  In this way, clients can run on the
> widest range of hardware, and the task of implementing a client is
> made as simple as possible. (...)  This negotiation [of pixel data
> formats] has been designed to make the job of the client as easy as
> possible.  The server must always be able to supply pixel data in
> the form the client wants.

[5]: https://datatracker.ietf.org/doc/html/rfc6143

This is even worse than it sounds, because one of the possibilities is
8-bit paletted data!  (With a palette sent by the server with
SetColorMapEntries, using 16 bits per color channel.)  I don’t know
how to test my VNC server for handling of unusual pixel data formats,
since all the VNC clients I’ve tested with let me use the same format.
(Fortunately at least the ServerInit message lets the server specify
its preferred pixel format.)

This is also the source of a hassle with rfbproxy: it can’t replay
16-bit-pixel sessions to 32-bit-pixel clients or vice versa.

Another kind of annoying thing is that you can’t specify pixel formats
on a per-rectangle basis, which otherwise might be useful for
selectively reducing color depth in certain areas of the screen.  The
main purpose for this would be bandwidth reduction, though, and the
existing encodings pretty much have that covered.  TRLE in particular
does use palettization.

But there are even bigger design errors.

### Fixed-format binary client-to-server messages ###

The protocol represents PointerEvent { ButtonMask:0 XPosition:572
YPosition:24 } as the bytes, in hexadecimal, 05 00 02 3c 00 18.
Lovely efficiency, a six-byte message to tell us where the mouse is,
though we do still have to byteswap the 16-bit fields.  This format
requires writing specific serialization code on the client and
deserialization code on the server, and it imposes arbitrary limits:
no more than 8 buttons, no more than 65536 pixels in X or Y, no
negative positions, and no extra fields like pressure. subpixel
position, or orientation.  And, to actually send it across the network
to the server without waiting, it probably gets a 20-byte TCP header
prepended, and a 20-byte IP header, and a 14-byte Ethernet header, for
a total of 54 bytes of overhead and 60 bytes of total packet.  Most
likely nowadays it’s encrypted in an SSH packet, which is a minimum of
20 bytes of data.

I tried benchmarking the encoding process by writing an encoding
program in C, but at any reasonable optimization level, GCC’s
optimizer kept optimizing my entire benchmark loop away.  But it
should be apparent that it’s on the order of 15 instructions in the
usual case (untested):

        mov rax, [rdi]             # load buffer pointer from buffer struct
        lea rax, [rax + 6]         # add 6 bytes to buffer pointer
        cmp rax, [rdi + 8]         # compare to buffer end pointer
        jae 1f                     # jump to buffer reallocation code if necessary
        mov byte ptr [rax - 6], 5  # store type byte into buffer
        mov rcx, [rsi]             # load button mask field from in-memory struct
        mov [rax - 5], cl          # store buttons byte into buffer
        mov rcx, [rsi + 8]         # load x position from in-memory struct
        mov [rax - 4], ch          # store bytes in big-endian order
        mov [rax - 3], cl          # (there’s probably a better way to byteswap)
        mov rcx, [rsi + 16]        # load y position from in-memory struct
        mov [rax - 2], ch
        mov [rax - 1], cl 
        mov [rdi], rax             # update buffer pointer
        ret

Consider an alternative: "b= x=572 y=24\n" would have been 14 bytes, 8
bytes extra.  But it would have allowed backward-compatible additions
of fields and not required special serialization and deserialization
code for each struct type.  I wrote some anyway in C for the sake of
comparison (it was 22 lines and rechecked the buffer bounds before
appending every byte) and it took 382 instructions and 40 nanoseconds
when compiled with -O.

When VNC was invented in 01997, it would have taken longer than 40
nanoseconds, but it would have been about the same 382 instructions.
Or, given that my C version of the fixed-format code they chose took
54 instructions when compiled the same way, the free-format code would
probably be more like 100 instructions.  Somewhere between 6 and 24
times as much time.  The efficiency difference is real!

But those 100–400 instructions only have to run as often as the mouse
reports a new position — generally not more than 100 times a second.
The most time you could be wasting would be 40'000 instructions per
second.  That wouldn’t even bog down an Apple ][.

Essentially, the vast bulk of the computation done in the VNC
protocol, on either the client or the server, is shuffling around
pixel data.  That code path *does* have to be optimized.  But the
system’s performance doesn’t really depend on whether client-to-server
events are represented in the protocol as text or binary.  Even if the
text format were three times as big, we’re talking about going from
600 bytes of framebuffer update requests per second to 1800, 14kbps of
data to encode and decode.  Optimizing that encoding at the expense of
extensibility or code complexity is foolish.

### Inability for the server to advertise capabilities ###

The VNC ServerInit message just specifies the width, the height, the
pixel format, and a name.  The *client* can advertise capabilities in
a SetEncodings message, but the server has no such ability in the
standardized VNC protocol.  (You could define a pseudo-encoding for
the server to advertise capabilities in, once it has the chance to
send a framebuffer update, but nobody has).  The “Tight security type”
(see below) does add such a feature in the form of an extended
ServerInit message.

VNC authentication
------------------

I see the RFC’s way to use passwords, called “VNC authentication”, is
plausibly somewhat secure (though the RFC says it’s “cryptographically
weak”):

> The client encrypts the challenge with DES, using a password
> supplied by the user as the key.  To form the key, the password is
> truncated to eight characters, or padded with null bytes on the
> right.  The client then sends the resulting 16-byte response.

rfbproto.rst clarifies:

> The lowest bit of each byte is considered the first bit and the
> highest discarded as parity. This is the reverse order of most
> implementations of DES so the key may require adjustment to give the
> expected result.

This still has some security problems (DES is weak enough to
brute-force now, it’s apparently only a single iteration, it does
nothing to counter MITM attacks because it doesn’t produce a session
key the way SRP does, and, because the client provides no salt, a
malicious server can send a canned challenge for which it’s already
precomputed a dictionary) but at least it doesn’t directly reveal the
password to the server and doesn’t permit replay attacks.  I hadn’t
realized this!  Since this is authentication type 2 (type 0 being
“invalid” and type 1 being “none”) I assume this is what my existing
VNC clients and servers are doing to each other.  This is reassuring.

If you wanted to implement this protocol without storing the client’s
password on the server, you could pregenerate a number of challenges
(perhaps a hundred thousand or so), generate the corresponding
responses, and store a database of those challenge-response pairs.
Each new connection would use a new challenge-response pair, perhaps
deleting it from the database to prevent replay attacks.

In protocol 3.3 the authentication protocol is unilaterally decided by
the server, changing the security handshake.

The RFC lists 9 more security-type entries.  3–15 and 128–255 are
reserved for RealVNC, and the others are 16: Tight (not the
encoding!), 17: Ultra (an alternative VNC server that supports the
Tight encoding), 18: TLS, 19: VeNCrypt, 20: GTK-VNC SASL, 21: MD5 hash
authentication, and 22: Colin Dean xvp.  All of these are deprecated
in the RFC as “historic assignment”, despite being registered with
IANA.

[19]: https://github.com/xvpsource/xvp?tab=readme-ov-file

rfbproto.rst does document a number of these, and reassigns 5, 6, 13,
129, 130, and 133 to variants of RSA-AES.  I’m not sure what AnonTLS
is.

It says VeNCrypt is “a generic authentication method which
encapsulates multiple authentication subtypes”, including “Plain
authentication (should be never used)”, “TLS encryption with no
authentication”, “X509 encryption with no authentication”, “TLS
encryption with SASL authentication”, etc.

ServerInit and overall display configuration
--------------------------------------------

The ServerInit message includes “the name associated with the
desktop”, which seems like a good thing to set from the Yeso window
title.  As with Yeso, though, I don’t think this is changeable later,
though the RFC mentions pseudo-encoding -307, “Peter Astrand
DesktopName”, probably referring to the TigerVNC founder Peter
Åstrand.

Although the ServerInit message provides the width and height,
TigerVNC has an `-AcceptSetDesktopSize` option, so apparently there's
a way for the client to tell the server what size its display is so
the server can resize the virtual display to fit?  Yeso programs can
generally handle that.  I suspect this is the “251: Pierre Ossman
SetDesktopSize” client-to-server message type listed in the RFC, and
that it originates in noVNC.

Aha, it’s documented in rfbproto.rst.

There’s a `DesktopSize` pseudo-encoding (-223).  The client sends it
to declare that it’s willing to cope with display size changes; a
rectangle with this pseudo-encoding provides the new display size as
its width and height.

Unicode
-------

In the RFC, `ClientCutText` is documented as Latin-1, which is a huge
problem.  §6 of rfbproto.rst says:
  
> The encoding used for strings in the protocol has historically often
> been unspecified, or has changed between versions of the
> protocol. As a result, there are a lot of implementations which use
> different, incompatible encodings. Commonly those encodings have
> been ISO 8859-1 (also known as Latin-1) or Windows code pages.
> 
> It is strongly recommended that new implementations use the UTF-8
> encoding for these strings. This allows full [Unicode] support, yet
> retains good compatibility with older RFB implementations.

But rfbproto.rst also specifically says `ClientCutText` and
`ServerCutText` are Latin-1.  There’s an extension that fixes this.

Relative vs. absolute mouse movement, mouse warping, mouselook, and pointer lock
--------------------------------------------------------------------------------

PS/2 and USB mice provide relative movement information rather than
absolute positions, while a VNC viewer window generally receives an
absolute mouse position from the windowing system it’s running under.
So VNC’s client-to-server `PointerEvent` contains 16-bit absolute X
and Y coordinates, plus an 8-bit button mask, nothing else.

This usually works fine if the VNC server is something like an X
server, but it’s a problem when VNC is used as an interface to virtual
machines that want to emulate PS/2 or USB mice; QEMU, Xen, and VMWare
are three examples.  To solve this problem without VNC, QEMU running
in an SDL window on your desktop will capture your mouse if you click
on its window, requiring some kind of Vulcan nerve pinch (ctrl-alt-G,
I think).  I think this is implemented under X-Windows by hiding the
mouse cursor and calling XWarpPointer() periodically to keep the mouse
from escaping the window.  That’s how Minetest does the analogous
thing, anyway.

By default, though, if you’re accessing QEMU over VNC, there’s some
sort of random offset between the mouse pointer drawn by the guest
operating system and the one you’re moving around over the VNC viewer
window, and you hit a wall when you move your mouse out of the VNC
viewer, because all QEMU can send to the guest OS as an emulated USB
mouse is relative motion, and the guest OS doesn’t tell QEMU where the
mouse pointer is.

The standard workaround for this is to [run QEMU with `-usbdevice
tablet` or, in more recent QEMU, `-device
nec-usb-xhci,id=usb,bus=pci.0,addr=0x4 -device usb-tablet`][21].  This
works because graphics tablets provide absolute positions to the guest
OS, absolute positions like the data the VNC protocol gives QEMU.
[This requires the guest OS to add support for the tablet device][22]
but is otherwise a relatively reasonable workaround.

(Even without VNC, this seems like it would be a better user
experience, because the QEMU window would work fine without being able
to capture the mouse.)

By contrast, in theory, deriving relative position data from absolute
position data is easy; you just subtract the previous pointer
position.  This is how AVNC’s “touchpad mode” moves the mouse around
on the VNC server: wherever you put your finger on the screen serves
as the reference point from which your movement is tracked, so the
mouse cursor makes the same movement elsewhere on the screen.

However, [in practice, this doesn’t always work][23]:

> I’m trying to use a VNC server to play [first-person shooter] fps
> games, but when launching any fps game, the view will be rapidly
> snapped to the ground and the player will start spinning.

And in the relevant, still-open [TurboVNC bug report #67 by
marino-mrc][24]:

> Basically the mouse works well during the [VNC] session, in the
> [Steam] menu[,] and in the game menu[,] but when I start to play[,]
> it becomes “crazy”[,] and the game is out of control.  If I press
> “esc”[,] the mouse works normally[,] and I can exit the game using
> the [Half-Life] menu.

I think this happens because of how games typicaly implement
mouselook; as I said above about Minetest, in order to capture the
mouse, they just use XWarpPointer() or its moral equivalent to move
the mouse cursor back to the center of the window whenever they detect
that it’s somewhere else, and use the delta from the center of the
window as the relative movement.  But if the user is using an absolute
pointing device, the new mouse cursor position may be quite far from
the center of the window, and attempting to warp the pointer won’t put
it any closer.

On a graphics tablet or touchscreen, deriving relative movements from
absolute positioning should be restarted when there’s a new touchstart
event; you don’t want to treat the large delta between where you
picked up your finger and the new position you put it down as a large
sudden movement.  But a mouse pointer over the VNC viewer window does
not have the same advantage; the best you can do is probably to move
the mouse outside the VNC viewer.  So having the VNC viewer capture
the mouse really is the best solution for cases like this.

VMWare and QEMU have added two separate extensions to the VNC protocol
to solve this problem.

I really like mouselook but it seems like it's probably best to not
rely on being able to implement it.

Here’s a further list of links to digest to maybe fill out this
section at some point:

* [TigerVNC feature request #1121: support for relative mouse
  movements][25] (closed)
* [TigerVNC feature request #1198: Add support for notifying clients
  about pointer movements][37] (using VMWare’s extension, merged by
  Ossman)
* [PiKVM handbook entry on absolute and relative mouse movements][26]:
  “When using relative mode, the browser will exclusively capture your
  mouse when you click on the stream window in PiKVM once. When you
  press Esc, the browser releases the mouse.”
[37]: https://github.com/TigerVNC/tigervnc/pull/1198
* [Relative mouse and keyboard over SSH (or: I just want to play
  Minecraft)][27] (a hack using a simple Python script)
* [TigerVNC feature request #619: Mouse
  synchronization/grabbing/capture/wrapping][28] for CS:GO and
  Bioshock (new issue)
* [RealVNC RelativePtr setting][29]
* [QEMU Pointer Motion Change Pseudo-encoding][30] in the RFB protocol
  spec: “The server can switch between different pointer motion modes
  by sending a rectangle with this encoding. If the x-position in the
  update is 1, the server is requesting absolute coordinates, which is
  the RFB protocol default when this encoding is not supported. If the
  x-position in the update is 0, the server is requesting relative
  deltas.”  Sounds like a race condition.
* [The Adder VNC viewer has a “mouse calibration” feature][31] where
  it sends artificial pointer events and looks at what changes on the
  screen, but they recommend just emulating an absolute pointer
  device.
* [Qemu & VNC - how to use absolute pointing device][32]
* [TightVNC open bug report from 02007 on SourceForge][33]: “I tested
  this with RealVNC - RealVNC supports relative mouse movements and
  there, the games are playable. However, RealVNC uses a proprietary
  protocol extension for this. TightVNC could be easily extended using
  the QEMU Pointer Motion Change pseudo-encoding or - even better and
  more flexible - the General Input Interface extension using gii
  pseudo-encoding. The [latter] would have the additional benefit of
  adding Joystick/Gamepad support.”
* [Cendio ThinLinc bug 7794, support relative mouse movements, NEW
  since 02021][34].  This is reported by Pierre Ossman, one of the
  main authors of noVNC and TigerVNC.  This outines what Ossman thinks
  the behavior should be for ThinLinc and outlines some use cases.  It
  also mentions the “Web browser pointer lock API”, which is the way
  to solve the problem for browser 3D games.
* [Discussion of Jonas Ådahl’s pointer warp emulation for
  xwayland][35]: “With Wayland, the X server is no longer in control
  of the pointer position, and as such, clients warping the pointer
  has no effect. This causes many applications (mostly games) to fail
  to function properly, as they depend on the warp requests to
  actually take effect. (...) With pointer warp emulation, a client
  warping the pointer is translated into Xwayland using of the
  relative pointer[0] and pointer constraints[1] Wayland protocols,
  while faking the pointer position exposed to the rest of the X
  server.”
* [Implementation of Jonas Ådahl’s pointer warp emulation for
  xwayland][36]
* [noVNC open feature request #1520: Support grabbing the pointer with
  the Pointer Lock API][38]
* [noVNC open feature request #1493: Full mouse control][39]
* [VMWare Blast relative mouse support for 3D applications like
  AutoCAD][40]
* [VMWare Remote Console relative vs. absolute mouse handling][41]


[21]: https://unix.stackexchange.com/questions/555082/host-mouse-pointer-not-aligned-with-guest-mouse-pointer-in-qemu-vnc
[22]: https://dev.haiku-os.org/ticket/5989
[24]: https://github.com/TurboVNC/turbovnc/issues/67
[25]: https://github.com/TigerVNC/tigervnc/issues/1121
[26]: https://docs.pikvm.org/mouse/
[27]: https://yingtongli.me/blog/2019/11/18/input-over-ssh.html
[28]: https://github.com/TigerVNC/tigervnc/issues/619
[29]: https://help.realvnc.com/hc/en-us/articles/360002254618-RealVNC-Viewer-Parameter-Reference#quality-0-53
[30]: https://github.com/rfbproto/rfbproto/blob/master/rfbproto.rst#777qemu-pointer-motion-change-pseudo-encoding
[31]: https://support.adder.com/tiki/tiki-index.php?page=VNC%3A%20Mouse%20Calibration
[32]: https://superuser.com/questions/341594/qemu-vnc-how-to-use-absolute-pointing-device
[33]: https://sourceforge.net/p/vnc-tight/feature-requests/482/
[34]: https://bugzilla.cendio.com/show_bug.cgi?id=7794
[35]: https://lists.x.org/archives/xorg-devel/2016-April/049351.html
[36]: https://github.com/jadahl/xserver/commits/wip/xwayland-pointer-warp
[38]: https://github.com/novnc/noVNC/pull/1520
[39]: https://github.com/novnc/noVNC/issues/1493
[40]: https://docs.vmware.com/en/VMware-Horizon-Client-for-Windows/2203/horizon-client-windows-user/GUID-D49EF6B2-14D5-437A-AF31-F806161B34F9.html
[41]: https://knowledge.broadcom.com/external/article/326181/erratic-mouse-pointer-movement-in-virtua.html

More on VNC protocol extensions
-------------------------------

The RFC doesn’t document the Tight encoding, just TRLE, even though it
was evidently ten years old at the time the RFC came out.  It falsely
claims, “Other encoding types exist but are not publicly documented.”
So it seems like RealVNC was playing kind of dirty even then.  Now
they’re much more so, falsely claiming that [RealVNC invented the VNC
protocol][17]:

> The RFB protocol is used to transmit screen pixel data from one
> computer to another over a network, and send control events in
> return.  It’s a simple yet powerful protocol invented by RealVNC.

Because the Tight encoding results from 02001 didn’t compare against
the “obsolescent” RRE, TRLE, and ZRLE, just raw, the “obsolescent” but
widely supported hextile, and zlib, I’m guessing those are more recent
additions.  Zlib is missing from the RFC, though it’s assigned
encoding number 6; CoRRE is 4.  Actually the RFC also documents that
the Tight encoding is encoding number 7 and “tight options” are -1 to
-222, -224 to -238, and -240 to -256.

[Replit has designed an audio extension to the VNC protocol][10], and
apparently [so has QEMU][11].  Replit’s uses a so-called
“pseudo-encoding”, where the client advertises support for a protocol
extension by offering a weird pixel format that doesn’t really exist,
which is the main official extension mechanism for VNC.  In this case
the pseudo-encoding is 0x52706C41, ‘RplA’.

[10]: https://github.com/replit/rfbproxy?tab=readme-ov-file#replit-audio-rfb-extension
[11]: https://github.com/rfbproto/rfbproto/blob/master/rfbproto.rst#qemu-audio-client-message
[17]: https://help.realvnc.com/hc/en-us/articles/360002320638-How-does-VNC-technology-work

There’s a cursor pseudo-encoding (-239) in the RFC which allows the
client to draw the mouse cursor locally.  The server sends data in
this pseudo-encoding to update the cursor shape.  This might be a
useful thing to add to Yeso, too; it includes as a special case the
ability to hide the cursor (which is close to what VNC does by
default).

There are also eight more client-to-server message types listed in the
RFC.  I’m not sure how the server advertises that it can accept these,
or parses them if they’re sent even though it can’t.  (rfbproto.rst
mentions that the “Tight security type” permits tunneling of data,
authentication using one of the other security types, and an extended
`ServerInit` with server capabilities advertised.)

“xvp” is apparently [“Xenserver VNC Proxy”][19], a proxy server
providing password-protected VNC-based access to the consoles of
virtual machines hosted on XenServer and Xen Cloud Platform.  “A
custom Java-based VNC client, xvpviewer, is supplied with xvp.  This
is based on the TightVNC viewer, but with xvp-specific additions to
allow virtual machine shutdown, reboot and reset to be initiated from
the viewer, and to provide mouse-wheel support.”  (This suggests that
xvp is from the epoch when browsers ran Java.)

rfbproto.rst documents the QEMU extensions, and it also has the CoRRE
and zlib documentation maliciously deleted from the RFC.

One of the new additions in rfbproto.rst is the possibility of having
multiple screens, but still just using a single framebuffer, which I
think is maybe like Xinerama.  The idea is to enable applications to
not open windows split across multiple screens.

I don’t remember the RFC explaining this, but it relied on it
implicitly:

> The client must keep track of the contents of the entire
> framebuffer, not just the areas currently covered by a screen.
> Similarly, the server is free to use encodings that rely on contents
> currently not visible inside any screen.  For example it may issue a
> CopyRect rectangle from any part of the framebuffer that should
> already be known to the client.

But I suppose the client could avoid this by not supporting CopyRect.

Other questions I have
----------------------

- Are there VNC extensions for multitouch?  Maybe “General Input
  Interface (gii)”?
- Is there a way to emulate a scrollwheel in AVNC?  More and more
  things are unusable without a scrollwheel.  As usual VNC implements
  the mousewheel as buttons 4 and 5.
- What about Wayland?
- Is there a reasonable extension for authentication?
- H.264?  Apparently, rfbproto.rst has §7.6.11 “Open H.264 encoding”.
- Rootless display?  Unfortunately the search term “rootless” gets me
  lots of stuff about installing stuff on cellphones without root.
- Copying and pasting images?
- File upload and download?
