In 02003 [Vdovin and Loktev evidently wrote a chapter "Deformable
Mirrors with Thermal Actuators"][vl] about deforming a mirror with
through-hole resistors, and the YouTube channel "Breaking Taps"
replicated it, though with lower displacements of 2-3 microns instead
of 20 microns.  He claims it's not practical outside of hobbyist uses.

[vl]: https://research.tudelft.nl/en/publications/deformable-mirrors-with-thermal-actuators

Vdovin and Loktev's paper reports response times of about 5 seconds
and ΔT of 100 K.  They also report a "thermal tremor" of several
hundred nanometers caused by uneven cooling within the actuator, with
variation over roughly a second; forced-air cooling with a fan would
presumably both reduce the amplitude of this "tremor" and the response
time.

I think it's possible to do better.  You can multiplex the resistors
(easier with series diodes), and you can use, say, 0805 surface-mount
resistors standing up on end, joining two circuit boards 2 mm apart,
one of which is epoxied to a solid block of material (porcelain, say)
and the other of which is epoxied to the back of a mirror.  One board
carries horizontal power lines while the other carries horizontal
ones; the minimal spacing between resistors is probably something like
2 mm.  If you have 48 control lines connected in a conventional matrix
multiplexing arrangement, you can control 24 × 24 = 576 actuator
resistors in this way, covering at least a 48 × 48 mm area.

This design would increase the stiffness of the actuators
significantly.  Vdovin and Loktev report that in their case the
actuators were already too stiff, 30 times stiffer than the mirror, in
the sense that the mirror initially had rather bad aberration because,
presumably, of deformations introduced during the process of soldering
their (370-micron-thick silicon) mirror to the ends of the resistor
leads.  A stiffer mirror would help to solve this and is probably
unavoidable with more everyday materials, because the flexural
rigidity of a plate is cubic in its thickness.  A 2.4-mm-thick sheet
of window glass would be 270 times stiffer than a 370-micron-thick
sheet of glass, though silicon is presumably a bit stiffer.

However, a different way to solve the problem of the initial
aberration of the mirror is to polish it after mounting it on the
actuators, so that any strains built into the actuator contraption are
compensated by the polishing.  Grinding is the normal way to polish a
mirror, but electropolishing may be a better alternative.

Aside from adaptive optics for atmospheric turbulence, which this is
too slow for, this is potentially interesting as a focusable mirror or
a generator of interesting caustics.

I don't know what ceramic chip resistors are made from, but ceramics
like porcelain generally have coefficients of expansion around
8 ppm/K, a little lower than most metals.  With 100 K working
temperature range, that gives you 800 ppm of expansion; on 2 mm
length, the resistors will elongate 1.6 microns.  A 1.6 micron
depression across a 48 mm arc gives you a radius of curvature of
180 m: 

    You have: sqrt((24 mm)**2 + (180 m)**2) - 180 m
    You want: 
            Definition: 1.6e-06 m

The Airy disk radius of λ/d with λ = 555 nm and d = 48 mm is 14
microradians, about 2.4 arcseconds, about 25 times better than the
standard human visual acuity of an arcminute.  (1.22λ/d is for round
apertures.)  But 180 meters seems too far a focal length for
human-scale purposes.

The most common resistor temperature range seems to be -55° to +155°,
and conventionally an 0805 is ⅛W.  To dissipate ⅛W at 12 volts, V²/R
needs to be about 1200 ohms, thus carrying 10 mA, or up to 240 mA
across any 24-pixel row or column, a very reasonable amount.  But
that's an average; the peak current is 5.8 amps, so you might need a
largish power transistor.

Looking at [TE's datasheet for their CRG thick film resistors][0],
which are US$0.008 at Digi-Key, I see that the ceramic substrate is
high purity alumina.  This has the advantage that its strength and
stiffness is pretty high ([azom says][1] 69-665 MPa tensile strength
and 215-413 GPa Young's modulus) and most reports say that its TCE is
around 8 ppm/K as I was calculating above, though [Pertti Auerkari
says in VTT Research Note 1792][2] that it's only 4.9-5.5 ppm/K.

[0]: https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1773204&DocType=DS&DocLang=English
[1]: https://www.azom.com/properties.aspx?ArticleID=52
[2]: https://dl.icdst.org/pdfs/files/4c21bc5a494ee04128ad68d3ef82f5de.pdf "Mechanical and physical properties of engineering alumina ceramics, ISBN 951-38-4987-2, published in 01996"

If we have 5 ppm/K but 120 K of working range (35°-155°) we only have
600 ppm instead of 800 ppm, and thus 1.2 microns on a 2-mm-long
resistor rather than 1.6.

This is consistent with the 2-3 microns reported by "Breaking Taps"
because they were using through-hole resistors (as did Vdovin and
Loktev).

We could maybe stack up several such SMD resistors, soldering them end
to end, but we need about 20 of them to get to a reasonable 9 m focal
length, putting the two circuit boards about 40 mm apart.  This also
means that instead of dissipating up to 72 watts as before (and
probably 36 watts most of the time) we're dissipating 1.4 kilowatts,
which is stupid.

So how did Vdovin and Loktev get such large displacements?  Maybe they
were using resistors built on ceramics with a much larger
coefficients.  Or maybe the leads of their through-hole resistors,
perhaps with copper's linear TCE of 16-17 ppm/K, played a bigger part
in the expansion than the resistors themselves.  Most brasses are
around 20 ppm/K, and [Electronics.StackExchange reports that many
through-hole leads are actually some ferromagnetic alloy][4], though
not usually resistor leads, or [maybe resistor leads too][5], and [the
EEVBlog forums confirm][6].  The TCE of steel is not as high as
copper's, about 10.6, twice that of alumina, but the main benefit is
just that it's much longer, close to the 40 mm I was talking about
above.

[4]: https://electronics.stackexchange.com/questions/16559/why-some-through-hole-component-leads-are-ferromagnetic
[5]: https://electronics.stackexchange.com/questions/116171/what-kind-of-metal-are-the-legs-of-resistors-made-from
[6]: https://www.eevblog.com/forum/reviews/resisitors-with-steel-leads/

If you wanted a lead material with a higher TCE still, zinc at 34.2
seems like the best practical choice.  Tin at 23 is very expensive,
and plutonium at 35.7 is even more so and also has other practical
disadvantages.

It occurs to me that if your main objective is making your resistor
long, with rapid response, maybe you'd be best with nichrome wire.
It's pretty cheap, you can get it up to 1100° without any problem, and
you can make it really long.
