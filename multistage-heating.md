Nichrome or kanthal wire used for heating things like kilns has a
limited lifetime because it's being used at the limits of its solid
temperature range.  As I understand it, it tends to corrode over time
and develop thin spots which become hot spots.

But in heating from room temperature to, say, 1400°, the majority of
the heating happens at much more moderate temperatures.  So possibly a
way to ameliorate this problem, if air can be tolerated, is to inject
most of the heat using inexpensive, long-lived heating elements at
quite moderate temperatures, and finally use shorter-lived elements at
higher temperatures.  For example, you blow room-temperature air into
some regular nichrome coils, which heat it up to 600° and last
forever; the 600° air proceeds into more nichrome coils which are
replaced regularly and heat it up to 1100°; and if you need 1400° you
can use a carbon arc, inductively-heated sacrificial iron pipe, or
burning charcoal to get there.

