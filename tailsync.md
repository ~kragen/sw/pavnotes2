tailsync: maintain a replica of a remote append-only file
=========================================================

Examples:

    tailsync sofia@athena.example.org:/var/log/error.log ./athena-error-log
    tailsync --repl

Synopsis:

    tailsync [--rsh=RSH] [-Fv] [USER@]HOST:SRC DEST
    tailsync [-v] [URL] DEST
    tailsync --repl

The `tailsync` program uses `ssh` or HTTP to efficiently maintain a
local replica of an intermittently-connected remote file that is
created and modified by other processes, as long as those processes
modify it in only one of two ways:

1. Appending data to it.
2. Replacing it with a fresh file.

In the first case, tailsync appends the same data to your local file.
In the second case, it renames your local file to a new name, appended
with a version number of the form `.~53~`, with a unique value for
`53`, and then replicates the fresh file.  The result of all of this
is that you can locally run a command like `tail -F` on the local file
with more or less the same effect as running `tail -F` on the remote
file.

You could do something similar with

    rsync -b --inplace --append-verify --backup \
        sofia@athena.example.org:/var/log/error.log ./athena-error-log

I’m not sure that tailsync offers enough advantages over rsync to
justify writing it, but my ideas are:

1. tailsync never deletes data.  If it discovers that data has changed
   in the part of the file it had previously downloaded, it interprets
   that to mean that the file has been replaced with a new file, just
   as if the file had gotten shorter, so it renames the old file.
   This can fill up your disk in a hurry.
2. tailsync operates continuously when invoked with the `-F` option,
   either polling the file periodically or using inotify.  It
   automatically reconnects if the ssh connection is lost, for example
   due to network reconfiguration or a laptop sleep.  (Hopefully ssh
   doesn’t prompt you for a password.)
3. tailsync can handle files only accessible over HTTP, though it
   cannot verify already-replicated data in them.
