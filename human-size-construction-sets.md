What would it take to make a construction set that worked like legos
but could build you a house?  Could you make it cheaper than
conventional construction methods?

There do exist several human-scale construction sets already.

Structural framing systems
--------------------------

The most popular are X-shaped T-slot square aluminum extrusions which are
connected together with T-slot nuts and/or by drilling and/or tapping
them for bolts, sometimes called “structural framing systems”;
Fischertechnik and Constri are popular small-scale children’s
construction sets that are somewhat similar.  They’ve found especially
wide use in frames for 3-D printers and, oddly enough, trade-show
booths.  In [a recent YouTube video][0] a young couple with skills in
not just mechanical engineering but in being adorable, being funny,
and recommending that you buy terrible products like the Glowforge,
use them to build a 45-kilogram laptop computer.

[One T-slot extrusion manufacturer has a reasonable-looking
introductory page][1] at and Hackaday has published introductory
guides in [02021][2], [02016][3] (which covers the history; these are
about 30 years old now), and and [02018][4].  [V-Slot][5] is a popular
open-source brand.  and [80/20][6], [Bosch Rexroth][7], and
[MakerBeam][8] are three other popular brands.  Keith Neufeld [has
described his approach to 3-D printing lap-joint connectors][9] for
the struts with FreeCAD.

[0]: https://youtu.be/uUVfHaW6nWs
[1]: https://vention.io/resources/guides/t-slot-aluminum-extrusion-structure-design-guide-77
[2]: https://hackaday.com/2021/05/25/getting-started-with-aluminum-extrusions/
[3]: https://hackaday.com/2016/12/14/a-how-to-in-homebrew-design-fab-and-assembly-with-extruded-profiles/
[5]: https://openbuildspartstore.com/v-slot-20x40-linear-rail/
[6]: https://8020.net/
[7]: https://www.boschrexroth.com/en/us/products/product-groups/assembly-technology/topics/aluminum-profiles-solutions-components/
[8]: https://www.makerbeam.com/brands/makerbeam-10mmx10mm/
[9]: http://www.neufeld.newton.ks.us/electronics/?p=1783

Because the T-slots are continuous and translationally symmetric, you
have infinite variability in the possible distance between connections,
even without cutting (which can be done pretty easily, since it’s just
aluminum); this continuous degree of freedom makes them more versatile,
but also means that the measurements have to be applied by the person
doing the assembly, not the manufacturer.  The digital quantization
of space enforced by Legos, Erector sets, Construx, Capsela, Ramagon,
Lincoln Logs, etc., greatly simplifies assembly; you never have to take
an analogue measurement when you’re assembling Legos, but you can easily
build things whose dimensions are repeatable to within 100 microns.
With structural framing systems, you have to have a measuring tool,
and measure.

Heckballs, my public-domain laser-cuttable human-scale construction set
-----------------------------------------------------------------------

My own MDF-laser-cuttable draft construction set design from 02016 is
called Heckballs; you can see [some photos of an early version][10]
and notes on improved versions and baby-testing in [Cut 4][11], [Cut
5][12], and [Cut 7][13].  You can download the PDF files from there (I
think the Cut 7 version of the beams and the Cut 5 versions of the
vertices is the best one so far) and have a set cut at any local sign
shop that has a low-power laser cutter capable of cutting MDF, a type
of business you can find in any city or town, except in the poorest
countries.  Cutting the Cut 7 Heckballs kit (16 vertices and 13 beams)
cost me US$27 at the time, but I think the local sign shop here in
Morón would charge me about US$5 for the same service.

At the scale I designed Heckballs, you can easily use them to build
person-sized structures, because the amount of material saved by
cutting it smaller would not have reduced the cost significantly due
to the cost of laser-cutting.  It isn’t suitable for a tinyhouse; the
structures are quite weak and fragile, and MDF generally swells badly if
a drop of water contacts it, and falls apart entirely if it gets wet.
It should be possible to laser-cut the same designs out of Masonite or
transparent acrylic, but I haven’t tried it, because there were so many
obvious improvements that could be tested with MDF.  Certain possible
improvements would make them suitable for laser-cutting or plasma-cutting
out of mild sheet steel or mild steel plate, which have similar elastic
limits to MDF but much higher rigidity.

Heckballs is in the public domain.

[10]: http://canonical.org/~kragen/sw/laserboot/cut-3/README.md.html
[11]: http://canonical.org/~kragen/sw/laserboot/cut-4/README.md.html
[12]: http://canonical.org/~kragen/sw/laserboot/cut-5/README.md.html
[13]: http://canonical.org/~kragen/sw/laserboot/cut-7/README.md.html

Human-sized brick construction sets
-----------------------------------

I suspect that if you wanted to make a human-sized construction set
that was brick-styled rather than strut-and-joint, that doesn’t cost
an unreasonable amount, and that quantizes space Lego-style, you could
probably blow-mold one-liter bricks out of the PET preforms used for
3-liter coke bottles, with two round tapered studs per brick on top,
and matching holes on the bottom.  (Lego’s 8 studs offer more
flexibility but might be too demanding of precision for blow molding.)
These could be very cheap; Alibaba offers [27-gram PCO1881
preforms][14] from Anhui Creative Packaging Technology at 5¢ each in
quantity 10000.  Those not designed for hot filling are even cheaper.

Some corrugations in the vertical walls could help to increase the
otherwise lamentable buckling resistance of the vertical walls of
the right rectangular parallelepiped, and by mating with neighboring
bricks, they would tend to block cracks in the wall and resist lateral
displacements of individual bricks.  A vaguely Legoish form factor of
80mm×79mm×158mm would be about the same size as a standard fired
clay brick, and a standard 2.4-meter wall would be 30 bricks high,
which seems like a reasonable amount to assemble.  1.1 meters of such
a wall would have 7 bricks in each course, so building a closet-sized
room you could stand up in would require about 840 bricks blow-molded
from preforms costing about US$45.  Making the bricks taller and thinner
could probably reduce this by a third or more.

You could assemble the resulting plastic bricks empty, but if you
wanted to make them solid and weighty, you could fill them with salt
water or sand.  According to my notes in file `3d-printing-stone.html`,
you’d need 2.4kg of sand for each one-liter brick, costing about 3.1¢,
about the same cost as the bottle.  You might be able to grout the sand
with a small amount of binder to stabilize it further, slaked lime being
the cheapest choice by mass and almost certainly adequate at a high enough
loading.  The usual mix is about 2 parts lime to 3 parts sand, which would
add about 5¢ of cost per brick, but the mechanical stabilization provided
by the plastic bottle, especially against surface abrasion, might allow
using much lower binder loadings.  Aerated concrete made with surfactants
could be considerably lighter, which might be able to reduce the cost
per brick, and would certainly reduce the risk that the wall would kill
you when it fell on you.  So would making the bricks taller and thinner.

I don’t see a way to make vertical holes through blow-molded bricks like
those you see in extruded clay bricks; if you found a way to do that,
you could run wooden dowels or steel rebar vertically through the wall
to reinforce it.

[14]: https://www.alibaba.com/product-detail/PCO1881-28Mm-27g-Hot-Filling-Crystallized_1600269890681.html

Considering the alternatives
----------------------------

I don’t know, I feel like struts, joints, and panels (in the style of
Ramagon or Construx or V-Slot or maybe Heckballs) would make a better
construction set for human-scale constructions.  At the point where
a closet-sized room costs US$100 of bricks, requires manually placing
800+ individual parts, and is prone to killing people when you knock it
over because it weighs 2048 kg, I think it’s time to consider whether
maybe Brunel was onto something when he recommended building things
out of metal struts instead of stone blocks.

You can build the same closet-sized room out of 19 meters of aluminum
extrusions weighing 6 kg and costing US$150, plus 24 corner T-nut
brackets (US$40), 64 screws (US$1), and 22 square meters of paneling
(US$100 to cover both the inside and outside) weighing something like
25 kg.  32 kg instead of 2048, 64 times lighter, and about one fourth
of the number of parts to assemble.  As a bonus, you can build a roof
with it.  It costs three times as much, though.

Those prices are mostly from random websites offering V-Slot products.
Rexroth is probably more widely used but also more expensive.  [The
prices in Argentina are only slightly higher, close to US$10/meter for
20×20 V-Slot beams][15], because they’re domestically made by Aluar,
local production being one of the benefits of open-source designs.

[15]: https://articulo.mercadolibre.com.ar/MLA-837649403-perfil-aluminio-2020-1-mt-tipo-bosch-impresora-3d-cnc-v-slot-_JM

Seems like there ought to be a space for a Heckballs-like thing in
that space; remember that my 13 struts and 16 vertices cost me US$27,
and I estimate that now it would cost me US$6, and it’s already capable
of building a much weaker structure of nearly the same size.

Sheet-cutting economics
-----------------------

Aluminum extrusions are fairly cheap, which is why Jacque Fresco was
so enchanted with them and why extrusions are so widely used even when
aluminum’s light weight is not needed, this T-slot stuff being just
one example among many.  Steel is cheaper per kilogram than aluminum,
and the gap only widens further if you consider the cost per unit of
mass or, I think, per unit of strength.  Aluminum’s three killer
advantages are that it’s lightweight, making it a common choice for
airplanes; it’s more corrosion-resistant; and it’s easily extruded and
otherwise shaped.

I have the hypothesis, supported by very little, that the cost of
aluminum extrusions is roughly proportional to their volume.  In
theory, at least, the cost of cutting parts out of a sheet of metal or
MDF should instead be proportional to the surface area of the cut,
which is to say, the thickness of the sheet multiplied by the part’s
perimeter.  But the strength, stiffness, or toughness it provides to a
finished assemblage should be proportional to its area.  So, if sheet
cutting is more expensive, you can just make the parts bigger until it
isn’t.

So far, though, I haven’t attempted to validate any of hypotheses: the
two cost hypotheses or the strength hypotheses.  Nor do I have a good
handle on what the costs of CNC cutting of sheet metal are; even if
all my hypotheses are correct, is the scale where it gets cheaper than
aluminum extrusions 20mm-wide beams, 200-mm-wide beams, or
2-meter-wide beams?

According to file `heck-balls`, steel costs about twice what MDF does
per unit volume, but has 50× MDF’s Young’s modulus and 16× MDF’s
tensile strength, so to keep a beam from buckling, you ought to be
able to spend 25× less on materials with steel than with MDF.  But you
still have to cut it to shape.  This might be about the same cost, or
it might be more expensive, but I doubt that it will be much cheaper.

By cutting out some shapes from a thin steel sheet and snapping them
together, perhaps with heavy leather gloves, you should be able to
make a structural steel tube of any precisely-designed shape.

Interlocking compressed earth blocks or bricks
----------------------------------------------

There are a few different designs for these, typically with studs on
the tops of heavy bricks, hollows in the bottoms, and
tongue-and-groove interlocks in the ends, with an aspect ratio of
precisely 2:1 between the horizontal dimensions.  The [Blocco
Mattone][16] is one; a machine possibly called [“Eco Brava Burning
Free Interlock”][17] from “Shandong Weida Construction Machinery
Co. Ltd.”  or possibly “Brick Making Machine Model 2 W” from “Shandong
Zhenting Machine” is another; a third, which has the distinction of
using only tongue and groove in both dimensions (permitting free block
displacement) is [by Makiga Engineering Services of Kenya][18].  A
fourth is from [Fraink Fonseca’s Verde Equipamentos][20] in Minas
Gerais.  A fifth is the [Henry Block Machine][21] from Henry
Intelligent Block Machine Manufacturers.

[16]: https://www.youtube.com/watch?v=PKB869OW57I
[21]: https://www.youtube.com/watch?v=mrdCJ589jn8
[20]: https://www.youtube.com/watch?v=T4x1r_6MsaI
[17]: https://www.youtube.com/watch?v=j04vjQ7kUsY
[18]: https://www.youtube.com/watch?v=-c_0kE6Nbsk

The Zhenting and Verde machines do better than the first two mentioned
at ensuring that all the corners are beveled or rounded so they won’t
chip, but even the Zhenting and Verde bricks do have some sharp
corners.  They do have continuous vertical air spaces within the wall
like extruded bricks, for weight reduction or for rebar reinforcement.

The Henry machine has bigger chamfers, turns out two bricks every
15–20 seconds or so (with two workers unloading it), and has a
gigantic plus-sign-shaped interlocking shape on top, extending to the
edges, rather than a stud.  Or, from a different perspective, each
half-brick has a nearly-cubical piece missing from each of its upper
four corners, and a corresponding piece on each of its lower four
corners, not counting draft angle.  It has generous chamfers on all
the corners except the horizontal ones on the ends of the brick.  It
seems to be a hydraulic design with a quasi-floating mold; the Blocco
Mattone machine has a floating mold, but I think Henry just has
separately powered hydraulic rams above and below.  They make machines
that produce many kinds of compressed earth bricks, not just
interlocking ones.

The two-stud designs permit right-angle turns, but it occurs to me
that making all the bricks slightly curved (an option in Makiga’s
machine) may be more architecturally flexible.  Typical block length
is around 300mm, and a reasonable turn radius for a wall is around
600mm, which suggests that a block might occupy around half a radian,
which would be about 28.6°.  If we round that to 30° (.524 radians)
then three blocks makes a right-angle turn, but we can also make 30°
and 60° turns.  To make a straight wall out of such curved bricks, you
would alternate 30° curves in each direction.  Does that work?  With a
running bond?

A weakness in this approach is that compressed earth bricks must be
made from soil free of organic material, I suppose because it will
expand and crack the brick; this means they will have very low tensile
strength, so every maker recommends reinforcing them with expensive
portland cement.

Well, non-stabilized compressed earth blocks have a long
history. [Henry has a 10,000 psi machine][22] which they claim avoids
the need for cement.

[22]: https://www.youtube.com/watch?v=-4lfJV2DkWQ
