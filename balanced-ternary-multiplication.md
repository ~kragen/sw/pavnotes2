Balanced ternary is cool.  The place values are powers of three but
the digits are -1, 0, and +1, is almost as simple as in binary, but
the numbers are shorter.  You count 0, +, +-, +0, ++, +--, +-0, +-+,
+0-, and so on.

Multiplication is almost as simple as in binary.  If you have ++0-
(35) multiplied by +00- (26), you can do this by adding:

    ++0-000  (from the + at the beginning of +00-)
       --0+  (from the - at the end)

giving the correct result ++-+-0+ (910).

Converting numbers to ternary from decimal is kind of a pain.  981686,
say:

- divide by three and get 327228 with a remainder of 2,
- divide by three and get 109076 with a remainder of 0,
- divide by three and get 36358 remainder 2,
- divide by three and get 12119 remainder 1,
- divide by three and get 4039 remainder 2;
- 1346, remainder 1;
- 448, remainder 2;
- 149, remainder 1;
- 49, remainder 2;
- 16, remainder 1;
- 5, remainder 1;
- 1 remainder 2;
- 0 remainder 1.

So in regular ternary it's 1211212121202.  You can convert this to
balanced ternary (all the 2s become -s and generate a carry to the
left, turning - into 0, 0 into +, and + into - generating a further
carry) or you can round the original division differently, getting for
example 327229 with a remainder of -1.

I thought dividing by three might be easier in base 60.  In base 60
981686 is 4.32.41.26.  One third of this is 1.30.53.48: a third of 4
is 3 with remainder 1, and that remainder 1 propagates down into the
next digit as 20, adding to the 10 that is a third of 32.  The
remainder of 2 from 32 propagates down into the next place as 40,
which gets added to the 13 that is a third of 41, and again we have a
remainder of 2, which propagates into the last place as 40, getting
added to the 8 that is a third of 26.  And your remainder is 2.

And thus we get:

- x 4.32.41.26
- 2 1.30.53.48
- 0 0.30.17.56
- 2 0.10.5.58
- 1 0.3.21.59
- 2 0.1.7.19
- 1 0.0.22.26
- 2 0.0.7.28
- 1 0.0.2.29
- 2 0.0.0.49
- 1 0.0.0.16
- 1 0.0.0.5
- 2 0.0.0.1
- 1

This is a fairly local procedure: each digit of the result depends on
the digit above it and the digit to its left, like division by 2 in
base 10.  With division by 2 in base 10, the rule is "half" the digit
above plus 5 if the digit to the left is odd.  With division by 3 in
base 60, the rule is "a third" of the digit above plus 20 if the digit
to the left is 1 mod 3 or 40 if the digit to the left is 2 mod 3.

To get balanced ternary, you have to modify the procedure for the last
digit, incrementing it if the digit above it (that it's "a third" of)
was 2 mod 3.

I was thinking that maybe this would be significantly easier than
converting to ternary in base 10, because it avoids long division, but
I'm not sure it is.  To divide 981686 by 3 in base 10 with long
division, the parts of the dividend you look at are 900000 leaving 0,
80000 leaving 20000, 21000 leaving 0, 6 leaving 0, 8 leaving 2, and 26
leaving 2.  It's not obvious to me that this is harder than the
sexagesimal equivalent.

A potential improvement for the base-10 procedure is to divide by 9
instead of 3.  So 900000 gives 100000 leaving 0, 80000 gives 0 leaving
80000, 81000 gives 9000 leaving 0, 600 gives 0 leaving 600, 680 gives
70 leaving 50, and 56 gives 6 leaving 4, so your least significant
nonary digit is 4, having only subtracted a multiple of 9 four times
instead of the six required to get a single base-3 digit, and the next
dividend is 109076 as before.
