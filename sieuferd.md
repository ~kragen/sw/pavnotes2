Eirik Bakke’s [SIEUFERD][0] (or, as he sometimes spells it, SIUEFERD)
is a really interesting and promising generic database frontend, a
“schema-independent database UI”.  The elevator pitch is “a new kind
of graphical query interface [with] the power of SQL but the
ease-of-use of a spreadsheet”.  (Unfortunately, he forgot to implement
the DDL part of SQL, just doing the DML part.)

Overview of SIUEFERD
--------------------

The basic idea is that you are always looking at a query result in an
Excel-like GUI, but with nested tables, and by selecting context-menu
options (or toolbar buttons, etc.) you modify the query, with
operations at your disposal powerful enough to compose any SQL-92 DML
query.  Moreover, you can edit the database through the query results,
again as in Excel.  And, for large numbers of fields, you can switch
to a “forms” interface (like a CRUD data entry screen or SQLite’s
`.mode line` or PostgreSQL’s `\x on` or MySQL’s `\G`), using
heuristics to get a reasonable layout (perhaps with multiple columns),
but without losing the ability to modify the query.

[0]: https://people.csail.mit.edu/ebakke/sieuferd/

He has a [five-minute talk on Vimeo with a 3.3-minute demo][1], [a
ten-minute demo at HYTRADBOI from 02022][7] ([archive link][8]), and a
series of papers on it: [02010 on comparing database usability][20]
([dspace link][23])in which SIEUFERD is called “Sieuferd”, [02011 on
schema-independent database UIs][2], [02011 on spreadsheet-based UIs
for one-to-many relationships][3] in which it was called Related
Worksheets (a 10-page condensed version of [his 58-page master’s
thesis][19]), [02013 on automatic layout][4] ([dspace link][9]),
[02016 on “expressive query construction” through direct
manipulation][5] (his most cited paper; [dspace link][10], [12-minute
talk video][49]), and [his doctoral thesis from 02016][6]).  The
earliest version of this work seems to be [a 14-minute talk from
02009][50] when it was his Master’s Thesis work in progress entitled
NQ Prototype 2.0, just doing automatic layout of query results from
SQL extended with nested tables (*i.e.*, relation-valued attributes),
“for XML or to present to the user”, which is already pretty magical.

[1]: https://vimeo.com/173726371
[2]: https://people.csail.mit.edu/ebakke/research/databaseui_cidr2011.pdf
[3]: https://people.csail.mit.edu/ebakke/research/related_worksheets_chi2011.pdf
[4]: https://people.csail.mit.edu/ebakke/research/reportlayout_infovis2013.pdf
[9]: https://dspace.mit.edu/bitstream/handle/1721.1/87067/Karger_Automatic%20layout.pdf?sequence=1&isAllowed=y
[5]: https://people.csail.mit.edu/ebakke/research/sieuferd_sigmod2016.pdf
[6]: https://dspace.mit.edu/bitstream/handle/1721.1/107280/972900808-MIT.pdf?sequence=1
[7]: https://www.hytradboi.com/2022/ultorg-a-user-interface-for-relational-databases
[8]: https://archive.org/details/hytradboi2022/HYTRADBOI+Ultorg+-+Eirik+Bakke.mp4
[10]: https://dspace.mit.edu/bitstream/handle/1721.1/107280/972900808-MIT.pdf?sequence=1
[19]: https://dspace.mit.edu/bitstream/handle/1721.1/63075/725897442-MIT.pdf?sequence=2
[20]: https://repositori.udl.cat/server/api/core/bitstreams/3acd8674-9902-4596-8072-8ce201e80cde/content
[23]: https://dspace.mit.edu/bitstream/handle/1721.1/115065/Karger_besdui.pdf?sequence=1&isAllowed=y
[49]: https://vimeo.com/170964637
[50]: https://vimeo.com/8125488

### Fatal flaws ###

Although it is very inspiring, it has three fatal flaws, any one of
which makes it unusable from my point of view:

1. It’s proprietary.
2. It’s a desktop app, written in Java with Swing, rather than a web
   app.  (In a demo in 02020, Bakke admitted that it lost all the
   views you had defined when you exited the app, but presumably
   that’s been fixed by now.)
3. It doesn’t support DDL, just DML.  So you can’t create a database
   in it, though you can modify the data in one.  I’m not sure you can
   insert rows, either, making it little more than a report-generation
   program, like Crystal Reports or RPG III, but with a fantastic UI
   instead of a fantastically shitty UI.

Unfortunately, Bakke is a perfectionist, so there isn’t much chance
he’ll ever fix it.  The fact that he’s released anything at all after
12 years of work is a minor miracle.

### Context-menu operations on fields in the view ###

These include Excel-like sort and filter.  Sorting options include
ascending, descending, secondary, and clearing the sort; filtering
brings up a list of the unique values with checkboxes and a search
text field.  Other context-menu options include join, insert
calculated field (later called “formula”), hide field, bringing up a
field selector with a checkbox per field (including in joinable
tables), hide parent if empty (see below), collapse duplicate rows
(SQL `distinct`), and “delete” (presumably of the field).

### Joins ###

SIEUFERD looks for foreign-key constraints in the database schema to
suggest joins, so you can add rows from a separate table to your view
(“perspective”).  By default, if your join follows a one-to-many
relationship, this gives you a hierarchically nested view; each row in
the “master” table has a table of rows from the “detail” table nested
inside of it, and if you add computed fields to the master-table part
of the view, they can compute aggregate functions like COUNT, COUNTD
(count distinct), or SUM over fields of the detail table.  If there is
a reference to a detail-table field that *isn’t* inside an aggregation
function, the join reverts from this hierarchical join to a
traditional SQL join, just as if you had traversed a many-to-one
relationship.

By default the joins are left outer joins, but there’s a “hide parent
if empty” option which provides inner joins.  I’m not sure how this
interacts with filtering by criteria on the child; because Bakke keeps
using the same small set of databases and queries for his demos decade
after decade, I suspect the answer is that you typically get a lot of
unwanted rows.

You can also set up a join without a pre-existing foreign-key
relationship, using a dialog box that shows the field names of two
different tables.

### Usability ###

The [usability paper][20] compares (the 02010 version of) Sieuferd to
Rhizomer, Virtuoso Facets, and PepeSearch, on a benchmark the authors
designed.  Naturally Sieuferd comes out on top in capabilities, but
surprisingly PepeSearch required only a third as many user actions and
a third as much time, for the tasks that it could complete.  The
results are in [a GitHub repository][21] ([w3id][22]) with a bit more
data.

[21]: https://github.com/rhizomik/BESDUI
[22]: http://w3id.org/BESDUI

### Is this direct manipulation? ###

Bakke claims SIEUFERD supports query construction through a “direct
manipulation” user interface, which I usually think of as not having
all the fucking popup menus everywhere.  Shneiderman’s original paper
defining “direct manipulation” ([ACM version from 01984][39]) says it
“involves representation of the object of interest, rapid incremental
reversible actions[,] and physical action instead of complex syntax”.

Certainly SIEUFERD represents the query results and uses a sort of
physical action instead of complex syntax, and the query construction
is incremental in the sense that you add and remove one column or join
table or filtering criterion at a time.  But it’s not as *rapid* as
typing a query, and it’s enormously less incremental than dragging a
rubberband line in SKETCHPAD or resizing a window with a rubberband
rectangle, with the feedback loop running around 0.5 Hz instead of
10–100 Hz.

A clue about the world Shneiderman was writing about is in his
overview of peripherals (§2.5, p.242 (7/21)):

> The hundreds of variations in the basic items such as keyboards,
> screens and hard copy printers must be reviewed for
> suitability. Then the numerous additional devices such as light
> pens, touch screens, graphics tablets, joysticks, programmed
> function keys and rotating knobs must be considered.

Note the absence of mice from these lists!

In explaining his idea of “direct manipulation” (§4), he starts by
talking about “enthusiastic users” and listing their characteristics:

> 1. Mastery of the system.
> 2. Competence in performance of their task.
> 3. Ease in learning to use the system originally and acquire 
>    new features.
> 4. Confidence in their capacity to retain mastery over time.
> 5. Enjoyment in using the system.
> 6. Eagerness to show it off to novices.
> 7. Desires to explore more powerful aspects of the system.

He claims that these feelings were produced by features:

> The central ideas seemed to be visibility of the object of interest,
> rapid reversible actions and replacement of complex command langage
> syntax by direct manipulation of the object of interest — hence the
> term ‘direct manipulation’.

His canonical examples (though he says none has all the design
features he admires):

- “full-page display editors” and “display editors such as EMACS on
  the MIT/Honeywell MULTICS system or ‘vi’ (...) on the UNIX system”:
  “Most display editors operate at high speed 120 characters/s (1200
  baud), a full page in a second (9600 baud) or even faster.  This
  high display rate coupled with short response time produces a
  thrilling sense of power and speed.”
- VISICALC.
- a “spatial-data management” system where you can zoom in and out and
  glide around with a joystick, whether viewing GIS data or data arranged
  in some kind of conceptual 2-D space.
- Video games like PONG, Missile Command, “Galaxyian”, Space Invaders,
  etc., “maybe the most exciting, well-engineered (...) application of
  these concepts”: “The commands are physical actions such as button
  presses, joystick motions or knob rotations whose results are shown
  immediately on the screen.  There is no syntax to remember and
  therefore no syntax-error messages.”
- CAD/CAM systems when they’re driven by light pens, into which
  category he shoehorns newspaper page layout and process control
  dashboards in continuous-flow plants.
- Driving an automobile.
- Recording and replaying robot motions, which is where he says the
  term “direct manipulation” is “most accurately applied”.
- Zloof’s 01975 query by example.
- “Advanced office automation systems” like the Xerox Star and IBM’s
  PICTUREWORLD.
  
Based on these examples, he extracts his three central features of
“direct manipulation”:

> 1. Continuous representation of the object of interest.
> 2. Physical actions or labelled button presses instead of complex
>    syntax. [This is oomitted from the Wikipedia summary.]
> 3. Rapid incremental reversible operations whose impact on the
>    object of interest is immediately visible.

I think SIEUFERD does fit pretty well into this description, except
that I don’t know about the reversibility.  I have some misgivings
about to what extent the impact of a query change on a million-record
result can be immediately visible, though; you can only fit a few
thousand records on your screen at a time.

[39]: https://dl.acm.org/doi/10.5555/2092.2093 "The future of interactive systems and the emergence of direct manipulation, by Ben Shneiderman, originally presented as the Keynote Address at the NYU Symposium on User Interfaces, 01982-05-26–28, then published with numerous typographical errors in 01982, Behaviour & Information Technology, 1:3, 237-256, DOI 10.1080/01449298208914450"

Also influential is [Hutchins, Hollan, and Norman 01985][46] (2449
cites in Google Scholar to poor Shneiderman's 788) and [maybe Norman
and Hutchins 01988][47].  The first says in its abstract:

> We identify two underlying phenomena that give rise to the feeling
> of directness.  One deals with the information processing distance
> between the user’s intentions and the facilities provided by the
> machine.  Reduction of this distance makes the interface feel direct
> by reducing the effort required of the user to accomplish goals.
> The second phenomenon concerns the relation between the input and
> output vocabularies of the interface language.  In particular,
> direct manipulation requires that the system provide representations
> of objects that behave as if they are the objects themselves.  This
> provides the feeling of directness of manipulation.

[46]: https://www.ra-anders.de/pdf/id/1229.pdf "Direct Manipulation Interfaces, Human-Computer Interaction, 1985, Volume 1, pp. 311-338"
[47]: https://apps.dtic.mil/sti/tr/pdf/ADA198417.pdf "Computation via Direct Manipulation, Final Report, Contract N00014-85-C.0133, 01984-12-01 to 01988-02-69, DTIC ID AD-A198 417, 33 pp."

Possibly this is misdated because they keep citing Laurel’s “Interface
as Mimesis” from 01986, but maybe that’s a chapter in a book that
hadn’t come out yet, co-edited by Norman.

Unlike Shneiderman, this seems to prohibit the dialog boxes and
right-click menus that are such a prominent feature of SIEUFERD.

Interestingly, though, *their* first example is a visual programming
language for plotting and statistically analyzing data, one which
reifies a dataflow graph on the screen, but permits selecting points
by brushing a region in a plot.  They go on to quote Shneiderman as
the authoritative definition, also quoting the virtues he identifies:

> 1. Novices can learn basic functionality quickly, usually through a
>    demonstration by a more experienced user.
> 2. Experts can work extremely rapidly to carry out a wide range of
>    tasks, even defining new functions and features.
> 3. Knowledgeable intermittent users can retain operational concepts.
> 4. Error messages are rarely needed.
> 5. Users can see immediately if their actions are furthering their
>    goals, and if not, they can simply change the direction of their
>    activity.
> 6. Users have reduced anxiety because the system is comprehensible
>    and because actions are so easily reversible.

They also actually cite a paper of his from *01974*, “[A computer
graphics system for polynomials][48]”, which I unfortunately don’t
have a copy of.

[48]: https://doi.org/10.5951/MT.67.2.0111

Much of the rest of their paper is aimed at identifying not just the
advantages of direct manipulation but also its disadvantages:

> Why do direct manipulation systems feel so natural?  What is so
> compelling about the notion?  On the other hand, why can using such
> systems sometimes seem so tedious?

Also, they trace its history magnificently: SKETCHPAD (which they
spell “*Sketchpad*”), Borning’s ThingLab, Budge’s Pinball Construction
Set, Hollan’s own Steamer, and of course spreadsheets.  Conspicuously
absent from the list are Emacs, video games, and query by example.
They do mention video games and screen editors later, but as examples
whose status as direct-manipulation interfaces is ambiguous.

Their conception of “direct manipulation” is much more compellingly
explained and motivated than Shneiderman’s.

Avi Bryant’s DabbleDB: another inspiration
------------------------------------------

[DabbleDB][11] ([OSCON planner demo][18]), which shut down in
[02011][12] after [being acquired by Twitter in 02010][13] ([Bryant’s
announcement][15]) and [turning off signups immediately][14]
([DabbleDB’s announcement][17]) and [going into export-only mode][16]
later, had some similar functionality, including context-menu
filtering and sorting and group-by on fields, but also permitted you
to build new databases, and in particular to refactor subsets of
fields into new tables connected by joins.  Also, its default search
was a grep-like search across all fields.  I don’t think it could
display a join result in a table, though, much less a nested table.

[11]: https://youtu.be/MCVj5RZOqwY "Avi Bryant demos Dabble db, uploaded by Tavis Rudd 02012-07-04"
[12]: https://news.ycombinator.com/item?id=2339077
[13]: https://blog.twitter.com/official/en_us/a/2010/more-than-dabbling.html
[14]: https://news.ycombinator.com/item?id=1421166
[15]: http://web.archive.org/web/20100614003337/http://www.avibryant.com/2010/06/life-changing.html
[16]: https://news.ycombinator.com/item?id=2339077
[17]: http://web.archive.org/web/20100613222451/http://blog.dabbledb.com/2010/06/140character-dabbling.html
[18]: https://youtu.be/6wZmYMWKLkY

As far as I know SIEUFERD doesn’t support bulk editing (`update foo
set bar = baz where quux` or `delete from foo where quux`), but
DabbleDB had a checkbox column for such things.

### Grouping ###

Another nice thing about DabbleDB was that the aggregate functions
(sum, median, mean, max, etc.) were displayed in a dropdown menu
*together with the values they would have*.  (Unfortunately, he also
went with the hierarchical-menu approach for formula entry, so
multiplication, addition, subtraction, and division were submenus,
with another submenu for what to multiply or divide by.)  Aggregates
were applied both to groups of rows (by whatever the group-by
criterion was; DabbleDB’s report format was also hierarchical, but
just in the section-by-section way that Agenda was) and to the whole
column.  (Clicking on a section header produces a query that filters
to just that section.)

DabbleDB had a “compact view” option that suppressed the individual
rows in each group, providing a non-hierarchical output similar to SQL
group-by.  SIEUFERD’s generalized hierarchical approach seems superior
to me, though I’m not yet entirely clear on how it would work with
group-by operations that aren’t related to a join.

Dabble’s “group by” submenu on a field offers a number of options; for
dates, for example, it offers grouping by year, month, quarter, or
date, and for strings, it offers grouping by exact string or by first
letter.  But there is no live preview on these.

### Joins, (or rather, links,) and schema refactoring ###

DabbleDB also gave you hypertext navigation of views, in the sense
that a field could be configured to link to a record (“entry”) in
another table (“category”), or actually, I suppose, a view of it.  One
way to create a new table was by “configuring a field” to be a “link
to entry in category” and selecting “new category”.  Then later you
could “move” other fields to the “new category”.  (It seems to me that
in cases where you have a multiple-column primary key, or no primary
key, for the new detail table, you probably want to be able to do this
to all the fields at once.)

The edit (“detail” or “form”) view in DabbleDB automatically included
a nested table of any applicable one-to-many tables; the example in
the demo above is that Avi refactored the “name of salesperson” field
in his sales-prospects table to be a link to entries in a new
“category”, and the resulting detail view contained a list of links to
records in the “sale” table, though displaying only the “company”
field in the link, without the address, value, date, etc.  Unlike
SIEUFERD’s form view, the detail view only displayed a single record.

Another schema refactoring (“migration”) DabbleDB offered was
converting a text field to a choice from a fixed set of options, which
it prepopulated from the currently existing values.  In wowbarbts I
got a cheap version of this with browser autocomplete: all the fields
were text, but the browser would suggest values that had previously
been used for the same field.  DabbleDB’s other types were number,
money, date/time (including intervals), Dabble user, “link to entry”
(a foreign key), and “list of entries”, thus departing from 1NF.

DabbleDB’s flow for adding a field in the detail view was “Add field
to category” followed by a popup modal for the field title, which then
gets added to the detail view.  (The field seems to have implicitly
defaulted to type text.)  The field had a null value in other records.

### UI flow for the table view ###

Like Lotus Agenda, DabbleDB offered a calendar view of its data (if
you had a date/time field), and also some GIS and plotting views, as
well as the default table view.

DabbleDB’s flow for unhiding fields in the table view was better than
SIEUFERD’s; it showes a “Summary” column (by default, but hidable)
listing name/value pairs for undisplayed fields, and clicking on one
of them added it to the display.  There was also an “add column”
button on the table view which dropped down a list of candidate fields
to unhide, including a “new field” option; this also seems like an
easier workflow than SIEUFERD’s popup dialog with a list of
checkboxes.

Unlike SIEUFERD (or Excel, or wowbarbts), DabbleDB’s table view wasn’t
implicitly editable, but you could say “edit column” to populate a
column with editable text fields so as to have a more spreadsheet-like
experience.

### The data-entry and QBE form builder ###

DabbleDB had a drag-and-drop form builder for specialized data entry
forms, with checkboxes for “let user create new entries”, “let user
search”, and “let user edit entries in the view”; this seems like
mostly a duplication of the query editing capabilities.  On the other
hand, it included an explanatory-text feature that is sorely needed
for data entry.

### Import ###

While SIEUFERD accesses existing live Excel data (I don’t know how,
maybe by reading the files or maybe by invoking Excel; it can import
CSV files too), DabbleDB does an Excel import from the tab-separated
text you get from copy and paste.  The field-definition page in the
import flow shows the first row as candidate field names, and has a
checkbox to use it as data instead.

Research citing SIEUFERD
------------------------

DabbleDB unfortunately didn’t provoke much academic interest, but
there are a lot of interesting-looking papers citing SIEUFERD:

- [Evaluating interactive data systems][24], Rahman, Jiang, and Nandi,
  at OSU and Twitter, 02019, VLDB, 28 pages.  Mentions Tableau,
  Microsoft Power BI, and a system called DBTouch that sounds
  interesting.  Largely focused on (multi)touch interaction and
  interactive performance, though, which are not so interesting except
  on very large databases.  Mostly it’s a review of literature on how
  to *evaluate* these systems, rather than of the systems themselves,
  and it also introduces a metric called “latency constraint
  violation”.
- [Your notebook is not crumby enough, REPLace it][25], Brachman et
  al., supposedly “Woodstock ’97”; this is erroneously labeled because
  it cites lots of papers up to 02018.  Their open-source system,
  “Vizier”, is intended to fix the “lack of reproducibility,
  versioning, and support for sharing” of Jupyter and similar systems
  by “help[ing] analysts to build and refine data pipelines (...)
  combin[ing] the flexibility of notebooks with the easy-to-use
  data-manipulation facilities of spreadsheets” and tracking
  provenance so you know where your dataset came from and can
  reproduce it.  It mentions zeppelin.apache.org as another similar
  notebook.  Vizier seems to be functional rather than relational in
  nature, though it has table-valued variables; it supports things
  like adding and deleting rows and columns, but not joins.  But
  dataflow cells work by creating SQL views of existing datasets.
  They seem to have cited SIEUFERD by mistake; it appears in their
  bibliography endnotes but with no reference to the endnote in the
  text of the paper.
- [NOAH][26]: Interactive Spreadsheet Exploration with Dynamic
  Hierarchical Overviews, Rahman et al., CC-BY-ND, VLDB, 02021.  They
  wrote a [big proprietary codebase in Java][40].  They’re focused on
  actual spreadsheets, motivated by million-row datasets, and
  basically it seems like they’re building a better UI for pivot
  tables, with brushing or something.  Also, though, it’s a plugin for
  [DataSpread][42], whose tagline is “combines the intuitiveness and
  flexibility of spreadsheets and the scalability and power of
  databases”.  They cite the SIEUFERD work in the introduction: “Other
  work tries to make spreadsheets more scalable [5, 68], responsive
  [6, 69], and expressive [2, 3, 7, 74], but none of these papers
  address the usability challenges underlying spreadsheet data
  exploration that occur on even modest-sized datasets.”  The SIEUFERD
  work was references 2 and 3.  7 is a self-citation of Bendre et
  al. on “redesigning spreadsheets for scale” (DataSpread again), and
  74 is Tyszkiewicz 02010 below.
- Jerzy Tyszkiewicz, Spreadsheet as a relational database engine. In
  SIGMOD. ACM, 195–206, is referenced from the NOAH paper.  It’s just
  about how Excel’s MATCH and INDEX functions let you compile
  relational joins into slow spreadsheet formulas.  Lame.
- DataSpread has a [YouTube demo][43].
- [Towards a Holistic Integration of Spreadsheets with Databases: A
  Scalable Storage Engine for Presentational Data Management][27],
  Bendre et al., 02017 UIUC tech report, arXiv preprint
- [Visual query languages to design complex queries: a systematic
  literature review][28], Silva, Fidalgo, Ferro, and Franco, Software
  and Systems Modeling volume 22, pages 1217–1249 (02023).  This seems
  like maybe what I’m *most* interested in, but I don’t have a copy.
  Their 56 references seem appealing.
- [A Tutorial on Visual Representations of Relational Queries][29],
  Gatterbauer, NEU, 02023, apparently a tutorial given at VLDB.  The
  online document is a sparse four pages.
- [Object Spreadsheets: A New Computational Model for End-User
  Development of Data-Centric Web Applications][30], McCutchen,
  Itzhaky, and Jackson, Onward! 02016.
- [The Lish: A Data Model for Grid Free Spreadsheets][31], a 198-page
  OU thesis by Alan Geoffrey Hall, CC BY-BC-ND, 02019.  Describes a
  spreadsheet based on nested lists of cells.  §2.3.6 is “relational
  tables in spreadsheets” and mentions SIEUFERD, as well as some other
  related systems: pivot tables and “power pivot”, Liu and Jagadish’s
  02009 direct-manipulation query building “SheetMusiq” prototype,
  Cervesato’s 02007 NEXCEL which makes relations first-class objects
  in the worksheet.
- [Sigma Worksheet: Interactive Construction of OLAP Queries][32], by
  Gale et al. at Sigma Computing, 15 pp.
- [Interactive Browsing and Navigation in Relational Databases][33],
  by Kahng et al., CC BY-NC-ND, VLDB 02016, 12 pp.
- [Wildcard: Spreadsheet-Driven Customization of Web
  Applications][34], by Litt and Jackson at MIT, ACM Programming
  02020, 10 pp.  They added an AJAX scraper to Firefox using an
  interception API they haven't reproduced in Chrome, though they
  think it’s possible, which is interesting.  This is about getting a
  spreadsheet view for data in existing web pages to sort, filter, and
  update it, and in particular to do join queries across multiple
  tables inferred from DOM or AJAX data, though limited to a sort of
  star schema.
- [End-user Software Customization by Direct Manipulation of Tabular
  Data][35], by Litt, Jackson, Millis, and Quaye, CC By, SIGPLAN
  Onward ’20.
- [Litt’s dissertation, “Building Personal Software with Reactive
  Databases”, from August 02023][38].  It turns out Jackson was his
  advisor and Karger was on his committee; he also thanks Jonathan
  Edwards, Shriram Krishnamurthi, and Arvind Satyanarayanan for
  “wisdom and mentorship”, and Millis and Quaye were “wonderful UROPs”
  he “had the opportunity to work with”.  “In Wildcard [paper linked
  above], we show how users can use a spreadsheet-like reactive table
  interface to extend and modify the behavior of an existing web
  application, like reorganizing the prioritization of articles on a
  news site. In Potluck [which has a PEG-like aspect for extracting
  data], we show how reactive formulas can be layered over text notes
  to enrich them with interactive behavior, like scaling ingredients
  in a recipe. Finally, in [Riffle][41] [which sounds a lot like
  Meteor: “all view components coordinate through a unified reactive
  relational database”] we demonstrate how the simple reactivity model
  from spreadsheets can scale to help developers build complex
  high-end user experiences, like a music library manager.  Throughout
  all these tools, the patterns of fast reactive updates, live views
  of data, and direct manipulation interactions enable simpler
  approaches to specifying computational behavior than traditional
  programming tools can offer. These projects contribute towards a
  future where the creation and tailoring of software is not limited
  to a programmer elite, but is democratized for all.”  He wants to
  liberate data from its applications to empower users, situating his
  solution as a “reactive database” like Airtable.  He says Exhibit
  (Huynh, Karger, and Miller, 02007) is another SIEUFERD-like system,
  and also overviews Quilt (Benson et al. 02014), Gneiss (Chang and
  Myers 02014), Object Spreadsheets (McCutchen, Itzhaky, and Jackson
  02016, mentioned above), and Mavo (Verou, Zhang, and Karger 02016).
  Incidentally he also cites a conflicting definition of “direct
  manipulation”: “Hutchins, Hollan and Norman [32] define a direct
  manipulation interface as one that uses a model-world metaphor
  rather than a conversation metaphor,” an article from 01985 just
  entitled "Direct Maanipulation Interfaces".
- [Riffle: Reactive Relational State for Local-First
  Applications][41], Litt et al. 02023, licensed CC BY, looks super
  cool but is more about using a relational database as a reactive
  data store for a general application, not really about making a
  generic browser for any database.
- [Data Exploration through Dot-Driven Development][36], by Petricek,
  CC BY, ECOOP 02017.
- [OVI-3: A NoSQL visual query system supporting efficient
  anti-joins][37], by El-Mahgary et al., CC BY, 25 pp.  They says it’s
  a NoSQL visual query system (VQS) based on incremental querying.  By
  “anti-joins” they mean negation, I suppose of the sort `where
  student.id not in (select student_id from graduates)`.  It talks
  about Datalog and column databases and such advanced things, but the
  information on the UI is a bit sparse; it looks clunky from the
  screenshots they did include.

[24]: https://www.researchgate.net/profile/Lilong-Jiang/publication/337226579_Evaluating_interactive_data_systems_Survey_and_case_studies/links/5de1e07692851c8364549246/Evaluating-interactive-data-systems-Survey-and-case-studies.pdf
[25]: https://par.nsf.gov/servlets/purl/10129817
[26]: https://par.nsf.gov/servlets/purl/10216116
[27]: https://arxiv.org/pdf/1708.06712.pdf
[28]: https://link.springer.com/article/10.1007/s10270-022-01071-4
[29]: https://arxiv.org/pdf/2308.10319.pdf
[30]: https://dl.acm.org/doi/pdf/10.1145/2986012.2986018
[31]: https://oro.open.ac.uk/68292/1/Alan%20Hall%20thesis%202019-11-11%20to%20Library.pdf
[32]: https://arxiv.org/pdf/2012.00697.pdf
[33]: https://arxiv.org/pdf/1603.02371.pdf
[34]: https://dl.acm.org/doi/pdf/10.1145/3397537.3397541
[35]: https://dl.acm.org/doi/pdf/10.1145/3426428.3426914
[36]: https://drops.dagstuhl.de/storage/00lipics/lipics-vol074-ecoop2017/LIPIcs.ECOOP.2017.21/LIPIcs.ECOOP.2017.21.pdf
[37]: https://link.springer.com/article/10.1007/s10844-022-00742-4
[38]: https://dspace.mit.edu/bitstream/handle/1721.1/152650/litt-glitt-phd-eecs-2023-thesis.pdf?sequence=1&isAllowed=y
[40]: https://github.com/dataspread/NOAH
[41]: https://dspace.mit.edu/bitstream/handle/1721.1/152980/3586183.3606801.pdf?sequence=1&isAllowed=y
[42]: http://dataspread.github.io/
[43]: https://www.youtube.com/watch?v=MAK36CB14YI

Airtable
--------

It’s a startup with [US$1.35B in funding][45], with the latest round
being for half that total ($735M, Series F, Dec 13, 2021, on a US$11B
valuation) which seems to imply that the investors think it has either
a near certainty of making half a billion dollars a year in profits
soon, or a reasonable chance of making several times that.  Having
watched some Airtable demos on YouTube but not used it, it looks a lot
like a more complete version of DabbleDB.  I wonder if Avi Bryant
regrets the Twitter acquisition!

[45]: https://tracxn.com/d/companies/airtable/__Xdq7WaiA79BBRynm6WLMDo_kp-jvnqvpW1GxVVOirjE

Litt’s description of Airtable from his doctoral dissertation sounds
appealing, and very much indeed like DabbleDB:

> While it is difficult to define an exact boundary for what
> constitutes a reactive database system, a useful illustrative
> example is Airtable, a popular commercial end user programming
> platform used for tasks like project management, CRM, and marketing
> planning. It allows users to visually define relational schemas,
> with column types ranging from general ones like text, numbers, and
> dates to more specific ones like emails and phone numbers. Users can
> edit the data and the schema in a web browser using a generic table
> editor UI.
>
> Like a spreadsheet, the table view in Airtable puts data in the
> foreground and provides direct manipulation interactions to edit the
> data and schema as well as specify computations. A major difference
> is that the underlying data model is not a freeform 2D grid, but is
> instead a relational schema with strict column types and explicitly
> modeled relationships between entities. Using this table view, users
> can specify *views* (i.e., queries) over tables—including sorting,
> filtering, grouping, and selecting subsets of columns—all using
> direct manipulation interactions. They can also add *computed
> columns*, which contain formulas that compute derived values. Unlike
> a spreadsheet, the formulas are automatically applied once per
> record in the table; there is no need to manually copy the formula
> to every row. Like a spreadsheet, computed values update reactively
> when underlying data changes. This table view is a sort of hybrid
> between a spreadsheet and a database. The enforced relational
> structure prohibits many common uses of spreadsheets, e.g. for
> constructing arbitrary data layouts in a 2D grid, while the
> structure simultaneously makes it easier to enforce data integrity
> and perform rich queries without making mistakes. Figure 1-3 shows
> some of the key features of the Airtable table view.
>
> In addition to the table editor view, Airtable comes with several
> built-in user interfaces for editing and visualizing the data in the
> relational database. One particularly important kind of view is
> *forms* for collecting user input that adds rows to the
> database. Other views include calendars, maps, galleries of cards,
> kanban boards, timelines, and Gantt charts, which are all fairly
> generic views which can apply to many domains. All of these views
> update automatically and reactively when the underlying data
> changes. In addition, users have tools to visually define their own
> custom item view layouts and simple interactive UIs on top of the
> database.
>
> Airtable shows some of the key characteristics of the genre of
> reactive databases: a structured data model, a table view that
> supports viewing, editing and querying the data; a scalar formula
> language for computing derived columns on the table; other rich
> views over the tabular data; and reactive updates throughout the
> entire tool. Similar tools have appeared in other popular commercial
> products like Coda, Notion, and Glide, making this set of features
> almost a kind of common standard for modern end-user programming
> interfaces.

The UI flow is better than DabbleDB’s in some ways.

Other ideas
-----------

It should be possible to make in-memory column databases fast enough
to execute any ordinary database query on keystroke-by-keystroke
timescales.  See file `ram-database-speed.md` for notes on this.

- sparklines with groups
- cushion-treemap shading for table rows
- color gradients
- ERDs
- field name generation
- brushing and crossfiltering
- treemaps
- more direct joins, using a particular row as an example
- pivot table stuff with joins
- PEG parse tree display in nested tables
