I cooked three eggs today with a solar hot water heater.  This
quasi-hotel thing we’re staying at has a couple of them on the roof,
and today they were boiling.  So I put three eggs in a bowl and ran
them under the hot water faucet for five minutes, and when I took them
out, they were cooked, though with runny yolks.

(The shower here scares me, though Mina did successfully shower
without injury.)

It’s currently 1:37 AM and 15.5° outside, but the temperature gauges
on the hot water heaters say they’re at 83° and 85°; in the late
afternoon, around 16:30, when they were boiling steam out of their
little chimneys, they said 93° and 97°.  Their capacity is 200ℓ, so
70° of ΔT represents about 59MJ of stored energy; this loss of 10°
represents heat leakage through the insulation on the order of 8MJ,
which is 300–350W over 7 hours, and about 5W/° of thermal
conductance (0.2°/W of thermal resistance).

With the tank’s heat capacity of 840kJ/°, this gives a time constant
of about 2 days, so after four or more successive days with heavy
clouds, the ΔT would drop to under 10°, which would be too cold for
comfortable showers on hot days.  Storing the energy in TCES, with
muriate of lime or something, would solve that problem, but that’s
another topic.

What I thought was super cool was being able to turn on the tap and
run solar energy over my eggs until they were cooked.  I didn’t have
to go out in the sun with the mosquitoes to cook with solar energy,
which is especially nice since we’re in the middle of a dengue
outbreak.  I didn’t have to cook the eggs before sundown (though now
that it’s several hours later, they might take longer, but 74° is
enough).  I didn’t have to reorient a solar oven’s reflectors because
the sun had moved.  I didn’t need a big lead-acid battery bank,
although a 200-liter stainless steel tank can’t be that cheap, even
without the vacuum-insulated collector tubes.  I don't know the brand,
but a [Peabody PE-T200RESIS][0] is similar, and that costs US$620,
US$10/MJ of storage capacity (100kJ/US$).

An interesting thing about that Peabody system is that its collectors
are all within a 1.78-meter square, about 3.17m² and thus ideally 3.2
kilowatts peak.  That's 19¢ per watt, about double the current
wholesale price of low-cost photovoltaics.

[0]: https://articulo.mercadolibre.com.ar/MLA-780444742-peabody-termotanque-solar-200-l-kit-electrico-acero-inox-_JM

I feel like you could maybe do a better job of this than just cheaping
out by omitting the thermostatic mixing valve from a domestic
hot-water system, thus inadvertently enabling its users to cook eggs
or sous-vide steak. For one, you can’t boil a teakettle with this
system, much less run a deep-fat fryer; it’s limited to just under
100°.  Using a different coolant than unpressurized water would help
with that.  Another problem is that the system lifetime is probably
pretty short because water is so reactive.  The owner is going to have
to replace the hot-water valve rubber pretty often, and maybe even the
hot-water pipes.

Most alternative coolants (brine, propylene glycol, transformer oil,
etc.) are things that you wouldn't want to just let boil off or dump
down the drain after one use; you’d have to recirculate them in a
closed circuit.

Air is one exception, and its main disadvantage is that its low
density and low specific heat require 4000× higher volumetric flow
rates for a given heat flow rate, resulting in large ducts despite its
much lower viscosity.  For example, transferring 2400 watts with water
at a ΔT of 70° requires 8.2 milliliters per second, half a liter a
minute.  With air you need 29000 ml/s, 60 cfm.

Pressurized steam is another, and if you allow the steam to condense
at 200° to transfer the heat, well, at about [35kJ/mol][1] you get
about 1.9MJ/kg.  This is only 1.3 grams per second, and even at that
pressure I think the steam is much lower viscosity than liquid water.
[It requires 1.55MPa][2], a pretty safe pressure, though a bit higher
than truck tires.  The reactivity issues are worse but over a smaller
area.

[1]: https://en.wikipedia.org/wiki/Enthalpy_of_vaporization#/media/File:Heat_of_Vaporization_(Benzene+Acetone+Methanol+Water).png
[2]: https://pages.mtu.edu/~tbco/cm3230/steamtables.pdf

The ideal here is that, when the sun shines, thermal energy is
collected in some kind of thermal store up to whatever the safe limit
is, and then whenever you want to cook or whatever, you turn a knob or
press a button to apply many kilowatts of power to the problem.  Given
the surprising lack of cost advantages nowadays for such solar thermal
collectors, it might even make economic sense nowadays to do the solar
collection with photovoltaic panels, only converting excess production
to heat for cheaper storage.

A tilted heat pipe without capillaries might be a useful sort of
“thermal transistor” for such a system.  It transmits heat as steam
(or some other vapor, e.g., ethanol) as long as its hot end is lower
than its cold end so that drops of condensation can flow down.  The
usual kinds of heat pipes, with capillaries, could serve to carry heat
downwards.
