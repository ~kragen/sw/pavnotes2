I never really understood the [Chinese Remainder Theorem][0].

[0]: https://en.wikipedia.org/wiki/Chinese_remainder_theorem

Consider the [Pulverizer Algorithm][1].  In its simplest form:

> You are given two natural numbers.  If they are unequal, reduce the
> larger one by the amount of the smaller one, then start again.

[1]: https://en.wikipedia.org/wiki/Ku%E1%B9%AD%E1%B9%ADaka

In “Writing ARM and Thumb Assembly Language”, author unknown (the ARM
assembly-language manual, ARM DUI 0068B), §2.5.4, this is used to
motivate conditional instructions (here translated to use gas unified
syntax):

    gcd:    cmp    r0, r1
            subgt  r0, r1
            sublt  r1, r0
            bne    gcd

So, for example, starting from (10, 7) we move successively to (10 - 7
= 3, 7), (3, 7 - 3 = 4), (3, 4 - 3 = 1), (3 - 1 = 2, 1), and (2 - 1 =
1, 1), and then must stop because they have become equal.  This gives
us the greatest common divisor of 10 and 7, which is 1.  If we start
at (10, 6), instead we move to (4, 6), (4, 2), and (2, 2).

It’s clear to me that the number at the end must be some multiple of
the GCD, because all the numbers in every step are, because the
subtraction does not change that.  It’s not obvious to me [how to show
that it is][2], in fact, itself the GCD and not some other multiple of
it, but for the nonce I can just assume that.

[2]: https://en.wikipedia.org/wiki/Euclidean_algorithm#Proof_of_validity

The crucial leap for the CRT is that each of the numbers in this
sequence is a sum of multiples of the original two numbers *a* and
*b*.  In the example above:

- 1·10 + 0·7 = 10, 0·10 + 1·7 = 7
- 1·10 - 1·7 = 3, 0·10 + 1·7 = 7
- 1·10 - 1·7 = 3, -1·10 + 2·7 = 4
- 1·10 - 1·7 = 3, -2·10 + 3·7 = 1

At this point we can stop early, because what we wanted was to get to
1, and we’re already there.  (We also could have skipped the stop in
the middle at 4 by just directly subtracting 2·3 from 7.)  The
information we wanted was that -2·10 + 3·7 = 1.  For the purpose of
the CRT, this means:

- -2·10 % 7 = 1.
- 3·7 % 10 = 1.

Given this information, if we want to construct a number *n* such that
*n* % 7 = *x* and *n* % 10 = *y*, we can get there by adding *x*
copies of -2·10 to set the value mod 7 without affecting the value mod
10, and *y* copies of 3·7 to set the value mod 10 without affecting
the value mod 7.  So if (*x*, *y*) = (3, 4), we have 3·-2·10 + 4·3·7 =
-60 + 84 = 24, which does indeed magically have the desired
remainders.

This also means that we’ve computed that the multiplicative inverse of
10 in mod-7 arithetic is -2 (or 5, since -2 % 7 = 5) and the
multiplicative inverse of 7 in mod-10 is 3.

Freud’s coke-freak confidante Wilhelm Fliess was obsessed with the
numbers 23 and 28, in Martin Gardner’s account.  (I haven’t read
Fliess’s own writings.)  In particular, Fliess was fascinated with the
observation that any integer could be thus expressed as an integer
linear combination of 23 and 28, not knowing that this was a property
of any pair of relatively prime integers.  By applying the Pulverizer
we obtain successively 5 = 28 - 23, 3 = 5·23 - 4·28, 2 = 5·28 - 6·23,
1 = 11·23 - 9·28.  These coefficients, 11 and -9, are the keys to
Fliess’s twisted little heart.

So if we want to express some number in Fliess’s biorhythm form, say
53, we start by finding its residues mod these bases: 53 % 23 = 7, and
53 % 28 = 25.  Then to get numbers that are 7 mod 23, we take -7·9·28
= -63·28 = -1764, and to get numbers that are 25 mod 28, we take
25·11·23 = 275·23 = 6325.  6325 - 1764 = 4561, which isn’t 53 but is
close; it’s only off by 4508, which is just 7·23·28.  The easiest
solution is to subtract 7·28 = 196 from 275, giving us 79·23 - 63·28 =
53; you could also subtract 7·23 = 161 from -63, giving us the less
appealing but still correct 275·23 - 224·28 = 53.

A slightly simpler way to do this is to just compute 53·(11·23 -
9·28), since 11·23 - 9·28 = 1.  This distributes out to (53·11)·23 -
(53·9)·28 = 583·23 - 477·28 = 13409 - 13356 = 53.  But of course most
of the 583 and most of the 477 are unnecessary: 583 = 28·20 + 23 and
477 = 23·20 + 17, so this can be reduced to 28·20·23 + 23·23 -
28·20·23 - 17·28, or more simply 23·23 - 17·28 = 53.

