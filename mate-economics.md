One mate of yerba weighs about 37 grams and makes about half a liter
of yerba mate, enough to thoroughly caffeinate two or three people.
Yerba is sold in bags of 250g, 500g, 1kg, 5kg, and 10kg; 1kg is
currently (02024-07-21) $3500 to $7700, with about $4000 seeming to be
the most common price, for brands like Amanda, with Nobleza Gaucha
being cheaper, and the imported Canarias being the $7700 one.  The
dollar is at AR$1430 today (mid-market blue rate).  So a mate of yerba
is about 37 × $4 = $148 ≈ 10¢.  This works out to $75 (5¢) per person
or $300 (20¢) per liter.

By contrast, a 500mℓ Coca-Cola costs $1200 and rots your teeth.

[Ale Salvino has recorded a short YouTube video explaining in English
how to prepare it][0].

[0]: https://www.youtube.com/watch?v=fNipKlsyFHU
