I was interested to read in [a paper from 02017 about molten-salt
coolants for concentrated solar powwer][0] that you can depress the
melting point to only 383° in a mix of NaCl/KCl/MgCl₂ with molar
fractions 27.5%/32.5%/40.0% and thus mass fractions 20.5%/30.9%/48.6%,
even though the component salts alone don't melt until 801°, 770°, and
714°.

[0]: https://www.sciencedirect.com/science/article/pii/S0038092X17301792 "Survey and prediction of thermophysical properties of binary and ternary eutectic salts from NaCl, KCl, MgCl2, CaCl2, ZnCl2 for heat transfer and thermal storage fluids in CSP, by Li, Xu, Wang, Haob, and Xiao, doi 10.1016/j.solener.2017.03.019"

This is a low enough temperature not just for a significant range of
coolant uses but also convenient use as an ionic liquid solvent and
perhaps electrolyte.  They claim "low corrosivity to nickel-based
alloys", though perhaps that's assuming you mix something into the
salt to eat any HCl that forms., like metallic magnesium.

It'd be nice to know if you could do a similar trick with a mix of
salts of something like aluminum, magnesium, and copper, so that the
electrolytically produced metal is stable in air.  Hydrothermal
aluminum chloride is not useful for this; you cannot dehydrate it, but
maybe sulfates, perchlorates, nitrates, or acetates would be
friendlier.  Some of these are not high-temperature tolerant and would
require a so-called "deep eutectic system".
