Suppose you have a brass coin weighing 5 grams, and you turn it into
machinery.  How complex can this machinery be?

Suppose it’s about 8.7 g/cc.  Then this coin is 0.6 cc: 600 cubic
millimeters.

With ECM we can reasonably make beams that are 10 microns wide and 100
microns long; humans can see these with the naked eye and are too
large to be a serious inhalation hazard for them.  They’re about as
big as a grain of sand.  Such beams would be about 0.00001 cubic
millimeters, or maybe a third of that if they’re made as I-beams or
C-channels with 1-micron precision.  So you could make about 60
million of them out of the coin, or three times that in the other
case.

Computing machinery
-------------------

A CPU only needs about 10000 active parts, so this is 6000 mechanical
CPUs’ worth; as static RAM, it would be around ten megabits,
1.25 megabytes.  And if we naïvely scale operating frequency inversely
to scale, well, ten-centimeter-scale angle grinders routinely spin at
10k RPM (170 Hz), so 100-micron-scale beams might routinely handle
170 kHz.  (Transistors on the same scale routinely ran at 2+
gigahertz; as I understand it, that’s because electrons are fifty
thousand times lighter than silicon atoms and a hundred thousand times
lighter than copper atoms, so they move 300 times as fast under the
influence of the same energy.)

So, for experimentation, cost of materials is not really a problem
until you start getting into really exotic materials like isotopically
pure things.

If this motion were dissipative and irreversible, we might be talking
about moving 20 microns in a microsecond, with a peak velocity of
40 m/s, so one of these 90-nanogram beams might acquire and dissipate
70 nJ per cycle.  60 million of them at 170 kHz would thus require
70 kW, 14 kW per gram.  This is probably four or five orders of
magnitude too large to be practical, even if the parts are distributed
over a fairly large volume and immersed in coolant.  Even a 10000-beam
CPU built in this way would require 120 W.  So to make it practical we
would benefit from either much lower speed, reversible computation, or
both.  For example, if we ran at 64 kHz and were 90% reversible, the
CPU would require only 0.6 W, which is maybe still challenging, but
manageable.

Dropping the mass with I-beam-type structures as described above would
probably tend to improve speed, because it would reduce the mass by
more than the stiffness, and perhaps reduce energy consumption.

Lattices
--------

Suppose you made these beams into a wire mesh: 30 million horizontal
and 30 million vertical.  This works out to 3 km × 3 km, one coin
spread over an entire city.  So if you’re trying to scoop up materials
from, say, seawater, you probably are going to be able to filter a
very large amount of seawater.  But the mesh is probably not going to
be very strong, even if 10 microns is below the critical dimension for
flaw-insensitivity.

By contrast, if you make it into a 10-micron-thick foil, like the
cheaper kinds of household aluminum foil, it only covers 600 square
centimeters.  XXX this suggests that my mesh figure above is totally
wrong because the mesh should be 20% of the foil!

If you instead make a three-dimensional lattice, you will have about
as much beam material in between layers as in each layer.  It would be
half as much if it’s a cubical lattice, but roughly as much for more
practical lattices, and the layer spacing is also only about 82
microns.  Also if you make the layers omnitriangulated, horizontal
lines on the same layer are 87 microns apart and have twice as many
beams between them, so in 1 cm × 1 cm you have 114 rows of 100 beams
for about 114 · 3 · 100 = 34000 beams in all, and a cubic centimeter
with 68000 beams per 82-micron thickness would have 122 layers and
thus 8.3 million beams, weighing 0.72 g/cc, just barely light enough
to float in water.

This kind of lattice could be useful for metamaterials with greater
stiffness and lower weight than the base metal, but it can’t increase
the hardness or tensile-strength-to-weight ratio over that of the base
metal, unless the flaw-insensitivity thing comes into play and you get
the orders of magnitude improvement that comes with that.

You could probably arrange for the surface to be superhydrophobic,
with only points of beams in contact with water; superhydrophobic
microfluidic channels might be advantageous for reduced flow
resistance (because most of the surface of the water in the channel
could be moving rather than stationary) and reduced reactivity with
the metal.

