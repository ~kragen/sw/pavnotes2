Getting RISC-V cross compilation set up
---------------------------------------

Debian/Ubuntu is pretty impressive with respect to supporting cross
compilation for Linux these days, though there are still rough spots.
Tab completion of this:

    $ sudo apt install libc6-
    Display all 150 possibilities? (y or n)

suggests that it can cross-compile for amd64, x32, powerpc, mipsr6el,
i386, m68k, ppc64, mips32, mips64r6, mips64r6el, arm64, hppa, sh4,
s390x, sparc64, and a bunch of weird combinations that I can't make
heads or tails of.  And there are GCC cross-compilation packages for a
bunch more architectures that don't have a libc6 cross-compilation
package, such as XTensa, MSP430, m68hc1x, h8300, and of course AVR,
not to mention MinGW.  Binutils has support for DJGPP and the Z80, and
cc65 can cross-compile for the 6502, while sdcc supports the Z80,
Z180, 8051, HC08, PIC, DS80S390 (?!), STM8, and I think more recently
Padauk.  Depending on how you count, that's compilers for 25
architectures.

But what I'm most interested in right now is RISC-V.  So I installed
`libc6-dev-riscv64-cross` and `libc6-riscv64-cross` so I could
cross-compile for RISC-V.  I tried installing
`gcc-riscv64-unknown-elf` but that turned out to be the wrong thing;
it's maybe right for microcontrollers but not for Linux executables,
and it craps out complaining that it can't find include files or
libraries:

    $ riscv64-unknown-elf-gcc hello.c
    hello.c:1:10: fatal error: stdio.h: No such file or directory
        1 | #include <stdio.h>
          |          ^~~~~~~~~
    compilation terminated.
    $ riscv64-unknown-elf-gcc -I /usr/riscv64-linux-gnu/include hello.c
    /usr/lib/riscv64-unknown-elf/bin/ld: cannot find crt0.o: No such file or directory
    /usr/lib/riscv64-unknown-elf/bin/ld: cannot find -lc
    /usr/lib/riscv64-unknown-elf/bin/ld: cannot find -lgloss
    collect2: error: ld returned 1 exit status
    $ riscv64-unknown-elf-gcc -I /usr/riscv64-linux-gnu/include hello.c -L /usr/riscv64-linux-gnu/lib
    /usr/lib/riscv64-unknown-elf/bin/ld: cannot find crt0.o: No such file or directory
    collect2: error: ld returned 1 exit status

Instead, I installed `gcc-10-riscv64-linux-gnu` and was able to
compile a hello-world program, but without chrooting it, it's hard to
run unless you link statically (with `-static`):

    $ riscv64-linux-gnu-gcc-10 hello.c 
    $ file a.out
    a.out: ELF 64-bit LSB shared object, UCB RISC-V, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux-riscv64-lp64d.so.1, BuildID[sha1]=5d2dae037cdf8bfdec6b881e294a56ef5dd36e1a, for GNU/Linux 4.15.0, not stripped
    $ ./a.out
    /lib/ld-linux-riscv64-lp64d.so.1: No such file or directory

Here's the unoptimized disassembly of the hello-world main().  RISC-V
assembly is sort of long-winded, unfortunately, but pretty tight
thanks to the C extension.

    0000000000000624 <main>:
     624:   1101                    addi    sp,sp,-32
     626:   ec06                    sd      ra,24(sp)        # save return address
     628:   e822                    sd      s0,16(sp)        # and caller's frame pointer;
     62a:   1000                    addi    s0,sp,32         # set up a frame pointer in s0
     62c:   87aa                    mv      a5,a0
     62e:   feb43023                sd      a1,-32(s0)
     632:   fef42623                sw      a5,-20(s0)
     636:   00000517                auipc   a0,0x0           # set up PC-relative pointer to string
     63a:   07250513                addi    a0,a0,114 # 6a8 <__libc_csu_fini+0x2>
     63e:   f13ff0ef                jal     ra,550 <puts@plt>
     642:   4781                    li      a5,0             # compute return value 0
     644:   853e                    mv      a0,a5
     646:   60e2                    ld      ra,24(sp)
     648:   6442                    ld      s0,16(sp)
     64a:   6105                    addi    sp,sp,32
     64c:   8082                    ret

With -Os I get more reasonable code; this is 24 bytes.

    0000000000000560 <main>:
     560:   1141                    addi    sp,sp,-16          # allocate minimal stack space
     562:   00000517                auipc   a0,0x0             # load PC-relative string pointer
     566:   13650513                addi    a0,a0,310 # 698 <__libc_csu_fini+0x4>
     56a:   e406                    sd      ra,8(sp)           # save return address
     56c:   fe5ff0ef                jal     ra,550 <puts@plt>  # invoke puts() with a0 argument
     570:   60a2                    ld      ra,8(sp)           # restore return address
     572:   4501                    li      a0,0               # set up return value 0
     574:   0141                    addi    sp,sp,16           # deallocate stack frame
     576:   8082                    ret                        # return

It's failing to run because the RISC-V dynamic linker isn't installed
where it expects.  We can just invoke the dynamic linker manually but
then it fails because the libs aren't where it expects:

    $ /usr/riscv64-linux-gnu/lib/ld-linux-riscv64-lp64d.so.1 ./a.out
    ./a.out: error while loading shared libraries: libc.so.6: cannot open shared object file: No such file or directory

With `LD_LIBRARY_PATH` we can finally get it to run by brute force:

    $ LD_LIBRARY_PATH=/usr/riscv64-linux-gnu/lib /usr/riscv64-linux-gnu/lib/ld-linux-riscv64-lp64d.so.1 ./a.out
    hello, world

It turns out, though, that it's possible to get this to work
transparently.

Adding a `binfmt_misc` wrapper binary to run RISC-V binaries with cross libs
------------------------------------------------------------------------------

It's able to run at all because QEMU emulates the RISC-V instruction
set transparently on Linux:

    $ dpkg -l qemu-user-binfmt qemu-user
    Desired=Unknown/Install/Remove/Purge/Hold
    | Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
    |/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
    ||/ Name             Version           Architecture Description
    +++-================-=================-============-================================================
    ii  qemu-user        1:4.2-3ubuntu6.21 amd64        QEMU user mode emulation binaries
    ii  qemu-user-binfmt 1:4.2-3ubuntu6.21 amd64        QEMU user mode binfmt registration for qemu-user

qemu-user supports an astounding list of architectures, including
several that have never been manufactured in hardware form: aarch64,
aarch64\_be, alpha, arm, armeb, cris, hppa, i386, m68k, microblaze,
microblazeel, mips, mips64, mips64el, mipsel, mipsn32, mipsn32el,
nios2, or1k, ppc, ppc64, ppc64abi32, ppc64le, riscv32, riscv64, s390x,
sh4, sh4eb, sparc, sparc32plus, sparc64, tilegx, x86\_64, xtensa, and
xtensaeb.

Ideally I could set up qemu-user to set up `LD_LIBRARY_PATH`
automatically.  Actually I can if I invoke qemu-user manually, [as
`vivek_v` explained on Stack Overflow][2]:

    $ qemu-riscv64 -L /usr/riscv64-linux-gnu a.out
    hello, world

[2]: https://stackoverflow.com/questions/16158994/how-to-solve-error-while-loading-shared-libraries-when-trying-to-run-an-arm-bi

`Mita_` additionally explained that you can set this up in an
environment variable called `QEMU_LD_PREFIX`.  But I don't want my
executables to fail to run if some environment variable is missing!

The [`binfmt_misc`
docs](https://docs.kernel.org/admin-guide/binfmt-misc.html) explain
kind of how the implicit invocation works, saying:

> If you want to pass special arguments to your interpreter, you can
> write a wrapper script for it.

The kernel exports readable information about the supported misc bin
formats:

    $ cat /proc/sys/fs/binfmt_misc/qemu-riscv64
    enabled
    interpreter /usr/bin/qemu-riscv64
    flags: OC
    offset 0
    magic 7f454c460201010000000000000000000200f300
    mask ffffffffffffff00fffffffffffffffffeffffff

This apparently comes from /usr/share/binfmts and is managed by
update-binfmts:

    $ cat /usr/share/binfmts/qemu-riscv64
    package qemu-user-binfmt
    interpreter /usr/bin/qemu-riscv64
    magic \x7f\x45\x4c\x46\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xf3\x00
    offset 0
    mask \xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff
    credentials yes
    fix_binary no

So I've written a wrapper script:

    $ cat /usr/local/bin/run-riscv64 
    #!/bin/sh
    exec /usr/bin/qemu-riscv64 -L /usr/riscv64-linux-gnu "$@"
    $ /usr/local/bin/run-riscv64 a.out 
    hello, world

And now I've edited /usr/share/binfmts/qemu-riscv64 to invoke it, and
reloaded it using update-binfmts:

    $ cat /proc/sys/fs/binfmt_misc/qemu-riscv64
    enabled
    interpreter /usr/bin/qemu-riscv64
    flags: OC
    offset 0
    magic 7f454c460201010000000000000000000200f300
    mask ffffffffffffff00fffffffffffffffffeffffff
    $ sudo update-binfmts --unimport qemu-riscv64
    $ cat /proc/sys/fs/binfmt_misc/qemu-riscv64
    cat: /proc/sys/fs/binfmt_misc/qemu-riscv64: No such file or directory
    $ sudo update-binfmts --import qemu-riscv64
    $ cat /proc/sys/fs/binfmt_misc/qemu-riscv64
    enabled
    interpreter /usr/local/bin/run-riscv64
    flags: OC
    offset 0
    magic 7f454c460201010000000000000000000200f300
    mask ffffffffffffff00fffffffffffffffffeffffff

However, it still doesn't work, even though the wrapper script works
if invoked by hand:

    $ ./a.out
    bash: ./a.out: cannot execute binary file: Exec format error
    $ run-riscv64 a.out
    hello, world

I suspect that, contrary to what the kernel documentation says, I need
to supply a native binary, though the `javawrapper` example cited in
the docs was certainly a shell script.  Supplying a native binary does
seem to work; I wrote this 9-line C program (which merely runs
run-riscv64), compiled it (for amd64!), and installed it as
/usr/local/bin/launch-riscv64:

    #include <unistd.h>
    #include <stdio.h>

    int main(int argc, char **argv)
    {
      argv[0] = "/usr/local/bin/run-riscv64";
      execv(argv[0], argv);
      perror(argv[0]);
      return -1;
    }

Then I tweaked /usr/share/binfmts/qemu-riscv64 to point to it:

    $ cat /usr/share/binfmts/qemu-riscv64
    package qemu-user-binfmt
    interpreter /usr/local/bin/launch-riscv64
    magic \x7f\x45\x4c\x46\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xf3\x00
    offset 0
    mask \xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff
    credentials yes
    fix_binary no

And after using update-binfmts as above, I can now run my dynamically
linked RISC-V executable with no further ceremony:

    $ riscv64-linux-gnu-gcc-10 hello.c
    $ file a.out
    a.out: ELF 64-bit LSB shared object, UCB RISC-V, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux-riscv64-lp64d.so.1, BuildID[sha1]=5d2dae037cdf8bfdec6b881e294a56ef5dd36e1a, for GNU/Linux 4.15.0, not stripped
    $ ./a.out
    hello, world

This is great!

Actually now I think I see an easier way I could have gotten this to
work: `qemu-riscv64 --help` says, among other things, "Defaults:
`QEMU_LD_PREFIX` = /etc/qemu-binfmt/riscv64".  I bet I could have just
made a symlink from /etc/qemu-binfmt/riscv64 to
/usr/riscv64-linux-gnu!  And that would even work if I run
qemu-riscv64 manually, for example to use its `-strace` option.  But I
haven't tried it yet.

I thought maybe I could add RV64 as a foreign arch but only in Debian unstable
------------------------------------------------------------------------------

[Michael Tokarev][3] seems to suggest that the right solution is to
`dpkg --add-architecture` and install `libc6:riscv64` rather than
`libc6-riscv64-cross` (attempting to translate his ARM suggestions).
Aurelien Jarno helpfully explains:

> libc6-armhf-cross is only for gcc usage, not to execute binaries.

[3]: https://www.mail-archive.com/debian-bugs-dist@lists.debian.org/msg1833259.html

So far I only have:

    $ dpkg --print-foreign-architectures
    i386

But this doesn't work because Ubuntu doesn't have RV64 support or even
ARM support, only i386 and amd64:

    $ sudo dpkg --add-architecture riscv64
    $ sudo apt install libc6:riscv64
    Reading package lists... Done
    Building dependency tree       
    Reading state information... Done
    E: Unable to locate package libc6:riscv64
    $ sudo apt update
    ...
    Reading package lists... Done
    E: Failed to fetch http://us.archive.ubuntu.com/ubuntu/dists/focal/main/binary-riscv64/Packages  404  Not Found [IP: 91.189.91.38 80]
    E: Failed to fetch http://us.archive.ubuntu.com/ubuntu/dists/focal-updates/main/binary-riscv64/Packages  404  Not Found [IP: 91.189.91.38 80]
    E: Failed to fetch http://us.archive.ubuntu.com/ubuntu/dists/focal-backports/main/binary-riscv64/Packages  404  Not Found [IP: 91.189.91.38 80]
    E: Failed to fetch http://security.ubuntu.com/ubuntu/dists/focal-security/main/binary-riscv64/Packages  404  Not Found [IP: 2001:67c:1562::15 80]
    E: Some index files failed to download. They have been ignored, or old ones used instead.

However, this would probably a much better solution than hacking
around with the "cross" libraries, because the `*-riscv64-cross`
packages only cover the basic runtime libraries like libc, libobjc,
and libstdc++, not things like libjpeg-dev or libx11-dev.  So if you
want to compile a Linux RISC-V executable with a GUI, you probably
need to use a distribution that has RISC-V support.  (Ironically, you
can compile a Windows GUI executable with MinGW and run it with WINE
more easily than you can compile a Linux RISC-V GUI executable and run
it on Linux.)

Unfortunately, though Debian stable is better than Ubuntu here, it
still doesn't come through.  [libx11-6 in stable][1] is only available
on amd64, arm64, armel, armhf, i386, mips64el, mipsel, ppc64el, and
s390x.  However, [in unstable][2] many more architectures are
supported, including riscv64 ("unofficial port").

[1]: https://packages.debian.org/stable/libs/libx11-6
[2]: https://packages.debian.org/unstable/libs/libx11-6

Ubuntu GDB doesn't support cross-debugging by default
-----------------------------------------------------

qemu-user allows you to listen on a port for a connection from GDB:

    $ qemu-riscv64 -L /usr/riscv64-linux-gnu -g 2159 a.out

But the standard Ubuntu GDB isn't built with RISC-V support:

    $ gdb -q a.out
    Reading symbols from a.out...
    (No debugging symbols found in a.out)
    (gdb) target remote localhost:2159
    Remote debugging using localhost:2159
    warning: Architecture rejected target-supplied description
    Truncated register 37 in remote 'g' packet
    (gdb)

Interestingly, after this happens, qemu immediately continues
execution until the program exits.

This is unfortunate because I've found stepping through programs
instruction by instruction very helpful for understanding assembly
languages.

Presumably I could either build GDB with support for an RV64 target or
just use the RV64 GDB from RV64 Debian, running GDB itself with
qemu-user.

Disassembling strrchr from the RV64 glibc
-----------------------------------------

We can ask the dynamic loader what libraries it's loading but
unfortunately that tells us which libraries qemu-user is loading:

    $ LD_TRACE_LOADED_OBJECTS=1 LD_LIBRARY_PATH=/usr/riscv64-linux-gnu/lib /usr/riscv64-linux-gnu/lib/ld-linux-riscv64-lp64d.so.1 ./a.out
        linux-vdso.so.1 (0x00007ffd9cd8b000)
        libgmodule-2.0.so.0 => /lib/x86_64-linux-gnu/libgmodule-2.0.so.0 (0x00007f5087ca3000)
        libglib-2.0.so.0 => /lib/x86_64-linux-gnu/libglib-2.0.so.0 (0x00007f5087b7a000)
        libgnutls.so.30 => /lib/x86_64-linux-gnu/libgnutls.so.30 (0x00007f50879a4000)
        librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007f508799a000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f508784b000)
        libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f5087830000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f508780b000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f5087619000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f5087613000)
        libpcre.so.3 => /lib/x86_64-linux-gnu/libpcre.so.3 (0x00007f50875a0000)
        libp11-kit.so.0 => /lib/x86_64-linux-gnu/libp11-kit.so.0 (0x00007f508746a000)
        libidn2.so.0 => /lib/x86_64-linux-gnu/libidn2.so.0 (0x00007f5087449000)
        libunistring.so.2 => /lib/x86_64-linux-gnu/libunistring.so.2 (0x00007f50872c5000)
        libtasn1.so.6 => /lib/x86_64-linux-gnu/libtasn1.so.6 (0x00007f50872af000)
        libnettle.so.7 => /lib/x86_64-linux-gnu/libnettle.so.7 (0x00007f5087275000)
        libhogweed.so.5 => /lib/x86_64-linux-gnu/libhogweed.so.5 (0x00007f508723e000)
        libgmp.so.10 => /lib/x86_64-linux-gnu/libgmp.so.10 (0x00007f50871ba000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f5087cd0000)
        libffi.so.7 => /lib/x86_64-linux-gnu/libffi.so.7 (0x00007f50871ac000)

It turns out qemu-user has an -E option to solve exactly this problem:

    $ LD_LIBRARY_PATH=/usr/riscv64-linux-gnu/lib qemu-riscv64 -E LD_TRACE_LOADED_OBJECTS=1 /usr/riscv64-linux-gnu/lib/ld-linux-riscv64-lp64d.so.1 ./a.out
        libc.so.6 => /usr/riscv64-linux-gnu/lib/libc.so.6 (0x00000040019ae000)
        /lib/ld-linux-riscv64-lp64d.so.1 => /usr/riscv64-linux-gnu/lib/ld-linux-riscv64-lp64d.so.1 (0x0000004000000000)
    $ ls -l /usr/riscv64-linux-gnu/lib/libc.so.6
    lrwxrwxrwx 1 root root 12 Apr  3  2020 /usr/riscv64-linux-gnu/lib/libc.so.6 -> libc-2.31.so

So I can disassemble the C library and see what's going on.

    $ riscv64-linux-gnu-objdump -d /usr/riscv64-linux-gnu/lib/libc-2.31.so
    ...
    0000000000066e3e <strrchr@@GLIBC_2.27>:
       66e3e:       1101                    addi    sp,sp,-32                            The stack pointer is always supposed to be 16-byte aligned.
       66e40:       e822                    sd      s0,16(sp)                            s0 (x8), s1 (x9), s2 (x18), and ra (x1) are call-preserved
       66e42:       e426                    sd      s1,8(sp)                             (see RISC-V Assembly Programmer's handbook).
       66e44:       e04a                    sd      s2,0(sp)                             All the s* registers including sp are call-preserved, and gp (x3)
       66e46:       ec06                    sd      ra,24(sp)                            and tp (x4) also must be passed to callees and interrupts.
       66e48:       0ff5f493                andi    s1,a1,255                            a1 is argument 1, and we only want the low byte.
       66e4c:       842a                    mv      s0,a0                                Overwrites s0 with a0, which I guess is argument 0, the string.
       66e4e:       4901                    li      s2,0                                 li and mv are both pseudo-ops, really addi.
       66e50:       e489                    bnez    s1,66e5a <strrchr@@GLIBC_2.27+0x1c>  So if s1 (the byte we got from a1 originally) is zero
       66e52:       a005                    j       66e72 <strrchr@@GLIBC_2.27+0x34>     we jump to the strlen call?  Otherwise we go to the strchr arg setup below.
       66e54:       00150413                addi    s0,a0,1                              Target of later backward jump, top of loop.
       66e58:       892a                    mv      s2,a0
       66e5a:       8522                    mv      a0,s0                                Jump target of above bnez.  Calling strchr with s0, s1.
       66e5c:       85a6                    mv      a1,s1
       66e5e:       fbeff0ef                jal     ra,6661c <strchr@@GLIBC_2.27>        ra is the return-address register, which is why it's call-preserved.
       66e62:       f96d                    bnez    a0,66e54 <strrchr@@GLIBC_2.27+0x16>  End of loop: if a0 (return value of strchr) is nonzero, repeat.
       66e64:       60e2                    ld      ra,24(sp)                            Restoring call-preserved registers for ret.
       66e66:       6442                    ld      s0,16(sp)
       66e68:       64a2                    ld      s1,8(sp)
       66e6a:       854a                    mv      a0,s2                                Return value goes in a0 I guess.
       66e6c:       6902                    ld      s2,0(sp)
       66e6e:       6105                    addi    sp,sp,32                             Restore stack pointer.
       66e70:       8082                    ret
       66e72:       d29ff0ef                jal     ra,66b9a <strlen@@GLIBC_2.27>
       66e76:       00a40933                add     s2,s0,a0
       66e7a:       60e2                    ld      ra,24(sp)                            Duplicate procedure epilogue.
       66e7c:       6442                    ld      s0,16(sp)
       66e7e:       64a2                    ld      s1,8(sp)
       66e80:       854a                    mv      a0,s2
       66e82:       6902                    ld      s2,0(sp)
       66e84:       6105                    addi    sp,sp,32
       66e86:       8082                    ret

An interesting thing about strrchr here is that it's 74 bytes of code.
This is actually a little longer than the 64-byte version in the amd64
glibc, which is only 10 instructions.  This, by contrast, is 32
instructions.

It isn't obvious to me at all how this finds a pointer to the last
occurrence of a byte in a string, but I guess it's calling strchr
repeatedly until it returns null.

Most popular instructions: ld, sd, li, addi, mv, j, jal, add, auipc, beqz, lw
-----------------------------------------------------------------------------

We can get some instruction popularity stats:

    $ riscv64-linux-gnu-objdump -d /usr/riscv64-linux-gnu/lib/libc-2.31.so |awk '{print $3}' | sort | uniq -c | sort -nr

From 257029 lines of disassembly output this produces 144 lines for
distinct instruction mnemonics, of which the first 44:

      36499 ld
      25129 sd
      22230 li
      22070 addi
      21996 mv
      12265 j
      10757 jal
      10043 add
       8262 auipc
       7700 beqz
       6829 lw
       5701 bnez
       4967 slli
       4748 beq
       4279 sw
       3828 lbu
       3813 bne
       3734 
       3433 ret
       3340 lui
       2973 andi
       2904 sext.w
       2876 addiw
       2255 bltu
       2069 sub
       2024 bgeu
       1926 sb
       1789 srli
       1335 and
       1063 or
        894 bltz
        813 bge
        811 subw
        770 blt
        769 lhu
        684 jalr
        680 addw
        676 ecall
        533 blez
        393 fence

This is mostly pretty unremarkable stuff: load doubleword, store
doubleword, load immediate, add immediate, move, jump, jump and link,
add, auipc (for constructing PC-relative addresses), beqz (branch if
zero), load word, branch if not zero, shift left logical immediate,
branch if equal, store word, load byte unsigned, branch if not equal,
blank lines, return, load upper immediate (for constructing
non-PC-relative immediates that don't fit in 12 bits), AND immediate,
sign extend word, add immediate word, etc.

There are some pseudoinstructions listed separately.  li and mv are
forms of addi; j is a form of jal; beqz, bnez, bltz, and blez are
forms of beq, bne, blt, and ble; sext.w is a form of addiw.

slt/sltu is a pretty useless instruction
----------------------------------------

One of the ones I was wondering about is `slt`, whose variant `sltu`
turns out to be #62 on the list with 119 uses (0.05% of all
instructions).  It also has `slti` and `sltiu` variants.  These
instructions are the equivalent of most of the SETcc instructions on
amd64: setz, setl, setg, setnz, setb.  They compare two values for
ordering, but unlike the `bge` or `blt` instructions, instead of doing
a conditional jump based on the result, they store a 1 or 0 in a
register.  This rather directly implements the semantics of the C `<`
(or `>`) operator, but it's pretty rare in C that you want to do
something with that result other than take a control-flow branch.
With an additional instruction to invert the result they directly
implement the semantics of `>=` or `<=`.  For example, from
`__strtof_l`:

       35e5a:       fd07071b                addiw   a4,a4,-48    # subtract 48 from a4, maybe to convert from ASCII to binary
       35e5e:       0ff77713                andi    a4,a4,255    # mask it down to an unsigned byte, maybe in case it became negative
       35e62:       00e63733                sltu    a4,a2,a4     # overwrite a4 with a 0/1 indicating whether a2 < a4 (at this point a2 = 9 I think)
       35e66:       00174713                xori    a4,a4,1      # invert that result
       35e6a:       1702                    slli    a4,a4,0x20   # shift it left 32 bits
       35e6c:       9301                    srli    a4,a4,0x20   # shift it right 32 bits (unsigned!  but that makes no sense)
       35e6e:       9aba                    add     s5,s5,a4     # add this weird result to s5

I think probably I need to look at the source code in
`glibc-2.31/stdlib/strtof_l.c`, or actually `strtod_l.c` to figure out
what's going on here.  But that file is 1800 lines long and has about
14 places where it's trying to figure out if something is between '0'
and '9', and none of them seem to be adding the comparison result to
anything or shifting it first left and then right by 32 bits.  The
same shift sequence also appears in gettext so I suspect the compiler
is emitting it as part of some implicit type conversion:

       2a4d0:       bf0d                    j       2a402 <gettext@@GLIBC_2.27+0x3a>
       2a4d2:       00a93933                sltu    s2,s2,a0
       2a4d6:       00194913                xori    s2,s2,1
       2a4da:       1902                    slli    s2,s2,0x20
       2a4dc:       02095913                srli    s2,s2,0x20
       2a4e0:       b70d                    j       2a402 <gettext@@GLIBC_2.27+0x3a>

I'm not getting a great sense of how to use this instruction from
reading the long-winded disassembly of glibc.  I did find one clear
case, in `__libc_alloca_cutoff`, which is literally returning the
result of a `<=` operator as an int, according to
`nptl/alloca_cutoff.c`, which I think is the version of
`__libc_alloca_cutoff` that gets built.  There are also versions of
`__libc_alloca_cutoff` in htl and fbtl, but htl/ seems to be the Hurd
thread library, and fbtl seems to be an old version of nptl.

I think probably this instruction is not very important and maybe
shouldn't have been included in RISC-V.  In the very rare case where
you really want a 0 or 1 in a register to indicate the comparison
result, the benefit over using a conditional branch to get it there is
surely extremely marginal.

Actually it's not quite that extreme: where applicable, objdump
disassembles `slt` or `sltu` as the pseudo-instructions `snez` (278
uses), `seqz` (269 uses), and `sgtz` (24 uses), and there are also 27
uses of plain `slt`, 21 of `slti`, and 15 of `sltiu`.  (+ 119 278 269
24 21 15) = 726 total uses, so instead of 0.05% of instructions `sltu`
is 0.28%.  `seqz` has the semantics of the C `!` operator, storing 0
or 1 in a register, and `snez` has the semantics of the right side of
the C operators `||` and `&&`: they return 0 or 1, not the last value
evaluated.  `snez` gets used pretty often to choose between two
constants, like in this case in `fnmatch` where it's used to return
either 0 or 2 depending on whether a byte was NUL:

       8cc76:       0654c503                lbu     a0,101(s1)
       ...
       8cc7e:       00a03533                snez    a0,a0
       ...
       8cc84:       0506                    slli    a0,a0,0x1
       8cc86:       6105                    addi    sp,sp,32
       8cc88:       8082                    ret

(glibc-2.31/posix/fnmatch.c apparently has the source code for
`fnmatch` but offhand I don't see any C code that seems to correspond
to this logic; it returns -2 in lots of places but never 2.)

Or in this case in `sethostid` where it returns 0 or -1 depending on
whether s0 was 4:

       a0472:       ffc40513                addi    a0,s0,-4
       a0476:       00a03533                snez    a0,a0
       a047a:       40a00533                neg     a0,a0
        ... (a conditional branch and epilogue is omitted here) ...
       a0490:       6145                    addi    sp,sp,48
       a0492:       8082                    ret

The source code in glibc-2.31/sysdeps/unix/sysv/linux/gethostid.c
says:

    return written != sizeof (id32) ? -1 : 0;

The conditional-branch way to do this would be 7 instructions (5 or 6
executed) instead of 5 instructions, which seems slightly worse but
maybe not so much worse as to justify imposing an extra instruction on
the CPU.

        addi a0, s0, -4
        bnez a0, 1f
        li a0, -1
        j 2f
    1:  li a0, 0
    2:  # function epilogue needed here
        addi sp,sp, 48
        ret

A full example of this, which seems to work when I test it, assembles
as follows:

    00000000000005e4 <main>:
     5e4:   842a                    mv      s0,a0
     5e6:   7179                    addi    sp,sp,-48
     5e8:   e022                    sd      s0,0(sp)
     5ea:   ffc40513                addi    a0,s0,-4
     5ee:   e119                    bnez    a0,5f4 <main+0x10>
     5f0:   557d                    li      a0,-1
     5f2:   a011                    j       5f6 <main+0x12>
     5f4:   4501                    li      a0,0
     5f6:   6402                    ld      s0,0(sp)
     5f8:   6145                    addi    sp,sp,48
     5fa:   8082                    ret

An interesting thing here is that, of the 7 instructions for this
conditional and return, 6 are compressed instead of 2.  So the
addi/bnez/li/j/li combo is 12 bytes of code, same as the addi/snez/neg
combo.

Notes on writing RISC-V assembly by hand
----------------------------------------

The fact that the assembler has register aliases a0, s0, etc., makes
it pretty easy to remember which registers the standard ABI specifies
as call-preserved: ra and s\* (and zero I guess) but not a\* and t\*.
I do miss ARM's ldm/stm instructions for this; the RISC-V manual
suggests you can implement them as "millicode" for tighter code, but
of course GCC's -Os doesn't.  On pp. 102–103 (120–121/128) of
riscv-spec-20191213.pdf there is a discussion of this, ending: "While
reasonable architects might come to different conclusions, we decided
to omit load and store multiple and instead use the software-only
approach of calling save/restore millicode routines to attain the
greatest code size reduction," referring you to §5.6 of Andrew
Waterman's dissertation for more details.

In the dumpulse example mentioned earlier, 20 of the 345 bytes of code
were saving and restoring 5 registers on entry and exit of
`dumpulse_process_packet`.  You could imagine a six-byte li/jal
millicode sequence to set up the stack frame and a two-byte j to tear
it down and return.  Maybe you could compress these further: there's a
compressed C.JAL in RV32C, though not RV64C, with an 11-bit jump
offset; but it overwrites ra (x1).  There is no way to use C.JAL with
the alternate link register x5 (t0).  There is also C.JALR, which are
in RV64C as well, which you could use for the millicode call if you're
willing to spend a register on it; the register field in C.JALR is
five bits, so it can be a register that isn't addressable by most C
instructions.

In the aforementioned §5.6, Waterman uses uncompressed `jal t0,
prologue_2` and `jal x0, epilogue_2` instructions for his millicode
procedure prologue and epilogue, the second one being a tail call
(just a `j`).  There are instructions after it but they are the target
of a conditional branch:

        c.beqz a0, 16
        jal    t0, prologue_2
        c.mv   s0, a0
        c.addi a0, -1
        jal    ra, factorial
        mul    a0, a0, s0
        jal    a0, epilogue_2
    16: c.li   a0, 1
        c.jr   ra

I was thinking you could use a preceding `li` instruction to tell the
millicode which registers to save and how big a stack frame to set up,
and Waterman indeed did do that — for the case of saving and restoring
more than two registers.  This left him with 122 bytes of
prologue/epilogue millicode for RV64C, a 4% code size reduction over
inline prologue/epilogue code, and a 3% increase in dynamic
instruction count on SPEC CPU2006.  (Because he didn't have physical
hardware he couldn't report wall-clock times; I think these numbers
are from `spike`.)  He also shrank Linux by 7.5% while increasing its
boot dynamic instruction count by 2.1%.  He points out that
profile-guided optimizations can probably improve this tradeoff
substantially.

For extremely small programs without recursion, especially in
extremely small RAM, you could conceivably use a different link
register for each level of the call stack.  This would be a pain to
write by hand, and it would require your call instructions to be
uncompressed (which they are anyway on RV64C) but for some reason C.JR
can return to an address in any link register.

RISC-V assembly is often even more long-winded than amd64 or ARM
assembly, which is annoying.

GCC nested functions
--------------------

I wrote this program to see how GCC handles nested functions on
RISC-V.  Typically this involves a little bit of runtime code
generation (RTCG) on the stack so that a single-word pointer "to the
nested function" can point to a trampoline that adds a context pointer
before invoking the real nested function.

    #include <stdio.h>
    #include <stdlib.h>

    int main(int argc, char **argv)
    {
      char *words[] = { "bespoken", "readouts", "coopering", "subtitling",
                        "demographer", "censoring", "uttered", "flashes",
      };
      enum { nwords = sizeof(words) / sizeof(words[0]) };

      int k = atoi(argv[1]);
      int compare(const void *a, const void *b)
      {
        char *ac = *(char**)a;
        char *bc = *(char**)b;
        return ac[k] - bc[k];
      }

      qsort(words, nwords, sizeof(words[0]), compare);
      for (size_t i = 0; i < nwords; i++) {
        printf("- %s\n", words[i]);
      }
      return 0;
    }

You invoke it as `./qsortn 1` for example to sort the word list by its
second letter.

With -Os the nested function itself is compiled thus, as a leaf
function of 9 instructions and 24 bytes:

    00000000000009b4 <compare.0>:
     9b4:   0003a683                lw      a3,0(t2)   # t2 (x7) is used to pass the context pointer
     9b8:   6118                    ld      a4,0(a0)   # a0 is a, a4 is ac
     9ba:   619c                    ld      a5,0(a1)   # a1 is b, a5 is bc
     9bc:   9736                    add     a4,a4,a3   # ac + n
     9be:   97b6                    add     a5,a5,a3   # bc + n
     9c0:   00074503                lbu     a0,0(a4)   # load byte unsigned; odd that this isn't compressible
     9c4:   0007c783                lbu     a5,0(a5)
     9c8:   9d1d                    subw    a0,a0,a5   # finally compute return value
     9ca:   8082                    ret

main() is rather long, 69 instructions and 192 bytes (vs. 54
instructions and 240 bytes on amd64), with a largish stack frame of
304 bytes to accommodate the whole array of 8-byte string pointers as
well as the nested function (for which I think it reserved a lot more
space than it needed).  31 of those instructions (84 bytes) seem to be
the runtime code generation, generating 28 bytes of code within the
stack frame.

    0000000000000830 <main>:
     830:   7169                    addi    sp,sp,-304   # sp is x2
     832:   ea4a                    sd      s2,272(sp)   # s2 is x18
     834:   00002917                auipc   s2,0x2       # this is computing a pointer into the .got
     838:   86c93903                ld      s2,-1940(s2) # 20a0 <__stack_chk_guard@GLIBC_2.27>
     83c:   00093783                ld      a5,0(s2)     # maybe a buffer overflow protection measure?
     840:   1094                    addi    a3,sp,96     # sp+96 is base address for RTCG
     842:   8536                    mv      a0,a3        # which is the first argument to riscv_flush_icache
     844:   fdbe                    sd      a5,248(sp)   # store the stack protector canary at 248(sp)
     846:   1a1c                    addi    a5,sp,304
     848:   f9be                    sd      a5,240(sp)   # store sp+304 at 240(sp)
     84a:   39700793                li      a5,919
     84e:   d0be                    sw      a5,96(sp)    # store instruction word 0x397 in code at 96(sp)
     850:   0183b7b7                lui     a5,0x183b
     854:   28378793                addi    a5,a5,643 # 183b283 <__global_pointer$+0x1838a83>
     858:   d2be                    sw      a5,100(sp)   # store instruction word 0x01838b83 in code at 100(sp)
     85a:   0103b7b7                lui     a5,0x103b
     85e:   38378793                addi    a5,a5,899 # 103b383 <__global_pointer$+0x1038b83>
     862:   d4be                    sw      a5,104(sp)   # store instruction word 0x0103b383 in code at 104(sp)
     864:   000287b7                lui     a5,0x28
     868:   06778793                addi    a5,a5,103 # 28067 <__global_pointer$+0x25867>
     86c:   ee26                    sd      s1,280(sp)
     86e:   e64e                    sd      s3,264(sp)
     870:   84ae                    mv      s1,a1        # save argv in s1 for later use
     872:   d6be                    sw      a5,108(sp)   # store instruction word 0x00028067 in code at 108(sp)
     874:   4601                    li      a2,0         # flags for riscv_flush_icache are 0
     876:   00000797                auipc   a5,0x0       # load PC-rel address of callee compare.0 into a5
     87a:   13e78793                addi    a5,a5,318 # 9b4 <compare.0>
     87e:   198c                    addi    a1,sp,240    # sp+240 is the end argument to riscv_flush_icache
     880:   05810993                addi    s3,sp,88     # compute address of k (sp+88) in s3
     884:   f606                    sd      ra,296(sp)
     886:   fcbe                    sd      a5,120(sp)   # store address of callee into code at 120(sp)
     888:   e436                    sd      a3,8(sp)     # a3 is still sp+96, entry point of generated code
     88a:   f222                    sd      s0,288(sp)
     88c:   f8ce                    sd      s3,112(sp)   # finally store sp+88 (&k) into code at 112(sp)
     88e:   0820                    addi    s0,sp,24     # compute sp+24 for later memcpy
     890:   f81ff0ef                jal     ra,810 <__riscv_flush_icache@plt>  # aha, here we are done with RTCG
     894:   04000613                li      a2,64        # we'll memcpy 64 bytes from 0x2008 (within .data)
     898:   00001597                auipc   a1,0x1       # (these are the pointers to the 8 strings)
     89c:   77058593                addi    a1,a1,1904 # 2008 <__dso_handle+0x8>
     8a0:   8522                    mv      a0,s0        # memcpy dest is sp+24
     8a2:   f5fff0ef                jal     ra,800 <memcpy@plt>
     8a6:   6488                    ld      a0,8(s1)     # load argv[1] as atoi argument
     8a8:   00000497                auipc   s1,0x0       # load address of "- %s\n" into s1 for later printf use
     8ac:   1f048493                addi    s1,s1,496 # a98 <__libc_csu_fini+0x74>
     8b0:   f71ff0ef                jal     ra,820 <atoi@plt>
     8b4:   66a2                    ld      a3,8(sp)     # address of trampoline code is at 8(sp), from a3 above
     8b6:   ccaa                    sw      a0,88(sp)    # store atoi return value (k) at 88(sp)
     8b8:   4621                    li      a2,8         # a2 = sizeof char *
     8ba:   45a1                    li      a1,8         # a1 = nwords = 8
     8bc:   8522                    mv      a0,s0        # s0 = sp+24 is the address of the array to sort
     8be:   f23ff0ef                jal     ra,7e0 <qsort@plt>
     8c2:   6010                    ld      a2,0(s0)                  # top of printf loop, s0 is string pointer
     8c4:   85a6                    mv      a1,s1                     # format string loaded earlier
     8c6:   4505                    li      a0,1                      # printf_chk takes a flag arg 0
     8c8:   0421                    addi    s0,s0,8                   # increment string pointer
     8ca:   f07ff0ef                jal     ra,7d0 <__printf_chk@plt>
     8ce:   fe899ae3                bne     s3,s0,8c2 <main+0x92>     # end of printf loop, s3 is end of array?
     8d2:   776e                    ld      a4,248(sp)              # load stack protector canary
     8d4:   00093783                ld      a5,0(s2)
     8d8:   00f70463                beq     a4,a5,8e0 <main+0xb0>
     8dc:   f15ff0ef                jal     ra,7f0 <__stack_chk_fail@plt>
     8e0:   70b2                    ld      ra,296(sp)
     8e2:   7412                    ld      s0,288(sp)
     8e4:   64f2                    ld      s1,280(sp)
     8e6:   6952                    ld      s2,272(sp)
     8e8:   69b2                    ld      s3,264(sp)
     8ea:   4501                    li      a0,0
     8ec:   6155                    addi    sp,sp,304
     8ee:   8082                    ret

Once all the nested function setup stuff and stack protector stuff and
local variable initialization stuff is out of the way, the body of
main() is quite small and simple.  The atoi() call is two instructions
to call and one instruction to store the return value.  The qsort()
call is 5 instructions.  The printf() loop is 6 instructions plus a
hoisted auipc/addi pair earlier.  And then there's 4 instructions of
stack protector and 8 instructions of function epilogue.

This `riscv_flush_icache` thing is apparently a system call added in
Linux 4.20 (system call number 259, or found in the vDSO).  It
evidently takes arguments "start, end, flags"; in this case sp+96,
sp+240, 0.  I think that in earlier versions of RISC-V you would have
just used FENCE.I.  IIRC the RISC-V manual explains that the FENCE.I
instruction has been removed from the base RISC-V instruction set
because it's not good enough for Linux: in a multicore system you
might do your RTGC on one core, run your FENCE.I, and then get
suspended by the kernel and then later resume execution on a different
core that didn't see the FENCE.I (or another thread might execute on a
different core).  So apparently you need to do a syscall to ensure
that the instruction cache is purged across cores when you do RTCG.

Dumpulse is 345 bytes, which is bigger than it should be
--------------------------------------------------------

I wrote [Dumpulse](https://github.com/kragen/dumpulse) to be as small
as possible, but on a big-endian 32-bit ARM with Thumb2, where it
compiles down to just over 200 bytes.  Compiling it for RV64, it ends
up at 345 bytes, which is slightly better than amd64 or i386 and
slightly worse than AVR.  This is not the #1 code density I was
expecting from RV64IC.

    $ riscv64-linux-gnu-gcc-10 -Wa,-adhlns=dumpulse-riscv.lst -g -Os -Wall -Wpedantic -std=c89   -c -o dumpulse-riscv.o dumpulse.c 
    $ size dumpulse-riscv.o
       text	   data	    bss	    dec	    hex	filename
        345	      0	      0	    345	    159	dumpulse-riscv.o

Part of this seems to be a matter of byte manipulation and bitshift
instructions not having compressed representations, and part of it is
that it's doing its byte-swapping into the big-endian order used by
dumpulse packets using byte manipulation and bitshift instructions.
Like, according to the listing:

      31:dumpulse.c    ****     | (u32)p[0] << 24
      32:dumpulse.c    ****     | (u32)p[1] << 16
     133 0060 03C90500              lbu     s2,0(a1)
      33:dumpulse.c    ****     | (u32)p[2] << 8
     135 0064 83C71500              lbu     a5,1(a1)
     139 0068 13854400              addi    a0,s1,4
      32:dumpulse.c    ****     | (u32)p[1] << 16
     144 006c 1B198901              slliw   s2,s2,24
      33:dumpulse.c    ****     | (u32)p[2] << 8
     146 0070 9B970701              slliw   a5,a5,16
      33:dumpulse.c    ****     | (u32)p[2] << 8
     148 0074 3369F900              or      s2,s2,a5
     150 0078 83C73500              lbu     a5,3(a1)
     152 007c 3369F900              or      s2,s2,a5
      34:dumpulse.c    ****     | (u32)p[3]
     154 0080 83C72500              lbu     a5,2(a1)

The listing is super confusing, partly because of the intercalation of
different computations (and it was worse before I stripped out the
extra labels and .loc directives) but it sure looks like it's doing
the big-endian conversion with a bunch of individual bit shifts and
unsigned byte loads, bit shifts, and bitwise ors, each of which is a
two-parcel (four-byte) uncompressed instruction.

Probably it is a design mistake that lbu can't be a compressed
instruction in this case; the C extension allocates compressed
encodings to lw, ld, lq, flw, and fld, but not lb or lbu, and
similarly for stores.

It would seem to make sense that you wouldn't allocate compressed
instructions to bitwise ops like these, given how uncommon they are,
but in fact compressed instructions *are* available.  The most popular
bitwise operation in the glibc disassembly above was slli, with 1.9%
of the total, followed by andi, both of which are bitwise operations
with an immediate constant.  You don't get bitwise operations between
two runtime-computed values until `and` and `or` (about 0.5% and 0.4%
of the total respectively).  As it happens, andi and slli *can* be
compressed instructions if the immediate is small enough and the
source and destination register are the same, but slliw cannot.  And
register-to-register and, or, and xor do have compressed two-argument
encodings, which presumably are only not being used here because of
the use of the s2 register: the compressed format can address s0 (x8),
s1 (x9), a0 (x10), a1 (x11), a2 (x12), a3 (x13), a4 (x14), and a5
(x15), but not s2.

Anyway, the ridiculous result is that a byte-swapped 32-bit word load
ends up requiring like 40 bytes of code.  Possibly GCC could do a
better job here but I think this might be a weak point in RISC-V.

A kind of funny thing about this is that the add instruction that is
commonly used to compute effective addresses has a lot of spare bits:
it's an R-type instruction, with seven low-order opcode bits, three
funct3 bits, and 7 funct7 bits, and RV32I only has 10 R-type
instructions.  For all of add, slt, sltu, and, or, xor, sll, and srl,
which are distinguished by funct3, funct7 is 0000000b, while for sub
and sra, it's 0100000b.  For all of these the low-order seven opcode
bits are 01 100 11, LSB last; the "11" at the end means it's not a
compressed (16-bit, single-parcel) instruction.  (For some reason the
actual bit instruction encodings of uncompressed instructions are
relegated to Chapter 24 of the spec, which omits the instruction
encodings of the compressed instructions, which are in Chapter 16.)

Another super confusing aspect of the listing above is that the
hexadecimal bytes given on the left are in byte order, with the least
significant byte first, while objdump displays them word by word, with
the least significant bits on the right.  The above bit of code looks
like this in (riscv64-linux-gnu-)objdump -d:

    0000000000000060 <.LBB24>:
      60:   0005c903                lbu     s2,0(a1)
      64:   0015c783                lbu     a5,1(a1)

    0000000000000068 <.LBE26>:
      68:   00448513                addi    a0,s1,4

    000000000000006c <.LBB31>:
      6c:   0189191b                slliw   s2,s2,0x18
      70:   0107979b                slliw   a5,a5,0x10
      74:   00f96933                or      s2,s2,a5
      78:   0035c783                lbu     a5,3(a1)
      7c:   00f96933                or      s2,s2,a5
      80:   0025c783                lbu     a5,2(a1)

Anyway, a thing I was going to suggest was that if the uncompressed
R-type add instruction used its four unused bits to specify a bit
shift for one of the operands, that would be enough to fuse the
bit-shifting operation with the bitwise or when byteswapping.  It
would also allow the elimination of an slli instruction for array
indexing in many cases, though that can usually be done with a smaller
shift.

Bitwise operations are uncommon enough that they don't seem worth
dedicating a lot of optimization resources to, but they're so cheap in
hardware and so expensive in software that it seems worth it to have
instructions for them.  Byte swapping is a similar case, though
perhaps the case for byte swapping instructions is weaker, because
they're even more uncommon.

The Adler32 checksum I foolishly chose for Dumpulse uses byte
operations but no bitwise operations, and it compiles to 68 bytes, two
bytes shorter than for amd64.  Here's the C:

    #define MOD_ADLER 65521L
    static u32 adler32(u8 *p, size_t len)
    {
      u32 a = 1, b = 0;
      while (len--) {
        a += *p++;
        b += a;
        if (!(len & 0xf)) {
          if (a >= MOD_ADLER) a -= MOD_ADLER;
          if (b >= MOD_ADLER) b -= MOD_ADLER;
        }
      }
      return b << 16 | a;
    }

And here's some annotated disassembly.  I am still not finding RISC-V
assembly super readable but I guess it's easier than ARM.

    0000000000000000 <adler32>:  # a0 is p, a1 is len
       0:   6641                    lui     a2,0x10    # load 65536 into a2
       2:   7841                    lui     a6,0xffff0 # and -65536 into a6
       4:   4701                    li      a4,0       # b = 0
       6:   4785                    li      a5,1       # a = 1
       8:   537d                    li      t1,-1      # when len reaches -1, exit loop
       a:   1641                    addi    a2,a2,-16  # update a2 to be 65520 (MOD_ADLER-1)
       c:   283d                    addiw   a6,a6,15   # and a6 to be (32-bit) -65521
    000000000000000e <.L2>:                              # top of loop
       e:   15fd                    addi    a1,a1,-1     # decrement len
      10:   00659763                bne     a1,t1,1e <.L5>  # exit?
      14:   0107151b                slliw   a0,a4,0x10      # b << 16
      18:   8d5d                    or      a0,a0,a5        # | a
      1a:   2501                    sext.w  a0,a0           # sign extend to 64 bits (ABI requirement)
      1c:   8082                    ret
    000000000000001e <.L5>:                              # end if
      1e:   00054683                lbu     a3,0(a0)     # load *p
      22:   00f5fe13                andi    t3,a1,15     # len & 0xf (optimization)
      26:   0505                    addi    a0,a0,1      # p++
      28:   9fb5                    addw    a5,a5,a3     # a += byte
      2a:   9f3d                    addw    a4,a4,a5     # b += a
      2c:   88ba                    mv      a7,a4        # useless copy of b value
      2e:   fe0e10e3                bnez    t3,e <.L2>   # skip back to top of loop unless !(len&0xf)
      32:   00f67463                bgeu    a2,a5,3a <.L4>  # if a5 (a) > 65520: (equivalent to >= 65521)
      36:   010787bb                addw    a5,a5,a6          # subtract 65521 from it
    000000000000003a <.L4>:
      3a:   fce67ae3                bgeu    a2,a4,e <.L2>   # similarly, if a4 (b) > 65520,
      3e:   0108873b                addw    a4,a7,a6          # subtract 65521 from it
      42:   b7f1                    j       e <.L2>         # skip back to the top of the loop

I think it's safe to say that GCC 10's RISC-V code quality is not
ideal here, given the useless copy of b into a7 in the inner loop.
And it seems to be doing instruction scheduling to try to expose ILP,
though I don't know if that actually helps on any current hardware,
because I can't imagine how it would end up with the goofy split apart
constant loading we see here otherwise.  I have to think that would
frustrate macro-op fusion in some (not yet implemented) cases.

I think it would be okay to use 64-bit math for the arithmetic here,
and I'm not sure why GCC doesn't.

Looking at the logic of the Adler32 optimization, I'm not sure it's
correct in my C code.  If a acquires a value close to 65000, I feel
like in 16 iterations b could acquire a value close to a million, and
never get brought back down by the subtractions.

QEMU slowdown is about an order of magnitude, and GCC's output isn't great code
-------------------------------------------------------------------------------

### Dumb Fibonacci microbenchmark ###

    __attribute__((fastcall)) int fib(int n)
    {
        return n < 2 ? 1 : fib(n-1) + fib(n-2);
    }
    main(int c, char **v) { printf("%d\n", fib(atoi(v[1]))); }

This machine is a bit slow:

    $ gcc -O5 -o amd64-fib fib.c
    fib.c:7:1: warning: ...
    $ time ./amd64-fib 40
    165580141

    real    0m1.909s
    user    0m1.895s
    sys     0m0.009s

But on this toy microbenchmark QEMU has no detectable slowdown due to
RISC-V:

    $ riscv64-linux-gnu-gcc-10 -O5 -o riscv-fib fib.c
    fib.c:7:1: warning: ...
    $ time ./riscv-fib 40
    165580141

    real    0m1.900s
    user    0m1.788s
    sys     0m0.021s

With recent versions of GCC this is no longer a particularly good
microbenchmark test; GCC unrolls the recursive loop aggressively, so
the benchmark results are sensitive to exactly how far it unrolled
them.  The fib() function in the output is 170 instructions long, with
a 256-byte stack frame.

### Linear feedback shift register program ###

Here's a slightly less lame microbenchmark:

    #include <stdio.h>

    __attribute__((weak)) int lfsr(unsigned char (*b)[16])
    {
      int i = (*b)[0];
      int c = (*b)[i+1] ^= (*b)[(i+1) % 15 + 1];
      i++;
      (*b)[0] = i % 15;
      return c;
    }

    int main()
    {
      unsigned char buf[16] = {0, 1};
      for (size_t i = 0; i < 1024; i++) {
        for (size_t j = 0; j < 16; j++) {
          if (i % 15 == 0) printf("%3d ", buf[j]);
        }

        int c = lfsr(&buf);
        if (i % 15 == 0) printf("(%d)\n", c);
      }

      return 0;
    }

Compiled for RISC-V and run in QEMU this works and takes 45
milliseconds:

    $ riscv64-linux-gnu-gcc-10 -Wall -Werror -O5 -std=gnu99    lfsr.c   -o lfsr-riscv
    $ time ./lfsr-riscv 
      0   1   0   0   0   0   0   0   0   0   0   0   0   0   0   0 (1)
      0   1   0   0   0   0   0   0   0   0   0   0   0   0   0   1 (1)
      0   1   0   0   0   0   0   0   0   0   0   0   0   0   1   0 (1)
      0   1   0   0   0   0   0   0   0   0   0   0   0   1   1   1 (1)
      0   1   0   0   0   0   0   0   0   0   0   0   1   0   0   0 (1)
      0   1   0   0   0   0   0   0   0   0   0   1   1   0   0   1 (1)
      0   1   0   0   0   0   0   0   0   0   1   0   1   0   1   0 (1)
      0   1   0   0   0   0   0   0   0   1   1   1   1   1   1   1 (1)
    ...
      0   1   0   0   1   0   0   0   0   0   0   0   0   0   0   0 (1)

    real    0m0.045s
    user    0m0.035s
    sys     0m0.005s

Compiled natively it's 4 ms:

    $ cc -O5 -Wall -Werror -std=gnu99    lfsr.c   -o lfsr
    $ time ./lfsr
    ...
    real    0m0.004s
    user    0m0.000s
    sys     0m0.003s

That's crudely about an 11× slowdown.

I handwrote a 15-instruction version of the lfsr() function in RISC-V
assembly that also seems to work, but it's not any faster.  Note that
in the comments I'm using `b` to mean the 15 bytes of previous LFSR
output, not the 16-byte buffer that also includes the counter.

            .globl lfsr
    lfsr:   lb a1, (a0)             # a0 points to beginning of 16-byte buffer
            addi a1, a1, 1          # increment counter i in a1 stored in first byte
            addi a4, zero, 15       # can't compare to an immediate constant, so load 15 into register a4, oddly not compressed
            mv a2, a1               # copy i+1 to a2 for (i+1) % 15 calculation
            bne a1, a4, 1f          # skip if i+1 != 15
            addi a2, a2, -15        # but if it was, subtract 15, result in a2
    1:      add a3, a0, a1          # calculate &b[i] in a3
            addi a5, a2, 1          # calculate (i+1) % 15 + 1 in a5
            lb a1, (a3)             # load b[i] into a1
            add a5, a0, a5          # calculate &b[(i+1) % 15] in a5
            sb a2, (a0)             # store incremented value (i+1) % 15 in first byte
            lb a0, (a5)             # load b[(i+1) % 15] into a0
            xor a0, a0, a1          # calculate final XOR result in a0
            sb a0, (a3)             # store a0 at b[i] and implicitly return it
            jalr zero, ra           # return from subroutine, oddly not compressed

This assembles to 50 bytes.  GCC's output is also 15 instructions,
using the division instruction `remw`, but compresses to four bytes
shorter, 46 bytes:

    0000000000000858 <lfsr>:
     858:   00054783                lbu     a5,0(a0)
     85c:   463d                    li      a2,15
     85e:   86aa                    mv      a3,a0
     860:   2785                    addiw   a5,a5,1
     862:   02c7e63b                remw    a2,a5,a2
     866:   97aa                    add     a5,a5,a0
     868:   0007c503                lbu     a0,0(a5)
     86c:   0016059b                addiw   a1,a2,1
     870:   95b6                    add     a1,a1,a3
     872:   0005c703                lbu     a4,0(a1)
     876:   8f29                    xor     a4,a4,a0
     878:   0ff77513                andi    a0,a4,255
     87c:   00a78023                sb      a0,0(a5)
     880:   00c68023                sb      a2,0(a3)
     884:   8082                    ret

Two of those 15 instructions seem to be unnecessary — an mv to save a
copy of the needlessly clobbered a0 register and an andi to mask off a
bunch of bits that are already 0.  `-Os` makes it worse.  A little
more thought yielded this 13-instruction 38-byte version, still no
faster in QEMU, still using no division but only 4 temporary registers
instead of 6:

            .globl lfsr
    lfsr:   lbu a1, (a0)          # load unsigned counter byte i at a0 into a1
            addi a1, a1, 1        # increment it
            addi a2, a1, -15      # compute (i+1) - 15
            beqz a2, 1f           # but unless the result is zero (compressible jump!)
            mv a2, a1             # overwrite it with (i+1) (this mv is an SSA φ function)
    1:      add a1, a1, a0        # so a2 is (i+1)%15.  then compute &b[i] in a1 from a0 + (i+1)
            sb a2, (a0)           # store %15 result at a0
            lbu a3, (a1)          # load byte b[i] from a1 into a3
            add a2, a2, a0        # add (i+1) % 15 to a0 to get other byte address in a2 (offset by -1)
            lbu a0, 1(a2)         # overwrite a0 with byte from address a2+1
            xor a0, a0, a3        # xor bytes fetched from a1 (b[i]) and a5 (b[(i+1)%15])
            sb a0, (a1)           # store xor result at address a1
            ret                   # oddly enough, the assembler can compress this

I think this can be bummed down to 12 instructions.

By comparison, GCC's amd64 version is 22 instructions and 64 bytes:

    00000000000013c0 <lfsr>:
        13c0:	f3 0f 1e fa          	endbr64                       no-op for control flow integrity
        13c4:	0f b6 07             	movzbl (%rdi),%eax            load byte unsigned into %eax
        13c7:	b9 89 88 88 88       	mov    $0x88888889,%ecx       we're going to divide by 15 by multiplying by 2290649225
        13cc:	8d 50 01             	lea    0x1(%rax),%edx         increment i into %edx
        13cf:	48 89 d0             	mov    %rdx,%rax              and into %eax
        13d2:	48 0f af d1          	imul   %rcx,%rdx              actually do the multiply
        13d6:	89 c6                	mov    %eax,%esi              this is still i+1, now in %rax and %rsi I think
        13d8:	48 98                	cltq                          convert long to quadword I think?  implicitly in %esi? or %edx?
        13da:	48 c1 ea 23          	shr    $0x23,%rdx             right shift multiply result 35 bits?
        13de:	89 d1                	mov    %edx,%ecx              yeah uh
        13e0:	c1 e1 04             	shl    $0x4,%ecx              no idea but
        13e3:	29 d1                	sub    %edx,%ecx              I'm guessing this is some kind of fixup and we're still in ÷15
        13e5:	29 ce                	sub    %ecx,%esi              and now %rsi has (i+1) % 15?
        13e7:	8d 4e 01             	lea    0x1(%rsi),%ecx         and now %rcx is (i+1) % 15 + 1
        13ea:	48 63 c9             	movslq %ecx,%rcx              sign-extend that into %rcx for use in indexing
        13ed:	0f b6 0c 0f          	movzbl (%rdi,%rcx,1),%ecx     load byte unsigned offset from %rdi, index %rcx, into %ecx
        13f1:	32 0c 07             	xor    (%rdi,%rax,1),%cl      and the other byte indexed with %rax gets xored
        13f4:	88 0c 07             	mov    %cl,(%rdi,%rax,1)      and stored back at b[i]
        13f7:	0f b6 c1             	movzbl %cl,%eax               and zero-extended to form a return value
        13fa:	40 88 37             	mov    %sil,(%rdi)            and store the updated i back
        13fd:	c3                   	retq
        13fe:	66 90                	xchg   %ax,%ax                no-op to align the next function

Or, with `-Os`, a more comprehensible 18 instructions and 55 bytes, 42
bytes without counting the nop-padding at the end:

    0000000000001229 <lfsr>:
        1229:	f3 0f 1e fa          	endbr64 
        122d:	0f b6 0f             	movzbl (%rdi),%ecx
        1230:	be 0f 00 00 00       	mov    $0xf,%esi
        1235:	ff c1                	inc    %ecx
        1237:	89 c8                	mov    %ecx,%eax
        1239:	48 63 c9             	movslq %ecx,%rcx
        123c:	99                   	cltd   
        123d:	f7 fe                	idiv   %esi
        123f:	8d 42 01             	lea    0x1(%rdx),%eax
        1242:	48 98                	cltq   
        1244:	8a 04 07             	mov    (%rdi,%rax,1),%al
        1247:	32 04 0f             	xor    (%rdi,%rcx,1),%al
        124a:	88 04 0f             	mov    %al,(%rdi,%rcx,1)
        124d:	0f b6 c0             	movzbl %al,%eax
        1250:	88 17                	mov    %dl,(%rdi)
        1252:	c3                   	retq   
        1253:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
        125a:	00 00 00 
        125d:	0f 1f 00             	nopl   (%rax)

Anyway, this is probably not the best interface for this function; it
would usually be better to loop over the entire buffer once so that
the caller has a whole new fresh buffer to work with per call, rather
than just one byte.  That would probably require a little more code,
but it would take less time, and you wouldn't need to store i in the
structure.
