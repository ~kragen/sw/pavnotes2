i see.  so you think that perhaps in 01974 a computer industry startup in pittsburgh had more of a chance than it would have today, 50 years later, and that there are many more places in the world that are like pittsburgh in that way than places that are like shenzhen?  that is, that maybe in 01974 there were dozens of cities where you could launch something like perq, and now there are only ten or something, so it was 'much more geographically inclusive' 50 years ago, even though those ten cities are much farther apart now, and the dozens in 01974 were all in the usa?

i have two doubts about this thesis, i'm not sure there *were* dozens of cities where you could launch something like perq 50 years ago, particularly considering they went bankrupt in 01985, possibly because of being in pittsburgh.  i also suspect there *are* dozens of cities where you could do it today—i don't think you have to be in a 'champion city' to do a successful hardware startup.  it's a little hard to answer that question empirically, because we have to wait 10 years to see which of today's hot startups survive that long

but maybe we can look at the computer companies that are newly-ish big, restricting our attention to mostly hardware, since that's what three rivers was doing and where you'd expect the most concentration.  xiaomi, founded 02010 in beijing.  huawei, founded 01987 in shenzhen.  tencent, founded 01998 nominally in the caymans but really at shenzhen university.  loongson, founded 02010 in beijing.  sunway (jiangnan computing lab), founded 02009 (?) in wuxi.  dji, founded 02006 in hong kong, but immediately moved to shenzhen.  allwinner, founded 02007 in zhuhai (across the river from shenzhen and hong kong).  mediatek, founded 01997 in hsinchu, where umc is.  nanjing qinheng microelectronics (wch, a leading stm32 clone vendor), founded 02004 in nanjing.  espressif, founded 02008 in shanghai.  rockchip, founded 02001 in fuzhou.  nvidia, founded 01993 in silicon valley.  alibaba (parent company of t-head/pingtouge), founded 01999 in hangzhou.  bitmain, founded 02013 in beijing (?).  hisilicon, founded 01991 in shenzhen, which designed the kirin 9000s cpu huawei's new phones use and is the largest domestic designer.  smic, which csis calls 'the most advanced chinese logic chip manufacturer' (someone should tell csis about tsmc), and makes the kirin 9000s, founded 02000 in shanghai.  smee, ('the most advanced chinese lithography company' —csis) founded 02002 in shanghai. mobileye, founded in jerusalem in 01999.  biren, the strategic gpu maker, founded 02019 in shanghai.  moore threads, the *other* strategic gpu maker, founded 02020 in beijing.  unisoc (spreadtrum), fourth largest cellphone cpu company, founded 02001 in shanghai.  tenstorrent, founded 02016 in toronto.  zhaoxin, the amd64 vendor, founded 02013 in shanghai.  changxin (innotron), the dram vendor, founded 02016 in hefei.  fujian jinhua, the other dram vendor, founded 02016 in, unsurprisingly, fujian, i think in fuzhou.  raspberry pi, founded 02009 in cambridge (uk).  ymtc, the nand vendor, founded 02016 in wuhan.  wingtech, the parent company of nexperia (previously philips), founded 02006 in jiaxing.  huahong, founded 01996 in shanghai.  phytium, makers of feiteng, founded 02014 in tianjin.  oculus, founded 02012 in los angeles.  zte, founded 01985 in shenzhen.  gigadevice, founded 02005 in beijing.  piotech, founded 02010 in shenyang.  amec, founded 02004 in shanghai.  ingenic, founded 02005 in beijing.  silan, founded 01997 in hangzhou.  nexchip, founded 02015 in hefei.  united nova technology, founded 02018 in shaoxing.  cetc, the defense contractor, founded 02002 in beijing.  naura, the largest semiconductor equipment manufacturer in china, founded 02001 in beijing.

maybe my sampling here is not entirely unbiased, but we have 7 companies in beijing, 7 in shanghai, 6 in shenzhen, 2 in hangzhou, 2 in hefei, 2 in fuzhou, and then one each in each of wuxi, zhuhai, hsinchu, nanjing, silicon valley, jerusalem, cambridge, wuhan, toronto, jiaxing, tianjin, los angeles, and shaoxing.  that seems like 19 cities where you could plausibly start a new computer maker today, given that somebody did in the last 30 years.  did the usa ever have that many in the 01970s?

to me this doesn't support your thesis 'countries had to reinforce their champion cities so they can compete against other champion cities [resulting in] the redlining of 98% of america and similar phenomena in those countries as well' so that 'focus on (...) international competition [promoted] interregional inequality in countries'.  rather the opposite: it looks like winning the international competition and heavy state investment has resulted in increasingly widespread computer-making startups throughout china, mostly in the prc, while computer makers founded elsewhere in the last few decades mostly failed.  i mean, yes, none of these startups are in xinjiang or mississippi.  but they're not all in one province of china, either; they're distributed across hebei province (sort of!), jiangsu, anhui, hubei, zhejiang, fujian, israel, taiwan, california, england, shanghai, and guangdong.  taiwan and 7 of the 22 provinces† of the prc are represented in this list.  they tend to be the more populous provinces, though there are some provinces conspicuously missing from the list, like shandong and henan

guangdong: 126 million

jiangsu: 85 million

hebei: 75 million

zhejiang: 65 million

anhui: 61 million

hubei: 58 million

fujian: 41 million

shanghai (city): 25 million

beijing (city): 22 million

tianjin (city): 14 million

the total population of the above regions is 570 million

taiwan is another 24 million people, bringing the total to some 600 million.  so this is not quite half the population of china.  so while there is surely some 'redlining' going on in china, i don't think it's against 98% or even 60% of the population

especially if we add california, israel, england, and ontario to the list, i think it's clear that the computer industry today is far more geographically inclusive than it was 50 years ago

______

† the prc also contains 5 'autonomous regions', two 'special administrative regions' (hong kong and macau), and four direct-administered municipalities, of which three are on the list
