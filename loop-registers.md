A thing that may be handy for optimizing inner loops on simple
hardware: “loop registers”, consisting of a start-address register, an
end-address register, and a loop-counter register.  When the low bits
of the PC match the end-address register and the loop-counter register
is nonzero, instead of incrementing the PC we decrement the loop
counter and reload the low bits of the PC from the start-address
register.  In this way a loop body can reasonably be as short as a
single instruction, with the looping logic running concurrently with
the body execution without a general-purpose superscalar execution
engine.  The Raspberry Pi Pico pioasm coprocessor has something like
this.

Plausibly, if full width, the start-address register can be unified
with a link register for subroutine returns; the end-address register
could conceivably also be used for that, but this is probably not so
useful because it requires loading the register on subroutine entry
with the address where the subroutine ends rather than using a return
instruction, so it doesn’t save any instructions, and the
load-end-address instruction or instructions will necessarily be
larger than the minimal size for a return instruction.

It may be better to instead have the loop counter count upwards,
though this costs an extra register for the loop limit and a
comparator to compare the two registers.  While in theory this doesn’t
add any power, my survey in file `c-stack-bytecode.md` in Dernocua
found 14 integer up-counting loops, one integer down-counting loop,
and 7 other for loops (linked-list iteration, pointer loops,
NUL-terminated sentinel loops, etc.)  Moreover, I introduced the
down-counting loop myself.  So there’s a lot of code out there that
can be straightforwardly compiled to use a loop-counter register that
counts upward, while compiling it to use one that counts downward is
usually much less trivial.

You might have a single instruction that sets the loop-end register
and another single instruction that sets the loop-start register to
two instructions later (skipping a “delay slot” that can be used for
further loop initialization), and either sets the iteration limit and
zeroes the loop counter, or in the down-counting case sets the loop
counter.  Consider this loop from CMU Sphinx:

                for (i = 0; i < n_float32; i++) {
                    SWAP_FLOAT32(&float_feat[i]);
                }

`SWAP_FLOAT32` is a byte-swapping macro, but suppose it’s an
instruction.  `n_float32` is a local variable and `float_feat` is a
local pointer variable; we can suppose these are in registers.  Then
our assembly code might look like this:

        loop_to t1    ; n_float32
        loop_end 1f   ; set loop end register to point to instruction after loop
        t3 := t2!i    ; 32-bit load from base float_feat indexed by loop counter
        swap_int32 t3
        t2!i := t3    ; 32-bit store (this ! stuff is BCPL syntax)
    1:

This makes the byte-swapping inner loop 3 instructions rather than
5–6, which is a potentially very significant speed boost.  If we don’t
have available a basereg+wordoffset addressing mode, or if the counter
register is not usable as the index, it would be less of a boost.

If we instead wanted to start the counting at 2, we could reshuffle
the instructions as follows:

        loop_end 1f
        loop_to t1
        i := 2        ; load loop counter with immediate constant in delay slot
        t3 := t2!i
        swap_int32 t3
        t2!i := t3
    1:

Going back to counting from 0, if the hardware implements a down
counter instead to save transistors, we need a negative-offset
indexing mode, and we need to start by calculating the end address,
consuming another GPR:

        t4 := t2 + t1*4  ; calculate end of array, hopefully one insn
        loop_from t1     ; initialize loop counter
        loop_end 1f
        t3 := t4!-i
        swap_int32 t3
        t4!-i := t3
    1:                   ; we arrive here when i was decremented to 0, not -1
