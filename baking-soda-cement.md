As I waited at the bus stop last night for hours (for a bus that would
never come), I was thinking about ways to accelerate the setting of
slaked lime into lime cement.  It’s well known that baking soda can
quickly harden cyanoacrylate by donating hydroxyls, and that it forms
part of the carbonate buffer system so important for pH control in
biochemistry, where [carbonate][1] (pK<sub>a</sub> 10.33) is the
conjugate base of [bicarbonate][0] (pK<sub>a</sub> 10.3 still,
pK<sub>b</sub> 7.7), which is in turn the conjugate base of [carbonic
acid][2] or carbon dioxide: CO₃⁻⁻ → HCO₃⁻ → H₂CO₃.  So, I reasoned, if
you add baking soda to limewater, which is very basic, it ought to
buffer the basicity of the limewater, neutralizing some of the slaked
lime with its hydroniums and turning into carbonate, [which is in fact
the “causticizing” process by which lye was made before Solvay and
Leblanc][3].  But calcium carbonate is the (insoluble) product of the
setting of lime cement, and its crystallization should drive the
reaction toward consuming all the slaked lime.  So could baking soda
set lime cement in seconds to minutes instead of weeks?

The stoichiometry here should be 1:1:

> Ca(OH)₂ + NaHCO₃ → NaOH + CaCO₃ + H₂O

By my calculations that’s 74.1g Ca(OH)₂ to 84.0g NaHCO₃.  Ammonium
carbonate ought to work too, with the benefit that the reaction
product can be driven out as a gas by gentle heating.

An excess of baking soda ought to decausticize the resulting lye into
washing soda, also helping to drive the reaction forward by reducing
the solution pH:

> NaOH + NaHCO₃ → Na₂CO₃ + H₂O

Still, there’s the question of whether this would produce a useful
solid, or just a suspension of isolated crystals floating in water.
Arun Wagh’s phosphate cements book says that rapid reactions of
potential acid-base cements often results in cementation failing
because no continuous, amorphous gel phase forms, so cation sources of
lower solubility than slaked lime are preferred.

[0]: https://en.wikipedia.org/wiki/Bicarbonate
[3]: https://en.wikipedia.org/wiki/Alkali_manufacture#The_Lime_process
[2]: https://en.wikipedia.org/wiki/Carbonic_acid
[1]: https://en.wikipedia.org/wiki/Carbonate

(See also file `brick-contact-cement.md` for more applications of this
kind of thing.)

Baking soda makes weird wrinkly patterns on cast concrete panels
----------------------------------------------------------------

Unsurprisingly, I am not the first one to think of this.  [Jeff Gerard
recorded a video showing his ornamental results, saying][a]:

> An easy way to create natural stone-like texture in your concrete is
> to use baking soda in the molds. (...) 
> 
> The areas where the concrete touches the baking soda freezes up to
> hold the shape and texture of the baking soda.  This is because the
> sodium bicarbonate acts as an accelerator in the concrete, causing
> it to rapidly stiffen.  The surface of the concrete that touches the
> baking soda stiffens to form a crust that can wrinkle crack and
> deform during casting, and this is where the stone-like texture
> comes from.
> 
> I first learned about using baking soda with concrete to create
> these textures from a student back in late 2007 or very early
> in 2008.  I can’t really remember exactly when.  He was involved in
> the cast stone industry, and told me that it was an old-school
> technique used to create the look of weathered limestone, fossilized
> coral, and other “natural” stone textures. (...)
> 
> The surface of the concrete has stiffened up.  It, it stiffens up
> very quickly, you know, within seconds, and that crust doesn’t flow
> and blend into itself (...) It’s very thin, (...) and it’s not super
> hard, it’s, it’s more clay-like at that point; but it pushes and
> plows the baking soda, which collects, and as this bulge merges this
> way and this bulge merges this way.  There’s a line of baking soda
> that collects in between, and the two, you know, fronts of concrete
> don’t blend into each other, and that crust preserves all this
> texture.  So here’s a little cracking and some wrinkling, and I find
> this to be fascinating.

[a]: https://youtu.be/Rq4LnVc0Nec "How to Create Texture in Concrete with Baking Soda, uploaded 02022-08-09 by The Concrete Countertop Institute"

Gerard doesn’t seem to be thinking about the formation of lye or
washing soda.  A notable feature of his results is that the
baking-soda-treated areas of his concrete are quite light in color,
almost pure white, even though he was using a gray portland cement
mix.

Pozzolime sets up in a few hours with 1–4% baking soda
------------------------------------------------------

[Al-Attar, Al-Shathr, and Hadi][4] found that the inconveniently slow
setting time of their “pozzolime” mix could be shortened by adding
baking soda, which worked better than alabaster or cement kiln dust.
By “pozzolime” they mean lime plus a pozzolanic additive, as used
since the Neolithic, but in particular the patented formulation of
Kadum, Al-Attar, and Al-Azzawi from 02017, of 50% “hydrated lime”, fly
ash, and enough silica fume to get it to set (at least 20%).  1% to 4%
addition of baking soda produced dramatic reductions in initial
setting time, from 21 hours without to 5–8 hours with, but further
baking soda did not further decrease the setting time.  (Final setting
times were uniformly about 5 hours longer.)

[4]: https://iopscience.iop.org/article/10.1088/1757-899X/737/1/012063/pdf "doi:10.1088/1757-899X/737/1/012063 CC-BY 3.0"

This is a very different result from Gerard’s “stiffens up very
quickly (...) within seconds” from portland cement in contact with dry
baking soda powder, but maybe it’s compatible with his observation
that it’s “not super hard, it’s, it’s more clay-like at that point.”

They also did some experiments with alabaster, which of course sets up
in a few minutes (even at 4% in one of their experiments) but which I
have heard dramatically compromises the final strength of the
concrete.  I was curious whether baking soda might have a similar
deleterious effect.  Like Gerard, but bizarrely for a concrete
research paper, al-Attar *et al.* did not bother to measure the
strength of the materials they produced.

Up to 5% baking soda strengthens portland concrete
--------------------------------------------------

[Jang, Kim, Park, and Lee did find such an effect in 02015][5], but
only over 5%; up to 5% the baking soda strengthened the (portland)
concrete, but higher amounts started to weaken it.  They weren’t
interested in accelerating the setting time, though, and only reported
on “early strength development” after 5 days.  They found that the
baking soda increased the pH in the pore water, desirable for
passivating steel reinforcement.  (Also, free slaked lime can form
alabaster if exposed to dissolved sulfates.)  They also suggest that
the heat of the setting concrete might decompose the NaHCO₃ to Na₂CO₃,
which they say would react with the Ca(OH)₂, though I’m not so sure.
(I think it’ll react better if it’s still in NaHCO₃.)

[5]: https://doi.org/10.1016/j.conbuildmat.2015.07.121 "The influence of sodium hydrogen carbonate on the hydration of cement, Construction and Building Materials, Volume 94, 02015-09-30, pp. 746-749"

Baking soda makes concrete absorb CO₂
-------------------------------------

[A team at MIT and Harvard][10] rediscovered this pH-raising effect in
02023 and proposed it could be used to sequester CO₂, or at least emit
less of it, and issued a [press release][11].

[10]: https://academic.oup.com/pnasnexus/article/2/3/pgad052/7089570?login=false
[11]: https://cee.mit.edu/new-additives-could-turn-concrete-into-an-effective-carbon-sink/

Baking soda softens water for drilling mud
------------------------------------------

In the opposite direction, apparently [baking soda is used to
precipitate dissolved portland cement and other calcium ion sources
out of water that could flocculate bentonite drilling muds][6] or
acrylic ionomers:

> One pound (0.45 kg) of SODIUM BICARBONATE will remove 0.88 lb (0.4
> kg) of lime, which is roughly equivalent to 1.3 lb (0.6 kg) of
> cement.

[6]: http://www.rigmanufacturing.com/wp-content/uploads/2012/02/SODIUM-BICARBONATE.pdf

Decorative limestone-appearing concrete
---------------------------------------

[There’s an abandoned US patent application from 02010][7] on
sprinkling baking-soda-paste “clumps of various sizes” in a mold to
get “a decorative surface on a cast concrete tile” by creating
“complex voids in the tile’s upper surface,” and thus “producing a
surface texture similar to limestone.”  Possibly this was abandoned
because of the prior art mentioned in Gerard’s video.  Interestingly,
it claims that carbon dioxide gas is produced, forming “bubble
channels (68) extending deep into the dried concrete.”  It recommends
about 4–5 parts of baking soda to 1 of water by volume to make an
appropriately crumbly baking soda paste.  It also says, “It is known
in the art to spread fine baking soda powder over wet concrete to etch
the surface,” which is news to me and sounds like the *opposite* of
the immediate hardening effect I’m looking for and which Gerard
reports.

[7]: https://patents.google.com/patent/US20100219552A1/en

Super-rapid-setting concrete foam
---------------------------------

[An expired US patent from 02001, 6,485,561][8], proposes using sodium
*carbonate* to accelerate the hardening of concrete foam in order to
keep it from collapsing before hardening.
Amusingly the assignee was “XCELR8R
Inc.”  He discusses separate possibilities of using lime cement and
using portland cement; with the lime cement, he suggests adding the
“accelerator” *last*, after mixing the cement foam, which suggests
that in that case the reaction is very rapid indeed.  Apparently the
standard way to stabilize concrete foam was to mix in ground chicken
feathers.

He *also* suggests using sodium bicarbonate, or carbonate or
bicarbonate of any other metal such as lithium, potassium, calcium, or
magnesium.  (I’m pretty sure the last two of those won’t work at all.)
He says the preferred surfactants for the foam are non-ionic
surfactants like Surfonic SF-95 or (and I’m not sure this is
non-ionic) “tergitol NPG”.  He says that for the portland mix,
“typically” you add sodium carbonate at 0.1–15% of the weight of the
“cement used in the mix” and “more preferably” 0.5–3%

In his first example he used 600g of portland cement (or possibly
600mℓ, which would be more like 1500g) and 12g of sodium bicarbonate
for 2% by weight.  “The material set up in 2 min. as not to flow out
of a 16 oz cup when inverted.”  Cooool.

In his second test he used 18g of sodium carbonate instead (3% of the
weight of the cement) and reports that it set up in one minute instead
of two.

He did a bunch more tests; one that used sodium carbonate to 10% of
the weight of the cement set up in 15 seconds.  None with lime cement,
tho.

[8]: https://patents.google.com/patent/US6485561B1/en

Washing soda and ferric salts accelerate concrete setting
---------------------------------------------------------

[There’s a US patent 4,444,593 from 01983][9] on getting concrete to
set in at most about 3 minutes by adding sodium or potassium carbonate
and either the sulfate, chloride, or nitrate of the ferric ion to it,
with up to 4% accelerator by weight of the cement.  The patent says
that current shotcrete compositions use sodium aluminate, sodium
carbonate, and/or sodium silicate, all of which are dangerously
caustic, and also contains this gem:

> Sodium carbonate accelerators have poor initial set characteristics,
> and although they will reduce drying shrinkage, they also reduce the
> ultimate strength of the resulting concrete.  Similarly, concretes
> containing sodium silicate accelerators exhibit low ultimate
> strengths.

It also links other rapid-setting cement patents using other
chemistries, such as 4,066,469 (using finely ground cement and a
non-phosphorous acid salt of an alkali metal) and 2,918,385 (using
alabaster).

In their first graph, it seems like ferric sulfate alone was even more
effective, getting the cement to set up in a minute.  But they say
that such a cement “would serve no useful purpose” because the final
set takes too long.  Apparently using the carbonate as well helps with
this.  They prefer ferric sulfate to the other trivalent salts because
it’s less corrosive, probably to iron.

[9]: https://patents.google.com/patent/US4444593A/en

A recent open-access article with more comprehensive experiments
----------------------------------------------------------------

In [a super awesome paper from 02019][12], Wang et al. describe the
literature:

> Carbonates and bicarbonates with alkali were also among the
> most-used accelerators, such as Na₂CO₃ and NaHCO₃. (...) It can be
> seen that there exist conflicted findings on the influence of Na₂CO₃
> and NaHCO₃ on the setting time and physical properties of
> cementitious materials. It is necessary to carry out a comprehensive
> study on the effects of the two accelerators on the properties of
> cement paste and make a comparison between the two. In order to
> investigate and compare the effects of the two accelerators on the
> properties of OPC paste, the same amount of Na₂CO₃ and NaHCO₃ with
> 0%, 1%, 2%, 3%, and 4% weight of OPC were added into different mixes
> and the setting time and compressive strength at ages of 1, 7, and
> 28 days were studied.

Unfortunately, they, too, did no tests with simple slaked lime.

They did conclude that the compressive strength at 28 days was highest
with no accelerator (40MPa) and lowest (25–30MPa) with the highest
amounts of hardener (4%), with intermediate mixes providing
intermediate strengths, and 1% providing a fast set with no
detrimental effects on strength.  They liked the bicarbonate better.

[12]: https://www.mdpi.com/1996-1944/12/7/1033 "Comparison of Effects of Sodium Bicarbonate and Sodium Carbonate on the Hydration and Properties of Portland Cement Paste, by Yuli Wang, Fengxia He, Junjie Wang, and Qianku Hu, CC-BY"

Their theory about why high amounts of sodium reduce the concrete’s
final strength is that they terminate the Si-O-Ca-O-Si chains of
calcium silicate hydrate, similar to sodium silicate; the bicarbonate
weakens the concrete less because it contains less sodium.  This makes
me think that the bicarbonate or carbonate of ammonium might have
another special advantage: you might think that ammonium silicate
would be a soluble silicate similar to the silicates of sodium and
ammonium, but in fact it apparently isn’t stable at room temperature
without additional components, like aluminum or methylation.  So I
suspect you’d end up with ammonium hydroxide reaching its equilibrium
with ammonia, which would involve outgassing a lot of gaseous ammonia;
if sufficient egress was available, there would be no weakening
effect, permitting the use of ammonium carbonate or bicarbonate in
larger quantities than you can use sodium or potassium compounds.

Outgassing large quantities of ammonia of course has some practical
disadvantages, particularly for indoor use.  But it might also be
valuable if foamed "cellular concrete" is the desired product.

