My design for Hadix uses software transactional memory with lock-free
synchronization with strict priority scheduling and without using
memory remapping.  The objective of this is to guarantee real-time
responsivity on microcontrollers with a simple programming model,
something which has never been achieved, so I don't know if it will
succeed.  Secondary objectives include:

- reasonable efficiency on modern manycore CPUs, even those with MMUs,
  with throughputs of at least a third of what the hardware can
  possibly achieve;

- object-capability security sufficient to run mutually untrusted code
  in elfos in the same address space.

Not among its objectives:

- compatibility with existing code.

- extremely high absolute performance.

Elfos, apuros, flors (rosas and lirios), and llaves
---------------------------------------------------

In response to events such as keystrokes or network packets, Hadix
runs code in *elfos*, which are STM transactions.  Each elfo has a
priority or *apuro*; the system guarantees that an elfo of higher
apuro never has to wait for an elfo of lower apuro for more than some
very short time.  Information that isn't elfo-local is stored in
*flors*.  Each flor is identified by a *llave*, the possession of
which grants access to the flor.  There are three types of flors:

- *lirios blancos*, which are immutable arrays of bytes.
- *lirios azules*, which are immutable arrays of llaves.
- *rosas*, which are mutable arrays of llaves.

(All rosas are azules.  There are no analytic *a posteriori*
judgments, *pace* Kripke.)

Signos, oler, and besar
-----------------------

Two operations on llaves permit access to the data in the
corresponding flors, returning a *signo* which thereafter permits
efficient access until the elfo terminates, because it is represented
by a raw memory pointer.  *Oler* returns a read-only signo, and
*besar* returns a read-write signo.  Besar is only applicable to
rosas.

Either oler or besar applied to a rosa makes a mutable shadow copy of
the rosa, private to the elfo.  There are no shadow copies of lirios,
so oler is cheaper applied to a lirio, and its speed does not depend
on the lirio's size.

Although lirios cannot be changed, they can be destroyed.

Finar, renacer, and parir
-------------------------

If the elfo finishes execution successfully, a commit operation called
*finar*, all of these shadow copies atomically replace the earlier
version of the rosa.  During its execution it can also *parir* other
elfos, which will start to run if and when it finars.

The segregation between lirios and rosas permits Hadix to track
read-write access to rosas.  When an elfo olers a flor, Hadix adds the
flor to its oler-set; when it besars a rosa, Hadix adds the rosa to
its oler-set and its besar-set.  If the elfo attempts to finar, Hadix
verifies that none of the flors in its oler-set have been changed
since it started (by the finar of another elfo); if this fails, the
elfo's changes are discarded and it is restarted from the beginning, a
process called *renacer*.

On systems with more than one processor (hardware thread), it's
possible for an elfo with lower apuro to attempt to finar while a
higher-apuro elfo is still running.  (On a single-processor system,
this can't happen: Hadix pauses the lower-apuro elfo to give the CPU
to the higher-apuro elfo.)  If the lower-apuro elfo were to finar
successfully at that point, it's possible it might have besared a rosa
(or, as explained later, deleted a flor) the higher-apuro elfo had
olered or even besared.  This would renacer the higher-apuro elfo,
violating our desired real-time responsivity guarantees.  So in this
case the finaring of the lower-apuro elfo is delayed until there is no
active higher-apuro elfo.

Avoiding copying rosas: a dead end
----------------------------------

I tried for a long time to figure out how to avoid copying rosas in
the oler case, because if the elfo never besars the rosa the copying
is wasted work, and potentially a lot of it if the rosas are large.  I
kept finding a way to make it work, apparently, and then finding a
hole in my approach, and then finding a way to patch the hole, etc.,
finally ending up with a system that required a deadlock detector.

The first problem is that if an elfo first olers a rosa and then later
besars it, perhaps in another subroutine, it's essential that the
changes made via the read-write signo from besar be visible through
the read-only signo from oler.  Anything else would open the door to
very subtle bugs.  So, if the signo is just a raw machine memory
pointer, not copying the rosa in the oler case means that we can't
(always) relocate it in the besar case either.  In at least some
cases, we have to make a "backup copy" instead of a shadow copy, then
modify the old copy, in case we are olering it.

The second problem, then, is that another elfo might be olering (or
even besaring) the rosa, which means that it has a pointer to the
memory area you're going to be scribbling on, which could violate the
desired isolation property.  If the other elfo is of lower apuro, the
solution is simple: force it to renacer.  If it's higher apuro, you
can just block until it's done.

The third problem occurs if the elfo that has already olered the rosa
you want to besar has the same apuro.  Neither blocking nor restarting
the other elfo is a valid solution, because both would allow two elfos
of the same apuro to prevent the other from ever completing
successfully.  If the other elfo isn't currently running, you could
maybe cede the CPU to it so it can finish up, but that's really just a
form of blocking; that other elfo might cede the CPU right back to you
if it's trying to besar a rosa you have olered.

So at this point you have reintroduced blocking, though only among
elfos with the same apuro, and so you need to introduce a deadly
embrace detector which renacers one of the elfos in the cycle.  But
it's not obvious that this restores the guarantee of forward progress;
possibly the reborn elfo will deadlock with the same other elfos
again.

It occurred to me that if you have some stable order among the elfos
in the deadlock cycle, you could make sure to always kill the same
elfo if the deadlock recurs.  Then at least you don't lose the
progress you made in the other elfos.  But what if your stable victim
choice algorithm happens to pick the only elfo that isn't stuck in an
infinite loop?  We've lost the guarantee of progress we wanted.

There might be a way to rescue this approach with further epicycles,
but instead I'm giving up.  Instead, my solution is this: olering a
rosa copies it, just in case you later besar it.  This prevents an
unfinared elfo from renacering, delaying, blocking, or pausing the
execution of any other elfos.

In this context, the solution to the efficiency problem of large rosas
is to replace a rosa of N llaves with a lirio azul of N llaves with a
single-llave rosa pointing to it.

Soplar brotes
-------------

Elfos have elfo-local memory called *brotes*; when they terminate,
this memory is either deallocated or converted into flors.

When an elfo allocates memory, it is in the form of a brote, either
azul to hold llaves or blanco to hold bytes, referred to by an
associated signo.  An elfo can *soplar* a brote into a flor, thus
receiving a llave that it can store in a flor azul (a lirio azul or a
rosa); if it does not do so, the brote does not survive the elfo.  A
brote blanco can only be soplared into a lirio blanco, but a brote
azul can be soplared into either a rosa or a lirio azul.

I was thinking that the possibility of soplaring the brote into a
lirio requires a certain sort of linearity in signo handling to
prevent the read-write brote signo from surviving the conversion to a
lirio, which would violate the lirio's immutability.  But actually I
think that doesn't really matter: the signo can't survive the elfo,
and it's only if the elfo finars that the lirio becomes visible as a
lirio, that is to say, to other transactions.  It is important to
ensure that the same brote doesn't get soplared more than once,
because then you could have a lirio aliasing a rosa, but that can be
checked at finar time.

XXX should brotes also be categorized as flors?

Recycling garbage
-----------------

I wanted lirios to be immutable, but that seemed to require a
system-wide cyclic garbage collector, which seems very complex to
provide with guaranteed real-time responsivity --- not known possible,
if we're seeking efficiency.  My compromise is that lirios can't be
mutated, but they can be deleted.  (Rosas can also be deleted.)

This unfortunately means that Hadix needs to track which lirios an
elfo has olered, not just which rosas; but at least it doesn't need to
copy the lirios.

Storage allocation for flors is handled by hierarchically nestable
resource pools; these pools can have both maximum sizes and minimum
reservations.  This makes it possible to guarantee that a
system-critical elfo will have enough space to run successfully, even
if it has to create new lirios to replace old ones, regardless of the
load on the rest of the system.

I thought maybe I could get away without recording olering lirios in
the elfo's log, but then when it attempts to finar, there's no way to
detect whether one or more of its olered lirios has been deleted by an
elfo that finared previously.  In the usual case this would be fine
because the elfo that deleted the old lirio would also have replaced
the llave used to reach it (ultimately, by besaring some rosa that the
later-finaring elfo olered), and so the conflict would be detected and
handled by renacering.  But, in cases where a still-referenced flor
gets deleted, not recording lirio-olering would inevitably give rise
to violations of transaction serialization, where an elfo was able to
finar after having observed an inconsistent state of the store.

Still, the distinction between lirios and rosas avoids the need to
snapshot lirios when you oler them.

Even though there's no system-wide garbage collector, Hadix should
make it fairly easy to write a concurrent, parallel mark-and-sweep
garbage collector for a particular subsystem which is guaranteed not
to delay it unless it fails to keep up with the garbage load.  I don't
know how to use it to write an efficient generational collectors.

However, much of the performance benefit of an efficient generational
collector should be obtainable by ephemeral brotes: the death of an
elfo is a nursery collection of a generational collector.

In a single-processor system, the concurrency issues imposed by
deletion should be fairly minimal; when a deletion is finared, Hadix
can scan the runnable elfo queue to see if any of them have olered (or
besared) the deleted flor, preemptively renacering any of them at that
point, and then the space can be immediately recycled.  (Preemptive
renacering is potentially a useful optimization when committing an
update to a rosa, too, but XXX

Some other notes on performance
-------------------------------

Because the signo is implemented as a raw memory pointer and size,
once the elfo has the signo, each subsequent access to data within the
flor requires no interaction with the transaction system, and only
about 3-4 instructions: a bounds check and an indexed fetch or store.
Elfos within the same address space all share the same copy of a
lirio, and XXX

Error handling: morir and momias
--------------------------------

In addition to the transaction-ordering conflicts that cause an elfo
to renacer, a variety of primitive operations can fail.  This causes
the elfo to abort or *morir*, which has in common with renacer that
all of the elfo's changes to the transactional store are discarded.

When an elfo morirs, though, it is not restarted.  Instead, it
produces a *momia*, which encapsulates the information needed to debug
the problem, and a new error-handling elfo is started to process the
momia.

Big unknowns to tackle
----------------------

- Can the above approach (rosas, lirios, elfos, pools, and apuros)
  provide the responsiveness guarantees I'm looking for?  Can I
  quantify how small the preemption penalty is?

- What does it look like to write code in this environment for, for
  example, a Space Invaders game, an IDE with background compilation,
  a livecoding music environment, a toy Unix, a MUD, a web server with
  scripting, a web scraper, or a microcontroller system generating a
  real-time NTSC signal in software?  Will the flors turn out to be
  awkward to program with?

- Can I get enough performance out of a simple compilation strategy
  from a capability-secure bytecode?  Like, 53% of native within a
  single thread?  An interpreted implementation is probably fine for a
  lot of prototyping, but I don't want to paint myself into a system
  where it's impossible to get reasonable performance.  Two possible
  approaches occur to me:

    - Register allocation, static typing, and hoisting bounds checks
      out of loops.

    - Numpy-like vector instructions in the virtual machine, with a
      bit of pipelining or tiling of vector operations in order to
      increase arithmetic intensity.

- How common is renacer in practice?  That's too ill-defined; what I
  mean is, how easy is it in practice to write your code such that
  renacer doesn't become a big performance bottleneck?
