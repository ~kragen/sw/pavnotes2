There are some "conductive inks" that produce conductive traces, either
copper, silver, or graphite.  Some of these are just suspensions of metal
particles, while others are materials like silver oxalate which produce
the desired metal upon heating.  As far as I know, these all produce
relatively poor conductors with significant microphonics because the
resulting material consists of metal particles in fairly poor contact.

A third possibility is self-propagating high-temperature synthesis
("SHS").  

SHS of metals
-------------

SHS is usually used to produce exotic materials that are hard to make
by conventional means, but it can also make ordinary metals, and has
been used for that for a century and a half.  You ignite a metastable
multiphase mixture, for example of particles of different materials,
which can react exothermically to produce the desired metal, but does
not do so at working temperature.  The solid product is determined by
the composition of this preform; if it does not melt, its geometry is
also determined by the geometry of the preform.

In this way a continuous mass of metal can be produced; though, if it
does not melt, it will be porous.

A water-based paste of such a mixture can be extruded onto a refractory or
disposable support surface, onto the surface of a powder bed, or *into*
a powder bed that is built up layer by layer, any of which support the
paste in the desired geometry until firing.

When deposited in a powder bed, if the powder is of density similar to
the paste and the reaction products, you can produce a free-form solid
metal 3-D print.  In the case of building a circuit, circuit components
can also be placed inside the powder bed, but the ignition process will
subject them to higher temperatures than conventional soldering.

It is probably useful to remove the water from the paste before
ignition, either by baking it out or by allowing it to evaporate slowly.
Otherwise it will produce a lot of gas during firing, which may disrupt
the desired geometry.

In addition to circuits, 3-D printing metal offers the possibility
of printing optimized fractal geometry that provides much higher
strength-to-weight ratios in buckling than the bulk metal, and also
vastly improved heat exchangers.

Tenorite + aluminum
-------------------

Copper is relatively easy to reduce from its oxides; tenorite (CuO)
has a heat of formation of only -156 kJ/mol, so with its molar mass of
80 g/mol, the reduction reaction consumes only 2.0 MJ/kg.  Moreover,
tenorite is easy to produce by roasting copper in air and is 80% copper
by weight.  By mixing tenorite with some other metal with a stronger
affinity for oxygen, aluminum being the conventional choice (sapphire is
102 g/mol, -1675 kJ/mol, 16.4 MJ/kg, -557 kJ/mol O, 4.0 g/cc), we can
get a mixture that produces a solid copper mass with a moderate amount
of some relatively inert oxide.  Smaller particle sizes will produce
faster reactions and potentially higher spatial resolution.

> 3CuO + 2Al = Al2O3 + 3Cu

Stoichiometry is two aluminums (27 g/mol) per three tenorites: 82%
tenorite, 18% aluminum by mass.  The result is ideally 65% copper (64
g/mol), 35% alumina.  However!

Moderators
----------

This mixture is known to be dangerous to work with because of its
high energy density, which can boil the copper and aluminum and even
their oxides, disrupting the desired geometry and possibly killing
the philosopher.  In addition to using larger particle sizes, thinner
features, or a less enthusiastic reducing agent than aluminum, there
are several fillers that can be added to moderate this by adding more
thermal mass; for example, extra metallic copper can be included, or
for that matter extra aluminum, or pre-combusted alumina, or copper in
the form of cuprite (Cu2O, 143 g/mol, 6.1 g/cc), which is 89% copper by
weight and -170 kJ/mol, thus having not only a slightly higher energy
cost to remove the oxygen, but also half as much oxygen per weight
(and nearly per volume).

A disadvantage of cuprite is that synthesis en masse is more difficult
than with tenorite.  A 1:1 molar mix of tenorite with metallic copper
particles is easy to synthesize, slightly denser, and takes slightly
less energy to liberate the copper.

All kinds of other inert materials can also be incorporated to add
thermal mass and also affect the properties of the resulting material;
for example, alumina fiber, zirconia fiber, carborundum fiber, cubic
boron nitride, or tungsten carbide.  Relatively few materials are really
inert at the temperatures needed to reduce and sinter the metal, though.

Oxides and similar ceramic reaction products as functional fillers
------------------------------------------------------------------

Sometimes, aside from its strength, it is mechanically useful for
a material to be stiff.  Some metal oxides (and nitrides, borides,
carbides, oxynitrides, oxyborides, boronitrides, borocarbides, and
oxyboronitrides) are among the stiffest materials known, and a cermet
composite incorporating them will be stiffer than the cermet's metal
component alone.  Sometimes they also make it harder in, for example,
the sense of being more wear-resistant.  Alumina in particular is among
the hardest materials known.  So it may not be desirable for all purposes
to remove the oxide byproduct of this reaction.

In the limit, you can produce an object that is mostly or entirely
ceramic by this means.

Sintered particle removal
-------------------------

We can reasonably anticipate that, if the SHS preform is fired while
supported by a powder bed, whatever powder is used will sinter to
its surface, which may be desirable or undesirable; the surface will
consist of the sintered powder bed rather than the metal.  When it's a
problem, we can resolve it by using a powder that can be dissolved away
after firing by something mild enough to leave the metal untouched.
But the powder needs to be refractory enough to not simply boil away
upon firing, fill the nascent metal with gas, or mix itself into
the nascent metal in liquid form.  Most refractories are fairly inert
anywhere in the neighborhood of room temperature, and therefore hard to
selectively remove, but there are a few exceptions.  

### Magnesia alba ###

One outstanding case is philosopher's wool or magnesia alba (40 g/mol,
-601.6 kJ/mol, 15.0 MJ/kg, -602 kJ/mol O, 3.6 g/cc).  Quicklime is
another example.  Either of these can be dissolved readily by mild acids
such as vinegar; perhaps even boric acid would be adequate.

Using such mild acids is desirable to avoid etching the metal that has
been formed, but in the case of magnesia, bisulfate or dilute vitriol
will form extremely soluble epsom salt, and will at least not form oxides
with the metal.  (For quicklime this would probably not work well.)

#### Magnesium as reductant ####

Magnesia can also be formed as part of the reaction product by
substituting metallic magnesium for the aluminum as the reducing
agent.  There is a range of concentrations within which both the metal
produced and the oxide are continuous phases, forming interpenetrating
open-cell foams, which enables the removal of the oxide formed during
the high-temperature synthesis.  Alumina can be reactive in this way,
depending on which of its numerous phases it has assumed and how much
surface area is exposed, but generally it is quite unreactive and
difficult to remove.

### Quartz ###

Quartz flour would be a real headache to remove.  If previously roasted
to form cristobalite, the sintered cristobalite can reportedly be removed
at moderate temperatures with lye in water.  But it's probably less of
a headache to steer clear of silica entirely for this purpose unless
you want a fused silica surface.

### Boron ###

Another interesting possibility is using an elemental boron powder bed.
Boron is quite refractory, but B2O3 (boria) melts at a low temperature
and has a high vapor pressure, and its formation is pretty exothermic
(70 g/mol, -1254 kJ/mol, 17.9 MJ/kg, -418 kJ/mol O, 2.46 g/cc).  Thus
borothermic reduction is exothermic when applied to oxides of copper,
carbon, hydrogen, manganese, lead, and iron, though not magnesium,
aluminum, and silicon.  Perhaps this means that a little excess oxygen in
the SHS mix would coat its surface in boria rather than boron, and the
boria can then be baked off at a moderate temperature in a subsequent
process step, leaving just loose boron particles.  This is especially
appealing for reaction mixes that would otherwise produce oxides of
carbon: perhaps the boron can reduce the carbon and capture the oxygen,
preventing excessive gas evolution.  Conceivably this could even work
to absorb water vapor.

### Zinc oxide ###

Zinc oxide should work in a similar way to magnesia, but, in addition
to weak acids, it can also be removed with strong bases.  If zinc is
used as the reductant in place of magnesium, it produces less heat (and
cannot reduce very reactive metals like aluminum), and the zinc metal
is prone to boiling off.

Zinc is 65.38 g/mol and 7.14 g/cc, melts at 419.53 degrees, boils at 907
degrees.  ZnO is 81.38 g/mol and 5.606 g/cc, solid until 1975 degrees,
heat of formation -348 kJ/mol, LD50 240 mg/kg.  That works out to 4.3
MJ/kg and gives us -348 kJ/mol oxygen, much lower than magnesia or
alumina and lower than boria or silicon, but still higher than iron.

Zinc oxide is easily available as micron-sized particles because of
zinc's low boiling point; molten brass is constantly throwing off zinc
oxide nanoparticles.

### Soluble phosphates ###

I was thinking water-soluble metal phosphates like calcium dihydrogen
phosphate may be another option here, since phosphoric acid likes
to polymerize instead of boiling, but maybe that's not quite right.

Wikipedia says monocalcium di(dihydrogenphosphate) (234.05 g/mol,
2.220 g/cc, solubility 2 g/100 ml, popular leavening agent) instead
melts at 109 degrees and decomposes at 203 degrees, and even at room
temperature it tends to disproportionate to phosphoric acid and "dicalcium
phosphate" (monocalcium hydrogenphosphate).  As a leavening agent it's
not decomposed, but rather acts as an acid to neutralize baking soda.

The first step of polymerization is to form the dimer pyrophosphate,
and the pyrophosphates are usually more water-soluble than the plain
phosphates, but (di)calcium pyrophosphate is 3.09 g/cc, 254.053 g/mol, and
insoluble enough in water to use as a toothpaste abrasive, but soluble
in HCl and nitric; you get it by heating "dicalcium phosphate" past
240-500 degrees.  So (di)calcium pyrophosphate or "dicalcium phosphate"
would work, but they don't eliminate the dependency on mild acids.

Alloying copper: tin, lead, zinc, silver, and their oxides
----------------------------------------------------------

Copper happily alloys with many metals, including tin (to form bronze),
zinc (to form brass), silver, iron, and aluminum; and lead is commonly
added to these alloys despite being almost insoluble in copper.  In all
cases the alloy has lower conductivity than pure copper, and in most cases
a much lower melting point and significantly higher mechanical strength.
Magnesium has a strong enough affinity for oxygen to reduce any of
these metals from their oxide, and aluminum can reduce any of them
except aluminum.  So you can, for example, mix zinc oxide, tenorite,
and magnesium to produce a brass object upon firing (with pores full of
magnesia); or cassiterite, tenorite, and aluminum to produce a bronze
object upon firing, with pores full of sapphire, or more likely one of
its metastable allotropes; or tenorite and excess aluminum to produce
an aluminum bronze object.  (For example, ISO 428 CuAl8 aluminum bronze
is 7-9% aluminum, the rest copper.)

While copper doesn't melt until 1084 degrees, brass melts between 900
and 940 degrees, and among the numerous brasses are admiralty brass (69%
copper, 30% zinc, 1% tin), common rivet brass (63% copper, 37% zinc),
free machining C360 brass (61.5% copper, 35.5% zinc, 3% lead, 0.35%
iron), and rose brass (85% copper, 5% zinc, 5% tin, 5% lead).

Bronze is typically about 12% tin and melts around 950 degrees.  Tin is
118.710 g/mol and 7.265 g/cc, and melts at 231.93 degrees but doesn't boil
until 2602 degrees.  It is conventionally carbothermally reduced from its
oxide cassiterite, but I don't know what the oxide's heat of formation is.
XXX 

Silver is 107.8682 g/cc, melts at 961.78 degrees, and doesn't boil until
2162 degrees; Ag2O decomposes to silver and oxygen above 160 degrees
in air.  The metal forms a eutectic with 71.9% silver and 28.1% copper
used in vacuum brazing, but XXX what is the eutectic's melting point?
The oxide is 231.735 g/mol, 7.14 g/cc, melts at 300 degrees, and has
slight solubility in water.  Its heat of formation is almost nil, only
-31 kJ/mol, or 0.13 MJ/kg or -31 kJ per mole of oxygen.  That makes it
basically a highly compressed form of pure oxygen: even though it's
only 6.9% oxygen by weight, that's still 0.49 g/cc of oxygen, free
for the taking.  Its other remarkable property is its high electrical
conductivity, which is the reason for silver-plating connectors: they
can oxidize without losing their conductivity.

Lead is particularly interesting here because litharge is 92.9% lead
by weight, lead has the very low melting temperature of 327.5 degrees
(permitting lower synthesis temperatures), lead is fairly inert to acids
and commonly included in alloys to improve machinability, and litharge's
heat of formation is quite low (-219.41 kJ/mol, 223 g/mol, 1.0 MJ/kg,
219 kJ/mol O, 9.5 g/cc), and conductive vitriol-proof lead dioxide's
is even lower per oxygen (-274.47 kJ/mol, 239 g/mol, 1.1 MJ/kg, -120
kJ/mol O, 9.49 g/cc).  And although it doesn't dissolve in copper, 63%
lead and 37% tin is a eutectic alloy which melts at I think 183 degrees.

Copper is more malleable than its alloys, but their lower melting
temperature permits operating the reaction at a lower temperature,
which may reduce its tendency to crack during cooling.

Unlike in conventional metallurgy, the alloy composition can be
varied continuously across space by depositing different amounts of
two or more pastes in different places, permitting much more freedom
of case-hardening-like effects.  This requires that the resolution of
positioning be comparable to or smaller than the diffusion distance
during firing.

Organic reductants: hydrocarbon polymers
----------------------------------------

An interesting question is whether, instead of reducing with a different
metal that produces a solid metal oxide, we could use an organic
material such as paraffin or a UV-crosslinked polyester resin to reduce
the tenorite.  Not only would this allow us to eliminate the water from
the paste formula, it might allow us to make the object self-supporting
during firing (eliminating the surface-sintered powder problem), and
permit us to use other shaping techniques to initially print it, such
as FDM or stereolithography, whether with lasers or with LCD spatial
light modulation.

A priori this seems conceivable, though it will produce gas: with oxygen,
hydrocarbons generally burn to CO, CO2, and H2O, and at a high enough
temperature these will be entropically favored over the solid metal
oxides and hydrocarbons.  CO2 is 44 g/mol, -394 kJ/mol, 9.0 MJ/kg,
-197 kJ/mol O; H2O is 18 g/mol, -284 kJ/mol, 15.8 MJ/kg, -284 kJ/mol O.
Contrast this with cuprite's 170 kJ/mol O and tenorite's 156 kJ/mol O,
and it seems that the reaction would even be mildly exothermic, even after
subtracting off the moderate heat of formation of a typical hydrocarbon
like C17H36 (-480 kJ/mol, stealing about 27 kJ/mol for H2O or 28 kJ/mol
for CO2, or about 14 kJ/mol if both gases are produced.)

Addition of cheaper oxygen sources like oxides of lead or silver (XXX)
would make it significantly more exothermic.  Manganese dioxide has
an even higher density of oxygen per unit mass, and it's barely more
power-hungry than tenorite (MnO2 is 87 g/mol, -520 kJ/mol, 6.0 MJ/kg,
5.0 g/cc, and thus 260 kJ per mole of oxygen).

*Some* gas evolution may be useful to increase the propagation speed of
the reaction and keep it from petering out.

Hydrocarbons also offer the possibility of significantly lower reductant
masses in the mix, below 5%.  Consider cuprite and some arbitrary
long-chain hydrocarbon without many cross-links or unsaturated bonds:

> 3Cu2O + CH2 = 3Cu + CO2 + H2O

3 moles of cuprite is 429 g, one mole of CH2 is 14 g, so the
stoichiometric mix is only 3.1% reductant.  But I'm guessing the
stoichiometric flame temperature of that mix is probably too low to
propagate.

Zinc oxide in particular is known to have a self-sustaining exothermic
reaction with linseed oil and with chlorinated rubbers, but not
most rubbers, where it is commonly used as a vulcanizing agent and
anti-bacterial.

Thermite-printing iron
----------------------

In air, given the opportunity, iron burns to hematite (160 g/mol, -824
kJ/mol, 5.3 g/cc, -275 kJ/mol O, 5.2 MJ/kg).  Because -275 is so much
higher than -557, aluminum thermite routinely produces molten iron for
thermite welding, ever since Victorian times.

But for 3-D printing we don't need to melt the iron, just sinter it.
And we can include a bunch of carbon to make cast iron: it shouldn't
carbothermically reduce the aluminum at mere melting-iron temperatures.
In addition to reducing the melting point of the iron, I think this should
reduce the tendency of the product to crack from shrinking as it cools.

Iron is especially appealing for structural applications because it is
much cheaper than copper and much stronger; its magnetic properties are
sometimes also desirable.

Chromium
--------

The refractory chromia, viridian, or eskolaite, Cr2O3 (151.9904
g/mol, -1128 kJ/mol, 5.22 g/cc, and thus -376 kJ/mol O and 7.4 MJ/kg,
melts at 2435 degrees, boils at 4000 degrees) is nontoxic and can be
aluminothermically or silicothermically reduced to chromium, a much
harder and more refractory metal than iron or the copper alloys, as
well as more inert.  So this technique can be used to 3-D print porous
chromium, with reductants including aluminum, magnesium, silicon, and
probably boron, and that may be valuable.  Historically Hans Goldschmidt
in Germany in the late 01890s prepared the first carbon-free chromium
by aluminothermic reduction.

Chromium is 51.9961 g/mol and 7.19 g/cc; it melts at 1907 degrees and
boils at 2671 degrees.  Its Young's modulus is an astounding 279 GPa.

Printing in stainless steel (11+% chromium) may be more economical
than printing in pure chromium, and the result is more malleable and
less brittle.  It is, however, far less refractory.

Titanium
--------

Titanium is light (4.506 g/cc), mildly refractory (melting at
1668 degrees), very common (ninth most common element in the crust),
biocompatible, about as strong as steel (430 MPa UTS), and looks fantastic
when you anodize it.  It would be super great if we could print in
titanium this way, but for reasons I don't understand, we probably can't.

Titanium dioxide (79.866 g/mol, 4.23 g/cc in rutile form, -945 kJ/mol,
and thus -472 kJ/mol O and 11.9 MJ/kg) is widely available and fairly
refractory (melting at 1843 degrees).  The Kroll process for producing
titanium does not reduce it directly from titanium dioxide; instead the
dioxide is converted to the chloride through carbothermic reduction,
which is then distilled and reduced with magnesium at 1100 degrees
(with calcium in Kroll's original version).  This was preceded by the
Hunter process of reducing the chloride with sodium at 700-800 degrees.
Carbothermic reduction without chlorine instead produces titanium carbide,
not titanium metal, but I don't know why you can't just use aluminothermic
reduction or reduce it directly with magnesium.  In the Armstrong process,
the chloride is reduced by molten sodium; I don't know what distinguishes
this from the Hunter process.

There is also an "FFC Cambridge" process under development.

Additional inorganic reductants: silicon and iron
-------------------------------------------------

Quartz is 60 g/mol, -911 kJ/mol, 15.1 MJ/kg, -456 kJ/mol O, so silicon
should work well as a reductant for most metal oxides, better than boron
though not as well as aluminum or magnesium.  Unfortunately the resulting
oxide will be hard to remove, and I don't think it adheres well enough
to metal matrices to strengthen a cermet, though it will stiffen it.
In its amorphous form it also has an extremely low thermal coefficient
of expansion, lower than invar, so a cermet that's mostly fused quartz
might be a useful material for precision measurement.

For noblish metals like copper and lead (and of course silver), even
iron should work as a reductant, producing iron oxides which can be
removed with relatively mild acids.

Additional oxygen sources: water, ammonium dichromate, NaMnO4
-------------------------------------------------------------

If you need extra heat to overcome the activation barrier for
reluctantly-reducing metals like copper and especially iron, a possibility
is to include extra reductant in the mix and an additional source of
oxygen to burn it.  Water is a possibility here, perhaps locked up in
the hydration of something like aluminum chloride, magnesium sulfate,
sodium sulfate, or a polyacrylate, but it will produce hydrogen if you
successfully reduce it.

In addition to the commonly used gas-producing alternatives I will omit,
possibilities include ammonium dichromate (252 g/mol, -1795 kJ/mol, 7.1
MJ/kg, and I think 256 kJ/mol O though that assumes the ammonia survives
and the chromium is reduced to metal, neither of which is very likely)
and NaMnO4 (XXX what is its heat of formation?).  These are unfortunately
expensive and have some safety concerns associated with them.

Tape or wire
------------

In addition to 3-D printing paste for automated fabrication, or shaping
the paste by hand, you could make a roll of tape made of such a metastable
mixture, with a little organic adhesive on one side; or you can make the
mixture into a flexible coating on the surface of a wire which can be
bent by hand or with pliers, twisting it around more wire to form joints.
These materials can be fairly easily shaped by hand, or with the aid of
some kind of form or armature, into a wide variety of shapes; then they
can be ignited to synthesize the desired object in sturdy metal (or cermet).

Metal wood and foamboard
------------------------

Wood is mostly a thermoset carbohydrate foam reinforced with
unidirectional fibers, and this structure gives it an outstanding
strength-to-weight ratio and outstanding properties as a prototyping
and low-volume nanufacturing material: you can cut it with a knife,
nail it together, and jump up and down on the resulting structure.
Wood's anisotropy means you can make a wedge for splitting wood out of
the same wood.

### Metal wood ###

An appealing prospect is being able to make "metal wood" out of metal
foam reinforced in the same way with metal fibers.  The basic approach
to doing this is to make a hot piece of material containing solid metal
fibers far from their melting point, soft metal that's either plastic
because it's close to its melting point or actually liquid, and some sort
of gas that blows the soft metal into a foam; then you allow it to cool,
hardening the foam.  SHS is a promising way to produce such a piece of
material because it offers the possibility of heating up the whole thing
at once, rather than slowly waiting for heat to soak from the outside
to the inside while the foam's structure collapses.

Iron or chromium wires would work well for the fibers, while bronze,
aluminum, or aluminum bronze seems like it would work well for the
material between them.  Water is the easiest choice for the gas, perhaps
converting into hydrogen in the process.  It's important that the wires
remain solid during the expansion process or the expanding foam will
break them into pieces.  Chromium wires in iron foam might work too,
but the iron might dissolve them.

The fibers will constrain the lengthwise expansion of the object as it
processes, but it may encounter other constraints as well, which will
prevent it from expanding there.  In addition to simplifying bending,
this permits approaches to fastening that do not work with wood, such
as sticking a stick through a hole before you ignite it for expansion.

### Foamboard and other sandwich panels ###

Foamboard is a kind of sandwich panel: two faces of relatively stiff, hard
paper, separated by a lightweight rigid foam "web" that is nevertheless
much softer than the faces.  Such sandwich panels are ubiquitous in nature
and engineering: corrugated cardboard, skulls, drywall, iris leaves, etc.
The same SHS-foam approach as for "metal wood" can produce foamboard,
expanding foam between two sheet-metal faceplates, but because the
faceplates can cool themselves by exposure to a cooler outer environment,
is much less constrained in the materials it can use.

Before expanding the foam, the material can be flexible or even foil-like,
permitting fairly free-form shaping.  However, if not constrained to
this shape during expansion, it will tend to unfold itself as it swells.

### Washing ###

In these foam cases it may be possible to wash out unwanted oxide
products afterwards, but depending on the geometry it may be much more
difficult than in the 3-D printing cases.  If not, maybe you don't need
to produce gas to inflate the foam at all; you can just wash out the
solid oxide product.
