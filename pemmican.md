Pemmican is a traditional preserved foodstuff used historically by a
number of Native American tribes, which was then crucial to the French
*voyageurs*.  It consists mostly of fat, which is mixed with powdered
lean meat, and sometimes dried berries, especially for the
*voyageurs*.  It is shelf-stable over a span of years, possibly
decades, when saturated fat is used.  It can be eaten dry or, more
commonly, stewed with water, because it contains very little water of
its own.

This seems like an appealing invention to me for several reasons:

1. It’s probably balanced enough to survive on indefinitely, though
   [one paper from 01945 reports symptoms that sound like protein
   poisoning by the second day][8].

2. It’s portable, and I like portability.  A pemmican mixed by weight
   to be 45% fat, 45% protein, and 10% water and trace constituents,
   provides 5.9kcal per gram, so a 2000-kcal daily intake weighs 340g
   and occupies about 360mℓ.  It would be feasible to carry two
   months’ basal rations on your back if you didn’t have to carry
   anything else, or to pack them into a single 20ℓ bucket.
   
3. Unlike most trail food, it’s compatible with a ketogenic diet; 340g
   of the mix described above provides 150g of protein, and of its
   energy, 610kcal comes from protein.  This is less than the
   metabolic limit to deamination, so all that protein is available as
   energy, but plenty high enough to ensure adequate protein.  The US
   RDA is 0.8g/kg of body weight, which IIRC is higher than the needs
   of 90% of the study patients, so you can probaably safely cut the
   protein down even further.
   
4. It’s shelf-stable without refrigeration, making it a good food for
   disaster preparedness, since it doesn’t require electricity to stay
   good.

5. At least in theory it seems like it should be relatively cheap and
   easy to prepare in bulk.

To powder the meat you must first dry it.  Drying it to jerky dryness
is insufficient; attempting to crush up jerky in a mortar and pestle
just aggregates it.  It needs to be drier than that.

Suppose we go with 35% protein, 55% fat, and 10% water and trace
constituents, which gives 6.35kcal/g pemmican and 22% of calories from
protein, 110g protein per 2000kcal, weighing 100*π*g.  This mix might
be harder to keep solid!  But that seems like something you could
probably solve with minimally nutritive fibrous fillers like wheat
bran or psyllium husks, and maybe a coating of confectioners’ glaze
(shellac).

[8]: https://www.cabidigitallibrary.org/doi/full/10.5555/19461402085

Protein source pricing
----------------------

Butchers here give away beef fat for free or for a nominal price, so
we can start by considering the materials cost of a kilogram of
pemmican (three days’ rations) as the cost of 350 grams of meat
protein.

Right now (April 02024) beef has shot up to around $8000/kg (US$8/kg),
but the pork we’re eating tonight is $3600, and chicken wings are
$1000.

The USDA database says [“1 oz.” of chicken wing including the bone][0]
contains 1.93g of fat and 2.66g of protein.  In modern units, that’s
28.3g, so that’s 6.8% fat and 9.4% protein, the rest being water and
bone.  So $1000/kg, US$1/kg, is US$1 per 94g of protein, $10600 per
kilogram of protein, or US$3.70 per 350 grams.  So chicken-wing
pemmican should cost about US$3.70/kg.

[Pork chops][1] are 26g protein and 17g fat per 100g, thus 26% and
17%, thus containing 2.8× as much protein per gram, a number so much
higher that I suspect it might be counting the cooked weight rather
than the raw weight.  Nevertheless, the 4× cost advantage for chicken
wings is still dominant; pork pemmican would be US$4.85/kg by this
measure.

I bought a bag of “Armonia” brand instant dehydrated whole milk last
week, and since then I’ve been making yogurt from it.  It cost me
$6380 and contains 800g of dried milk, enough for 6.5ℓ.  According to
the bag, each 25g of the powder (for preparing 200mℓ) contains 10g of
carbohydrates, 5.1g of protein, and 7.3g of fat, totaling 22.4g; the
remaining 2.6g are presumably water.  That makes it 20.4% protein by
weight, a bit lower in protein than the pork chops, but more expensive
per kilogram, so it’s even more expensive as a source of protein than
pork is: US$13.70/kg of pemmican, if it’s even possible to make
pemmican from it.

I was thinking that yogurt, which is of course not a conventional
pemmican ingredient, has a couple of possible advantages as a pemmican
ingredient beyond its protein content:

- Milk contains free polar lipids and amphiphilic proteins which
  stabilize emulsions (such as milk), and perhaps these would increase
  the digestibility of otherwise fairly indigestible saturated fats.
- The lactic acid from the yogurt fermentation could act as an
  additional inhibitor to microbial degradation of the pemmican even
  if it absorbs moisture.

It does not seem plausible that the yogurt bacterial cultures would
survive in dormant form in the pemmican, so we should not expect such
pemmican to function as a probiotic.

Dried yogurt is [easily made in a food dehydrator][4], [a traditional
Turkish food named keş][5] or [kurut aʂi][6] (or “kisk” in Lebanon,
“kashk” in Iran, “jubjub” in Syria, or “kusuk” in Iraq, or “jameed” in
Jordan).  Sun-drying reportedly takes 2–7 days, the food dehydrator
several hours.  Industrially [spray-drying seems to work][7].

[0]: https://www.fatsecret.com/calories-nutrition/generic/chicken-wing-ns-as-to-skin-eaten?portionid=6262&portionamount=1.000
[1]: https://www.fatsecret.com/calories-nutrition/usda/pork-chops-(center-rib-bone-in-cooked-pan-fried)?portionid=58903&portionamount=100.000
[4]: https://www.instructables.com/Dehydrate-Yogurt/
[5]: https://turkishstylecooking.com/kes-dried-yogurt-recipe.html
[6]: https://www.dishesorigins.com/kurut-dried-yogurt-as-the-ever-preservable-cheese/
[7]: https://www.frontiersin.org/articles/10.3389/fnut.2023.1186469

Eggs are not cheaper than the meats.  They cost $4500 for 30 last I
checked, weighing about 60g each, and [12.6% protein, 9.9% fat][3],
which gives us $150 per egg, $2500 per kilogram of egg, $19800 per
kilogram of protein, and $6900 = US$6.90 per kilogram of pemmican.
Moreover, successfully dehydrating eggs requires desugaring them,
which sounds hard.  A potential advantage is that the yolk contains
even more emulsifiers than milk does; last time I made mayonnaise, I
used nine egg yolks (about 170g) to stabilize about a kilogram of
mayonnaise.

[3]: https://www.fatsecret.com/calories-nutrition/generic/egg-whole-raw?portionid=51772&portionamount=100.000

Possible vegetable protein alternatives include whey protein, gluten,
and soy protein isolate.  [Texturized soy protein][16] is $2260/kg,
which is *much* cheaper than any of the sources listed above, but it
contains a substantial amount of carbohydrate and is relatively poor
in protein quality.  [Pure gluten (search term “seitan”) costs
$12000/kg][17], costlier than chicken, but can probably be extracted
by hand from flour at a much lower cost.  And [whey protein costs
$44000/kg][18].

[16]: https://articulo.mercadolibre.com.ar/MLA-1383524053-soja-texturizada-extra-fina-x-1-kg-soja-deshidratada-carne-_JM
[17]: https://www.mercadolibre.com.ar/gluten-puro-de-trigo-1-kg-seitan/p/MLA21236137
[18]: https://www.mercadolibre.com.ar/suplemento-en-polvo-natural-whey-proteina-suero-concentrada-nacional-1kg/p/MLA34156300

Recipes
-------

Supposing I get the chicken meat off the bones, perhaps by boiling,
and [the meat is 27% protein and 19% fat.][11] Then, if I dehydrate it
to 8% humidity (with a little salt to keep it from spoiling while
drying), it will be 8% water, 54% protein, 38% fat.  So a 35%-protein
pemmican will be 65% dried chicken meat, the balance pure fat.

[11]: https://www.fatsecret.com/calories-nutrition/generic/chicken-wing-ns-as-to-skin-eaten?portionid=50393&portionamount=100.000

Lit
---

[Schillat reports][9] that 50%–60% of the pemmican might be melted
fat.  [Shay][10] says that mixing dried meat and fat for trail food or
preservation is a worldwide practice, and notes that “pemmican” is
from the Cree word “pimîhkâ”, but gives the names “bakkwa” (central
China and Malaysia), “kilishi” (Hausa), and “kavurmeh” (central
Africa).  He gives meat-to-fat ratios of 5:4 (Turtle Mountain
Anishinaabe), and 1:1 on [“a popular website”][12].  He also gives a
protein-to-fat ratio of 7.4:12.7 (37% protein).

[9]: http://monika-schillat.eu/texte/e_pemmican.pdf
[12]: https://www.wildernesscollege.com/pemmican-recipes.html
[10]: https://www.oxfordsymposium.org.uk/wp-content/uploads/2022/06/Shay-Formatted.pdf "Pemmican: An Ideal Trail Food.  Draft Version: Not for Citation or Attribution"

[Benjamin Alvord reports in 01883:][13]

> But the efficient remedy [to scurvy on Arctic expeditions] is to
> have pemmican made which is permeated with lime-juice, as
> recommended in the ‘Report of the surgeon-general of the navy for
> 1880’ (see p. 356). Gen. P. S. Wales said,—
>
> > “The indispensable necessity of lime-juice in the
> > sledging-parties, and the difficulties of carrying it, and
> > preparing it for use, induced me to suggest the propriety of
> > combining the juice and pemmican in the proportion of one ounce to
> > the pound of the latter. The pemmican is greatly improved in taste
> > and flavor, and will, I believe, be more assimilable. This is an
> > important modification, as there are persons who cannot eat the
> > ordinary article.”
>
> The article was prepared as proposed, and tried in Washington, and
> pronounced to be very palatable.

[13]: https://www.jstor.org/stable/pdf/1758327.pdf "Importance of Lime-Juice in the Pemmican for Arctic Expeditions, by Benjamin Alvord, Science, Oct. 5, 1883, Vol. 2, No. 35 (Oct. 5, 1883), p. 471"

Vitamin C would also serve as an antioxidant, scavenging free
oxidizing radicals, thus retarding the process of rancidification.

Today’s Tanka Bar contains equal quantities of carbohydrates and
protein and a much smaller quantity of fat, so it is not really
pemmican.

[Rodahl says in 01961][14]:

> It seems that by studying these extreme phenomena we might be able
> to get some idea of what the mechanism is behind it. In general it
> appears that your actual performance in a given situation may depend
> greatly on what you did before.  In other words, there is a factor
> of adaptation to be considered.  If you give a man pemmican and then
> give him normal food and then pemmican again, or send him out on a
> field trial on repeated occasions you will find that somehow he
> apparently adjusts or adapts to this pemmican ration.
>
> You may be acquainted with the fact that in some instances people
> collapse after three days of subsistence on pemmican.  I am talking
> about the kind that contains roughly 50 percent fat and 50 percent
> meat. The worst day is the third day.  If they can somehow survive
> that third day, they usually pull through.  Apparently this is a
> matter of adaptation.

[14]: https://apps.dtic.mil/sti/tr/pdf/AD0256350.pdf#page=187 "Human Performance in the Cold, by Kaare Rodahl, invited talk?, pp.189–192 of Advisory Board on Quartermaster Research and Development Committee on Foods, February 1961"

This sounds very much like ketosis and ketoadaptation.

In 01923 Clarke wrote a [“Critical Review”][15] of Vilhjalmur
Stefánsson’s “The Friendly Arctic” in the Bristol Medico-Chirurgical
Journal, saying of pemmican:

> Pemmican is a prepared and condensed food consisting of beef, fat,
> and dried fruits. Its most essential constituent is fat. Scott's
> supply contained 60 per cent., and was found most satisfactory.
> Stefansson considers it should contain at least 50 per cent. fat.
> Some of his pemmican, deficient in this respect, caused serious
> ill-health to the men and actual starvation to those dogs living
> solely on pemmican. Viewed from the standpoint that the diet was too
> rich in protein, the author states that “protein poisoning”
> occurred.  “This leads to nephritis or derangement of the kidneys,
> of which a common symptom is swelling of the body, beginning usually
> at the ankles.”

Clarke believes this is actually a symptom of fat deficiency, which is
close to how it’s understood today.

[15]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5057635/pdf/brmedchirj271640-0061.pdf "The Physiology of Diet and Hygiene at the North Pole, by Cecil Clarke, Bristol Medico-Chirurgical Journal 40(147), 52."
