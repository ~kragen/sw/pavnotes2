I was talking with Ben Sittler tonight about the Hadix virtual machine
for the Zorzpad autarkic personal computer, and he suggested a
particularly interesting approach to JIT compilation.

Relevant Zorzpad background
---------------------------

My plan has been to use bytecode interpretation with a stack-based
bytecode, more or less like Smalltalk-80.  The theory is that this
gives the best code density.  This is my foremost consideration
because the Apollo3 CPU I'm using in the Zorzpad only has 384KiB of
RAM (of which only 64KiB is zero-wait-state "TCM"; the rest is
slower).  The small RAM is a problem because I want a complete
self-hosting development environment on the device.  I think the RAM
size will be a much more restrictive limitation on that kind of thing
than the CPU speed (48 MHz at almost 1 ICP), because the CPU is fast
enough to read its whole RAM in about a millisecond, while the
corresponding number for minicomputers and human-interactive
microcomputers has always been close to a second.

One nagging issue is that writing a bytecode interpreter and a
bytecode-targeting compiler (or compilers) isn't enough to close the
loop.  If that's all you have, where does the bytecode interpreter
come from?  It has to be written in machine code (ARM Thumb-2, in this
case, because the Apollo3 is a Cortex-M4F), which means it has to come
from a different development environment of some kind, either a person
writing ARM machine code by hand or more practically some kind of
assembler or compiler.

Even the stupidest native-code compiler has the advantage that its
output can run without such a *deus ex machina*.  But I had written
off native-code compilers for the Zorzpad because even the best ones,
even for the code-densest architectures like Thumb-2, RV32C, and the
PDP-11, are significantly fluffier.  JIT compilation from bytecode
could be an option, because the amount of memory required for the
native code version can be limited, but it seemed like too much
complexity, in particular because evicting a native-code chunk from
the compilation cache seems to require on-stack replacement of return
addresses in the evicted code to prevent accidentally returning into
different code.

Compilation trampolines
-----------------------

Ben suggested a mechanism that seems like it should resolve this.
Rather than returning directly into the JIT-compiled native code,
return into an interpreter stub, which jumps to what would have been
the return address in the native code, if it's still valid.

The Cortex-M4F core has some branch prediction, but if I'm reading the
manuals right, the branch misprediction penalty is only 2 or 3 cycles.
So, totally confusing the branch predictor might be okay, even though
it would be utter death on a faster chip.  In that case the stub code
path might look something like this:
    
    pop {r4, r5, r6, pc}   @ at the end of a JIT-compiled subroutine
    @ this transfers control to a stub which does this:
    ldr ip, [r4]
    cmp ip, r5
    bne welp_recompile_the_damn_thing
    bx r6
    @ This transfers control to the appropriate place in its caller

This is probably two unpredicted branches (the pop and the bx) plus
four extra instructions, so probably about 10 extra cycles on the
return.  It also probably costs an extra several cycles on the call;
the simple way to do it would be something like

    ldr lr, =returning_stub
    add r6, pc, #2                  @ for some value of 2
    push {r4, r5, r6, lr}
    b other_subroutine_entry_point  @ predicted :)

This would probably be 9 cycles, but it assumes r4 and r5 already have
the right values, which seems unlikely: a pointer to a memory location
storing some information about the native-code subroutine being
executed, and the current value of that location.  More likely would
be something like this:

    ldr lr, =returning_stub
    ldr r4, [r7, #12]
    ldr r5, [r4]
    add r6, pc, #2
    push {r4, r5, r6, lr}
    b other_subroutine_entry_point

This is now something like 13 cycles.  Also, though, it's a shitload
of code to emit.

A better option is maybe to call a centralized recompilation
trampoline with the desired callee in, say, r0.  The call to the
trampoline and both the return into it and the happy-path return
*from* it are all normal, predictable jump targets.  Moreover, the
trampoline can then handle compiling the *callee* if necessary.  The
transfer of control to the callee will, however, essentially never be
predicted by the CPU's branch predictor.

(See file `jit-bootstrap.md` for more details.)

Predicted execution speed
-------------------------

Still, that seems like a reasonable price to pay.  Right now in my
prototype bytecode interpreter my estimate for call and return is 48
clock cycles (for a one-argument subroutine).  The trampoline
mechanism seems like it’ll be around 40.

The stupidest possible native-code compiler still eliminates the
per-bytecode dispatch overhead, which is especially appealing for a
stack-machine design, where you need twice as many VM instructions to
do anything as with the currently-more-fashionable register-machine
designs.  My current estimate is that bytecode dispatch in my
prototype takes about 7 cycles, which is a punishing overhead for
things like add (3 cycles with the stack stuff, 1 as a normal CPU
instruction) and local variable fetch (5 cycles, often 0 as normal CPU
instructions but 2 if an ldr).  And it might be enough.

Simple snippet concatenation compilation
----------------------------------------

Ben also pointed out that the ARM/Thumb ldm and stm instructions are
ideally suited for a certain particularly stupid kind of code
generation:

    adr r0, =template        @ PC-relative template
    ldm r0, {r0-r6}          @ load 28 instruction bytes from template
    orr r3, r7, r3, lsr #16  @ fill in missing 16 bits in template
    orr r5, r8, r5, lsr #8   @ fill in missing byte in template
    add r6, r9               @ apply runtime-determined offset to r6
    stm r10!, {r0, r6}       @ emit 28 bytes of code, update r10 pointer

This example might or might not be perfectly realistic, but if it's
all accessing zero-wait-state TCM, I think this six-instruction
sequence would run in 20 cycles and emit something like 10 Thumb-2
instructions.  This suggests that you wouldn't have to run the
JIT-compiled code very many times before the JIT compiler was only a
minority of the runtime.

Consider these two handlers from my prototype interpreter, which
implement respectively addition and local-variable fetch:

    00000020 <handler_add>:
      20:   f854 3d04   ldr.w   r3, [r4, #-4]!        @ pop operand stack
      24:   441d        add r5, r3                    @ add to TOS register
      26:   f816 3b01   ldrb.w  r3, [r6], #1          @ load next bytecode
      2a:   f857 f023   ldr.w   pc, [r7, r3, lsl #2]  @ look up in table & leap

    0000002e <handler_my>:
      2e:   f003 030f   and.w   r3, r3, #15           @ local var idx from byte
      32:   c420        stmia   r4!, {r5}             @ push operand stack
      34:   f858 5023   ldr.w   r5, [r8, r3, lsl #2]  @ index off r8 into TOS
      38:   f816 3b01   ldrb.w  r3, [r6], #1          @ (same dispatch as above)
      3c:   f857 f023   ldr.w   pc, [r7, r3, lsl #2]

The first of these would just be a literal six-byte sequence.  Being
six bytes is, I guess, a bit awkward for my hoped-for ldm/stm
approach; you could make it work, if you were planning on writing the
JIT compiler in ARM assembly, which I'm not, by having two versions
for the two different possible word alignments: an odd-alignment one
that jams f854 into the high part of a buffer register and then emits
the buffer register and 441d3d04, and an even-alignment one that emits
3d04f854 and buffers 441d in the buffer register, so either way you
end up emitting 54 f8 04 3d 1d 44.

The odd-alignment snippet-emitter would then jump to the
even-alignment dispatcher, and the even-alignment snippet-emitter
would then jump to the odd-alignment dispatcher.

The second one would emit a piece of code that runs in 4 cycles and
looks like this:

      c420        stmia   r4!, {r5}             @ push operand stack
      68f5        ldr r5, [r6, #12]             @ index frame pointer, or
      9d03        ldr r5, [sp, #12]             @ index stack pointer

In the even-alignment case this just blats out a single 4-byte
instruction pair; the odd-alignment case sticks c420 into the buffer
register, spits it out, and then buffers up the ldr instruction.  The
03 in 9d03 is just literally the index into the stack, in words.  So
the compilation handler, if written in assembly, might look something
like this:

    compile_my_even:
                 and r3, #0xf               @ extract local offset
                 ldr r4, =my_even_template  @ load 4-byte instruction skeleton
                 add r4, r3, lsl #16        @ compute fetch for correct local
                 stmia r5!, {r4}            @ append to code being built
                 b dispatch_even            @ process next bytecode
    my_even_template:
                 .word 0x9d03c420

This would take probably 7 cycles, plus another 7 cycles to dispatch
it originally, and then 4 cycles to execute it each time.  So
compiling it this way and then executing it once would take 18 cycles,
which is surprisingly close to the 12 cycles I estimate it takes now.
If it were executed twice or more, compiling and executing it would be
faster.

How typical this is of other VM instructions I don't know, but it
looks promising.

Other considerations
--------------------

The current virtual-machine prototype interpreter has to dedicate
several registers to things the JIT-compiled code wouldn't have to
worry about.  It would still need a TOS register and
operand-stack-pointer register, as shown above, but it wouldn't need
to keep a program counter in r6, a bytecode-table pointer in r7, a
return-stack pointer in r8 (it would just use sp), or a literal-vector
pointer in r9 (it would just use pc-relative addressing), and neither
would it need the temporary registers used for compilation.

This potentially frees up registers for doing things closer to the
application domain.  (The particular one I was thinking of today was
indexing a virtual memory page table.)

So very likely even the stupidest possible native-code compiler would
mean that frequently executed bytecode would run at something like a
third of the speed of decent code, rather than the one-twentieth I
currently expect, which would barely meet my runtime performance goal
of 60fps full-screen video output.  Though appealing, by itself, that
probably wouldn't be a good enough reason to use this approach.

Hybrid JIT/AOT compilation
--------------------------

The stronger reason is that such a bytecode-to-native-code compiler
closes the self-replication loop: it means that the compiler can
itself be written in bytecode, or rather, in whatever language
compiles to the bytecode.  This of course means that the
JIT-compilation won't be as fast, but plausibly it might still be
bearably fast.

What's needed here is merely to be able to generate the same code the
JIT compiler generates, except without the trampolines, and maybe a
little bit of extra assembly, and save it as the machine's boot image.

If we inflate the fairly handwavy 14-cycle compilation time above for
the local-variable-fetch bytecode by our handwavy factor of 3, it
becomes 42 cycles, which means that this approach to compilation
doesn't beat standard bytecode interpretation until the bytecode has
been executed 6 times: 72 cycles with standard interpretation, 66 with
stupid compilation.  This still seems eminently acceptable.

This seems like a pretty simple route to a self-sufficient development
environment with reasonable performance.
