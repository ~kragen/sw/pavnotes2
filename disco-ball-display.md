Suppose you’re looking at a disco ball that’s spinning at, say,
2000rpm, [typical for a cordless electric drill][0].  An array of
fairly directional blinking LEDs is reflected in the disco ball.  By
controlling when the LEDs are turned on, you can control which mirrors
of the disco ball are lit up when.  Can you get a useful computer
display out of this?  Like, one that's better than just looking at the
LED array directly?

[0]: https://www.protoolreviews.com/truth-about-cordless-drill-torque-speed/

In this form, perhaps not.  What you see in a single mirrored facet on
the disco ball sweeps across the LED matrix once per revolution, 33
times per second.  If the image of an LED as seen in the facet is
similar in size to the facet itself, you can typically only see a
given LED through a single facet at a time.  But the view in the facet
might sweep over a couple hundred LEDs while it’s in view.  The
potential benefit comes from the fact that multiple different facets,
in different vertical positions in your field of view, might sweep
over the same couple hundred LEDs, thus multiplying the effective
number of pixels on the display.

Suppose the angles of the facets are constrained to some angle from
parallel to the surface of the disco ball, like ±15°.  Still, by
distributing the LEDs over a sphere that goes all the way around the
disco ball (with a hole on one side for your viewing), we can display
visible light freely over the whole visible hemisphere of the disco
ball.

Suppose the disco ball subtends half a radian of your visual field,
comparable to a normal computer screen at a normal viewing distance,
thus appearing as a circle of 0.2 square radians, and can display a
quarter of a megapixel, which is roughly the lowest comfortable screen
resolution for text.  Then each pixel is 0.8 square milliradians,
giving it 1 milliradian in diameter.  This is large compared to the
limit of normal human visual acuity (nominally an arcminute or 0.3
milliradians) but not ridiculously so.  If the disco ball is
positioned at a distance of 500mm from your eyes and is 250mm in
diameter, each facet must be 0.5 mm in diameter.  Assuming the facets
are flat mirrors and the LEDs are a comparable distance from the disco
ball, the LEDs visible in the facets are on the order of 1 mm in
diameter.

The circumference of the disco ball is then 785 mm, and at its equator
its surface is moving 160 meters per second, half the speed of sound,
which is probably too much.  So let’s scale the system down by a
factor of 4: the disco ball is 125mm from your eyes, 62.5mm in
diameter, with 125-μm-diameter facets reflecting the images of
250-μm-diameter LEDs.  Furthermore, let’s cut the rotation rate to
only 1000rpm.  Now it’s only moving 20.4m/s, which is much more
reasonable.

Are we going to suffer diffraction problems?  A bit.  The [Airy-disk
radius][1] of 1.22*λ*/*d* for 555nm gives us a beam angle of 5.4
milliradians.  That means that yellow light from the incoming LED,
after reflecting off the facet, will be smeared across a
5.4-milliradian-radius cone, 10.8 milliradians in all; red light is
even worse.  But it doesn’t mean that the pixel you see subtends 10.8
milliradians at your eye; it means that the spot of light projected
onto your eye subtends 10.8 milliradians as seen from the disco ball,
or about 1.4 millimeters, which is still smaller than your pupil.  It
also means that the LEDs need to be spaced more than 1.4 millimeters
apart along their track.

[1]: https://en.wikipedia.org/wiki/Airy_disk

Speaking of tracks, I think the LEDs should be set in parallels of
latitude on the outer sphere that surrounds the disco ball, and the
disco-ball facets should be divided into zones of latitude, each zone
directed at a particular strip of LEDs.  If you can angle the facets
±15°, then you can angle their field of view ±30°, which means each
zone can cover (about) 60° of latitude on the disco ball; you can get
away with probably four zones and four latitude-lines of LEDs.

The most polar latitude-lines of LEDs will be at 22°30' from the pole;
if the “celestial sphere” on which they’re mounted is 156mm in radius,
those circles will be 156mm sin 22°30' ≈ 60mm in radius, so 375mm in
circumference.  Placing these LEDs 1.4mm apart gives about 268 LEDs in
this line, so we might have 1072 LEDs in all, in all four lines.

The least polar facets in these most polar zones, those at 45° of
latitude and inclined further toward the pole to show LEDs at 11°15'
(approximating the LEDs as infinitely distant, which is a terrible
approximation) are inclined at 5°7'30" from “horizontal”, which is a
lot less than the 15°; each one scans over the whole circle of LEDs in
half a turn, or about 15ms.

I think I’m starting to see a problem here: the LEDs are placed so
close together that their images in the facets have no space between
them.  If you want a quarter million pixels with only a thousand LEDs,
then you want to see nothing, a dark space between LEDs, in each facet
98.6% of the time, during which time the LEDs on each side of the dark
space are servicing other facets in the same zone.
