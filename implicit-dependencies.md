I wrote a Python embedded DSL for Zhegalkin polynomials the other
night, and I realized I had reached the threshold of something
interesting:

    >>> from zhegalkin import symbols, Var, one
    >>> u, v, w, x, y, z = symbols('u v w x y z')
    >>> sumbit = x ^ y
    >>> carry = x & y

So now we've defined a half-adder.  We can apply it by supplying
values for the variables:

    >>> sumbit(x=1, y=0), carry(x=1, y=0)
    (<zhegalkin.One object 1>, <zhegalkin.Zero object 0>)
    >>> sumbit(x=1, y=1), carry(x=1, y=1)
    (<zhegalkin.Zero object 0>, <zhegalkin.One object 1>)

But that application is actually being done by substitution and
partial evaluation, which has an even more interesting result: by
doing a substitution within the formula we can apply that same
half-adder to different arguments in order to compose it into a full
adder!

    >>> sum2 = sumbit(x=sumbit, y=z)
    >>> c2 = (carry | carry(x=sumbit, y=z)).distribute()
    >>> sum2.st()
    'x + y + z'
    >>> c2.st()
    'xy + xz + yz'

And those are indeed the correct Zhegalkin polynomials for the sum and
carry from a full adder.  (Remember that Zhegalkin addition is XOR,
not inclusive OR.)  We can chain those to another level of full adders
to get the Zhegalkin polynomials for the high-order bit and the carry
out from a two-bit adder with carry in:

    >>> s3 = sum2(x=c2, y=u, z=v).distribute()
    >>> c3 = c2(x=c2, y=u, z=v).distribute()
    >>> s3.st()
    'u + v + xy + xz + yz'
    >>> c3.st()
    'uv + uxy + uxz + uyz + vxy + vxz + vyz'

I'm pretty sure the sum bit there is correct, and I hope the carry bit
is too.  A little partial evaluation clarifies things a bit:

    >>> c3(x=0, y=0, z=0)
    <zhegalkin.Product object u * v>

So if the three low-order input bits are zero (the carry-in and the
two low-order bits of the addends) then the carry-out is only 1 if
both of the high-order input bits are 1.  That sounds right.  How
about if both the high-order input bits are 0, can the carry-out bit
be 1?

    >>> c3(u=0, v=0).st()
    '0'

No, good.  What if just one of them is 0?

    >>> c3(u=0).st()
    'vxy + vxz + vyz'

In that case both the other high-order input bit and two of the
low-order input bits need to be 1 to get the carry-out to be 1, which
sounds right.  What if we restrict to the case where x=1 and y=0?

    >>> c3(u=0, x=1, y=0).st()
    'vz'

Then the carry-out is 1 only when v is 1 and z is 1.  Okay, I'm
satisfied that this definition makes sense.

An interesting thing that's happening here is that every variable I
define thus is, in effect, a function, even though it has no argument
list.  `s3` here is a function of u, v, x, y, and z, because c2 is a
function of x, y, and z, and u and v are functions of u and v.  We can
read the definition `sum2(x=c2, y=u, z=v)` as a sort of
counterfactual, meaning, "the value sum2 would have if x were c2, y
were u, and z were v".  The result here is a *composition* of sum2
with c2 and a great deal of flexibility.

This relates to my thoughts on "principled APL": these "symbols"
introduced in the second line above are the "independent variables" or
"indices" or "coordinates" or "axes", while the other variables (the
formulas) are analogous to arrays.  In the Zhegalkin context, the
underlying values aren't arrays, and the operations on them are
Boolean rather than arithmetical or quantificational; but I think a
similar sort of interactive programming interface would probably work.
