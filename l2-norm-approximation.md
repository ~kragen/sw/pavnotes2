I was playing around a bit today with piecewise-linear approximations
of the ℓ² norm, especially for the ℝ² case, because I am interested in
multiplication-free things for small microcontrollers.

The two-dimensional 11-operation 2.2% `l2prox`
----------------------------------------------

I [came up with][0] the approximation (2(ℓ ᪲ + ℓ¹) + (5ℓ¹ ↑ 7ℓ ᪲))/11.2,
where:

- *a* ↑ *b* = (*a* if *a* ≥ *b* else *b*), the pairwise maximum
  operator;
- ℓ¹ = |*x*| + |*y*|, the usual ℓ¹ norm;
- ℓ ᪲ = |*x*| ↑ |*y*|, the usual ℓ ᪲ norm.

[0]: http://canonical.org/~kragen/sw/dev3/l2prox.c

In two dimensions, this seems to have a worst-case error of about
2.2%, approximating the circular contours of the exact ℓ² norm with
almost-regular hexadecagons.  Except for the final 1/11.2 scaling
factor, you can calculate the whole thing with addition, subtraction,
and bit shifts, without any multiplications, squarings, divisions, or
square roots.  And I think it’s still a norm, in the sense that:

- ∀*a*: ∀*b*: *f*(*a* + *b*) ≤ *f*(*a*) + *f*(*b*)
- ∀*a*: ∀*s* ∈ ℝ: *f*(*sa*) = |*s*|*f*(*a*)
- *f*(*a*) = 0 ⇔ *a* = 0⃗

The contours of this norm plotted against the ℓ² norm look like this:

<img style="width: 45em"
     alt="(plot of l2prox contours)"
     src="./l2prox-plot.svg"
/>

In C I calculated it in floating point as follows:

    double h = fabs(here.x), v = fabs(here.y), L8 = fmax(h, v), L1 = h + v;
    return (2*(L8 + L1) + fmax(5*L1, 7*L8)) / 11.2;

My procedure for computing it in 11 arithmetic operations with one
temporary register is untested, but is as follows.  It omits the final
scaling.

    // t0 := -x
    // x := t0 ↑ x           -- |x|
    // t0 := -y
    // y := t0 ↑ y           -- |y|
    // t0 := x + y           -- ℓ¹
    // y := x ↑ y            -- now we store ℓ ᪲ in y
    // x := t0 + y           -- and ℓ¹ + ℓ ᪲ in x
    // t0 := t0 + (t0 << 2)  -- 5ℓ¹
    // y := (y << 3) - y     -- 7ℓ ᪲
    // t0 := t0 ↑ y          -- 5ℓ¹ ↑ 7ℓ ᪲
    // rv := t0 + (x << 1)

This doesn’t quite correspond to any CPU instruction set’s repertoire
of operations; it’s close to ARM, but I think ARM still needs two or
three instructions to compute “↑”, and ARMs all have multipliers
anyway.  RISC-V needs a conditional jump for that, plus additional
operations for the bit shifts, and an additional temporary register.

This has a longest dataflow path length of 6 operations if we include
the negations.  In Graphviz:

    digraph approxl2 {
      rankdir=LR;
      {x -> "-x"} -> "|x|" -> {ℓ¹ ℓ ᪲} -> "ℓ ᪲ + ℓ₁" -> result;
      {y -> "-y"} -> "|y|" -> {ℓ¹ ℓ ᪲};
      ℓ¹ -> "5ℓ¹" -> "5ℓ¹ ↑ 7ℓ ᪲" -> result;
      ℓ ᪲ -> "7ℓ ᪲" -> "5ℓ¹ ↑ 7ℓ ᪲";
      result [label="2(ℓ ᪲ + ℓ¹) + (5ℓ¹ ↑ 7ℓ ᪲)"];
    }

<img style="width: 45em"
     alt="(node-link dataflow graph)"
     src="./l2prox.svg"
/>

Jon Atkins pointed out that 2(ℓ ᪲ + ℓ¹) + (5ℓ¹ ↑ 7ℓ ᪲) works out to
2(shorter + 2 longer) + (5(shorter+longer) ↑ 7 longer).  If you
already have *x* and *y* sorted so that *x* is the smaller of the two,
this gives the following 6-step procedure with one temporary:

- *t*₀ := *x* + (*y* << 1)
- *y* := *x* + *y*
- *y* := *y* + (*y* << 3)
- *x* := (*x* << 3) - *x*
- *y* := *y* ↑ *x*
- *t*₀ := *y* + (*t*₀ << 1)

But, in order to do that, we must first take the absolute values and
possibly exchange them, which takes 6 more operations:

- *t*₀ := -*x*
- *t*₀ := *t*₀ ↑ *x*  (now *t*₀ has |*x*|)
- *x* := -*y*  (use *x* as a temporary)
- *y* := *x* ↑ *y*  (now *y* has |*y*|)
- *x* := *y* ↓ *t*₀  (now *x* is the smaller; “↓” is the pairwise
  minimum operator, the counterpart to “↑”)
- *y* := *y* ↑ *t*₀  (and now *y* is the larger)

So this turns out to probably not be a win.

In three dimensions the error goes up
-------------------------------------

In three dimensions the error is about 9.5%, but only if we bump the
divisor up from 11.2 to 12.5; with 11.2 it's more like 18.6%.
Possibly tweaking the other parameters (2, 5, 7) could improve this a
bit.

The high-dimensional case?
--------------------------

One of the most important cases for calculating the ℓ² norm of a
vector on a microcontroller is measuring the power of a signal,
perhaps a signal produced by a DSP algorithm.  But I don’t think
anything similar to the above algorithm will work very well for that.
The trouble with just multiplying the ℓ¹ norm by a fudge factor, as if
you were a non-true-rms ac voltmeter, is that you underweight
extremely large values.  But the ℓ ᪲ norm *overweights* them.

It’s worth noting that the ℓ² norm isn’t a function of the *order* of
the vector’s components, just their *distribution*.  You can permute
the vector however you want without changing the norm.  (This is not a
property of norms in general, but it is true of the ℓ¹, ℓ², and ℓ ᪲
norms we’ve mostly been discussing.)

If we had a lot of memory, maybe it would make sense to *sort* the
samples in the signal by absolute value, which would make it easy to
compute their quantiles.  Then we could do some kind of computation on
some quantiles, because the lower quantiles of the signal contribute
very little to the ℓ² norm.  But if we had a lot of memory, we’d
probably have a hardware multiplier, too.

But it may make sense to do some kind of approximation of the top few
quantiles or of the overall distribution.  I don’t know, maybe a
max-heap or a histogram or something.

<link rel="stylesheet" href="http://canonical.org/~kragen/style.css" />
