An Advent of Code problem from last year posed the problem of
extrapolating polynomial sequences using Newton’s method of divided
differences, restricted to the case of evenly spaced samples, though
unfortunately without explaining any of that context.  Their example
vanishing triangles were:

    0   3   6   9  12  15  18
      3   3   3   3   3   3
        0   0   0   0   0

    1   3   6  10  15  21  28
      2   3   4   5   6   7
        1   1   1   1   1
          0   0   0   0

    10  13  16  21  30  45  68
       3   3   5   9  15  23
         0   2   4   6   8
           2   2   2   2
             0   0   0

    5  10  13  16  21  30  45
      5   3   3   5   9  15
       -2   0   2   4   6
          2   2   2   2
            0   0   0

The task given was to fill in additional leftmost and rightmost
columns.

Here’s a basic explanation with Sympy of why this works for the cases
of linear and quadratic sequences.  I’m writing out how to do the
calculation with Sympy, but they’re all simple enough to do by hand
(though evidently not simple enough for me to do in my head, because I
kept screwing them up):

Linear sequences
----------------

The first sequence (0, 3, 6, 9, ...) is just 3*n*.  If we evaluate
3*n* at two points *n* = *x* and *n* = *x*+1 we get 3*x* and 3(*x* +
1), which expands out to 3*x* + 3:

    In [35]: [(3*n).subs({n: val}).expand() for val in [x, x+1]]
    Out[35]: [3*x, 3*x + 3]

The difference between these two is just 3:

    In [36]: ((3*n).subs({n: x+1}) - (3*n).subs({n: x})).simplify()
    Out[36]: 3

This generalizes to any sequence of the form *p*₁ = *an* + *b*; the
*b* cancels out, as does the *an*, and we are left with just *a*:

    In [37]: p1 = a*n + b

    In [38]: p1.subs({n: x+1}).expand()
    Out[38]: a*x + a + b

    In [39]: p1.subs({n: x+1}).expand() - p1.subs({n: x})
    Out[39]: a

So if we calculate the [first forward differences of any linear
sequence][0] like this, which is to say any sequence *s* such that
*s*[*i*] = *ai* + *b* for some value of *a* and *b* we end up with a
row of constants (and those constants are *a*.)  So we can extend the
linear sequence forward and backward by extending the row of
constants.

[0]: https://en.wikipedia.org/wiki/Binomial_transform

Quadratic sequences
-------------------

Consider the case of quadratic sequences, like the second example (1,
3, 6, 10, 15, ...), *p*₂ = *an*² + *bn* + *c*.  If we evaluate this at
*n* = *x* + 1, we get *ax*² + 2*ax* + *a* + *bx* + *b* + *c*:

    In [40]: p2 = a * n**2 + b * n + c

    In [41]: p2.subs({n: x+1}).expand()
    Out[41]: a*x**2 + 2*a*x + a + b*x + b + c

Now, if we subtract off *p*₂ evaluated at *n* = *x* to get a formula
for the first differences, something interesting happens:

    In [42]: p2.subs({n: x+1}).expand() - p2.subs({n: x})
    Out[42]: 2*a*x + a + b

We only had one quadratic term, *ax*², and it cancels!  So we’re left
with 2*ax* + *a* + *b*.  And the *really interesting thing* about
*that* is that we already know what the vanishing triangle looks like
from there: the first differences of a linear sequence are constant,
as we showed above, and its second differences vanish (are zero).  So
now we know that the *second* differences of a *quadratic* sequence
are constant, so its *third* differences vanish.  And that's what we
saw in the example, which I first saw in Lancelot Hogben’s
introductory math book _Mathematics for the Million_, but which I
think dates back to at least Euclid:

    1   3   6  10  15  21  28
      2   3   4   5   6   7
        1   1   1   1   1
          0   0   0   0

As it happens, given the leftmost column here (1, 2, 1, 0), we could
calculate what *a*, *b*, and *c* have to be for *p*₂ = *an*² + *bn* +
*c*.  *c* is just the first element (1), but *b* is a little more
complicated; the first differences are 2*ax* + *a* + *b*, and in this
case *x* = 0, so we know *a* + *b* = 2, but we don’t directly get
either *a* or *b*.  The third one is a little easier: the second
differences (the difference of 2*ax* + *a* + *b* from *x* = 0 to *x* =
1, or *x* = 1 to *x* = 2) are 2*a*, as we saw before, so *a* = ½, and
b = 1½.  So our formula is ½*x*² + 3*x*/2 + 1.

Note that this is a simple system of linear equations; if we denote
the leftmost column here as *t* = [1 2 1] we have:

            c   = t₀
     a + b      = t₁
    2a          = t₂

Being a simple system of linear equations means that linear algebra is
full of algorithms for efficiently solving systems like this without
resorting to ad-hoc substitution.  This also means that the
coefficients of the quadratic polynomial and that leftmost column are
linear transformations of one another.  It also means that for *any*
sequence whose difference table vanishes on the third row like this,
we can *construct* a quadratic formula which will generate it if
evaluated at the integers!  So we have not just a proof but a
*constructive* proof that every sequence whose triangle vanishes on
the third row is quadratic, as well as vice versa.

More generally
--------------

You can observe that the sequence of a first-order polynomial (a
linear sequence) vanishes after two rows of first differences, and
that a second-order polynomial (a quadratic sequence) vanishes after
three rows.  And we can add that a zero-order polynomial (a constant)
vanishes after one row.  So you might conjecture that in general a
polynomial of some order *m* will always give rise to a vanishing
triangle of *m*+1 rows.  And this is in fact true, but this note is
perhaps already getting a bit long for its intended purpose.

Practical uses
--------------

Calculating a new column for the vanishing triangle is very easy, as
explained in the Advent of Code exercise; you just calculate the
prefix sum (`cumsum`) of the previous column:

               ... k₄    k₀ + k₁ + k₂ + k₃ + k₄
             ... k₃    k₀ + k₁ + k₂ + k₃
           ... k₂    k₀ + k₁ + k₂
         ... k₁    k₀ + k₁
       ... k₀   k₀
    ...  0    0

This means that you can calculate the next value from a polynomial
sequence of order *m* in *m* - 1 additions, without even needing any
multiplications.  Also, you only need *m* registers of memory.

This is how Babbage designed his Difference Engine to churn out
navigational tables (of numbers) using polynomial approximations to
some desired function.  (Babbage’s method was very slightly different
in order to permit half of the additions to be carried out in parallel
and to avoid reading and writing any register in the same clock
cycle.)  And, even today, it’s a speedup in any context where
tabulating evenly spaced values of a function is desired (as opposed
to merely evaluating it at some arbitrary point), especially when
multiplication is more expensive than addition.

When somewhat generalized (to Newton’s method of divided differences,
which can take input points with any spacing, instead of these evenly
spaced points) it is commonly used today for polynomial interpolation
(the problem of discovering a polynomial from some points on it).
Shamir’s “secret sharing” algorithm uses polynomial interpolation
(over a finite field, not over the integers or rationals) to split an
arbitrary piece of data into *M* shares of which any *N* < *M* are
sufficient to recover the original data; but, moreover, it is
*necessary* to have *N* of the shares to recover any information about
the original data.
