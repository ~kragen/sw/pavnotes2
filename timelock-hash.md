There's a "timelock encryption" construction where the idea is to
intentionally allow an attacker with enough resources to decrypt your
message, but only after enough time has elapsed.  I learned about it
from Gwern but I think he explained it wasn't his invention.

1. You generate, for example, N = 1048576 secure random numbers K[i]
of the size required for your encryption algorithm.

2. You produce truncated keys T[i] by chopping off the last M = 32
bits of each of K[i], so that an attacker who knows T[i] can find K[i]
with 2<sup>M-1</sup> work, and you calculate a redundancy-adding
function R(T[i]) on them, which adds enough redundant bits to T[i]
that it can be reliably distinguished from a random bit string.

3. You calculate ciphertexts U[i] = E(K[i-1], R(T[i])) for i > 0, U[0]
= T[0], which can be done in parallel.

4. Finally, you use the last key K[N-1] to encrypt your plaintext
E(K[N-1], P).

5. You publish the list of encrypted keys U and the encrypted
plaintext E(K[N-1], P).

An attacker can find K[i+1] by in 2<sup>M-1</sup> attempts by
appending M random bits G to T[i], L = T[i] || G, and they know they
have succeeded when their decryption result D(L, U[i+1]) = R(T[i+1])
is the redundant encoding an encryption key and not a random string.
They can do all the expected 2<sup>M-1</sup> tries in parallel if they
have enough hardware, but they cannot begin trying to crack K[i+1]
until they have done that much work.

So the recipient not only has to do N 2<sup>M-1</sup> times as much
work as the message sender, they also have to take at least N times
more *time* to do it, regardless of how much hardware they have
(assuming cracking the whole key K[i] without knowing T[i] is
infeasible, that brute force is the best attack on the cipher chosen,
etc.).  Key-stretching approaches like scrypt can make the individual
encryption operations arbitrarily slow and require arbitrarily much
hardware; you could, for example, require 1 gibibyte-hour of scrypt
work to derive an AES key from K[i].  With the parameters given, I
think a sender with a cluster of 65536 16-gibibyte nodes with adequate
GPU would need 1 hour to generate a message; a recipient with 131072
or more equivalent nodes would then need some 1048576 hours, or about
120 years, to decrypt it.

This is a nice scheme.  If you set M = 0 the receiver needs to do the
same amount of work as the sender but cannot take advantage of any
parallelism, making highly parallel receivers no faster than serial
ones.  It doesn't seem to provide a way for a serial sender to encrypt
a message in a way that will take longer for an arbitrarily parallel
recipient to decrypt than it took the sender to encrypt; the sender's
parallelism corresponds precisely to the receiver's serial bottleneck.

You can do a somewhat similar construction in secure hashes by
artificially weakening a backward-chained secure hash function, as
used in OPIE (S/Key) one-time passwords.

In OPIE passwords, as I recall the idea, the prover who wants to prove
their identity starts with some secret S and computes some sequence of
secure hashes K[0] = S, K[i > 0] = H(K[i - 1]), and then gives, say,
(K[N = 1024], N) to a recipient who wants to verify their identity
later through a secure channel.  In the future when the prover wants
to send a message to the verifier over a channel that is subject to
eavesdropping but not to malicious editing of your messages, they
decrement N and include (K[N], N) in the message to authenticate it.
The verifier can compute H(K[N]) and verify that it matches their
previously memorized K[N+1]; if some messages are dropped they may
have to compute H(H(K[n])) or H(H(H(K[N]))) or something, but they can
still verify it.  The state for both the prover and the verifier is
very small; the verifier only needs to store (K[N], N), and the prover
only needs to store (S, N), although storing an additional logarithmic
amount of information allows the protocol to execute in constant time
rather than exponential time.

(The context of OPIE was the 01990s internet when Joncheray's "Simple
Active Attack on TCP" had not been deployed or perhaps even conceived;
strong encryption was subject to export controls from the US, but
strong cryptographic signatures and other authentication methods were
fair game; and hand computers were not yet a thing, and even laptops
were an expensive luxury.  A fellow I knew carried a list of S/Key
passwords on a card in his wallet so he could log in remotely, over
the unencrypted internet, to his [presumably non-classified] account
at Sandia National Labs.  Once the telnet connection was established,
it was supposed that an eavesdropper wouldn't be able to take it over
without somehow knocking the machine he was sitting at off the net,
but it was desired to prevent them from opening a new telnet
connection of their own with the snooped password.  Another context
where this sort of thing might make sense is where the eavesdropper
receives the message too late to interfere with it, perhaps because it
took them too long to decrypt it.)

You can extend this by calculating K[i > 0] = H(K[i - 1] || A[i]) at
the beginning and storing the A[i] on the prover and sending (K[N],
A[N], N) to authenticate the message.  The A[i] can be meaningless
nonces or something more interesting, such as the root of a Merkle
tree or a one-time-use Merkle signing key, and I think this
construction is actually from Merkle's dissertation in the 01970s.

You could perhaps intentionally weaken this protocol so that an
attacker can, with enough effort, compute a different predecessor key,
but much more slowly than the original prover.  This is only slightly
different from the basis of Bitcoin's proof of work, but it comes with
a built-in backdoor: the original prover has efficiently precomputed a
"blockchain" extending far into the future, so at any time they can
produce a longer "blockchain" than the brute-force attackers could.
Obviously basing a cryptocurrency on this would be a bad idea, but
maybe it's useful for something, like a dead-man switch.

(I don't yet see how to apply the series/parallel inversion to the
OPIE construction; it's harder to compute backwards than forwards just
because the hash function is harder to compute backwards than
forwards, not because the prover can do something in parallel that the
attacker must do serially.)

What I really had in mind was is a way to publish human-readable
content hashes for things like secure library imports; see file
`secure-library-imports.md` for details.
