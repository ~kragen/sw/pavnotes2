[Aluminum][2] is 2.7g/cc, 26.9815384g/mol, and 2.65e-8 Ωm.

Household aluminum foil is 10μm (one of the thinnest things you can
buy at the supermarket), so a 2.65mm-wide strip 1m long measures 1Ω
and, at 26.5mm³, weighs 72mg.

The standard enthalpy of formation of Al₂O₃ (101.96g/mol) is
-1675.7kJ/mol, which works out to -837.9kJ/(mol Al) or 31.053MJ/kg,
which [accords with the Wikipedia table][0].  This is an annoyingly
high energy density for electrodeposition, but fantastic as a solid
fuel for aluminum-air fuel cells.

Aluminum’s [thermal conductivity is 237W/m/K][1], almost as high as
copper’s 401W/m/K, potentially making aluminum a usable electrode for
things like spot welding and electric discharge machining (EDM)
despite its depressingly low melting point of 660.32°.  Together with
its high thermal conductivity, its high boiling point of 2470° makes
it an appealing high-temperature coolant, but it’s fairly corrosive.

Because its oxide’s enthalpy of formation is -558.6kJ/(mol O), while
water’s is only [-285.83kJ/mol][3], aluminum’s reaction with water is
exothermic, though normally its hard, adherent, refractory oxide film
(m.p. 2072°, b.p. 2977°) forms a formidable kinetic barrier.  And by
reacting with water, you’d only get half the energy you’d get from,
say, an aluminum-air battery; the other half bubbles off in the form
of hydrogen.  Having an affinity for oxygen much stronger than
hydrogen’s, a feature also evident in their standard electrode
potentials, explains why electrorefining of aluminum is not done
hydrothermally.

Vaporizing aluminum with heat, for example to poke holes in aluminum
foil with a spark, requires a lot of heat.  I haven’t bothered to look
up how its specific heat changes with temperature, but its enthalpy of
fusion is 10.71kJ/mol (396.9kJ/kg), its enthalpy of vaporization is
284kJ/mol (10.5MJ/kg), and at room temperature its molar heat capacity
is 24.2J/mol/K (.897J/g/K), which crudely multiplying by ΔT = 2470K
gives us another 2.2MJ/kg to heat it to its boiling point.  So you
need something in the neighborhood of 13MJ/kg, almost all consumed by
the enthalpy of vaporization, or, in more useful terms, 13J/mg.

Vaporizing it seems like it could be dangerous for four reasons:
because any plasma at 2470° radiates a lot of UV, because aluminum is
so corrosive, because the volume of an ideal gas at that temperature
would be fairly large (225ℓ/mol, which for aluminum vapor at 1 atm
works out to 120g/m³, <⅛ the density of air), and because in air it
would tend to rapidly consume the oxygen.  120g/m³ at 1 atm works out
to 850kJ/kg that the aluminum vapor would have to do on the
environment, which is very small compared to the 10.5MJ/kg enthalpy of
vaporization itself.  But, as said above, aluminum vapor has other
disadvantages as a heat-engine working fluid.

At 660°, a black body would emit 43kW/m², almost entirely in the MWIR,
but aluminum has fairly low emissivity; I think that without paint it
will emit more like 4kW/m².  The hypothetical 1Ω foil resistor
mentioned above would have an area of 0.00265m² on each side and would
therefore emit about 20 watts at this temperature, half on each side.
It follows that in a vacuum it will eventually melt if subjected to
more than about 20 watts, which would be about 4.5 amps at 4.5 volts.
Because it’s so thin, convective cooling might permit it to handle
much more power than that.  Its 72mg mass gives it about 65mJ/K of
heat capacity, so if perfectly insulated, heating it to the melting
point at that current would take on the order of two seconds.

The origami malleability of aluminum foil could be very valuable for
ECM machining form tools.

[0]: https://en.wikipedia.org/wiki/Energy_density#In_chemical_reactions_(oxidation)
[1]: https://en.wikipedia.org/wiki/List_of_thermal_conductivities
[2]: https://en.wikipedia.org/wiki/Aluminum
[3]: https://en.wikipedia.org/wiki/Properties_of_water
