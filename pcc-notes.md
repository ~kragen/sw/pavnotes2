For Hadix on the Zorzpad I'm thinking about maybe using Steve
Johnson's pcc compiler to compile C to a compact bytecode.  [WP
says][0], "On December 29, 2009, pcc became capable of building a
functional x86 OpenBSD kernel image," so it's a reasonably capable
compiler; it's been updated to support C99, though not C11, C17, or
C23.  However, the vast majority of C code out there is C89 or C99, in
part because Microsoft keeps not updating their compilers.

[0]: https://en.wikipedia.org/wiki/Portable_C_Compiler

pcc is 103446 lines of C source, if we believe cloc, which should be
about 300K of bytecode, if we believe my handwaving estimates; that's
small enough to fit in the Zorzpad's memory but maybe not small enough
to also fit data in there.

Of this, 53857 lines is in the `arch/` directory, which contains
backends for aarch64, amd64, arm, hppa, i386, i86, m16c, m68k, mips,
mips64, nova, pdp10, pdp11, pdp7, powerpc, riscv, sparc64, superh, and
vax.  It's probably fairly rare that I would use any of these, since
I'd be compiling to the Hadix bytecode, writing my own backend.  The
ARM backend in particular is 3987 lines of code, and the nova backend
only 1459, so writing a new backend seems relatively approachable.

Of the remaining 50 KLOC, 8900 lines are in the `f77/` directory,
which think are not used when compiling C.  This leaves a bit over 41
KLOC, plus a hypothetical 4000 KLOC I haven't written.  45 KLOC should
be around 135 KB of bytecode, which seems like it should fit well into
the 384KiB on the Zorzpad's Apollo3.  In fact, it probably fits even
if I compile it for Thumb-2 directly, but then any compiler bugs would
be likely to crash the system or corrupt data.
