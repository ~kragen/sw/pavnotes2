There's a [Scientific American article][0] about a base-20 number
system invented by a group of Inupiat schoolkids (each an Iñupiaq,
according to their declension), [part of Unicode since Unicode 15.0 in
September 02022][1] after a [successful proposal][2].  It's
particularly simple and logical; each base-20 numeral is a single
sequence of strokes made without lifting the pen.  In ASCII art:

           1: \      2: \/     3: \/\    4: \/\/

    5: /   6: /      7: /      8: /      9: /
              \         \/        \/\       \/\

    10 >   11 >      12 >      13 >      14 >
              \         \/        \/\       \/\

       /      /         /         /         /
    15 /   16 >      17 >      18 >      19 >
              \         \/        \/\       \/\

[0]: https://www.scientificamerican.com/article/a-number-system-invented-by-inuit-schoolchildren-will-make-its-silicon-valley-debut/
[1]: https://www.adn.com/alaska-life/2022/11/06/numerals-invented-by-kaktovik-students-can-now-be-used-digitally/
[2]: http://www.unicode.org/L2/L2021/21058-kaktovik-numerals.pdf

The 0 looks vaguely like ɣ, the IPA symbol for a voiced velar
fricative; ɤ, the IPA symbol for a close-mid back unrounded vowel; or
➰, which is supposedly called the "kururi"; and they say it's a
combination of the 5 and the 1:

      \/
      /\
     (__)

The Unicode proposal says the bottom part of the character (with the
ones) should be square, with a golden rectangle on top for the fives,
forming an overall golden-rectangle shape.

These are obviously much more logical than the Western Arabic numeral
system I typically use, though you could stop at 9 instead of 19 if
you're trying to match a base-10 linguistic system.  What's less
obvious is that they're also probably more *efficient*.

It seems plausible that the number of strokes in different directions
your pen must make to write a character should be roughly proportional
to the time required; without validating this hypothesis, we can
calculate this for this numbering system for digits 0 to 5 as:

    >>> s0 = [3, 1, 2, 3, 4]

Then we can derive the stroke counts for numbers up to 19:

    >>> s1 = s0 + [n + m for n in [1, 2, 3] for m in range(5)]
    >>> s1
    [3, 1, 2, 3, 4, 1, 2, 3, 4, 5, 2, 3, 4, 5, 6, 3, 4, 5, 6, 7]

Other numbers up to 299 require two digits, one of the number mod 20
and the other of the number div 20:

    >>> s2 = s1 + [n + m for n in s1[1:] for m in s1]
    >>> len(s2)
    400
    >>> s2
    [3, 1, 2, 3, 4, 1, 2, 3, 4, 5, 2, 3, 4, 5, 6, 3, 4, 5, 6, 7,
     4, 2, 3, 4, 5, 2, 3, 4, 5, 6, 3, 4, 5, 6, 7, 4, 5, 6, 7, 8,
     5, 3, 4, 5, 6, 3, 4, 5, 6, 7, 4, 5, 6, 7, 8, 5, 6, 7, 8, 9,
     6, 4, 5, 6, 7, 4, 5, 6, 7, 8, 5, 6, 7, 8, 9, 6, 7, 8, 9, 10,
     7, 5, 6, 7, 8, 5, 6, 7, 8, 9, 6, 7, 8, 9, 10, 7, 8, 9, 10, ...

The last number, 399, requires 14 strokes (plus lifting the pen
between the digits.)  The average is 7.15 strokes per character:

    >>> sum(s2) / len(s2)
    7.15

However, that's sort of a worst case, because we stop right before the
number of strokes falls back to 7.  If we are writing the number up to
some random number less than 400, the average will always be less:

    >>> for i in range(10):
    ...      print([round(sum(s2[:n])/n, 2) for n in [random.randrange(400)]][0], end=' ')
    ...
    6.21 6.08 2.0 3.2 4.66 4.77 6.15 6.71 2.38 5.68 >>> 

The corresponding Western Arabic numerals have fewer strokes; as with
the Kaktovik 0, I'm counting loops as triangles:

    >>> a = [3, 1, 3, 4, 3, 4, 3, 2, 4, 3]

But they also contain less information; a base-10 numeral is 3.32 bits
of information, while a base-20 numeral is, naturally enough, 4.32.
These digits average 3.0 strokes and 1.11 bits per stroke, while the
Kaktovik digits average 3.7 strokes and 1.18 bits.  This suggests that
the Kaktovik numerals will be more efficient.

We can compute how many strokes are needed for the Western Arabic
numerals up to 400:

    >>> a100 = a + [n + m for n in a[1:] for m in a]
    >>> a100
    [3, 1, 3, 4, 3, 4, 3, 2, 4, 3, 4, 2, 4, 5, 4, 5, 4, 3, 5, 4,
     6, 4, 6, 7, 6, 7, 6, 5, 7, 6, 7, 5, 7, 8, 7, 8, 7, 6, 8, 7,
     6, 4, 6, 7, 6, 7, 6, 5, 7, 6, 7, 5, 7, 8, 7, 8, 7, 6, 8, 7,
     6, 4, 6, 7, 6, 7, 6, 5, 7, 6, 5, 3, 5, 6, 5, 6, 5, 4, 6, 5,
     7, 5, 7, 8, 7, 8, 7, 6, 8, 7, 6, 4, 6, 7, 6, 7, 6, 5, 7, 6]
    >>> a400 = a100 + [n + m for n in a[1:4] for m in a100]
    >>> len(a400)
    400
    >>> sum(a400)/400
    7.7

So Western Arabic numerals up to 400 require an average of 7.70
strokes per number, while the Kaktovik numerals only require 7.15.  If
we instead pick random stopping numbers, the Kaktovik numerals nearly
always win, typically by 5–9%:

    >>> for i in range(10):
    ...     n = random.randrange(400)
    ...     print(n, round(sum(s2[:n])/n, 2), round(sum(a400[:n])/n, 2), round(sum(a400[:n])/sum(s2[:n]), 2))
    ... 
    255 6.21 6.66 1.07
    99 5.6 5.7 1.02
    85 5.24 5.58 1.07
    142 5.5 5.79 1.05
    73 4.86 5.52 1.14
    268 6.3 6.79 1.08
    93 5.39 5.66 1.05
    304 6.63 7.03 1.06
    301 6.65 7.03 1.06
    376 6.92 7.54 1.09

This is a somewhat dubious way to assign the probabilities for
different numbers, but it seems to suggest that the Kaktovik numerals
will always win for any reasonable weighting.

The Mayan base-20 numerals are more immediately comprehensible than
the Kaktovik numerals, but I think they are harder to write and more
error-prone to read.

A way that base 20 is worse is that the multiplication table is
substantially more unwieldy to memorize; however, if you can overcome
that, both multiplication and division become more practical.  For
example, numbers between 1000 and 8000 have four base-10 digits but
only three base-20 digits, so multiplying two of them in the usual way
in base 10 will require 16 multiplication-table lookups and summing
four partial products of usually 5 digits, while doing it in base 20
requires 9 lookups and summing three partial products of usually 4
digits, about 40% less work (aside from the number of strokes
required).
