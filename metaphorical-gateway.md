Spirits we call *algorithms* possess glowing slabs of glass, metal,
and resin.  They speak with the voices of the dead and of things that
seem human, that tell us they are human, but that never were human.
The intangible force of lightning, a gas so light it easily permeates
through solid metal, ebbs and flows a billion times in an eyeblink,
waves flooding through billions of tiny gates.  Daemons fulfill our
requests when we speak to them in code: a code of one tribe called
ASCII, or Unicode, a code by which to govern the world, but only if we
can govern ourselves, which is harder than ever.

Our houses and our bodies are constantly penetrated by invisible
messages between these unearthly beings we have molded from earth.
They speak to one another with voices we cannot hear, voices made of
light, an esoteric light that passes through walls like they were
frosted glass.  Invisible messages enciphered using ciphers no man
knows how to break have become more commonplace than bread.

In books we have riches beyond belief; the humblest among us can carry
a thousand books in his pocket, and the shadow libraries of Alexandra,
Maksim, and Anna have twenty times as many books as that of Alexandria
ever did, two or three million in all.  These libraries are open to
all 24 hours a day, sending any book desired anywhere on Earth in
minutes.  Consequently they are hated by the rich and powerful, and
using them is commonly illegal.  Today, as in the time of Hypatia,
governments burn libraries.

