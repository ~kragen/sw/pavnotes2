Notes on the history of the PFE Forth environment
=================================================

The first Forth I used was Dirk Uwe Zoller’s PFE, back in 01996 or so,
and I have the vague memory that it could be compiled for MS-DOS.
(I’d like to see if I can run it on FreeDOS.)  It’s written in C,
which makes this plausible.

PFE 0.33.71 has no DOS support
------------------------------

But downloading [PFE 0.33.71][0] I find that this support has been
removed; the TODO file says:

> * run on as many posix-systems as possible, and may make a backport
>   to win/dos (currently only supported as cygwin).

[0]: https://master.dl.sourceforge.net/project/pfe/pfe/0.33.71/pfe-0.33.71.tar.bz2?viasf=1

0.9.14 does have DOS support
----------------------------

This brings into question whether my memory is wrong.  The earliest
version on SourceForge is [0.9.14][1], from 01995 or 01994, and its
README says:

> This package contains all [necessary] ANSI-C source files to build a
> running Forth-environment on most UNIX machines, on DOS and on OS/2.

[1]: https://sourceforge.net/projects/pfe/files/pfe/0.9.14/

IAFA.PKG says:

> Runs on several Unix-like operating systems and on MS-DOS and
> OS/2. Makefiles readily provided for AIX, FreeBSD, Linux, HP-UX,
> Ultrix, DOS and OS/2. DOS and OS/2 executables included (with EMX
> DOS extender).

(The executables are *not* included in the 0.9.14 package on
SourceForge.)

And in src/config/WATCOM there are configuration files for the Watcom
compiler, which I think [is free software now][17] under the [Sybase
Open Watcom Public License][18].  And there’s a term-dj.c:

[17]: https://open-watcom.github.io/
[18]: https://github.com/open-watcom/open-watcom-v2/blob/master/license.txt

    /*
     * term-dj.c ---        Terminal driver for DOS and OS/2 with DJGPP,
     *                      there is almost nothing to do.
     *
     * Adapted from term-emx.c by Antonio Costa (acc@asterix.inescn.pt)
     * Works in 25 and 50 lines text modes (at least).
     *
     * (duz 24Feb94)
     * (acc 27Jul94)
     */

That’s heartening!  But it seems to have some kind of dependency?

> I tried to resort to a common subset of all UNIX [behavior] known to
> me.  Fortunately this in fact allowed porting to DOS and OS/2 thanks
> to the great emx-package.

It was distributed via sunsite.unc.edu, later Metalab and now
[ibiblio.org][2]:

> Please check for the latest version via anonymous ftp from
>
> > roxi.rz.fht-mannheim.de:/pub/languages/forth/pfe-?.?.?.tar.gz
>
> (accessible from germany only) or
>
> > sunsite.unc.edu:/pub/linux/devel/forth/pfe-?.?.?.tar.gz

[2]: https://en.wikipedia.org/wiki/Ibiblio

Naturally roxi.rz.fht-mannheim.de doesn’t exist in 02024.

Ibiblio seems like a potential place to look for old versions if
earlier SF versions don’t have DOS support.  It’s been reorganized a
lot; [/pub/linux/devel][3] does exist, [but /pub/linux/devel/forth has
been moved][4].  It turns out that it only has PFE 0.9.12 and 0.9.13
though, plus 4tH, GForth, and a thing from 02005 called [VNPForth][5],
very-nonportable Forth.

[3]: https://www.ibiblio.org/pub/linux/devel/!INDEX.short.html
[4]: https://www.ibiblio.org/pub/linux/devel/lang/forth/!INDEX.short.html
[5]: https://www.ibiblio.org/pub/linux/devel/lang/forth/vnpforth-1.4.tgz

[EMX][10], due to its OS/2 origin, seems to have lived on
[Hobbes][11], which [shut down a few months ago][12] to [the sorrow of
its former maintainer][16], but [probably there are archives of
Hobbes][13], [Jason Scott having said not to worry][15], and [an EMX
page on EDM2 still exists][14], and [there’s a SourceForge page
too][19] which seems to have [an updated version from 02005][20],
though the SourceForge page is a bit misleading.

As for EMX, the EDM2 page about it says:

> (...) it should be taken into consideration that most of the
> compilers available for the system are based on old versions of the
> GNU system that were developed by the FSF themselves before the PGCC
> takeover of the codebase. In most cases you should consider Open
> Watcom or newer versions of GCC first for new ports and projects.

[10]: https://en.wikipedia.org/wiki/EMX_(programming_environment)
[11]: https://hobbes.nmsu.edu/?path=%2Fpub%2Fos2%2Fdev%2Ftools%2Ftoolkits
[12]: https://hackaday.com/2024/01/10/the-hobbes-os-2-archive-will-shut-down-in-april/
[13]: https://news.ycombinator.com/item?id=38929369
[14]: http://www.edm2.com/index.php/The_EMX_Project
[15]: https://mastodon.archive.org/@textfiles/111728995296654678
[16]: http://beesbuzz.biz/blog/14299-Hobbes-OS-2-Archive-An-end-of-an-era
[19]: https://emx.sourceforge.net/
[20]: https://sourceforge.net/projects/emx/files/emx/0.9d-fix04/

Inventory
---------

In [the main PFE directory][6] we have:

- 0.33.71
- 0.33.70
- 0.33.69
- 0.32.94
- 0.32.92
- 0.32.55
- 0.28.17
- 0.30.97
- 0.29.1
- 0.9.14 

In [`pfe_attic` we have][7]:

- 0.30.x
- 0.29.0
- 0.28.16

In [pfe-test we have][8]:

- 0.32.56
- 0.32.55
- 0.30.45 

And in [pfe-preview we have][9]:

- 0.33.68
- 0.33.65
- 0.33.61
- 0.33.59
- 0.33.58
- 0.33.48
- 0.33.44
- 0.33.42
- 0.33.34
- 0.33.33
- 0.33.30
- 33.29
- 33.28
- 0.33.27
- 33.26
- 33.24
- 33.17
- 33.13
- 32.94
- 0.32.85
- 0.32.74
- 0.32.40
- 0.31.55
- 0.31.34
- 0.30.95
- 0.30.45
- 0.30.16
- 0.29.2 

[6]: https://sourceforge.net/projects/pfe/files/pfe/
[7]: https://sourceforge.net/projects/pfe/files/pfe_attic/
[8]: https://sourceforge.net/projects/pfe/files/pfe-test/
[9]: https://sourceforge.net/projects/pfe/files/pfe-preview/

So if we wanted to binary-search on version numbers, halfway between
0.9 and 0.33 would be about 0.21, and the closest would be 0.28.16 in
the attic.

0.28.16 doesn’t support DOS
---------------------------

[0.28.16][21] from January 02000 already has the TODO entry I quoted
from 0.33.71:

> * run on as many posix-systems as possible, and may make a backport
>   to win/dos (currently only supported as cygwin).

So, I guess, 0.9 is the most likely option.

The ChangeLog file describes some of the history:

> 2000-03-24 <guidod@gmx.de>
> 
>  * release of 0.28.15 which is the version shipped with
>    the Tektronix K1297-2 system. You should note that only
>    the vxworks version are the ones being shipped, the
>    cygwin32 port is only an optional by-product of my
>    personal efforts for automake/autoconf. Most of the
>    ports had been done on my personal account although
>    Tek/MPT has some uses for the hpux and solaris ports too.
>    But they had not been product critical, the hpux version
>    had been recompiled about every two months, the linux
>    version about every 3 months, and the solaris version
>    about once per year.
> 
>     In this Changelog there will be bugfixes and changes that
>    are on behalf of my employer - I will mark them with my
>    employee e-mail adress <guidod@tek.com>. These will 
>    probably come over in larger bulks, so you will most
>    usually see my private e-mail adress <guidod@gmx.de>.
>    Well, not all changes are 100% distinguishable so I
>    had been awaiting consent of the patent-&-license 
>    manager of Tek/MPT. Today's the day.
> 
>  * In 1998 I had been contacting Dirk-Uwe Zoller - well,
>    his e-mail adress had changed since 1995 but I did
>    find him. He told me that he has no intentions at all
>    to do anything about Forth again, and so it came that
>    I did not inform him any further about changes to
>    the PFE. Please don't bother him either about anything
>    that has to do with Forth, 'cause his mind is set
>    elsewhere these days.
> 
> 1995-10-01 (on behalf of <duz@roxi.rz.fht-mannheim.de>)
> 
>  * version 0.9.14, the last release for a long time.

And indeed the config/EMX and config/WATCOM directories are gone;
INSTALL explains:

> The Portable Forth Environment uses now automake/autoconf to compile
> on unixish systems. Most of the elder configure-system has been
> deleted.

It mentions “Versions 0.1x.x,” namely “0.10.x and 0.11.x,” which did
apparently still use that system, but I suspect they were
Tektronix-internal and never released.

[21]: https://sourceforge.net/projects/pfe/files/pfe_attic/0.28.16/pfe-0.28.16.tar.bz2/download

0.9.14 requires hacking to compile with current compilers
---------------------------------------------------------

In order to see if I can use this PFE 0.9.14 to run my Forth code on
FreeDOS, it seems like it would be worthwhile to see if I can use it
to run my Forth code on Linux first before I wrestle with FreeDOS
toolchains.  But it doesn’t build.  PFE’s handwritten autoconf system
gets everything wrong when I try to run it:

    : src; sh config.sh 
    Configuring pfe for Linux...
    guessing config.h for Linux
    sys/types.h ... no
    sys/ioctl.h ... no
    fcntl.h ... no
    sys/stat.h ... no

`no` for everything until it obviously fails at invoking GCC:

    rm -f pfe helpidx showhelp *.o core *.core a.out *.bak *~
    gcc -m486 -pipe -Wall  -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT -DPREFIX=/usr/local -DPFELIB=/usr/local/lib/pfe -DPFEHLP=/usr/local/lib/pfe/help check_c.c -s  -ltermcap -lm -o check_c
    gcc: error: unrecognized command-line option ‘-m486’

Okay, well.  I can run it by hand:

    : src; gcc -pipe -Wall  -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT -DPREFIX=/usr/local -DPFELIB=/usr/local/lib/pfe -DPFEHLP=/usr/local/lib/pfe/help check_c.c -s  -ltermcap -lm -o check_c
    : src; ./check_c 
    #define CELL_TYPE long
    #define HALF_CELL_TYPE int
    #define HIGHBYTE_FIRST 0
    #define CELLSIZE 8
    #define SFLOATSIZE 4
    #define DFLOATSIZE 8
    #define CELL_ALIGN 8
    #define SFLOAT_ALIGN 8
    #define DFLOAT_ALIGN 8
    #define KEEPS_SIGNALS 1

That’s evidently part of what has to go into the aforementioned
`config.h`.  But maybe I can fix its guessing logic; how does it work?

    ./types.h:# include <sys/types.h>               /* size_t, time_t and friends */
    ./guesscfg.sh:if hasinc sys/types.h;    then define HAVE_SYS_TYPES_H;   fi
    ./guesscfg.sh:if grepinc fpos_t stdio.h sys/types.h

What’s `hasinc`?

    hasinc ()               # test if an include file is available
    {
      echo_n "$1 ... " >&2
      echo "#include <$1>" > $tmpc.c
      rm -f $tmpc.i
      if $make $tmpc.i >$null 2>&1
      then
        echo "yes" >&2
        includes=$includes" $1"
        rm -f $tmpc.c $tmpc.i
        return 0
      else
        echo "no" >&2
        rm -f $tmpc.c $tmpc.i
        return 1
      fi
    }

Probably `$make` is failing with an error that it’s discarding.  Let’s
undiscard it:

    if $make $tmpc.i #>$null 2>&1

Now we get:

    guessing config.h for Linux
    sys/types.h ... gcc: error: unrecognized command-line option ‘-m486’
    make: *** [Makefile:78: tmp1360534.i] Error 1
    no

Aha, okay, well, -m486 again, eh?

    #
    # Linux/options.mk ---  Part of Makefile for PFE, compiler options for Linux.
    #

    PREFIX  = /usr/local
    PFELIB  = $(PREFIX)/lib/pfe
    PFEHLP  = $(PFELIB)/help

    CC      = gcc -m486 -pipe -Wall

I can fix that; it's in config/Linux/options.mk.

    CC      = gcc #-m486 -pipe -Wall

This looks much healthier:

    : src; sh config.sh 
    Configuring pfe for Linux...
    guessing config.h for Linux
    sys/types.h ... yes
    sys/ioctl.h ... yes
    fcntl.h ... yes
    sys/stat.h ... yes
    direct.h ... tmp1361490.c:1:10: fatal error: direct.h: No such file or directory
        1 | #include <direct.h>
          |          ^~~~~~~~~~
    compilation terminated.
    make: *** [Makefile:78: tmp1361490.i] Error 1
    no

Except `config.h` now looks like this:

    /*
     * config.h --  Automatically generated file, don't change.
     */

    #ifndef __CONFIG_H
    #define __CONFIG_H

    #define HOST_SYSTEM "Linux"
    #define Linux 1
    #define UNIX_FILENAMES 1
    #define ISO_CHARSET 1
    gcc  -E  -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT -DPREFIX=/usr/local -DPFELIB=/usr/local/lib/pfe -DPFEHLP=/usr/local/lib/pfe/help tmp1361490.c > tmp1361490.i
    #define HAVE_SYS_TYPES_H 1
    gcc  -E  -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT -DPREFIX=/usr/local -DPFELIB=/usr/local/lib/pfe -DPFEHLP=/usr/local/lib/pfe/help tmp1361490.c > tmp1361490.i
    #define HAVE_SYS_IOCTL_H 1

So, uh, I guess I better undo my change to `guesscfg.sh`.

But there’s still some sort of difficulty with macros and GCC getting
stricter over the last 30 years:

    gcc   -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT -DPREFIX=/usr/local -DPFELIB=/usr/local/lib/pfe -DPFEHLP=/usr/local/lib/pfe/help   -c -o core.o core.c
    In file included from forth.h:59,
                     from core.c:34:
    core.c: In function ‘comma_’:
    macros.h:61:36: error: lvalue required as increment operand
       61 | #define INC(P,T)        (((T *)(P))++)
          |                                    ^~
    macros.h:122:51: note: in expansion of macro ‘INC’
      122 | #define COMMA(X)        (*(Cell *)DP = (Cell)(X), INC (DP, Cell))
          |                                                   ^~~
    core.c:152:3: note: in expansion of macro ‘COMMA’
      152 |   COMMA (*sp++);
          |   ^~~~~
    core.c: In function ‘dot_quote_execution_’:
    macros.h:63:38: error: lvalue required as left operand of assignment
       63 | #define ADD(P,N)        ((char *)(P) += (N))
          |                                      ^~
    macros.h:115:25: note: in expansion of macro ‘ADD’
      115 | #define SKIP_STRING     ADD (ip, aligned (*(Byte *)ip + 1))
          |                         ^~~
    core.c:172:3: note: in expansion of macro ‘SKIP_STRING’
      172 |   SKIP_STRING;
          |   ^~~~~~~~~~~

I mean, I would think that `((Cell *)(DP))` would be an lvalue? And so
would `((char *)(P))`?  But maybe not?  Maybe I can specify -std=gnu99
or something?  [The GCC 3.3.1 docs say][22] in the “C Extensions »
Generalized Lvalues” section:

> Compound expressions, conditional expressions and casts are allowed
> as lvalues provided their operands are lvalues. This means that you
> can take their addresses or store values into them.

[22]: https://gcc.gnu.org/onlinedocs/gcc-3.3.1/gcc/Lvalues.html

That’s in between “Referring to a Type with `typeof`” and
“Conditionals With Omitted Operands”, and is removed by [GCC
4.0.4][23], but still present in [GCC 3.4.6][24], which I guess is the
last version of GCC 3.

[23]: https://gcc.gnu.org/onlinedocs/gcc-4.0.4/gcc/Typeof.html#Typeof
[24]: https://gcc.gnu.org/onlinedocs/gcc-3.4.6/gcc/Lvalues.html#Lvalues

So I guess PFE relied on a GCC extension to C that went away in [GCC
4.0, which came out in 02006][25].  This is explained in [the GCC 4.0
changelog][26]:

> * The cast-as-lvalue, conditional-expression-as-lvalue and
>   compound-expression-as-lvalue extensions, which were deprecated in
>   3.3.4 and 3.4, have been removed.

[25]: https://gcc.gnu.org/releases.html
[26]: https://gcc.gnu.org/gcc-4.0/changes.html

So I guess `-std=gnu99` isn’t going to help!  It’s a little strange
that it would compile with Watcom C in that case, though, isn’t it?
Well, it looks like it has specific hacks for GCC in macros.h:

    #if defined __GNUC__ && !defined __STRICT_ANSI__ && !defined __cplusplus
    /* Use non-ANSI extensions avoiding address-of operator: */
    #define INC(P,T)        (((T *)(P))++)
    ...
    #elif defined MSDOS
    #define INC(P,T)        (((T *huge)(P))++)
    ...
    #else
    /* Force (or fool) ANSI-C to do typecast's it normally refuses by */
    /* casting pointers, not objects, then reference the casted pointers: */
    #define INC(P,T)        ((*(T **)&(P))++)
    ...
    #endif

Well, I can 0&& that into oblivion, can’t I?

    #if 0 && defined __GNUC__ && !defined __STRICT_ANSI__ && !defined __cplusplus

Now core.c almost compiles:

    : src; make
    gcc   -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT -DPREFIX=/usr/local -DPFELIB=/usr/local/lib/pfe -DPFEHLP=/usr/local/lib/pfe/help   -c -o core.o core.c
    core.c: In function ‘dot_quote_execution_’:
    core.c:172:3: error: address of global register variable ‘ip’ requested
      172 |   SKIP_STRING;
          |   ^~~~~~~~~~~

That’s also mentioned in the GCC 4.0 notes actually (and it’s only a
problem because of the previous change).  `ip` is declared in
`virtual.h`:

    #ifdef REGIP                    /* the instruction pointer */
    register Xt *   ip asm (REGIP);
    #else
    extern Xt *     ip;
    #endif

There are similar REGW, REGSP, REGRP, etc., macros, defined higher up:

    /* First the register assignments if GNU-C is used. ======================== */

    #if !defined __GNUC__ || defined  __STRICT_ANSI__ || defined __cplusplus
    # undef USE_REGS
    #endif

    #ifdef USE_REGS
    # if defined AIX1

    #   define REGIP "%ebx"

    # elif defined Linux || defined FreeBSD || defined EMX || defined NeXTstep

    #   define REGIP "%ebx"
    #   define REGSP "%ebp"

Well.  I can 1|| that into oblivion too.

And now it almost compiles!  All that’s left is this:

    gcc   -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT -DPREFIX=/usr/local -DPFELIB=/usr/local/lib/pfe -DPFEHLP=/usr/local/lib/pfe/help   -c -o unix.o unix.c
    unix.c: In function ‘uselibrary_’:
    unix.c:89:9: warning: implicit declaration of function ‘uselib’ [-Wimplicit-function-declaration]
       89 |   *sp = uselib ((char *) *sp);
          |         ^~~~~~
    unix.c: Assembler messages:
    unix.c:109: Error: invalid instruction suffix for `push'

And that’s because it wasn’t designed for 64-bit systems:

    #define cpush(x)        \
            __asm__ __volatile__ ("pushl %0;"::"g" (x));

It was released in 01995, and the AMD64 spec wouldn’t be released
until 02000 or implemented until 02003.

Once I removed the “l” from `pushl` I only got a link error:

    /usr/bin/ld: unix.o: in function `uselibrary_':
    unix.c:(.text+0x171): undefined reference to `uselib'

Which I had gotten a warning about before.  There’s a uselib(2) man
page, explaining that it’s a system call that’s been removed:

> uselib - load shared library (...)
>
>     [[deprecated]] int uselib(const char *library); (...)
>
> The system call uselib() serves to load a shared library to be used
> by the calling process.  It is given a pathname.  The address where
> to load is found in the library itself. The library can have any
> recognized binary format. (...)
>
> uselib() is Linux-specific, and should not be used in programs
> intended to be portable.
>
> This obsolete system call is not supported by glibc.  No declaration
> is provided in glibc headers, but, through a quirk of history, glibc
> before glibc 2.23 did export an ABI for this system call.
> Therefore, in order to employ this system call, it was sufficient to
> manually declare the interface in your code; alternatively, you
> could invoke the system call using syscall(2).
>
> In ancient libc versions (before glibc 2.0), uselib() was used to
> load the shared libraries with names found in an array of names in
> the binary.
>
> Since Linux 3.15, this system call is available only when the kernel
> is configured with the CONFIG_USELIB option.

Wow.  Okay.  I guess I could change `uselibrary_` to call `dlopen()`
but I’m not sure what purpose it would serve to `dlopen()` a library
and then not return a pointer to it?  I guess I’ll just `0&&` the two
`#if`s in here for Linux.

And now I have PFE 0.9.14 running!  At least it can handle `3 4 + .`
by printing `7 ok`.

Here’s the patch including all the changes:

    diff -urN pfe-0.9.14/src/config/Linux/options.mk pfe-0.9.14-hacked-to-build/src/config/Linux/options.mk
    --- pfe-0.9.14/src/config/Linux/options.mk	1995-10-31 13:36:30.000000000 -0300
    +++ pfe-0.9.14-hacked-to-build/src/config/Linux/options.mk	2024-08-18 21:04:34.675410145 -0300
    @@ -6,7 +6,7 @@
     PFELIB	= $(PREFIX)/lib/pfe
     PFEHLP	= $(PFELIB)/help

    -CC	= gcc -m486 -pipe -Wall
    +CC	= gcc #-m486 -pipe -Wall

     # comment out -DUNROLL_NEXT when you run it on a Pentium
     OPTIM	= -O2 -fomit-frame-pointer -DUSE_REGS -DUNROLL_NEXT
    diff -urN pfe-0.9.14/src/macros.h pfe-0.9.14-hacked-to-build/src/macros.h
    --- pfe-0.9.14/src/macros.h	1995-10-31 21:46:12.000000000 -0300
    +++ pfe-0.9.14-hacked-to-build/src/macros.h	2024-08-18 21:27:06.312002948 -0300
    @@ -56,7 +56,7 @@
     #define OFFSET_OF(T,C)	((char *)&(((T *)0)->C) - (char *)0)

     /* inc/decrement, push/pop of arbitrary types with arbitrary pointers */
    -#if defined __GNUC__ && !defined __STRICT_ANSI__ && !defined __cplusplus
    +#if 0 && defined __GNUC__ && !defined __STRICT_ANSI__ && !defined __cplusplus
     /* Use non-ANSI extensions avoiding address-of operator: */
     #define INC(P,T)	(((T *)(P))++)
     #define DEC(P,T)	(--((T *)(P)))
    diff -urN pfe-0.9.14/src/unix.c pfe-0.9.14-hacked-to-build/src/unix.c
    --- pfe-0.9.14/src/unix.c	1995-10-31 21:46:23.000000000 -0300
    +++ pfe-0.9.14-hacked-to-build/src/unix.c	2024-08-18 21:49:31.401151876 -0300
    @@ -44,7 +44,7 @@




    -#if defined (Linux) && !defined (__cplusplus)
    +#if 0 && defined (Linux) && !defined (__cplusplus)
     /************************************************************************/
     /* Linux shared library calls -- KAH 930824				*/
     /************************************************************************/
    @@ -73,7 +73,7 @@
            __asm__ __volatile__ ("call %2;movl %%edx, %0;movl %%eax, %1":	\
            "=g" (resulthi), "=g" (resultlo):"g" (sub): "eax", "edx");
     #define cpush(x)	\
    -	__asm__ __volatile__ ("pushl %0;"::"g" (x));
    +	__asm__ __volatile__ ("push %0;"::"g" (x));

     /* not sure if this float stuff is right.  Are singles and 
        doubles same length?  Are these stored in a different forth stack?
    @@ -164,7 +164,7 @@

     LISTWORDS (system) =
     {
    -#if defined Linux && !defined __cplusplus
    +#if 0 && defined Linux && !defined __cplusplus
       /* shared libraries */
       CO ("CALL-C",		call_c),
       CO ("USELIBRARY",	uselibrary),
    diff -urN pfe-0.9.14/src/virtual.h pfe-0.9.14-hacked-to-build/src/virtual.h
    --- pfe-0.9.14/src/virtual.h	1995-10-31 21:46:13.000000000 -0300
    +++ pfe-0.9.14-hacked-to-build/src/virtual.h	2024-08-18 21:33:14.382040962 -0300
    @@ -38,7 +38,7 @@

     /* First the register assignments if GNU-C is used. ======================== */

    -#if !defined __GNUC__ || defined  __STRICT_ANSI__ || defined __cplusplus
    +#if 1|| !defined __GNUC__ || defined  __STRICT_ANSI__ || defined __cplusplus
     # undef USE_REGS
     #endif

But it doesn’t work:

    : src; ./pfe
    A portable Forth environment written in C. Version 0.9.14 of 01-November-95
    Copyright Dirk Uwe Zoller 1995. Please enter LICENSE and WARRANTY.

    To quit say BYE.

    Hi there, enjoy Forth!

    : 4- 4 - ; 3 4- . 
    Error: "4-" invalid memory address

