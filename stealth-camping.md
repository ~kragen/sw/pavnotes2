Apparently “stealth camping” is a thing on YouTube.  People camp in
bivvy sacks, hammocks, camouflage nets, World-War-I style trenches,
under bushes, etc., in locations such as wooded traffic circles and
outside police stations, and then endeavor to “leave no trace”.  There
is of course a big intersection with bushcraft competence porn like
John Plant’s Primitive Technology.

Unsurprisingly it’s mostly white people doing this, and one of their
comments is that long-sleeved, dark-colored shirts, hats, and gloves
decrease their visibility significantly.  I’d think that a
dark-colored mask would also help.

A kind of goofy thing is that most of them travel to their camping
spots with cars.  I’m pretty sure that you can camp a lot more
stealthily in most parts of the world by just sleeping in your car, so
I think probably the appeal of this as a hobby is largely the thrill
of the risk of getting caught.  Or maybe that just makes the videos on
YouTube get watched more.

One of the recommendations one Influencer gives is to use propane or
butane for cooking, if possible, rather than a campfire, in order to
produce less light and smoke, and maybe dig a hole with a mound around
it to stop light and sound from the cooking fire, which also helps
with efficiency.  It occurs to me that one of the rarely-remarked-on
advantages of alcohol as a fuel is that its flame produces very little
light, even less than propane or butane, and no smoke.  Mark Jurey’s
“penny stove” is even lighter than the ultralight camping stoves they
commonly use, and can comfortably cook food.

(Self-heating food containers would be even better, since they can
cook food even while you’re hiking down a trail, and don’t produce any
light at all.  The TCES energy storage materials aren’t quite as
compact, but still plenty compact.  Alternatively, batteries, though
they’re a little less compact: heating 1ℓ of water inside an insulated
container from 0° to boiling requires 418kJ, about 31000 milliamp
hours at 3.7V or 23000mAh at 5V, a rechargeable battery that easily
fits in your hand.)

Pemmican or similar high-calorie-density foods also seem like they
would be useful.

They also like to talk about the so-called “Dakota fire hole”, two
small 300-mm-deep pits connected by a 150-mm-long tunnel, with a wood
fire in the larger pit, in order to ensure that the fire has adequate
oxygen (reducing smoke) and burns below ground level.

And they love glowsticks and red headlamps.

It occurs to me that sentry devices such as wireless cameras and FPV
drones could be used both to stop stealth camping (by detecting the
stealth campers) and to facilitate it, in four ways:

1. By giving the stealth campers advance warning of people
   approaching, enabling them to escape; they could return later to
   retrieve their sentry gear.
2.  To examine how visible their own campsite is from some distance
    off.
3. For capturing evidence if there’s any kind of trouble.
4. For scouting out potential campsites ahead of time to observe
   whether there are sprinklers, regular visitors, etc.

I’m not sure what you’d do about snoring.

The most effective “stealth camping” strategy seems to be camping in a
box truck, because generally people ignore those, and people in
vehicles are generally treated less roughly than apparently homeless
people.
