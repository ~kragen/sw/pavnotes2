I was playing with Falstad’s circuit.js and [simulated this class-D
amplifier][0]:

![(circuit diagram with two op-amps)](./two-opamp-pwm.png)

    $ 1 1e-7 3.452441195350251 50 5 43 5e-11
    R 32 208 -16 208 0 1 40 0.5 0 0 0.5
    a 176 224 336 224 9 15 -15 1000000 0.16515906121078794 0.1651299246073442 100000
    c 336 224 336 288 0 2.2000000000000002e-8 -1.595334981411385 0.001
    w 176 240 176 320 0
    r 176 384 176 464 0 100
    g 176 464 176 480 0 0
    w 336 384 336 288 0
    w 416 288 416 256 0
    r 336 224 416 224 0 10000
    r 416 192 528 192 0 100000
    w 528 240 528 192 0
    r 32 208 96 208 0 100000
    r 96 288 96 208 0 100000
    r 176 320 176 384 0 10000
    r 176 320 288 320 0 100000
    w 288 320 592 320 0
    w 592 240 592 320 0
    w 528 240 592 240 0
    w 96 208 176 208 0
    r 592 240 656 240 0 100
    c 656 240 656 304 0 0.000001 2.095017908820403 0.001
    g 656 304 656 320 0 0
    r 656 240 720 240 0 1000
    c 720 240 720 304 0 1e-7 1.688980214758755 0.001
    g 720 304 720 320 0 0
    x 225 276 319 279 4 12 error\sintegration
    x 424 143 536 146 4 12 Schmitt\scomparator
    x 615 204 734 207 4 12 low-pass\soutput\sfilter
    x 424 160 518 163 4 12 generating\sPWM
    w 336 384 176 384 0
    g 96 288 96 320 0 0
    w 416 288 336 288 0
    w 416 224 416 192 0
    a 416 240 528 240 9 15 -15 1000000 -1.3183253629670897 -1.285145467345016 100000
    o 0 512 0 4098 0.625 0.00009765625 0 2 0 3
    o 2 2 0 4099 2.5 0.025 1 2 2 3
    o 10 2 0 4106 20 0.2 2 2 10 3
    o 23 512 0 4354 2.7599068709341803 0.0017744098217740145 3 2 23 3

[0]: http://falstad.com/circuit/circuitjs.html?ctz=CQAgjOCmC0DsIGYB0AWArAJhSsYCcaCaADBmhCSGiCglTLgFABKiGIGxAHCNGAGwduIYuBqjiSahJFTGAQ3CxBGLIgQq1ecNT7UwxQ0dkDyaPMX5gMB2F1h4UJ-uQx48WfsVgJs7A0bEjADG6ppOCBocXDyiGEicgUmBGDA8fFIEkSh4XGA4YAhc0kiGYIwA7koqKKJgymwSjABO1YhcTvWCKPxOdYaMAOZtPZ0NKFwyQVWRgkURURgxIpU0AtE8OCpogkGtsxxqW4d94IEta4L47Jg81yJn51W3HLVUS+AeK-vsnDx4KmE-XOrQBGxAYL+DwCRguXUabXm0JBiM44IQaOBsKqSx4GNE5nY+JWzy+WAJX2J03ePHJVDJb2pkOE8Khe3pvzeLhqMgCIRA3NeokFCGIp0kSQg8WIBGI9QsMU4tToErlQwFO0QYo1c0x3x1QpAsDRdKx-ONcTeFq1pzAMHgYCQ-BiuVI+VgaHsaBKZXV1tFTn9eqCAA9DtQMA0EPgOA4aOB2JBms0APbNAA6AGcAJYAOwALpBBs15Pnsync4wwyg1Pk6IQrj149YQABlYIACwAttn8-ms8EU12AA7yEv5tNVgVgCPanxOTjwTrsAA2KYq0FHmczWZTAFd88OD1mAGbZleF5pTmudLxUMB3fh0ZcgQaQXNJ0t5wZZgAKAHUAFlVgOJF4SRIJhkhZYwSpElLnRRZlmpY5VCcY57iCRRULeF46W0GdeEImFjAyaMuAxBs3H4WBuDweAMiWch0B6ecSHWEighTB5XAeWpclkfgyFkQJ6O5YS4geBBGG434+JlbR4h9YSpQ4RAZLONTRBwSwhFkOT-FEaTZPrFttKIBckA9dxLHsGVfAfYgVVKOVYFgbAZS4Gw3NqfJqDoX46GkoA

Inside the class-D amplifier, there are five resistors, two opamps,
and a capacitor.  An input opamp compares the input voltage against
the current output voltage, computing an instantaneous error signal,
which it integrates.  An output opamp is configured with positive
feedback instead of negative, adding a little positive feedback to the
differential integrated error signal to get Schmitt-trigger action.

Then I simplified it further to [four resistors, two opamps, and a
cap][1]:

![(better circuit diagram)](./simplified-two-opamp-pwm.png)

    $ 1 1e-7 13.097415321081861 50 5 43 5e-11
    R 224 208 176 208 0 1 40 0.5 0 0 0.5
    a 224 224 336 224 9 15 -15 1000000 0.26278058266212556 0.26279046941368417 100000
    c 336 224 336 272 0 2.2000000000000003e-9 0.7258945732012363 0.001
    w 224 240 224 320 0
    r 224 320 224 384 0 1000
    g 224 384 224 400 0 0
    w 464 320 464 256 0
    r 352 224 464 224 0 10000
    r 464 192 576 192 0 100000
    w 576 240 576 192 0
    r 464 320 576 320 0 10000
    w 576 240 576 320 0
    r 640 320 704 320 0 100
    c 704 320 704 384 0 0.000001 2.611001578135656 0.001
    g 704 384 704 400 0 0
    r 704 320 768 320 0 1000
    c 768 320 768 384 0 1e-7 2.6343680864860906 0.001
    g 768 384 768 400 0 0
    x 233 297 327 300 4 12 error\sintegration
    x 476 132 588 135 4 12 Schmitt\scomparator
    x 664 275 783 278 4 12 low-pass\soutput\sfilter
    x 476 149 570 152 4 12 generating\sPWM
    w 336 320 224 320 0
    w 464 320 336 320 0
    w 464 224 464 192 0
    a 464 240 576 240 9 15 -15 1000000 0.26278058266212556 2.2624501373262804 100000
    x 316 387 442 390 4 24 Circutrulito
    403 144 80 256 160 0 0_512_0_4867_0.625_0.00009765625_-1_2_0_3_Input
    403 352 256 448 320 0 2_4_0_4611_2.5_0.003125_-1_2_2_3_C1
    403 784 368 912 432 0 16_512_0_4866_10_0.0015625_-1_2_16_3_Second\sfilter\sstage
    403 560 368 688 432 0 13_512_0_4866_10_0.4_-1_2_13_3_First\sfilter\sstage
    403 592 160 720 224 0 9_4_0_4618_20_0.000390625_-1_2_9_3_Output\svoltage
    x 195 357 209 360 4 12 R1
    x 391 197 405 200 4 12 R2
    x 512 344 526 347 4 12 R4
    x 508 167 522 170 4 12 R3
    x 279 189 297 192 4 12 IC1
    x 515 277 533 280 4 12 IC2
    w 640 320 576 320 0
    x 664 290 875 293 4 12 (to\spermit\svisualizing\soutput\saverage)
    w 352 224 336 224 0
    368 352 224 352 192 0 0
    403 288 112 416 176 0 41_4_0_4098_5_3.2_-1_2_41_3_IC1\soutput
    w 336 272 336 320 0

[1]: http://falstad.com/circuit/circuitjs.html?ctz=CQAgjOCmC0Ds4GYB0AGAnLALGArAgTGCgBxjEBsEOKIOImCtMYYAUAEoj76ZcnixyfYiBoRMNFEjqTR01gEMuPZbwQIh3XmnB1oucCiPG5+cvljEUOYmfNh8OHEKlmLaFJnJpsG4tngiYxRWAGMQdU0VSK5YfFEuJHxglJSEGB0pOJsfHFgCFAcNRikjNgB3VS4JKoLRVgAnWuTa-wSgkIBzVt4teiMEkMqvNRaRrmd6poQceL7xvrFgxvpyXjA0eLyhDfil5crt6poj3anV0ZPBCJb9o1ZD654roTqQprWaOthPG9kgsIgH6XIG-BBtSSoYIQfBIShBXCWMAzciTUqFVjdYERNrYiSyd6gkGCERvQz3cIkv5A8ikiFQOCJcgIBi0khrCjoFAuKFsLG0nG8Kn4wasAAeXHUXAwN3gCAG63ikAaDQA9g0ADoAZwAlgA7AAukE6DQUBp1qr14vo12RW2IImRdEVIAAyqEABYAWx1BoN2tCqq9AAcFKaDerreQ1rE6JZGBYRC6ADaq8rQUNarXa1UAVwNwfz2oAZjrk0aGtbMLbMDo8mJZvRwPFOpA9cqzfrOtqAAoAdQAsg8IhpqX03sPxnUYhPhjH5jGziElAsakdniAdAZ9HQOiZXOZLNZbNHCI5JrC3JhqMj8m4rOsUtaEGBXsR4JhMPEEB4myoAMI6g0oT5g0ubJr6qqsBIjBgJ+IBWBMOzkASAD6OAOKhKCoZgFCwFhcKOARwQYKi5g4Kh+iofgWGoQgqEAJJ6oWBrQSgjAzHMkyfqStxcDhtFeCw1HSMR7EOBRVE0TR9H-mwMFAm0fibg49AEO05DoZh2G4dGqFEGJuDkZRYDUfpmn0a6kCBnqAAmJZlhW2pagaCitmxjDOF8Aq0km6liPRGE0TpFCaQZUiYCZZnInRqEAGJAS5Dnlsqzmue5Ck4Js4AoUCLSLJuAk6ZQxDUdh6LsR4xlSahaCxQA8vmLHagAbqq5ZuZA1obHQMzwMkOgaDQLrsGwEo-hAGwftYfDDc2IDsPg1pBRE8GzK81ZNqp7CYMt-CvvAszxGAPxbfE7AINa7jgMQOj4DKZwugxcnLQYFiHVK+CIU9-5LZUnzUkcE4StGvS-u+dD3YwLoABQRtqwbKj6-pai1OparmCgQQAXl2OZNUWWoKC1HatgAlMOnG1KOiysMpVPjo2S71ApX2Oqp2A7Ncw2mZFOnoKVFHIDRNXYLFz1gPjBb5pTNNxCOry3KwQA

The input signal is connected directly to the non-inverting input of
the error-integration opamp IC1.  Its output is connected to the high
side of the capacitor C1, 2200pF in this simulation.  IC1 drives a
current through C1 proportional to the instantaneous output error, so
C1’s voltage represents the integrated error.  C1’s low side is
connected directly to IC1’s inverting input, which is tied to ground
through a 1kΩ resistor R1, so each milliamp through C1 produces a
change of 1 volt on IC1’s inverting input.  The low side of C1 is
*also* connected directly to the inverting input of the output opamp
IC2; C1’s high side is connected to IC2’s non-inverting input through
a 10kΩ resistor R2, so IC2 mostly acts as a comparator, driving its
output to the positive or negative rail according to whether C1’s
current voltage is above or below ground.  To add a little hysteresis
to this comparator, there’s a 100kΩ resistor R3 between IC2’s output
and its non-inverting input, so there’s a resistive divider formed by
R3 and R2 back to the high side of C1, which adds a tenth of IC2’s
output back into its non-inverting input, which sets the hysteretic
threshold over which C2 oscillates to about a tenth of IC2’s maximum
output voltage.  Finally, the feedback loop is closed with a 10kΩ
resistor R4 between IC2’s output and IC1’s inverting input, which
forms a resistive divider with R1, attenuating the input by a factor
of 10.

This is an improvement over the previous designs I’d come up with
where the error measurement and integration were handled by two
separate op-amps.

The instantaneous error is almost always large, because it comes from
subtracting the (attenuated) output voltage, which is either at the
negative rail or the positive one, from the input voltage.  It will
only ever be small if the circuit is close to its limits, at which
point the oscillation will slow down and finally stop, with the output
op-amp stuck to one rail or the other.  Because in this case the
negative feedback path between the output and input has an attenuation
of 11× (11kΩ/1kΩ) this will happen if the input voltage goes near or
beyond 1/11 of the output voltage range.

The current waveform through C1 of course averages zero, because C1 is
a capacitor, so current through it must always average zero.  But it
does it in an interesting way: over any particular short time period,
it’s a PWM square wave that looks like IC2’s output square wave upside
down, but with a DC offset.  The DC offset moves the narrower part of
the cycle further from zero volts, and the wider part closer, so that
their areas are equal.

The frequency of oscillation varies with the duty cycle.  It’s driven
by the error current signal through the 1kΩ R1 and the capacitance of
C2, and how fast that reaches the hysteretic threshold set by IC2’s
output limits and the positive-feedback divider R3-R2; in this
simulation, IC2 can output ±15V, and the positive feedback attenuates
that to ±1.5V.
