In Smalltalk-80 if you have some foobars and you want to get a list of
the bletchy ones, you say:

    bletchyFoobars := foobars select: [ :foobar | foobar isBletchy ].

But in [Smalltalk-78][0] you would instead use:

    bletchyFoobars ← foobars all∘̊ x suchThat∘̊ [x isBletchy].

[0]: https://smalltalkzoo.thechm.org/HOPL-St78.html

(Presumably this is also true in Smalltalk-76, but I don't have a copy
of that to run.)

For example, the SymbolTable⇒allGlobals method says (my approximate
transcription):

    allGlobals  | s  " Smalltalk allGlobals "
            "return an array of names of non-classes in me"
            [⇑ self contents all∘̊ s suchThat∘̊
                  [(self∘s Is: Class)≡false]]

Here self∘s means "item s of self", like self[s] in Python or JS; the
∘ and ∘← methods are used to get and set collection members
respectively.  The variable s is declared as a temporary variable in
the header line before the comment containing the example code
"Smalltalk allGlobals".  And I think, very much unlike later
Smalltalks, the [] after suchThat∘̊ were used just for syntactic
grouping, equivalent to parentheses; in fact when I replace them with
() the code continues to work.

So I think something very special is going on here, specifically that
the hollow colon character (which doesn't exist in Unicode, so I'm
rendering it as ∘̊) is used to introduce *call-by-name* method
arguments.  The mechanism here is, I think, the same as in ALGOL-60:
instead of evaluating an expression like (self∘s Is: Class)≡false to,
say, a boolean, and passing that boolean in to the all∘̊suchThat∘̊
method, the compiler packages it up into a thunk that can be evaluated
zero, one, or many times, to get its current value.  Moreover, the
thunk generated for the variable s supports not just *getting* but
also *setting* its current value.  That's how the all∘̊suchThat∘̊ method
is able to work: it sets its first argument, s in this case, to a
different value of the array before each invocation of its second
argument, the boolean expression.

The interface to this functionality is a bit more explicit than in
ALGOL-60, though.  Array⇒all∘̊suchThat∘̊ looks like this (again,
approximate transcription):

    all∘̊ t1 suchThat∘̊ t2 | t3 t4 t5
            [t3 ← (self species new: self length) asStream.
            for∘̊ t4 to: self length do∘̊
                  [t5 ← self ∘ t4.
                  t1 value← t5.
                  t2 eval ⇒ [t3 next← t5]].
            ⇑t3 contents]

My interpretation of this, which might be a bit wrong, is as follows.

This is a method called all∘̊suchThat∘̊ which takes arguments t1 and t2,
and declares variables t3, t4, and t5 (the names make me suspect that
the source code was lost and this version is decompiled from
bytecode).  It starts by setting up t3 as a stream on a new array of
the same species as the receiver (Vector in the case of an Array or
its subclasses Interval, String, or Substring).  A "stream" is a thing
you can append values to.

Then it runs a loop using I think for∘̊to:do∘̊, which I am guessing is a
built-in control structure, both since no receiver is in evidence in
the syntax and because there's a ParsedForLoop class; the hollow
colons indicate that the "for" argument (the variable t4), and the
"do" argument (the loop body) are similarly unevaluated.  for∘̊to:do∘̊
sets t4 in turn to each of the valid indices of the array and
evaluates the loop body.  The loop body fetches the t4th element of
self and puts it in t5, copies it to the value t1 points to, such as s
(using value← rather than just ← because the idea is to change the
caller's variable, not just the method argument that points to it),
and then evaluates t2, the boolean condition (and forcing that thunk
is also not transparent as it would be in ALGOL-60; you have to call
the "eval" method).  The ⇒ is a conditional which, if its left
argument is true, evaluates its right argument; in this case the right
argument invokes the next← method on the stream to append t5 to it, if
the evaluation of t2 returned true.

Finally, once the loop is done, it returns a possibly empty array of
the accumulated items.

I think this kind of call-by-name approach is an interesting mechanism
for higher-order programming.  In this form it's setting the compiler
a really nasty challenge; here t2 is only ever used as an rvalue (with
"eval") and t1 is only ever used as an lvalue (with value←), but
although I don't understand much of the compiler, I think the compiler
doesn't know this when it's compiling a call to the method.  That is,
when it's compiling, for example, allGlobals above, it doesn't know
whether the method called will attempt to assign to its second
argument and read from its first.  So I think it has to compile thunks
to do both (which I think is called a "remote").

In this particular case the only advantage of the ancient call-by-name
approach is that you get arguably nicer syntax, because you can say, I
think,

    foobars all∘̊ x suchThat∘̊ x isBletchy

rather than

    foobars select: [ :x | x isBletchy ]

And if you were to implement `for` as a function in this way you could
use syntax like

    for: x in: foobars do:
        something(x)
        something_else(x)

rather than something like

    for foobars, x => {
        something(x)
        something_else(x)
    }

This degree of syntactic sugar is not a particularly strong argument,
particularly balanced against the semantic complications and
implementation headaches involved.  I mean if you want syntactic sugar
you can always use macros, right?

The particular case where I think this kind of thing might be
especially interesting is IMGUI programming, where you'd like to be
able to say things like

    self slider: 'Azimuth' for∘̊ starMap.azimuth from: 0 to: 360;

The hypothetical IMGUI slider method needs to both read and write the
struct member starMap.azimuth, but if it can do this through just a
memory pointer instead of read and write thunks, that might be better;
it occupies one word instead of four words, and accesses through it
involve single-machine-instruction memory accesses instead of a whole
subroutine call and return.

In Rust this would look something like

    slider("Azimuth", &mut starMap.azimuth, 0, 360);

And of course if you have the ability to pass in mutable references
like this, as well as the ability to conveniently pass in blocks
(without necessarily being able to treat them as lvalues), you have
everything you need to write functions or methods like all∘̊suchThat∘̊.

See `tclish-basic.md` for some thoughts on this.
