Triangular tilemaps
===================

I've been playing with tilemaps in Godot; in version 4, it added a Wang tile system which allows you to paint a tilemap with “terrains” (which I think would be better called “materials”) and let it pick the particular tiles that satisfy those constraints.  And I’ve been thinking about how to make this kind of thing more powerful, and it occurred to me that a particular approach using triangles would allow much more expressivity.

How Godot terrains work
-----------------------

In Godot 4, you can assign one or more “terrain sets” to a TileSet, each one comprising several “terrains”, which you can then paint onto (parts of) the tiles in the tile set; once you’ve done this, you can paint these same terrains onto your TileMap in the world, and it will update the tiles around the point where you’re editing to satisfy your requested terrains as well as it can.  As an example, in a top-down RPG map, you might have terrains for “dirt”, “grass”, and “forest”, with tiles providing transitions between each of these. In a platformer, you might have “terrains” for ladders, platforms, and rocks.

You can have multiple tiles that would satisfy the same constraints, specifying probabilities for them.  This is helpful for adding some random variation to your scenes.

The particular system it chose for the terrains is a little bit weird; in a pure Wang-tile system, you’d paint the *edges* (or, in the perhaps more common corner-tile variant of Wang tiles, the corners) of your tiles with “colors” to force the spaces adjoining that place to accommodate that choice, by picking a different tile if necessary.  But, in Godot’s terrain system, you assign terrains to the four edges *and also the center*, and the targets you click on the tilemap editor are the *centers* of the tiles.  This makes it possible to paint, for example, platforms as short as a single tile. “Platform” (or, say, “rounded grassy platform”) is a terrain type, and each of the tiles of that type have it in their center, and also on their left and/or right edges, as appropriate. The top and bottom edges are set to “no terrain,” which allows them to match tiles that don’t belong to that terrain set, as well as one another.  The same is true of left and right edges that terminate a platform.  This allows the painting of platforms as short as a single tile, which have the same edge colors as blank-space blocks, differing only in their center.

There seem to be some lacunae in my understanding here that I hadn’t noticed: if I paint an area of my tilemap with this “grassy platform” terrain, why doesn't it just fill up the area entirely with single-tile platforms?  But it doesn’t; as I paint horizontally, the platform smoothly extends from one tile, to two (a left end adjoining a right end), to three (adding a horizontal element in the middle), to more.  Moreover, I can come back and later extend a platform, causing it to join smoothly.

In addition to edge matching, which is what works best for things like these platforms, Godot terrains also support corner matching and edge-and-corner matching.  Edge matching and corner matching have five terrains per tile, while edge-and-corner matching has nine.

With this approach, a full edge-matching tileset for a single foreground material on a single background (say, brick against sky in a platformer) requires 17 tiles: four corner tiles, four edge tiles, one center tile, three tiles for single-brick-tile horizontal lines (left, center, right), three tiles for vertical ones (top, center, bottom), a tile for isolated brick with sky around it, and a pure sky tile. Squares are pretty symmetric, though!  So, for “isotropic” materials, which are more appropriate in RPG maps than in platformers, you can generate most of these tiles from rotations of other tiles.  You only have 7 unique configurations: corner, edge, solid center, line center, line end, isolated brick, pure sky.

Purely-edge-matched square tiles
--------------------------------

With square tiles and only edge matching (no center colors), you could still draw single-tile-wide platforms if your platform-ending tiles only had half a tile’s width of platform in them.  The platform boundaries would just be half a tile offset from the main tile grid in *x*.  A more sensible approach would be to use corner matching rather than edge matching, so they’re offset by half a tile in both *x* and *y*.

This still requires 16 tiles, which is about the same as 17.  For isotropic materials, you have 6 distinct tiles: all sky, one corner brick, two adjacent corners brick, two opposite corners brick, one corner sky, all brick.  This is a somewhat bigger improvement over 7.

An interesting thing to note here is that, with corner matching, you’re painting materials onto the *dual tiling* of the tilemap you see: a tiling whose tile centers are the vertices of the painted tilemap, and whose vertices are consequently its centers.

Triangular tilemaps
-------------------

A common alternative to square tilemaps (or, historically, square pixel arrays) is hexagonal tiling, which features more consistent center-to-center distances among adjacent tiles, and a higher degree of symmetry: sixfold rotation and 12 reflection axes, rather than fourfold rotation and 8 reflection axes.  Its dual is the triangular tiling.  What if we use triangular tiles in our tilemaps with corner matching, painting our materials onto the hexagonal lattice formed by the vertices of the triangular tiling?

(Godot's tilemap also offers an “offset square” variant of hexagonal tiling, similar to brick masonry running bonds and sometimes seen on ceramic tile floors; each row of squares is offset by half a tile from the previous one.)

With triangular tiles, for two materials, such as brick and sky, there
are only 4 tiles up to rotations: all sky, one corner brick, two
corners brick, and all three corners brick.  This is less work than
the 6 we’d need with corner-matching squares.  But each of these tiles
occurs in 6 rotations, so if our material is anisotropic, we may have
up to 24 tiles to paint instead of 6 or 16.

However, in the particular case of brick against sky, plausibly all we really need to distinguish is the orientation to the horizontal, so we can use reflections.  The all-sky case only needs one tile.  The one-corner-brick case needs two tiles plus their reflections: one for the case where the corner is an upward-pointing or downward-pointing corner of the triangle, and the other for the case where it’s on the top or bottom edge instead.  The two-corner brick case similarly has two cases.  And the all-brick case only has one.  So we're back to 6 tiles.

I suspect that things get more exciting for larger numbers of isotropic materials, as you might find in an RPG map.  Suppose you have 5 materials.  Then there are 5 single-material tiles, 5·4 = 20 dual-material tiles (up to rotations), and I think 5·4·3/3! = 5·2 = 10 three-material tiles (up to rotations and reflections), so a total of only 35.

The corresponding number for square tiles is significantly larger: 5 single-material tiles, 5·4/2 = 10 checkerboard dual-material tiles (up to rotations), 5·4/2 = 10 non-checkerboard dual-material tiles (up to rotations), 5·4·3/2 = 30 three-material tiles with the same material on two adjacent corners (up to rotations and reflections), 5·4·3/4 = 15 three-material tiles with the same material on two diagonally opposite corners (up to rotations and reflections), and 5·4·3·2/8 = 15 four-material tiles (up to rotations and reflections), for I think a total of 85.  This is a significant advantage, but not as enormous an advantage as I had thought.  I think the advantage gets bigger with more materials, but I’m not going to work it out right now.

Of course, Wang tile sets don’t have to be “complete” in the sense of offering every possibility; they can encode constraints, for example on which terrains can be adjacent, or what orientations combinations can happen in.  Then, a simple (if computationally costly) search procedure, similar to the WaveFunctionCollapse algorithm, can procedurally generate a map for you. Triangular tiling offers the possibility of more visual variety from fewer tiles; each maximally-asymmetric triangular tile can be rotated and reflected into 12 positions, if you turn that option on, so you're drawing 12 tiles for the price of one.
