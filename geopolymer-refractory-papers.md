[Geopolymer binding system for refractory castables][0], EPO
application EP2951133B1 by Jennifer Werz, Bertram Kesselheim, Darina
Rudert, and Kai Beimdiek of the kiln-maker Refratechnik Holding GmbH,
is unfortunately entirely in German except for the claims, which are
the uninformative part but are also translated badly into English and
French.  They seem to be proposing making a refractory geopolymer
cement out of an “activator combination” of caustic magnesia (or
similar) with forsterite (or similar) as a less-reactive magnesium
source, and an aluminosilicate “binder” of metakaolin (or similar).
And then of course they propose using it to bind fairly conventional
refractory aggregates like perlite, mullite, silicon carbide,
graphite, or whatever.

[0]: https://patentimages.storage.googleapis.com/82/90/21/bb21393319fff1/EP2951133B1.pdf

However, this corresponds to the [US patent 10,029,945(B2)][1], for
which they apparently translated the German patent even more badly
into English.  It defines “geopolymer” as “Si and Al atoms, which are
bound into a polymer network by way of oxygen atoms”, and explains
that most of the above is already well-known; the invention they claim
is a way to improve the shelf lives of such castable refractory powder
mixes by including two different magnesia sources, one more
water-reactive than the other.  Unfortunately, they don’t really
explain how that works, or for that matter how you’re supposed to get
a polymer Si-O-Al-O network rather than Si-O-Al-O-Mg-O by activating
the geopolymer with magnesia instead of waterglass.

[1]: https://patents.google.com/patent/US10029945B2/en

****

[Fly Ash Based Geopolymers With Refractoriness Properties][2] ([HTML
abstract][3]) is a paper from 02021 by Katarzyna Zarębska, Piotr
Zabierowski, Magdalena Gazda-Grzywacz, Natalia Czuma, and Paweł Baran,
all at the AGH University of Science and Technology; the preprint is
CC-BY.  They made a geopolymer insulator incorporating significant
amounts of (lignite) fly ash, which is widely recycled for
construction uses in the EU (43%) but not in Poland (0.7%, 33.7
thousand tonnes out of 5.01 million), and some of their samples
withstood temperatures of over 1200°, while others softened at only
850°.  The other ingredients were perlite, waterglass, lye, and
hydrogen peroxide.  It had low thermal conductivities from 0.086W/m/K
up to 0.232W/m/K due to porosities in the range 16%–61%, with
compressive strengths in the range 1.35–4.98MPa.

[2]: https://assets.researchsquare.com/files/rs-802844/v1/f0fda48b-e1e7-4e4a-952a-6ca390d859d6.pdf?c=1631888389
[3]: https://www.researchsquare.com/article/rs-802844/v1

The paper unfortunately begins with several pages of filler about the
importance of the circular economy and whatnot, which does have the
benefit of including a nice definition of “geopolymer”:

> Geopolymers are inorganic polymeric aluminosilicate materials,
> similar to the ceramic material [sic]. Their Si-O-Al structure
> (tetrahedral chains of [SiO₄] and [AlO₄]) forms a three-dimensional
> network (Koleżyński et al., 2018; Kumar and Kumar, 2011; Ma et al.,
> 2018). These materials consist of long chains (copolymers) of
> silicon and aluminum oxides, metal cations stabilizing them, most
> often sodium, potassium or lithium and bound water (Duxson et al.,
> 2007).

Their fly ash was 45% silica and 85% silica, alumina, and lime
(“mainly quartz and mullite”).

Due to the EP 150 perlite (which they say was “40–90 kg/m³,”
apparently not having bothered to measure its density) mixed 1:1 with
the fly ash, and in one case due to including about 1% hydrogen
peroxide, the resulting substance was porous and therefore insulating.
The thermal conductivities of the substances produced
(0.086–0.232W/m/K) were much higher than those of the perlite
(0.045–0.059W/m/K), though in their conclusion they say they are
“similar”.

They are especially proud of their “SII4” (series II #4) material,
which was the one with the 0.086W/m/K conductivity, saying it “fully
meets the requirements for thermal insulation in Poland (...) as a
material for single-layer walls.”  Unfortunately they report that that
one didn’t have any fly ash in it: it had 57.1g of perlite and 236.8g
of liquid components (one part by mass 8 mol/dm³ NaOH, two parts
sodium waterglass, and 0.036 parts of 30% H₂O₂), cured at 342 K (69°)
for 24 hours.  Unfortunately for my purposes, it was also the least
refractory, melting at only 850°, even lower than the 900° of its
perlite component, presumably because the sodium fluxes it.

Their SI3 sample (series I #3) seems more interesting to me: melting
point above 1200°, 1.375 MPa compressive strength, 0.197W/m/K, made
with 114.5g of a 1:1 mix of fly ash and perlite bound with a 1:2 by
mass mix of 8mol/dm³ KOH and potassium waterglass, cured at 363 K
(90°), also for 24 hours.

The preprint doesn’t mention the modulus or concentration of either
waterglass.  I emailed the corresponding author to ask; she explained
that the sodium waterglass was minimum 39% solids, with a molar
modulus of 2.4–2.6 silicon to sodium, but didn’t mention the potassium
case.

Just after doing this, I found that [the paper had been published][4].
Unfortunately, I don’t have access.

[4]: https://link.springer.com/article/10.1007/s10098-022-02309-x

******

[Kotsay and Grabowski][5] published an open-access paper this year
(MDPI, CC-BY, [also on ResearchGate][6]) on making geopolymer
construction cement from modulus-1.5 waterglass (*i.e.*, 1.5 silicons
per sodium), metakaolin, and waste soda-lime glass.  They find that
the soda-lime glass can substitute for at least half the metakaolin in
the usual mix, also contributing alkali and thus reducing the need for
waterglass; their highest-compressive-strength specimen was
“50SLWG0.5”, “50% waste glass” (by weight, among the solids) with a
liquid-to-solid ratio of 0.5, achieving an impressive 76.9 MPa
compressive strength when, if I understand correctly, cured at 80° for
7 days; though, actually, all the geopolymer specimens thus cured were
close to that.

[5]: https://www.mdpi.com/1996-1944/16/15/5392
[6]: https://www.researchgate.net/publication/372847355_Properties_of_Geopolymers_Based_on_Metakaolin_and_Soda-Lime_Waste_Glass/fulltext/64ca5364806a9e4e5cdfb73a/Properties-of-Geopolymers-Based-on-Metakaolin-and-Soda-Lime-Waste-Glass.pdf?origin=publication_detail

They have a nice table describing their ingredients on p. 3/12.  The
waterglass was 62% water by weight, 29% SiO₂, and 9% Na₂O equivalent.
This unfortunately contradicts their 1.5 modulus; 1.5 moles of SiO₂
would be 90g, while a mole of Na₂O would be 62g, which would mean that
a 29%-by-weight SiO₂ solution would be 20% Na₂O, not 9%.  Perhaps
they’re describing the waterglass not in the form in which they used
it, after alkalizing with lye, but in the form in which they bought
it, which would be modulus 3.32, which is roughly the upper limit of
water-solubility, the form in which it is most commonly sold because
it minimizes safety concerns.

They also say their soda-lime waste glass was 72% SiO₂, 1% Al₂O₃, 12%
CaO+MgO, and 14% Na₂O (equivalent) by mass.  I take these numbers to
be representative of the class of materials rather than measured on
their particular material.  They also report that it has 3987m²/g of
surface, and the metakaolin (52% SiO₂, 41% Al₂O₃, 1.4% Na₂O, etc.) has
20'000m²/g.

The granulometry of their waste glass is not clear to me, though they
call this out as an important consideration in using waste glass in
portland-cement concrete.

The “50SLWG0.5” material is listed as having a 6.04 molar ratio of
SiO₂/Al₂O₃, a 1.40 molar ratio of Na₂O/Al₂O₃, and an 0.23 molar ratio
of Na₂O/SiO₂.  These are consistent, in that 6.04/1.40 = 0.23.  They
presumably calculated this from the numbers above and the molar
masses.
