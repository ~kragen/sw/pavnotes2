I was thinking about how to do stuff with air, heat, humidity,
desiccants, etc., and it occurred to me that possibly hollow bricks
are the best available building blocks.

The context is that you have some network of reaction chambers that
you want to maintain at different temperatures, move heat between,
move air between, move flue gases between, etc., for purposes such as
the following:

1. Making beef jerky.
2. Drying fruit.
3. Regenerating household desiccants.
4. Removing humidity from inside a cabinet and condensing it into a
   tank.
5. Dehydrating poop in a so-called “composting” toilet to keep it from
   actually “composting” before you move the poop to a composting
   place you aren’t trying to live inside of.
6. Dehydrating food waste (bones, potato peels, coffee grounds, used
   yerba mate, etc.) to keep it from “composting” and reduce its
   weight.
7. Burning confidential documents.
8. Converting calcium carbonate to quicklime.
9. Dehydrating 3-D printer filament before printing with it to prevent
   hydrolysis in the hotend.
10. Steam distillation of essential oils.
11. Firing pottery.
12. Reflowing solder on printed circuit boards.
13. Cooking food.
14. Harvesting drinking water from moist air.
15. Heating indoor air up to a comfortable temperature.
16. Cooling indoor air *down* to a comfortable temperature.
17. Freezing food for preservation.
18. Melting metals for metal casting.
19. Incinerating food waste.

Most of these can be done with temperatures in the 0°–100° liquid
range of water, though a few need a wider temperature range, and
almost all at ambient atmospheric pressure.  Many of them need
materials that can *withstand* a wider temperature range, though.

The most common hollow bricks, for example from Ctibor, cost [US$65
for a 144-brick pallet][0], are extruded red (earthenware) clay that
is then fired unglazed, measure 120mm × 180mm × 330mm, and have nine
longitudinal passages in them.  This works out to about 45¢ a brick,
but you can [buy a single brick for about 130¢][1].  There are plenty
of variations: two-thirds-thickness bricks with only six tubes,
double-thickness bricks with 12, load-bearing bricks (“ladrillos
portantes”) where the passages are vertical, etc.  They have in common
that they will all withstand temperatures up to 1000°, as well as
arbitrarily low temperatures, though thermal shock can break them
through uneven expansion.

[0]: https://www.mercadolibre.com.ar/ladrillo-hueco-12x18x33-9aug-1ra-pallet-144-u/p/MLA36268137
[1]: https://articulo.mercadolibre.com.ar/MLA-857814065-ladrillo-hueco-ceramico-12x18x33-9-tubos-marca-la-pastoriza-_JM

The brick’s ceramic material is relatively soft as ceramics go, and
can be cut and drilled, though not easily and not entirely without
risk of breakage; you can use a masonry drillbit, [a carbide-tipped
masonry saw blade][2] on a reciprocating saw, a [diamond
concrete-cutting blade on a circular saw][3] or [an angle grinder][4],
ideally [with water][5] and usually requiring chisel work to finish
the job, though [a hand-driven reciprocating saw with a masonry
blade][6] is another option.  All of this would be enormously easier
to [cut before firing the brick][7], 

[2]: https://www.youtube.com/watch?v=4qh_Ks_Fkww
[7]: https://www.youtube.com/shorts/4fwK3kodESo
[6]: https://www.youtube.com/watch?v=DXNlSQZxrMM
[5]: https://www.youtube.com/watch?v=A6laahdq6Go
[4]: https://www.youtube.com/watch?v=_Z-MBUJv7HI
[3]: https://www.youtube.com/watch?v=Ot0yExHmUxc
