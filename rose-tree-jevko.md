(Edited from <https://news.ycombinator.com/edit?id=33336283>.)

I was reading about Dariusz/Darius J “djedr” Chuck’s [Jevko][0]:

    Jevko = *("[" Jevko "]" / "`" ("`" / "[" / "]") / %x0-5a / %x5c / %x5e-5f / %x61-10ffff)

[0]: https://jevko.github.io/

For example:

    first name [John]
    last name [Smith]
    address [
      street address [21 2nd Street]
      city [New York]
    ]

He says this is simpler than S-expressions, which we could maybe write
in the same ABNF as

    ex = ("(" *ex ")" / *%x2a-10ffff) *%x0-20

We could write the data model corresponding to this grammar in OCaml as

    type ex = List of ex list | Atom of string

If we add double-quoted strings with GW-BASIC/SQL-style quoting (and
resolve the ambiguity greedily):

    ex = ("(" *ex ")" / *%x2a-10ffff / %x22 *(%x22 %x22 / %x0-21 / %x23-10ffff) %x22 ) *%x0-20

This corresponds to the data model

    type ex = List of ex list | String of string | Symbol of string

This still seems both simpler and more expressive than Djedr’s
one-line grammar, and it correctly parses Djedr’s examples of
S-expressions like

    (first-name"John"last-name"Smith"is-alive true age 27
     address(street-address "21 2nd Street"
             city"New York"state"NY"postal-code"10021-3100")
     phone-numbers((type "office"number"212 555-1234")
                   (type "home"number"646 555-4567"))
     children()spouse())

or

    (
        :first-name "John"
        :last-name "Smith" ...
    )

though of course the name-value pairing is lost there (because
S-expressions lack it).

The data model implied by this *abbreviated* Jevko grammar is
something like

    type jevko = atom list
    and atom = Char of char | Nest of jevko

but Jevko’s intended semantics are more clearly expressed by a grammar
like

    Jevko = *Subjevko Text
    Subjevko = Text "[" Jevko "]"
    Text = *(Digraph / Character)
    Digraph = "`" ("`" / "[" / "]")
    Character = %x0-5a / %x5c / %x5e-5f / %x61-10ffff

Essentially at this level of structure a Jevko is a (possibly empty)
set of name-value pairs followed by a plaintext body (“Suffix”).  This
is exactly like an email message, except that the values are
themselves Jevkos.  In OCaml we could write:

    type jevko = Jevko of (string * jevko) list * string

For Djedr’s example

    include[author]fields[articles[[title][body]]people[[name]]]

this gives the representation

    Jevko
     ([("include", Jevko ([], "author"));
       ("fields",
        Jevko
         ([("articles",
            Jevko ([("", Jevko ([], "title")); ("", Jevko ([], "body"))], ""));
           ("people", Jevko ([("", Jevko ([], "name"))], ""))],
         ""))],
     "")

This serializes to the above with this OCaml:

    (* XXX doesn’t escape `[ `] `` *)
    let rec dump (Jevko(hdrs, body)) = hdr(hdrs) ^ body
    and hdr = function [] -> "" | (k, v) :: t -> k ^ "[" ^ dump(v) ^ "]" ^ hdr(t)

    let dict kvs = Jevko(kvs, "")
    let text s = Jevko ([], s)

    let v = dict ["include", text "author";
                  "fields", dict [
                      "articles", dict ["", text "title"; "", text "body"];
                      "people", dict ["", text "name"]
                 ]]
    ;;

    print_endline(dump v)

This is almost a rose tree: an *ordered tree* with *labeled arcs* and
*labeled nodes*.  It differs from rose trees by adding node labels.

I think this is an *extremely useful* data model.  Email messages are
the one and only structured data format that has remained compatible
in active use and extension for over half a century; you can literally
take internet email messages from 01972 and load them into a mail
client today (most easily by putting them into a qmail-style maildir)
and everything will just work.  This is largely a result of the
decentralized extensibility properties of name-value pairs: mail
clients just ignore header names they don’t understand, and they don’t
require header names that weren’t originally present.  This is also
the basis of the extensibility of HTTP/1.0 and HTTP/1.1.

It’s also very similar to the data model of popular “semistructured”
or “free-form” databases of the 01990s like askSam and Filemaker.
Unlike RFC-822 and HTTP/1, but like Jevko, those systems support
recursively nested data.  askSam even used almost the same
firstname[John] syntax.

In this form it doesn’t have any syntactic flexibility: there is a
bijective relationship between ASTs and parseable byte strings.  In
particular it prohibits newlines that are semantically ignored; you
cannot reformat

    first name[John]last name[Smith]

to even

    first name[John]
    last name[Smith]

without changing its semantics.

If we want to simplify the data model to purely a rose tree

    type tree = Node of (string * tree) list

we could either declare trailing strings (bodies, suffixes) illegal or
we could declare that nonempty trailing strings are followed by an
implied `[]`, so that `a[b[c]d]` is equivalent to `a[b[c[]]d[]]`.

If we make that change, and additionally declare that leading and
trailing whitespace in labels are not significant, and for good
measure switch from `[]` to the more harmonious `()`, we can write the
above as

    first name (John)
    last name (Smith)

    player [
        name [singleplayer] pitch [3.49] yaw [84.6]
        inventory [
            width [0] size [32] name [main]
            item [qty [1] wear [63336] default:pick_bronze]
            item [qty [1] wear [38994] default:shovel_bronze]
            item [qty [1] wear [24388] default:axe_steel]
        ]
    ]


quotes

If your audience is people like me, I think it would probably be worthwhile for you to spend some time up front describing the intended semantics of a data model, as I've attempted above, rather than leaving people to infer it from the grammar.  (Maybe OCaml is not a good way to explain it, though.)  You might also want to specify that leading and trailing whitespace in prefixes is not significant, though it is in the suffix ("body"); this would enable people to format their name-value pairs readably without corrupting the data.  As far as I can tell, this addendum wouldn't interfere with any of your existing uses for Jevko, though in some cases it would simplify their implementations.

______

