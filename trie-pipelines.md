I was rereading the old discussion of McIlroy's famous 1-gram tabulating shell script in response to Knuth's hash-trie implementation in Bentley's column, and it occurred to me that perhaps it would be reasonable to make a sort of "shell-script Redis" that consumes a sequence of commands and emits a sequence of responses; essentially reifying the trie (or similar dictionary structure) as a Unix process.  If desired you could persist its state to a file, thus obtaining a primitive database, though I would contend that dumping processes to files for later resurrection is probably functionality that belongs properly to an OS rather than the programs themselves.

Redis itself at this point supports [a huge number of commands][0] including a specific "TopK" command group, but I had in mind a more parsimonious approach. Something like its [HINCRBY][1] and [HSCAN][2] commands would be sufficient for this narrow purpose, though they aren't really designed for the case where the command output goes somewhere else than to the requester, and they also have an extra parameter to support the existence of many hashes.

[0]: https://redis.io/commands/
[1]: https://redis.io/commands/hincrby/
[2]: https://redis.io/commands/hscan/

LevelDB seems somewhat closer; [its basic command set][3] is Open, Close, Put, Get, Delete, and a set of iteration commands: Seek, SeekToFirst, SeekToLast, Valid?, Next, Prev.  The iteration results contain a key and a value; the Get result is just a byte string. LevelDB also has a WriteBatch command to atomically apply a set of updates and a GetSnapshot/ReleaseSnapshot command pair to provide a point-in-time consistent view of the database.

[3]: https://github.com/google/leveldb/blob/main/doc/index.md

Adapting this interface to a situation where you're streaming in a bunch of update commands probably requires a slightly more elaborate interface in some ways.  You don't need Open and Close, but any iteration and any dependency between an old value and a new value has to be built in to the "server".

With respect to iteration, you could ask for iteration from key K1 to key K2, with just the keys, just the values, both, or just a count of pairs, but you might plausibly want to limit the number or the byte size of the results, etc.

With respect to updates, Redis offers [five forms of updates][4] to its hashes: set, set if not existent, increment by int, increment by float, and delete. For lists it offers [N forms of updates][5]: push, pop, pop from one list and push onto another, pop from any of multiple lists (blocking until one is nonempty), insert an element, remove elements from both ends, etc.  This is probably not exemplary; it seems to be designed for using Redis as a queueing system. I suspect that a good set of commands for the blob-oriented system I have in mind would be Set, Default (set if not existent), Add, Delete, and AppendBytes.

[4]: https://redis.io/commands/?group=hash
[5]: https://redis.io/commands/?group=list

This suggests the following set of nine commands:

- echo $string: writes $string to the output.
- save: ends the current "transaction", committing any writes, and
  starts a new one. Each "transaction" has a consistent snapshot view
  of the database (except that it sees its own writes), and its writes
  are not visible to other transactions or saved on disk until and
  unless it saves. These transactions are not fully ACID if there
  might be multiple concurrent transactions because nothing prevents
  inconsistency between concurrent transactions.
- abort: also ends the current transaction and starts a new one, but
  discards writes instead of committing them.
- delete $key: deletes $key if it is present.
- append $key $val: updates $key by appending $val to its value or to
  the empty string if not present.
- inc $key $val: updates $key by adding $val to its value or to 0 if
  not present.
- set? $key $val: if $key is not present, sets it to $val.
- get $key $nonexistent: writes the value associated with $key to the
  output, or $nonexistent if $key is not present.
- iterate %args: iterates over a sorted sequence of keys in the
  database, with the following arguments, all optional:
    - output: either keys, values, items, or count, to output
      respectively just the keys, just the values, both the keys and
      values, or the number of key-value pairs. The default is items.
    - start: the first key to include in the iteration, defaulting to
      the first one in the table.
    - stop: the first key not to include in the iteration; no keys
      after it will be included. Defaults to something after the last
      key.
    - limit: the maximum number of results to iterate over, default
      all of them.
    - offset: the number of results to skip at the beginning, default
      zero.
    - delimiter: a string to output before each key and each value,
      default none.

Command-line flags when you launch the thing should specify the disk file to use (if any), the output format (JSON, raw binary, tab-delimited, space-delimited, CSV, NUL-delimited, netstrings), and the input format (JSON, tab-delimited, space-delimited, NUL-delimited, netstrings).
