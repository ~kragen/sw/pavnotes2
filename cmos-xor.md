I was trying to figure out how many transistors you’d need for the
combinational logic to increment a binary value, which involves
calculating an output digit and a carry-out for each bit.  The
carry-out is the AND of the input bit and the carry-in (which is 1 at
the LSB), while the output digit is the XOR of the input digit and the
carry-in.  That is, we need a half-adder per bit.  The crucial
question there is how small you can make the XOR, which, being
nonmonotonic, is especially heavy in something like CMOS.

Doing things absolutely by the book, here’s a CMOS XOR gate made out
of two inverters and three CMOS NOR gates, totaling 16 transistors, in
Falstad’s circuit.js:

![(CMOS XOR schematic)](./cmos-xor-1.png)

    $ 1 0.000005 10.20027730826997 50 5 43 5e-11
    S 112 144 32 144 0 0 false 0 2
    S 112 448 32 448 0 1 false 0 2
    r 32 432 32 368 0 1000
    R 32 368 32 336 0 0 40 5 0 0 0.5
    r 32 464 32 528 0 1000
    g 32 528 32 544 0 0
    r 32 160 32 208 0 1000
    g 32 208 32 224 0 0
    r 32 128 32 80 0 1000
    R 32 80 32 32 0 0 40 5 0 0 0.5
    w 688 256 720 256 0
    162 720 256 720 384 2 default-led 1 0 0 0.01
    r 720 384 720 432 0 1000
    g 720 432 720 448 0 0
    w 112 144 112 208 0
    f 112 208 192 208 32 1.5 0.02
    g 192 224 192 240 0 0
    w 112 144 112 80 0
    f 112 80 192 80 33 1.5 0.02
    R 192 64 192 -16 0 0 40 5 0 0 0.5
    w 192 96 192 144 0
    w 192 144 192 192 0
    w 192 448 192 496 0
    w 192 400 192 448 0
    R 192 368 192 336 0 0 40 5 0 0 0.5
    f 112 384 192 384 33 1.5 0.02
    w 112 448 112 384 0
    g 192 528 192 544 0 0
    f 112 512 192 512 32 1.5 0.02
    w 112 448 112 512 0
    w 112 384 112 304 0
    w 448 176 448 192 0
    w 448 96 448 144 0
    R 448 0 448 -32 0 0 40 5 0 0 0.5
    f 368 80 448 80 33 1.5 0.02
    g 448 224 448 240 0 0
    f 368 208 448 208 32 1.5 0.02
    f 368 32 448 32 33 1.5 0.02
    w 448 48 448 64 0
    w 448 0 448 16 0
    f 256 208 336 208 32 1.5 0.02
    g 336 224 336 240 0 0
    w 336 144 336 192 0
    w 336 144 448 144 0
    w 368 80 368 208 0
    w 368 32 256 32 0
    w 256 32 256 208 0
    w 112 80 112 32 0
    w 112 32 256 32 0
    w 112 304 368 304 0
    w 368 208 368 304 0
    w 256 352 256 528 0
    w 368 352 256 352 0
    w 368 448 368 528 0
    w 336 464 448 464 0
    w 336 464 336 512 0
    g 336 544 336 560 0 0
    f 256 528 336 528 32 1.5 0.02
    w 448 320 448 336 0
    w 448 368 448 384 0
    f 368 352 448 352 33 1.5 0.02
    f 368 528 448 528 32 1.5 0.02
    g 448 544 448 560 0 0
    f 368 400 448 400 33 1.5 0.02
    w 448 448 448 464 0
    w 448 464 448 512 0
    w 192 144 224 144 0
    w 224 144 224 352 0
    w 224 352 256 352 0
    w 368 448 192 448 0
    R 448 320 448 288 0 0 40 5 0 0 0.5
    w 368 400 368 448 0
    w 608 352 608 448 0
    w 448 176 496 176 0
    w 496 176 496 352 0
    w 608 176 496 176 0
    w 608 224 608 352 0
    w 576 288 688 288 0
    w 576 288 576 336 0
    g 576 368 576 384 0 0
    f 496 352 576 352 32 1.5 0.02
    w 688 144 688 160 0
    w 688 192 688 208 0
    f 608 176 688 176 33 1.5 0.02
    f 608 352 688 352 32 1.5 0.02
    g 688 368 688 384 0 0
    f 608 224 688 224 33 1.5 0.02
    R 688 144 688 112 0 0 40 5 0 0 0.5
    w 688 256 688 288 0
    w 688 288 688 336 0
    w 448 144 448 176 0
    w 608 448 448 448 0
    w 448 416 448 448 0
    w 688 240 688 256 0

In equations, given inputs A and B:

> C = -A  
> D = -B  
> E = -(A+B)  
> F = -(C+D)  
> G = -(E+F)  

CMOS NOR and NAND are just slight rearrangements of one another, but
by virtue of doing this with NOR instead of NAND we get the carry-out
for free; it’s F.  So this XOR is really a half-adder all by itself.

Well, in this form, incrementing a 16-bit value takes 256 transistors,
assuming no lookahead carry.  Well, 242, because the LSB just needs a
NOT.  But that’s a lot!  Can’t we do better?  What about pass
transistor logic?

In theory a mux with pass transistors requires a dual-rail select
input and two pass transistors per input, four pass transistors for 2
inputs plus an input inverter, 6 total.  So if we select between A and
-A with a B-driven mux, we should be able to get an 8-transistor XOR.

And that does seem to work in simulation:

![(Schematic of MUX-based CMOS XOR)](./cmos-xor-2.png)

    $ 1 0.000005 10.20027730826997 50 5 43 5e-11
    S 112 144 32 144 0 0 false 0 2
    S 112 448 32 448 0 1 false 0 2
    r 32 432 32 368 0 1000
    R 32 368 32 336 0 0 40 5 0 0 0.5
    r 32 464 32 528 0 1000
    g 32 528 32 544 0 0
    r 32 160 32 208 0 1000
    g 32 208 32 224 0 0
    r 32 128 32 80 0 1000
    R 32 80 32 32 0 0 40 5 0 0 0.5
    w 496 64 528 64 0
    162 528 64 528 192 2 default-led 1 0 0 0.01
    r 528 192 528 240 0 1000
    g 528 240 528 256 0 0
    w 112 144 112 208 0
    f 112 208 192 208 32 1.5 0.02
    g 192 224 192 240 0 0
    w 112 144 112 80 0
    f 112 80 192 80 33 1.5 0.02
    R 192 64 192 32 0 0 40 5 0 0 0.5
    w 192 96 192 144 0
    w 192 144 192 192 0
    w 192 448 192 496 0
    w 192 400 192 448 0
    R 192 368 192 336 0 0 40 5 0 0 0.5
    f 112 384 192 384 33 1.5 0.02
    w 112 448 112 384 0
    g 192 528 192 544 0 0
    f 112 512 192 512 32 1.5 0.02
    w 112 448 112 512 0
    w 112 384 112 304 0
    w 112 80 112 -32 0
    w 112 -32 224 -32 0
    w 224 -96 320 -96 0
    w 224 -32 224 32 0
    w 224 32 320 32 0
    w 352 32 400 32 0
    w 352 -96 400 -96 0
    w 400 -32 400 32 0
    w 192 448 304 448 0
    w 272 -48 336 -48 0
    w 192 192 224 192 0
    w 400 -32 496 -32 0
    f 336 -48 336 -96 97 1.5 0.02
    w 304 -16 336 -16 0
    R 336 -96 336 -128 0 0 40 5 0 0 0.5
    g 336 32 336 48 0 0
    w 224 -96 224 -32 0
    w 400 -96 400 -32 0
    w 400 128 400 192 0
    w 224 128 224 192 0
    R 336 128 336 96 0 0 40 5 0 0 0.5
    w 272 208 336 208 0
    f 336 176 336 128 97 1.5 0.02
    w 400 192 496 192 0
    w 304 176 336 176 0
    w 400 192 400 256 0
    w 352 128 400 128 0
    w 352 256 400 256 0
    w 224 256 320 256 0
    w 224 192 224 256 0
    w 224 128 320 128 0
    w 272 -48 272 208 0
    w 304 -16 304 176 0
    w 496 -32 496 64 0
    w 496 64 496 192 0
    w 112 304 272 304 0
    w 272 304 272 208 0
    w 304 176 304 448 0
    g 336 256 336 288 0 0
    f 336 -16 336 32 96 1.5 0.02
    f 336 208 336 256 96 1.5 0.02

This is something I can’t breadboard out of discrete transistors
because I can’t find MOSFETs with a separate body terminal.  But then
again CMOS logic doesn’t really make sense for fabricating out of
discrete parts; better to use diode logic, so an AND or OR costs you
two diodes instead of 6 transistors.  It’ll be faster than CMOS at
that scale too.

There’s a 6-transistor variant of this in Falstad’s example circuits
([Logic families > CMOS > CMOS XOR][0]) which also uses four pass
transistors but only a single inverter, on input A.  A PMOS pass
transistor controlled by input B connects A to the output when input B
is low (and either A or the output is high, since enhancement-mode
MOSFETs don’t connect shit when all their terminals are at the same
voltage).  A second PMOS pass transistor, *controlled by input A*,
connects input *B* to the output when input *A* is low.  Each of these
PMOS pass transistors has its NMOS counterpart, the first connecting
-A to the output when B is high, the second connecting B to the output
when -A is high.  This is not decomposable into analog switch
circuits, because loads on the output may get passed along to either A
or B depending on the polarity of the load and the desired output.
You can convert it into an XNOR gate by swapping A and -A.

[0]: https://tinyurl.com/y8md5ngf

These pass-transistor-based gadgets can’t be chained indefinitely
because eventually the resistance will add up and they’ll get too
slow, but they’re being used to calculate output bits, not the carry
chain, so this doesn’t matter.  Unlike the NOR version I started with,
they also don’t inherently provide the AND needed for carry-out.

A regular CMOS AND gate is 6 transistors: a 4-transistor NAND and an
inverter for its output.  So a straightforward half adder done this
way requires 12 transistors per bit, 182 for a 16-bit increment (not
192 since, again, the LSB only requires an inverter).

But what happens if we get rid of that inverter?  We get an
inverted-sense carry bit.  Well, we can handle that just fine in the
next bit up: use XNOR insted of XOR to compute the sum bit, and
calculate the NOR of the inverted input bit and the inverted-sense
carry-in bit to get another inverted carry-out.  You don’t need any
extra transistors to invert the input bit because you can use the
inverter on one of the XNOR inputs.  So that gets your half adder down
to 10 transistors, 152 transistors for a 16-bit increment.

([There’s a 6-transistor circuit that purports to be a CMOS half
adder][1] but I don’t understand if or how it works.  The paper says
conventional CMOS half adders use 16 transistors.)

[1]: https://www.researchgate.net/figure/Schematic-diagram-of-existing-half-adder-using-Static-CMOS-technique_fig3_320557527

A 16-bit adder requires 15 full adders and a half adder, or 14 and two
half adders if carry-out is discarded, 300 transistors of half adders.
Then I guess you also need 14 OR (or NOR or NAND) gates to merge the
carries out of the two half-adders on each bit; supposing it’s NOR or
NAND, that’s 56 more transistors, total 356.

[The ARM1 actually did use pass-transistor-based CMOS transmission
gates for its adders][2] but spent 29 transistors per full adder
rather than the 24 I’ve computed above.  I haven’t dug into the
circuit enough to see where the difference is and whether I’ve just
got it wrong.

[The Z80 also used pass-transistor-based XOR, but in NMOS][3], in a
way analogous to the 6-transistor CMOS circuit described above: an
N-MOSFET which pulls the output down to -A when -B is high and an
N-MOSFET which pulls the output down to -B when -A is high, with the
usual depletion-load pullup on the output.  So if only one of the two
is high, the high side enables a transistor that pulls the output down
to the low side.  If they are both low, both transistors are in
cutoff, so they don’t pull the output down, and the output pullup can
pull it up.  And if they’re both high, all three electrodes of the
transistors are at Vdd, so again they can’t pull the output down.
This uses 6 enhancement-mode transistors for switching and 5
depletion-mode transistors as pullups.

[2]: http://www.righto.com/2016/01/counting-bits-in-hardware-reverse.html
[3]: http://www.righto.com/2013/09/understanding-z-80-processor-one-gate.html
