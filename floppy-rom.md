Today, Jeremiah Orians called my attention to one of the earliest
efforts to distribute mass-market software, Interface Age’s
[Floppy-ROM][0] from May 01977.  This was evidently a [flexi disc][1]
or phonosheet (the article uses the term “sound Sheet”) with data on
it encoded according to the 300-bit-per-second [Kansas City
Standard][2] (with a UART-style start bit and stop bit); once you cut
the master, you could stamp out thin vinyl copies of your software for
pennies and enclose them in magazines or envelopes.  The flexi-disc
manufacturer, EVA-TONE, had a 6½-minute time limit.

[0]: https://archive.org/details/Interface_Age_vol02I06_1977_May
[1]: https://en.wikipedia.org/wiki/Flexi_disc
[2]: https://en.wikipedia.org/wiki/Kansas_City_standard

This works out to about 11.7 kilobytes, plenty for the Motorola 6800
BASIC interpreter that was their payload, but enormously short of the
data capacity of the medium.  Suppose the effective number of bits of
a vinyl record is normally 10 bits (60 dB SNR) and maybe 8 bits in the
case of a more fragile flexi disc, and the bandwidth is 20 kHz per
channel.  There’s a one-to-one correspondence between a
20-kHz-bandlimited signal and a 40-ksps sampled signal (the
relationship being convolution with an appropriate sinc), so this is
equivalent to 40000 8-bit samples per second per channel.  That’s 320
kilobits per second per channel, 640 kilobits per second for a stereo
record.  A 6½-minute flexi disc could hold 31 megabytes; a 46-minute
double-sided LP could hold 220 megabytes.  A 90-minute long-playing
stereo cassette tape with half the bandwidth and an ENOB of,
pessimistically, 5, would not be quite as good, at 135 megabytes.

This would of course not have been useful for the usual cassette-tape
loading and saving use case: there were no data paths in personal
computers of the time that could feed 640 kilobits per second onto a
cassette tape, and no memories that held megabytes or even kilobytes.
But it was common for PCs of the time, built around a 6502, 6800, or
8080, to process 2400-baud data streams, to and from terminals or
modems, sometimes both at once.  And the phonograph record is an
isochronous medium; data streaming in from it could not even be
paused, due to the angular momentum of the turntable.  (By hand, you
could randomly seek to a different “track” of the groove, but record
players of the time generally could not do this under electrical
control.)

What kind of medium could have been built with these pieces in an
alternate history?

Suppose that we use frequency-division multiplexing to modulate a
number of 2400-bps data streams together into an audio signal, like
OFDM in modern Wi-Fi, then a simple circuit like an AM radio tuner to
demodulate just one of those streams at a time, under software
control.  The interface circuit in Interface Age (p. 30, 32/152,
“Figure 1. SWTPC AC-30 Cassette Tape Modulator/Demodulator schematic”)
uses 3 D flip-flops, 8 CMOS two-input logic gates, 4 discrete
transistors, 11 capacitors, 21 resistors (including a trimpot), and 3
diodes, probably 43 components in all.  So the equivalent of an AM
radio tuner with AGC on the output would have been within scope, and
operating at audio frequencies instead of radio frequencies would have
made it easier rather than harder.

Suppose we arbitrarily decide to use the band from 9kHz to 15kHz;
being less than an octave would simplify the decoding circuitry,
though 6 kHz is a little less than a third of the total bandwidth.  If
we use simple on-off keying in each frequency band, we need about
1200 Hz for each band, so we get 5 2400-bps bands on each channel and
10 in stereo, a total of 24000 bps, so in 6½ minutes we get 1.2 MB.
(For the time being, we’re disregarding issues of turntable wow and
flutter, nonlinearity, systematically fast or slow playback speed, and
dc balance on the signal.)

This is 40 times better than the Kansas City Standard, and bigger than
the floppy disks of the time (I think double-density 8" disks and
high-density 5¼" disks, each about that size, came a few years later),
but it’s still 25 times worse than the 31-megabyte theoretical
capacity.  A factor of 3.3 of this is due to using only a small amount
of the bandwidth, and the other factor of 8 is that our ENOB is
hypothetically 8, but we’re only getting one bit per (demodulated)
sample, so our constellation diagram just has two points: one at the
origin and one on the real line.

As I understand it, the standard way to solve this problem is to use
something like 256-QAM, so that each encoded symbol carries 8 bits of
information instead of 1, but lasts 8 times as long, so a
2400-bit-per-second signal is only 300 symbols or tones per second
(“baud”), so only occupies 150 Hz of bandwidth instead of 1200 Hz.
Then our 6 kHz gets broken down into 40 subbands instead of just 5,
and our stereo record lets us select from among 80 2400-bps streams
totaling 192 kbps.  I feel like a 256-QAM demodulator, especially with
the necessary ECC, would be demanding a lot from the hardware at the
time.  Given the possibility of including a broadband absolute
time-domain reference on the phonorecord, spread-spectrum and
ultrawideband modulations might be more practical; perhaps a simple
7-bit LFSR and a bit of auxiliary circuitry would be enough to
CDMA-demodulate one out of 254 or so 2400-baud channels.

So suppose you have such a medium: a 23-minute-per-side LP record with
254 2400-baud channels on it, totalling 220 megabytes; a simple
circuit which can demodulate any one of those channels and switch
among them under software control; and a sub-MIPS personal computer.
What would you want to have on this record?  It can hold the
equivalent of about 2000 floppy disks of the time, 200 boxes full, but
can’t access them randomly.

A simple, stupid thing you can do is to record only 15 seconds of data
(4500 bytes) in each of the channels, but repeat them over and over
from the beginning of a side of the record.  This gives you 160 blocks
of 4500 bytes, 80 on each side of the record, any of which can be read
at any time with 15 seconds maximum latency, or any of which can be
read immediately as soon as the previous block finishes; in effect you
can read the 80 blocks on a given side of the record in any random
order.

