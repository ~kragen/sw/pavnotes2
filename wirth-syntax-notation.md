Writing file `comfy-expressions.md` I ran across Wirth’s 01977 Short
Communication, [“What Can We Do about the Unnecessary Diversity of
Notation for Syntactic Definitions?”][0], which proposes the following
notation.  It uses `{}` for zero-or-more repetition and doubles quotes
within quotes.

    syntax     = {production}.
    production = identifier "=" expression ".".
    expression = term {"|" term}.
    term       = factor {factor}.
    factor     = identifier | literal | "(" expression ")"
               | "[" expression "]" | "{" expression "}".
    literal    = """" character {character} """".

[0]: https://dl.acm.org/doi/10.1145/359863.359883

I think this is somewhat simpler than the “minimal PEG language” from
[peg-bootstrap][1], which is 10 lines instead of seven, but that is
mostly because it handles whitespace, string quoting, and identifiers
(the tokenization concerns) directly instead of relegating them to an
implicit tokenization layer.  Wirth’s grammar carefully avoids left
recursion and nondeterminism (perhaps unsurprisingly, given his
enthusiasm for recursive-descent parsing), so you could easily treat
it as a PEG.  His choice of symbology is, I think, clearer.

[1]: https://github.com/kragen/peg-bootstrap/blob/master/peg.md#a-minimal-peg-language

This is an improvement from the symbology he used in [the PL/360
Stanford tech report][2] in 01965, in which he had no notation for
optional or repeated groups; for example (§3.1, p. 6 (p. 8/25)):

    <identifier> ::= <letter> | <identifier><letter> | <identifier><digit>

[2]: http://bitsavers.org/pdf/stanford/cs_techReports/CS33_Wirth_PL360_Dec65.pdf

He also wrote a grammar that produced the grammar; a production
following that one was:

    <ℱ register> ::= <identifier>

which he explains is shorthand for

    <general register> ::= <identifier>
    <floating register> ::= <identifier>
    <floating double register> ::= <identifier>

This format also uses the clumsy but unambiguous `::=` and requires
backtracking upon encountering it.

(Also in 01965, [in another Stanford tech report, Wirth and Helmut
Weber described Euler][7], which uses a very different notation for
its grammar, with productions like `φ₄ : body- → decl ; body-`, with
the nonterminals underlined.  Euler is mostly notable for being one of
the earliest attempts to formalize programming language semantics, .)

[7]: http://i.stanford.edu/pub/cstr/reports/cs/tr/65/20/CS-TR-65-20.pdf

So, somewhere in the 12 years between 01965 and 01977, Wirth’s
thinking on how to write grammars changed greatly, and, I would say,
very much for the better.  It was during this time that he released
Pascal, originally I think in 01970; there was a 55-page [ETH Zürich
tech report in 01973][3], which references ‘N. Wirth, “The Programming
Language Pascal”, ACTA INFORMATICA 1, 35-63 (1971) and Berichte der
Fachgruppe Computer-Wissenschaften Nr. 1 (Nov. 1970).’ and also
‘C.A.R. Hoare and N. Wirth, “An Axiomatic Definition of the
Programming Language Pascal”, Berichte der Fachgruppe
Computer-Wissenschaften Nr. 6 (Nov. 1972)’.

[3]: https://www.research-collection.ethz.ch/handle/20.500.11850/68910 (The programming language Pascal, Revised Report)

This 01973 Pascal report already uses curly braces for repetition (§3,
pp. 7–8 (pp. 13–14/55)), though in special cases it uses asterisks or
(superscript, circled) plus signs, similar to regular expressions:

> According to traditional Backus-Naur form, syntactic constructs are
> denoted by English words enclosed between the angular brackets <
> and >.  These words also describe the nature or meaning of the
> construct, and are used in the accompanying description of
> semantics.  Possible repetition of a construct is indicated by an
> asterisk (0 or more repetitions) or a circled plus sign (1 or more
> repetetitions).  If a sequence of constructs to be repeated consists
> of more than one element, it is enclosed by the meta-brackets { and
> } which imply a repetition of 0 or more times.

So Pascal’s “identifier” rule instead reads:

    <identifier> ::= <letter><letter or digit>*
    <letter or digit> ::= <letter> | <digit>

I think it’s maybe defensible to make the literal case the unmarked
case and specially mark the variable case (see file `tclish-basic.md`
for some notes on demotic programming and command languages using this
approach and file `tinyscript.md` for some notes on tiny languages, as
well as file `monkey-paw-pattern-rewriting.md` for some notes on a
more powerful text pattern matching language that uses `<>`), but I
think explicit repetition syntax is a real improvement in grammar
readability.

The curly-braces syntax occurs in §6.1.1 on p. 11 (p. 17/55):

    <scalar type> ::= (<identifier> {,<identifier>} )

(Pascal’s scalar types are what we would today call enum types; one of
Wirth’s examples is `(club, diamond, heart, spade)`.)

So, at this point Wirth had adopted `{ }` for repetition, but had not
yet adopted the `[ ]` notation for optional items that seems to
originate in the [01958 FLOW-MATIC manual][4] and have been
popularized by the [01960 COBOL report][5], and he was still using
`::=`, no explicit separator between production rules for different
nonterminals, and angle brackets around nonterminals rather than
quotes around terminals.  So out of five major changes between his
01965 style and his 01977 style, he had only made one by 01973, curly
braces for repetition.  They’re *almost* exclusively used for
delimiter-separated lists, things of the form `<x> {y <x>}`, so it’s
informative about Wirth’s way of thinking that he didn’t introduce a
construct with those semantics.  The one exception is declaration
blocks:

    <procedure and function declaration part> ::=
       {<procedure or function declaration> ;}

To clarify his syntax further, Wirth includes the famous railroad
diagrams in a three-page appendix.

[4]: http://www.bitsavers.org/pdf/univac/flow-matic/U1518_FLOW-MATIC_Programming_System_1958.pdf (p. 97 or p. 106/126)
[5]: http://www.bitsavers.org/pdf/codasyl/COBOL_Report_Apr60.pdf

I’m interested in how far back the curly braces go.  Dijkstra was
already using them by EWD472, the guarded command language, in 01975,
but perhaps Wirth had already adopted them by 01970.

[The Acta Informatica paper is owned by the ACM][6] but not freely
available.  A library copy is 29 pages long; it says it was received
in 01970.  The introductory paragraph of its §3 is exactly identical
to the paragraph I’ve quoted above, with the exception of the trailing
clarification about the curly braces, “which imply a repetition of 0
or more times.”  In general the ETH tech report seems to be a lightly
edited version of this paper, though the railroad diagrams are not
present.

[6]: https://dl.acm.org/doi/10.1007/BF00264291

However, there’s a crucial difference with respect to the braces!  The
production I quoted above for `<scalar type>` uses an asterisk after
the braces, which at first I thought was an error, but it is repeated
fairly consistently throughout the paper:

> &lt;scalar type> ::= (&lt;identifier> **{**,&lt;identifier>**}**\* )

In one case, it even uses the ⊕ after the braces instead:

> &lt;variant> ::= **{**&lt;case label> :**}**<sup>⊕</sup>
> (&lt;field list>) | **{**&lt;case label> :**}**<sup>⊕</sup>

This suggests that at this point the braces were at an intermediate
stage in Wirth’s mind, progressing from mere grouping (parentheses are
not grammar metacharacters in either of these two versions of the
Pascal report!) to denoting repetition in their own right in the 01973
version of the paper.  This was presumably suggested by the fact that
there was no little reason for grouping other than for repetition,
concatenation and alternation both being associative.

This also strongly suggests that Wirth was specifically the person who
originated the use of {} for repetition in EBNF and that he did so
between 01970 and 01973.  (Wikipedia tells me this is now part of the
proposed standard ISO 14977.)

But then, sometime between 01973 and 01977, he moved to suggesting a
radically different style:

1. `=` rather than `::=`.
2. `.` to end each production, eliminating the need for backtracking.
3. `identifier` rather than `<identifier>` for nonterminals, and
   consequently quoting of terminals.
4. Parentheses `()` for grouping.
5. The square brackets `[]` for optional items, from the 01958
   FLOW-MATIC manual and the 01960 COBOL Report.

What influenced these changes?  Was it his first sabbatical year at
PARC, apparently 01976/77 (according to his HOPL III paper on Modula-2
and Oberon)?  Did they begin earlier without being incorporated into
the Pascal report?  Did they stick, or did he return to a more
traditional style?

In the 01988 fourth edition of _Programming in Modula-2_, whose first
edition was published in 01982, Wirth does use exactly the notation he
proposed in 01977, which he simply calls “Extended Backus-Naur
Formalism (EBNF)”, with very nearly the description from the 01977
note, but with “production” renamed to “statement”, and without the
questionable syntax for string literals (in chapter 4, he explains
that this is because he wanted to describe the syntax at the token
level (“as sequences of symbols”) rather than the character level).

So it seems like this change of syntactic preference did indeed stick.

<style>
@import "et-book.css";
body {
    max-width: 35em;
    padding: 1em;
    margin: 0 auto;
    line-height: 1.4;
    font-size: 26px;
    color: rgba(0, 0, 0, 0.794);
    font-family: "et-book", "URW Palladio L", Palatino, serif;
}
pre, code { font-size: 80%; line-height: 0.9; color: rgba(55, 55, 55, 1) }
blockquote, pre {
    background-color: #eed;
    border: 1px solid #ccc;
    padding: 0.5em;
}
blockquote > p:first-child,
blockquote > ul:first-child,
blockquote > ol:first-child {
    margin-top: 0;
}
blockquote > p:last-child,
blockquote > ul:last-child,
blockquote > ol:last-child {
    margin-bottom: 0;
}
h1, h2 { margin: .75em 0 26px 0; border-bottom: 1px solid #aab }
h3, h4, h5, h6 { margin: .75em 0 .5em 0 }
h1, h2, h3, h4, h5, h6 { letter-spacing: 0.05em; color: rgba(136,0,0,.794) }
/* h1, h2, h3, h4, h5, h6, .metadata { font-family: Helvetica,Arial,sans-serif } */
h1:first-child, h2:first-child, h3:first-child,
h4:first-child, h5:first-child, h6:first-child,
h1 + h2, h2 + h3, h3 + h4, h4 + h5, h5 + h6,
h1 + p, h2 + p, h3 + p, h4 + p, h5 + p, h6 + p { margin-top: 0 }
h1 { font-size: 2em }
h2 { font-size: 1.59em }
h3 { font-size: 1.26em }
h4, h5, h6 { font-size: 1em; font-variant: small-caps; }
h4 { font-weight: bold; font-style: italic }
h5 { font-weight: normal; font-style: italic }
h6 { font-weight: normal; font-style: normal }
</style>
