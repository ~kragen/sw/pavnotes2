I was discussing with Ben Wiley Sittler the horrible ways terminal
input works on Unix.

`ICANON` vs. `<conio.h>` and `INKEY$`
-------------------------------------

The `.c_lflag & ICANON` approach used by Unix `termios` (previously
called CBREAK or RAW, with inverted polarity) lets an application
choose between full edited input lines of data and raw input keys, but
it’s extremely hard to use compared to the `getch()` function from
Borland `<conio.h>` or the `INKEY$` interface of GW-BASIC, and it’s
not robust to application crashes; [as the C FAQ warns][6]:

> If you change terminal modes, save a copy the initial state and be
> sure to restore it no matter how your program terminates.

[6]: https://c-faq.com/osdep/cbreak.html

The line-at-a-time interface provided to applications when ICANON is
turned on (“cooked mode”) is very desirable most of the time, though
the kernel tty driver’s editing facilities are deeply substandard from
the point of view of 02023, omitting not just mouse support but even
the ability to insert and delete anywhere but the end of the current
line.

With [`getch()` you get a raw character][2].  Microsoft implemented
the function (to lure away Turbo C or [Borland C++][7] programmers, I
think), and Microsoft’s documentation explains:

> The `_getch` and `_getwch` functions read a single character from
> the console without echoing the character. To read a function key or
> arrow key, each function must be called twice. The first call
> returns `0` or `0xE0`. The second call returns the key scan code.

[7]: http://bitsavers.informatik.uni-stuttgart.de/pdf/borland/turbo_c/Turbo_C++_Library_Reference_1990.pdf "Turbo C++ Library Reference, p. 190 (201/630)"
[2]: https://learn.microsoft.com/en-us/cpp/c-runtime-library/reference/getch-getwch?view=msvc-170

[Wikipedia’s `<conio.h>` page][4] links to [the Digital Mars
documentation][3]:

> **\_getch** obtains a character from **stdin**. Input is unbuffered,
> and this routine will return as soon as a character is available
> without waiting for a carriage return. The character is not echoed
> to **stdout**.  **\_getch** bypasses the normal buffering done by
> **getchar** and **getc**.  **ungetc** cannot be used with
> **\_getch**.  (...) Returns the next character read and extended
> character codes, but never returns an error.

[3]: https://digitalmars.com/rtl/conio.html
[4]: https://en.wikipedia.org/wiki/Conio.h

MS-DOS supported very limited keyahead: characters typed when the
program was busy computing and not polling for them were buffered up
in a 16-character FIFO, but not echoed.  The application program
itself was responsible for processing things like backspaces, using
the C runtime library, and for echoing; there was also a `getche()`
function which would echo the character.

This meant that the API was non-modal.  As I recall it, if you typed
“q”, a backspace, “a”, and Enter, what happened next depended on
whether the program called `getch()`, `getchar()`, or `getche()`.  If
it called the stdio `getchar()` (or equivalently `getc(stdin)` or
`fgetc(stdin)` or some other stdio function like `fgets`), the C
library would diligently loop through the input: drawing the “q” if
there was a delay before the backspace, then erasing it if so, then
drawing the “a”, then echoing the Enter, buffering up the newline and
returning the “a” to the program, having already forgotten the “q”.
But if the program called `getch()` or `getche()`, the library would
return the “q”, echoing it or not according to which function was
called.

(There was also a function called `kbhit()` which would tell you if
there was a character waiting for `getch()`; if not, `getch()` would
block.)

In GW-BASIC the [INKEY$ magic variable][8] would similarly return a
new character from the keyboard every time it was read (even if you
had typed a backspace immediately afterwards), or an empty string if
no character was pending, while the [INPUT statement][9] provided the
usual minimal set of editing functionality (just backspace, if I
recall correctly) and would block until you hit Enter.

[8]: https://hwiegman.home.xs4all.nl/gw-man/INKEYS.html
[9]: https://hwiegman.home.xs4all.nl/gw-man/INPUT.html

The Unix ability to type entire lines of text with editing while
seeing them was, in my experience, a big improvement in usability.
Perhaps today, 33 years later, computers have become sufficiently
faster that this is no longer so much of an issue?  Echoing characters
before the program requests them inevitably means the program cannot
control their echoing by requesting them in different ways.  (At best,
it could erase them after they’ve already been echoed, which is not
good enough for things like anti-shoulder-surfing password inputs.)
However, perhaps the same is not true of things like line editing.

Curses `getch()`
----------------

Though the [Borland C++ Library Reference Manual][7] linked above
claims, “**getch** is unique to DOS,” I believe the name [`getch()`
comes originally from curses][6], though the interface there is a
little different; quoting the ncurses manual:

> The getch, wgetch, mvgetch and mvwgetch, routines read a character
> from the window.  In no-delay mode, if no input is waiting, the
> value ERR is returned.  In delay mode, the program waits until the
> system passes text through to the program.  Depending on the setting
> of cbreak, this is after one character (cbreak mode), or after the
> first newline (nocbreak mode).  In half-delay mode, the program
> waits until a character is typed or the specified timeout has been
> reached. (...)  If keypad is TRUE, and a function key is pressed,
> the token for that function key is returned instead of the raw
> characters: (...) When a character that could be the beginning of a
> function key is received (which, on modern terminals, means an
> escape character), curses sets a timer.  If the remainder of the
> sequence does not come in within the designated time, the character
> is passed through; otherwise, the function key value is returned.
> For this reason, many terminals experience a delay between the time
> a user presses the escape key and the escape is returned to the
> program.

The “delay” and “no-delay” are nowadays generally called “blocking”
and “nonblocking”.  Note the extensive use of bug-prone modes
throughout the interface.

Key-release events
------------------

At the same time, ~ICANON “raw mode” fails to support the needs of
applications that want key-release events as well; conventionally
these are videogames, but it’s also unable to support the “quasimode”
approach to user interaction used in Jef Raskin’s Archy (previously
THE) and by alt-tab application switching.  (MS-DOS applications with
custom function keys sometimes displayed onscreen help relevant to the
current set of quasimodal keys held down, such as control, alt, and
shift.)

Quasimodal applications generally want to know the current state of
the keyboard, mouse, and touch devices, and to know the sequence of
changes of the keyboard and of pointer press and releases.  Notifying
them only of deltas (now Left Shift is pressed, now Enter is released)
is error-prone; in X-Windows and in DHTML, there are common bugs in
which an application fails to get notified of keys or mouse buttons
being released, because it has lost keyboard focus or because the
mouse has strayed outside of its window.  And when an application
starts, or when a submodule of an application is first invoked, the
deltas provide no information about what keys are already held down.
This can lead to usability problems.

VEOL: cooked mode until graduation
----------------------------------

Other programs, Ben pointed out, are best with a semi-cooked mode in
which they’re normally happy to get line-at-a-time input, but want to
be notified and take over if certain “magic” characters are pressed,
graduating to cbreak mode.  I seem to remember that [Bill Joy’s
original Berkeley csh][1], which was my shell when I got my first Unix
account in 01993, used ^D (the logout character) for filename
completion, a choice with the unfortunate effect that occasionally you
would accidentally log yourself out.

[1]: https://en.wikipedia.org/wiki/C_shell

Why?  My best guess is that ^D in cooked mode (ICANON) allows a
program’s blocked read to complete; if I type asdf^Dfoo^D to cat(1),
the result looks like this, with my cursor at the end of the line:

    $ cat
    asdfasdffoofoo

An additional ^D is needed to exit cat.  strace(1) displays what’s
happening in more detail:

    read(0, asdf"asdf", 131072)                 = 4
    write(1, "asdf", 4asdf)                     = 4
    read(0, foo"foo", 131072)                  = 3
    write(1, "foo", 3foo)                      = 3
    read(0, "", 131072)                     = 0
    munmap(0x7efc9ab7d000, 139264)          = 0
    close(0)                                = 0
    close(1)                                = 0
    close(2)                                = 0
    exit_group(0)                           = ?
    +++ exited with 0 +++

As the Linux termios(3) man page says about this character, VEOF:

> More precisely: this character causes the pending tty buffer to be
> sent to the waiting user program without waiting for end-of-line.
> If it is the first character of the line, the read(2) in the user
> program returns 0, which signifies end-of-file.  Recognized when
> ICANON is set, and then not passed as input.

csh also treated ESC as a filename completion character, which
apparently was enabled by setting ESC as the VEOL character:

> VEOL (0, NUL) Additional end-of-line character (EOL).  Recognized
> when ICANON is set.
> 
> VEOL2 (not in POSIX; 0, NUL) Yet another end-of-line character
> (EOL2).  Recognized when ICANON is set.

So, though I haven’t looked at the code, I think csh(1) was running
the terminal in cooked mode and treating the missing newline on the
line of input that it read as indicating a request for filename
completion.  This not only meant you could backspace in csh without
triggering a context switch to your csh process on every keystroke (a
more significant consideration on the PDP-11 where it was originally
developed than it is today) but also that csh didn’t have to duplicate
the kernel’s ICANON handling with code of its own, the way tcsh and
GNU readline(3) do.

There are any number of situations where it would be desirable to
delegate such text editing to a front-end process except when certain
magical keys are pressed.

`rlwrap`
--------

[Hans Lub’s `rlwrap` program][0] in effect replaces the kernel’s
ICANON handling with GNU readline whenever ICANON and ECHO are turned
on; this provides command-line history and editing to arbitrary
programs expecting line-at-a-time input, then gets out of the way when
the program requests raw mode, for example because it has its own
command-line editing or because it is prompting for a non-echoed
password.

[0]: https://github.com/hanslub42/rlwrap

Emacs shell-mode and the Plan9 terminal have a sort of similar
approach: they provide full text-editing facilities to the user,
including command-line history and copy and paste, without consulting
whatever application will receive the text.  They only unleash the
flood of text on the backend once the user is satisfied with it and
hits Enter.  Unlike `rlwrap`, they don’t monitor an ICANON setting,
and unlike the Unix kernel, they don’t offer VEOL keys to escape from
cooked mode, so backend applications cannot do things like filename
completion; which must instead be provided by Emacs itself.

Mosh
----

I’ve been using [Mosh][11] for remote login for a few months now.
With the long latencies attendant to sshing from Argentina, Mosh is a
huge improvement, providing enough user interface feedback to make
remote login sessions feel very responsive.

Mosh uses a predictive echo algorithm: when you start typing, it waits
to see if the remote end is echoing the characters it predicts it
will, and then starts echoing the characters locally, including some
minimal line editing like backspace and left and right arrows.  This
is sort of like dead reckoning from the last known position: it’s the
last known good screen plus or minus whatever characters you’ve
inserted or deleted, and the text that has only been thus predicted
but not yet confirmed is marked with an underline.  Eventually, when
the remote end catches up, the predicted text is discarded.

[11]: https://mosh.org

The idea of waiting to see if echoing is happening is mostly to avoid
echoing password characters, but also helps with things like Vim, or
video games, neither of which typically echo command keys.  Every time
you hit Enter, or if you use a weird editing key that Mosh doesn’t
know how to predict, the echoing state is reset, and it waits to see
how the remote end is going to respond.

Mosh’s approach, unlike `rlwrap`’s, doesn’t simplify the backend
application at all; if it neglects to provide arrow-key handling, for
example, Mosh will initially predict that it does, but then discard
the predicted edits once the backend application handles the arrow
keys in an unpredicted way.

A proposal: input-state queues
------------------------------

A frontend analogous to `rlwrap` manages the input for a backend.  The
frontend maintains a queue of pending input states that have not yet
been processed by the backend; each time the set of keys held down
changes, or a key repeats, that is an input state.  The frontend
manages the usual set of line-editing facilities, providing the user
with a responsive, featureful, and customizable editor, which edits a
queue of characters to eventually be read by the backend.  But the
backend can consume *either* the queue of characters *or* the queue of
input states, and can switch back and forth between them at will.  The
user can switch to a local-editing mode where the backend gets no data
until the user unblocks; the backend can also request to block its
request for characters until end-of-line or certain special keys are
hit.

Input consumption is fairly simple in the most common case, of a
regular letter.  The backend requests a character; the letter is
removed from the frontend queue and sent to the backend, and so are
all the input states before the input state that created that
character, including key-release events, modifier-key events, and
keypress events that created characters that were deleted before this
letter was typed.  Normally the backend will eventually acknowledge
the receipt of the character (with a TCP-like sequence number?) and
echo back the character, so the frontend will delete the character
from its displayed queue but also simultaneously draw the character
echoed by the backend.

I’m not as sure about other cases.  I intuitively am tempted to think
that when the backend consumes input-state events, the characters
enqueued by those input-state events should be removed from the queue
if they’re still present, but the problem there is that the characters
and input states are in general in different orders.  If I type “frm”,
then cursor back to insert an “o” before the “m” to make the word
“from”, a backend might consume the “frm” events and then switch to
consuming characters.  But the only character left is “o”.  How can
the backend sensibly echo back the “frm”?  How can the frontend tell
it where the “o” belongs?  Such a situation probably should be
prevented somehow, but maybe a lighter-weight, more robust solution
than tty modes exists.

As for echoing passwords, maybe each request from the backend includes
a bit indicating whether or not the frontend should be echoing
characters, so that when the backend sends the “Password: ” prompt or
removes Vim’s “-- INSERT --” or Kakoune’s “insert” from the screen,
it’s accompanied by a request not to echo the following characters.
There’s always a potential race condition where the user starts
key-ahead of the password or of Kakoune commands before that prompt,
but that seems like a manageable problem.

TCP-like sequence numbers are also how Kafka manages ultra-high-speed
message queues: the broker assigns serial numbers to each message
posted, and each subscriber is responsible for tracking the latest
sequence number they’ve seen.  Perhaps a cleaner approach proceeds
from numbering input states similarly and separating out a "predictor"
from the frontend.  The frontend just maintains a history of input
states and displays the highest-ranked output state.  Output states
are ranked by (inputnumber, poster) pairs.  The predictor is poster 0;
the backend is poster 1.  So (471, 0), the predicted output from input
471, outranks (470, 1), the backend’s output from input 470, but (471,
1) outranks them both.  And the predictor can look at previous backend
outputs to form its prediction, and they can contain things like the
cursor position and echoing flags.

This Mosh-like approach pushes all the line editing back into the
backend, which presumably can use a standard library to implement it.
Unfortunately, it doesn’t help much with things like command-line
history or copy-and-paste, which are benefits of Emacs shell-mode.
