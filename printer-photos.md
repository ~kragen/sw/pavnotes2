A 24-pin dot-matrix printer that prints 6 lines per inch (12 point
text, "pica") has 144 dpi vertical resolution and typically about the
same horizontal resolution.  On A4 paper (210 mm x 297 mm) this is
1190 x 1684, but typically there are unprintable margins on the paper,
so it's more like 1100 x 1684.

I saw a cute project for sending dithered photos to your grandparents
with a Telegram bot.  However, perhaps out of pure sadism, the
designer chose a thermal printer.  So the grandchild photos will
irreversibly fade over a few months to a year or two, like a physical
metaphor for senile dementia.  But I was thinking about data sizes and
stuff.

I decided to test with [this gorgeous cc-by photo of Hawaii by Pedro
Szekely][0] which is [28'524'896 bytes in its original size][1].
Converting it to 1100x734 color with netpbm yields [a 521K file][2].
Converting to CCITT G3 format yields [a 77K file][3] which [compresses
to 42K with PNG][4], but the default netpbm conversion loses most of
the detail in the shadows and the sky.  A fairly aggressive high-pass
filter and JPEG compression in the GIMP yielded a [102k RGB file][5]
which produced [a much more detailed 135k G3 file][6] and a [73K
PNG][7], which netpbm can compress to 64K with JBIG.  It's a little
ugly but probably about as clear as we can expect at that resolution
without improving the state of the art in dithering algorithms.  At
1200 dpi it would be the size of a postage stamp.

(Later I realized that maybe I should have rotated it 90° to get twice
as many pixels within the constraints of such a printer!)

Can we do better, whether for limited-capacity archival,
limited-capacity thumbnail caches, or for limited-bandwidth
transmission?  Converting to monochrome in the GIMP and exporting to
JPEG at very low quality of 6 yielded [a 20K JPEG][8] which converted
to [a 115K G3][9] and [a 65k PNG][10].  This is ugly with lots of
visible artifacts but still clearly shows a landscape with a cloudy
sky and a forested mountain, but Mina couldn't see the trees or rocks
or be sure that it was an ocean or a beach.

[0]: https://flickr.com/photos/pedrosz/51828921499/in/explore-2022-01-19/
[1]: ./51828921499_89bff3e7f9_o.jpg
[2]: ./1100x1684.jpg
[3]: ./1100x1684.g3
[4]: ./1100x1684g3.png
[5]: ./1100x1684-ldr.jpg
[6]: ./1100x1684-ldr.g3
[7]: ./1100x1684-ldr.png
[8]: ./1100x1684-ldr-bw-low.jpg
[9]: ./1100x1684-ldr-bw-low.g3
[10]: ./1100x1684-ldr-bw-low.png

Reducing the size to 400x267 and exporting with low quality gives [a
3965-byte JPEG][11] which is barely recognizable, especially [when
dithered down to 1 bit at the 1100x734 resolution][12].  [WebP gets
sort of reasonable quality at 12K][13] ([png][14]) but I can't seem to
adjust its quality low enough to compete with JPEG.  Upscaling back to
1100x734 1-bit-deep yields [a relatively understandable image][15].

[11]: ./400x267.jpg
[12]: ./400x267-upscaled.png
[13]: ./400x267.webp
[14]: ./400x267-webp.png
[15]: ./400x267-webp-up-g3.png

At 1 bit per second such a photo would take almost 9 hours to
transmit.  As 1-bit pixels with no error correction it would take just
over 178x178 pixels, about a 7.5 millimeter square.  Putting it into
the Bitcoin blockchain would cost about US$20.

I feel like you could probably do a much better job of this kind of
thing somehow and get such an image another order of magnitude under
4K.  In this case the appropriate cheat would probably be a Bob Ross
kind of thing where you worry a lot about getting the right texture in
the right areas of the image, and the right lines between them, but
not so much about where the individual points in the texture are.  I'm
not sure that would work well for things like faces or book page
scans.
