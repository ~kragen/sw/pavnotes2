Thanks to 3-D printer hotends, there are lots of 40-watt resistors on
the market for US$2 now.  These are packaged in a nickel-plated (?)
metal casing, with the resistance wire compacted inside of, probably,
magnesia.  Similar but larger devices are sold for US$4 as a way to
heat water.

Of course this is a silly way to buy a resistor.  Nichrome wire costs
under US$0.50 per meter, and broken space heaters are an abundant
source of it.  Digi-Key will happily sell you 5-watt through-hole
axial-lead ceramic resistors for 38¢ in quantity 10, 10-watt ones for
69¢, or 50-watt chassis-mount resistors for US$4.10.
