Packed beds of solid desiccants are desirable for TCES applications
because they avoid the headaches of liquid handling, but the cheapest
and highest-capacity solid desiccants are deliquescent, so they can
“overhydrate” and turn into a gummy lump which no longer permits
airflow. They also expand and contract as they absorb and release
water, which tends to make them crack and crumble over time, limiting
their cycle life.  Perhaps a nanocomposite desiccant consisting of a
resilient gel can solve these problems cheaply.

Kallenberger and Fröba’s hydrogel-derived matrix paper
------------------------------------------------------

In 02018 Paul Kallenberger and Michael Fröba published an open-access
article, “Water harvesting from air with a hygroscopic salt in a
hydrogel-derived matrix”, doi:10.1038/s42004-018-0028-9, where they
made alginate hydrogel balls of 75 wt% muriate of lime and then dried
them out.  Though the salt did not crystallize (as far as can be seen
with a scanning electron microscope) it absorbed water from air as per
normal, up to the point of deliquescence (though it’s not clear from
the paper how the deliquescence manifested: syneresis?), then
dehydrating back to the anhydrous muriate at 150°, already releasing
significant water under 50°.

The result is that they have an effectively solid desiccant which
reliably remains gas-permeable, can absorb moisture down to 30% RH,
absorb 100% of its own weight in moisture reversibly, and desorbs the
moisture at easily attainable temperatures.  As a bonus, it’s
nontoxic.

The xerogel has a nanoporous structure facilitating water-vapor
diffusion, enabling hydration times of a few hours for 2mm balls (they
used 12 hours in their experiments, but hydration was half complete in
about 2½ hours, eyeballing their charts).

The technique for making the alginate beads was the usual molecular
gastronomy technique of dripping sodium alginate solution into a
(saturated?) solution of muriate of lime to provoke ionotropic
gelation; they then waited for ionic diffusion to complete before
drying the balls.

Ionomeric variations
--------------------

Several variants of this technique occur to me; you can use different
ionomers, different desiccants, and different shapes, and it may be
useful to use a different cross-linker than the desiccant used.

Candidate ionomers that occur to me include polyacrylate (such as
carbomer), carrageenan (E407), polyvinyl alcohol, polyvinyl acetate,
locust bean gum (E410), gum tragacanth (E413), gum arabic (E414),
xanthan gum (E415), gellan gum (E418), flaxseed mucilage, chia
mucilage, psyllium husk fiber (Metamucil), carboxymethylcellulose,
hydroxypropylcellulose, guar gum, pectin (E440), gelatin, gelatinized
starch, hyaluronic acid, methylcellulose (Citrucel), cactus mucilage,
aloe vera mucilage, marshmallow root mucilage, slippery elm bark
mucilage, agar-agar, konjac (mostly glucomannan),
pectin, oligomers of silicate from waterglass, and chitosan.  (I’m
not sure these are all actually ionomers.)

Kallenberger and Fröba’s trick works because polyvalent cations like
calcium ionically cross-link alginate instantly.  A number of other
organic ionomers, [such as low-methoxy pectin][2], react similarly
with calcium ions to form gels.

[2]: https://en.wikipedia.org/wiki/Pectin#Chemistry

The nontoxic glyoxal is widely used to covalently cross-link
methylcellulose.  Chitosan can be similarly covalently cross-linked
with dicarboxylic acids (I think tricarboxylic citrate would work, but
alternatives surely include adipate (E355), malate (E296), malonate,
tartrate (E334), fumarate (E297), succinate (E363), glutamate,
aspartate, the possibly more toxic maleate, and the definitely more
toxic oxalate); [succinic and lactic acids are used for chitosan
hemostatic agents][1], even though lactic acid is monocarboxylic.
[Malonic acid is used in this way to crosslink corn and potato
starches][0].

[0]: https://en.wikipedia.org/wiki/Malonic_acid#Applications
[1]: https://en.wikipedia.org/wiki/Chitosan#Wound_management

Thiomers such as thiolated pectin, thiolated hyaluronan, and thiolated
chitosan (and, of course, natural latex rubber) can be crosslinked
with disulfide bonds.

Formaldehyde and glutaraldehyde can covalently cross-link proteins; I
think I’ve also seen glutaraldehyde used to cross-link starch in this
way.  Like glutaraldehyde, which will take warts off your feet,
glyoxal is a dialdehyde (the simplest one, as formaldehyde is the
simplest aldehyde) so I don’t understand why it’s nontoxic (though I
guess glyoxal is used as a histological fixative too).  Glutaraldehyde
can also cross-link thiols.

[Starch can be cross-linked with phosphate][8].

[8]: https://en.wikipedia.org/wiki/Phosphated_distarch_phosphate

Because carrageenans are sulfated I’d intuitively expect calcium or
barium ions to be able to ionically cross-link them.  [It turns
out][4], though, that ι-carrageenans gel in the presence of calcium
ions, κ-carrageenans gel in the presence of potassium ions, and
λ-carrageenans do not gel.  I think carrageenans also experience
some degree of pH-sensitive gelation.

[4]: https://en.wikipedia.org/wiki/Carrageenan#Properties

Gum arabic and gelatin were used in [gum printing][6] and cross-linked
by the UV photolytic degradation of the bichromates of potassium or
ammonium; I think this maybe produces chromium ions, but I’m not sure.
I think the Cyanomicon said that other versions of gum printing
instead used iron ions liberated by visible light, the same process as
in cyanotypy.

[6]: https://en.wikipedia.org/wiki/Gum_printing

[Locust bean gum][7] can be gelled by the addition of borax.

[7]: https://en.wikipedia.org/wiki/Locust_bean_gum#Chemistry

I think that some of the above plant gums are composed partly of
pectin.  But the broader category of pectin is also of interest
because it’s the second most massively produced biopolymer other than
cellulose.  I think that makes it likely to be the cheapest
non-waterglass gelling agent.  As mentioned above, low-methoxy pectins
are commonly gelled with Ca²⁺ and presumably would react similarly to
other polyvalent cations, potentially reducing the quantity of ionomer
required to transition from a sol to a gel if you’re using something
like muriate of lime as your desiccant.  High-methoxy pectins
generally gel with sugar and acid, although I’m pretty sure they have
a critical gelation concentration without any of that.

Soap
----

Fatty acids may be a viable alternative to ionomers.

[Calcium stearate][9] and allied soaps of polyvalent cations are
well-known as that annoying soap scum that forms in hard water; it can
be used to waterproof fabric and, in a process known as
[tadelakt][10], lime plaster.  They form from alkali soaps rather
rapidly in the presence of even small amounts of calcium ions; alkali
soaps like [sodium stearate][11] are sort of miscible with water in
all proportions, forming micelles above a critical concentration,
rather than crystals as most soluble solids do.  The micelles do not
aggregate as crystals do, so the resulting colloid is for some
purposes nothing more than a mixture of the soap and water whose
viscosity increases as it approaches 100% soap.

This suggests to me that you should be able to form balls of gel by
dripping a thick soap solution into a source of polyvalent cations
rich enough to form a skin on its surface, very much as with sodium
alginate.  The skin should prevent the soap ball from coming apart as
the cations diffuse into it through the pore water of the colloid;
cations like sodium, calcium, potassium, and magnesium should diffuse
much more rapidly than the fatty-acid anions.  It should similarly be
possible to diffuse a great deal of desiccant salts into the pore
water, just as with the alginate beads described above.

I haven’t tried this, and it occurs to me that there are some ways it
could fail:

1. The calcium stearate (and allied soaps) could fail to form a real
   gel due to their hydrophobic nature, expelling the pore water and
   whatever salts it contains, shrinking into tiny solid beads
   containing no desiccants.
2. The calcium stearate might fail to cohere into solid beads at all.
3. The calcium stearate might melt or chemically degrade at the
   temperatures needed to dehydrate the desiccant.  (This is probably
   fine.  Wikipedia lists 155° as a melting point; American Elements
   says 179°.)

[9]: https://en.wikipedia.org/wiki/Calcium_stearate
[10]: https://en.wikipedia.org/wiki/Tadelakt
[11]: https://en.wikipedia.org/wiki/Sodium_stearate

3-D printing
------------

It should be possible to extrude any of these gel precursors (sodium
alginate solution, sodium polyacrylate gel, potassium stearate micelle
colloid, etc.) through a nozzle submerged in a solution of gelating
ions in order to perform a good simulacrum of FDM 3-D printing. Upon
its exit from the nozzle, the surface of the substance will
ionotropically gelate, forming a fiber which is initially liquid
inside but which will gradually solidify through ionic diffusion,
similar to the process of solidification through cooling that happens
in normal FDM.  Moving the nozzle around permits depositing the
material in multiple layers on a substrate to build a desired 3-D
shape.

If the gelation is too slow, the fiber might dissolve in the water,
and if it’s too rapid, it might fail to adhere to the substrate or
previous layer.  This can be controlled by the concentration of
gelating ions in the solution.

This is potentially useful because it permits the construction of
shapes with a better surface-area-to-fluid-flow-resistance tradeoff
than a packed bed of beads.  (It also might be useful for making
things out of gels that aren’t desiccants.)

Extrusion
---------

An alternative process which is surely much faster would be to extrude
the gel precursor through a shaped extrusion die, the way macaroni,
aluminum profiles, and propellant grains are made.  For tubular
extrusions with holes, you might also need to inject the solution
bearing the gelation ions into the hole, but plausibly this is not
necessary.  This is another possible way to improve the flow/area
tradeoff.

Alternative desiccant salts
---------------------------

### Cheap minerals ###

Muriate of magnesia is similar to muriate of lime in its desiccant
powers, and I think [carnallite][12] (KMgCl₃) and tachyhydrite
(CaMg₂Cl₆) exceed them.  [Carnallite has recently been evaluated][13]
for TCES in an open-access article.  [Supplementary information.][14]

[12]: https://www.saltwiki.net/index.php/Carnallite
[13]: https://www.sciencedirect.com/science/article/pii/S2352152X24009897 (Hamze, Nevoigt, Sazama, Fröba, and Steiger, 02024)
[14]: https://ars.els-cdn.com/content/image/1-s2.0-S2352152X24009897-mmc1.pdf

The Hamze et al. paper suggests using carnallite in solid form,
dehydrating it from its dihydrate at 100°, saying that its “high”
deliquescence humidity (54%) reduces the risk of “liquefaction”, and
that the theoretical storage density is 1.52kJ/cm³ (1.52MJ/ℓ,
1.52GJ/m³).  They’re concerned with being able to use
easily-attainable low temperatures (<150°) due to what I think is an
outdated concern about using solar thermal collectors rather than
photovoltaic panels.  Nowadays PV panels are so cheap that you should
probably use them instead even if you’re just using them to heat up a
nichrome wire to dehydrate your salt.

(They do mention specifically the alginate gelation idea, as you'd
expect given that they have the same last author.)

Excitingly, they also mention two tachyhydrite papers!

I feel like 54% is still not a very high deliquescence humidity.

### Trivalent cations ###

For ionotropic gelation, however,
it would be nice to be able to use an aluminum salt, because
undoubtedly it would be better at ionotropic gelation, but I’m not
sure that either aluminum chloride or aluminum nitrate is suitable.

### Ways to use waterglass ###

Going in the opposite direction, the cheapest gelling agent is surely
sodium silicate, which can form hydrogels of surprising resilience.
But I think that if you add too many polyvalent cations to it, you may
expel the water from the gel structure and lose that resilience as
well as any possibility of containing dissolved desiccant salts.
(Soda-lime glass is notable for its lack of resilience, to the point
of being a cliché in pop culture with terms such as “glass jaw”,
depiction of glass on “fragile” shipping labels, the song “Heart of
Glass”, etc.)  So it might be worthwhile to consider alternative
desiccant salts that don’t contain polyvalent cations, such as the
expensive lithium bromide.  Carbonates of sodium or potassium might
work; waterglass forms them naturally over time, but I think it loses
resiilency in the process.
