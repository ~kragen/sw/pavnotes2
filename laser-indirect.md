Suppose you have some kind of a 3-D scene that you’re raster-scanning
a laser over while filming with a camera.  The simplest thing you can
do is to just sum up all the film frames pixel-by-pixel to get an
image of the scene, as seen by the camera, as illuminated by the
laser.  You can subtract off some kind of ambient lighting baseline
(perhaps the pixelwise minimum) to try to cancel out the non-laser
lighting.

Another obvious thing to do is to identify the brightest point in the
scene in each frame and identify that with the point of intersection
with the laser and the scene geometry, and use the displacement of
that spot from some vanishing point to estimate the depth of the scene
geometry at that point.  This is, as I understand it, how the
Cyberware 3-D laser scanner and similar devices work.

A slightly less obvious thing to do is to make a [“flying-spot
scanner][0] camera”: sum up all the pixels in each frame to get an
average brightness, thus getting an image of the scene *as seen by the
laser source* as if it were illuminated by a light source at the
camera position.

Scanning the laser over the scene many times provides, in effect,
[lock-in amplification][1], enabling the final laser image to have
lower noise and dc offset than the raw camera image.  The camera
exposure may need to be adjusted over time in order to provide
adequate dynamic range to capture both the bright laser point and
diffuse indirect reflections.  Deconvolution with an estimated camera
OTF and an estimated OTF of the laser will generally also be
necessary.

[0]: https://en.wikipedia.org/wiki/Mechanical_television#Flying_spot_scanners
[1]: https://en.wikipedia.org/wiki/Lock-in_amplifier

Most of the time a given camera pixel will mostly correspond to a
given point in the scene geometry.  If we do the same flying-spot
trick, but using a single camera pixel instead of the sum of all
pixels, we can render a rather dim and noisy view of the scene as if
it were illuminated from that point in the scene.

Of course, that’s also what we get from any one of the camera frames,
at least after subtracting off the ambient baseline — a view of the
scene as illuminated by the point that the laser is hitting at that
moment.

This is a many-dimensional dataset; you might collect the scene as if
illuminated from a million different points of view.  But a few
principal components are likely to account for most of the variation
in that dataset, probably corresponding to major parts of the scene
illuminated by different low-frequency spherical harmonics.  The
residuals from such a low-rank approximation should be the parts of
the scene that aren’t quite so simple, probably because they contain
strong specular reflections or refractions.

Where this starts to get more interesting is when we try to infer
scene geometry from this indirect lighting information.  A
straightforward gradient-descent approach for predicting those
specular reflections ought to make it possible to infer objects not in
direct view of either the laser or the camera, as well as inferring
fine detail.  As an example, scanning the laser over a specular object
that illuminates a visible Lambertian reflector will produce a pattern
on the Lambertian reflector precisely measuring the surface normal at
each point of the specular object as well as its degree of
specularity and any anisotropies present.

A self-organizing map may be useful for discovering pixels that share
illumination even when they are not close together.

By using lasers of different colors, we may be able to infer the
material composition of objects in the scene, perhaps with
subnanometer spectral resolution.  Near-infrared lasers are likely to
be especially interesting in this connection.

By scanning the laser across a person’s body, you should be able to
estimate subsurface scattering and therefore make estimates of their
internal structure.
