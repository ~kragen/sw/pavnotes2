Sourcing amorphous solar cells for the Erika Zorzpad.  Amorphous solar
cells are better suited to indoor lighting because their spectral
sensitivity closely matches that of human eyes, and nowadays so does
the spectrum of artificial lighting, while crystalline silicon is most
sensitive in the infrared.  Also, I suspect they might continue to
work usefully down to lower light levels.

Credit card solar calculators
-----------------------------

<https://articulo.mercadolibre.com.ar/MLA-1103847839-calculadora-solar-portatil-tipo-tarjeta-de-credito-chata-_JM>
is an AR$219 credit-card-sized calculator that claims to be solar.
It’s sold by a vendor in Capital, MEGACUPERSRL (MegaCuper) aka
MEGACUPER11 <https://www.mercadolibre.com.ar/perfil/MEGACUPER11>,
which seems to specialize in cheap Chinese import products.  “En Once,
cerca de Corrientes y Pueyrredon, de lunes a viernes de 10 a 16 y
sábados de 9 a 13.”

Three corners of the calculator on the image are at (175, 301), (918,
118), and (1029, 597), and the fourth is obscured by the hand.  This
gives delta vectors of (743, -183) and (111, 479); their dot product
gives an angle of 90.8°, which is close enough for me.

Google says: Av. Corrientes 2589, C1046AAD CABA, Opens 10AM Mon, 011
5988-2115.  That’s at Paso and Corrientes.

<https://www.megacupermayorista.com.ar/productos/calculadora-solar-portatil-tipo-tarjeta-de-credito-chata/>
lists the same calculator for AR$185 (94¢) instead of AR$215, giving
dimensions of 86 mm × 55 mm × 3 mm, and also says, “WhatsApp
1121553812 megacuper@gmail.com”.
<https://www.megacupermayorista.com.ar/contacto/> clarifies, “piso 8
of 39.”

The 765-pixel long edge measured above would then be 8.90 pixels per
mm, and the 492-pixel short edge 8.93 pixels per mm, which seems close
enough.  The corners of the solar cell are at (646, 281), (850, 231),
(862, 292), and (660, 344), giving deltas (-14, -63), (204, -50), (12,
61), and (-202, 52), giving these angles and lengths:

    >>> cd = c - numpy.roll(c, 2)
    >>> [math.acos(cd[i].dot(cd[(i+1)%4])/cd[i].dot(cd[i])**.5/cd[(i+1)%4].dot(cd[(i+1)%4])**.5)*180/math.pi for i in range(4)]
    [88.7572079920691, 92.64241042747123, 86.69321431063598, 91.90716726982369]
    >>> [v.dot(v)**.5 for v in cd]
    [64.536811201050213, 210.03809178337153, 62.169124169478209, 208.58571379651102]
    >>> [v.dot(v)**.5 for v in cd / 8.915]
    [7.2391263265339552, 23.560077597686092, 6.9735416903508929, 23.397163633932813]

±4° is plenty precise enough, and what we have is that the solar cell
is about 23.5 mm × 7.1 mm or 1.7 cm², 55¢/cm².

This place says they’re “wholesale” (“mayorista”) so they might demand
that I buy more than one thing.  They do have a bunch of things I want
though: mosquito-killing suction lamps (US$8), silicone chocolate
letter block molds (US$3), magsafe USB cables (US$2.50), bimetallic
oven thermometers (US$3.50), indoor/outdoor -50°–108° LCD digital
thermometers (US$3), self-healing cutting mats (US$7), hand-held
battery-powered rotary diamond engravers (US$3.50), sets of 30 diamond
bits for them (US$3), credit-card folding knives (US$1.50), portable
digital scales (US$4.50), 60× pocket microscopes (US$6), LED
lightbulbs with petals (US$5.50), behind-ear hearing aids (US$6.50),
vise-grips (US$6), UV LEDs with associated laser pointers (US$3),
8-color toilet bowl lights with motion sensors (US$2.50), kitchen
micro-torches (US$3.50), etc.

Digi-Key and Panasonic reference
--------------------------------

Zarutian points out
<https://www.digikey.com/en/products/detail/panasonic-bsg/AM-1801CA-DGK-E/2165188>,
a US$3.75 amorphous solar cell with 20 μA short-circuit current, 4.9V
open-circuit voltage, and 18.5 μA at 3 V at peak power point, which
gives 55 μW, which is 25 mm × 53 mm, or 13.25 cm².  This is slightly
cheaper per area (28¢/cm² instead of 55¢) but the alarming thing is
that its area is 1.3 watts of sunlight.  If that were for sunlight,
55 μW would be 0.004% efficiency.

By contrast
<https://www.digikey.com/en/products/detail/panasonic-bsg/AM-5610CAR-DGK-T/2165194>,
also a Panasonic amorphous silicon solar cell, is US$6.90 and claims
to produce 5.1 mA at 3.3 V in 25 mm × 20 mm, thus US$1.38/cm² and 3.4%
efficient.

Panasonic has memory-holed their “datasheets” or catalogs, but
<https://web.archive.org/web/20210507035730/https://panasonic.co.jp/ls/psam/en/products/pdf/Catalog_Amorton_ENG.pdf>
goes into some detail.  It gives “electrical properties of Amorton for
indoor use” under 200 lux as 7.3 μW/cm² for glass and 9.0 μW/cm² for
film.  Direct sunlight is about 100'000 lux and 1000 W/m², so linearly
extrapolating suggests that this light is 2 W/m² or 200 μW/cm², so
this would actually be 3.6%–4.5% efficiency.  (It warns, “Since
Amorton is designed for indoors use, please use it under 1000 lux.”
Maybe I can protect it by putting yellow plastic or glass over it.)

In this “datasheet” Panasonic characterizes their indoor cells’ output
down to about 35 lux, at which point they output about 4 μA, remaining
relatively linear up to 1000 lux and 80 μA.  That’s an increase of
about 50% in the current per lux, and the voltage also increases about
50% over that range, so they’re actually more than twice as efficient
at higher illuminances.  The outdoor cells are only characterized over
the range of 5 klux to 100 klux.

The Digi-Key numbers work out to 4.2 μW/cm²; if that’s specified under
the same conditions as above, it would be about 2–4% efficient.
(Sunlight contains a lot of invisible infrared light that's absent
from fluorescent light, so fluorescent light contains less energy per
lux.)  And, indeed, the AM-1801 from the Digi-Key page is listed at
3.0 V and 18.9 μA and 53×25 mm under 200-lux fluorescent light on
p. 7 of this catalog.

Welding masks
-------------

It’s common for self-darkening welding masks like
<https://articulo.mercadolibre.com.ar/MLA-1112751453-mascara-fotosensible-para-soldar-gladiator-ms801c-smg-_JM>
to use photovoltaic cells (which presumably simply apply a voltage to
an LCD).  That one in particular is claimed to have a 93-mm-wide
viewing window, which appears to be the same width as the PV cell
above it, which in proportion would be about 12 mm tall, giving it a
total area of about 11 cm².  This mask is sold for AR$4995, about
US$25.  This would make it about US$2.20/cm² as a source of solar
panels, four times as expensive as the solar calculator.

I don’t know whether these photocells are amorphous, but I suspect
not: any current output under ordinary indoor lighting would be a
drawback, and the illuminance from a welding arc is typically somewhat
brighter than direct sunlight, not dimmer, and dimmer environments are
where amorphous cells excel.  There are some cheaper masks, but only
by about a factor of 2, so it’s not an appealing source anyway.

Sizing
------

If each half of the Zorzpad is 100 mm × 120 mm, and the 120-mm-wide
space above the keyboard is partly occupied by a 58.8 mm × 35.3 mm
400×240 Sharp memory LCD, then there’s 61.2 mm × 35.3 mm available on
each half of the device for solar panels, 21.6 cm² on each half or
43.2 cm² in total.  At the 4.2 μW/cm² dim indoor illumination power
level described above, we’d get 181 μW, not quite enough to keep the
Ambiq Apollo3 CPU running continuously.  Five times that, for
1000 lux, is almost a milliwatt, which is enough for relatively full
funcioning.

43.2 cm² is 26 calculators’ worth.

In 100klux direct sunlight, we’d presumably get about 50 milliwatts,
which is only 1% efficient but plenty of power.  A 16%-efficient PV
cell could deliver that much using only 3.1 cm² in direct sunlight, so
it might be worthwhile to include some polycrystalline or
monocrystalline PV cells in the package as well.
