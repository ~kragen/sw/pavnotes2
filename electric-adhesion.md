Apple's new phone battery is glued in with a new kind of tape which
can be electrolytically delaminated.  By running a bit of current
between the battery case and the phone case, you can permanently
unstick the tape from the phone case, easing the task of replacing the
battery.

But it occurs to me that electrically activated adhesion should be
even easier, where by running a bit of current through an adhesive
between two metal parts you bond them permanently.

Structural metals are invariably polyvalent, and most have
water-soluble salts.  Their polyvalent cations can ionotropically
gelate a variety of anionic solutes to form amorphous cements.  In
particular, insoluble silicate and phosphate glasses are likely
cements.  Fillers can probably strengthen the system, and additional
solutes (spectator electrolytes) may help with conductivity.

To take a concrete example, maybe our adhesive is sodium silicate with
a low sodium concentration and a considerable amount of water.  It is
mixed with potassium chloride to increase conductivity and heavily
filled with finely divided corundum (alpha-alumina).  We put a thin
layer between two aluminum blocks with asperities smaller than the
alumina particles; the particles prevent direct contact.  On passing a
current through them, carried by the potassium chloride, aluminum ions
are thrown off the anode surface into the solution, where they
cross-link the silicate chains into a silicoaluminate rat’s nest which
links also to the alumina particles.  By virtue of forming a gel
containing plenty of nanoscopic pores full of water and spectator
electrolytes, we avoid “polarization” in the form of a solid glass
film on the anode preventing further ion release.

Alternative fillers include clays, quartz, glass fiber, and talc.
Alternative metals include nickel, copper, iron, titanium, zinc, tin,
silver, and their alloys.  Alternative spectator electrolytes include
chlorides, sulfates, nitrates, acetates, and perchlorates of alkali
metals and ammonium, except for the insoluble ones.  Alternative
adhesives include soluble phosphates such as dipotassium phosphate and
monoammonium phosphate, as well as sodium polyacrylate, carrageenans,
etc.

