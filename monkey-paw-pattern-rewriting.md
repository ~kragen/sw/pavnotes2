The Monkey’s Paw: rewriting pattern grammars
============================================

The Monkey’s Paw is a pattern-matching and rewriting language for
writing readable programming-language-independent parsing expression
grammars (PEGs) with a smooth incremental path from simple text-search
patterns up to AST-generating parsers and full-blown compilers.

Because I believe that explanations should begin with examples, here
are some simple Monkey’s Paw patterns:

    "xyzzy"
    [a-z]+
    <name> "(" <args> ")"
    <s: [-+]|> <m: [0-9]+ "." [0-9]* | "." [0-9]+> <e: [Ee][-+][0-9]+ |>

Language parser example; explanation of basic constructs
--------------------------------------------------------

Now, I’ll [draw the rest of the fucking owl][5].  Here’s a complete parser for a
very simple expression language, expressed as a Monkey’s Paw pattern:

    <expr: <first: <term: <digits: [0-9]+>              {parse_int(digits)}
                        | "(" \s* <expr> \s* ")"        {expr}
                        | <var: [A-Za-z_][A-Za-z_0-9]*> {read_var(var)}> {term}>
           \s* <rest: ( "+" \s* <term>  {term}
                      | "-" \s* <term>  {negate(term)}
                      )*>
           {add(first, sum(rest))}>  {expr}

[5]: https://knowyourmeme.com/memes/how-to-draw-an-owl

For example, it converts the string `5+ x - (z + 2)` into the form
`add(parse_int("5"), sum([read_var("x"), negate(add(read_var("z"),
sum([parse_int("2")])))]))`.

### Primitive constructs used ###

The constructs used here are as follows.  Each construct, when
applied, either *succeeds* or *fails*; if it succeeds, it *matches*
some substring of the input and evaluates to some *value*.

- `<expr: stuff>` defines a *capture group* named by the *key* `expr`
  which matches the same thing as `stuff` would.  Its *argument* is
  `stuff`.  Its value is a *namespace* consisting of a single
  key-value pair, in which the key (`expr`) is bound to the value of
  its argument (the value of `stuff`).
- Juxtaposition such as `"(" \s*` denotes concatenation; unquoted
  whitespace such as that separating the `"("` from the `\s*` is
  ignored.  Such a juxtaposition matches the concatenation of its
  operands, in this case a parenthesis followed by zero or more
  whitespace characters.  Its value is determined by the values of its
  operands as follows:
    1. If both operands evaluate to strings, it evaluates to the
       concatenation of the strings.
    2. If both operands evaluate to namespaces, it evaluates to the
       *union* of the namespaces.
    3. If one operand evaluates to a namespace and the other to a
       string, it evaluates to the namespace; the string is discarded.
       (I’m not sure this is the right thing.)  If you want to keep
       the string instead, you can wrap it in another capture group.
- `[0-9]` is a *character class*; it matches a single character if it
  is '0', '1', ... or '9', which is its value.  Similarly, `[A-Za-z_]`
  matches any ASCII letter or underscore.  (I suppose you’d want to
  use the usual hacks for including `-` or `]` in the class.)
- `[0-9]+` applies a postfix `+` operator to the character class to
  greedily match as many digits as possible.  These digits are
  implicitly concatenated into a string, which becomes the value of
  the capture group `<digits: [0-9]+>`, but see below about `*`.
  Unlike in regular expressions, this greedy matching does not
  backtrack: the pattern `[0-9]+ [0-7]` would always fail, even on
  strings like “97”, because the repetition will consume all the
  digits, leaving none left over for the second character class.
- `|` separates *alternatives*.  As per usual PEG semantics, these
  alternatives are tried in order; the value of the whole alternation
  is the value of the first alternative that succeeded.  Alternatives
  that did not succeed have no effect; any effects that may have
  resulted from attempting to match them, such as consuming input,
  must be undone by *backtracking*.
- `{parse_int(digits)}` is a *result block* which says that the string
  that is the value of `digits` in this context should be passed to a
  function `parse_int` to compute the value of this alternative.
  Result blocks cannot have more constructs to their right.
- `"("` and `")"` match literal parenthesis characters.  Like `[0-9]`,
  their value is the text they matched.
- `\s` matches a whitespace character, and its value is that character.
- `<expr>` denotes a capture group with the key `expr` with the same
  argument as previously.  Since this is within the argument of
  `expr`, it’s a recursive definition.  It’s an error to have two
  capture groups in the same pattern with the same key but different
  arguments.
- Postfix `*` in the argument of `<rest>` and in the argument of
  `<var>` matches zero or more occurrences of its operand.  The
  implicit string concatenation in `<var>` is the same as with `+`,
  but in the case of `<rest>`, the value of its operand is potentially
  some other type: whatever `parse_int`, `read_var`, `negate`, and
  (see below) `add` return, because those are the result blocks that
  flow into it.  So, instead of implicitly concatenating strings, it
  implicitly builds up a list of these items.

The union of two namespaces is defined by giving a magic “collision”
value to any keys that occur in both namespaces, and to any other key
that occurs in one namespace, the value it has in that namespace.  So
`x=1 y=2` unioned with `y=2 z=3` results in `x=1 y=COLLISION z=3`,
even though the value of `y` was the same in both namespaces.

The functions such as `parse_int` and `add` are not built into the
Monkey’s Paw; they are provided by the environment where it is used,
and may be defined in whatever programming language is convenient.
However, under some circumstances, they may be called during a parsing
attempt that is later abandoned.

The “repetition builds up a list” semantics are still underspecified,
and I need to clean that up.

### Further explanation of the example ###

The `{expr}` result block in `"(" \s* <expr> \s* ")" {expr}` means that,
if that alternative succeeds, its value is the value of the argument
of the `expr` capture group (the only capture group with the key
`expr`), and so is the value of the argument of `term`.  Similarly,
`{term}` at the end of the argument of `<first>` means that
`<first>`’s value is the value of `term`, not a namespace containing
the key `term`.

Similarly, `<var: [A-Za-z_][A-Za-z_0-9]*> {read_var(var)}` attempts to
match a letter or underscore followed by zero or more letters,
underscores, or digits; if successful, the string thus matched is
passed to the function `read_var` to compute the value of the
alternative and of the argument of `term`.

Finally, the list of terms and negated terms built up by the `*`
operator in the argument of `<rest>` is passed to the `sum` function,
and its result, along with the value of `<first>`, are passed to the
`add` function, and that becomes the value of the `<expr>` capture
group.  Because of the final `{expr}` result block, that’s also the
value of the entire pattern.

With suitable definitions of the functions used (`parse_int`,
`read_var`, `negate`, `add`, and `sum`) parsing an expression with
this parser can compute a numerical value, an abstract syntax tree, a
polynomial value, a compiled program, and so on.

Rationale
---------

Right now, grammars used for parsing are generally tied to particular
programming languages and even particular applications, which means
that each new application that wants to process a language requires
writing a new grammar for that language.  This lack of reusability
prevents any progress; the closest thing we have to a library of
grammars ready for parsing is Kaitai Struct.

Another problem impeding the construction of a permanent library of
grammars is the wide use of noncomposable grammar families such as
LALR and LL.  When designing a parser, it is useful to have your
parsing system inform you when you have introduced ambiguities so that
you can specify how to resolve them.  But the same intolerance for
ambiguity means that we cannot compose LALR grammars to form new LALR
grammars, and the consequence is that each new grammar is built from
scratch.

PEGs guarantee composability with the same linear-time parsing
performance guarantee as LALR, though they generally consume much more
space and time than LALR or LL parsers.  Their handling of ambiguity
leaves something to be desired, but at least it’s deterministic.

Additional primitive constructs
-------------------------------

The language parser above demonstrates recursion, concatenation,
repetition, alternation, literal strings, character classes, and
result blocks.  These are enough for a significant class of parsers,
but other parsers need more advanced features, and others are more
readable when done differently.  There are some other features in the
Monkey’s Paw which facilitate this: keywords, where-clauses, assertion
blocks, match negation, comments, and seek-clauses.

*Keywords* are alphanumeric words prefixed with `/`.  The currently
defined keywords are `/where`, `/if`, and `/not`.  Keywords permit the
Monkey’s Paw to be extended without compatibility hassles.

### Where-clauses ###

With a where-clause, the above language parser example could be
written as follows:

    <expr> {expr}
    /where (
        <expr: <first> \s* <rest>            {add(first, sum(rest))}>
        <first: <term>                       {term}>
        <rest: ( "+" \s* <term>              {term}
               | "-" \s* <term>              {negate(term)}
               )*>
        <term: <digits: [0-9]+>              {parse_int(digits)}
             | "(" \s* <expr> \s* ")"        {expr}
             | <var: [A-Za-z_][A-Za-z_0-9]*> {read_var(var)}>
     )

A where-clause, which is introduced by the `/where` keyword, provides
one or more definitions of capture group keys which may or may not be
used outside the where-clause.  What the contents of the where-clause
would match, or what value it would have, is inconsequential, because
it is never matched against anything; it is only parsed to find
definitions.

This may be more readable; it’s certainly much closer to a
conventional grammar in form, and it avoids the need for deep nesting.

### Assertion blocks ###

Assertion blocks are a construct to permit arbitrary Turing-complete
code outside the grammar to direct the parsing.

As an example of assertion blocks, Perl has a string syntax drawn from
here-documents in shell.  This Perl expression:

    <<poodle
    dachsund
    bloodhound
    poodle

evaluates to `"dachsund\nbloodhound\n"`, with the line containing just
“poodle” signaling the end of the string.  MIME multipart messages
have a similar syntax.  Recognizing such constructs is not just beyond
the capability of a finite-state-machine parser; it’s beyond the
capacity of a context-free grammar.  But it’s clearly not a difficult
programming task: you just compare each input line in turn to the
end-of-string marker.  Assertion blocks allow such constructs to be
integrated into a pattern match.

An assertion block is similar to a result block, but can occur
anywhere, and is introduced by the keyword `/if`, which is followed by
an expression in parentheses.  The function given in the block is
invoked, and parsing fails if it returns a false value.  Given
functions `getvar`, `setvar`, `not`, `string_equal`, and `strcat`, we can
recognize here-documents as follows:

    <heredoc: "<<" <delimiter: [^\n]+> "\n" /if(setvar("hddelim", delimiter))
       <body: <line: [^\n]*> "\n" /if(not(string_equal(line, getvar("hddelim"))))
              <body> {strcat(line, body)}
              | {""}>
       <line> "\n" /if(string_equal(line, getvar("hddelim")))
       {body}>

This starts by storing the delimiter string under the name `hddelim`
with the `setvar` function, which must not return false.  Then, in a
recursive loop in `<body>`, each line in turn is compared against the
stored delimiter using the `string_equal` function; if it returns
true, the match fails, and the match falls into the other case of
`<body>`, which matches nothing and evaluates to the empty string,
terminating the recursion.  After this, we attempt to read another
line and use another assertion block to require that it *is* equal to
the stored delimiter for the match to succeed.  (Otherwise, for
example, input truncated in the middle of a here-document wouldn’t
provoke an error message).

Something like assertion blocks is necessary not only for languages
like Perl (and, after all, only `perl` can parse Perl) but also for
commonplace syntaxes like C.  This line of C may be either an
expression statement or a declaration:

    t*u;

If `t` is a type previously declared by a typedef, it declares `u` as
a `t` pointer.  If `t` is a variable, instead it requests multiplying
`t` by `u` and discarding the result — useless but syntactically
valid.  Due to operator overloading, in C++ it may not even be
useless.

Note that the `setvar` function here is presumed to have a side effect,
but that side effect must be undone if `setvar` is invoked on a parsing
attempt that backtracks.  This is a general problem for all functions
used in assertion blocks, though most of them solve it the easy way by
having no side effects.

### Match negation ###

The standard example of match negation is C block comments, which do
not nest and therefore cannot contain `*/`:

    <comment: "/*" (/not("*/") [\x00-\xff])* "*/">

Before consuming each byte, this pattern verifies that a `*/` cannot
be matched at that position.  (Note that this parser assumes the input
consists of bytes rather than, say, Unicode codepoints.)

The contents of the negation may match some input and compute some
value, but these are discarded as if by backtracking.

Double negation verifies that a construct *does* match at a given
point, offering a sort of grammar intersection, traditionally provided
in PEGs by the `&` operator.

Parsing algorithms
------------------

See file `packrat-traversal.md` for some thoughts on parsing.

Fake user testing with GPT-4
----------------------------

I thought I would use GPT-4 as a fake user for user testing, to see
what kinds of aspects of the above might be misleading, on the theory
that if GPT-4 couldn’t guess what constructs meant, the average Stack
Overflow poster wouldn’t be able to either.  This was a very
interesting exercise, and gave me some confidence that the Monkey’s
Paw constructs are intuitive.  However, immediately afterwards, I fed
it some obfuscated Python code ([the Hilbert curve in Unicode art in
three lines of code][6]) which is explained almost perfectly, erring
only in guessing that it was the dragon curve rather than the Hilbert
curve.  So perhaps GPT-4 would be able to guess the meaning of
constructs in even a fairly incomprehensible language.

[6]: http://canonical.org/~kragen/sw/dev3/hilbert.py

### Basic comprehensibility seems good ###

Here’s the first astounding response, lightly edited:

> > Can you explain the text-matching pattern
> > `<s: <t: <v: [a-z]+> | <n: [0-9]+> | "(" <s> ")"> (<op: "+" | "-"> <t>)*>`?
>
> This pattern is a form of regular expression used in text matching
> and parsing. It’s designed to match certain types of strings,
> specifically mathematical expressions involving addition and
> subtraction. Here’s a breakdown:
>
> - `<s: <t: <v: [a-z]+> | <n: [0-9]+> | "(" <s> ")"> (<op: "+" | "-"> <t>)*>`:
>   This is the entire pattern.
> - `<s:` and `>` : These define a group named ‘s’. Everything between
>   these tags is part of this group.
> - `<t:` and `>` : These define another group named ‘t’. Everything
>   between these tags is part of this group.
> - `<v: [a-z]+>` : This matches one or more lowercase letters (from ‘a’ to ‘z’)
>   and assigns it to the variable ‘v’.
> - `<n: [0-9]+>` : This matches one or more digits (from 0 to 9) and assigns it
>   to the variable ‘n’.
>
> The ‘|’ symbol represents OR, meaning that either the condition
> before or after can be true for the match to occur. So, `<t:` can be
> either `<v:` (one or more lowercase letters) OR ‘<n:’ (one or more
> digits).
>
> The next part `"(" <s> ")"` allows for nested expressions within
> parentheses.  Finally, `(<op: "+" | "-"> <t>)*` matches an operator
> (‘+’ or ‘-’) followed by another term (`<t>`). The `*` means this
> pattern can repeat zero or more times. So overall, this pattern will
> match mathematical expressions like “a+b”, “1+2”, “(a-b)+3”, etc.,
> where ‘+’ and ‘-’ are operators, numbers are integers, and variables
> are represented by lowercase letters.

This was almost entirely correct, which is astounding, given that this
note (and thus the Monkey’s Paw) is almost certainly not in its
training set.  This gives me some confidence that human beings will
also be able to understand these patterns.
Of course it got some things wrong; it’s not a
regular expression, and the variable names are of course multiple
letters.  I corrected it, with the following results, again
edited:

> > That’s pretty good, except that it’s actually a Monkey’s Paw
> > pattern, not a regular expression, and the variable names can be
> > more than one letter
>
> I apologize for the confusion. You’re correct, this is a Monkey’s
> Paw pattern, not a regular expression.
>
> Monkey’s Paw is a language for defining text patterns and extracting
> data from them. It’s used in Google Assistant to understand and
> respond to user queries.
>
> Here’s the corrected breakdown: (...) So overall, this pattern will
> match mathematical expressions like “abc+def”, “123+456”,
> “(abc-def)+789”, etc., where ‘+’ and ‘-’ are operators, numbers are
> integers, and variables are represented by one or more lowercase
> letters.

It is not, of course, used by Google Assistant, although you can
apparently [annotate training phrases with types][0] for the
deprecated Conversational Actions, including RE2 regular expressions
and things like DateTime and Number, and [define query patterns][1] in
a similar way, with a wider available set of types such as TVSeries,
Email, MusicAlbum, and servesCuisine.  And [DialogFlow has similar
regexp support][2] for things like interactive voice response (IVR).
But as far as I know there’s nothing like the Monkey’s Paw in Google
Assistant.  Maybe we should be thankful.

[0]: https://developers.google.com/assistant/conversational/types
[1]: https://developers.google.com/assistant/df-asdk/reference/action-package/QueryPatterns
[2]: https://cloud.google.com/dialogflow/es/docs/entities-regexp

### GPT-4 suggests using `\s` for whitespace ###

Extending a bit further, I thought it would be worthwhile to see how
it would confabulate whitespace handling:

> > How does it handle whitespace?  How would you change the pattern
> > to handle whitespace differently?
>
> This Monkey’s Paw pattern does not explicitly handle whitespace. It expects no
> spaces between the elements of the expression, so it would match “a+b” but not
> “a + b”.
>
> If you want to allow optional whitespace around the operators and parentheses,
> you can add `\s*` in appropriate places. The `\s` matches any whitespace
> character (like spaces, tabs, or line breaks), and `*` means zero or more of
> them.
>
> Here’s how you could modify the pattern:
>
>     <s: <t: <v: [a-z]+> | <n: [0-9]+> | "\(" \s* <s> \s* "\)">
>        (\s* <op: "+" | "-"> \s* <t>)*>
>
> (...)

(I reformatted the code for readability; it had put it all on one line.)

Before this I was planning to use `_` to match and discard any amount
of whitespace, for example `"(" _ <s> _ ")"`.  But
this does seem like a sensible and straightforward way to specify
whitespace handling, and it’s more comprehensible for new users.

### Or using `{}` for whitespace ###

I thought I’d see what alternatives the entropy monster
would confabulate for me:

> > Actually, that’s not correct; The Monkey’s Paw doesn’t use \s for
> > whitespace, instead providing a more convenient facility.  What is the correct
> > syntax for it?
>
> I apologize for the confusion. In Monkey’s Paw, whitespace is
> handled differently than in regular expressions.
>
> In Monkey’s Paw, you can use `{}` to match any amount of whitespace
> including none. If you want to ensure there is at least some
> whitespace, you can use `{1,}`.
>
> Here’s how you could modify the pattern:
>
>     <s: <t: <v: [a-z]+> | <n: [0-9]+> | "(" {} <s> {} ")">
>         ({} <op: "+" | "-"> {} <t>)*>
>
> (...)

This {} thing seems like a distinctly worse option to me, because who
is going to guess that that means whitespace?

### And it can guess what `\s` means ###

Without any context, I
fed it the \\s option, and it immediately guessed the meaning,
though again attributing irregular powers to regular expressions:

> This pattern is a regular expression (regex) used to match specific
> strings of text. It’s written in a format that’s common in
> programming languages like Python or Java, and it’s used to parse or
> extract information from text. Here’s what each part means:
>
> - `<s: <t: <v: [a-z]+> | <n: [0-9]+> | "\(" \s* <s> \s* "\)">`: This
>   is the main part of the pattern. It matches either a variable
>   (`<v: [a-z]+>`), a number (`<n: [0-9]+>`), or an expression
>   enclosed in parentheses (`"\(" \s* <s> \s* "\)"`). The `\s*` parts
>   mean that there can be any amount of whitespace (including none)
>   before and after the matched string.
>
> (...)

### But the same test fails with `{}` ###

Given instead the `{}` option, it guessed that it was a wildcard
character, despite the contextual clues that would make whitespace
more useful:

> (...)
>
> \5. `"(" {} <s> {} ")"`: Matches an opening parenthesis `"("`,
> followed by any character `{}`, then follows the pattern defined by
> symbol `s`, again any character `{}`, and finally a closing
> parenthesis `")"`.

### GPT-4 suggests using `<a> <b> -> <b> <a>` for rewrite rules ###

I thought I would see what kind of pattern-rewriting syntax it would
confabulate, so I asked it:

> > How would you construct a Monkey’s Paw rewriting pattern grammar
> > that converts these expressions into RPN?
>
> (...)
>
> To convert these expressions into Reverse Polish Notation (RPN), we
> need a rewriting grammar that changes the order of operands and
> operators. Here’s an example:
>
>    <s: (<t1: <v1: [a-z]+> | <n1: [0-9]+> | "\(" \s* <s1> \s* "\)">)
>        (\op1: "+" | "-") (2: <v2: [a-z]+> | <n2: [0-9]+> |
>                              "\(" \s* <s2> \s* "\)">)*>
>    ->
>    <s1: (<t1>) (<op1>) (<t2>)>
>
> This pattern matches the same expressions as before, but reorders
> them into RPN. For example, "abc + def" would be rewritten as "abc
> def +", and "( abc - def ) + 789" would become "abc def - 789 +".

This is of course buggy as hell, but it does suggest that the `<a> <b>
-> <b> <a>` syntax is sort of reasonable.  Which is not what I’m
using!

### It can guess that `_` consumes whitespace ###

Let’s see if it can guess that the `_` syntax I used to use is meant to
eat whitespace.

> > Can you explain the text-matching pattern `<s: _ <t: <v: [a-z]+> _
> > | <n: [0-9]+> _ | "(" _ <s> ")" _> (<op: "+" | "-"> _ <t>)*>`?
>
> This pattern is a complex one and seems to be used for parsing
> mathematical expressions. Let’s break it down:
>
> - `<s: _ <t: <v: [a-z]+> _ | <n: [0-9]+> _ | "(" _ <s> ")" _>` : This part
>   of the pattern is defining a rule for matching a term `s`. A term can be either:
>     - A variable `v` which consists of one or more lowercase letters (`[a-z]+`),
>     - Or a number `n` which consists of one or more digits (`[0-9]+`),
>     - Or another expression enclosed in parentheses, where the
>       expression itself is defined by the same rule `s`.
>
> The `_` symbol usually represents any amount of whitespace.

It confused `s` and `t` but did successfully guess that `_` was for
whitespace.

### It suggests the unreadable `<v:_: ...>` as a whitespace-ignoring construct ###

Another alternative I’ve thought about, used by Darius
Bacon (in Parson, I think) is to have two kinds of nonterminals: those
that ignore internal whitespace and those that do not.  It
confabulated something kind of ugly:

> > How would I change this pattern to use the special no-whitespace
> > Monkey’s Paw group type to not allow whitespace inside numbers and
> > variable names?  Right now it thinks `9 9` is a valid number and
> > `no you` is a valid variable name.
>
> (...)
>
> To prevent whitespace inside numbers and variable names in Monkey’s
> Paw, you can use the special no-whitespace group type `<_:`. Here’s
> how you would modify your pattern:
>
> - `<s: <t: <v:_: [a-z]+> | <n:_: [0-9]+> | "(" <s> ")"> (<op: "+" | "-"> <t>)*>`
>
> This will ensure that `9 9` is not considered a valid number and `no
> you` is not considered a valid variable name. The `_:` group type
> does not allow any whitespace characters within its match.

However, upon being presented with this syntax without context, it
was unable to guess its meaning:

> > Can you explain the text-matching pattern
> > `<s: <t: <v:_: [a-z]+> | <n:_: [0-9]+> | "(" <s> ")"> (<op: "+" | "-"> <t>)*>`?
> > How does it work?
>
> This pattern is a recursive pattern used for parsing expressions in a language.
> It’s written in a format that’s often used in compiler and interpreter design,
> and it’s designed to match mathematical expressions with addition and
> subtraction operations.
>
> (...)
>
> > What’s with the `_:` in the `n` and `v` definitions?
>
> The `_:` in the `n` and `v` definitions is a placeholder for a
> label. In this pattern language, you can label parts of the pattern
> to refer back to them later. However, in this case, the author of
> the pattern has chosen not to give these parts of the pattern
> labels, so they’ve used `_:` as a placeholder.

### It can guess that `/` rather than `|` also means alternation ###

Using `/` rather than `|` didn't impede it from roughly guessing the
semantics:

> \4. `/` This symbol is used as an OR operator, meaning that the pattern will
> match either what comes before or what comes after this symbol.

### It can successfully guess the meaning of `{parse_int(digits)}` ###

I tried feeding it an earlier version of the parser/evaluator given in
the introduction section above (using `_` for whitespace), and it
handled it successfully:

> > How does this infix expression evaluator written in the Monkey’s
> > Paw work? `<expr: <first: <term: <digits: [0-9]+>
> > {parse_int(digits)} | "(" _ <expr> _ ")" {expr} | <var:
> > [A-Za-z_][A-Za-z_0-9]*> {read_var(var)}> {term}> _ <rest: ( "+" _
> > <term> {term} | "-" _ <term> {negate(term)})*> {add(first,
> > sum(rest))}> {expr}`
>
> This expression evaluator is written in a language called PEG
> (Parsing Expression Grammar), which is used to define syntax for
> languages. The Monkey’s Paw is a tool that uses PEG to parse and
> evaluate expressions.
>
> Here’s how this evaluator works:
>
> 1. `<expr: <first: <term: ...> {term}> _ <rest: (...)*> {add(first,
>     sum(rest))}> {expr}`
>
>     This is the main rule of the grammar. It matches an expression
>     (`expr`), which consists of a `first` term followed by zero or
>     more additional terms (`rest`). The `_` symbol represents
>     whitespace, which can appear between terms. After parsing, it
>     adds up all the terms using the `add(first, sum(rest))`
>     function.
>
> 2. `<term: <digits: [0-9]+> {parse_int(digits)} | "(" _ <expr> _ ")"
>     {expr} | <var: [A-Za-z_][A-Za-z_0-9]*> {read_var(var)}>`
>
>     This rule matches a term in the expression. A term can be
>     either:
>
>     - A sequence of digits (`[0-9]+`), which are parsed into an
>      integer using `parse_int(digits)`.
>
>     - An expression enclosed in parentheses (`"(" _ <expr> _
>       ")"`). The result of evaluating this sub-expression is returned
>       as-is.
>
>     - A variable name (`[A-Za-z_][A-Za-z_0-9]*`), which starts with
>       a letter or underscore and may contain alphanumeric characters
>       and underscores. The value of the variable is retrieved using
>       `read_var(var)`.
>
> 3. `<rest: ( "+" _ <term> {term} | "-" _ <term> {negate(term)})*>`
>
>     This rule matches zero or more additional terms that are
>     preceded by either a plus or minus sign. If the term is preceded
>     by a plus sign, it’s added to the total; if it’s preceded by a
>     minus sign, it’s subtracted from the total (by negating its
>     value using `negate(term)`).
>
> In summary, this evaluator parses and evaluates infix expressions that consist
> of integers, variables, and parentheses, with addition and subtraction
> operations. It uses recursive descent parsing: when it encounters parentheses,
> it recursively parses the enclosed sub-expression before continuing with the
> rest of the expression.

Human quasi-user-test
---------------------

I showed an experienced human volunteer the same two examples; first

    <s: <t: <v: [a-z]+> | <n: [0-9]+> | "(" <s> ")"> (<op: "+" | "-"> <t>)*>

He correctly guessed that the nonterminals were abbreviations of
`sum`, `term`, `var`, and `num`, and pointed out that it was odd that
variable names couldn't contain numbers.  He also correctly guessed
that since whitespace wasn’t mentioned, the parser would reject it.

Then I showed him

    <expr: <first: <term: <digits: [0-9]+>              {parse_int(digits)}
                        | "(" _ <expr> _ ")"            {expr}
                        | <var: [A-Za-z_][A-Za-z_0-9]*> {read_var(var)}> {term}>
           _ <rest: ( "+" _ <term>  {term}
                    | "-" _ <term>  {negate(term)}
                    )*>
           {add(first, sum(rest))}>  {expr}

and asked him to explain how it worked.  He had some difficulty
counting the nested angle brackets.  Otherwise he understood
everything.

Public human quasi-user-test
----------------------------

I [posted a comment on the orange website quoting some of the above
parsers][3] and got interesting feedback from [Vidar Hokstad][4], who's
working on an embedded parsing DSL in Ruby; quoting in part:

> Your format is interesting. I read it as the `<name: ...>` bit
> serving as a capture?  If were to map that to Ruby, I’d probably use
> the Hash syntax to try to approximate it, so you’d end up with
> something [like] "`{expr: {first: ...}`".  Incorporating the actions
> without some extra clutter would be a bit tricky, because the
> operator [overloads] won’t let you add a block, and so you’re stuck
> with a minimum of `->() {...}` or `lambda { ... }`, but I think
> representing it reasonably cleanly would be possible (of course this
> looks like a simple enough format to just parse directly as well).

[3]: https://news.ycombinator.com/item?id=38243309
[4]: https://hokstad.com/

This seems promising!

The Monkey’s Paw’s grammar in itself
------------------------------------

The Monkey’s Paw can define its own language in about 20 lines of
code, without any rewrites:

    <expr: <term> <alternatives: ("|" <_> <term>)>>
    /where (
        <_: (\s | "#" [^\n]* "\n")*>
        <term: <factor>+ ("{" <_> <tree> "}" <_> |)>
        <factor: <atom> <_> <postfixes: (<any: "*"> <_> | <some: "+"> <_>)*>>
        <atom: "(" <_> <expr> ")" <_> | <group> | <string> | <class>
             | <where> | <not> | <assertion> | <escapeseq>>
        <group: "<" <name> (":" <_> <expr> |) ">" <_>>
        <string: "\"" <contents: ([^\\"] | <quoted>)*> "\"" <_>>
        <quoted: "\\\\" | "\\"" | "\\n">
        # XXX \n in classes.   also the ]- syntax is wrong, and a range
        # standardly can start with -.
        <class: "[" ("^" |) ("]" |) ("-" |) ([^]-]+ ("-" [^]] |))* "]" <_>>
        <where: "/where" <_> <factor>>
        <not: "/not" <_> <factor>>
        <assertion: "/if" <_> "(" <_> <tree> ")" <_>>
        <escapeseq: "\\s" <_>>
        <tree: <name> ("(" <_> <tree> ("," <_> <tree>)* ")" |) | <string>>
        <name: [-a-zA-Z0-9_]+>
    )


<link rel="stylesheet" href="http://canonical.org/~kragen/style.css" />

<script src="http://canonical.org/~kragen/sw/netbook-misc-devel/addtoc.js">
</script>
