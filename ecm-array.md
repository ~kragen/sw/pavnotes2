I was thinking about the limits of ECM.  I think the voxel power of
ECM (see file `voxel-power.md`) with conventional electronics likely
depends on the process gap.

A relevant [open-access review paper was published last year][0].

[0]: https://chemistry-europe.onlinelibrary.wiley.com/doi/pdf/10.1002/elsa.202100123 "Electrochemical 3D micro- and nanoprinting: Current stateand future perspective, by Hengsteler, Lau, Zambelli, and Momotenko"

If you’re scanning an ECM electrode rapidly across a flat surface
etching pits in it, you can probably modulate the electrical ECM
signal at about 1 MHz or so, depending on the parasitic inductances
and capacitances in the system.  If you have ten such electrodes, you
ought to be able to deliver 10 megavoxels per second.  But it might
turn out that in practice you can only get kilovoxel rates with any
precision, depending on how long the electrically stimulated reactions
actually take to ramp up and down; PECM setups commonly vibrate the
electrode at kilohertz rates.

100 A/dm² is about 500 nm/s of deposition or erosion
----------------------------------------------------

Such ramping effects, though, would only stop you from making
microsecond-scale pulses; you could still have millisecond-scale
pulses whose length is controlled to microsecond precison.  But in
this case another problem rears its head, that of the cutting speed.
Consider the case where you’re electrolytically cutting at a current
density of 100 A/dm², a Faraday efficiency of 50%, and a divalent
product.  The volume electrochemical equivalent for divalent iron is
13.25 μm dm²/A h (see file `ecm-notes.md`), so 100 A/dm² gives us
370nm/s of cutting speed.

The survey paper linked above
reports deposition rates for a variety of electrodeposition techniques
in the range 115-500 nm/s for micron-scale electrodeposition,
confirming that this first-principles calculation
applies in the real world.
It also reports lateral voxel sizes on the order of 150-10000nm.

By cutting off the current with a temporal
precision of 1 μs, you get, in theory, a cutting depth precision of
370 *femto*meters, about a thousandth of an atom.  This may not be
useful, both because you’re unlikely to know where the surface was to
start with with such precision, and because for many purposes the
inevitable atom-sized asperities in the surface make this less
important.

You might be able to measure the surface location to such precision by
tapping on it with a probe, the way STMs, AFMs, and most relevantly,
SECMs do; but you definitely can’t etch away less than an atomic
thickness of material at a point, though you may be able to, on
average, over an area.

SECMs typically have precision on the order of microns (limited by
convolution with the field around the microelectrode, which has a
diameter on the order of a micron) using current on the order of tens
to thousands of picoamps.  So, if you’re trying to use SECM microscopy
to measure your surface topography, these low currents might be a
limiting factor on speed, as you have to integrate them over a long
enough time to average out EMI.

For visible-light optics you want depth precision of about 40 nm, a
tenth of a wavelength of blue light.  Suppose you’re shooting for
voxels of about 40nm in all three dimensions.  Then etching away or
electrodepositing a voxel takes 100 milliseconds, wasting five orders
of magnitude of timing precision and possible voxel power.  Can you
boost the current?

Maybe you could get 1MA/dm² and thus 5mm/s
------------------------------------------

Much higher current densities would be necessary before
microsecond-scale pulse control can increase your voxel power, maybe
five orders of magnitude.  But I’m not sure higher current densities
are possible in practice without overheating the apparatus.  10 A/dm²
is in the normal range for plating but too slow for electroforming or
ECM.  100 A/dm² at a moderate 1.5 V is already 1.5 W/cm².  At the
point that we are focusing 1000 A/dm² we have 15 W/cm², which seems
still within the range for water cooling.  But at 1 MA/dm², even if
the overvoltage required doesn’t increase, we have 15kW/cm², 150W/mm²,
and 3.7mm/s, so microsecond pulse control gets you 3.7-nm
depth precision.  The above-mentioned 40 nm
would be a microsecond at 11MA/dm².

I suspect that in fact reaching higher current densities requires
nearly proportionally higher overvoltages; at narrower process gaps
you need less overvoltage, but the dielectric strength of water limits
how narrow you can make the process gap.  I don’t know that you can
get down to the nanometer range that would allow you to pattern the
surface at this 40-nm scale.

If we’re talking about a cutting electrode that is itself 40 nm in
size, 11 MA/dm² is only about 2.8 μA, so it’s not a large absolute
amount of current.  A 40 nm cube of copper is about 0.4Ω, or would be
if it had the same conductivity as bulk copper anyway, so the voltage
drop across the tip is only a microvolt or two.  Even materials of
resistivity an order of magnitude higher would be okay.  (Graphite
and other carbon is potentially relevant.)  The tip can
be the end of a cone, as in an STM or AFM, so the resistivity of the
rest of the wire leading up to it can be negligible.

This microvolt is low enough that I think it might actually be
feasible to get these mega-amps per square decimeter across a
micron-sized process gap, with an extra factor of 25 ohmic resistance
from the distance and another factor of 100 for not being a metal.
That’s still only 3 millivolts.

[PECM commonly uses][2] 1000A/cm², which is 0.1Ma/dm², suggesting that
this might be feasible.

[2]: https://www.youtube.com/watch?v=12-IOyuPJZo?t=2m0s

An array to boost voxel throughput to a teravoxel per second
------------------------------------------------------------

An array of such cathodes, individually movable in Z to control the
process gap, might be spaced 10 microns apart, so a square centimeter
would contain a million of them.  They could conceivably pattern a
surface at a teravoxel per second if each one is delivering a
megavoxel per second, but if the process gap is an entire micron,
you’re probably spreading that 2.8 μA over about a square micron.
This means you only need about 400 μs pulse control to hit the
requisite 40 nm depth precision, so you really only have about 2500
voxels per second per electrode, or 2.5 gigavoxels per second across
the whole array.

Of course the million-channel control electronics would be a challenge
of its own; you’d have to fabricate it in a way integrated with the
array, like a DLP chip or an LCD.

You could maybe get more voxel power per channel despite a
micron-scale process gap with still higher current densities.  Molten
salt electrolytes might be a reasonable way to get them without
overheating the apparatus.

Bootstrapping up to a large array
---------------------------------

A lot of electrodeposition microfabrication is done with dielectric
pipettes (normally glass) with the anode deep inside them, so the
geometry of the anode itself is inconsequential.  But let’s consider
the fabrication of conductive metal sharp tips of the type commonly
used for AFM, STM, and occasionally SECM, which can be used for
cutting (in ECM mode) or possibly even for electrodeposition, for
example for a very short time or if the anode reaction is purely
hydrolytic rather than consuming the anode.

### Direct single-step electrodeposition is too slow, but trusses could work ###

Suppose you want to electrodeposit a tip, using the 500nm/s speed I
earlier said was common for electrodeposition, so you get ten voxels
per second per active tip.  A solid tip that tapers conically to a
point over a height of 100μm up to a diameter of 100μm so you can grab
it with macroscopic tools is a quarter million cubic microns, or 4.1
billion voxels.  That would take 13 years to deposit.  (Chopping the
tip off at 40nm diameter makes an insignificant difference to the
volume or time.)

Suppose instead we build a truss structure of 40-nm-diameter wires,
forming a conical shell two layers thick, with an amount of solid
material equivalent to covering about 5% of the surface of the cone,
say because the wires are about a micron long between truss joints.
Maybe you build it on the tip of a 100-μm-diameter wire.  *πr*√(*r*² +
*h*²) works out to about 18000 square microns, so we’re talking about
a volume of 1000 square microns by 40nm, about 625000 voxels.  That’s
a deposition time of about 17 hours, which is significantly less than
13 years.

The delicate truss would probably be destroyed by the immense forces
of surface tension if allowed to dry, unless dried in a supercritical
drier; maintaining it submerged for its entire life may be the best
expedient.

(Maybe these tips are platinum or lead oxide or graphite or something
so they don’t themselves erode when you use them as anodes, getting
the cations to electrodeposit from flowing electrolyte rather than
from the anode.  For the time being, forget about the problem of
converting the electrodeposited material to something that can
withstand attempted anodic dissolution.  This problem goes away for
ECM, which is subtractive and doesn’t erode the tooltip, a fact which
is probably relevant to precision as well.)

I don’t think these are very satisfying answers, though.  At 400 nm
per second we can traverse the 112-micron slant length of the cone in
5 minutes.  Three such wires form a tip.  The distance between 15
minutes and 17 hours seems unreasonable.  (And, of course, if you can
do the 1MA/dm² current density suggested earlier, you go 5 microns per
millisecond, so instead of 15 minutes, it’s 67 milliseconds.)

### Hogging and finishing ###

An approach commonly used in conventional machining is to first hog
out the rough shape of a part with a rough tool and rough tolerances,
removing the bulk of the material, and then do a finishing pass with a
much lower material removal rate and often a different, more precise
tool.  This seems like it ought to work for ECM and electrodeposition
as well.

Perhaps you electrodeposit the bulk of your 100-μm-diameter cone with
a 10-μm wire at the same 400nm/s current density, thus requiring 30
seconds per 10-μm voxel.  250 of those voxels make up the rough shape
of the solid cone, and this takes 2 hours instead of 13 years.  Then
you have a shell to fill in around the outside of the cone of roughly
5μm thickness; that surface is 18000 square microns, as I said before,
so this is 90000 cubic microns.  With a 1-μm wire depositing 1-μm
voxels in 3 seconds each, this requires another 270000 seconds, 3
days.  Then you have a remaining shell averaging 0.5 μm (9000 cubic
microns) to deposit with, say, an 0.1-μm wire in 250ms per voxel.
This takes a month.

But you don’t actually care about the surface roughness of most of
your tip.  You could build up the main cone in 2 hours, then build a
1-μm-resolution cone on its tip with a volume of just 333 cubic
microns and thus 333 voxels, taking 10 minutes, and then build a
40-nm-resolution cone on its tip in an even shorter period of time.
The rest of the cone’s surface finish can go fuck itself.

A similar multiscale approach can be used for ECM: cut the rough shape
with a 100-micron-diameter sphere, then a finer shape with a
10-micron-diameter, then finer still with 1-micron-diameter.  For
things like conventional optical surfaces the first scale is
sufficient, since you can still position the larger tool with the same
precision; you just can’t cut inside corners.

### Subtractive electrolytic sharpening ###

An alternative way to get a sharp tip is to sharpen a wire
electrolytically, an operation which can be repeated if the wire gets
dull, as it will if you’re using it as a metal source in
electrodeposition.  Four methods occur to me to do this:

- Isotropic: a common method for making STM and AFM tips today is
  isotropic anodic dissolution of a round wire, starting at its
  surface, until the wire breaks.  This requires the electrolytic
  conditions to be maintained in the electropolishing regime, where
  bubbles are not evolved, and erosion is concentrated at surface
  asperities rather than, say, at grain boundaries.  Up to a point,
  such isotropic morphological erosion can sharpen a spherical point,
  but to the degree to which it concentrates on surface asperities, it
  will tend to embiggen the tip’s radius instead of ensmallening it.

- Pyramid: by eroding the wire with a flat planar electrode larger
  than the wire, in three non-parallel positions, the intersection of
  the planes defines a point.  Only enough precision is needed to
  ensure that this point is within the bulk of the metal; on initial
  sharpening, it can be anywhere within the wire, and on later
  sharpenings, you need to remove enough metal to get below the
  previous point.  Since by hypothesis the wire is smaller than the
  tool electrode, it will probably be easier to rotate the workpiece
  than to revolve the tool around it.  Flat surfaces can be achieved
  by the so-called Whitworth three-plate method, though probably
  Whitworth didn’t invent it.  This can be done electrolytically if
  the surfaces are immersed in an electrolyte that permits anodic
  dissolution but not electrodeposition, for example because the pH is
  too low.

- Cone: by rotating the wire continuously against the flat cathode
  instead of only to three positions, a conical tip rather than a
  pyramidal tip is obtained.  A disadvantage is that the material
  removal rate is lower than for the pyramid method because of the
  line contact between the workpiece and tool: at the same
  overvoltage, which must be limited in order to stay in the desired
  anodic dissolution regime, you will get much less current and
  therefore much less material removal.  An advantage of the cone is
  that it avoids strong electric potential gradients around the edges
  of the tip when in use, because it doesn’t have any edges.

- Two cones: instead of going indirect through flat surfaces, you can
  sharpen two dull cones against one another, rotating each one around
  its axis while translating them to one another parallel to their
  slant angle, alternating the current polarity so they dissolve
  alternately, and moving them closer as they become smaller.
  (Alternatively, rotating a single point against a flat surface.)
  The line contact between the cones will not erode them both
  uniformly, because as they are translated it will usually not extend
  over the entire cone length.

All of these techniques can be combined with the “primary edge,
secondary bevel” approach conventionally used for sharpening
macroscopic cutting tools down to microscopic edges: a “primary” tip
with a small included angle is produced with a higher-MRR technique,
and then a much more precise “secondary” tip with a larger included
angle is cut into its tip, removing only a tiny amount of material,
which often permits the use of much higher precision.  The secondary
tip can also be resharpened several times before having to resharpen
the primary tip.

In particular, the combination of a primary pyramid tipped with a
secondary cone seems appealing.

These sharpening approaches would seem to offer a promising “top-down”
route to microfabrication.

### Multiplying a template through multiple generations ###

Consider, too, die-sink ECM.  If you can sink the ECM die at 400nm/s
on average then it should take 4 minutes to make an arbitrary number
of tips from a single negative die.  You can start with one positive
(male) tip made by meniscus-controlled electropolishing of a round
wire under meniscus control until it breaks, as is commonly done for
STM tips, then use it to cut four negatives in a die in 16 minutes.
Then you can use that die to cut four positive tips onto a third
workpiece in 4 minutes, which you can then use to cut sixteen negative
spaces into a die in 16 more minutes.

Every 20 minutes you do two workpiece swaps and polarity reversals and
the number of tips quadruples.  So after three hours you have a quarter
million tips.  They’ll probably be dull, due to accumulated error over
12 generations; but you can sharpen them electrolytically, just the
way you did the original wire, but for much less time.

It might be interesting to use this approach with movable type;
instead of digging several cone-shaped holes in the first die, you dig
several hundred letter-shaped holes in it with letter-shaped cathodes
(ideally many at a time, using multiple concurrent “print heads”), and
then you can use this “stereotype” to electrolytically erode a “cast
plate”, which can then be used for ink-stamping onto a surface.  The
replacement of letters with mask sets for standard cells for planar
integrated circuits is obvious.

This is a couple of steps shorter than the standard Gutenberg
printing-press process, because there the first step is to strike a
punch into copper to make a copper matrix, from which you can cast as
many “sorts” of each letter as you need from typemetal, which are then
packed into a “chase” to make a “forme”, which can then be used either
directly to print on paper (as Gutenberg did), or to make a “flong”
from papier-mâché or plaster, into which a “stereotype” of typemetal
is cast, which is what is inked.

This three-step process is in between these processes (of five and
seven steps, respectively) and Bi Sheng’s original two-step
movable-type process, in which porcelain characters were arranged on a
plate and pressed directly against a page.

### Sheet cutting ###

If you can cut the ECM electrode out of a metal foil, you may be able
to get by with immensely less cutting, even for single-point cutting.
224 microns of cutting a sheet with something resembling a wire stuck
through it at 400nm/s is 10 minutes.  You might need several times
that.

Improving precision by combining ECM with electrodeposition
-----------------------------------------------------------

Unless confined by something like a meniscus, both ECM and
electrodeposition sort of fade out over a large area, compromising the
technique's precision for shapes with high detail.  An additional
shielding anode, made of graphite or lead dioxide or mixed metal
oxides or platinum, can be used to prevent unwanted electrolytic
erosion far from the cutting cathode.  The conventional approach is
just to insulate the electrode with glass, but a shielding anode can
cut off the electrical field entirely a short distance beyond it, and
so ought to be more effective.

A different way to look at it is that now you have *two* tool
electrodes, one depositing and one eroding, and the boundary between
the deposition region and the erosion region is quite sharp.  Faraday
efficiency suffers.

The same is true if you use the same electrode for both processes, one
after the other, in different positions.  This means you’re doing ECM
cutting with a roughened electrode, but it avoids the problem of metal
or hydroxide building up on your shielding cathode.

This approach also enables “depolarizing” the workpiece, and as I
understand it, that is why it is commonly used in PECM, without
repositioning the tool electrode in between electric pulse polarities.

Bulk electrodeposition on a scaffold
------------------------------------

If you electrodeposit a thin scaffold or truss of wires through
selective electrodeposition, you can later embiggen it by by using it
as an electrode for conventional electrodeposition.  Three techniques
here seem likely to be important:

- Electrodepositing metal on the cathode to embiggen it with metal
  (the most conventional kind).

- Electrolytic mineralization, for example depositing magnesium
  hydroxide or calcium carbonate from solutions of magnesium or
  calcium ions.

- Ionic cross-linking, where the (possibly embiggened) structure is
  deliberately anodically dissolved as a source of polyvalent cations
  to precipitate a solid from an electrolyte prone to such
  precipitation, such as soluble silicates, phosphates, or organic
  ionomers.  To ensure that the entire sacrificial scaffold dissolves,
  it must taper continuously wider up to the root, like a tree, so
  that the voltage gradient due to the resistivity of the wire and
  minor uncontrolled variations in dissolution rate don’t strand an
  intact piece of the electrode in the insoluble precipitate, where it
  can derange electrical fields forever after.  Unless that’s what
  you’re into.

These are potentially important not just because they ease the
material-deposition requirements on the high-precision
electrodeposition system, but because they permit, in effect,
electrodeposition of dielectrics.

A crucially important technique is to build structures that are partly
dielectric and partly conductive, in particular with metallic
conductive parts supported by dielectric parts, for example flexures,
insulating sheaths, or mandrels that carry magnetic coils.  This can
be achieved by depositing two metals, such as copper and iron, and
setting the ionic dissolution voltage to sacrifice the more reactive
metal without tarnishing the nobler one.

