So far, ARM32 assembly just looks super sweet, like, more convenient
to program in than even i386.  I didn't realize RISC could be this
nice.

Combining these observations with the items in file `or10n-notes` and
in file `barrel-shifter-datapath.md`, despite not having written any
ARM assembly yet and only a little RISC-V, I feel like I have a good
idea of what kind of computer I wish I had:

- Load-store architecture, RISC-style.
- Mostly without microcode, RISC-style.
- 3-operand register-to-register ALU instructions, RISC-style.  Not,
  say, an operand stack, or 8086-style 2-operand instructions, or
  8086-style memory operands.
- ARM-like pre- and postincrement addressing modes, because they shave
  an instruction off non-leaf function prologues and epilogues and
  because they make iterating over an array twice as fast and more
  readable.
- ARM-like base+offset+index addressing mode for at least loads,
  stores, and adds (LEA).
- More restricted ARM-like in-instruction shifts for register
  operands: 0-7 bits, only to the left, so only 3 bits instead of 8.
  Not sure if for general ALU operations or just scaling indices for
  addressing.
- No delay slots for branches.  No SPARC-style register windowing.
- A link register instead of a hardware stack.
- 31 32-bit general-purpose registers, RV32-style.  This implies 15
  bits of register routing in a 3-operand instruction.
- A zero register, RISC-V-style, to reduce the complexity of
  instruction decoding.
- Tiny pet peeve: in the ABI, put a single-register return value in a
  *different* register than a single-register argument list, because
  they are almost always live simultaneously in the callee, so putting
  them in the same register requires unnecessary movs.
- Making PC inaccessible as a normal register, RISC-V-style.
- AUIPC instruction for PC-relative addressing, RISC-V-style.
- But no LUI instruction, because a LUI+ADDI pair is probably slower
  than just loading a constant from an ARM-style constant pool.
- Multiple in-order shared-nothing cores on a chip, each with tens or
  hundreds of kilobytes of private RAM, rather than SMP.
- NOR Flash program boot memory, mapped into the same memory space as
  the RAM (von Neumann, not Harvard).
- 32-bit addressing for off-chip RAM.  Maybe virtual memory: 128 KiB
  would be 32 4-KiB pages, and invoking a QSPI page fault routine when
  a memory reference misses in a 32-item TLB doesn't seem like an
  unreasonable amount of complexity.  Basically it's conflating L1
  cache with virtual memory, dunno if that's a reasonable idea.
- Barrel processors with two or four register sets, alternating cycle
  by cycle, to provide deterministic I/O timing and to reduce the
  pressure on things like branch predictors.  This obviates a lot of
  hardware peripherals.
- RISC-V-like crazy scrambled instruction formats to maximize decoding
  speed.
- RISC-V-like millicode for LDM/STM context save and restore, to avoid
  stalling barrel pipelines and delaying interrupts, without
  sacrificing the convenience of terse prologues and epilogues.
- One level of zero-overhead hardware definite looping, with special
  registers for loop start, loop end, and loop count remaining.  Aside
  from the DSP applications targeted by OR10N, this compensates for
  the absence of LDM/STM in fast memcpy, memset, memcmp, etc.
- RVC-like compressed instructions: freely intermixable instruction
  lengths, trivially decoded, with special-purpose registers, assigned
  enough of the encoding space to be useful.  No T-like mode bit!
- Hardware multipliers, even if not single-cycle, with
  multiply-accumulate.
- No divide instruction.
- RISC-V-style compare-and-branch instructions rather than condition
  code flags and/or ARM-style predication bits.
