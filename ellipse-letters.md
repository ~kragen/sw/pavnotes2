I was thinking about the Fourier character generator thing (see file
`lfsr-letterforms.md`) and an interesting idea occurred to me.  What
if you compose letterforms by sequentially drawing a series of
ellipses with a CRT or laser?  In an analog circuit each ellipse is
characterized by:

- *x* offset *x*₀;
- *y* offset *y*₀;
- *x* amplitude *b*;
- y amplitude *c*
- phase offset *φ*.

The parametric equations for the ellipse are then

> *x* = *x*₀ + *b* cos *t*  
> *y* = *y*₀ + *c* sin (*t* + *φ*)

which can be used to draw an ellipse of any size, aspect ratio, and
orientation.  For circuit design, I suspect it is productive to
rewrite the second equation with the angle-sum formula so that all the
transcendentals are either constants (within a given ellipse) or
functions of *t*, because at ordinary AF and RF frequencies, applying
time delays to signals is difficult, especially variable time delays.

In particular this can provide you with straight line segments in any
orientation.

The idea here is that, at each clock cycle, you load new (*x*₀, *y*₀,
*b*, *c*, *φ*) into your deflection-generation circuit while the beam
is blanked, and draw a new ellipse.
