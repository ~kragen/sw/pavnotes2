This refurbished Dell Inspiron 1440 has 4GiB of RAM.  memorystock.com
says it “takes the DDR2 PC2-6400 800MHz SODIMM type” and [Dell forums
say it can be upgraded up to 8GiB][0].  [The _Setting Up Your
Inspiron_ manual][1] confirms the 8GiB and DDR2 aspects (p. 57,
59/70), but is silent on the clock speed, perhaps because they offered
the same model of laptop with processors demanding 667 MHz, 800 MHz,
and 1066 MHz.

[0]: https://www.dell.com/community/Inspiron/DELL-INSPIRON-1440-RAM-UPGRADE/td-p/7502681
[1]: https://downloads.dell.com/manuals/all-products/esuprt_laptop/esuprt_inspiron_laptop/inspiron-1440_setup-guide_en-us.pdf

The [Inspiron 1440 service manual][2] shows the procedure for removing
the back cover.  My Phillips #0 screwdriver was not enough to open the
cover; it came off only with the application of a disquieting amount
of force, launching the not-quite-captive screw into parts unknown and
revealing two Kingston 2GB 2Rx8 PC2–6400S 666-12-E2 SODIMMs (and a
Broadcom wireless card).  So I guess I probably don’t need 800 MHz
memory, I have an inferior CPU.

[2]: https://dl.dell.com/manuals/all-products/esuprt_laptop/esuprt_inspiron_laptop/inspiron-14_service%20manual_en-us.pdf

4GiB DDR2 SODIMMs do exist but are unusual, selling for [US$70
(AR$19520) new][3] or [half that from a seller who claims they haven’t
tested them and bought them by mistake][4].  I’d need two: US$140.  Or
US$280 if I have to buy twice to get ones that work.  An 8GiB DDR3
SODIMM is instead AR$7000–AR$12000 (US$25–43) from numerous sellers,
so I guess there’s a major scarcity premium already.

[3]: https://articulo.mercadolibre.com.ar/MLA-678936125-4gb-ddr2-667-800-mhz-sodimm-para-notebook-nueva-sellada-_JM
[4]: https://articulo.mercadolibre.com.ar/MLA-1159063342-memoria-ddr2-4gb-pc2-6400-800mhz-sodimm-_JM

US$140 seems expensive to me, but the competition is something like
[this mystery meat Asus laptop for US$214][5], [this 14" 3kg Dell
E7450 with 8GiB, 1366×768, and an i5 for US$240][6], or [this 16.5"
Acer Aspire 3 A315-21 W10 2.3 GHz with an four-core A4-9125,
1920×1080, and 8GiB DDR4 for US$250][7] and a 120GB SSD.

[5]: https://articulo.mercadolibre.com.ar/MLA-1169372190-asus-notebook-pc-_JM
[6]: https://articulo.mercadolibre.com.ar/MLA-1148861273-notebook-dell-e7450-i5-23ghz-8gb-500gb-hdd-5ta-gen-_JM
[7]: https://articulo.mercadolibre.com.ar/MLA-1161941657-pc-notebook-acer-_JM

