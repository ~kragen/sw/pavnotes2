I've been doing a journaling exercise every morning; the exercise was
originally proposed as writing three pages longhand every morning, but
I write small, so I asked a friend how much three pages was.  She said
that for her it was about 750 words, so I'm shooting for that.  But
that leaves the question of how to find out how many words I'm writing
longhand.

My approach is to randomly sample a few lines, count the words on
those lines, and then extrapolate the total number of words.  I wrote
a program which randomly draws some lines and asks me for their
lengths, then calculates the average and multiplies by the total
number of lines (72 in this case):

    words on line 47: 6
    words on line 49: 14
    words on line 72: 2
    words on line 40: 11.5
    words on line 39: 12.5    
    average: 9.2 words per line, estimate 662 words total

    words on line 6: 15
    words on line 18: 13
    words on line 45: 11
    words on line 15: 9    
    words on line 57: 12  
    average: 12.0 words per line, estimate 864 words total

Those two samples of five lines gave a pretty wide spread, like a 3:4
ratio.  I tried a couple of samples of ten lines:

    words on line 35: 4
    words on line 25: 14
    words on line 69: 12
    words on line 9: 14
    words on line 43: 14
    words on line 55: 11
    words on line 11: 11
    words on line 40: 11.5
    words on line 63: 6
    words on line 29: 15
    average: 11.2 words per line, estimate 810 words total

    words on line 57: 12
    words on line 42: 16
    words on line 59: 9
    words on line 63: 6
    words on line 54: 12
    words on line 58: 14
    words on line 23: 18
    words on line 44: 10
    words on line 13: 12.5
    words on line 34: 12
    average: 12.2 words per line, estimate 875 words total

That's a narrower spread: ±8%.  But I can combine the four samples to
get a larger sample and get a better estimate:

    words on line 6: 15
    words on line 9: 14
    words on line 11: 11
    words on line 13: 12.5
    words on line 15: 9    
    words on line 18: 13
    words on line 23: 18
    words on line 25: 14
    words on line 29: 15
    words on line 34: 12
    words on line 35: 4
    words on line 39: 12.5    
    words on line 40: 11.5
    words on line 42: 16
    words on line 43: 14
    words on line 44: 10
    words on line 45: 11
    words on line 47: 6
    words on line 49: 14
    words on line 54: 12
    words on line 55: 11
    words on line 57: 12  
    words on line 58: 14
    words on line 59: 9
    words on line 63: 6
    words on line 69: 12
    words on line 72: 2

That works out to 15 + 14 + 11 + 12.5 + 9 + 13 + 18 + 14 + 15 + 12 +
4 + 12.5 + 11.5 + 16 + 14 + 10 + 11 + 6 + 14 + 12 + 11 + 12 + 14 + 9 +
6 + 12 + 2 = 310.5 words on 27 lines, which is 11.5 words per line, or
an estimate of 828 words.  If that's the real total, then the 810
number was 2.2% low, and the 875 number was 5.7% high, which seems
like a much more reasonable degree of error than the 20% low that I
got in the first sample of 5 (though the second sample of 5 was high
by only 4.3%).

I think those 27 lines are probably a reasonable approximation of the
overall (very skewed) line length distribution, so I ought to be able
to use them to simulate sampling different random numbers of lines
from that distribution to see how often the average is unreasonably
wrong.
