Emacs has a markdown-mode now.  It’s convenient in some ways, with
some very useful syntax highlighting, but flawed in others.  M-RET
adds a new list item.

It has org-mode-like folding if you press TAB on headers.

Interestingly, markdown-mode has a C-c C-x TAB for
markdown-toggle-inline-images.

I have some Markdown commands of my own, which I bind globally: C-M-_
makes the current line a Markdown setext-style second-level header by
underlining it with an appropriately-sized `----`, and C-M-= does the
same with `====` for a first-level header.  These are idempotent and
convergent: if an underline line is already present, it is removed.
M-\` makes the previous word `inline code` or, if it already is,
extends the markup to the next whitespace character to the left.  This
allows me to type text first and markup later.  I do the same with
M-\* for *asterisks*.

Fixing tab width
----------------

One of the annoying things is that this markdown-mode sets the tab
stops to 4 characters, which totally breaks pasting code into my
Markdown.  But I’m not sure how that is configured.  There are
variables called tab-stop-list and tab-width, and tab-width has a
buffer-local binding.  Aha, quite straightforwardly:

    (define-derived-mode markdown-mode text-mode "Markdown"
      "Major mode for editing Markdown files."
      ;; Natural Markdown tab width
      (setq tab-width 4)
      ;; Comments
      (setq-local comment-start "<!-- ") ...)

Well, that’s easy enough to hack around with an ugly hook in
`.emacs.d/init.el`:

    (require 'markdown-mode)
    (add-hook 'markdown-mode-hook 'fix-fucking-markdown-tab-width)
    (defun fix-fucking-markdown-tab-width ()
      (setq tab-width 8))

Better pre blocks
-----------------

Might be worthwhile to see if markdown-mode has something for
indenting code blocks that’s easier than C-x C-x C-u C-x i... it calls
them “pre blocks”.  Aha, markdown-insert-pre (C-c C-s p) and
markdown-pre-region (C-c C-s P), though they do the wrong thing if
it’s already pre: they indent everything but the first line further.
Also annoyingly C-c C-s C-p is unbound, and C-c C-s p doesn’t yank
when you have no region set, as I think it should.

I added this thing which is basically a keyboard macro to paste a pre
block from the kill-ring with C-S-y, which I think is a pretty
significant improvement over C-y C-x C-x C-c C-s p:

    (define-key markdown-mode-map (kbd "C-S-y") 'markdown-yank-pre-block)
    (defun markdown-yank-pre-block ()
      "Yank the current thing as an indented Markdown pre block."
      (interactive)
      (save-excursion
        (let ((start (point)))
          (yank)
          (let ((end (point)) (tab-width (default-value 'tab-width)))
            (untabify start end)
            (markdown-pre-region start end)))))

I think it's important to untabify text when you paste it into a
Markdown pre block because unless you render the text with 4-space
tabs it will display incorrectly.  And if, God forbid, your Markdown
processor dumps it into HTML with the tab characters intact, the
indentation could render as anything at all.  So this uses `let` to
ensure that `untabify` gets the standard value of tab-width, not
whatever braindamage markdown-mode imposes, before it indents the
region.

I think the greatest drawback of this command is that it doesn't
support M-y, yank-pop, to paste a non-latest kill-ring entry.
