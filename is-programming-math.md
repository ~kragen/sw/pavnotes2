Programming is in a sense a mathematical construct, same as
tic-tac-toe. Both activities take place in this abstract realm of
freely chosen axiomatic systems, divorced from physical reality, and
this is likely something that the humans have been doing since before
recorded history.

But, as Amber and I were talking about the other day, there's a much
more recent development, perhaps only 2500 years old, which we usually
require to call something "mathematics": rigorous proof, taking
advantage of the non-contingent nature of this abstract realm to make
general assertions that admit of no doubt (until you try to apply them
to the contingent universe, anyway). Sometimes we call this the
hypothetico-deductive method.

And you can program or play tic-tac-toe as much as you like without
ever using the hypothetico-deductive method, and therefore without
ever needing math in the sense we understand it today. People seem to
have built pyramids, balanced accounts, and computed calendars for
millennia before ever doing any math. Math is a very effective way to
do this kind of thing, but it is never required, any more than
bulldozers and backhoes are required to build a dam. You can do
everything with a shovel.
