The Maya tzolk’in date system is a residue number system in which 260
days are distinguished by a 20-day repeating cycle of day names,
analogous to the named days of the week, in parallel with a 13-day
count.  Today (02023 September 13) is 9 Etz’nab’ (perhaps “mirror”),
tomorrow is 10 Kawak, the following day is 11 Ajaw, and then we have
12 Imix, 13 Ik’, 1 Ak’b’al, and so on, until we get back to Etz’nab’,
but this time it’s 3 Etz’nab’.

Fliess’s cocaine-addled “biorhythm” system is another such system:
you’re born on the first day of your 23-day male or “physical” cycle
and the first day of your 28-day female or “emotional” cycle, and when
you’re 24 days old you’re on the 24th day of the female cycle but the
first day of the emotional cycle again, and so on, and the system
repeats after lcm(23, 28) = 644 days.  (He also proposed that each
rhythm has a waveform, perfectly sinusoidal with that period.)  Later
followers, perhaps less drugged than Fliess but no less lacking in
epistemic hygiene, added a 33-day “intellectual” cycle so it takes
21252 days to repeat.

Given that biorhythms are continuous-time, there was no need to use
commensurable intervals as their basis; any two incommensurable
intervals would have yielded a perfectly nonrepeating Lissajous,
mapping every point throughout infinite time to a nearly unique point
within the very finite unit square.  I think most sets of three
incommensurable intervals would yield perfect non-repetition within a
unit cube.

Returning to digital discrete systems, though, it occurred to me that
such a system with relatively large, relatively prime bases, encoded
as words, would be ideal for generating intelligence-community-style
codenames like FANCY BEAR, OLYMPUS FIRE, ONIONBREATH, Byzantine Hades,
FRIEZERAMP, and so on.  If the first few adjectives are “old”,
“local”, “system”, “great”, “small”, “mean”, and “social”, and the
first few nouns are “home”, “place”, “group”, “party”, “women”,
“point”, “men”, and “school”, then with two words, you start out with
“old home”, “local place”, “system group”, “great party”, “small
women”, “mean point”, and “social men”; after running through the
other adjectives and nouns, on the next go-round, you instead have
“old place”, “local group”, “great women”, “small point”, “social
school”; and on the following go-round, you have “old group”, “local
women”, “great point”, “small school”, etc.

This is enormously better than the place-value approach of “old home”,
“old place”, “old group”, etc., then continuing on to “local home”,
“local place”, etc.  You distribute the similarity much better over
time.

You could very easily make multi-word sequences this way, like Jitsi
room names: mature extremes telephone just, progressive harassments
document frequently, grateful well-beings evaluate notably, etc.  But
I think those names are probably too long to make good code names;
they’re worse than Byzantine Hades, which apparently everyone just
abbreviated BH.  You probably want to stick to about three syllables.
Moreover, for some purposes, it may be useful to avoid words that are
too evocative, like those chosen by What3words, [which offers place
names such as glorified.bodily.passage, beats.member.daily,
tugging.bunny.organ, inflamed.flesh.massaged, credit.card.denied,
shame.without.regret, forgotten.previous.husband,
swollen.member.blown, and former.decent.wiping][0].

[0]: https://news.ycombinator.com/item?id=11896883

Extremely common words like “small” (the #183 most common in the
British National Corpus) and “point” (#207) tend to be short and not
too evocative, but also quite polysemic.  What’s the best place in the
word list to look?

The most common 8 words are “the of and to a in it is”.  You could
conceivably use a “noun preposition noun” format (“place of men”,
“party in women”, “school to point”), but these don’t sound enough
like names, and your preposition list is going to be short, so eat up
a syllable for little return.  So these are probably not useful.

The next most common are “was that I for on you he be”.  These
similarly.

The 8 starting at item 32 are “or we an there her were do been”.  Has
anyone ever gone so far as her were do been?  Similarly.  We have to
go further out to find words in open classes.

At #64 we have “time my out like did only me your”.  Still too many
structure words.

At #128 we have “does made oh say going erm before”.  We’re starting
to get terms that could be used, but we’re also getting out of the
monosyllables.

At #256 we have “enough left development week form country face
almost”.  If we filter to only monosyllables, we have “left week form
face room tell six high”.  These are mostly nouns, but also mostly
verbs (in part because, though verbing weirds language, anything can
be a verb in English; *hlor u fang axaxaxas mlo*: upward, behind the
onstreaming, it mooned!)  But Left Week Form, Form Face Room, and Room
Six High are all perfectly reasonable names.

At #512 monosyllables are getting scarce, but we find “call true price
eight type wife seem close”.  Of these, the nouns are “call price type
wife close”, the verbs are “call true price type seem close”, and the
adjectives are “true eight close”.  I’d probably exclude “true” and
“wife” on grounds of emotional valence (do we want “False Wife Call”?)

At #1024 we find “arm win green d note Charles source rule post”,
which seems quite promising.  Green Arm Note or Source Rule Post seem
like quite usable names.

At #2048 we have “youth quick lots hole drugs sport desk noise”, which
seems potentially too memorable.  Let’s not call our project Youth
Hole Drugs, please.  Also it might be good to exclude inflected forms
like “drugs”.

At #4096 we have to search quite a bit to find monosyllables, but with
enough searching we get “pope mud Ross Moore ford Clarke shed shame”.
We’re clearly getting into words that carry a great deal of meaning
(out of 90.1 million words these each occur between 1897 and 1926
times, so with a zero-order Markov-chain model they carry about 15.5
bits) but they’re also all nouns.  But also mostly verbs and
adjectives.  Because they carry so much meaning they’re likely to be
emotionally rich rather than a blank slate we can project meaning
onto; a “Moore mud pope” or a “mud shame shed” is already a fairly
vivid image.

At #8192 we have “ram peered Poole gill stark priced cheer mug”, all
with 714-721 occurrences in the corpus, which is even more poetic.

At #16384 we have “quaint decks threes Stroud stacks snacks moat
hoarse”, all with 240-241 occurrences.  These are mostly inflected
forms of much more common words and, unless you’ve never been to
Stroud, also quite vivid.  (It wasn’t in my vocabulary.)

At #32768 we have “Pepys lifes hemp dill crotch coves coughs cloaked”.
Even more vivid, and again mostly inflected forms.  Except for
“crotch”, “dill”, and “hemp”, which are far too vivid to be a *tabula
rasa*.

At #65536 we have “Kuhl Kris Jons Hoss Hoffs hie Harte hap”, proper
nouns and archaic words.

(The whole word list only contains 109557 words.)

So somewhere around #256-#1024 is probably best.  Looking around #400
for non-inflected monosyllables, the first 31 are “south age start
sense run read sort third care try else free thus pay past ten shall
death love whose range play leave land course gone ask word street
turn trade”.  Picking just nouns without strong valence, we have
“start sense run sort third care try pay past ten range play leave
land course word street turn trade”, 19 words.  A simple strategy is
to use 5, 6, and 7 of these:

    | start | sense  | run    |
    | sort  | third  | care   |
    | try   | pay    | past   |
    | ten   | range  | play   |
    | leave | land   | course |
    | word  | street |        |
    | turn  |        |        |

This gives us the following list of code names:

- Start Sense Run (ok)
- Sort Third Care (no)
- Try Pay Past (no)
- Ten Range Play (ok)
- Leave Land Course (no)
- Word Street Run (ok)
- Turn Sense Care (no)
- Start Third Past (no)
- Sort Pay Play (no)
- Try Range Course (no)
- Ten Land Run (ok)
- Leave Street Care (no)
- Word Sense Past (no)

and so on.  This isn’t working well because these of the structural
ambiguity: most of these words, in addition to being nouns, are often
used as verbs.  “Leave street care” is much more plausibly interpreted
as an imperative (to leave the care of the street) than a noun phrase
(the care of the street where leave is taken).  Similarly with things
like “sort third course”.  This problem is mostly confined to the
first word, though; “Street care leave” or “Street leave care” is
fine.  “Third sort course” and “Course third sort” also seem okay.

So starting around #800, looking for non-inflected monosyllables that
are *plausibly* nouns, I find “hour wish risk mark fish park style cup
page shop hall rose bill oil claim blood film stand charge glass site
front”.  Of these, the less vivid ones that are rarely verbs are “hour
style cup hall film glass”, so perhaps these can be our first digit.
The second digit then might be “mark fish park page rose” and the
third “bill oil stand charge shop site front”.  This gives us:

- Hour Mark Bill
- Style Fish Oil
- Cup Park Stand
- Hall Page Charge
- Film Rose Shop
- Glass Mark Site
- Hour Fish Front
- Style Park Bill
- Cup Page Oil
- Hall Rose Stand
- Film Mark Charge
- Glass Fish Shop
- Hour Park Site
- Style Page Front
- Cup Rose Bill

These seem reasonably usable, except that “cup page oil” is maybe a
bit suggestive if you think about Senators who like to bend over their
pages, and probably “style” doesn't work very well as an adjective and
should therefore probably be in the last position.  There are 210
possible combinations.  I think it should be straightforward to
construct relatively prime sets of a few hundred words for each
position, particularly as they need not be disjoint (though perhaps
adjacent positions should be).

As with any residue number system, to recover the serial number from
such a code name (for example, for ordering comparisons), we can use
the Chinese Remainder Theorem.  “Style” is the second of a set of 6;
“page” the fourth of a set of five; “front” the seventh of a set of 7.
If we count from zero, these are 1, 3, 6.  In PARI/GP:

    ? style = Mod(1, 6); page = Mod(3, 5); front = Mod(6, 7);
    ? {[chinese(style, page), chinese(page, front), chinese(style, front), 
        chinese(style, chinese(front, page))]}
    %2 = [Mod(13, 30), Mod(13, 35), Mod(13, 42), Mod(13, 210)]

And indeed there are 13 items above “Style Page Front” in the above
list.  We get more interesting results for “Film Page Front”, which
doesn’t occur in the sequence until much later, when all the two-word
pairs have wrapped around:

    ? film = Mod(4, 6);
    ? {[chinese(film, page), chinese(film, front),
        chinese(film, chinese(page, front))]}
    %3 = [Mod(28, 30), Mod(34, 42), Mod(118, 210)]

