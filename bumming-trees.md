I was thinking about how to implement binary search trees efficiently,
and it occurred to me that you can probably get a speed advantage for
searches by sort of cdr-coding either the left or right spine from
each node.

Literally none of the code in this note has ever been tested, so it
might contain ridiculous mistakes.  In particular, I pretty often get
the sense of assembly-language conditional tests reversed.

Three-word nodes: 11½ cycles per level of the tree
--------------------------------------------------

First consider the conventional approach.  Suppose we’re on ARM and
our binary search tree node has three 32-bit fields: the key, the left
subtree, and the right subtree.  I think the code for the inner loop
for search looks something like this.  Our search key is in r0 and the
current tree node pointer is in r1.

    tsearch:    tst r1, r1
                beq notfound     @ null pointer means we’re out of tree
                ldr r2, [r1]     @ load key from current tree node
                cmp r1, r2       @ compare it to search key
                beq found
                bhi 1f           @ if high, we want the right subtree
                ldr r1, [r1, #4] @ if low, the left
                b tsearch
        1:      ldr r1, [r1, #8]
                b tsearch

I’m not sure this is correct but it’s probably about the size of it.
By rotating the loop we can eliminate one of the unconditional
branches:

        1:      ldr r1, [r1, #8] @ right subtree case moved to top
    tsearch:    tst r1, r1
                beq notfound     @ null pointer means we’re out of tree
                ldr r2, [r1]     @ load key from current tree node
                cmp r0, r2       @ compare it to search key
                beq found
                bhi 1b           @ if high, we want the right subtree
                ldr r1, [r1, #4] @ if low, the left
                b tsearch

I think these 9 instructions is probably about as good as it gets.
Let’s suppose we’re running this on a scalar in-order processor with
no branch prediction, which I think accounts for the vast majority of
ARM chips (which are embedded microcontrollers), and that we don’t
have to worry about cache misses.  Also, let’s assume that a taken
branch always costs 2 extra cycles, so 3 cycles in total, an
assumption that is close to the truth for ARM processors without
branch prediction, but never correct.

In the left-subtree case, we have 12 cycles per loop (8 instructions,
2 memory references, and one taken branch for 2 extra cycles), and in
the right-subtree case, 11 cycles, because it executes one instruction
less.  On average, then, we need 11½ CPU cycles per tree level.

Cdr-coding the left spine
-------------------------

Let’s replace the left-pointers with sequential cells in memory.  We
alternate between keys and right-subtree pointers; immediately
following the right-subtree pointer is the left subtree.  So our loop
now walks along this array of key/pointer pairs until it finds a key
that is less than or equal to the key we’re searching for.  To ensure
that this always happens, we store a key of 0 at the end of the array;
which is less than or equal to every possible search key (according to
unsigned comparisons such as `bhi`).  Here’s the idea expressed in C:

    typedef struct kvpair {
      unsigned key;
      struct kvpair *right;
    } kvpair;

    kvpair *tsearch(kvpair tree[], unsigned key)
    {
      for (;;) {
        if (!tree || (key == tree->key)) return tree;
        if (key < tree->key) tree++;
        else tree = tree->right;
      }
    }

What GCC comes up with for this is:

       0:   b138            cbz     r0, 12 <tsearch+0x12>
       2:   6803            ldr     r3, [r0, #0]
       4:   428b            cmp     r3, r1
       6:   d004            beq.n   12 <tsearch+0x12>
       8:   d901            bls.n   e <tsearch+0xe>
       a:   3008            adds    r0, #8
       c:   e7f9            b.n     2 <tsearch+0x2>
       e:   6840            ldr     r0, [r0, #4]
      10:   e7f6            b.n     0 <tsearch>
      12:   4770            bx      lr

Or, for 64-bit RISC-V, one instruction shorter but two more bytes:
                                                                    
                                                           # <------\
       0:   c911                    beqz    a0,14 <.L1>    # -------|---\
       2:   411c                    lw      a5,0(a0)       # <-\    |   |
       4:   00b78863                beq     a5,a1,14 <.L1> # --|----|---\
       8:   00f5f463                bgeu    a1,a5,10 <.L4> # --|--\ |   |
       c:   0541                    addi    a0,a0,16       #   |  | |   |
       e:   bfd5                    j       2 <.L5>        # --/  | |   |
      10:   6508                    ld      a0,8(a0)       # <----/ |   |
      12:   b7fd                    j       0 <tsearch>    # -------/   |
      14:   8082                    ret                    # <----------/
                                                           
Or, optimized for speed rather than size:

       0:   c911                    beqz    a0,14 <.L9>
       2:   411c                    lw      a5,0(a0)
       4:   00b78863                beq     a5,a1,14 <.L9>
       8:   00f5f763                bgeu    a1,a5,16 <.L5>
       c:   491c                    lw      a5,16(a0)
       e:   0541                    add     a0,a0,16
      10:   feb79ce3                bne     a5,a1,8 <.L16>
      14:   8082                    ret
      16:   6508                    ld      a0,8(a0)
      18:   b7e5                    j       0 <tsearch>

Written straightforwardly in ARM assembly, using different jump
destinations for different results rather than different return
values, it looks like this:

    tsearch:    tst r1, r1
                beq notfound
        1:      ldr r2, [r1]
                cmp r0, r2
                beq found
                bhi 1f
                add r1, #8
                b 1b
        1:      ldr r1, [r1, #4]  @ right subtree is at +4 now
                b tsearch

### ARM postincrement: 9½ cycles ###

To lump the incrementation of our array pointer into the key fetch
instruction, we can use a [postincrement][0] of `#8`.  This allows us
to reverse the sense of the test and eliminate the unconditional jump:

    tsearch:    tst r1, r1
                beq notfound
        1:      ldr r2, [r1], #8  @ point to following entry
                cmp r0, r2
                beq found
                blo 1b            @ if lower, continue inner loop
                ldr r1, [r1, #-4] @ negative offset
                b tsearch

[0]: https://www.labs.cs.uregina.ca/301/ARM-addressing/lecture.html

This way the left-subtree case is 4 instructions, a single memory
reference, and a taken jump, for a total of 7 cycles, while the
right-subtree case is all 8 instructions, two memory references, and a
taken jump, for 12 cycles, for a total of 9½ cycles on average, if we
assume both left and right branches are equally likely.

### Thumb-1: 10½ cycles ###

However, neither the post-increment nor the negative offset is
available in Thumb-1, so on Thumb-1 we actually have to do this:

    tsearch:    tst r1, r1
                beq notfound
        1:      ldr r2, [r1, #4]
                add r1, #8
                cmp r0, r2
                beq found
                blo 1b
                ldr r1, [r1]
                b tsearch

Now at label 1 our r1 pointer is confusingly pointing, not at the
actual key/pointer pair that we are about to examine, but 4 bytes
before it, possibly at a right-subtree pointer we might have
considered taking.  The left-subtree case is 5 instructions, 1 memory
reference, and 1 taken branch, for a total of 8 cycles, and the
right-subtree case is 9 instructions, 2 memory references, and 1 taken
branch, for a total of 13 cycles, 10½ cycles on average.

### Push down the equality: 9 cycles ###

Another small optimization is to move the equality case into one of
the subtrees:

    tsearch:    tst r1, r1  @ <---------\
                beq notfound            |
        1:      ldr r2, [r1], #8 @ <-\  |
                cmp r0, r2       @   |  |
                blo 1b           @ --/  |
                beq found               |
                ldr r1, [r1, #-4]       |
                b tsearch   @ ----------/

This shaves another cycle off the left-subtree case, getting it down
to 6 cycles (3 instructions, 1 memory reference, 1 taken branch) and
the average per tree node down to 8 cycles (the right-subtree case
still being 8 instructions, 2 memory references, 1 taken branch, for
12 cycles.)

### Interlude: B+-trees ###

This representation is reminiscent of a B+-tree, though I’ve never
before heard anyone suggest putting sentinel keys at the end of
B+-tree nodes to speed up a linear search over them.  If you add those
sentinels, and put the keys in backwards order, you could run this
exact search code on actual B+-trees.  Interchanging the left and
right subtree cases would allow you to do it without changing the
order, but the sentinel would change.

B+-trees are commonly used for external files rather than in-memory
data structures.

### Trusted memory-mapped files: 8½ cycles ###

In the case that the tree is contained in a memory-mapped file, with
the non-null pointers being relative to the beginning of the file, we
only need one additional instruction:

    etsearch:   tst r1, r1
                beq notfound
                add r1, r3        @ offset pointer by the file base address
        1:      ldr r2, [r1], #8
                cmp r0, r2
                blo 1b
                beq found
                ldr r1, [r1, #-4]
                b etsearch

Here the base address of the file is in r3, adding one cycle to the
right-subtree case, getting us back to 8½ cycles.

### Untrusted memory-mapped files: 11½ cycles ###

This presupposes that the file is as trustworthy as anything else in
our memory space, as if it were a dynamically linked library, because
corrupt file structures here can cause us to read arbitrary memory
addresses, possibly crashing the program or worse.  For external
suspicious files, we’d have to bounds-check each address; suppose the
upper end of the mapped file is passed in (the normally callee-saved)
r4:

    estsearch:  sub r4, #7         @ adjust end of file
        2:      tst r1, r1
                beq notfound
                add r1, r3
        1:      cmp r1, r4         @ check each pointer against end of file
                bhs outofbounds    @ if pair not inside file, crap out here
                ldr r2, [r1], #8
                cmp r0, r2
                blo 1b
                beq found
                ldr r1, [r1, #-4]
                b 2b

Now our left-subtree path is 5 instructions, 1 memory reference, and 1
taken branch, for a total of 8 cycles, and our right-subtree path is
11 instructions, 2 memory references, and 1 taken branch, for a total
of 15 cycles, or 11½ cycles on average.  There’s no way to check that
each array within the file is properly terminated, but because the
pointer check is inside the loop, you at least can’t run off the end
of the file.  An infinite loop of pointers within the file would be
easy to construct, though.

### Computing about the leafnode case less often: 8½ cycles ###

Something is bugging me, though.  Let’s go back to the fully-in-memory
case without any file base addresses or offsets, the “Push down the
equality” case above.  Couldn’t we use that `beq found` to get rid of
the extra unconditional jump instruction?  What if we `bne` or `bhi`
to the top of the loop instead?  Then we could move the `found` case
to the bottom, entirely outside the loops.  Maybe that would look like
this:

        2:      ldr r1, [r1, #-4]    @ right-subtree case: load pointer
    stsearch:   tst r1, r1           @ check pointer for null
                beq notfound
        1:      ldr r2, [r1], #8
                cmp r0, r2
                blo 1b
                bhi 2b
                b found

The left-subtree case is still 3 instructions (and a memory reference
and taken branch, for a total of 6 cycles) but this cuts the
right-subtree case down from 8 instructions to 7 (and two memory
references and a taken branch, for a total of 11 cycles).  This gives
us an average of 8½ cycles.  And it’s still only 8 instructions of
code.  If you could arrange for it to fall through into `found` it
would be only 7.

### Using cbz: 8 cycles ###

Jeez, I completely forgot the compare-and-branch-if-zero instruction!
Which gets it down to 7 instructions.

        2:      ldr r1, [r1, #-4]
    zsearch:    cbz r1, notfound
        1:      ldr r2, [r1], #8
                cmp r0, r2
                blo 1b
                bhi 2b
                b found

Here the left-subtree case is still 3 instructions, 1 memory access,
and 1 taken branch, so 6 cycles, but the right-subtree case has been
reduced to 6 instructions, 2 memory accesses, and 1 taken branch, so
10 cycles, and the average of 6 and 10 is 8.  It’s 20 bytes of code.

### 64-bit RISC-V ###

It’s also 7 instructions on RISC-V, though 28 bytes instead of 20:

        2:      ld a0, -8(a0)           # right-subtree case: load pointer
    zsearch:    beqz a0, notfound       # bail out if pointer is null
        1:      ld a5, (a0)             # load key from pointer
                addi a0, a0, 16         # increment pointer by two 8-bit units
                bltu a1, a5, 1b         # left-subtree case
                bltu a5, a1, 2b         # right-subtree case
                j found                 # found case

I probably want to write it for 32-bit RISC-V, but I’m not sure how to
persuade GCC to emit 32-bit code.  Also the disassembly of this looks
wrong, saying `bnez` instead of `beqz`, so I may be doing something
else wrong.

### On amd64 it’s 9 instructions and 32 bytes ###

I hope I got this right:

    0000000000000000 <stsearch-0x4>:
       0:   48 8b 40 fc             mov    -0x4(%rax),%rax

    0000000000000004 <stsearch>:
       4:   48 85 c0                test   %rax,%rax
       7:   0f 84 00 00 00 00       je     d <stsearch+0x9>     # notfound
       d:   48 8b 08                mov    (%rax),%rcx
      10:   48 8d 40 08             lea    0x8(%rax),%rax
      14:   48 39 c7                cmp    %rax,%rdi
      17:   72 f4                   jb     d <stsearch+0x9>
      19:   77 e5                   ja     0 <stsearch-0x4>
      1b:   e9 00 00 00 00          jmp    20 <stsearch+0x1c>   # found

Of course how fast it will run is anybody’s guess.  Presumably on a
fast processor with a cache in front of slow SDRAM, you would want to
bias the tree rather heavily to the left to minimize the number of
cache misses, at which point you would probably be better off doing
binary search within each array rather than sequential search.  But
sequential search has more predictable branches, which also might
matter.

Representing binary trees with run-time code generation: 5 cycles
-----------------------------------------------------------------

If we’re willing to trust the data file, though (perhaps because we’re
generating it at runtime!), let’s consider the case of
dynamically-linked libraries for real.  If we represent the tree as
executable code, and we only test 16 bits at a time, this can be much
faster.  Here are two consecutive tree nodes in this format:

    f643 02a3       movw    r2, #0x38a3
    4290            cmp     r0, r2
    d0fe            beq.n   0 <found>
    d804            bhi.n   2c                @ right subtree

    f242 0223       movw    r2, #0x2023
    4290            cmp     r0, r2
    d0fe            beq.n   0 <found>
    d800            bhi.n   2e                @ another right subtree

This only requires 4 cycles per left-subtree case or 6 cycles per
right-subtree case, almost twice as fast as the dumb-data approach
that doesn’t use runtime code generation.  And each node here only
requires 10 bytes, barely more than the 8 bytes required by the
dumb-data approach, though occasionally you’d need a node with a
larger jump offset, or a trampoline.  And probably you actually want
to store something in some other register to indicate *where* the
result was found, because in most cases you wanted to associate that
key with some value, a role which was played by r1 in the dumb-data
code.

You might protest that in this case the keys in the tree are only 16
bits wide.  While that’s true, it hardly costs anything extra to hang
another 16-bit-wide tree off each leafnode in the first tree if you
want to do a 32-bit search.
