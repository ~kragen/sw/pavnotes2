I was looking at Virgil Dupras’s CollapseOS again today,
because it is very relevant to my interests.
Unfortunately, my conclusion is that it’s cosplay, not
engineering.

On [the main page][0] it explains:

> [Winter is coming][1] and Collapse OS aims to soften
> the blow. It is a Forth ([why Forth?][2]) operating
> system and a collection of tools and documentation
> with a single purpose: preserve the ability to program
> microcontrollers through [civilizational collapse][3].

[0]: http://collapseos.org/
[1]: http://collapseos.org/why.html
[2]: http://collapseos.org/forth.html
[3]: http://collapseos.org/civ.html

I think this is a very useful goal; more generally I
think it is important to get the ability to program
microcontrollers independent of the existing industrial
infrastructure, because as Stuxnet and the current
ransomware pandemic demonstrate, that infrastructure
carries enormous risks.  And, even if global
civilizational collapse is unlikely, local
civilizational collapse typically happens more than once
a century; Ukraine, for example, is currently being
invaded by Russia, and shortages are consequently
rampant in both countries, to say nothing of the
apocalyptic conditions of living under bombardment or
military occupation.  And, even in milder disasters,
like the internet blackout being imposed by the
government in Bangladesh this month, it’s very valuable
to not be wholly dependent on the internet to keep your
devices running..

However, we start running into serious problems almost
immediately after that, as the slogan from a fantasy
novel and the use of Forth suggests we will:

> It is designed to:
> 
> 1. Run on minimal and improvised machines.
> 2. Interface through improvised means (serial, keyboard,
>    display).
>
> (...)
>
> Features:
>
> * Runs on Z80, 8086, 6809 and 6502 machines with very
>   little resources.
> * Can assemble Z80, AVR, 8086, 6809 and 6502 binaries.
> * Can disassemble 6502 and 6809.  Can program AVR
>   microcontrollers.

If your objective is “preserve the ability to program
microcontrollers through civilizational collapse” you
are never going to end up with the first step being
“Runs on Z80, 8086, 6809 and 6502 machines”!  There are
a few 8086 microcontrollers out there, and some variants
of the 6809, but virtually no Z80s or 6502s.  If you are
pawing through junkyards for microcontrollers you are
going to go a hell of a long time before you find a Z80.

Super common microcontrollers include ARM chips and 8051
variants.  These have been produced over the last 25
years in orders of magnitude greater quantity than Z80s
and 6502s ever were.

And “Interface through improvised means (serial,
keyboard, display)” is silly.  What’s going to be on the
other end of that serial port?  A teletype?  Where are
you going to get a teletype after eBay shuts down in the
collapse?  Because Z80s outnumber teletypes a thousand
to one in junkyards.  Using an ASCII text interface to
talk to your terminal over a serial port was useful in
the 01970s, when with sub-MIPS computers were scarce
relative to CRT controllers and teletypes.  It makes
sense today, or in a post-apocalyptic future, as a way
to use existing software.  It makes no sense in a world
where most USB-C power supplies include an ARM
microcontroller running at 10s of megahertz and the
junkyards are full of discarded cellphones.

This is nostalgic retrocomputing, not an engineering
effort to build a resilient computing infrastructure.

CollapseOS says it “can *program* AVR microcontrollers”
but what you’d need, if anything, is the ability to *run
on* AVR microcontrollers.  Because you *can* find those
in the junkyard, even if not nearly as commonly as ARMs.
And every electronics hobbyist has some Arduinos around.
You can program an AVR over SPI with debounced toggle
switches if you have to.

What’s crucial for the immediate decades after a
putative civilizational collapse, which is what CollapseOS
purports to be designed for, is not being able to run in
8K of memory on a processor capable of 0.3 MIPS, but
rather being able to figure out how to reprogram the
microcontroller in a wallwart or electric drill or hard
disk, which is probably a 32-bit chip with 100× as much
CPU power, 32K of Flash, and 4K of RAM, and generate a
usable NTSC or PAL or VGA signal to drive your salvaged
monitor.

As for storage, storage is ridiculously abundant; the
world is flooded with SD and MicroSD cards, and they all
speak SPI.

What’s abundant: Android phones, USB keyboards, TVs with
VGA inputs, TVs with composite video inputs, Ethernet
hubs, ARM microcontrollers.  What’s scarce: Z80s, 8086s,
6809s, 6502s, machines with 8K of memory.

Older microcontrollers and sometimes lower-end
microcontrollers are often programmed instead with mask
ROM, which makes them impossible to reprogram.

*Actual* original Intel 8051 chips have a pin on them
that forces them to run code from external memory, so
you don’t have to care about what their mask ROM says.
But original Intel 8051s haven’t been designed into new
products since the 01980s, and computer products in the
01980s were manufactured in tiny volumes compared to
anything in the 21st century.

I strongly sympathize with the desire to cosplay as Steve
Wozniak, but that isn’t going to help any more with
civilizational collapse than cosplaying as Rambo does.

[The Why page answers this criticism to some extent][5]:

> Why go as far as 8-bit machines? There are some 32-bit ARM
> chips around that are protoboard-friendly.
>
> First, because I think there are more scavenge-friendly
> 8-bit chips around than scavenge-friendly 16-bit or 32-bit
> chips.
>
> Second, because those chips will be easier to replicate in
> a post-collapse fab. The [Z80] has 9000 transistors. 9000!
> Compared to the millions we have in any modern CPU, that's
> nothing! If the first chips we're able to create
> post-collapse have a low transistor count, we might as
> well design a system that works well on simpler chips.

[5]: http://collapseos.org/why.html#morebits

It’s certainly true that, if you’re fabbing your own chips,
it’s easier to fab chips with less transistors than chips
with more transistors.  But we’ve learned things about CPU
design since the Z80 was designed.  We’d do well not to
forget them.  [In 01994 the 21-bit MuP21 was 7000
transistors, including video output][6], and the ARM 2 was
27000 transistors.

[6]: https://en.wikipedia.org/wiki/Transistor_count

But most of the rest of the CollapseOS notes are about
“scavenge-friendly electronics”, and for that you should
focus on what’s abundant in junkyards, not what would you
could fab in your garage.

[The particular predictions of civilizational
collapse][4] are also based on the specific prediction
that energy prices will go up because of fossil fuel
shortages.  this was a very reasonable prediction in
02000, and still a plausible one in 02010, though no
longer the most likely outcome.  But, when that was
written in 02020, photovoltaic modules were already
cheap enough that energy abundance became inevitable.
Most of those modules will survive for decades after any
potential collapse, making tens of watts easily
available unless you’re living underground like the mole
people.

[4]: http://collapseos.org/civ.html

On the other hand, if energy efficiency is your
objective, you definitely want to be using electronics
that are as recent as possible, not 6502s and Z80s or
8086s, which are by comparison ridiculously
power-hungry.  (The embedded Z80 variants in TI
calculators and the original Gameboy are inefficient,
but nowhere near as bad as a genuine Z80, which needs
close to 1000 milliwatts and doesn’t have a reasonable
deep-sleep mode.)  You can get 150–1500 picojoules per
instruction, very-low-power-consumption sleep modes, and
fast wakeup times out of most 21st-century
microcontrollers.  The Z80 uses closer to three million
picojoules per instruction.  (See file
`keyboard-powered-computers` in Dercuano.)

This metaphor from the civilizational-collapse page, of
a bingeing drug addict “looking for [their] next fix”,
is unrealistic:

> The underlying cause of this is that for 200 years,
> we’ve been on an energy binge.  We’re at the end of
> it, we’re looking for our next fix, but there isn’t
> going to be one.  We just need to drastically reduce
> our energy consumption.

However, it shows what I think is the real motivation
for Collapse OS.  It’s formulated in religious terms of
moral virtue and vice, not pragmatic engineering
concerns of efficiency and practicality.  It’s not an
engineering effort to foster resiliency, but an
essentially religious effort to atone for the sins of
the worldly through ascetic renunciation of pleasures
such as GUIs, returning to an imagined Arcadian past of
austere virtue.

I understand this point of view.  It resonates with me.
I’ve been vegetarian, spent time in ecovillages, mixed
adobe for construction, written my own bootstrapped
dialect of Forth, moved halfway around the world to
spend most of my adult life living as an illegal alien
in a third-world country, tutored kids in what
Brazilians call *favelas*, and so on.  Greed and
selfishness really do produce immense human suffering.

But, just as engineering is not a substitute for
renunciation, renunciation is not a substitute for
engineering.
