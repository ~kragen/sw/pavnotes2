As I was practicing my Chinese by reading the
datasheet for the Padauk PMS150C
microcontroller (part of the 3¢
microcontroller family, though I think the
PMS150C in particular is more like 5¢) it
occurred to me that the way we do watchdog
timers is probably overcomplicated, involving
as it does a number of flip-flops and digital
comparison.  Couldn’t we just time the
watchdog with an analog RC timer?  Like a 555;
you short the capacitor to ground to feed the
watchdog, and if it ever charges all the way
up to ⅔Vcc, an analog comparator (maybe just a
long-tailed pair) trips and generates a reset
pulse.

If you set up the initialization conditions
correctly, maybe the watchdog could also serve
as a power-on reset circuit.  Maybe you’d want
to generate a reset whenever the capacitor
voltage was *below* a certain level and short
it to Vcc to feed it.

On a chip, a difficulty with this is that the
available RC time constants are very small.
The PMS150C watchdog is settable, if I
understood the Chinese correctly, to timeouts
ranging from 125ms to 4 seconds.  But the
standard resistors in the Skywater 130 PDK
range from about 0.1Ω up to 2000Ω, occupying
area proportional to their resistance, and an
extremely large capacitor might be 0.5pF.  So
the highest RC time constant available is
about 1ns.  You could set the threshold
further than ⅔, but you’re still talking about
a few nanoseconds, which is far too short for
a watchdog.  So you’re back to dividing the
clock back down with a bunch of flip-flops.

(Camenzind says you can multiply capacitance
with the Miller effect for feedback
compensation capacitors, thus getting
capacitance 200× greater, but I don’t think you
can use Miller capacitance for RC timing.)

But I think you could probably use a current
source instead of a resistor to charge your
capacitor.

Camenzind mentions the “epi-pinch resistor” as
one simple possibility, which he says can
supply a few microamps in a small space.  A
microamp into a picofarad is still a volt per
millisecond, which is too fast for a watchdog
but much slower than the nanoseconds suggested
above.  And it’s subject to about a factor of
8 error.

We don’t need super precise timing for the
watchdog, but 8:1 variation from one chip to
the next seems like it might be a bit much.

More precise are current mirrors, and as
Camenzind explains in Chapter 3, you can
easily get an integral multiple of a reference
current by using multiple-emitter transistors
to carry more current at the same Vbe,
permitting the reduction of a reference
current by, say, a factor of 3 (and a few
additional percent base current error).  A few
stages of N:1 reduction permits a fairly
precise tiny charging current for our tiny
timing capacitor.

Camenzind also points out you can get rough
ratios in a current mirror by using MOSFETs
with different channel widths.
