It just occurred to me that deliquescent salts like [the muriate of
lime][1] have a special virtue for cement powders: the powder will
consolidate itself into a solid mass without any compression.  This is
usually a pain, because it means you have to diligently protect stored
product from exposure to air, but in some circumstances it could be a
great benefit.

[1]: https://www.saltwiki.net/index.php/Calcium_chloride

Consider, for example, a granulated mixture of calcium chloride,
monoammonium phosphate, and some inert fillers like halloysite, other
clays, talc, acicular wollastonite, acicular mullite, carbon fiber,
berlinite, etc., prepared in a dry atmosphere and carefully protected
from humid air.  The grains of different materials should pour like
sugar or sand, and so you should easily be able to get a thin layer on
a surface, or to fill an upward-facing crack or mold with them.

Over the next hours, days, or weeks, if the humidity is above the 30%
deliquescence relative humidity of muriate of lime, those grains will
become sticky and then wet, and their surface area in contact with the
monoammonium phosphate grains will begin to dissolve those grains.  At
the interface, gradually higher calcium phosphates will form, which
are fairly hard and quite water-insoluble; this will encrust the
grain, growing around it like a pearl.  Gradually diffusion across the
interface will slow down and effectively stop, both of phosphate ions
out of the grain and of calcium ions into the grain, but the newly
formed calcium phosphate will have cemented the monoammonium phosphate
grain to its neighboring grains of filler and growing monoammonium
phosphate, so you eventually have a porous apatite-cemented rock whose
pores are mostly filled with oil of lime.

In between, there should be a plastic stage during which the sticky
oil of lime holds the mass together even if you squish it, which
should allow you to mold it like clay.  Like a clay body, it will have
almost no elastic deformation, so you can shape it very precisely;
unlike a clay body, it does not need to be fired to cement it into a
solid mass, just left exposed to air (and perhaps soaked afterwards to
remove excess oil of lime), so it will not slump or contract in
firing.  This might allow more precise shaping than forming wet clay
can offer. However, the lubricated granular mass might still have slow
viscous flow.

Many of the hardest minerals that can be formed have strongly negative
enthalpies of formation, while their water-soluble precursors have
quite moderate ones, so in general these reactions can produce
dangerous amounts of heat.  In this form, the speed of the reaction
should be limited not only by the formation of the insoluble barrier
layers described above, but also by the moderate amount of reagents in
a liquid solution at any one time, and by the tendency of the porous
mass to evaporate the water from that liquid solution if it starts to
heat up, which will reduce the reaction rate again by greatly reducing
the diffusion speed of the ions.

Although I've suggested using acicular fillers above to minimize the
amount of cementation that is needed to produce a solid mass, it may
often be desirable for most of the mixture to be a fine aggregate such
as quartz sand or hydroxyapatite sand, in order to reduce both
porosity and cost.

In most cases the cement formed will consist of a polyvalent cation
and a polyvalent anion.  [Deliquescent reagents offering polyvalent
cations][2] include the chlorides of aluminum, calcium, trivalent
(ferric) iron, magnesium, divalent cobalt, divalent tin, and zinc, and
mixtures of these such as tachyhydrite (and here carnallite also
merits a mention, though it contains potassium); the nitrates of
aluminum and trivalent (ferric) iron; the iodide of calcium; the
sulfates of divalent manganese and divalent tin; and the [perchlorates
of calcium and magnesium][3].  Deliquescent reagents offering
polyvalent anions include phosphorus pentoxide, potassium carbonate,
and potassium pyrophosphate.  Hydroxides of polyvalent cations, such
as goethite and brucite, also tend to be water-insoluble, so it may
also be of interest that the oxides and hydroxides of potassium and
sodium are deliquescent.  Often, though, they are not very strong
physically.

Chlorides and perhaps perchlorates are probably not desirable in cases
where the finished cement should not corrode metals. However, a
particular case of interest can occur when the chloride is combined
with an ammonium compound, because the sal ammoniac thus formed can be
"sublimed" to remove it from the finished product at 337.6°, which is
a fairly moderate temperature.  For this purpose it is desirable for
the ammonium to be present in stoichiometric concentrations, which
suggests the use of (tri)ammonium phosphate or (di)ammonium carbonate
(salt of hartshorn, E503); unfortunately both of these will gradually
lose their ammonia at room temperature, producing more acidic salts
which will produce some muriatic acid upon double metathesis with a
chloride.  This could be considered either a nuisance because of the
noxious nature of the outgassing or a blessing because it will happen
fairly quickly even at room temperature.

In this connection, the sulfates and nitrates (and maybe calcium
iodide) may be more desirable deliquescent sources of polyvalent
cations for coating metal objects or for metal-reinforced cements.

[Carolyn Moorlag's master's thesis from 01997][6] is interesting
reading about bonding fillers with aluminum phosphates.

[6]: https://open.library.ubc.ca/media/stream/pdf/831/1.0078780/1

If the deliquescence proceeds from the cement's source of anions
instead, such as phosphorus pentoxide or lye, the source of polyvalent
cations can be just about any salt, though soluble salts will probably
work better. For example, granulated phosphorus pentoxide can be mixed
with granulated alum, or granulated lye with granulated epsom
salt. Epsom salt and alum are not deliquescent, but the water of
deliquescence from the other component should get them to react.

These are not the only possible cementation reactions; RCL
Jonckbloedt's dissertation reports, for example, that granulated
olivine reacts with hot sulfuric and hydrochloric acids over a few
hours to form amorphous silica which cements the olivine into a solid
mass unless the mixture is vigorously stirred, and of course the
hardening reactions of common cements like quicklime, portland cement,
and plaster of paris are not metathesis reactions like those described
above.  But they seem like particularly desirable reactions because in
many cases they produce quite strong ceramics.

[2]: https://en.wikipedia.org/wiki/Category:Deliquescent_substances
[3]: https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1002/2016GL068919 "Deliquescence-induced wetting and RSL-like darkening of a Mars analogue soil containing various perchlorate and chloride salts, by Jacob Heinz, Dirk Schulze-Makuch, and Samuel P. Kounaves, in Geophysical Research Letters, May 02016, DOI 10.1002/2016GL068919"

If the materials are instead ground to a fine powder, it can be
adhered to surfaces as a continuous coating using the usual methods,
such as dipping an object into a powder bed (fluidized or not),
pouring the powder over an object, or electrostatic spraying; and the
reaction should proceed much more rapidly because of the much greater
surface area of the particles.  By the same token, the fine powder
will be much more sensitive to brief accidental exposures to moist
air.

If, instead of premixing the granular solid reagents, we mix the
filler with the non-deliquescent powder, we can use them in a
powder-bed printing process in which the deliquescent reagent is added
selectively to each layer of the powder bed, like binder in a
binder-jetting process, the powder bed will support the geometry of
the 3-D printed object while it cures by absorbing moisture from the
air.  For efficient deposition, the deliquescent "binder" could be
added as a suspension in a liquid it's not soluble in; good candidates
for suspending these highly polar salts without solvating them might
include limonene, turpentine, eucalyptus essential oil, citronellol,
cumene, hexane, ether, gasoline, kerosene, naphtha (Zippo fuel),
toluene, xylene, dioxane, etc.

The deliquescent reagent would also work as a binder if selectively
deposited as an aqueous solution, but in that case the reaction will
be much faster, and the technique is more broadly applicable, because
the reagent need only be soluble rather than deliquescent.

Another possibly interesting way to apply this technique is to fill a
quilt with the granular mix, or to stick it between two sheets of
paper or cloth that are sewn together and perhaps coated with a
non-aqueous constant-tack ("pressure-sensitive") adhesive.  As long as
the powder remains in powder form inside the sheet, the sheet is
flexible and pliable; but once it is exposed to air, it will start to
harden, and eventually remain rock-solid in the form it was shaped to
when soft.

If you could arrange for the crystals grown during the cementation reaction to themselves be very acicular, you might be incorporate the reagents into 

[The Earth-crust abundance of the relevant elements][5] is:

* oxygen: 47%
* silicon: 28%
* aluminum: 8.2%
* iron: 5.0%
* calcium: 4.2%
* sodium: 2.4%
* magnesium: 2.3%
* potassium: 2.1%
* hydrogen: 0.14%
* phosphorus: 0.11%
* sulfur: 420ppm
* chlorine: 170ppm
* zinc: 75ppm
* nitrogen: 25ppm
* cobalt: 25ppm
* tin: 2.2ppm
* iodine: 0.45ppm

[5]: https://en.wikipedia.org/wiki/Abundance_of_elements_in_Earth%27s_crust

Interestingly missing from all of the above is titanium, which is
0.57% of the crust, more than phosphorus, sulfur, or zinc.  See file
`titanium-cements.md`.

Nitrogen, carbon, sulfur, and of course hydrogen and helium are much
more abundant in the solar system; they have been depleted from the
Earth's crust.
