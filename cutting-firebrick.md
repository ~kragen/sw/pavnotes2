It’s commonly pointed out that you can cut typical mostly-alumina
insulating firebrick with regular woodcutting tools like hand saws.
However, the woodcutting tools don’t last very long under this kind of
treatment; the alumina is of course mostly crystalline and quite hard,
so it rapidly erodes away the tools’ teeth.

Experimental archaeologists testing abrasive stone cutting in Egypt
found that, with the quartz sand used there, copper tube drills were
far superior to much harder bronze, which eroded away too rapidly
under the onslaught of the sand.  Local bamboo-like reeds also seemed
like they should work, but in fact didn’t work at all, perhaps because
they couldn’t exert enough pressure on the sand.  This possibly
explains why the advent of copper tools seems to be
near-contemporaneous with the advent of monumental stonework.

(Centuries later, the Egyptians learned of emery, which is alumina and
makes a much better abrasive, but the early work was all done with
quartz sand.)

Copper today has a reputation for being hard to cut in the machine
shop, because it is “gummy”, meaning among other things that it forms
long, stringy chips rather than breaking cleanly (though apparently
[permanent marker, glue stick, or packing tape can ameliorate
this][0].)  I suspect that this makes it more resistant to abrasion,
accounting for its better-than-bronze archaeological performance.  And
so I suspect it will work better than more-abradable metals in cutting
insulating firebrick.

[0]: https://www.engineering.com/story/the-secret-to-cutting-gummy-metals "Research at Purdue by Anirudh Udupa and Srinivasan Chandrasekar"

Tool steels are among the least gummy metals around, so I suspect that
the hardened-steel teeth of a typical wood-cutting saw are among the
most vulnerable.

Other [metals notorious for their gumminess][1] include low-carbon
steel, stainless, titanium, nickel alloys, chromium alloys, and
especially Hastelloy.  All of these are much harder than copper and
so, I suspect, will abrade even less.  I suspect that in general the
nearly-pure metals will work best, because they maintain the greatest
ductility, which I think is the quality of interest here.

[1]: https://www.harveyperformance.com/in-the-loupe/tips-for-machining-gummy-materials/

You could also try cutting the firebrick with something harder so the
alumina won’t abrade it, but that pretty much limits you to diamond,
carborundum, and borazon; I suspect this is a case where bending like
a reed works better than steadfastly withstanding the hurricane like
an oak.

A ball chain, like those used on dog tags and lamp pull chains, may be
an interesting alternative shape to a saw blade.  It can be fed
through a small hole and make cuts that rapidly change in direction,
like a jigsaw blade or abrasive wire saw blade, and a great deal of
material will need to be eroded off its surface before it starts to
lose its effectiveness.  Solid copper ball chain and stainless ball
chain are available today on the market.

For many purposes it would be ideal for your saw blade to expose new
saw teeth when the existing ones erode away, the way grinding wheels
expose fresh abrasive.  A metal sheet full of holes can perform this
way: when erosion reaches the edge of a hole, two new teeth are
formed.  Round holes would work, but triangular holes would probably
work better.  A bandsaw or reciprocating saw blade of this form would
probably work very well, as would a circular saw blade.

Such flat blades potentially offer greater cutting speeds than
something like a cable or chain because they have greater tensile
strength for a given kerf width.  Cutting speed per unit movement of
the saw blade tends to increase with pressure at the cutting face,
which also increases friction, which increases the tension on the
blade, which is ultimately a limiting factor.  With a given blade
material and aspect ratio, though, larger size provides greater
tensile strength per unit of cutting-face area, and so may also be
able to increase cutting speeds.

But even soft cotton string can provide enough pressure to cut through
many kinds of insulating firebrick.  And I think you could probably
anneal bands cut from a beer can to get nice tiny bandsaw blades.
