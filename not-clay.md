(Most of what I outline below is already well-known.)

Consider a dry powder concrete mix of:

1. Optionally, a skin-safe inert granular aggregate material such as
   100μm quartz, carborundum, silica fume, steel, styrofoam, cuprite,
   powdered rubber, graphite, sapphire, carbon black, ground perlite,
   sawdust, powdered polyester resin, coffee grounds, alabaster,
   tungsten carbide, powdered aluminum, chalk, grog, powdered epoxy
   resin, used yerba mate, magnesia-stabilized cubic zirconia,
   magnetite, titanium dioxide, or some mixture of these;
2. Optionally, a skin-safe acicular, platy, or fibrous aggregate such
   as wollastonite, finely chopped steel wool, mullite, finely chopped
   basalt fiber, finely ground mica, finely ground cellulose fiber,
   finely ground carbon fiber, finely ground carborundum fiber, or
   some mixture of these;
3. Optionally, a skin-safe soft hydrogel-forming ionomeric material
   such as gluten, starch, pectin, agar-agar, carboxymethylcellulose,
   sodium polyacrylate, polyvinyl acetate, polyvinyl alcohol,
   hydroxypropylmethylcellulose, carrageenan, or gelatin, or a plain
   thickener additive which increases the viscosity of aqueous
   solutions without forming gels, such as sucrose or muriate of lime,
   or some mixture of these;
4. A skin-safe high-strength slower-acting cement such as geopolymer
   mix (low-pH waterglass plus reactive aluminosilicates such as
   metakaolin), low-pH waterglass on its own, plaster of paris, or an
   acid-base phosphate cement; and
5. Optionally, a foaming mix which will slowly generate gas when
   hydrated and capture it in bubbles.

You ought to be able to formulate such a powder so that when you wet
it, you get a hand-moldable plastic mud or putty, which after a few
hours sets rock-hard without further intervention.  Spackle,
ceramic-tile adhesive, and drywall mud (“joint compound”) are existing
examples of such formulations, but many of them don’t have enough wet
strength to support detailed modeling, nor enough dry strength to
handle substantial loads.  I think it’s possible to do better.

Crafters have done a lot of experiments along these lines already, for
example using [toilet paper dispersed in water and PVA glue][0] to add
wet strength to a portland-cement mix.  (Recipe 1 from that video: 22g
dry toilet paper, dispersed in hot water, then compacted to 110g wet
pulp (½ cup), plus ½ cup of PVA glue, ½ cup of non-DAP-brand drywall
mud, and a cup of portland cement; recipe 2: ½ cup PVA glue, ½ cup
paper pulp, ⅔ cup portland cement.)  And of course adding plasticity
to clay bodies with carbohydrates goes back to [al-Razi][1] if not
earlier.

[0]: https://www.youtube.com/watch?v=8JzjIZVlSd8
[1]: https://www.youtube.com/watch?v=HbLdNKM9y68

Ideally, the inert granular aggregate (ingredient #1) is the vast
majority, and mostly determines the dry strength, color, stiffness,
density, surface texture, material cost, conductivity, and other
properties of the final article.  In practice, the cement will usually
occupy a significant fraction of the volume, reduce the stiffness
significantly, and add a significant amount of cost.  (Sometimes this
reduced stiffness may be a desirable feature.)  Powdered aluminum is
used in duct tape to, I think, reduce gas permeability and increase
opacity and reflectivity.  This inert granular aggregate will also
tend to reduce the springback of the wet putty, and they can make it
more thixotropic if the grains undergo a jamming transition when at
rest.

Grog is a hybrid: it contains metakaolin, one of the ingredients for
geopolymer cement, as well as more inert ingredients like quartz.

If the granular aggregate particles are of a wider range of sizes,
they can occupy a larger volume fraction of the finer product, which
will usually tend to increase strength and stiffness and reduce
porosity.

Particles of lightweight, porous granular aggregate such as ground
perlite or styrofoam can reduce the density and rigidity of the final
product and increase its porosity, when that is desirable.

The other aggregate (ingredient #2) should increase wet strength,
providing greater modeling freedom, but also may increase dry
strength and toughness.

In some cases surface treatment of either aggregate will affect the
final strength.  For example, corona treatment, deposition of a thin
layer of carbon black, treatment with a hydrophilic silane, or
treatment with a strong acid or base will either increase or decrease
adhesion between the aggregate particles and the cement matrix.
pIncreasing adhesion will increase the strength of the final concrete.
Decreasing adhesion with the non-granular aggregate, as is done in
ceramic–ceramic composites, may increase fracture toughness by
improving the ability of acicular or platy aggregate to bridge cracks
through the cement.

The hydrogel-forming material or other thickener (ingredient #3)
provides most of the wet strength, holding the putty in place while
it’s being shaped and until the slower-acting cement hardens.  Some
cements, especially those including waterglass, are adequate wet gel
formers on their own.  Many possible cements will tend to increase or
decrease the pH of the wet mix, which will increase the gel-forming
tendency of some of these ionomers and decrease that of others.  (For
example, geopolymer mixes will tend to increase the pH, while
acid-base phosphate cements will tend to decrease it, because the
phosphate is more soluble than the source of cations.)

The thickener, if not destroyed by the cement, remains in the final
material and can contribute qualities such as waterproofness or
reactivity.  For example, at a high enough temperature, carbohydrate
thickeners can react with oxygen provided by an aggregate such as
cuprite to produce gas, or heat can dehydrate them to carbon if the
resulting water vapor can escape the article.  Even without a cement,
heat can reduce gluten to 

The cement (ingredient #4) transfers load between the particles of
aggregate, if present, and typically limits the strength and
dimensional stability of the final article.  In many cases it also
limits the service temperature of the article; hydrated alabaster in
particular begins to dehydrate back to the hemihydrate under 200°,
which can cause it to crumble.  Under some circumstances this can
provide the valuable function of recycling the article back into
powder.

Usually a geopolymer mix is done with sodium silicate as the
waterglass, but sodium silicate is difficult to redissolve into water
if it solidifies.  (Maybe powdering it very finely, perhaps by heating
it until it foams and then grinding up the foam, would help with
this).  Potassium silicate should be almost as good, and I believe it
has the benefit of being compatible with powdering.  Geopolymer mixes
are reported to have pot lives measured in hours.

The foaming mix (ingredient #5), if present, serves two important
purposes.  First, it can cause the mix to expand to fill up gaps,
which is useful in some kinds of molding and gluing applications.
Second, it can produce a high-porosity final article, which for some
applications has important advantages:

1. Much lower thermal conductivity.
2. Lower cost per unit volume.
3. Greater resistance to crack propagation and therefore,
   surprisingly, higher toughness.
4. Greater rigidity per unit mass, as in sandwich panels.
5. Lower hardness and therefore greater ease of cutting and abrading.

As an alternative to including a gas generator in the foaming mix, you
can wet it with dilute hydrogen peroxide instead of water to produce
the gas, including only a surfactant in the mix to stabilize the
resulting bubbles.  Almost any candidate cement will contain enough
ions that catalyze the decomposition of H₂O₂, but if not, you can add
a small amount of any of many different catalysts.

FDM
---

Rather than shaping the putty by hand, you can extrude it through a
syringe, which allows you to “3-D print” various shapes.  For this
kind of thing, superplasticizers (as commonly added to portland-cement
concrete) may be helpful.

Tape
----

Another alternative to shaping by hand or by FDM, commonly used for
making casts for setting broken bones, is to stick the powder to a
gauze, scrim, or paper with a non-aqueous adhesive like those commonly
used for “pressure-sensitive” tape.  The granular powder will cover
the adhesive, allowing the tape to be coiled into a roll without a
backing layer and without sticking to itself.  To use the tape to
build up a shape or to stick two things together, you must add water
to wet it, either before or after application.  Applying the water
after application affords less opportunity for the water to wash away
some of the particles.

This approach offers four major advantages for certain applications:

1. The cloth functions as a sort of armature, holding the paste in
   place until it hardens, permitting the fabrication of much thinner
   sections of material than the wet strength of the paste alone could
   sustain.  If it includes plastic elements such as annealed steel
   wire, it may be capable of great flexibility of form.
2. The tape allows easy manual deposition of a single layer of
   consistent thickness over a large area, which is useful for
   sticking two objects together.
3. Once the paste has hardened, the cloth is a continuous, anisotropic
   fibrous reinforcement running through the finished article.  If the
   cloth incorporates high-strength materials such as carbon fiber,
   glass fiber, carborundum fiber, or basalt fiber, this can add
   enormous strength.  In so-called unidirectional tape, most of the
   fibers run along the length of the tape, so most of the strength is
   in that direction.  (This approach also permits skin-friendly use
   of fibers like fiberglass that would be skin-unfriendly if they
   were just mixed into the powder.)
4. The cloth tape can incorporate large holes, providing a sort of
   prefab honeycomb or truss structure and achieving even higher
   stiffness-to-weight ratios.

A slight variant, commonly used for making piñatas with wheat paste
and paper, is to dip the cloth into a thin aqueous suspension of the
powder to coat it with the slowly hardening mix, then use it
immediately.
