I was thinking that you could do all kinds of neat bootstrapping
tricks with programming that aren't more generally applicable: give
the bot an IDE.

One step of bootstrapping: train the NN to write code that compiles
(unlike, say, <http://sprunge.us/gTdlxE>; assembly is my example
because that was the most recent time I saw GPT-4 generate
syntactically invalid code; it's a lot better in JS and C).

Second step of bootstrapping: train the NN to predict whether code
will compile or not.

GAN step: train the NN to write code that the other NN is confident
will compile.  (Note that this is backward from a normal GAN, where
the adversary tries to *trick* the evaluator.)  The second NN is
intended to guide this third NN to write code that doesn't just barely
manage to compile, but instead is obviously compilable, to keep the
third NN from wasting all its time randomly changing programs to try
to get them to compile.

One step redux: train the NN to write code that passes a test suite.

Second step: train the NN to write a test suite.

Third step: train the NN to evaluate how good a test suite is relative
to an informal spec and how it could be improved ("constitutional
AI").

Summarization step: train the NN to summarize a code base.

Bottom-up summarization step: train the NN to summarize a summary of code.

Top-down summarization step: train the NN to predict the more-detailed
summaries the bottom-up summarization NN was working from, thus
decomposing a system design into modules.

Proof assistant step: train an NN to produce a proof of correctness of
code in Coq.

Proof assistant tactic step: train an NN to predict which proof
tactics in a proof assistant will result in writing a successful
proof.

Hypothesis step: train an NN to produce crashing or otherwise failing
test cases for a program.
