I have set up two fans a meter inside the main house doorway to
circulate cool air through the house.  The incoming air is 19°, the
air it’s replacing is about 30° (because yesterday was hot), the
doorway is about 1 m × 2 m, and the airstream in the middle averages
roughly 200mm/s, according to bits of aluminum foil dropped from
different heights in the center of the doorway.  This probably means
that the overall airstream averages about 100mm/s, since at the walls
there is no motion.  This works out to about 0.2m³/s of airflow (420
“cfm”), and thus about 240g/s.  At ΔT = 11K and 1J/g/K, this is about
2600 watts of cooling.

The two fans I’m using are each 350mm in diameter and blowing about
2m/s, which works out to about 0.2m³/s per fan.  Presumably the reason
for the overall flow rate being smaller than this (smaller than half
of this, in fact) is that much of the air they are sucking in is being
sucked in from inside the house.  This makes me think I can roughly
triple their efficiency if I can put them outside the house instead of
inside.

Trying that increases the apparent wind speed in the middle of the
front hallway to maybe 300mm/s, but, because I’m taking the
measurements now in turbulent air rather than laminar air, the
measurements are all over the place; some bits of garlic skin or
aluminum foil fall straight down to the floor even from head height.
And I still don’t have a way to measure the temperature change over
time in the house.
