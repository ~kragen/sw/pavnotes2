I was thinking about rapidly exchanging humidity between air and
muriate of lime (or magnesia).  One difficulty is removing the heat
generated when the humidity is taken up by the desiccant solution, but
I think this can be handled by recirculating the same air.  A
different difficulty is the diffusion of the water into the desiccant
itself; a Monster can full of solid muriate took some six months to
finish deliquescing in my living room.

It occurred to me that in conventional swamp coolers we accelerate
humidity exchange between air and water by pumping the water over
excelsior pads.  We could likewise soak excelsior, or paper from a
non-crosscut shredder, with the muriate solution (perhaps with the aid
of vacuum, since the muriate solution isn't very good at wetting
hydrophilic things like cardboard).  The muriate solution is syrupy,
and I propose regenerating it in place with hot, dry air rather than
replacing it with fresh desiccant, so I think it can maybe just be
left in place on the excelsior.

Supposing we do this, how fast can we cycle between absorbing water
and regenerating it?  The faster we can cycle, the more water we can
remove for a given mass or volume of desiccant.  But the water needs
time to diffuse from the surface of the desiccant into its core.

We can get a brutally oversimplified estimate by observing that such
diffusion processes tend to take time proportional to the square of
the depth to which they penetrate, because as the diffusion zone
penetrates deeper, the gradient driving the diffusion process gets
proportionally shallower.  The observed six months to diffuse 100mm
gives the totally irresponsible estimate of 2Gs/m² for the relevant
coefficient.  Scaling this coefficient by the square of 100μm, which
is probably about the right order of magnitude for the half-thickness
of excelsior, we get about 20 seconds.

So the excelsior approach ought to be able to saturate our desiccant
with humidity from the air in about 20 seconds at room temperature, at
least if we have a way to remove the heat produced (I favor rapidly
cycling the air through an evaporatively-cooled heat exchanger).
Removing the water with hot air ought to be a bit faster, say 10
seconds.  So we should be able to cycle water through the system about
100 times an hour.  And these desiccants can absorb roughly their own
weight in water, so each kilogram of desiccant should be able to
remove about 100 kg of water per hour.

Looking at [UIG’s copy of the Linric Company psychrometric chart][0]
we see that at 30° air can hold about 27½g of water per kg, normally
requiring 100kJ/kg air to remove, though only 62kJ/kg of that is the
heat of vaporization of the water.  This works out to 3.6 kJ per gram
of water.  If we assume we need 4kJ per gram of water, then 100kg
water/hour/kg desiccant works out to 100kW per kg of desiccant.  So a
100-watt backpack-sized air conditioning unit might require only a
gram of desiccant, and a 4kW small-house-sized unit might require 40
grams.  This seems quite economical.  I’m quite pleased with this
back-of-the-envelope estimation exercise.

[0]: http://www.uigi.com/UIGI_SI.PDF

That’s a vaguely estimated power *consumption* at a coefficient of
performance below 1, though.  Only 62kJ/kg represents actual delivered
cooling performance, maybe 3kW per kg of desiccant.
