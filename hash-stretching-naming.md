Zooko's Triangle proposed that names, for example for public keys,
network addresses, or documents, can be any two of human-readable,
secure, and decentralized.

Human-readable names can be securely assigned by a centralized
authority like DNS; when you want to know what document is denoted by
"US Constitution" or what IP address is denoted by "google.com", you
send a message to the centralized authority and trust the reply.

Human-readable names can be insecurely assigned in a decentralized
fashion by just treating any random person as an authority.  You can
ask several people and see if their answers agree, but if you are
subject to a Sybil attack, they might all happen to be the same
person who is trying to fool you.

Decentralized names can be secure if they are assigned by a secure
hash function.  For example,
33d5b0600eed937022386d3d1afd2316f1fa219bd0e3ce723c8b1fb49352498f is
the SHA-256 of the King James Version of the Bible; if any random
person gives you a file purporting to be that file, you can calculate
its SHA-256, and if it matches, you know you have the right file,
because although an infinite number of other files would hash to the
same value, we conjecture with some confidence that finding one is
computationally infeasible.  But it is not very human-readable.

One solution to Zooko's Triangle is Nakamoto consensus: a
decentralized system in which proof of work by hashing adds to a chain
of blocks, each containing some new name bindings.  This has certain
disadvantages but does seem to work.

Another attack on the problem is the S/Key or
correct-horse-battery-staple approach: represent a secure hash in base
2048 or 4096 using words as the digits.  In the S/Key wordlist, the
above SHA-256 is represented in base 2048 as "ABE INTO CRAG MUDD TOWN
OTTO MURK YES CUFF TEET MOSS WRY DANG WOLF FRET EDDY STEM DINT GOSH
GAS WHOM AHOY AFAR PAN".  I have a 4096-entry wordlist I like better
which instead represents it as "to th mare he inert rare title gp glad
crap task gods plank board sub reply ace strap gives vary topic
cheer".

These names are too long to be very human-readable.  Memorizing one of
them would be a significant task, and noticing a small discrepancy
like "ABE INTO CRAG MUDD TOWN OTTO MURK YES CUFF TEET MOSS WRY DINT
WOLF FRET EDDY STEM DANG GOSH GAS WHOM AHOY AFAR PAN" can be
difficult.  If we truncate them, we lose computational infeasibility;
"ABE INTO CRAG MUDD TOWN OTTO" is more human-readable, but it only
represents 72 bits, so finding a second preimage for it through brute
force will take on average 2⁷² tries.  At 100 million tries per second
per machine, that's only about 1.5 million machine-years, so an
attacker with a million machines could find a second preimage in only
a year and a half.

The Antminer S19 Pro (US$3700) benchmarks at 110 terahashes per
second, so one S19 Pro could compute those hashes in 16 months, except
that as I understand it it's computing double SHA-256s, and I don't
think you could apply it to this task anyway.  But you could build
slightly different hardware that would work.

Today the Bitcoin hashrate is 254 exahashes per second, which would be
sufficient to find a second preimage for a 72-bit hash like that every
18.6 seconds, if they were the right kind of hash.

(In some contexts it's important to resist collision attacks as well
as second-preimage attacks, which requires twice as many bits of hash.
The standard example of this is signing a contract: if Mallet gives
Alice a contract to sign cryptographically in exchange for some
payment, Alice faces the risk that Mallet has generated two versions
of the contract with the same hash, giving her the innocent one to
sign.  Then Mallet can use Alice's signature on the malicious
contract.  I will assume that collision attacks are not a threat in
this context.)

Suppose that we are willing to expend some extra resources to creating
such human-readable, secure, decentralized names.  For example, this
13-year-old laptop can do 2 million SHA-256s on each of its two cores,
according to `openssl speed sha256`.  If I were to set it to hashing
for 24 hours, it would do about 350 billion hashes, a bit over 2³⁸.
(As with Bitcoin, we can include a nonce in the string being hashed
which we vary to try to find the right hash.)  On average, about eight
of the hashes it computed would have 35 trailing zero bits; about four
of those would have 36; about two of those would have 37; and about
one would have 38.

What if we use the hash with 38 trailing zero bits for the name?  We
can either fix the number 38 in the design or include it in the name.
My laptop can find one in 24 hours, while a machine of the class of
the Antminer S19 Pro would require 2.5 milliseconds.  Then all we need
is enough actual name bits to give us the second-preimage resistance
we want: 66, say, six S/Key words.  Finding a second preimage with 38
trailing zero bits would require calculating on average 2⁽³⁸⁺⁶⁶⁾ =
2¹⁰⁴ SHA-256 hashes.

Such names might take forms like these, depending on how we represent
them:

- VEDA.CRAY.HICK.LIMB.LONG.OF/38
- then:blitz:ball:erika:trial:girl
- of/schlegel/clutter/resents/assimilate
- fugujo-sinefu-janifi-rijuni
- 佰婪抑辳茎
- this tara moody tonic tutor firm 38

These seem reasonably human-readable to me, even if they aren't in the
same league as "yahoo.com".

This is probably a safe design right now, but not a very conservative
one.  Today it would take a billion machines of the class of the
Antminer S19 Pro almost six years to find a second preimage to such a
hash.  They would cost somewhere over US$3 trillion to build (a bit
more than the yearly exports of the People's Republic of China) and
weigh 13 million tonnes.  This is clearly within the capabilities of
the human race even today, although it would be a project similar in
scale to World War II.  But this machine is still several orders of
magnitude away from the ultimate limits of computation, even using
everyday atomic matter, and the human industrial economy is many
orders of magnitude away from the ultimate limits of manufacturing
capacity.

I don't know what the transistor count of the Antminer chips is, but
they're probably within an order of magnitude of the Apple M1 Ultra,
which is a dual-chip module with 20 cores and 114 billion transistors
in 840.5 mm² of silicon area fabricated in TSMC's 5-nanometer process.
(Flash chips get much higher areal densities by stacking the
transistors hundreds of layers deep.)  If we assume the chip is 100
microns thick, like many current solar-cell chips, that's 1.4
quintillion transistors per cubic meter, used basically as switches.
That gives us a volume of 7.4e-19 m³ per transistor, a little under a
cubic micron.

But the nearest-neighbor distance in silicon's crystal structure is
0.235 nm, so this is roughly 4000 atoms across, somewhere on the order
of 64 billion atoms per transistor.

So even without switching from atomic matter to some kind of
degenerate matter, transistors can be about eleven orders of magnitude
smaller, or 36 orders of binary magnitude.  They can probably run
faster than the few GHz we're using, too.  And, even without leaving
the Solar System, you can plausibly convert Jupiter into computronium;
if converted to silicon atoms, this would be about 4e52 silicon atoms,
about 2¹³⁸ Apple M1 Ultras' worth of transistors at one transistor per
atom: 36 orders of binary magnitude smaller than the M1 (implicitly
estimated here at 400mg), plus 102 orders of binary magnitude more
mass.

So, to be secure against a Kardashev Type 2 civilization, you might
want 104+138 = 242 bits of security, if they're stuck at clock rates
of a few GHz.  And you might want security against quantum computers,
too, which I think doubles the number of bits of hash you need against
second preimage attacks: 484 bits.  And, even if you can't get a
speedup by building the machines out of indium phosphide or diamondoid
reversible Merkle bucking-spring gates or something instead of
MOSFETs, probably you *can* run the computers faster if you build them
out of degenerate matter on the surface of a neutron star.

But for a few years at least you ought to be able to use things like
then:blitz:ball:erika:trial:girl as human-readable, secure,
decentralized names, without relying on the blockchain to reach a
social consensus on their meaning.
