Checking prices 02022-10-23.  The dollar is at AR$289.

First, we checked online.

Rice at Día is $117/kg.  Lentils $400/kg at the local supermarket, at
Día $379/kg.  Flour at Día is $123/kg.  Instant polenta $212/kg.
Non-instant polenta $174/kg.  Oil is cheaper if you buy it from the
oil store, but sunflower oil at Día is $261/ℓ.  At Día, Matarazzo
brand elbow macaroni $364/kg, or Día brand $240/kg.  At Carrefour,
Matarazzo brand “municiones” pasta is $390/kg.  At Carrefour,
Arcor-brand “ave maria” pasta (similar to “municiones”), $268/kg.  At
Coto, Ramoneda-brand garbanzos, $351/kg; Coto-brand garbanzos
$124/400g with a 2-for-1 offer, which works out to $310/kg.  At Coto,
dry non-split peas Egran, $397/kg.  Split peas, $259/kg.

Then, I went to Chen Ruiyun’s neighborhood grocery store.

    | Store       | Brand      | Food                 |  AR$ | perunit |   US$ |
    |-------------+------------+----------------------+------+---------+-------|
    | Día         |            | Rice                 |  117 | kg      |  0.40 |
    | "           |            | Lentils              |  379 | "       |  1.31 |
    | Chino       |            | "                    |  400 | "       |  1.38 |
    | Día         |            | Instant polenta      |  212 | "       |  0.73 |
    | "           |            | Regular polenta      |  174 | "       |  0.60 |
    | "           |            | Flour                |  123 | "       |  0.43 |
    | "           |            | Sunflower oil        |  261 | ℓ       |  0.90 |
    | Día         | Matarazzo  | Elbow macaroni       |  364 | kg      |  1.26 |
    | "           | Día        | "                    |  240 | "       |  0.83 |
    | Carrefour   | Matarazzo  | "Municiones" pasta   |  390 | "       |  1.35 |
    | "           | Arcor      | "Ave María" pasta    |  268 | "       |  0.93 |
    | Coto        | Ramoneda   | Garbanzos            |  351 | "       |  1.21 |
    | "           | Coto       | "                    |  310 | "       |  1.07 |
    | "           | Egran      | Whole dry peas       |  397 | "       |  1.37 |
    | "           | ?          | Split peas           |  259 | "       |  0.90 |
    | Chen Ruiyun | La Tikay   | Sugar                |  260 | "       |  0.90 |
    | "           | Lucchetti  | "Municiones" pasta   |  300 | kg      |  1.04 |
    | "           | Favorita   | 000 white flour      |  150 | kg      |  0.52 |
    | "           | Purocol    | Alcohol              |  440 | ℓ       |  1.52 |
    | "           | Dos Anclas | Salt                 |  220 | kg      |  0.76 |
    | "           | Nestle     | Condensed milk       | 1270 | kg      |  4.39 |
    | "           | ?          | Peanuts in shell     |  130 | 201 g   |  0.45 |
    | "           | La Serenis | Dried nonfat milk    | 1750 | kg      |  6.06 |
    | "           | RAD        | RAD-60 energy drink  |  320 | 500 mℓ  |  1.11 |
    | "           | Taragüi    | Yerba mate (BCP)     |  800 | kg      |  2.77 |
    | "           | Alicante   | Baking soda          | 2000 | kg      |  6.92 |
    | "           | ?          | Dark chocolate chips |  150 | 103.61g |  0.52 |
    | "           | Arlistan   | Instant coffee       | 5200 | kg      | 17.99 |
    | "           | Savora     | Pseudo-mustard       |  640 | kg      |  2.21 |
    | "           | Quaker     | Oatmeal              |  892 | kg      |  3.09 |
    | "           | Cocinero   | Sunflower oil        |  444 | ℓ       |  1.54 |
    | "           | Dino       | Seasoned salt        |  540 | kg      |  1.87 |
    | "           | Frutigran  | Chocolate chip cooky | 1280 | kg      |  4.43 |
    |             |            | Raisins              |      |         |  0.00 |
    |             |            | Flax seeds           |      |         |  0.00 |
    |             |            | Poppy seeds          |      |         |  0.00 |
    |             |            | Dried onions         |      |         |  0.00 |
    |             |            | Dried corn           |      |         |  0.00 |
    |             |            | Chia seeds           |      |         |  0.00 |
    |             |            | Prunes               |      |         |  0.00 |
    |             |            | Cocoa                |      |         |  0.00 |
    |             |            | Sesame seeds         |      |         |  0.00 |
    |             |            | Sunflower seeds      |      |         |  0.00 |
    #+TBLFM: $6 = $4 / 289; %.2f

The peanuts in the shell may be more easily stored than shelled
peanuts, but there’s definitely a price conversion factor, and they
also take up much more space.  These are roasted, which presumably
adds to their price, but unsalted.  12½ of them weighed 24.47 g.  The
plate with the shelled nuts weighed 21.10 g; empty it weighed 3.08 g;
with only the shells it weighed 9.48 g; empty again it weighed 3.10 g.
This gives a weight of 6.38 g of shells and 18.02 g of nuts, which sum
to 24.40 g, indicating measurement error on the order of 50 mg.
18.05/24.44 = 74% nut, leaving 26% shell.

So roasted nuts in the shell are AR$130/201g = 45¢/201g = US$2.2/kg
(!) = AR$650/kg, netting you, for roasted nuts without shells,
AR$870/kg = US$3.0/kg.  This is not a good price for peanuts, but
perhaps buying in bulk (or unroasted) would yield much better prices.

The dark chocoolate chips are semi-bitter and would be excellent for
chocolate chip cookies.  They cost AR$1450/kg = US$5.00/kg, which I
think is maybe a bit high.  I bought them by accident thinking them
raisins.

I bought 200 grams of dried nonfat milk at the price listed above; the
package says it makes 2 liters.

The Quaker oatmeal is the non-instant type, bought in a box.
Argentine oatmeal commonly comes with unremoved grain hulls mixed in
with the oatmeal, which taste and feel in the mouth much like toenail
clippings; I suppose the oatmeal is meant for horses rather than
people, even though it is rolled.  We will see if this box is free of
this affliction.

The above prices suggest that the cost of a cup of milk is about
AR$34, a cup of milk with coffee is about AR$78, and a mate of yerba
for two people is AR$28.  (See file `material-observations-02022.md`.)
However, coffee without milk made according to the recipe on the
bottle (1.5 g of Arlistan instant coffee per cup) would be only AR$8,
just unpleasantly weak.

