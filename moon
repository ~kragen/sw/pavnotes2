[[[

> The full error-checking saves time when developing new programs.
> The programmer can concentrate on the essential aspects of the
> program without fussing about minor mistakes, because the machine
> will catch them.  The ability to change the program incrementally
> greatly speeds up development.

> Once the initial exploration phase is over, it is possible to turn
> prototypes into products quickly.  Good performance can be achieved
> without a lot of programmer effort and without sacrificing those
> development-oriented features that are also of value later in the
> program's life, during maintenance and enhancement.

]]]