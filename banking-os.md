I've been thinking for a while about an alternative approach to
operating systems using multiple processors, and it occurred to me
today that it can be expanded to using multiple memory banks instead.
But it probably shouldn't be.

Traditional timesharing operating systems
-----------------------------------------

Traditional timesharing operating systems work by context-switching a
single CPU between "user" and "kernel" modes and indirecting the user
code's access to memory through some kind of a page table.  Code in
kernel mode can reconfigure the page table; code in user mode
typically can't even observe it.  This prevents the user code from
altering the kernel's data or data belonging to a different process.
When the kernel decides to run a different process, it sets up the
page tables for the new process's memory maps before returning to user
mode.

In these systems, the kernel gains control from the user process in
two ways: through external interrupts (whether from I/O devices or
from a system timer) and through system calls.

The paging mechanism is fairly expensive, although it allows for all
kinds of cool tricks with virtual memory bigger than physical,
memory-mapped huge files, memory shared or transferred between
processes, copy-on-write forking, and so on.  The costs of paging MMUs
include:

- extra latency on every memory operation, often even when the
  required data is already in cache
- largish and expensive CAM in the MMU for a TLB
- extra complexity in memory caching
- reliable restartability for every instruction sequence, including in
  cases of out-of-order execution, because page faults can happen in
  perfectly correct code
- great difficulty with deterministic timing, since not only page
  faults but even TLB misses create unpredictable delays
- resulting side-channel information leaks between processes and from
  the kernel into processes

The cluster OS
--------------

One alternative I was thinking about, now that transistors are so
cheap we can have millions of computers on a chip, was to have a
"kernel node" and one or more "user nodes", each a full-fledged
computer with its own CPU and RAM.  The kernel would always be running
on the kernel node, and it can at any time reboot any of the user
nodes.  The nodes are connected by a network; at least the kernel node
can communicate with each user node, and possibly the user nodes can
communicate directly as well without the interposition of the kernel.
When a user node reboots, its boot ROM clears all of its RAM and loads
a new program from the kernel node.  None of the nodes have an MMU.

System calls are replaced by sending a message over the network to the
kernel node.  When a process wants to sleep, it first saves its state
to stable storage before informing the kernel that it no longer needs
a node until being reanimated.  As in the exokernel, the kernel warns
user processes when they are about to be evicted from their node in
order to run a higher-priority process; this gives them time to save
their state in the same way.

In low-power systems, the kernel node might actually be powered down
most of the time, being powered back up only when a user node sends it
a request or when it receives an I/O interrupt.  Of course, as well as
running device drivers on the kernel node itself, it can run them on
other nodes.

The smallest version of this system has two nodes: a kernel node and a
user node.  This is sort of wasteful since, without further
architectural changes, the kernel never does any useful work, but it
has half the CPU resources of the whole system.

The bank-switching OS
---------------------

Chatting with Jeremiah Orians about obsolete computers and their
bank-switching schemes like LIM EMS, it occurred to me that you could
use memory bank switching rather than paging as a memory protection
scheme.

*Without* bank switching, you can use more memory chips than your data
bus has bits by disabling some of the chips some of the time.  In a
small 8-bit computer with a 16-bit address bus, you might use
16-kibibit RAM chips with 14 address lines, arranged into 4 banks of 8
chips each.  In any given memory access, only one memory bank is
active; you decode the remaining 2 address lines to pull low the
chip-enable line of only one of the four banks, leaving the other
three inactive.

Memory bank switching was commonly used in such cramped computers to
expand their memory space.  The way this works is that you drive some
of those chip enable lines with something else.  Maybe you have a
register somewhere and you connect one of its bits to one bank's CE
and its logical inversion to a different bank's CE.  Then you can
switch between these two banks by writing to that bit of the register.
In the example above, you could get 80 KiB into a machine built around
an 8080, 6502, or 6800.  This was especially common when one of the
banks was ROM and the other RAM.

If you switch out *all* the banks, you have something like two
separate computers that share a CPU, though at the moment of the
transition the register contents teleport from one computer to the
other.  If we want to protect one computer from the other, this could
be dangerous, because by setting the PC and SP to arbitrary values
before the transition, either one can probably provoke data corruption
in the other.

When you switch from kernel mode to user mode, this is probably okay;
our concern is protecting the kernel from the user process, not vice
versa.  But when we switch from user mode to kernel mode, whether
because of a system call or an I/O interrupt, we want to ensure that
the PC points at the kernel entry point, regardless of what the user
process tries, and that we don't use the SP until and unless it's been
set to something safe by the entry-point code.  Moreover, we want to
ensure that the kernel-mode bit that enables the kernel-memory chips
can't be set at other times.

(It would probably work even better to switch a single bank rather
than all of them, because then the kernel could access most of user
memory, which could be useful for passing data between the kernel and
user process that was too large to fit in registers.  16KiB or even
4KiB is probably enough for a kernel.)

I don't know to what extent historical 8-bit CPUs had a reset
mechanism suitable for this: you need a "reset in progress" line that
becomes active at reset time that you can wire up to relevant the
chip-enable lines, can remain active for an arbitrarily long time, can
be made inactive under program control, and then cannot become active
again under program control without another reset.  Then you'd have to
wire up all your interrupts to the reset line.  It would be simple
enough to design a CPU to have this feature, enormously simpler than a
TLB.

For context switching between processes, you also need some way to
clear the user memory that the kernel can't see because it's been
bank-switched out.

Bank-switching with a slight twist
----------------------------------

All of the above is a very clever way to unmap the kernel memory when
the user process is running without requiring any memory mapping
hardware built into the CPU or reducing the address space available
for user processes.  But it has some big problems.

Because the kernel can dereference valid pointers into user space with
impunity, there is every possibility that it will contain bugs where
it does so without validating that they actually point into user
space.  User code may pass invalid pointers (into the space shadowed
by the kernel) by accident or on purpose, but in either case it has
passed the kernel a pointer to its own data, and if the kernel fails
to check that pointer, disaster is likely to ensue.  If the kernel
*does* check the pointer, it's still an error, so this design makes
both the user and the kernel bug-prone.

There's also the difficulty with clearing out the shadowed user
memory.

Suppose that, as before, we have a "kernel mode" line that comes out
of the CPU and is decoded as another bank-selecting address bit; but
now we add special privileged "loadfs" and "storefs" instructions that
access user-mode memory when we're in kernel mode.  (In user mode,
ideally they would trap.)  Now we just don't map the user banks in
kernel mode at all; whenever the kernel wants to access user space it
must use these special instructions, and they deactivate the "kernel
mode" line for the duration of the memory access.  The problems
described above evaporate.

There still remains the problem that the only way to switch between
two user processes is to erase all of user memory, which is slow.

MPUs
----

Probably a better solution, when we aren't limited by narrow address
buses, is a memory protection unit.  In its simplest form, this is
just a bitmap which indicates which blocks of the virtual memory space
are kernel memory and which are user memory, along with a kernel-mode
flip-flop as before.  No address translation is done, but the MPU
traps on memory accesses in the wrong mode: user accesses to kernel
memory or kernel accesses to user memory.

A hypothetical MPU along these lines for a machine like those
described above might contain 8 bits, each one governing 8 KiB of the
address space; for a 32-bit machine with 384KiB of memory you might
have a 32-bit register indexed by bits 31, 30, 8, 7, and 6 of the
address, where the least significant bit is bit 0.  This divides the
384KiB, if it's contiguous in one of the four quadrants of the address
space, into 6 64KiB blocks, and allows the kernel to deny user
programs access to the other quadrants (perhaps they contain
memory-mapped I/O devices).  If this division into quadrants is
unnecessary, you could index it with bits 8, 7, 6, 5, and 4, thus
dividing the 384KiB into 24 blocks of 16KiB.

This all presupposes that the instruction set is amenable to
position-independent code, so that in effect address translation is
done by user code in the cases where it is required.

For protected multitasking, you probably want at least a second MPU
register to indicate which blocks are currently invalid; merely
marking the memory of a sleeping task as kernel memory is an
unsatisfactory solution because a kernel bug might result in reading
data from it that the kernel then relies upon as if it were kernel
data.  Two bits per memory block permits four states rather than just
three.

Many microcontrollers have MPUs roughly similar to this.
