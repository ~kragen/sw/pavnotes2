2-D sampled SDF (“signed distance function”) decal letterforms are a
common approach to making letterforms scalable on the GPU; your shader
thresholds a bilinearly-interpolated sample from the SDF texture.  At
low texture resolution and high screen resolution this can produce
Comic-Sans-like deformation of sharp corners.  Multichannel SDFs
ameliorate this problem by sampling a multilayer texture such that the
SDF contours of each channel don’t have bevels; the intersection of
the decals from each layer gives the final decal.

(That’s not an adequate explanation, but provides enough information
to [Google the paper, even though Valve has memory-holed it][0], and
to remind you of its details and the later developments if you’ve
forgotten them.)

[0]: https://www.valvesoftware.com/publications/2007/SIGGRAPH2007_AlphaTestedMagnification.pdf

I’m interested in anisotropic text scaling for a few different
reasons.  It seems to be a useful solution to some UI representation
issues (see file `microui-recording.md`), it’s potentially a useful
resource for microtypography, etc.  I’m also interested in stroke
fonts, because Hershey fonts are awesome, and in variable stress.

So it occurs to me that, instead of computing a sampled multichannel
SDF to get nice sharp corners on angular letters, we could compute a
sampled multichannel SDF to facilitate stroking and anisotropic
scaling.  Instead of storing on each texel only the *distance* to the
nearest point in a letterform, as a sampled SDF does, we store the
*displacement* (*Δx*, *Δy*) to the nearest point in the letterform.

Stroking with different pens, including anisotropy compensation
---------------------------------------------------------------

This gives us more freedom to make slight changes in the shape of the
letterforms.  We can compute the usual SDF simply as √(*Δx*² + *Δy*²),
and we can compute dilations and approximate erosions of it by adding
a constant (or changing our threshold), but we can also compute good
approximations of a number of other variations on it.

Knuth found that to give stroked one-pixel curves a constant visual
weight it was best to use a diamond-shaped pen, which we could
approximate with |*Δx*| + |*Δy*| < 1, assuming we’re measuring
distances to an infinitely thin stroked letterform rather than an
outline.  It’s only an approximation because the nearest point by this
**L**₁ metric may not be the point to which we originally measured,
but it should be close enough most of the time.

We can approximately dilate by an axis-aligned ellipse by using
√(*aΔx*² + *Δy*²) for some constant *a*.  This can be used to give
vertical (or horizontal) stress to letterforms from a stroke font, or
to modify the stress of letterforms in an outline font (somewhat), or
to correct the modification to stress that results from anisotropic
scaling of the texture, for example to produce an expanded or
condensed font.  This is only an approximation of the ideal dilation
because, again, the nearest point in this stretched space may not be
the original nearest point.

To rotate the stress ellipse, we can use √(*a*(*Δx* + *sΔy*)² +
(*Δy* - *sΔx*)²) instead.  Similarly, we can rotate and
anisotropically scale the diamond pen.

Another norm of interest here is the **L**∞ norm: *Δx* ↑ *Δy*, where ↑
is the pairwise max operator, returning whichever of its two arguments
is larger.  This gives us a pen that is an axis-aligned square.  This
can be rotated and anisotropically scaled in the same way.

More generally we can dilate the letterform SDF by any pen we can
write down a closed-form SDF for.  For example, -2 + √((0 ↑ |*Δx*| -
5)² + (0 ↑ |*Δy*| - 3)²) gives us an axis-aligned roundrect with a
corner radius of 2, a height of 6, and a width of 10, although this is
far enough from being round that artifacts from measuring to the wrong
point might start to be obtrusive.

In general it is probably more efficient to threshold the squared
distance rather than actually taking the square root.  In the previous
example, for example, we can compare 4 < (0 ↑ |*Δx*| - 5)² + (0 ↑
|*Δy*| - 3)².

Sampling the displacement SDF
-----------------------------

Computing the original SDF for the letterforms often does not have to
be extremely efficient because it can be precomputed.  A brute-force
approach when a letterform consists of line segments (as in Hershey
fonts) is to sample the SDF for each line segment and take their
minimum.  [Inigo Quilez’s implementation][2] for a line from `a` to
`b` dilated by a sphere of radius `r` is:

    float sdCapsule( vec3 p, vec3 a, vec3 b, float r )
    {
      vec3 pa = p - a, ba = b - a;
      float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
      return length( pa - ba*h ) - r;
    }

Here `ba` is the displacement from one end of the line segment to the
other; [`clamp`][3](*x*, *y*, *z*) is (*x* ↑ *y*) ↓ *z*, where ↓ is
the pairwise minimum operator; and `length` is the Euclidean norm.  So
`a` + `ba*h` is the point on the line segment nearest the point we’re
querying it at.  It’s probably most convenient in this case to return
that point rather than the distance to it.

(Note that for a particular object the quantities `ba` and `1/dot(ba,
ba)` remain constant for all points `p`, so they may be worthwhile to
precompute.)

[2]: https://iquilezles.org/articles/distfunctions/
[3]: https://registry.khronos.org/OpenGL-Refpages/gl4/html/clamp.xhtml

This is very simple but potentially slow if you have a lot of line
segments and a lot of pixels.  You can of course use a BVH to optimize
the search for nearby line segments, but approximate algorithms may be
good enough.

A very simple approximate linearithmic-time algorithm is to start with
a pixel grid of all nil, draw nearest intersection points into it
Bresenham-wise, and then repeatedly pick the nearest frontier pixel to
“relax” by marking it done, adding each of its neighbors to the
frontier if they are not done, and storing its nearest point in each
of its neighbors who don’t already have a closer point.  Basically
Dijkstra’s shortest path algorithm.

This is approximate because only a few points on each segment are
used, the ones that were sampled in the initial Bresenham step.

I think it might be reasonable to use a pixelwise skeletonization
algorithm to compute the initial set of points, but in this case you
need to store a “height” along the skeleton which is subtracted from
the SDF that results from it.
