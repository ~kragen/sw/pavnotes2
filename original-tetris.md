(sleep 3; mame ie15 -uimodekey F12 -rs232 null_modem -bitb socket.127.0.0.1:2323 -window -skip_gameinfo  -autoboot_delay 1 -autoboot_script ie15.lua ) & ~/pkgs/simh-2024-07-05_06-07-03-Windows-Win32-4.0-Current-670a3728/PDP11.exe rt11.ini 

startup_command = "\rRUN DL1:TET\r"

co = coroutine.create(function ()
  natkeyboard = manager.machine.natkeyboard

  one_moment = emu.attotime.from_double(0.02)

  key_right = manager.machine.ioport.ports[":ie15:keyboard:TERM_LINE1"].fields["Right"]
  key_next_setup = manager.machine.ioport.ports[":ie15:keyboard:TERM_LINE3"].fields["Next SetUp"]
  key_setup = manager.machine.ioport.ports[":ie15:keyboard:TERM_LINEC"].fields["SetUp"]

  type_key = function(key)
    key😒et_value(1)
    natkeyboard😛ost_coded("")
    emu.wait(one_moment)
    key😒et_value(0)
    natkeyboard😛ost_coded("")
    emu.wait(one_moment)
  end

  type_key(key_setup)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_next_setup)
  type_key(key_right)
  type_key(key_next_setup)
  type_key(key_setup)

  natkeyboard😛ost_coded(startup_command)

end)

coroutine.resume(co) 

Benjamin C Wiley Sittler says:```
startup_command = "\rRUN DL1:TET\r"

co = coroutine.create(function ()
  natkeyboard = manager.machine.natkeyboard

  one_moment = emu.attotime.from_double(0.02)

  key_right = manager.machine.ioport.ports[":ie15:keyboard:TERM_LINE1"].fields["Right"]
  key_next_setup = manager.machine.ioport.ports[":ie15:keyboard:TERM_LINE3"].fields["Next SetUp"]
  key_setup = manager.machine.ioport.ports[":ie15:keyboard:TERM_LINEC"].fields["SetUp"]

  type_key = function(key)
    key😒et_value(1)
    natkeyboard😛ost_coded("")
    emu.wait(one_moment)
    key😒et_value(0)
    natkeyboard😛ost_coded("")
    emu.wait(one_moment)
  end

  type_key(key_setup)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_right)
  type_key(key_next_setup)
  type_key(key_right)
  type_key(key_next_setup)
  type_key(key_setup)

  natkeyboard😛ost_coded(startup_command)

end)

coroutine.resume(co)
``` 
