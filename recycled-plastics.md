When writing file `house-regenerator.md` I got to looking at bulk
recycled plastics on MercadoLibre, because it turns out that
polyethylene is one of the few materials with a significantly higher
specific heat than the usual 1 J/g/K.  Recycled plastics aren't as
cheap as sand or gravel but they're cheaper than a surprising number
of things.

[Recycled low density polyethylene pellets for making bags cost
AR$230/kg][5] = US$0.79/kg and [HDPE and PP are about the same][6].

One vendor of these recycled plastics is [Marcelo Aless 333 in
Avellaneda][7], aka Cooperativa de Trabajo Amanecer Ltda in Burzaco or
maybe Sarandi or Villa Dominico, or "mellis carp Aless" in Google
Maps; they use the term "recuperado" rather than "reciclado" for some
reason.  "Peletizado" is another good search term, and "inyección" and
"soplado" are even better.  Sometimes it's said to be "en grumo"
instead of "peletizado".  Google Maps has photos of the plastics the
dude has uploaded and has the address "EVR Sarandi Buenos Aires AR,
Gral. Deheza 1550, B1874", 11 6113-4321, Villa Dominico, Buenos Aires,
<https://amanecer.coop.ar/>, <mailto:coopamanecer@yahoo.com.ar>.  This
is nearish the Villa Dominico station, the one between Sarandi and
Wilde, and even closer to the Avellaneda Municipal Cemetery.  [Another
vendor, Javi Krau in Florida Oeste, has a 25 kg minimum][8]; Nestor at
CIAPLAS is another vendor in Lanús; [Nacho Plast has various plastics
but a 50 kg minimum][9].  [Braskem-brand new HDPE costs
US$1.40/kg][10].

[5]: https://articulo.mercadolibre.com.ar/MLA-1126470590-pellet-polietileno-baja-densidad-recuperado-_JM
[6]: https://articulo.mercadolibre.com.ar/MLA-1193297035-pp-pellet-polipropileno-y-pead-polietileno-alta-densidad-_JM
[7]: https://articulo.mercadolibre.com.ar/MLA-836291941-pead-polietileno-cajones-_JM
[8]: https://articulo.mercadolibre.com.ar/MLA-842136492-polietileno-verde-manzana-pebd-inyeccion-de-primera-_JM
[9]: https://articulo.mercadolibre.com.ar/MLA-797601798-alto-impacto-recuperado-negro-scrap-industrial--_JM
[10]: https://articulo.mercadolibre.com.ar/MLA-1122702252-polietileno-ad-soplado-virgen-_JM

[NIST has a paper on the specific heat of polypropylene][11], which
depends not only on temperature but also on crystallinity.  It uses a
formula weight of 42.08 (CH₂-CHCH₃-) and gives a formula for specific
heats in the 230–350 K range of (1.5912e6 T⁻² + 0.3837 T - 64.551)
J/mol/K for amorphous polypropylene, with experimental values at 290 K
mostly in the 70–80 J/mol/K range, because crystallinities were in the
0.25–0.75 range.  I guess this means about 1.7–1.9 J/g/K, which is
comparable to polyethylene.  [Engineering Toolbox gives 1.92
J/g/K][12].  But [LDPE is 2.6 J/g/K][13] and paraffin wax is even
higher.

[11]: https://srd.nist.gov/JPCRD/jpcrd189.pdf
[12]: https://www.engineeringtoolbox.com/specific-heat-capacity-d_391.html
[13]: https://designerdata.nl/materials/plastics/thermo-plastics/low-density-polyethylene
