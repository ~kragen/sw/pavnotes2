Kitchen aluminum foil is commonly only 10 microns thick, though some
brands are as thick as 50 microns, so a roll of aluminum foil occupies
a tenth the volume of the equivalent area of paper.  This makes it
appealing as a medium for writing.  Moreover, there is little or no
danger of the foil cold-welding on Earth thanks to the passivation
layer that forms in contact with air, and although metallic aluminum
is probably not stable over geological timescales (native aluminum is
unknown to geology) it is still probably stabler than the 1000-year
lifespan of paper or photographic film.

A codex of aluminum foil might be less usable than a codex of paper
because of the tendency to hole the pages with plastic-deformation
singularities while turning them by hand, but a reel like a reel of
film or a Torah scroll should be fine.

Writing on the surface of the reel with ink might make it
significantly thicker, but a hole through the foil would not, and you
can make such holes conveniently with electrical sparks if their
diameter is comparable to the thickness of the foil.  Such holes are
perfectly transparent, while the unholed foil is essentially
completely opaque to visible light, so by backlighting the foil you
can achieve brightness ratios of 1000:1 or more.

10-micron holes can be positioned with better precision than 10
microns, perhaps 1 micron, and they can reasonably take up about half
the area of the foil without weakening it unduly, so their centers can
be some 15 microns apart.  As a crude approximation this gives you 100
possible positions for a hole within each 15 × 15 micron area, plus
the possibility of not having a hole, for an information density of
some 360 kilobytes per square centimeter:

    You have: log2(100) bits/(15 microns)**2
    You want: bytes/cm**2
        * 369103.12
        / 2.70927e-06

If you had such a microfilm reel of 300 mm diameter, it would contain
7 km of foil:

    You have: pi (150mm)**2 / 10 micron
    You want: 
            Definition: 7068.5835 m

If it were 30 mm wide, it could contain almost a tebibyte:

    You have: (log2(100) bits/(15 microns)**2) (pi (150mm)**2 / 10 micron) 30 mm
    You want: gibibytes
        * 728.9563
        / 0.0013718243

It would also weigh 5.7 kg, heavier than current tebibyte disks and
rather unwieldy for convenient handling, though still lighter than a
Torah scroll, which is conventionally carried by a single person.

If you instead wanted to make a reel human-readable and pocket-sized,
you could use a 5×8 pixel font with 15-micron pixel spacing, encoding
information only in the presence and absence of holes.  For 15 microns
to be enlarged to the 1 arc minute required for standard human visual
acuity, you need a focal length of about 52 mm, which nearsighted
people have natively, and farsighted or presbyopic people can achieve
with moderate magnification such as a jeweler's loupe.  (This would be
about a 19-diopter lens, so reading glasses aren't enough; using the
standard equation for magnifying power, V = 250 mm φ + 1, this would
be a "5.8×" lens: stronger than many magnifying glasses, but quite
weak as loupes go.)

The width of a 20-mm strip would be 1333 pixels, 166 lines of text,
something like a full-sized newspaper, or 2½ A4 pages one above the
other.  If the reel had an inner diameter of 10 mm and an outer
diameter of 50 mm so as to fit in a pocket, it would be only 188 m and
weigh 102 g:

    You have: (pi (25 mm)**2 - pi(5 mm)**2) / 10 micron
    You want: 
            Definition: 188.49556 m
    You have: (pi (25 mm)**2 - pi(5 mm)**2) 20 mm 2.7 g/cc
    You want: 
            Definition: 0.1017876 kg

That's 3.77 m² of total area holding 16.8 gigapixels:

    You have: (pi (25 mm)**2 - pi(5 mm)**2) 20 mm / 10 micron
    You want: 
            Definition: 3.7699112 m^2
    You have: (pi (25 mm)**2 - pi(5 mm)**2) 20 mm / 10 micron / (15 micron)**2
    You want: 
            Definition: 1.6755161e+10

That's 419 million characters or 79300 pages:

    You have: (pi (25 mm)**2 - pi(5 mm)**2) 20 mm / 10 micron / (15 micron)**2 / 5 8
    You want: 
            Definition: 4.1887902e+08
    You have: (pi (25 mm)**2 - pi(5 mm)**2) 20 mm / 10 micron / (15 micron)**2 / 5 8 80 66
    You want: 
            Definition: 79333.148

419 megabytes is a lot less than 729 gibibytes; the losses are due to
encoding only one bit of information per hole instead of 6.6 (6.6×),
using 40 bits per character instead of 8 (5×), using 20 mm width
instead of 30 (1.5×), and using an outer diameter of 50 mm instead of
300 (36×).

To keep the thin reel of aluminum foil from breaking, you could
reinforce it with thicker diagonal strips, two or three across the
height of the tape, which could potentially be several times thicker
than the aluminum itself.  Wound on the reel these have a helical
form, and it can be arranged so that on the storage reel they don't
overlap the helices of the immediately previous and successive
windings.  Such a reinforcement might be 50 microns thick, made of
polyimide or UHMWPE, and 500 microns wide, with a sort of
bell-curve-like cross section to avoid embossing sharp creases into
the aluminum laid over it.  These reinforcement strips would be
directly on top of reinforcement strips from 13–20 windings ago, so
they would add 25%–40% to the size of the roll, a significant but not
overwhelming amount.

Nickel foil would probably work as well as aluminum, *would* be stable
over geological time, and would pose less risk of breaking.  It would
be heavier, though, and I don't know where to get it.  (Nickel strip
("fleje de niquel") is readily available for making battery packs but
it's 100–150 microns thick rather than 10 microns thick, and
hot-rolling nickel is considerably more demanding than hot-rolling
aluminum.)

Moving at a fast-forward speed of 1 m/s this pocket tape would take a
bit over three minutes to reach one end of the tape from the other.
This is a bit slow for following a hyperlink but possibly still faster
than the equivalent in a paper library of 128 volumes or so.

In all likelihood you can get legible text in a significantly smaller
area by offsetting hole positions.  Also, you can vary hole size as an
additional data channel, even if you are making them with sparks in
air.  So the above estimates are conservative.

