If you’re appending to a dynamically-allocated array inside a
subroutine, each time you append some data, you need to check whether
the data will overrun the allocated space and, if so, expand the array
by allocating more space.  If this is a thing you’re doing a lot, you
might reasonably spend a couple of local variables on it; callee-saved
registers, maybe.  In the common case, this means appending is 2–4
machine instructions; in a made-up idealized instruction set:

        if %p10 == %p11 goto 1f
    2:  *%p10++ <- %t3
        ...
    1:  call expand_buffer
        goto 2b

In a less ideal instruction set, the first instruction pair might be
more like this:

        %p10 <=> %p11?
        if eq goto 1f
    2:  *%p10 <- %t3
        %p10++
        ...

The key thing here is that, in the common case, this operation is
*much faster* than a subroutine call, about an order of magnitude.
But the rare case of reallocation is much slower, so adding in a call
to a millicode subroutine is no big deal.

This looks almost exactly like the open-coded pointer-bumping nursery
allocation code used by many garbage-collected languages.

There are some interesting possibilities here:

1. What if this output stream is implicitly passed to callees, so they
   can write to it?
2. What if `expand_buffer` does something like swap call stacks,
   making this mechanism suitable for producer-consumer coroutines?
3. How about consuming input instead of producing output?
4. What if it’s not a *stream* so much as it is a *stack*?
5. How about writing more than one byte at a time?
6. What would hardware support for this pattern look like?  Is that a
   sensible thing to do?

Dynamically scoped output streams
---------------------------------

Machine registers are implicitly passed to callees.  It’s also pretty
common for a subroutine that’s writing to a stream to invoke callees
to output to the same stream.  Maybe it’s worthwhile to take advantage
of this coincidence?  This avoids this kind of nonsense in the
calling sequence of each such subroutine:

        # before the call
        %arg0 <- %p10
        %arg1 <- %p11
        # ...other args...
        call sukutrule
        # ...
    sukutrule:
        push %p10
        push %p11
        %p10 <- %arg0
        %p11 <- %arg1
        # ...body of sukutrule...
        pop %p11
        pop %p10
        return

Passing this context as a dynamically-scoped variable in two registers
reduces the above to the following:

        call sukutrule
        # ...
    sukutrule:
        # ...
        return

These implicit arguments must be supplied to any routine that uses
them; effectively they have their own slightly unusual calling
convention.  It slightly reduces the register pressure, perhaps.

If one of these routines wants to write to a different output stream,
it can save the existing register pair on the stack and restore it
when it’s done.  At the assembly-language level, this looks like a
somewhat different interface from the C standard I/O interface;
instead of saying

    fputc('\n', f);
    fputc('\t', f);
    fputc('\n', g);

you are saying the equivalent of

    putchar('\n');
    putchar('\t');
    FILE *f = redirect(g);
    putchar('\n');
    redirect(f);

with each “call” being a handful of instructions; but a compiler
targeting this calling convention could fully bridge that gap.

Dynamic handling of buffer-full events
--------------------------------------

It would be simple enough for `expand_buffer` to do a statically
predetermined thing, like this in pseudo-C:

    __attribute__((no_caller_saved_registers))
    void expand_buffer()
    {
        size_t newsize = (%p11 - buffer)*2;  // buffer is global or TLS var
        void *np = malloc(newsize);       // (but either way doesn’t need a reg)
        if (!np) abort();
        memcpy(np, buffer, %p10 - buffer); // copy data to new buffer
        %p10 = np + %p10 - buffer;         // move fill pointer to new data
        %p11 = np + newsize;               // set limit pointer
        buffer = np;
    }

And clearly that’s what it should do some of the time.  But given that
it needs to save and restore four registers or so, and memcpy probably
a few hundred to a few thousand bytes, we might as well add the
additional extra overhead of indirecting such handling through a
function pointer.

What else might you do, other than the above?

1. Allocate a new non-contiguous buffer for the additional data,
   adding it to a doubly-linked list of buffers, thus avoiding the
   unpredictable pause to copy the data.
2. Flush the current output buffer to a file and reset the output
   pointer to its beginning.
3. More generally, save all the callee-saved registers and switch
   contexts to a different coroutine that will consume the buffered
   data, not switching back until the buffer is empty.  This might be
   as simple as `setjmp`, but usually it will require switching
   stacks, and in some cases might involve heavier-weight operations
   like loading an overlay or compiling the code that is to be run.

Consuming input rather than producing output
--------------------------------------------

The pattern for consuming input looks almost exactly the same:

        if %p8 == %p9 goto 1f
    2:  t3 <- *%p8++
        ...
    1:  call refill_buffer
        goto 2b

Here I’m using a different pair of pointer registers than I was using
for the output example, because it seems to me that it is common for a
single subroutine to have both an input stream and an output stream.

In my alternative #3 above, where `expand_buffer` switches contexts to
a different stack, it might be the case that the different stack is
reading precisely from the buffer that was just filled; so its %p8 and
%p9 will be set up by `refill_buffer` to delimit the new data.

In a case like this, you might want to tell `refill_buffer` ahead of
time where to go if the buffer can’t be refilled because the stream
being consumed has ended.

### As an iteration protocol ###

A common case for this kind of thing is iterating over sequences from
Python-like generators.  Here is a spectrum of different possible ways
to handle computing a new value for a loop variable:

1. ≈2 ops, where an op is more or less a RISC instruction: increment a
   counter, compare to a limit, jump to the top of the loop if it’s
   still less.  DSPs commonly do this in parallel with whatever else
   you’re doing (“zero-overhead looping”), and superscalar processors
   usually will too.
2. ≈3 ops: load a value from a buffer, as above.  However, writing it
   into the buffer took another ≈3 ops, so it’s more like 6.
3. ≈11 ops: invoke a block that increments a counter and compares to a
   limit, using the modified calling convention for blocks I wrote
   about in file `block-arguments.md` in Dernocua, and a bit in file
   `tclish-basic.md`.
4. ≈19 ops: invoke a full-blown closure to do the same thing.
5. ≈30 ops: invoke a full-blown dynamic method dispatch.

The closure looks something like this in ARM Thumb-2 assembly.  The
closure pointer is in r4, and it returns the next value in r0 and an
end-of-loop flag in r1, which will be 0 in case of end of loop.
Here’s the caller, who is trying to loop:

        ldr r5, [r4]    @ load closure code pointer outside of loop
    1:  mov r0, r4      @ pass closure pointer to closure
        blx r5          @ invoke closure
        cbz r1, 1f      @ compare and branch if zero to exit loop
        @ ... loop body does whatever it wants with r0 ...
        b 1b            @ we can’t cbz or cbnz backwards, sigh
    1:  @ post-loop code

That’s ≈6 ops right there, depending on whether you have pipeline
flushes and whether you count them as ops.  Here’s the
index-generation closure code.  It’s getting passed the closure
pointer in r0, so it needs to load the counter and limit:

        ldr r1, [r0, #4] @ load counter
        ldr r2, [r0, #8] @ load limit
        cmp r1, r2
        beq 1f           @ skip to exit case if we’re done
        add r1, #1
        str r1, [r0, #4] @ store updated counter
        sub r0, r1, #1   @ reconstitute un-updated counter!
        mov r1, #1       @ not end of loop!
        bx lr            @ return new loop counter
    1:  mov r1, #0
        bx lr

That’s another ≈13 ops, for a total of ≈19 per item, a punishing
overhead for small loops.  If you have to look up the function pointer
for r5 in a hash table or something, that adds a bit more.

It’s not surprising that Apple [switched the Objective-C iteration
protocol `NSEnumerator` to a buffer-based thing like this called
`NSFastEnumeration`][0], as [explained in the GCC docs][1].

[0]: https://stackoverflow.com/questions/5163169/fast-enumeration-vs-nsenumerator-in-objective-c
[1]: https://gcc.gnu.org/onlinedocs/gcc/Fast-enumeration-protocol.html

A drawback of these buffering approaches is that, if they’re not
giving up control until the buffer is full (for the writer) or empty
(for the reader), they may generate items that are not required; that
could take arbitrarily long.

Stack allocation
----------------

I mentioned above that the pattern is exactly like the pattern of
bump-pointer allocation.  This means it’s also exactly like stack
allocation, except that the stack is never popped.

But sometimes you *could* pop the stack to a previously saved
position.  This is useful in a variety of situations:

1. Backtracking: undoing some previously produced output, for example
   compilation output produced while attempting a parsing alternative
   that didn’t succeed.
2. Recomputation of derived values: XXX

XXX

Writing more than one byte at a time
------------------------------------

The crucial thing for writing more than one byte at a time is that the
limit comparison is no longer for equality, but for ordering; and it
must not be vulnerable to overflow.  This is handled correctly by `if
(%p10 + n - %p11 >= 0)`, which won’t give an incorrect answer due to
overflow unless you’re writing 2³¹ or 2⁶³ bytes past the buffer limit.

This is particularly advantageous if you have unaligned memory access
instructions that allow you to write a number of items in one or two
clock cycles.

Hardware support
----------------

There are a few irksome things about buffering output in this way,
implemented in software:

1. It uses up two general-purpose registers rather than one.  Or zero.
   This hurts code density.
2. On an in-order machine, the bounds check takes an extra cycle.
3. On an in-order implementation of an especially doctrinaire RISC
   machine like RISC-V, so does incrementing the fill pointer!
4. You need an extra `expand_buffer` callsite for each place where you
   try to write into the buffer, because you can’t conditionally jump
   and link in most instruction sets, including Thumb-2 (but not ARM).
   This also hurts code density.
5. If you’re using this approach to iterate over a sequence, and that
   sequence happens to be something simple like an arithmetic
   sequence, you need to write it into an array first, which makes
   this approach about three times slower.
6. In memory, it is restricted to unit-stride vectors.
7. It’s all entirely voluntary.

Suppose instead you had two or more *range registers* in your CPU,
which are a lot like segments.

Each range register is divided into a base register *b*, a limit
register *m*, a stride register *s*, and a handler register *h*.  For
the moment I’ll assume word-oriented memory.  The available operations
include:

- Some way to save and restore a range register from memory or another
  range register.
- `next(R) -> w` reads a word from [R.b] and adds R.s to R.b.
- `read(R, idx) -> w` reads a word from [R.b + R.s × idx].  (The
  particular addressing modes available in the CPU for idx are not
  that important; assume it can be at least an integer register.)
- `write(R, idx, w)` writes a word to [R.b + R.s × idx].  Some range
  registers may not permit this.
- `set_handler(R, label)` sets R.h to `label`.
- `decimate(R, n)` sets R.s to R.s × n.
- `drop(R, n)` adds R.s × n to R.b.
- `drop_back(R, n)` subtracts R.s × n from R.m.

If `next`, `read`, or `write` tries to access an address beyond R.m,
then the current instruction address is stored in a link register, and
control is transfered to R.h.

None of these operations require microcode execution.

It may be useful to restrict `n` to be a power of 2 so that no actual
multiplication is required, to have a non-indirect bit on the register
which causes `next` to return R.b rather than accessing memory, and to
allow R.s to be 0.

This is not just a question of I/O buffering; things like matrix
multiplication can benefit from this approach as well.

These range registers are a few tweaks away from being a memory
protection system, too.
