I feel like I finally got to the point of understanding "why Forth."
I've written about this before in these pothi, but now I think I might
have a better formulation.

Forth isn't a programming language; it's a debugger
---------------------------------------------------

The confusing thing about Forth is that people think it's a
programming language, and the problem is that, considered as a
programming language, it's *terrible*.  So why do some people love it?
I think the answer to the mystery is that Forth is *really* a monitor
or debugger, like DDT or GDB, or the ROM monitor at CALL -151 in the
Apple ][ ROM.

There's [a standard word in Forth][0] called `?` which outputs the
value at a given memory location to the terminal, the same as the
sequence `@ .`.  For example:

    variable foo  ok
    foo ? 0  ok
    53 foo !  ok
    foo ? 53  ok
    1 foo +!  ok
    foo ? 54  ok
    foo @ . 54  ok

[0]: https://www.complang.tuwien.ac.at/forth/gforth/Docs-html/Examining.html#index-g_t_003f-_0040var_007b-a_002daddr-_002d_002d---_007d--tools-2808

*This makes no sense* for a programming language.  Why would you
devote a precious single-character syntactic token like `?` to
abbreviating a three-character sequence?  In C it's the *conditional
operator*.  In Kotlin it's the *nullability type suffix*.  These are
very important to their respective languages!

But it's [in the draft proposed ANSI standard for Forth][1], in the
optional Programming Tools wordlist, and every Forth I've ever used
has it.

[1]: https://www.taygeta.com/forth/dpans15.htm#15.6.1

If you think of Forth as a debugger, it makes perfect sense that
you want a single-character command to print the value of a variable
(or other memory location).  Every debugger has one!  Some don't even
require you to hit RET!  It would be intolerable to have to type SPC @
SPC . RET every time you wanted to inspect a value in your debugger!

However, Forth has enough of a built-in scripting language that you
can use it as a macro assembler, you can write a text editor in it,
and you can write databases in it, just like you can write a text
editor or database or terminal emulator in Bash.  (I have in fact seen
a terminal emulator written in Bash.)

Bash is not a good language to write a text editor or database or
terminal emulator in.  It's slow and bug-prone.  It can be made to
work, but Bash is primarily designed as an interactive user interface,
not a programming language, and it compromises a lot of virtues you'd
like in a programming language.

Compared to assembly language or C, Forth is also slow and bug-prone,
and also compromises a lot of virtues
for the sake of interactivity.  But, by virtue of being
scriptable, Forth is a *really great* ROM monitor!

Lars Brinkhoff points out, "Part of the problem is people implementing
Forth in another language.  They will learn about the Forth virtual
machine, but not how to program in Forth."  And I think that's very
true: the great virtue of Forth is its interactive user interface,
which is not really a question of the language design or the virtual
machine design.

Well, Forth kind of *is* a programming language
-----------------------------------------------

Forth isn't quite *just* a scriptable ROM monitor.  It's almost the
minimum possible that you have to add to machine code to get
interactivity, naming, nested expressions, virtual memory, compile-time
metaprogramming, structured control flow, higher-order programming,
and subroutines with arguments and return values.  But it's still
basically a simple scripting layer over assembly language, rather than
a *programming language* as the term is conventionally understood.

Forth has a somewhat conflicting design objective: it's a toolkit for
constructing a "problem-oriented language", what's commonly described
nowadays as a "domain-specific language".  SQL, regular
expressions, and CSS are familiar examples.
The Forth text interpreter is intended to act as a user
interface to your application, which you write (and make scriptable!)
by providing a problem-domain vocabulary to the end-user.

This is similar to how people use Bash or Tcl.

How to stop juggling the stack in Forth
---------------------------------------

Stack juggling is a common obstacle when people begin to write Forth,
and it commonly presents such an obstacle that people give up on Forth.
It took me a long time to get past it.  I'm mostly past it,
anyway, I think.

It seems to me that the stack juggling problem comes from programmers
trying to avoid using variables in Forth, because variables in Forth
are "global", and we all know global variables are bad.  But Forth
variables aren't really global.  They're lexically scoped!  Every
subroutine can have a separate variable named `x`!  They aren't
*reentrant*, so if you set them in a recursive subroutine you usually
have to manually save and restore their values, but that's a
comparatively minor problem.

I think it may be a useful practice to write your Forth code initially
without any stack manipulations except DROP — no DUP, no SWAP, no >R,
no R@, no OVER, certainly no NIP TUCK ROT -ROT.  When you're tempted
to use a stack manipulation operator, use a VARIABLE.  (Or a VALUE.)

I suspect stack juggling may have been less of a problem when Forth
was born 50 years ago.  50 years ago many programmers used MIX, PDP-8,
FORTRAN, BASIC, and COBOL, and none of them supported recursive
subroutines!  But now we're accustomed to allocating our local
variables in stack frames, which uses less memory and makes any
subroutine recursion-safe and multithreading-safe; so our
immediate impulse on attempting to use Forth is to put our variables
on the operand stack.  This very quickly leads to unmaintainable code,
the opposite of the intended effect.

I've often had bugs in Forth due to keeping a value on the stack
instead of in a VARIABLE or VALUE; I've never had bugs due to
keeping a value in a VARIABLE instead of on the stack.  So now I try
to err towards the latter, because the worst that happens that way is
that my code is kind of boring and long-winded, instead of clever and
hard to debug.  See file `forthlocals` for more details.

So now I use variables in Forth only slightly less than in C.

If you start to use a *small* amount of stack manipulation, then Forth
gets better.  You can get away with keeping *one* anonymous variable
on the return stack and usually one on the operand stack as well, and,
of course, if you do do something recursive, you have to use the
operand stack to save the old values of variables on entry and restore
them on exit, as if they were assembly-language registers.  (Recursion
is not Forth's forte, but it's a lot better at recursion than FORTRAN
or BASIC.)

But it's easy to go past the point of a *small* amount of stack
manipulation, and suddenly Forth gets enormously worse, because you're
playing 4-D mental chess trying to get your stack effects right.

Why you shouldn't add stack-allocated variables to Forth
--------------------------------------------------------

When confronted with "global" variables in Forth, many people think,
well, I'll just add local variables to Forth — as aap said, allocate
some space on the stack and index into it, as Ken Thompson's B does.
Some Forths even come with this feature.

The problem with this is that it cripples Forth as a debugger!  The
way you single-step a Forth program normally is that you type the name
of the line of code you want to run.  After you run the line of code,
you can look at the variables that it may have changed.  But that
doesn't work if you create local variables by allocating some space on
the (return) stack and indexing into it, because those variables
aren't there anymore.

Also, it creates a strong pressure to make your
Forth words more than a line of code, which also cripples your ability
to single-step the code by typing the name of a word.

Propitious circumstances for enjoying Forth
-------------------------------------------

In addition to using VARIABLEs (or VALUEs) instead of stack
manipulations, try programming interactively in something like Gforth or Open
Firmware, rather than in a Forth file that you then save and then feed
to a batch-mode Forth compiler.  That's because, if you're going to feed an
input file in a low-level language to a batch-mode compiler, you're
probably always going to be happier if it's a C compiler, because C is
a much better programming language than Forth;
but Forth is a much better interactive user
interface.

It's probably also advantageous if what you're trying to program with
Forth is something like a typical embedded microcontroller
application: logically pretty simple, but requiring significant
experimentation to figure out where reality diverges from what you
think it is.  Forth's forte is simple commands and writing bytes to
I/O ports to see what happens, not sophisticated algorithms and data
structures and readable code.  [It's what Yossi Kreinin praised about
Tcl][2], except that Forth can do it in four kilobytes instead of two
megabytes.  Unfortunately, Gforth can't.

[2]: https://yosefk.com/blog/i-cant-believe-im-praising-tcl.html

If you don't use stack manipulations except DROP, basically
Forth is only a little worse than C, considered as a programming
language.  Everything takes roughly the same amount of code as it does
in C, but you don't have any static typing, and you can accidentally
pass arguments to the wrong subroutine or consume the wrong number of
results, so you have runtime bugs that would be compile-time errors in
C, and you have to debug them.

But, on the other hand, you have full-fledged metaprogramming with the
entire Forth system at compile time, and you can also use the entire
Forth system to debug your program as well instead of trying to script
GDB with Python — or, far worse, gdbscript.

Am I full of shit?
------------------

Kind of.  I'm not a Forth expert by any means, though I did write
<https://github.com/kragen/stoneknifeforth>, which does compile itself
successfully!  But it's not really Forth because it isn't interactive.
I didn't understand what I wrote above at the time.

Chuck Moore has been saying some of this stuff over and over again for
decades, but it's hard to understand because although it's clear that
he's contradicting beliefs that we take for granted, it's not clear
*which*.  (I'm kind of guessing myself, because I'm not a Forth
expert.)  This is the challenge of attempting to communicate across
paradigms.

See file `forth-distance.md` and file `interactive-forth.md` for more
about the Forth user interface.
