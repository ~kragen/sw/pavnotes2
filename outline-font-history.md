In file `alan-kay-talk-02024.md` I noted that in Alan Kay’s lecture
last month he claimed that not just PostScript but also outline fonts
were invented at Xerox PARC.

Were they really a PARC invention?  Knuth published four papers about
METAFONT in 01979, including [the METAFONT manual as a Stanford tech
report][1] [available from DTIC][2] with an American Mathematical
Society copyright, which has no bibliography, but in the text cites
René Descartes’ “revolutionary work *La Géometrie* in 1637”,
“sixteenth-century methods of mathematical type design” with ruler and
compass, Knuth’s papers on dragon curves with C. Davis and J. C. Knuth
(presumably his wife Jill Carter Knuth), and nothing about Xerox PARC,
though it was within a day’s walk distance of Stanford.

[The 192-dpi Xerox Graphics Printer (XGP) was at five universities by
01972][4], two of which were Stanford and MIT, and although you
*could* draw a 192-dpi font pixel by pixel, you really wouldn’t want
to.  So, did it ship with outline fonts?

Knuth’s 01982 paper “The Concept of a Meta-Font” in *Visible Language*
cites P. J. M. Coueignoux’s 01975 dissertation [“Generation of roman
printed fonts”][8] at MIT as “the first use of sophisticated mathematical
curves to describe letter shapes to a computer”.  But then he cites
Mergler and Vargo’s 01968 [“One Approach to Computer Assisted Letter
Design”][5], also published in *Visible Language*, where they describe
a system called ITSYLF, an Interactive Synthesizer of Letterforms,
which plotted letters on a Cal-Comp [sic] plotter.

ITSYLF supported continuously variable parameters in a font, but it
wasn’t really a font design program; it was a metafont, written in
machine code, to which you could input parameters such as stroke
widths.  It used straight lines and Piet Hein’s superellipses to draw
the outlines of the characters; Bézier splines have the advantage over
superellipses that they’re trivial to rotate, scale, and skew, which
was fundamental to PostScript and its PARC predecessors.

*Visible Language* comes through again, explaining the history in more
detail in 02016, in Patrick Baudelaire’s [“The Xerox Alto Font Design
System”][7], describing a program named FRED:

> This article is from a talk given at Stanford University in 1983 at
> a seminar for the Association Typographique Internationale
> (ATypI). It describes pioneering digital font software developed at
> the Xerox Palo Alto Research Center in 1974. Built for prototype
> personal workstations, the software uses mathematical curves called
> “splines” to define the outlines of letter shapes that are converted
> to bitmaps (pixel mosaics) for use on computer screens and digital
> printers. This spline-bitmap model is used today for the screens of
> nearly all computers, smart phones, ebooks, and other text
> displays. Previously unpublished, the manuscript appears here as
> digital font archaeology – a glimpse of concepts from four decades
> ago that became the technology of much that we read today. We are
> grateful to Patrick Baudelaire for permission to publish it as he
> wrote it in 1985 and to the Cary Graphic Arts Collection of
> Rochester Institute of Technology for providing scans of the
> original manuscript and images in its collection.

So, did Xerox PARC invent outline fonts as we know them today — the
year before Coueignoux reinvented them independently, because PARC
didn’t publish their work?

Interestingly there is a [Xerox PARC source code archive][3].

Coueignoux also did his master’s thesis at MIT in 01973 on fonts.  It
was entitled “Compression of type faces by contour coding”.  Is it
possible he invented outline fonts, and the Alto Font Design system
worked from his thesis the next year?  It doesn’t seem to be on
DSpace, unfortunately.  In his 01981 [“Character generation by
computer”][9], Coueignoux explains:

> 4. Contour Coding
>
> 4.1 Introduction
>
> Area coding methods [such as run-length coding] have the advantage
> of their simplicity. They fail for two main reasons. First, medium
> to large type sizes require too high a quality to be directly
> encoded for high resolution devices. Second, the flexibility
> provided is rather poor, which also limits the generation of larger
> type sizes. In either case, the lack of more global information is
> the culprit. Area coding is too interpreted, too close to the final
> grid. Since an area is just what is enclosed in a contour, one
> should resort to this higher level of definition. In this section,
> we first review differential run-length and chain-link
> coding. Spline coding is examined next. Original work done in this
> field is presented. [...]

In his doctoral dissertation, he describes the history of the field as
he saw it at the time, citing Mergler and Vargo’s work:

> Computer aided design reverses the priorities [from computer
> encoding of existing fonts as Hershey did, though he apparently
> doesn’t know about Hershey’s work] and first requires the digital
> code to be understandable, so as to be devised, input and modified
> by a type designer without any background in programming. No attempt
> in this direction was known to the author till the program had been
> completed. It is all the more striking, therefore, to see how close
> the program comes to be a natural continuation of the work done by
> H. W. Mergler and P. M. Vargo in 1968 at Case Western Reserve
> University [12]. The latter authors designed a system for 24 upper
> case letters, excluding the Q and the J. Although deemed important,
> serifs did not receive a full treatment and some finer details were
> omitted: absence of fillets to connect horizontal strokes to the
> main vertical stem in letters like L or D; no provision made for
> lopsided curve strokes as in D; oversimplification of the model for
> C, G, S; etc. Nevertheless, this attempt embodied the same line of
> thought which was at the origin of this work, and first demonstrated
> its practicality for computer generation of type fonts.

Later he elaborates on his previous work:

> Another attempt to encode general black and white planar pictures
> was made by the author in 1972, and especially applied to type faces
> \[24\].  Here too, the contours are encoded without human
> intervention.  Extrema in the horizontal and vertical directions are
> arbitrarily taken to break the contours into parts which can be
> approximated by splines of order three; straight segments and
> half-circles are recognized when part of the contours and encoded as
> such.

His reference “24” is of course his 01973 master’s thesis, which
unfortunately I don’t have a copy of.  (Maybe Knuth didn’t either;
it’s hard to imagine why he’d cite Mergler and Vargo’s 01968 work and
Coueignoux’s 01975 work without mentioning his 01973 work.)

So I think I can tentatively conclude that Coueignoux invented outline
fonts as we know them today, and the Alto Font Design System FRED the
next year was probably inspired by his work rather than being
independent reinventions.  Though Baudelaire’s short bibliography for
his 01983 talk (as republished in 02016 by *Visible Language*) doesn’t
cite Coueignoux or any other earlier work explicitly, it also doesn’t
claim it was the first, instead saying:

> The Alto font design system was probably one of the earliest fully
> operational system[s] using spline outlines.

But wait!  There may have been a third origin: URW founder [Peter
Karow’s system IKARUS][11] in 01972, based on spline-based
computer-aided design software in use at companies like Aristo.

Coueignoux’s list of target devices was interesting:

> a photodisplay flying spot processor, whose resolution can go to 60
> lines per mm; a dot matrix plotter, whose resolution is around 4
> lines per mm; a facsimile receiver, whose resolution is around 4
> lines per mm also; a television screen with an internal refreshing
> memory, whose digital grid pattern is 256 x 256, flicker free.

If we read “4 lines per mm” as “8 pixels per mm” (*i.e.*, 4 lines with
spaces between them) it works out to 203 per inch, within rounding
error of the 192 given above for the XGP.  The “facsimile receiver”
note is interesting; though Bain set up his first commercial fax
service in 01865, it didn’t really take off until [newspapers started
using it for wiring photos in the 01920s][10], still using Bain’s wet
electrochemical process.  So raster electrical printing was available
to universities before the XGP, and at about the same resolution.

[1]: https://dl.acm.org/doi/10.5555/892233
[2]: https://apps.dtic.mil/sti/citations/ADA083229
[3]: https://xeroxparcarchive.computerhistory.org/Xerox_PARC_source_code.html
[4]: https://historyofinformation.com/detail.php?id=970
[5]: https://journals.uc.edu/index.php/vl/article/download/5032/3896
[7]: https://journals.uc.edu/index.php/vl/article/view/5920
[8]: https://dspace.mit.edu/bitstream/handle/1721.1/27408/02149218-MIT.pdf?sequence=1&isAllowed=y
[9]: https://www.sciencedirect.com/science/article/abs/pii/0146664X81900393?via%3Dihub
[10]: https://en.wikipedia.org/wiki/Wirephoto
[11]: https://www.dutchtypelibrary.nl//PDF/Miscellaneous/Digital_Typography_and_AI.pdf
