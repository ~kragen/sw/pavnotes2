I wanted to know whether saturated fat (the kind that makes up most of
mammal and bird fat and tropical oils like coconut and palm oil) was
better or worse for you than unsaturated fat (the kind that makes up
most of fish oil and non-tropical oils of vegetable origin).  At this
point the evidence strongly suggests that, at least if you live in the
rich world, saturated fat is pretty bad for you, and unsaturated fat
is much less bad for you.

The Cochrane Collaboration is kind of the gold standard for medical
research review studies.  In my search I ran across this recent
open-access Cochrane review of only randomized clinical trials:

Hooper L, Martin N, Jimoh OF, Kirk C, Foster E, Abdelhamid AS.
Reduction in saturated fat intake for cardiovascular disease.
Cochrane Database of Systematic Reviews 2020, Issue 8. Art. No.:
CD011737.  DOI: 10.1002/14651858.CD011737.pub3.

<https://www.cochranelibrary.com/cdsr/doi/10.1002/14651858.CD011737.pub3/pdf/full>

They conclude that over the four or so years in the 16 RCTs they
reviewed, the decreases in all-cause mortality and cardiovascular
mortality due to lower saturated fat intake, if any, were too small to
detect with the usual p-values, but their measure of combined
cardiovascular events *did* decrease significantly, with a RR CI from
0.70 to 0.98.

On the face of it, this sounds kind of optimistic for the thesis that
saturated fat is fine: are heart attacks really that bad if they don't
increase your risk of dying?  Maybe there was some countervailing
health benefit, like you're more likely to die of a heart attack but
less likely to die of cancer or an accident.  And they have some
quotes with a remarkably low degree of hedging: "Critical importance.
Reducing saturated fat intake probably makes little or no difference
to cardiovascular mortality."

But I don't think it's actually that optimistic, for three reasons.
The numbers I've looked at so far don't suggest an actual protective
effect; the average death rates and cardiovascular death rates in the
intervention groups (with lower saturated fat) were still lower, just
not significantly so.  Presumably part of the reason for that is that,
almost necessarily, fewer experimental subjects had a heart attack and
died from it than merely had a heart attack.  But that would only
account for wider confidence intervals.

I think the stronger argument is that the studies were only for (an
average of) four years or so, and usually it takes about 50 years for
cardiovascular disease to kill you on a standard unhealthy "Western"
diet (and this is an aspect of "Westernization" we didn't get from the
Song Dynasty).  So it's really pretty alarming that lowering saturated
fat intake significantly reduces the "combined cardiovascular event"
rate over only four years.  Presumably there is progressive damage (or
recovery) if you continue the intervention for ten or twenty years.

The third argument is that a stroke that doesn't kill you can still be
pretty bad.
