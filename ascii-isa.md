i was chatting tonight with drakonis about
[uxn](https://100r.co/site/uxn.html), and I said that one of its flaws
was that it evaded efficient compilation, requiring interpretation and
thus a vast waste of the computing power it's hypothetically intended
to conserve.  I pointed at the SNOBOL Implementation Language as a
possible alternative: a set of some 130 operations which could be
defined as acceptably efficient assembler macros for the machines of
the day, in which SNOBOL4 was written.  I claimed that a problem uxn
and Forth share for this purpose is being stack-based, and that uxn's
limited address space is a major burden for little real benefit.

Being stack-based makes the abstract machine simpler, but it puts the
problem of register allocation on the virtual-machine implementation
rather than the programmer.  An implementation can always store some
of the virtual registers in RAM at fixed locations; a 6502
implementation probably wants to store most of them in the zero page.

Consider a instruction set for a fictious machine, optimized for human
readability (at least in a hex editor), intended for simple,
acceptably efficient but not optimal compilation to whatever native
code is convenient.  You'd like most of its fictious instructions to
compile to one or two machine instructions for the target.  And you'd
like it to mostly be higher-level than every target machine code,
because it's much easier for a simple compiler to expand out a single
fictious instruction to five real ones than to fuse two low-level ops
into a single instruction.

Still, maybe it should be RISC.  Most RISC is a three-address code,
and 32-bit instructions are common, so it's reasonable to use four
8-bit fields, or two 8-bit fields and a 16-bit field.  And we can put
printable ASCII bytes there when possible.

The register file
-----------------

Usually the fields in RISC instructions represent registers.  If you
have 32 registers, you can quite reasonably name them with a block of
32 ASCII characters: `@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_` seems more
promising than the alternatives.  So you might represent adding
register E to register F with the result in B as four ASCII bytes like
`B+EF`, `+BEF`, `BE+F`, or `EF+B`.  It might be better to have only 16
registers, like RV32E; 32 32-bit registers is 128 bytes, which
wouldn't fit along with any other fictious-machine state in a 6502 zero
page or in RV32I registers.  16 registers also permits choosing one of
several printable ASCII blocks to name them, such as `
!"#$%&'()*+,-./`, `0123456789:;<=>?`, `@ABCDEFGHIJKLMNO`,
`PQRSTUVWXYZ[\]^_`, and `` `abcdefghijklmno``.  This last block offers
the possibility of using the upper-case register names to indicate an
extra bit or two of opcode.

32-bit registers seem like probably the right compromise for a wide
range of applications; they can be implemented efficiently on 32-bit,
36-bit, and 64-bit machines, and they aren't too egregious on 16-bit
machines, though 8-bit machines like an AVR, Z80, or 6502 will feel
some pain.  32 bits is enough that you rarely have to worry about
overflow or splitting things up across address spaces, which are
constant hassles on 16-bit machines.

Register-register operations
----------------------------

Such a three-address format covers all the "R-type" instructions in
RISC-V without breaking a sweat:

    + add  - sub   [ sll 
    < slt  u sltu  ] srl   r sra
    & and  | or    ^ xor

You might choose to omit some of these or add a few others (especially
multiplication and abjunction), but they're pretty close to the basics
for arithmetic.

So you might represent `b = b + a; c = c + b` as the 8 bytes
`+bba+ccb`.  This is not super compact, but human-readable machine
code can't be.

But arithmetic, other than addition, is less central
to programming than copying data around, accessing memory, and
transferring control.

RISC-V handles copying data between registers with the `add`
instruction and a magic zero register.  For human use it is probably
better to use a separate `mv` opcode byte, like `=`; an instruction
might say `b= c` if we use the second byte as the opcode field, or `=b
c` if the first.  Having no magic zero register also may simplify your
compiler slightly if your target platform doesn't have a zero
register, and 16 is already a fairly small register file without going
to 15.

    = mv

Register-immediate operations
-----------------------------

RV23I includes six register-immediate instructions using a 12-bit
immediate, a source register, and a destination register; these are
`addi`, `slti`, `sltiu`, `andi`, `ori`, and `xori`.  It uses `addi`
with the zero register as a load-constant instruction.  I think you
could profitably implement these with a sign-extended 8-bit immediate,
though not, generally, a printable ASCII one.  It would still be
readable in a hex editor, though.

A human-readable way to encode these opcodes would be to reuse the
opcode byte for the non-immediate operation, but capitalize the
destination register.  So, for example, if `b = g + f` is encoded as
`+bgf` normally, we could instead encode `b = g + 102` as `+Bgf`.  The
capital version of the `` ` `` is `@`.

A 16-bit load-immediate instruction is likely also useful, and could
analogously use the same `=` opcode byte and a capital letter.  A
16-bit load-upper-immediate instruction would allow the loading of a
full 32-bit value in two instructions.

This suggests the following opcode mapping, with the asterisks
indicating the upper-case destination register:

    +* addi  =* li16   #* lui
    <* slti  u* sltiu
    &* andi  |* ori    ^* xori

That covers all the I-type instructions in RV32I except `jalr`, `lw`,
amd `lb`, plus `li16`.

Having a separate `lui` instruction may, with simple-minded compilers
incapable of op fusion, impair efficiency on architectures like amd64
where you can load a 32-bit immediate constant in a single
instruction.

PC-relative addressing and control flow
---------------------------------------

Current architectures like amd64 and RISC-V make heavy use of
PC-relative addressing so that code is position-independent by
default; you don't have to relink it every time you load it at a
different address.  Shared libraries on i386 bend over backwards to
keep the address of their Global Offset Table for global variables in
%ebx all the time, both losing a scarce register and introducing an
extra base register into those accesses.  Even on the 8086, jumps and
calls are PC-relative except when they're indirect, and on the 6502,
conditional jumps are.

Standard PC-relative jumps are extremely human-unfriendly; no two
references to the same address look alike, and you have to do
arithmetic on the offset field in your head to either write the code
or read it.  An appealing alternative that still gives some degree of
relocatability is page-relative addressing; if an 8-bit jump
destination field is 0x3a and the current PC is 0x8048ee20, that means
to jump to 0x8048ee3a.  Similarly for loading constants or pointers.
This is annoying when you insert or delete instructions and your old
instructions are now on a new page, but that's always annoying when
you're hand-editing machine code without a linker.  Some instructions
might instead have space for a 16-bit "chapter-relative" or even 24-bit
"volume-relative" address which permits references to more addresses.

Compilation means that the program-counter addresses of the fictious
machine will not be those of the target machine in any case; address
space with code in it will stretch and shrink in strange ways, and
because return addresses go on the stack, that cannot be hidden from
the program without adopting either some kind of static typing system
or expensive runtime type tagging.  So these page-relative or
chapter-relative addresses only survive up to compile time, when they
are converted to whatever form of addresses the target system prefers,
pointing to the real address of the generated code rather than the
fictious address of the fictious-machine code.

Constant data needs to be separated from instructions so it doesn't
get compiled, and also so that references to it from the program code
don't get converted in the above way.  This also means that constant
data containing code addresses, such as dispatch tables, either needs
to be tagged with some kind of linker relocation, or it needs to be
initialized at startup, similar to avr-libc copying initialized
non-PROGMEM data into RAM.  This is later-bound and probably fine
especially because it will normally immediately follow compilation.

So we need separate fictious instructions to load a literal constant
(RISC-V's `li` pseudo-instruction, the `=*` described earlier), a
pointer to code (its `la` pseudo-instruction), and a pointer to static
data (also `la`).  In a sense this makes our fictious machine a
Harvard architecture: we can freely put static data at the same
address as the code that uses it.

However, most of the foregoing discussion really only makes sense for
an actual hardware implementation of our fictious machine.  We are
supposed to imagine that a very simple-minded compiler is plowing
through the code at top speed, spraying out frothy native code for
amd64 or RISC-V or ARM or whatever, and when it encounters a backward
jump, it's supposed to somehow map the fictious address to whatever
address the corresponding native code is at?  That would require
either doing the compilation twice or maintaining an array of the
starting target address of *every fictious instruction emitted*.  And
it still involves headaches for the programmer: binary destination
addresses, page-boundary issues, and having to find all the references
to an address you had to move.

Far more justifiable is to have a `label` instruction which contains a
24-bit label; perhaps `:ext` defines the label `ext`.  As in gas,
there are special "local labels" which can be duplicated many times,
and a jump or load-code-address instruction must identify whether it
refers to the last previous occurrence or the first following
occurrence.  But in our fictious machine they are identified not by
being numeric but by beginning with `>`; for example, `:>tb`.  The
same syntax is used in an instruction to refer to a first-following
local label, but, for example, the last previous `>tb` is referred to
as `<tb`.

This all requires that the jump instruction, the call instruction, and
the load-code-address instruction have no other operands, including
link registers, registers to compare, or condition flags; all three
operand bytes are needed for the label.  It's probably fine not to
have a link register, since many platforms don't provide a choice of
stack pointers with the built-in call instruction, but we probably do
want a way to conditionalize jumps sometimes!  So we can use an
instruction prefix on the jump instruction for that; on some targets
it will compile to a separate instruction or two.

RISC-V has six conditional branch instructions: `beq`, `bne`, `blt`,
`bltu`, `bge`, and `bgeu`.  Each takes two registers as arguments.  I
think these can reasonably be written as `?` followed by a byte
to indicate the test: `?=`, `?!`, `?<`, `?u`, `?g`, and `?G`, say.

So this gives us something like this:

    :foo  define label foo
    jfoo  jump to label foo
    j<no  jump to last preceding label no
    j>no  jump to first following label no
    cfoo  call label foo
    Ca    call address in register a
    Ja    jump to address in register a
    r     return
    ?=fg  skip following jump unless register f == g
    ?!hk  skip following jump if register h == k
    ?<do  skip following jump if register d >= o signed
    ?udo  same, but unsigned
    ?gdo  skip following jump if register d < o signed
    ?Gdo  same, but unsigned

We still have no instruction for loading a code address, because where
does it go?  A shitty design is to use a prefix instruction again:

    'foo|  m  store address of label foo in register m

There are 16_777_216 24-bit labels and 857_375 printable ones, so this
is probably fairly adequate for programs of up to a few million lines.

Hardware implementations could treat the label instructions as nops
like amd64's endbr64 instruction and use a loader that overwrites the
24-bit label field with a 24-bit offset measured in 4-byte units, thus
providing direct jumps to anywhere within a 64-mebibyte range.  Larger
jumps could go through a PLT that loads the absolute destination
address into a register and jumps to it.

Threading, exceptions, and stack allocation
-------------------------------------------

Somehow you need to be able to switch stacks and do nonlocal returns.
How to do this portably I don't know.  On RISC-V the stack pointer is
just another register and you can index off of it for recursive local
variables.  Probably that's the right solution: declare that call and
return instructions use, say, `a` as the stack pointer.  This
prejudices 6502 implementations I guess; they have to store the return
address somewhere that isn't the stack page.

Memory access
-------------

RISC-V has `lw`, `sw`, `lb`, and `sb`; in Forth these are `@`, `!`,
`c@`, and `c!`, but the RISC-V instructions include a 12-bit signed
offset.  As with the register-immediate instructions we can reduce
this to 8 bits.  The offset field of the instruction has 64 subtracted
from it before being used, so `@` represents 0, `A` 1, `?` -1, and so
on.  XXX apply that to register-immediates too.

    @jeD  lw  load a word from 4 bytes after where register e points into j
    !jeD  sw  same, but storing j into RAM
    bjeD  lb  same, but loading a byte
    BjeD  sb  same, but storing a byte

Eventually they also added `lh` and `sh`, load and store halfword, and
`lbu` and `lhu`, which don't sign-extend.

Missing
-------

If we take RV32I as our criterion for what is needed, we're missing
`fence`, `ecall`, `ebreak`, and a number of pseudo-instructions that
use its magic zero register: `neg`, `seqz`, `snez`, `sltz`, `sgtz`,
`beqz`, `bnez`, `bltz`, and `bgtz`.  It might be worth it to add
fictious instructions for at least some of these rather than trying to
do op fusion by recognizing when a register is getting set to zero to
implement one of them.

Also I never defined a XXX instruction for loading a data address.

Opcodes
-------

Instructions:

    +  add    -  sub     [ sll 
    <  slt    u  sltu    ] srl    r sra
    &  and    |  or      ^ xor
    =  mv
    +* addi   =* li16   #* lui
    <* slti   u* sltiu
    &* andi   |* ori    ^* xori
    :  label  j  jump    c call (jal)
    r  ret    J  jr      C jalr
    @  lw     !  sw
    b  lb     B  sb
    |  move label address

Prefixes:

    ?= if eq  ?! if ne
    ?< if lt  ?u if lt unsigned
    ?g if ge  ?G if ge unsigned
    '  code address

37 opcodes in all, including each skip prefix, and nearly complete.
And it seems like it ought to be trivially implementable with
reasonable efficiency on all current and many obsolete CPUs.

One thing that's missing is the call to load a new block of code.

A CISCier, more free-form approach
----------------------------------

So, I think the above is wrongheaded.  If our objective is simple
compilation to reasonably efficient code for a variety of machines, we
probably want to provide CISCier ops.  3-operand instructions, rather
than 0-address, 1-address, or 2-operand instructions, and prefixes for
conditional jumps, are a good start, because they mean that instead of
writing something like

    mov %r8, %r9
    add %r10, %r9

we can write

    add %r9, %r8, %r10

which can be easily converted into the former on a 2-operand machine,
while doing the reverse is harder.  And inferring which fictious flags
are significant to later jumps is a significant problem, so a
conditional prefix semi-instruction for jumps is better.

However, the operand-size restrictions above serve no real purpose in
this context, especially for loading immediate constants; they will
require the programmer to split a single constant across two or more
instructions which the compiler then must merge.  And the paucity of
addressing modes has a similar effect: if you have a fictious
base+index+offset mode, it's easy to convert that to a series of
separate instructions with a temporary register on a weaker machine,
but if the target machine has a base+index+offset addressing mode,
recognizing a fictious instruction sequence that builds it up demands
more complexity from the compiler.  Similarly for autoincrement and
autodecrement modes like those present on the PDP-11, the 68000, and
the MSP430, and memory-indirect modes like those present on the PDP-8,
the PDP-11, and the MSP430.

With that in mind, maybe what's needed is not really an *instruction
set* at all but a *cross-platform assembly language*. See file
`xpasm.md` for some exploration of that theme.
