Some notes on a text file format for nested table layout.

Text formats have some nice advantages: you can copy and paste them,
they can be straightforwardly edited as a linear sequence of
characters, they are very fast to render, etc.  But historically they
don't do very well at sophisticated layouts.

Emacs control-character-delimited formats
-----------------------------------------

Emacs uses some weird control characters for delimiters.

In TAGS a `^L` separates sections for different source files (unlike
ex/vi tags files, which are TSV, Emacs TAGS files aren’t sorted by
identifier).  Each section has a header and zero or more identifier
records, separated by `^J`.  In the header, a “,” separates the filename
from a byte-length field.  Each identifier record contains an excerpt,
an identifier, a line number, and a byte index.  A DEL (byte 7F)
separates the excerpt from the identifier defined, and a `^A` separates
the identifier from a linenumber,bytenumber pair which says where to
find the identifier.

In Info files `^_` separates pages, but the rest of the markup is
printable ASCII.

In BABYL files <https://docs.python.org/3/library/mailbox.html#babyl>
<https://quimby.gnus.org/notes/BABYL> similarly messages are enclosed
within `^L` to begin and `^_` to end.  At the beginning of the file there
is a Babyl Options section that is also terminated with `^_`.  The first
line of each message is a comma-separated list of tags.

Elastic tabstops
----------------

<https://nickgravgaard.com/elastic-tabstops/> proposes using tabs to
terminate columns on each line, with some rightmost text that is to
the right of each column, and a minimum width (or maybe separation) of
1 between columns, so that a tab always displays as some nonzero
amount of space.  A column ends when we reach a line without enough
tabs on it.

Control character choice
------------------------

Encouraged by the BABYL example I think I should use `^L` and `^K` for
table-cell nesting; `^K` (VT) is conventionally considered whitespace,
resulting in a sort of graceful degradation when the tables are
handled by non-nesting-aware tools.  (`^_` is an alternative if `^K` is
too painful as a keybinding.)  Encouraged by the Elastic Tabstops
example I think I should use `^I` to terminate table cells (rather than
merely separate them), with trailing text not being placed in a table
cell, and `^J` to separate lines; but I do not think that the format
should impose a minimum width or spacing.  (You can use explicit
spaces for that.)  Encouraged by tbl, I think I should have magic
cookies in certain cells to indicate that the cell to the left or
above should span into the current cell; `^H` is the most reasonable
choice for spanning from the cell to the left, and `^M` is the most
reasonable choice for spanning from the cell above.  `^M^M` might be
an alternative too `^H` with the advantage of being conventional
whitespace; the logic of `^H` is that conventional teletype overstriking
is not a sensible thing to do at the beginning of a cell, and you’re
sort of erasing the cell boundary.

Videotex NATS and IPTC used `^L` as “start of instruction” and `^K` as
“end of instruction”, enclosing a “typographical instruction intended
for the typesetting device”, so there is some precedent for this.
They also used `^N` and `^O` to start and end, respectively, an
“emphasized region of text”.

### Nesting ###

One of the general problems with conventional Unix ad-hoc text files
is that they can’t reasonably talk about their own data because they
can’t nest, and so spaces or newlines in filenames expose lots of bugs
in shell scripts.  Similarly commas in users’ natural language names
or colons in their login names or home directories.  This gives rise
to error-prone exponential growth of escape characters:

    $ echo echo echo "\\\\\\\"" | sh | sh
    "

HTML’s rules, quopri rules, and URL-encoding rules only suffer from
linear growth, but you still have to get the number of decoding passes
right all throughout the string when you say &amp;amp;amp;, =3D3D3D,
or %252525.

This also creates lots of shell injection, SQL injection, and XSS
(HTML injection) vulnerabilities.

By using balanced nesting delimiters instead, with a quoting
convention to escape the odd unbalanced one that merely represents
data rather than potential structure, these problems are avoided; any
validly parsable string can be nested simply by adding another pair of
nesting delimiters without any internal rewriting.

Tree structure
--------------

The nesting of tables forms a tree structure in which each node is
either a string containing no unescaped
`^H`, `^I`, `^J`, `^K`, `^L`, or `^M`, or a child
table, and each arc from parent to child is labeled with an (x, y, w,
h) tuple.  (XXX does not take into account trailing text.)  (XXX maybe
some of these delimiters could survive unquoted.)

Linear-time batch layout algorithm
----------------------------------

The layout algorithm proceeds bottom-up, assigning each node a minimum
width and height which will be used to lay out its parent.

In the leaf nodes the width and height come from some font rendering
algorithm.  In the simplest case the width is the number of characters
multiplied by the font width and the height is the font height.
Suppose in the below that these are measured in pixels.

To lay out a table node, first a set of rows and columns is produced.
Each `^J` terminates a row, and the rows are processed in order to
produce the set of columns.  Before processing the first row, the
columnset is an empty list.  On each row, each table cell i is
appended to column i of the columnset; if there is not yet a column i,
an empty column is appended to the columnset.  Each newly created
column in the node is assigned a column id number, unique within the
node, starting at 0 for the first column created and counting upwards.
At the end of the row, any columns in the columnset to which no cell
was appended in that row are removed from it.  Until some columns are
thus removed, i and the column id are equal, but afterwards the column
id may be greater than i if a new column begins.

Each table cell thus created has a start row r0, a starting column id
c0 (which is not the i value, but the id), an ending column id c1
which is the first column id after the table cell ends, an end row r1
which is the first row after the table cell ends, a minimum width w
(in, say, pixels), and a minimum height h.  

(XXX we need to assign a column id to the ragged text on the right, so
that c1 doesn't refer to some other column to be created later, and
also store it somewhere; maybe just make a column there, with possibly
empty contents, and terminate and start a new one if that column stops
being ragged?)

From this we can derive two layout constraints:

    x[c1] ≥ x[c0] + w
    y[r1] ≥ y[r0] + h

By processing all the table cells, we accumulate many such
constraints.  In batch-mode layout, for duplicate (c0, c1) pairs we
can retain only the most stringent constraint, the one with the
largest w, and for duplicate (r0, r1) pairs we can retain only the one
with the largest h.  This gives us two matrices w[c0, c1] and h[r0,
r1].

To find the smallest solution to these constraints we set x[0] = y[0]
= 0.  To lay out the columns, we iterate for j from 1 to the number of
columns, setting x[j] = max(x[k] + w[k, j] for k < j where w[k, j] is
defined), and do the analogous computation for the row layout.  This
gives us an (x, y) position for each table cell.

This algorithm completes in worst-case time linear in the number of
table cells, because each table cell can add at most one column, one
row, one w[c0, c1] entry, and one h[r0, r1] entry.  Efficient handling
of sparse constraint matrices is required for large texts, but
probably SIMD instructions can accelerate it by a significant constant
factor of over an order of magnitude in small cases.

This amounts to finding a least fixed point of a sparse
upper-triangular matrix in the max tropical semiring (the max-plus
algebra).

The minimum width of the whole node is given by laying out the extra
ragged text on the right and taking the width of the widest row.  Its
minimum height is just the y-coordinate of the row following the last
row.  To these we may add some optional padding.

Given enough memory to store the layout, this algorithm can be
executed in two passes over the text, one in which the constraint
matrices are computed, with the widths and heights computed at the end
of each nested cell, and a second one in which the text is drawn at
the locations computed.

Name-value pair escape sequences
--------------------------------

Traditionally text on terminals could be augmented by mode shifts of
various kinds that did not themselves display but that changed the
display of subsequent text; for example, `^[[32m` on a color ANSI
terminal sets the foreground color to 2, green, `^[[1m` turns on
boldface, the FIGS and LETS characters in Baudot code cause subsequent
characters to be interpreted as numbers or letters, and the SO and SI
characters in ASCII have a similar effect on some terminals, switching
between alphanumeric and graphical characters.  These escape sequences
are unfortunately rather arcane, and their effects sometimes escape
from their intended scopes.

The “C1” control characters defined in ECMA-35 and ISO 2022 are
defined to be equivalent to `^[@`, `^[A`, ... `^[Z`, `^[[`, ` ^[\ `, etc, and in
particular the `^[[` used in the ANSI escape sequences above is “CSI”,
Control Sequence Introducer.  This convention in particular does not
define `^[(`, `^[<`, `^[#`, or any escape sequences beginning with lowercase
letters.

So I think a better alternative is to use `^[<property value>` to set
some arbitrary “property” to an optional “value” in the subsequent
text.  To delimit the extent of effect of such sequences, we can use
`^K` and `^L` as above.  Properties may affect text display:

    ^[<b>
    ^[<font Palatino>
    ^[<size 1.4>
    ^[<fg #f9c>

Or layout:

    ^[<padding 3 6 0 9>
    ^[<pos nw>
    ^[<border 2 dotted torn>

Or provide some other metadata potentially useful for some kind of
interaction:

    ^[<class username>
    ^[<url http://canonical.org/>
    ^[<table busroutes>
    ^[<date 020220410 185353Z>
    ^[<latlon -31.513501 37.175353>
    ^[<dir /usr/src/com>
    
As long as we’re appropriating ESC, we might as well also use it for
escaping unmatched `^K` and `^L` delimiters.

Paginated or per-column layout algorithm
----------------------------------------

If you have some arbitrarily large text, it is desirable to have a
better bound than linear time, and in particular it is desirable to
not have to even load parts of the text you aren't displaying.  Since
the algorithm described above doesn't wrap the text, it seems
reasonable to only lay out a page of it at a time.

For this purpose 
