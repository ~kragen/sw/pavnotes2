xHow big of a garden do you need to survive?

Let’s start with a very crude approximation: a person needs 2000 kcal
per day, including at least 80 grams of protein (320 kcal) and at
least 1400 kcal of non-protein energy; the protein needs to be between
16% and 30% of the total energy balance.  Obviously there are lots of
diets that meet those requirements but lack other essential nutrients
or are unacceptably unpalatable, but let’s start by seeing how much
land we need to meet those most basic requirements.  Instead of
interpreting the answer as “if you have this much land you can
survive, and this is how you should do it” we should interpret it as
“if you have less land than this you are almost certainly going to
starve to death no matter what you do”.

Commercial corn yields are around [9 tonnes per hectare][0] per year,
ranging from almost 6 to almost 13.  Commercial soybean yields are
around [2.8 tonnes per hectare][1], ranging from almost 1.5 to almost
4.0.  It’s usually possible to get higher yields with biointensive
cultivation, but it’s also easy to get lower yields.  I’m pretty sure
tonnes are measured dry for both of these, and that these are per year
rather than per harvest.

[Navajo corn contains][2] 5.2 grams of fat, 75 grams of carbohydrate,
and 9.9 grams of protein per 100 grams.  [35 grams of “corn grits
polenta”][3] (a contradiction, since grits are by definition
nixtamalized and polenta by definition isn’t) contain 0.5 grams of
fat, 27 grams of carbohydrate, and 3 grams of protein, working out to
1.4g fat/100g, 77g CHO/100g, and 8.6g protein/100g, which is pretty
much the same except for the fat, so I’ll use the Navajo corn numbers.

100g of [dry roasted soybeans contain][4] 21.6 grams of fat, 32.7
grams of carbohydrate, and 39.6 grams of protein.

Multiplying these out, per hectare per year, corn gives you 470kg of
fat, 6800kg of carbohydrate, and 890kg of protein (10% of calories
from protein).  Soy gives you 600kg fat, 920kg carbohydrate, and
1100kg of protein (32% of calories from protein).  So soy contains too
much protein, corn too little.  But corn gives you a lot more calories
per hectare (35 million rather than 13 million, or per day, 96000
rather than 37000), so using as little soy as possible to get up to
16% protein will result in a smaller land footprint.

If we multiply out the 80g protein per day, that works out to 29kg
protein per year, which could be entirely satisfied from 260m² of soy
or 325m² of corn.  The absolute minimal mix where you have enough soy
to get your protein up to 16% of calories is when the soy fraction *s*
of your calories fulfills:

> .16 = .32*s* + .10(1 - *s*)  
> .16 = .32*s* + .10 - .10*s*  
> .06 = .22*s*  
> .06/.22 = *s* = .27

So if 27% of your calories (540kcal/day) come from soy, 32% of that
27% is 8.6% of your calories from protein, and with the other 73% of
your calories (1460kcal/day) coming from corn, 10% of that 73%, or
7.3%, is protein.  So you get 15.9% of your calories from protein,
which is a small rounding error off from our survival goal.

But how much land is that?  Soy’s 37000kcal/day/hectare is
3.7kcal/day/m², so 540kcal/day is 146m².  Corn’s 96000kcal/day/hectare
is 9.6kcal/day/m², so 1460kcal/day is 152m².  So, under these
assumptions, you need 298m², roughly evenly divided between corn and
soy.  (In practice, companion planting might be a better idea than
separate plots.  And actually you’d probably want a wider variety of
foods, etc.)

Let’s see if I screwed something up.  152m² of corn producing 9
tonnes/hectare (900g/m²) per year gives you 137kg of corn in a year,
containing 7.1kg of fat, 103kg of carbohydrate, and 14kg of protein.
146m² of soy producing 2.8 tonnes/hectare (280g/m²) per year gives you
41kg of soybeans in a year, containing 8.9kg of fat, 13kg of
carbohydrate, and 16kg of protein.  In total your yearly diet is 16kg
of fat, 116kg of carbohydrate, and 30kg of protein.  Per day this
works out to 44g/day of fat (providing 390kcal), 320g/day of
carbohydrate (providing 1270kcal), and 82g/day of protein (providing
330kcal).  We’re up to 1990kcal/day, down by a 1% rounding error.
Plenty good enough.

[0]: https://farmdocdaily.illinois.edu/2022/03/international-benchmarks-for-corn-production-6.html
[1]: https://ag.purdue.edu/commercialag/home/resource/2022/03/international-benchmarks-for-soybean-production-2022/
[2]: https://www.nutritionvalue.org/Corn%2C_dried_%28Navajo%29_nutritional_value.html
[3]: https://www.fatsecret.com/calories-nutrition/bobs-red-mill/corn-grits-polenta
[4]: https://www.fatsecret.com/calories-nutrition/usda/dry-roasted-soybeans-(mature-seeds)?portionid=61051&portionamount=100.000

Buying instead of gardening
---------------------------

A somewhat surprising aspect of this is that 178kg of corn and soy
(23% soy by weight) will pretty much feed you for a year.  That’s
about 180 liters, only nine 20-liter buckets.

If you have access to markets, because you aren’t the Lykov family, it
also costs an amazingly small amount.  [CME’s corn futures ZCN4][5]
currently quote at “450'2”, which I believe is 450¼¢ per “bushel” of
[#2 yellow corn][6], [54 “lbs” per “bushel”][7] with 5% max damaged
kernels and 3% max foreign material and [not more than 5% other colors
of corn][8], and does not contain stones with an aggregate weight in
excess of 0.1 percent of the sample weight, nor 2 or more pieces of
glass, nor 3 or more crotalaria seeds (*Crotalaria spp.*), nor 2 or
more castor beans (*Ricinus communis L.*), nor 4 or more particles of
an unknown foreign substance(s) or a commonly recognized harmful or
toxic substance(s), nor 8 or more cockleburs (*Xanthium spp.*), nor
similar seeds singly or in combination, nor animal filth in excess of
0.20 percent in 1,000 grams; nor a musty, sour, or commercially
objectionable foreign odor.

Anyway, with all that out of the way, 450¼¢ per “bushel” is 18.4¢/kg,
and 137kg will run you US$25.18 at these prices.

Soybeans are also traded on CME, and [right now ZSN4 is quoted at
1178'2][9], which I think is 1178¼¢ per “bushel” of #2 yellow soybeans
(maximum 14% moisture), which in this case is [60 “lbs”][10], so this
works out to 43.29¢/kg, making 41kg cost US$17.75.  ([Trading
details][11], [standards][12].)

So your food budget on the world wholesale market would be about
US$43/year per person.  At retail you can’t get quite this low but you
can usually get within a factor of 2 or so.  It’s pretty bad here in
Argentina at the moment, though, because we’re in the grips of the
worst economic crisis in half a century; [400g of Elio brand “maíz
pisado blanco”][13] is $543 = US$0.54, which works out to US$1.35/kg,
and [730 grams (‽)  of instant polenta from the super-shitty Arcor
brand][14] is $1900 = US$1.90, US$2.60/kg.  (The non-instant kind used
to be cheaper, but it got price-controlled by the government years
back, so nobody sells it anymore.)  Soybeans are more reasonable,
[$5400 for 5kg][15], which works out to US$1.08/kg.

[A 35kg bag of yellow corn sold as chicken feed][16], however, costs
$15000 = US$15, 43¢/kg, from *Forrajería Locos por el Campo*.  So it’s
really just the supermarket supply chain that’s that screwed up in
this life-threatening way.  However, I wouldn’t bet my life on that
feed bag meeting USDA #2 yellow corn specs for stones, broken glass,
and castor beans.

[5]: https://www.cmegroup.com/markets/agriculture/grains/corn.html
[6]: https://www.cmegroup.com/markets/agriculture/grains/corn.contractSpecs.html
[7]: https://agrocommodityasia.com/shop/wholegrainsandseeds/corn/us-yellow-corn-grade-2/
[8]: https://www.ams.usda.gov/sites/default/files/media/CornStandards.pdf
[9]: https://www.cmegroup.com/markets/agriculture/oilseeds/soybean.quotes.html#venue=globex
[10]: https://www.canr.msu.edu/news/understanding_soybean_discount_schedules
[11]: https://www.cmegroup.com/content/dam/cmegroup/rulebook/CBOT/II/11/11.pdf
[12]: https://www.federalregister.gov/documents/2006/09/06/E6-14719/united-states-standards-for-soybeans
[13]: https://www.aehmall.com.ar/MLA-1696281744-maiz-pisado-blanco-elio-legumbres-400-g-_JM
[14]: https://www.mercadolibre.com.ar/polenta-instantanea-harina-de-maiz-presto-pronta-arcor-730-gr/p/MLA22308409
[15]: https://articulo.mercadolibre.com.ar/MLA-917579651-poroto-de-soja-de-5-kilos-_JM
[16]: https://articulo.mercadolibre.com.ar/MLA-849684324-maiz-entero-bolsa-x-35-kg-zona-norte-gba-_JM
