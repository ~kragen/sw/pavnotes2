When you’re recognizing a letter or other glyph written on a
touchscreen with a Graffiti-like unistroke recognizer, there are a
variety of slight variations you have to tolerate.  The position
varies somewhat, of course, and so does the rotation, the size, the
relative sizes of different parts of the stroke, the speed, the
details of the stroke, and so on.

It occurred to me that a relatively simple way to untangle most of
these is by treating each point in the stroke as a number in the
complex plane, then taking the Fourier transform of the stroke as a
whole.  The dc component, which is the mean, gives the position of the
stroke, rotation is just a change in phase, the size is just the
amplitude (which can be normalized away), and the details are mostly
high-frequency components.

The simplest way to normalize the rotation is by rotating the stroke
to put the initial touch point at a fixed angle, such as zero, but
this means that any noise on that initial point will be added to every
subsequent point in the stroke, which could make it the dominant
source of noise.  Maybe a better way is to subtract the dc component,
square the thus-centered points, and take the mean of the squares, to
get an average “major axis angle”, which of course must then be halved
if you’re going to do the rest of your work with unsquared points.

Each positive and negative frequency component of the Fourier
transform has an amplitude and phase described as a complex coeffient
that multiplies the corresponding complex exponential.  In a typical
case, a stroke might consist of on the order of 64 or so points
sampled uniformly in time, which will give rise to 64 such complex
coefficients; I suspect that mostly these will be pretty sparse, with
most of the energy concentrated in a few frequency components, mostly
the lower ones.

Because most strokes don’t begin and end at the same place, there’s a
giant artifactual discontinuity where Fourier analysis artificially
joins the start and end of the stroke together, a step which is likely
to contain a large fraction of the energy of the stroke.  A way to
suppress this artifact is the boustrophedon wavetable approach taken
to suppress clicks in MOD players for hand-drawn waveforms: instead of
just playing the wavetable forwards repeatedly, the MOD player
alternates between playing it forwards and backwards, so that
following the last sample in the wavetable, you have the next-to-last
sample instead of the first sample.  This eliminates the discontinuity
and also makes the wave an even function, like cosine or an
even-degree monomial.  (I’m not sure what that means when the
time-domain points are themselves complex-valued, though.)

I was thinking about principal components analysis on the Fourier
coefficients as a way to reduce the dimensionality of the data
further, enabling more efficient search.  But Fourier analysis is a
linear transformation, so I think the principal components of the
Fourier coefficients will just be the principal components of the
(centered, normalized, and derotated) original points.  This suggests
that PCA over a sample set of strokes might be actually a full
*alternative* to Fourier analysis for this purpose, and that might be
an improvement.

If you cluster together a bunch of unistrokes in such a feature space,
you can maybe guess when the same letter has been written many times,
and come up with an “ideal stroke” for the letter which will be,
probably, smoother and more flowing than each individual instance of
the letter.  You can maybe tween individual instances between this
ideal and the shape that was actually drawn, improving them by
reducing noise.
