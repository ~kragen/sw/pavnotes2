For ghettobotic projects it's often desirable to connect new
electronics to things that can't be soldered to, or only with super
soldering setups we don't have: carbon arc electrodes, steel,
flat-flex cable, aluminum PCBs, LCDs, aluminum foil, etc.  There are
purpose-made products for this, like copper adhesive tape and zebra
strip, but one of the most interesting to me is conductive epoxy,
which is epoxy filled with copper, silver, graphite, or gold.

In theory it should be pretty easy to make with a filler powder of
graphite or copper and some kind of binder.  [Rosie Research][1]
reports success with graphite powder and Elmer's Glue-All, which is a
PVA glue, and which she says works better than the acrylic paint some
others use.  "Add in a lot more graphite than glue, okay?" she says.

[1]: https://youtu.be/0TLHl_vBflQ

Finely divided copper should be easy to make electrolytically if you
have some bulk copper; just try for dendrites with massive overvoltage
and zap the cell whenever you get a short, maybe?  [Ranjit Bauri at
IIT Madras says][-i] high current densities (300-4000 A/m²), low ion
concentrations (30g/l of copper), low pH (150-250g/l of oil of
vitriol), and colloidal admixtures in the bath help to get porous
powders, but you only need 1-2 volts, and typically you use a sulfate
electrolyte, a lead-antimony cathode, and temperature of 40-60°.  See
also file `iron-isru.md`.

[-i]: https://youtu.be/2IHhIEfzoOo "Powder Fabrication Methods, NPTEL Online Course at 7m31s"

It occurs to me that it might be best to use binders like
carboxymethylcellulose, sodium polyacrylate (carbomer), and gelatin
that can work at fairly low concentrations in the solvent, because you
don't care how strong the dried glue is but you want the particles to
be in as much contact as possible.  Even diluting PVA glue with water
or alcohol may be useful.

[Mauricio Cordova of the Makerboat open design lab][i] reports success
with carbon obtained by putting selected fireplace charcoal in a
blender for ten minutes, which wouldn't work as well for copper.  Some
pieces had high resistance as measured by a multimeter, so he selected
the pieces under 100 ohms.  He decanted supernatant with a syringe to
avoid getting the particles floating on top.  Black acrylic paint was
one binder, 1 teaspoon in a glass (5ml paint, 100ml charcoal?), plus a
similar quantity of clear Elmer's glue (polyvinyl alcohol IIRC).

[i]: https://youtu.be/W_ouYLeIkoo "$1 DIY Conductive Ink and Paint (Non Toxic, homemade, cheap!)"

Alternative binders might include waterglass, wheat paste and similar
starches, carrageenan, sodium alginate, konjac, agar-agar, xanthan
gum, guar gum, gum arabic, gellan gum, latex paints other than sodium
polyacrylate, egg yolk, and so on.  It might be possible to use a
hot-melt binder such as EVA, or a crosslinking binder such as epoxy
resins, but these have the disadvantage that the volume fraction of
conductive solid particles will be the same in the finished solid as
in the flowable liquid.  But if the volume fraction of solids in the
liquid is too high, it will jam instead of flowing, and we want as
high a volume fraction of conductive material as possible in the
solid.

I'm told that agar-agar is the best gel-former of these, with a
critical gelation concentration of about 0.1% in water, though I
haven't found a table of CGCs.  It costs about US$25/kg; so a
plausible recipe might be:

- 40 g electrolytically powdered copper
- 5 g water
- 5-50 mg agar

After the water evaporates, the result should be 99.9-99.99% copper,
and hopefully the agar will be enough to keep it solid without
interfering with conduction.

Copper is a pretty good antibacterial, but with certain other
conductive fillers, such as carbon, it might be necessary to add some
kind of antibacterial such as blue vitriol to keep the conductive glue
from rotting.

[Pekcan and Kara 02012][0] list CGCs for gellan, alginate, and
(kappa?) carrageenan in one of their figures; they are all in the
range of 0.25% to 0.8% (w/w), often getting lower when potassium and
especially calcium ions are present.  In their table 2, they list
sol-gel transition temperatures for carrageenan varying from 27.9° and
48.5° at 1% up to 52.3° and 72.8° at 5%; the higher temperature for
each concentration is where the gel melts back into a sol, and the
difference is hysteresis.  So the CGC depends strongly on temperature,
counterion availability, and hysteresis.

[0]: https://www.researchgate.net/publication/258682829_Gelation_Mechanisms

But probably any of these organic gums would work in place of agar,
maybe at slightly higher concentration.

Non-water-based solvent-based adhesives are also an option.  Spirit
gum is pine resin dissolved in ethanol, and shellac is commonly also
dissoved in ethanol, but other alternatives include polystyrene
dissolved in all kinds of nonpolar organic solvents, PVC dissolved in
acetone, or rubber cement.  The metallic-ink pens I've seen all use
non-water-based solvents, and there may be a good reason for this,
like a strong tendency for metal particles to flocculate in water or
something.

Alternative conductive fillers include other solid metals such as
aluminum, gold, silver, nickel, tin, and lead, and their alloys.  Iron
is the cheapest, but its ferromagnetic nature will make it very high
impedance for high-frequency signals, which limits its uses in
assembling circuits; nickel has the same drawback.  Graphite is a
semiconductor rather than a metal; other solid semiconductors such as
silicon, silicon carbide, zinc oxide, indium tin oxide, indium zinc
oxide, indium oxide, gallium tin zinc oxide, indium gallium zinc
oxide, titanium dioxide, aluminum-doped zinc oxide, cuprite, or
zirconia might be interesting alternatives.

It's potentially interesting to include two separate binders, a fast,
weak one and a slow, strong one.  For example, if the fast binder is
something like carboxymethylcellulose, the slow binder might be
something like quicklime or portland cement.  (I forget if CMC
gelation is tolerant of high pH, but if not, a different binder would
work.)  The amount of slow binder can be quite small in the watery
mix but quite substantial once the mix is mostly dry.

A small amount of copper particles in a glue, not enough to make it
conductive, would make it bacteriostatic and probably also resistant
to insects.
