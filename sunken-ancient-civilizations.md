I think it's likely that there weren't any major civilizations older
than [Göbekli Tepe](https://en.wikipedia.org/wiki/G%C3%B6bekli_Tepe),
but it's only ten or twelve thousand years old. But [behavioral
modernity](https://en.wikipedia.org/wiki/Behavioral_modernity) goes
back at least 40_000 years.

You can totally presume that innovations like agriculture, writing,
pottery, etc., took a long time to arise; but the weird thing is that
they seem to have arisen almost simultaneously in widely separated
populations. We know of at least three inventions of writing
(cuneiform, hanxi, Mesoamerican) and possibly two more (hieroglyphics
and khipu), and all 3–5 of them happened in just the last 6000
years. That is, for the first 34_000 years or more of behavioral
modernity, nobody invented writing, and then on 2–4 separate
continents we had 3–5 independent inventions of writing within only
6000 years. And the situations with agriculture and pottery are
somewhat similar—though there are more likely routes for the
innovations to diffuse, because the inventions are older, we don't
have direct evidence that they actually did diffuse.

A possible place for the missing artifacts to be hiding is a short
distance offshore, due to the [rise of sea levels since the last
glaciation](https://en.wikipedia.org/wiki/Sea_level_rise#/media/File:Post-Glacial_Sea_Level.png). [Atlit
Yam](https://en.wikipedia.org/wiki/Atlit_Yam) was founded almost 9000
years ago, and it has been preserved under ten meters of water for
millennia in a heavily populated area but wasn't discovered until 60
years ago. 9000 years ago sea levels were 20 meters lower than they
are today; 15000 years ago they were almost 80 meters lower. If there
had been port cities 15000 years ago they might be under 70 meters of
water today.

But it's unlikely, because surely goods they traded with inland
peoples would have survived in the archaeological record, and it seems
unlikely that the art of writing would have been entirely lost.
