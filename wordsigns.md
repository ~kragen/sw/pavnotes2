I had a dream this morning in which I was using a 50-year-old computer
with a strange keyboard.  Above each letter key was a word key for a
word starting with that letter, except that “the ” was placed on the
“d” key.  The result was a substantial increase in typing speed with a
relatively light extra learning load.

Perhaps this was a memory from reading manuals for Timex-Sinclair
BASIC in my childhood; the ZX-81 came out 42 years ago and used
shifted letters for BASIC keywords.  But the computer in my dream was
a mainframe.  (Interestingly, the Czerweny clones of the Sinclair are
still abundant on MercadoLibre, commanding US$150-200.)

Could this work in practice to type English faster?  Suppose that,
instead of doubling the number of keys, we use an extra shift key,
similar to Alt, which combined with a letter produced a word and a
following space.

This turns out to plausibly save only about 10% of keystrokes. Simple
tweaks that seem plausible to master in a few months of daily focused
practice seem like they would get to 29% keystroke reduction, which
would be a 40% increase in words per keystroke.

I think that while learning a few hundred wordsigns (and bigram signs)
to speed up your typing of English by 40% might be a reasonable
approach in the absence of alternatives, likely installing Plover and
buying an NKRO keyboard would be a better tradeoff.

Assignments for single-letter (two-keystroke) wordsigns
-------------------------------------------------------

The simplest assignment would be the most common word starting with
that letter, except for one-letter words and “the”, which would be on
“d”.

This simple procedure gives the following terrible assignment, using
word frequencies from the British National Corpus:

- a and
- b be
- c can
- d the
- e er
- f for
- g get
- h he
- i in
- j just
- k know
- l like
- m more
- n not
- o of
- p people
- q quite
- r right
- s she
- t to
- u up
- v very
- w was
- x xi
- y you
- z zealand

Using the first unclaimed letter in each word to get a better
selection of words gives a perhaps better selection:

- a and
- b be
- c which
- d the
- e he
- f for
- g get
- h that
- i in
- j just
- k like
- l all
- m from
- n on
- o of
- p up
- q quite
- r are
- s is
- t to
- u but
- v have
- w was
- x next
- y you
- z size

If we additionally exclude 2-letter words on the basis that they don't
save enough keystrokes:

- a and
- b but
- c which
- d the
- e they
- f for
- g get
- h have
- i with
- j just
- k like
- l all
- m more
- n not
- o from
- p people
- q quite
- r are
- s this
- t that
- u would
- v very
- w was
- x next
- y you
- z size

### Estimating the payoff for single-letter wordsigns ###

Summing the frequencies of these words, we find that they amount to
19.8% of the words in my frequency table, as against 24.9% for the
first list and 28.5% for the second list.  Estimating the keystrokes
to type the Corpus as one more than the word's length, we get 16.3% of
the keystrokes as belonging to words in the first list, 19.0% the
second, and 15.4% the third; but if we try to estimate the keystrokes
*saved* as one *less* than the word length (because two keystrokes are
used for the wordsign), it's 7.4% for the first list, 8.7% for the
second, and 8.3% for the third.

(Because punctuation, capitalization, and words occurring less than 5
times are not included in my table, it’s probably a bit worse than
this in practice.)

### Optimal word choice ###

The top 26 words by estimated saved keystrokes using the above
procedure (f\*(len(w)-1)) are:

0. 2.467% the
1. 1.070% and
2. 0.586% of
3. 0.551% that
4. 0.507% to
5. 0.390% with
6. 0.369% in
7. 0.368% was
8. 0.334% for
9. 0.297% which
10. 0.283% have
11. 0.277% you
12. 0.277% this
13. 0.262% there
14. 0.259% they
15. 0.247% from
16. 0.217% it
17. 0.208% their
18. 0.203% would
19. 0.199% is
20. 0.193% were
21. 0.188% are
22. 0.185% not
23. 0.182% but
24. 0.178% had
25. 0.173% his

Note, for example, that “be” is not in the list, but the less common
“but” is; neither are “on”, “he”, “by”, “at”, or “she”, all of which
are more common than “were”, “which”, “there”, or “would”, but not
enough to make up for the length difference.

This sums up to 10.5% keystroke savings, which I think is the best you
can do with fixed single-letter wordsigns for single words.  Some of
the words would probably have to be assigned to totally irrational
letters like “j”, “k”, “p”, and “z”.  It might be best to assign “j”
and “k” in particular to common words, both because they are on the
home row, and because the irrational assignments will require more
practice to memorize.

Bigram signs
------------

An 8.7% savings is not really enough to justify the switch
(hypothetically you could get from 90 wpm to 98), but it does suggest
that this approach might be fruitful with a somewhat larger number of
wordsigns, and perhaps some bigram signs;
<https://www.quora.com/What-are-the-most-frequent-consecutive-words-in-English>
gives these frequencies for the 450-million-word [COCA][2], which
isn't freely available:

- 2'586'813 of the
- 2'043'262 in the
- 1'055'301 to the
- 920'079 on the
- 737'714 and the
- 657'504 to be
- 617'976 at the
- 616'400 for the
- 544'137 in a
- 537'718 do n't 

[2]: https://www.english-corpora.org/coca/

These represent about 20 million words from the corpus, about 5% of
it, and actually probably the majority of occurrences of “the”.  “Of
the” alone is over 1.2% of the corpus, which would put it around #7-9
if it were a double-weighted word, near “it”, “is”, and “was”.  If you
don't double-weight it it's around #22-25, with “this”, “not”, “but”,
and “had”.  Even “in a” is 0.24%, around #50, similar to “can”, “so”,
“no”, and “who”.  It might be worthwhile to assign wordsigns to some
of these.

Multi-character wordsigns
---------------------------------

26 wordsigns on keystrokes like hyper-a and hyper-d could be augmented
not only by non-alphabetic wordsigns (hyper-', hyper-.) but also by
wordsigns in cases where you'd already started a word; the second list
above, for example, excludes ‘with’, which is 0.72% of the words in
the BNC and 0.65% of the imputed keystrokes. You could assign it to w
hyper-i or w hyper-/.

The top 457 words in the wordlist amount to 50% of the keystrokes. But
assigning them all (or anyway the ones over 2 letters) to such
3-keystroke sequences would only save 15.8% of the imputed total
keystrokes, because most of them are already quite short.

Of the most common 31 words, 29 are over one letter.  If we suppose
these 29 words are assigned 2-stroke wordsigns of the form hyper-x,
that would save us 10.4% of our strokes.  If words 31 to 872 are then
assigned 3-stroke wordsigns of the form above, it would save another
16.0%, for 26.4% in all.  If we remove 10 from the list, it drops from
16.0% to 15.9%, but then we could assign the 10 common bigrams above
to 3-stroke wordsigns; if they were on average 6 strokes long this
would save you about another 2.5%, getting to 29.0%.  You could eke
out a few more tenths of a point with the kind of length-sensitive
assignments I outlined above.  Better compression than that probably
needs more context-sensitivity; more bigrams or even trigrams might
plausibly help.  More elaborate forms of context-sensitivity, such as
context-dependent wordsigns, are probably too much cognitive load and
would make typing slower, not faster.

Maybe you could also assign words to shifted letters: hyper-a and
hyper-A would be different words.  Shift-hyper-w isn’t shorter than w
hyper-i for “will ” but it might be easier to remember.

### Some candidate assignments for 128 two-letter wordsigns ###

Here are the top 128 single-word candidates for assigning to ssuch
3-stroke sequences, with the estimated fraction of total keystrokes
that would be saved by each.  It would be better to assign the most
common ones to 2-stroke sequences, but they are included for
completeness and to show how the ordering changes.  In particular,
note the complete absence of 2-letter words, because the savings for
them are computed as zero keystrokes:

0. 1.2335% the
1. 0.5348% and
2. 0.3670% that
3. 0.2600% with
4. 0.2225% which
5. 0.1961% there
6. 0.1889% have
7. 0.1847% this
8. 0.1842% was
9. 0.1728% they
10. 0.1668% for
11. 0.1649% from
12. 0.1560% their
13. 0.1526% would
14. 0.1387% you
15. 0.1287% were
16. 0.1150% about
17. 0.1071% been
18. 0.1012% will
19. 0.1007% could
20. 0.0995% what
21. 0.0993% government
22. 0.0990% people
23. 0.0939% are
24. 0.0923% not
25. 0.0909% but
26. 0.0904% between
27. 0.0888% had
28. 0.0887% should
29. 0.0864% his
30. 0.0849% because
31. 0.0815% said
32. 0.0805% more
33. 0.0790% through
34. 0.0781% other
35. 0.0758% she
36. 0.0750% these
37. 0.0732% something
38. 0.0710% first
39. 0.0694% information
40. 0.0691% them
41. 0.0683% some
42. 0.0675% different
43. 0.0652% into
44. 0.0652% her
45. 0.0641% then
46. 0.0614% time
47. 0.0603% however
48. 0.0596% like
49. 0.0581% development
50. 0.0580% another
51. 0.0561% only
52. 0.0560% against
53. 0.0557% children
54. 0.0552% your
55. 0.0548% important
56. 0.0546% think
57. 0.0540% years
58. 0.0533% being
59. 0.0531% those
60. 0.0523% all
61. 0.0522% although
62. 0.0517% has
63. 0.0503% just
64. 0.0503% before
65. 0.0498% thought
66. 0.0498% also
67. 0.0491% know
68. 0.0484% international
69. 0.0477% three
70. 0.0471% one
71. 0.0471% can
72. 0.0465% well
73. 0.0464% very
74. 0.0460% without
75. 0.0451% national
76. 0.0439% particularly
77. 0.0434% than
78. 0.0434% still
79. 0.0427% political
80. 0.0421% after
81. 0.0419% right
82. 0.0412% who
83. 0.0409% possible
84. 0.0405% business
85. 0.0400% company
86. 0.0394% most
87. 0.0393% number
88. 0.0391% over
89. 0.0390% back
90. 0.0386% going
91. 0.0383% really
92. 0.0379% available
93. 0.0378% themselves
94. 0.0370% might
95. 0.0369% within
96. 0.0369% always
97. 0.0367% much
98. 0.0363% under
99. 0.0363% education
100. 0.0362% further
101. 0.0360% following
102. 0.0357% many
103. 0.0357% british
104. 0.0356% system
105. 0.0353% world
106. 0.0351% particular
107. 0.0351% during
108. 0.0349% perhaps
109. 0.0348% management
110. 0.0346% little
111. 0.0342% already
112. 0.0340% nothing
113. 0.0339% while
114. 0.0339% anything
115. 0.0338% things
116. 0.0337% social
117. 0.0337% including
118. 0.0336% again
119. 0.0334% never
120. 0.0332% yeah
121. 0.0331% whether
122. 0.0330% second
123. 0.0329% problems
124. 0.0329% interest
125. 0.0329% him
126. 0.0327% probably
127. 0.0326% work

This would work out to about 10.4% savings.  It seems to be strongly
biased by the journalistic nature of the BNC.  If we only use words
that weren’t in the previous set of 26:

0. 0.1150% about
1. 0.1071% been
2. 0.1012% will
3. 0.1007% could
4. 0.0995% what
5. 0.0993% government
6. 0.0990% people
7. 0.0904% between
8. 0.0887% should
9. 0.0849% because
10. 0.0815% said
11. 0.0805% more
12. 0.0790% through
13. 0.0781% other
14. 0.0758% she
15. 0.0750% these
16. 0.0732% something
17. 0.0710% first
18. 0.0694% information
19. 0.0691% them
20. 0.0683% some
21. 0.0675% different
22. 0.0652% into
23. 0.0652% her
24. 0.0641% then
25. 0.0614% time
26. 0.0603% however
27. 0.0596% like
28. 0.0581% development
29. 0.0580% another
30. 0.0561% only
31. 0.0560% against
32. 0.0557% children
33. 0.0552% your
34. 0.0548% important
35. 0.0546% think
36. 0.0540% years
37. 0.0533% being
38. 0.0531% those
39. 0.0523% all
40. 0.0522% although
41. 0.0517% has
42. 0.0503% just
43. 0.0503% before
44. 0.0498% thought
45. 0.0498% also
46. 0.0491% know
47. 0.0484% international
48. 0.0477% three
49. 0.0471% one
50. 0.0471% can
51. 0.0465% well
52. 0.0464% very
53. 0.0460% without
54. 0.0451% national
55. 0.0439% particularly
56. 0.0434% than
57. 0.0434% still
58. 0.0427% political
59. 0.0421% after
60. 0.0419% right
61. 0.0412% who
62. 0.0409% possible
63. 0.0405% business
64. 0.0400% company
65. 0.0394% most
66. 0.0393% number
67. 0.0391% over
68. 0.0390% back
69. 0.0386% going
70. 0.0383% really
71. 0.0379% available
72. 0.0378% themselves
73. 0.0370% might
74. 0.0369% within
75. 0.0369% always
76. 0.0367% much
77. 0.0363% under
78. 0.0363% education
79. 0.0362% further
80. 0.0360% following
81. 0.0357% many
82. 0.0357% british
83. 0.0356% system
84. 0.0353% world
85. 0.0351% particular
86. 0.0351% during
87. 0.0349% perhaps
88. 0.0348% management
89. 0.0346% little
90. 0.0342% already
91. 0.0340% nothing
92. 0.0339% while
93. 0.0339% anything
94. 0.0338% things
95. 0.0337% social
96. 0.0337% including
97. 0.0336% again
98. 0.0334% never
99. 0.0332% yeah
100. 0.0331% whether
101. 0.0330% second
102. 0.0329% problems
103. 0.0329% interest
104. 0.0329% him
105. 0.0327% probably
106. 0.0326% work
107. 0.0326% experience
108. 0.0325% its
109. 0.0324% therefore
110. 0.0322% community
111. 0.0321% down
112. 0.0318% country
113. 0.0315% make
114. 0.0312% council
115. 0.0311% two
116. 0.0311% actually
117. 0.0310% himself
118. 0.0309% question
119. 0.0308% good
120. 0.0307% difficult
121. 0.0303% research
122. 0.0302% out
123. 0.0301% such
124. 0.0299% together
125. 0.0299% school
126. 0.0298% everything
127. 0.0297% services

This works out to 6.2%.  Note the boosted ranks of long words like
“government”, which is sixth here, but 135th by frequency of use.

On top of the previous estimate of 10.5% savings for single-letter
(two-stroke) wordsigns, this would work out to 16.7% savings in
keystrokes by memorizing 154 wordsigns.  This is only 58% of the
payoff of a 29% savings but it’s also about 18% of the memorization
effort, and it’s probably a bit more transferable across genres of
writing than the more fine-tuned 800+ word list would have to be.

Data sources for further digging
--------------------------------

[Google N-grams has a dataset that would maybe give better bigram
frequencies.][0] The 2-grams are split up into 500-some files of
different lengths but generally a few hundred megs, so this is
probably a few hundred gigs.  [The Brown Corpus][1] is more
manageable, supposedly freely available (even if I can’t find the
download link) and might be adequate.

[0]: https://storage.googleapis.com/books/ngrams/books/datasetsv3.html
[1]: http://icame.uib.no/brown/bcm.html

