I've tried out Kakoune a little and watched some videos about it and
about its clone Helix.  It's surprising, but I can pretty much use
Kakoune with Vim commands, so it's reasonably easy to pick up.  I
expected that the omission of ^W in insert mode would annoy me a lot,
since it would be better to omit backspace, but it's surprisingly
tolerable.

It occurs to me that there are some improvements that could probably
be made to the command model.  The cursor is a whole character wide,
as in vi, which is a mistake; the Bravo/Emacs model of the cursor being
positioned between characters is the correct one.  And Kakoune's concept
of a word doesn't include the whitespace around it, so it still has the
w/e distinction of vi, and it's unnecessarily difficult to cut and paste a
sequence of words as a result.  But the multiple-selection approach seems
brilliant, and the defense of modality (that it privileges insertion in
an unreasonable way, when most of the time you are navigating or editing
rather than inserting) seems plausible, if not obviously correct.

Kakoune's equivalent of visual-line-mode is called [the wrap
highlighter][0].  By default, Kakoune seems to scroll to the right when
a line gets too long, behavior I almost never want; the solution is
`addhl buffer/ wrap`, with available `-word` and `-indent` options.

[0]: https://github.com/mawww/kakoune/issues/484

History and autocompletion are great conveniences, as is the display of
available commands.  But running in a terminal is a terrible disadvantage.

The self-documenting nature of Kakoune is not quite as complete as Vim
or Emacs; there seems to be no way to find out what key a given command
is on, for example, and there's no overview of the documentation.

Having prompts uttered by a cute cartoon character is definitely a plus.

I'm not sure if the forward-back jumping of Kakoune's ^I/^O is better than
the tags stuff in Emacs and Vim, but I suspect that it is; having less ways
to get back to where you just were is probably better than having more.
