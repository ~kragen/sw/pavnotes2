Suppose you take an LFSR M-sequence in ±1 and multiply it pointwise
with, for example, an image of interest, then intercalate it with the
inverted image.  If you correlate by masking out the correct pixels
then you will get the original image, but any shifted version of the
image will have correlation precisely 0.  Taking the majority rule of
many such encoded images at different shifts will give you a dim,
low-contrast version of every image, with a square-root amount of
noise added by the other images.

By viewing the joined image through an opaque mask with holes cut in
it, whose shift is determined by parallax due to the angle of viewing,
you should be able to see some approximation of the correlation
between the mask pattern and the image at different shifts, improving
dramatically on the inverse-linear brightness bound.

Is there a way to get "opacity holograms" by this method that ensure
that the interference introduced by the interfering images has
expectation 0 everywhere?
