(Also related is file `tinyscript.md`, about possibly making tiny
scripting languages.)

I grew up on BASIC, and I have a bit of a soft spot in my heart for
it.  Like, here's some code from GORILLAS.BAS, demonstrating several
of the features of the genre:

      IF GSettings.useSound THEN PLAY "MBO0L32EFGEFDC"
      Radius = ScrHeight / 50
      IF Mode = 9 THEN Inc# = .5 ELSE Inc# = .41
      FOR c# = 0 TO Radius STEP Inc#
        CIRCLE (x#, y#), c#, ExplosionColor
      NEXT c#
      FOR c# = Radius TO 0 STEP (-1 * Inc#)
        CIRCLE (x#, y#), c#, BACKATTR
        FOR i = 1 TO 100
        NEXT i
        Rest .005
      NEXT c#      '...

    IF Mode = 9 THEN
        ExplosionColor = 2
        BackColor = 1
        PALETTE 0, 1
        PALETTE 1, 46      '...
      ELSE
        ExplosionColor = 2
        BackColor = 0
        COLOR BackColor, 2
      END IF

This is not great code, and the syntax is repellent, but it is very
accessible.  There's not a lot of ceremony or indirection or
justification.  It's very humble code that just wants to draw an
explosion as a series of circles, getting the animation speed from the
slowness of the circle-drawing code, with special cases hacked in
until it looked good enough.

BASIC is good at that sort of thing.  It was the first great "folk
logic" medium, predating Jupyter, Godot, JS, PHP, HTML, Excel,
HyperCard, MATLAB, 1-2-3, VisiCalc, Smalltalk, and even LOGO; it
enabled legions of the unwashed to realize their visions in the medium
of computation.

Another language with BASIC-like accessibility is Tcl.  [Yossi Kreinin
says, in "I can't believe I'm praising Tcl",][0] that the optimum for
things like Tcl and shell languages is this kind of interaction:

    $ pmem 0 stat
    IDLE
    $ pmem 0 bkpt 0 0xbff
    $ pmem 0 bkpt 1 0xa57
    $ pmem 0 cmd run
    $ pmem 0 stat
    DEBUG
    $ pmem 0 pc
    0xbff
    
[0]: https://yosefk.com/blog/i-cant-believe-im-praising-tcl.html

He elaborates:

> Tcl basically freaks me out with its two fundamental choices:
> 
> * **Tcl likes literals, not variables**. Tcl: `string, $var`. Pop infix:
>   `"string", var`.
> * **Tcl likes commands, not expressions**. Tcl: `doit this that`, `[expr
>   $this+$that]`. Pop infix: `doit("this","that")`, `this+that`.
> 
> So basically, pop infix languages (and I use the term in the most
> non-judgmental, factual way), pop infix languages are optimized for
> programming (duh, they are programming languages). Programming is
> definitions. Define a variable and it will be easy to use it, and
> computing hairy expressions from variables is also easy. Tcl is
> optimized for usage. Most of the time, users give simple
> commands. Command names and literal parameters are easy. If you are
> a sophisticated user, and you want to do `pmem 0 bkpt [expr [pmem 0
> pc] + 1]`, go ahead and do it. A bit ugly, but on the other hand,
> simple commands are really, really simple.
> 
> ...And in Python it's `doit("xx","yy")`. And in Lisp it's `(doit
> "xx" "yy")`, or `(doit :xx :yy)`, or `(doit xx yy)` if you make it a
> macro. And in Ruby it's `doit :xx :yy`, if you use symbols and omit
> parens. And that's about as good as you can get without using your
> own parser as in `doit "xx yy"`, which can suck in the (more rare)
> case when you do need to evaluate expressions before passing
> parameters, and doesn't completely remove overhead.

So I was thinking about this with reference to Smalltalk-78's
constructs for delaying evaluation (see file `smalltalk-78-loops.md`)
and programming languages good for IMGUI programming.

There are several things I'd like to somehow glom together into a very
offbeat programming language:

1. Space-separated arguments, like Tcl and bash and Lisp, rather than
   comma-separated arguments.  This implies that infix arithmetic
   expressions, if supported at all, need some sort of magic sigil to
   introduce them, like Tcl's `expr` or bash's `$(())`.  The calmest
   option here would seem to be `()`.

2. What Perl calls "barewords": `bkpt` should, by default, mean the
   string "bkpt" or a symbol `bkpt` and not the current value of a
   variable named `bkpt`.  Not numbers, just words.  This implies that
   when you *want* a variable as an argument you need some kind of
   sigil distinguishing it from a bareword, the traditional choice
   (sh, Perl, PHP) being `$var`, but that's pretty repulsive.  Most of
   the rest of the ASCII punctuation zoo is preferable: ``
   `!@#%^&*()_-+=[]{}:;'",<>./\?~| ``.  The lightest-weight choices
   would be `,var`, `:var`, or `.var`, though angle brackets `<var>`
   are worth a mention.
   
   A possible alternative here is to treat variable names as variables
   within infix expressions (where generally you don't want strings
   anyway), and use parens to put infix expressions in argument lists,
   in which case you'd just use `(var)`.

3. Conventional .-path access to nested objects, so `foo.bar.baz`
   means the `baz` member of the `bar` member of `foo`, assuming `foo`
   is some kind of aggregate (maybe `$foo`) and not a string.  Rust is
   popularizing the use of this even for positional access to tuples:
   `foo.1.baz.2`.  An interesting thing about sigils on variable names
   is that you don't need a separate operator like the `[]` in JS and
   Lua to index with a variable; if `$foo.bar` is the `bar` member of
   `$foo`, then `$foo.$bar` is the member whose name is found in the
   variable `$bar`.
   
4. `x = 3` syntax for variable declarations, perhaps with appropriate
   sigils: `$x = 3`.
   
5. Opt-in pass by reference and block syntax, so that it's possible to
   define control structures like `for &$x $items { wibble $x }`.  If
   the blocks and references aren't first-class, so they can't be
   stored in data structures, it's simple to guarantee downward-funarg
   discipline for them so that they can't escape.  (See file
   `block-arguments.md` in Dernocua for notes on how to implement this
   efficiently.)

6. Curly braces for blocks.  This is in large part because, even
   though I like Python's highly readable indentation-based syntax
   with colons, I'm not sure how to syntactically handle more than one
   block parameter to a command.

Part of the objective here is to avoid garbage collection while
minimally sacrificing the ability to do higher-order programming; if
that's successful, a language like this can be used in a lot of
embedded and interactive contexts where GC languages are commonly used
but sometimes regretted.

Some possible examples
----------------------

When outlining programming-language stuff I think it's good to look at
what some example code would look like.  Badly chosen examples can
make this work poorly, but a lot of the worst choices are obviously
bad as soon as you try them on *any* example.

### Yossi's debugging example

    pmem 0 stat
    pmem 0 bkpt 0 0xbff
    pmem 0 bkpt 1 0xa57
    pmem 0 cmd run
    pmem 0 stat
    pmem 0 pc
    pmem 0 bkpt ({pmem 0 pc} + 1)

Here the {} are being used to enclose a (prefix syntax) call to a
subroutine within an infix arithmetic expression.  This seems a little
better than Tcl.

### Building a linked list of 5000 numbers

This is a translation of some C code for an arena allocator.

This uses a `do` loop, and I'm trying out `.` as a sigil for
variables.  This frees up `:` for use as an initialization operator,
with the nice effect that the variable reads as a *label* for the
result of the command.

    do &.i .n {
        .p: km_start .km_libc_disc &.err
        if (!.p) { abort }

        .list: nil
        do &.j 5000 {
            .q: km_new .p
            set .q.i = .j
            set .q.next = .list
            set .list = .q
        }
    }

Here we have `set` statements to mutate existing variables rather than
declaring new ones.  This is not a super compelling example but at
least it's not abominable.

This dot syntax doesn't mesh well with, like, `$items.$i`, which
becomes `.items..i`, which is unreadable; you'd need `.items[.i]` or
something.  In its favor is that the dots are very calm, and it's
suggestive that .next is a variable inside .q in the same way .q is a
variable in, I guess, some surrounding context.

This example (as well as others later) calls out a scoping problem
with loop variables.  Presumably `.j` doesn't exist without the
do-loop, but that means that we can bring new variables into existence
by passing references to them as arguments.  This doesn't seem
absurdly error-prone: presumably usually when we're passing the
variable to `do` or whatever by reference, it's because `do` is going
to assign to it, so it won't be uninitialized.  Still, it might be
preferable to have a different syntax for introducing a new variable
like this.

### Testing a web server

This time I'm trying out a leading `:` for variable names and going
back to the conventional `=` for assignment.

    :portnum = 8086
    get :url = {
        netcat localhost :portnum &:socket {
            echo "GET $:url HTTP/1.0\r" :socket
            echo "\r" :socket
        }
    }

    expect :tested_url :expected = {
        diff ({get :tested_url}) :expected
    }

This is a somewhat fanciful example adapted from a shell script which
has a bunch of coroutines.  We're defining a couple of commands,
`expect` and `get`, and using `netcat` and `diff` commands.  `get` is
maybe returning a string?  And `netcat` is setting up a variable
called `:socket` which it uses to enable the `echo` command to yield
control back to it, maybe.

### Drawing some vectors

    Line = {
        storexyn 2
        newpath
        moveto $x.0 $y.0
        lineto $x.1 $y.1
        if (!$brushNone) {
            istroke
        }

        leftarrow 0 0 1 1
        rightarrow 0 0 1 1
    }

This is a subroutine definition adapted from one of the PostScript
definitions in the InterViews drawing program `idraw`.  This time I'm
trying out `$` rather than `:` for variables.  I'm not sure I like it.

### Network commands

    ping -b "172.24.74.47"
    ip addr

Optional parameters like the broadcast ping flag here are one of the
weakest parts of the approach I've sketched above.  See below for
slide-rule drawing code that has a similar, but worse, problem with
optional parameters.

Optional parameters are also a problem in languages like C, Fortran,
etc.  C++ tries to handle this with default arguments and overloading;
in Smalltalk this is commonly done with related method names with
different numbers of arguments; for example, in Smalltalk-78,
Rectangle has a bitsFromString: method which invokes its
bitsFromString:mode: method, which in turn invokes its
bitsFromString:mode:into:mode:clippedBy: method, as do
bitsFromString:mode:clippedBy: and
bitsFromString:into:mode:clippedBy:, which latter is also invoked by
bitsFromString:into:.  (Logically there are 26 other possibilities
which happen not to be defined at the moment.)  In Tcl instead it's
common to parse an argument list at runtime.  Both of these are
terrible.

Python and Common Lisp have a reasonably ergonomic way to deal with
this sort of thing: you pass in the optional arguments as a dictionary
of name-value pairs.  If you want access to the optional arguments to
be fast, you can first set them to their defaults and then iterate
over the arguments passed in the subroutine preamble, overwriting the
defaults; alternatively, if you want subroutine entry to be fast, you
can search the dictionary every time you access one of the optional
arguments.

Assuming argument access speed is more important than call speed, a
slightly more efficient setup would compile the default argument
values into the caller, so the caller always passes all the arguments
exactly once.  This avoids initializing default argument values that
are then overwritten.

### IMGUI menus

This time I'm trying out `*` to mark variables.

    menu V_iew {
        submenu T_oolbars {
            check_menu_item &*show_menu_bar "M_enu bar"
            submenu "B_ookmarks toolbar" {
                radio_menu_item &*bookmarks_toolbar "A_lways show" always
                radio_menu_item &*bookmarks_toolbar "O_nly show on new tab" newtab
                shortcut Ctrl+Shift+B
                radio_menu_item &*bookmarks_toolbar "N_ever show" never
            }

            menu_item "C_ustomize toolbar..." {
                launch_customize_toolbar
            }
        }

        submenu Side_bar {
            shortcut Ctrl+B
            check_menu_item &*bookmarks_sidebar B_ookmarks
            after {
                *history_sidebar = false
                *synced_tabs_sidebar = false
            }

            shortcut Ctrl+H
            check_menu_item &*history_sidebar H_istory
            after {
                *bookmarks_sidebar = false
                *synced_tabs_sidebar = false
            }

            check_menu_item &*synced_tabs_sidebar "S_ynced tabs"
            after {
                *history_sidebar = false
                *synced_tabs_sidebar = false
            }
        }
    }
            
This IMGUI menu setup, excerpted from Firefox's menu system, runs each
submenu block exactly once, but the `menu_item` block may or may not
get run, depending on whether the user is currently clicking on that
item.  Some items have shortcuts added to them; this necessarily
happens ahead of time in order to be able to run the `menu_item`
callback block.  (This is one possible way to handle optional
parameters, I guess, but the invisible dataflow is suboptimal.)

The variable `*bookmarks_toolbar` may get set to one of the strings
(or symbols?)  "always", "newtab", or "never", and if it's set to one
of them, a radio-button will appear.  Similarly, the `check_menu_item`
items toggle booleans.  The "after" blocks get run if the menu item
above them was activated; they ensure that only one of the three
sidebars is visible at a time.

If it weren't for the shortcut keys, you wouldn't need to run the
blocks for closed submenus.

IMGUI is sort of the opposite extreme from "optimized for usage":
IMGUI code needs to get run for every screen redraw.

### Generic internal iteration

This is the example I took from Smalltalk-78:

    select &:item :items pred cont = {
        each &:item :items {
            if (pred) { cont }
        }
    }

Or, in a more literal translation:

    select &:item :items pred cont = {
        do &:i ({length :items}) {
            :val = :items.:i
            set :item = :val
            if (pred) { cont }
        }
    }

The idea here is that this is invoked with something like

    select &:s :names { is_a self.:s widget } {
        print :s
    }

to print out the items in `:names` that are widgets; the two block
arguments become commands called `pred` and `cont` within the body of
`select`.  I'm supposing I don't need to write `({pred})` because I'm
not giving it any arguments.

This is still the Smalltalk or CLU "internal" approach to generic
iterators; this function doesn't support the kind of "external"
iteration that Python or Unicon does, which permits you to, for
example, zip or merge two iterators.

### Multiple precision arithmetic

This is the core of a multiple-precision subtraction function I wrote
originally in Python.  Here I'm using `\/` as an infix operator for
"pairwise maximum", which is a generalization of its use as logical
OR.  Here again I'm trying out `.` to mark variables and `:` for
initialization.

    .diff: []
    .borrow: 0
    do &.i (.a.len \/ .b.len) {
        .ai: if (.i < .a.len) { .a[.i] } { 0 }
        .bi: if (.i < .b.len) { .b[.i] } { 0 }
        .di: (.ai - .bi - .borrow)

        if (.di >= 0) {
            append .diff .di
            set .borrow = 0
        } {
            append .diff (.di + .x)
            set .borrow = 1
        }
    }

As with the linked-list building code, this is passable but
uninspiring; it's slightly worse than the original Python.  It is,
however, enormously preferable to the Tcl equivalent.

### The gorilla explosion code

Here I'm trying out `%` to mark variables.  I'm not sure if this
version is more or less readable than the original code but it sure
seems like it would be easier to write.

    if %gsettings.usesound { play MBO0L32EFGEFDC }
    %radius = (%scrheight / 50)
    %inc = if (%mode == 9) { .5 } { .41 }
    for &%c 0 %radius %inc { circle %x %y %c %explosioncolor }
    for &%c %radius 0 (-%inc) {
        circle %x %y %c %backattr 
        do &%i 100 { }   # useless delay loop
        sleep .005
    }
    # ...

    %explosioncolor = 2
    %backcolor = 0
    if (%mode == 9) {
        set %backcolor = 1
        palette 0 1
        palette 1 46   # ...
    }

### Bits of robotfindskitten 

Here's a C function from Alexey Toptygin's implementation of
robotfindskitten:

    static void message ( char *message ) {
            int y, x;

            /*@-nullpass@*/
            getyx ( curscr, y, x );
            if ( ( state.options & OPTION_HAS_COLOR ) != 0 ) {
                    attrset ( COLOR_PAIR(WHITE) );
            }
            (void) move ( 1, 0 );
            (void) clrtoeol();
            (void) move ( 1, 0 );
            (void) printw ( "%.*s", state.cols, message );
            (void) move ( y, x );
            (void) refresh();
            /*@=nullpass@*/
    }

In this Tclish BASIC thing I've been outlining, this would look more
like this:

    message :message = {
        getyx :curscr &:y &:x
        if :state.options.has_color { attrset ({color_pair white}) }
        move 1 0
        clrtoeol
        move 1 0
        printw "%.*s" :state.cols :message
        move :y :x
        refresh
    }

I think this is a significant improvement.

Here's another function:

    /*@-nullstate@*/
    static void read_messages(void) {
            unsigned int i;
            char *home_dir;
            char *user_nki_dir;

            /*@-mustfreefresh -mustfreeonly@*/
            state.messages = 0;
            state.num_messages = 0;
            state.num_messages_alloc = 0;

            for (i = 0; i < BOGUS; i++)
                add_message ( "", 1 );

    #ifndef S_SPLINT_S
            do_read_messages ( SYSTEM_NKI_DIR );
    #endif /* S_SPLINT_S */

            /* coverity[tainted_data] Safe, never handed to exec */
            home_dir = getenv ( "HOME" );
            if ( home_dir ) {
                    size_t home_len = strlen ( home_dir );
                    size_t user_nki_len = home_len + 1 + strlen ( USER_NKI_DIR ) + 1;
                    if ( ! ( user_nki_dir = malloc ( user_nki_len ) ) ) {
                            (void) fprintf ( stderr, "Cannot malloc for user NKI directory.\n" );
                            exit ( EXIT_FAILURE );
                    }

                    (void) strcpy ( user_nki_dir, home_dir );
                    user_nki_dir[ home_len ] = '/';
                    (void) strcpy ( user_nki_dir + home_len + 1, USER_NKI_DIR );
                    do_read_messages ( user_nki_dir );
                    (void) free ( user_nki_dir );
            }

            do_read_messages ( "nki" );
            /*@=mustfreefresh =mustfreeonly@*/
    }

This becomes something like this:

    read_messages = {
        set :state.messages = []
        set :state.num_messages = 0
        set :state.num_messages_alloc = 0

        do &:i :BOGUS { add_message "" 1 }
        if (!:S_SPLINT_S) { do_read_messages :SYSTEM_NKI_DIR }
        :home_dir = getenv HOME
        if :home_dir { do_read_messages (:home_dir ++ "/" ++ :USER_NKI_DIR) }
        do_read_messages nki
    }

I think this is a dramatic improvement but it's basically because this
code should have been written in a high-level language instead of C.
Probably even `num_messages` and `num_messages_alloc` could go away.

### Drawing a slide rule

Here's some actual Tcl code I wrote a quarter century ago to draw a
slide rule:

    proc hairline_slider {canvas name x1 y1 x2 y2} {
            # top/bottom linewidth
            set tblw 8
            # left/right linewidth
            set lrlw 2
            # fudge to make line joins right
            set fudge [expr ceil($lrlw/2)]
            set fudgex1 [expr $x1 + $fudge]
            set fudgex2 [expr $x2 - $fudge]

            set objects {}
            lappend objects [$canvas create line $fudgex1 $y1 $fudgex1 $y2 -width $lrlw]
            lappend objects [$canvas create line $x1 $y2 $x2 $y2 -width $tblw]
            lappend objects [$canvas create line $fudgex2 $y2 $fudgex2 $y1 -width $lrlw]
            lappend objects [$canvas create line $x2 $y1 $x1 $y1 -width $tblw]
            set midx [expr round(($x1 + $x2)/2.0)]
            lappend objects [$canvas create line $midx $y1 $midx $y2 -fill red]
            foreach obj $objects {
                    addtogroup $canvas $obj $name
            }
    }

This becomes something like this:

    hairline_slider .canvas .name .x1 .y1 .x2 .y2 = {
        .tblw: 8  # top/bottom linewidth
        .lrlw: 2  # left/right linewidth
        .fudge: ceil (.lrlw/2)  # fudge to make line joins right
        .fudgex1: (.x1 + .fudge)
        .fudgex2: (.x2 - .fudge)
        .midx: round ((.x1 + .x2)/2.0)
        
        each &.obj [
            ({create_line .canvas .fudgex1 .y1 .fudgex1 .y2 width .lrlw})
            ({create_line .canvas .x1 .y2 .x2 .y2 width .tblw})
            ({create_line .canvas .fudgex2 .y2 .fudgex2 .y1 width .lrlw})
            ({create_line .canvas .x2 .y1 .x1 .y1 width .tblw})
            ({create_line .canvas .midx .y1 .midx .y2 fill red})
        ] {
            addtogroup .canvas .obj .name
        }
    }

I mean I feel like maybe you could do a better API than the Tk canvas
API for this kind of thing but at least this approach gets in the way
less than Tcl does instead of more.

The above "list display" assumes that the evaluation context inside
list brackets [] is the command argument context, so if you want a
list of three strings you can write [red green blue] rather than
["red" "green" "blue"]; this has the drawback that, if you actually
want a list of subroutine results as in this case, you need ({})
around each one.  There are a couple of alternative possibilities:

1. The list items are evaluated as infix expressions, so you don't
   need the outer parens:

        each &.obj [
            {create_line .canvas .fudgex1 .y1 .fudgex1 .y2 width .lrlw}
            {create_line .canvas .x1 .y2 .x2 .y2 width .tblw}
            ...]

2. The list items are evaluated not as command *parameters* but as
   *commands* because they are on separate lines.  That is, a
   multiline [] block is syntactically like a {} block, but it's
   evaluated immediately instead of possibly later, and it's
