I’m interested in procedural generative architecture using shape
grammars like CityEngine’s CGA (presented by [Pascal Müller at
SIGGRAPH 2006][5] before he founded, IIRC, the company ESRI bought).
There’s [a Godot asset providing a shape grammar interpreter in the
Godot asset library][0], but the best information I’ve found so far
are [Rachel Hwang’s recorded lecture][1] from 02017 and some of her
student projects, including [Rishabh Shah’s demo][2] with WebGL and JS
and [Tabatha Hickman’s explanation of how she generated a suburb][3].
Hwang’s [demo of procedural castle generation with four rules in
Houdini Python][7] is especially inspiring.

[0]: https://godotengine.org/asset-library/asset/1871
[7]: https://www.youtube.com/live/t-VUpX-xVo4?t=1476
[5]: https://dl.acm.org/doi/10.1145/1179352.1141931
[3]: https://github.com/tabathah/Project4-Shape-Grammar
[2]: https://rms13.github.io/Project4-Shape-Grammar/
[1]: https://www.youtube.com/watch?v=t-VUpX-xVo4

The basic shape grammar procedure
---------------------------------

As [Hwang’s slides][4] explain, the basic shape-grammar algorithm is
very simple, in a sense even simpler than in an L-system, although the
entities manipulated are 3-D objects with geometric properties like
position and orientation, and possibly other things like color:

> Symbol = {terminal, non-terminal}  
> Shape = {symbol, geometry, numeric attributes}  
> Rule = {predecessor, successor = f(predecessor), probability}
>
> 1. Begin with some configuration of shapes (like an l-system axiom)
> 2. Select an shape S from set.
> 3. Choose a production rule with S as predecessor, compute successor(s) SNEW, add to set.
> 4. Remove S from the set.
> 5. Repeat until all shapes in the set are terminal.

[4]: https://cis700-procedural-graphics.github.io/files/shape_grammar_2_7_17.pdf

(There are variations like eliminating the
terminal/nonterminal distinction, doing the
derivation recursively depth-first rather than
breadth-first, changing the termination criterion,
making a production conditional on
nonintersection, also including context
information like a noise texture to affect the
production of successors, etc.  Müller et
al. attribute the invention of shape grammars to
“Stiny 1975”, a book published by Birkhauser
Verlag.)

The key thing here is that at each step the
geometry is just an unordered set, not a string.
Normally the f() will position the successors
relative to the predecessor, but the structure
isn’t actually hierarchical; it just looks like
it.

Müller et al. say, “Shape grammars can be
simplified to set grammars”, citing “Stiny et
al. 1982”, which suggests that perhaps the
formulation above is not what Stiny originally
proposed.

In CGA the f() are represented textually, as in [ESRI’s tutorial
example][6] (incomplete and reformatted): 

    Lot --> extrude(height) Building
    Building --> comp(f) { front : Frontfacade | side : Sidefacade | top: Roof }
    Frontfacade --> split(y) { groundfloor_height : Groundfloor
                             | { ~floor_height : Floor}* }
    Sidefacade --> split(y) { groundfloor_height : Floor 
                            | { ~floor_height : Floor}* }
    Roof --> offset(-0.4, inside) t(0, 0, -0.2)
    Floor -->
        split(x) { 0.5 : SolidWall | { ~tile_width : Tile }* | 0.5 : SolidWall }
    Groundfloor -->
        split(x) {           0.5 : SolidWall
                 | { ~tile_width : Tile }*
                 |   ~tile_width : EntranceTile
                 |           0.5 : SolidWall }
    Tile -->
        split(x) {    1 : SolidWall
                 | ~1.5 : split(y) { 0.4 : SolidWall | ~1.5 : Window | 0.4: SolidWall }
                 |    1 : SolidWall }

etc.  And they are parameterized with numeric attributes like
`floor_height`.  But this is not essential to the basic shape-grammar
formalism, which is satisfied to treat the successor function as
opaque, just evaluable.

A nice thing from Müller et al.’s original paper
is a grammar rule notation with arguments for the
nonterminals, different from what’s shown above:

    id: predecessor : cond ⇝ successor : prob

Their first example is a façade which is
subdivided into three floors if it’s over 9 meters
tall:

    1: fac(h) : h > 9 ⇝ floor(h/3) floor(h/3) floor(h/3)

Split grammars
--------------

Most of the rules in the larger example above replace a predecessor with
successors that are contained within its volume, so they are in a
sense purely subtractive; `split` subdivides the volume, while
`offset` to the `inside` discards some of it.  But that is not
inherent to the formalism.  I think `t()` is translation, and you can
clearly translate to outside the original volume, and the very first
rule starts with a `Lot` that is just a polygon on the ground and
extrudes it upward to produce the geometry of the `Building`.

[6]: https://doc.arcgis.com/en/cityengine/latest/tutorials/tutorial-6-basic-shape-grammar.htm

Subtractive rules satisfying this “containment property” have the
potential advantage that they inherently can’t create
self-intersections, so no special handling is required.  A shape
grammar thus restricted is a “split grammar” and was presented by
Müller et al. in their 02006 paper.

Applying this to a voxel world with fixed-size voxels, like Minetest,
seems somewhat challenging.  How would you make sure it bottoms out at
the right level?  [Sven Havemann of TU Graz points out that this is
already a less severe problem with things like CityEngine][8], where
your terminals get replaced with something like window façade geometry
you’ve modeled by hand in Blender to make sense at a particular size
(“pre-modeled assets”).  With `{ ~floor_height : Floor}*`, which tries
to split into Floors at intervals close to the `floor_height`, you
might end up with a 2-meter-tall floor and a 1.6-meter-tall door.  Or,
adjacent bits of geometry might be scaled differently, leading them to
not match up.

[8]: https://youtu.be/33IIMvkBRQg?t=921

Subdivision into stretchy parcels
---------------------------------

And it occurred to me that my work on algebraic text layout, based on
Tk’s `pack` layout manager, might provide a solution.  Maybe instead
of defining the sizes of things top down, you can define them bottom
up, by subdividing a rubber-sheet space that stretches to accommodate
the things you put into it.

Here’s a layout specification in this form:

    vr, hr, dhr = [~String(s) for s in '|-=']
    left = (('Chapters' | String('')) - hr -
            (chapno | vr | [t | ~String(' . ') for t in titles]))
    pages = 'Page' - hr - pnos
    table = ~('*' - hr - '*' - ' ') | ' ' | dhr - (vr | left | pages | vr) - dhr

Here “-” stacks boxes on top of each other, “|” stacks them left to
right, and “~” produces repetition.  Python lists have their items
converted to boxes and then stacked on top of each other.  This code
produces this layout:

    * ========================================================================
    - |Chapters                                                          Page|
    * |----------------------------------------------------------------------|
      | 0.|The unchained desert of the singing perfume. .  .  .  .  .  .    1|
    * | 1.|La brisa ilusa de la neblina abandonada. .  .  .  .  .  .  .    43|
    - | 2.|The smiling murder within her young daughters. .  .  .  .  .    80|
    * | 3.|Aquella hija tortuosa de su precipicio árido. .  .  .  .  .  .  96|
      | 4.|False villages of the foolish monks. .  .  .  .  .  .  .  .  . 114|
    * | 5.|El roble sacrílego de mi sacerdote desmoronado y enlutado. .   124|
    - | 6.|The brazen river of his filthy, fallen ivory. .  .  .  .  .  . 166|
    * | 7.|The laughing monster of his humble scream. .  .  .  .  .  .  . 203|
      | 8.|The rough tattoo of his unchained abomination. .  .  .  .  .   219|
    * | 9.|Los perfumes silenciosos de un árbol monstruoso muriéndose. .  237|
    - |10.|The sweet tower of the smooth fire. .  .  .  .  .  .  .  .  .  247|
    * ========================================================================

I think that applying the same approach to shape grammars may be very
productive for generating Minetest buildings.

Instead of replacing nonterminals in a context-free fashion in a
*set*, you replace them in something like a kD-tree; each nonterminal
is replaced with a new subtree of the kD-tree.  Once you’re done
replacing, you can propagate the minimum-size information bottom-up
from the leaves of the tree up to the root.

Suppose WOLOG that a given kD-tree node splits along the X dimension:

- Its minimum size in X is the *sum* of its two children’s minimum
  sizes in X.
- Its minimum size in Y is the *maximum* of its two chidren's minimum
  sizes in Y.
- Its minimum size in Z is the *maximum* of its two chidren’s minimum
  sizes in Z.

Once you get to the root of the tree, you can make
its actual size in each dimension be its minimal
size, and then propagate the size back down the
tree.  Every node in the tree is allocated a
parcel that is at least the space it requires in
each dimension, but it may get more space.  There
are several different possibilities for what to do
with the extra space:

- The parcel can contain the node at the node’s
  minimum size, with empty space around it, on one
  or more sides; this is Tk’s `-anchor` option.
- The node can be stretched to fill the parcel
  space, in one or more dimensions; this is Tk’s
  `-fill` option.
- The node can be *repeated* to fill the parcel
  space, in one or more dimensions, which Tk
  doesn’t have an option for, but which I’ve found
  useful for ASCII-art text layout, as shown above
  with the “~” operator; it provides a convenient
  way to get things like horizontal rules,
  vertical rules, and dot leaders.

A separate question is, when a node is stretched
along the dimension it’s split along, how does it
distribute the extra space to its children?  This
is Tk’s `-expand` option.

Repetition along a dimension varying randomly over
a range can be achieved by making a nonterminal
spawn a child of itself with some probability.  A
single occurrence of this device will produce
exponentially distributed sizes, but summing
several of them can tame that distribution into a
Gaussian.  A binomial distribution can be obtained
with a stack of several copies of nonterminal that
nondeterministically becomes either a given
descendant or nothing.

Instead of binary splitting nodes along different
axes, it may be more practical to have a
three-dimensional M×N×P array of children; the
binary splits 2×1×1, 1×2×1, and 1×1×2 would be
only three possibilities among many.  Each
production rule might still only have a fixed,
predefined number of subdivisions, because
arbitrary repetition can still be achieved as
before.

How would you model a stairway or pitched roof in
this way?  You want the roof to grow tall enough
to cover the bottom of the parcel it’s been
allocated.  This doesn’t fit well.

You could imagine other types of combining nodes
as well as this lattice; in particular, a
mirror-image node which makes a mirror-imaged copy
of its child, yielding a bilaterally symmetric
composite, could be valuable.  And you could
imagine a terminal node applying something like
WaveFunctionCollapse to fill the subdivided space.

Parsing space
-------------

I think there’s a simpler formulation available.
Consider the problem of generating a building to
fill a parcel of a certain size as a
constraint-satisfaction problem: given that this
parcel is a Building, what voxels might it contain?
You can think of this as sort of a parsing problem
where you can nondeterministically generate your
input string, and also it’s three-dimensional
instead of one-dimensional (see file
`parsing-as-constrained-generation.md`), and your
grammar consists of exhaustive subdivision
production rules like the flexible-parcel
formulation above.

Consider this system of productions for producing
pitched roofs; the voxels given are in the X-Y
plane, and everything is one voxel thick in Z:

    ^ → . ^ .
        / @ \

    / → . /
        / @
        
    \ → \ .
        @ \

    . → .
        .

    . → . .
        
    @ → @ @

    @ → @
        @
        
    ^ → / \

Each of these node types is also a voxel, so
expanding them is not mandatory (which is
fortunate, since otherwise the expansion would
necessarily be infinitely recursive).  As in the
flexible-parcels formulation, we impose the
constraints that each subnode in the same column
have the same width and each subnode in the same
row have the same height.  So if at some point we
decide to do this expansion:

    . /  →  .   | . /
    / @         | / @
            ----+----
            /   |   @

we can see that we will also need to expand the
first `.` and the last `@` to satisfy the
constraints.  (Unless we have ε-expansions.)

I assert without proof that the above grammar can
generate a pitched roof or double-pitched roof of
any width, but only of a given height for any
given width.  You could maybe define a house
(slice) with a nonterminal H that isn’t a voxel:

    H → ^
        @

By searching top-down for an expansion of a given
node type to fill a given parcel, we can ensure
that we generate a roof that is the right size for
the house.

A factory might be:
        
    F → F F
    
    F → /
        @

This formulation might generate a factory with
roof segments of different heights; here’s an
alternative that avoids this by constraining all
the roofs to be the same height:

    F → R
        @
        
    R → R R
    
    R → /

This approach seems like it is potentially very
expressive, and it permits a more direct
expression of things like “we want the heights of
houses to be normally distributed around 12 meters
with a standard deviation of 3” than adjusting
production probabilities.  It can also accommodate
things like “we want the heights of houses to be
*uniformly* distributed” which seems a lot harder.

It also eliminates the need for the `-anchor`,
`-fill`, `-expand`, and repetition options in the
internal representation, though they could still
be provided as syntactic sugar if convenient.
This is what makes this formulation simpler.

You can bound the search in many cases with
precomputed minimal and maximal parcel sizes for
each nonterminal.  This should ensure that it
terminates even with potentially infinite
recursion like the above.  This is a little
different from the minimal-parcel stuff described
in the previous section because that was a minimal
parcel size for a fully-expanded-out tree, and
this is a minimal parcel size for a nonterminal
type, the minimum across all of its possible
transitive expansions.

Of course you want to be able to make some
productions rotatable to different axes, but you
don’t necessarily want to generate houses with the
roof pointing sideways.  And, when you’re placing
a nonterminal, you often want to specify its
orientation; maybe the way to handle this is to
allow a production to provide a different
reference frame for its child nodes, so *it*
determines how they’re rotated.  Or reflected.

The examples above generate houses and factories
that are only one voxel thick in Z and are
entirely full of @.  This may be fine for the
front and back walls, but usually you want some
open space inside.  If @ is not an actual voxel,
you could generate an empty 2-D box of arbitrary
size with it, made of # stone, as follows:

    @ → # - #
        I . I
        = = =
        
    I → I || #
        #

    . → . . || .
               .

    - → - - || #
    
    = → = = || #

Extending this to 3-D requires a few more rules
but is not fundamentally that hard.

A pretty important facility for things like
Minetest would be post-filtering the generated
voxels through a voxel replacement table;
otherwise you’d have to define a whole new set of
productions to build houses out of a new material.

This top-down approach also facilitates additional
arbitrary constraints.  For example, you might
want to require that the front door of a house be
at ground level, even if the house is built on
sloping ground.  For this purpose you could allow
the house nonterminal to generate a foundation
embedded in the dirt to raise the ground floor up
to that level, but if you’re not *generating* the
surrounding dirt (which you might prefer to
generate with some kind of terrain model) you need
an additional constraint to reject configurations
where the front door is at the wrong level.
Similarly, you might prefer not to generate
windows underground or facing a wall.  Top-down
parcel subdivision facilitates this because each
node is being generated at a definite position,
unlike with my rubber-parcel formulation above,
where the sizes are determined bottom-up after
selecting all the terminals.

One particularly interesting constraint type is a
constraint that requires a particular voxel to
have a particular value.  This is an interesting
constraint type for two reasons:

- It permits interactive poking and prodding of a
  model.  If a stone castle is generated too
  squat, for example, just constrain a voxel
  somewhere above the castle to be stone, a
  relatively easy and intuitive interaction;
  satisfying that constraint will require the
  castle to get taller.  Similarly, you can
  interactively specify where a door or window
  ought to be.
- It may allow you to compose grammars easily, by
  using voxels generated by one grammar to
  constrain another.  Unsatisfiability may not be
  an easy thing to provide good UI feedback for,
  though.  And it imposes an order of evaluation
  which might be infeasibly inefficient, for
  example if the second grammar is only
  satisfiable under conditions that only occur
  very rarely under the first grammar.

### A wall with a door and windows ###

Suppose we have a wall W in the X-Y plane and we
want it to contain exactly one door and zero or
more windows.  We'll build it out of segments
called X, each of which can become either a
windowless three-block-tall wall or a
segment with a window Y in it:

    X → # || # # #
        #    # Y #
        #    # # #

The initial wall W contains one door.  To preserve
that property, we enable it to grow horizontally
by adding X segments on either side of the
door-containing segment, which ultimately pays off
in a door consisting of two voxels O and P:

    W → W X || X W || #
                      O
                      P

This system can generate walls like this:

    # # # # # # # # # # # #
    # # Y # # Y # O # # Y #
    # # # # # # # P # # # #

Windows are always separated by at least one block
from the door and two blocks from each other.  The
door can be at either end of the wall.  Small
tweaks to the rules can change these, as well as
providing walls without windows, walls with
multiple sizes of windows, walls with arbitrary
numbers of doors, etc.; left as an exercise to the
reader.

Walls of flexible height can be achieved with an
additional nonterminal:

    X → T || T T T
        #    # Y #
        #    # # #

    W → W X || X W || T
                      O
                      P
                      
    T → # || T
             #

### Generating connected paths ###

Can you use the plain grammar approach, without
additional constraint types, to generate a dungeon
with rooms connected by tunnels, as in Nethack?
Let’s consider two horizontal walls each with a
door; I’ll separate alternatives here with || and
use `'` for stone, `#` for tunnel, `+` for door,
and `=` for wall:

    S → A S || S A || +
                      #
                      +

    A → = || =
        '    =
        =

    ' → '
        '

    # → #
        #

This will generate two walls of equal length, each
containing a door somewhere, with a tunnel
connecting them.  You could add additional
productions easily enough to allow the walls to
end early.  But the doors will always be
vertically aligned, which is maybe an inadequately
mysterious dungeon.  Can we maybe make the tunnel
deviate a bit?

This would be the obvious thing to try:

    # → # ' || ' # || # #
    + → = + || + =

but, because it’s being carried out independently
on each row, that will result in the tunnel being
made of disconnected pieces.

What we want is something like Wang tile
constraints, but I’m not sure there’s a way to get
that within the shape-grammar approach.  We could
apply it as an arbitrary constraint on the
space-parsing process, as suggested above for
doors, but I don’t see a way to easily get it just
from the structure of the shape grammar.

If we want just a single dogleg, maybe we can do
it:

    S → T
        I
        B
        
    T → + || T =
    B → + || = B

So S can expand into a region with a door on the
top left and on the bottom right.  Now our
challenge is to expand `I` into a vertical version
of our horizontal-wall structure, but made partly
out of tunnel.  Let’s use `@` for the horizontal
tunnel.

    I → H || I || @ @
        I    J

    @ → @ @
    H → # '
    J → ' #
    
So our I can spawn Hs above and Js below before it
finally decays into a @ @, which can grow
horizontally into a long horizontal tunnel, and
then the Hs and Js link up that tunnel with the
doors using # vertical tunnels.

You could imagine stacking a
horizontally-reflected I below a normal I in order
to get a tunnel that can wander horizontally back
and forth twice.  And you could repeat that
arbitrarily.

I don’t know.  That was pretty hard to figure out.
This feels like it would be a lot harder to use
for purposes like this than example-based things
like WaveFunctionCollapse.  But it can guarantee
long-range properties like only having a single
door between two large rooms that share a wall
inside a building, or only having a single kitchen
in a house, or sometimes even connectedness, that
WaveFunctionCollapse can’t.

### Hilbert curves ###

I’m pretty sure you can formulate the Hilbert
curve in this form.  It’s easiest if you can
rotate nonterminals, but it’s just something like
this:

    A → B ! C || .
        < ! >
        A ^ A

    < → < ! || .

    > → ! > || .

    ^ → . || ^
             !

    V → . || !
             V

    ! → ! ! || !
               !

and B and C are appropriately rotated versions of
A.  Without a built-in rotation facility, I think
you could define B and C as

    B → A ^ B || .
        ! ! <
        D V B
        
    C → C ^ A || .
        > ! !
        C V D

    D → D V D || .
        < ! >
        B ! C

but I might have gotten some of that wrong.

You might need separate horizontally stretchable
and vertically stretchable walls, but I think that
the default nonstretchability of `.` might take
care of that.  This ought to generate things like
the following from A:

    . ! .
    . ! .
    . . .
    
    . . . ! . . .
    ! ! . ! . ! !
    . . . ! . . .
    . ! ! ! ! ! .
    . ! . . . ! .
    . ! . ! . ! .
    . . . ! . . .

I think all solutions for A are square with a
dimension one less than a power of 2.

This suggests that the top-down shape-grammar
approach might possess substantial
labyrinth-generation power.

### Irregular borders: recursive Wang tiles ###

Let’s consider a generalization of the
pitched-roof problem, again only in a 1-voxel
thick slice along the Z axis.  Suppose I have a
square region between Red and Blue; it has Red to
the top and left and Blue to the bottom and right.
I want it to match, as if it were a Wang tile.
The simple pitched-roof approach gives us a
straight border:

    / → R /
        / B

    R → R R || R
               R
               
    B → B B || B
               B

But if we have some other Wang tiles to use, we
can maybe get a more meandering border.  Suppose
`(` is blue on the right and red on the other
three sides; now we can add productions that use
it and get some variety:

    / → R ( || R /
        R /    ( B
        / B    / B

If we have an appropriately rotated `/` we can use
it to define candidate expansions for `(`:

    ( → R / || R /
        R \    ( B
               R \

So far, these only allow portrait-mode-shaped
expansions for `/`.  If we also have, say, `∩`,
blue on the bottom and red on the other three (a
rotated `(`), we can handle arbitrary aspect
ratios with a couple more productions:

    / → R R / || R ∩ /
        / ∩ B    / B B

Defining all these productions by hand is a pain,
and would be impractical for 3-D voxels, but it
seems apparent that you can generate them
automatically from Wang-tile matching rules.
Isolated islands like this are valid under
Wang-tile rules but might be disabled by a
generation option:

    / → ( ) /
        R / B
        / B B

With this sort of thing you can imagine
generating, for example, an island with an
irregular seashore.

This is also a solution to the connected-path
problem.

### Simple terrain ###

Suppose we have a horizontal dirt surface with air
above it, using # for dirt.

    = → .
        #

We want our air to be able to expand arbitrarily:

    . → . . || .
               .

If we just want to make a flat plain, we can add a
production to extend the surface horizontally:

    = → = =

But if we want a little variation, we can maybe
add an option where the surface goes up in the
middle:

    = → = = = || . = .
                 = # =

If we want this new bump to be able to stretch
horizontally as well (and possibly grow more
bumps) we need to enable the dirt to stretch too,
but only horizontally:

    # → # #

Finally, we might want to turn the surface into
just dirt as a final step:

    = → #

I think that’s sufficient to give us a
two-dimensional pixel terrain that can only vary
incrementally by one block at a time and ends at
the same height where it starts.  I don't know if
it ends up as fractal.

But how would we apply it in 3-D?  We could have
another production to rotate the `=` children into
the Z dimension (assuming that’s the other
horizontal dimension, as in Minetest) but it’s
only going to enable our bumps to stretch
horizontally into rectangular bumps.  I think this
may be a limit to the applicability of this
space-parsing approach.

Polyhedral subdivision
----------------------

Lots of things aren’t voxels, though.  Continuous
space is totally a thing, especially when you’re
modeling real-world things, for example, buildings
from GIS footprints.  Müller’s CGA approach was
initially proposed for generating building shells
for city models.  Is there a reasonable way to
apply the space-parsing approach to arbitrary
polyhedra?

As Müller et al. say, “It is not clear how to
write meaningful shape rules for general
polygons.”

Plant growth
------------

You may be able to get some mileage out of the
space-parsing approach for things like Minetest
trees: a jungle tree is a crown on top of a trunk
on top of roots, a crown is a tier or a tier on
top of a crown, a tier is a trunk with branches on
the side, a branch has some wood with some leaves
on the side, etc.  But I’m also interested in how
you’d go about making less cartoony plants, and
I’m not sure hierarchical subdivision of parcels
of space is going to do that.

Shape grammars of the unconstrained type seem like
they would be well suited to modeling plant
growth, but unlike in the case of architecture, it
might be desirable to move parts of the plant as
other parts of it grow.  Modeling plants thus
seems like it should be considerably more
intuitive than doing so with L-systems.

For this purpose, as with parcel subdivision and
space parsing, we’d like to preserve a
hierarchical relationship where the parent
transforms its children’s coordinate systems into
its own; but now the parent in this hierarchy may
no longer be the parent in the grammar derivation
tree, but rather a sibling node which may continue
evolving with further productions; for example, a
branch growing longer and curving toward the sun,
moving and rotating its leaf children.

In many cases to get natural plant growth we something like the
“occlusion” or collision tests Müller et al. use extensively in CGA
shape grammars, where a candidate production can only happen if it
fails to intersect existing non-ancestor geometry:

    1: tile : Shape.occ("noparent") == "none" ⇝ window
    2: tile : Shape.occ("noparent") == "part" ⇝ wall
    3: tile : Shape.occ("noparent") == "full" ⇝ ε

This converts a “tile” (in this example, one of the squares into which
a façade has been divided) into a square with a window in the middle
if it intersects no other geometry, into a square of wall if it partly
intersects, and into nothing at all if it’s fully inside some other
geometry.

Because it allows interactions between different parts of the
derivation tree, this depends on the ordering of expansion, so Müller
et al. define their system to work breadth-first.
