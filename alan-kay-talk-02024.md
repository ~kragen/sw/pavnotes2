I’m watching [a recorded talk by Alan Kay][0].  Some of his points:

Is a new Xerox PARC feasible today?  Yes, he says, but it wasn’t PARC
in the first place, it was ARPA.  PARC just harvested ARPA’s results
when ARPA abandoned them.  He mentions Janelia Labs and the Max Planck
Institute as two candidate places today.
   
Do young people have a bigger problem acquiring material resources to
pursue real innovation today?  He thinks so; he gives the contrast
with his ability to buy a house in Palo Alto near PARC for twice his
yearly salary when he started working there, a house whose price has
“inflated” by a factor of 70 since then, while salaries have only gone
up by a factor of 7.  (To me, this seems like largely a
California-specific problem.)  And he says he was the oldest person at
PARC at 30; the generation before him was advisors.

His recipe for doing real innovation, derived from how PARC worked,
involves finding a favorable exponential and spending a massive amount
of money to “time-travel” far enough up that exponential to know what
to invent in the new world, “skating to where the puck will be”.  And
certainly there are things you can only do in that way, innovations
that can only become broadly adopted after the price comes down a lot.
But the 

He says it’s “crazy” to sit around a campfire listening to somebody
talk like it was 150000 years ago to explain ideas that required
reading and writing to invent and explain them.  That’s fine for
stories, he says, which is what “our pink brains” want, but systems
aren’t stories.

“Bureaucracy has killed millions and millions more people than the
worst atomic weapons.”  Actual atomic weapons fatalities seems like a
bad thing to compare it to, since we’ve only had one nuclear war so
far, at a time when atomic weapons were in their infancy.

“But think about it: we just went through the inability of most of the
population of the planet to not [clearly “not” was an error]
understand how viruses and diseases work.  That was botched almost
everywhere.”

His conception of the humans: their minds are theaters, they treat
their beliefs as reality (“the worst thing about human beings”),
they’re *tiny* theaters, most of their mind is an ape mind, their
conclusions are (disastrously) societal, and their minds are made of
about 80 different “brainlets” which are often in conflict: “enactive”
learning by touching things, “iconic” learning by seeing them, and a
“symbolic mechanism” that is “both our downfall and our salvation”.

It’s interestingly nonmainstream that his conception of “modern
personal networked computing” as invented at PARC includes
“dynamically loadable emulation microcode” as well as “pointing” and
“bit-map screens”.  I guess bunnie’s “precursor” is the current
manifestation of that.

The rest of his list of “8½ visible invetions” they made at PARC with
about 25 researchers over about 5 years, out of 10 years that PARC
existed in that form:

- the GUI “with icons and pointing”
- OOP (“which is different from what’s called OOP today”: “software as
  networked virtual computers”)
- “about half of the internet” called PUP, PARC Universal Packet,
  “spread all over the country”.  (No mention of [IBM’s VNET, which
  was about the same size by 01986][6] and originated about the same
  time.)
- WYSIWYG (“symmetrical reading and writing”)
- the laser printer (“a page a second at 500 pixels per inch”)
- outline fonts and PostScript (PostScript technically wasn’t a PARC
  invention, but an invention made by Warnock and Geschke after they
  left based on their PARC work, and I think Knuth’s METAFONT work was
  concurrent with PARC’s work on outline fonts if it didn’t actually
  precede it.)
- Ethernet
- peer-to-peer and client-server

He said that this cost about US$10–12M a year in 15-years-ago dollars
(about 02009 dollars I guess).  “But (...) what’s interesting about
all this is all the technology that had to be invented that was
invisible.”

The point about Knuth and the history of outline fonts bears further
investigation, which is in file `outline-font-history.md`.

He said Xerox made back about 200 times its investment on PARC by
selling laser printers.

His main thesis about ARPA is that ARPA had paid for everyone’s
degrees and also taught all those grad students to work together,
which is how PARC could do so much with so little so quickly.

This suggests the hypothesis: I’ve often observed that the rapid
progress in computing science up to about 01975 suddenly stopped.
Could that be simply because ARPA stopped funding it?  It does seem to
be true that ARPA had funded most of the big developments; Kay’s slide
lists Whirlwind (with core memory, real-time graphics and pointing,
and the first interactive high-level language), SAGE, LISP (and AI in
general), timesharing, SKETCHPAD (interactive computer graphics,
constraint solving (which he doesn’t mention until later), and CAD),
the LINC personal computer, packet switching, the ARPAnet, the RAND
tablet and GRAIL, and Engelbart’s NLS.

(Kay has previously suggested the alternative hypothesis that
commercialization and “pop culture” crowded out real research.)

He describes Licklider’s management approach to cat herding as
offering “great visions” as “the ultimate cat toy”, distinguishing
Lick’s “Computers are destined to become interactive intellectual
amplifiers for everyone universally networked worldwide” from goals or
objectives.  It’s myth-making or inspiration rather than an
achievable, measurable objective — *destined* implies that you don’t
even have agency, it’s going to happen whether you want it to or not!
Cosmic, romantic.

“The #1 principle of doing anything like this where you are going to
(...) have the possibility of doing really large things is, the
goodness of the results correlates most strongly with the goodness of
the funders.  (...) Every era has really smart people — I’ve never
been in an era that didn’t have people as smart as the people I worked
with back then — but the funders (...) were wonderful in the 60s and
the 70s, and they’ve been ever more terrible ever since.”

He uses “fireflies” as a metaphor for something or other scientists
are supposed to look for that I don’t understand, “hidden, deeper
properties”.

He didn’t repeat his claim about how Smalltalk performance hasn’t kept
up with Moore’s Law.

I’m amused and horrified at his vision of current computing systems as
a sort of cargo-cult elephant created by a compromise between the
blind philosophers to end their warfare, with a brick wall mounted
atop two trees with a spear poking out of one end, a rope tied to the
other, and a python and a fan near the spear.

“Because our brains want to believe, rather than think, these can turn
into something like a religion.  The reason we don’t want to come up
with blind belief: there’s always more, and what it is, is something
that we can’t imagine.  So when we give a name to something, we (...)
hurt our ability to think more about it, because it already has a set
of properties, it already is the thing the word denotes.”

“The purpose of arguing is not to win but to illuminate. (...) When
people started getting heated in an argument, [PARC director Bob]
Taylor would say, ‘Type 2!  Type 2!’ and a type 2 argument was, all of
a sudden, you had to explain the other person’s position back to them
until they agreed with it, and then they had to explain your position
back to you until you agreed with it.  By the time that had happened,
you’d forgotten what you were arguing about.”

About how theater works and ARPA worked: “It’s the love of what you’re
going to get the audience to try to think about.  Because you can’t
tell the audience *anything*.  People only remember how you make them
feel.  (...) That’s why I do all these pictures and stuff, because I
want the pictures to do things you can’t do in regular words.”

[0]: https://youtu.be/dZQ7x0-MZcI
[6]: https://dl.acm.org/doi/pdf/10.1145/6617.6618
