Eddy currents in a piece of metal can not only heat it but repel it
from an electromagnet.  These die off exponentially below the surface
of the metal; [in aluminum at 100 MHz the skin depth at which they are
reduced to 1/*e* is about 8.2μm][0], so increasing the aluminum
thickness further has relatively little effect on the repulsion.

[0]: https://www.allaboutcircuits.com/tools/skin-depth-calculator/

Under some circumstances you can get an attraction instead, involving
phase-delaying the magnetic field in the middle of the surface of the
aluminum, so that waves of magnetic field move through the conductor
toward the electromagnet.  I don’t think this is feasible with
aluminum foil with macroscopic-sized electromagnets.

But this electrical repulsion still ought to be able to repel the
aluminum foil from spirals on a printed circuit board or coils of
hair-thin magnet wire.  If there isn’t too much air resistance, for
example if the PCB is full of vias, this should make it possible to
use this eddy-current repulsion effect to make an electromagnetic
speaker given a PCB and a sheet of aluminum foil.

An alternative approach which wouldn’t emit RF is to make an
electrostatic speaker, but this requires not just inconveniently high
voltages (typically a few kV) but also driving current into and out of
the foil.
