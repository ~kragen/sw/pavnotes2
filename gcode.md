I was looking at some G-code tutorial videos, like [one from
camInstructor][0], [the LinuxCNC docs][LinuxCNC], and [the Marlin
firmware docs][Marlin].

[0]: https://www.youtube.com/watch?v=HiXqFz-Nfh8
[LinuxCNC]: http://www.linuxcnc.org/docs/2.4/html/gcode_overview.html
[Marlin]: https://marlinfw.org/meta/gcode/

Basic syntax
------------

Optionally the file starts with a line saying `%` and the program
continues until the following `%`.  Otherwise it needs to contain an M2
(program stop) or M30 (program end).  Normally there's a "program
number" following the initial `%` saying something like `O6969`.

There are a bunch of basic "words" everybody knows:

   M3     (spindle on clockwise.  Parens enclose comments.)
   M6 T01 (M6 is tool change, to tool #1)
   G00 X0.0 Y0.0 S5000 (spindle speed 5000 rpm, rapids to XY origin)

LinuxCNC says, "A word is a letter other than N [used for line numbers]
followed by a real value."

I think maybe putting multiple words on the same line ("block") makes
them happen simultaneously, and apparently the order doesn't matter.
Spaces are ignored, even inside numbers.  I'm not sure if lowercase
letters are allowed in comments; nobody seems to use them.  Spaces are
apparently allowed in comments but rare.  A line can be commented with
a leading `/`.  LinuxCNC and Marlin allow comments to end of line with
`;` but I haven't seen machinists use this, though I have seen them use
it as an end-of-line marker on Haas machines in manual mode.

Some G-code commands like G1 (straight move, sometimes written G01) are
"modal".  LinuxCNC says, "A machining center may be in many modes at
the same time, with one mode from each modal group being in effect"; the
other modes in G1's modal group are the other motion commands, like G0,
G3 (arc), and another dozen or so.  I think the effect of being in mode
G1 is that for each line the controller executes a straight-line motion.

Nowadays it's evidently conventional to view G-code in sans-serif fonts,
with syntax coloring, on a white background.  Haas machines use a
fixed-width font and no syntax coloring.

A lot of G-code emitted by slicers and other CAM programs, or even
written by hand, just contains literal commands like those, in the
sequence that they are to be executed.  But G-code apparently is a real
programming language.

Its programming-language features are often considered "very advanced"
by machinists, perhaps in large part because the damage from a program
bug can be quite expensive.

Variables and arithmetic
------------------------

Variables ("parameters") are normally named with numbers: #1, #2, #3.
You assign to them with a conventional assignment statement:

    #1=8.0 (LENGTH)
    #2=2.0 (WIDTH)
    #3=0.5 (RADIUS)

You can interpolate these variables into a statement:

    X8.0 F20.0 (go to X=8.0 at feedrate 20.0 inches per minute)
    X#1 F20.0  (same thing)

Infix expressions are supported with Tcl-like `[]`:

    S[600*3.82/#4]  (spindle speed to 600*3.82 divided by variable 4)
    X[#1-#3] F20.0
    G03 X#1 Y#3 R#3 (ccw arc to X=#1, Y=#3 with radius #3, set arc mode)
    G01 Y#2         (G03 is modal; return to G01 mode to cut straight to Y=#2)

But this is not necessary for assignment statements:

    #130=#130+1

I don't know what the type system is; these variables aren't declared,
and everything I've seen so far is a signed real number, possibly
floating-point.

At least in LinuxCNC you can say #[1+2] to mean #3, so you can do array
indexing, and similarly ##2 is an indirection.  Valid variable numbers
in LinuxCNC range up to 5399.

LinuxCNC also suports named variables with names in `<>`: `#<_endmill_rad>
= [#<_endmill_dia>/2.0]`.  The leading `_` makes it global.  I'm not
sure how widespread this is.

Other operators include `**`, `MOD`, `AND`, `OR`, and `XOR`, the last of
which are for boolean values and have precedence below the comparisons
(see below).  Expressions also include function calls like `acos[0]`,
which answers in degrees.  LinuxCNC's other functions are sin, cos,
asin, sqrt, tan, ln, round, fup (ceiling), fix (floor), and abs, and it
has a weird form of atan2: `atan[y]/[x]`.

Control flow
------------

Line numbers are added with an N word at the beginning of the line and
are usually 4 digits (max in LinuxCNC is 5):

    N9922

In some controllers here's a WHILE statement; evidently relationals are
spelled out, like in Fortran but without dots:

    WHILE[#131LT#151]DO1  (I guess while #131 < #151)

Other relations include EQ, NE, GT, GE, and LE.

[The program I got that example from][1] doesn't seem to use FOR-NEXT so
maybe it doesn't exist.  I think DO1 matches up with a later END1 that
marks the end of that WHILE loop.  There's also IF GOTO, used here to
skip over a single line containing an assignment statement:

    IF[#147LT#161]GOTO2525
    #147=1
    N2525G1X[[#161*#166]-0.250-#166/2]Z#146F40.0

[1]: https://www.youtube.com/watch?v=_GkQ9GEPPbo

GOTO also exists without IF:

    GOTO9000 (can be used to exit a WHILE loop early)

This is deeply opposed to popular programming values, but I did write
a lot of code like this when I was a kid, in BASIC, so I have a certain
affection for this kind of thing.

Subroutines
-----------

[There seems to be a GOSUB][2]:

    M98 L11 P1779  (call subprogram 1779, looping 11 times)
    (...)
    M30 (program end; alternatively M2, program stop)

    O1779
    (...definition of subprogram 1779...)
    M99 (return from the subprogram)

[2]: https://gcodetutor.com/cnc-macro-programming/subprograms-m98-m99.html

Unless you're on a FANUC machine, you can apparently also say `M98 P51779`
to run subprogram 1779 5 times, same as `M98 P1779 L5` on a FANUC or
Haas.  This suggests that the subprogram numbers all need to be 4 digits.
The stack is guaranteed to be at least 4 levels deep.

It's kind of fun to think about this iteration count as an aspect of
subroutine calls.  On a conventional subroutine call stack, you could
implement this by pushing the subroutine entry point address onto the
stack four times on top of the normal return address, like Golang defer
statements, then jumping to it as usual, so that the subroutine will
return five times before finally ending its Groundhog Day nightmare.

Some machines, like Haas machines, allow you to call line numbers N1234 with
M97 P1234, or even with M98 P1234.  Since the various O1234 programs are
sometimes stuck into a single flat namespace, this can be more
convenient.

This doesn't seem to have any local variables though, or even parameters
or return values.  But in a sense the machine state, such as the current
position and feed rate, is a parameter and a return value, and of course
side effects happen, sometimes giving a new meaning to "destructive
updates".  G91, incremental coordinate mode, makes the coordinates in
each line relative rather than absolute, which can be undone with G90,
absolute coordinate mode.

On some controllers, though not Mach3 [or LinuxCNC][lcref], [G65 invokes a
"macro", which is a subroutine with parameters][3], so you can say `G65
X2.5 Y3.0 Z5.4` using the word arguments to load 2.5 into variable #24, 3.0
into variable #25, and 5.4 into variable #26, before invoking the macro.
(But, which macro?  I suspect a typo in the tutorial.)  Apparently you
can't have parameters G, L, N, O, or P, but the other English letters
are okay.

[lcref]: http://www.linuxcnc.org/docs/2.4/html/gcode_main.html
[3]: https://www.cnccookbook.com/m98-m99-g-code-cnc-subprograms/

LinuxCNC control flow
---------------------

However, [LinuxCNC has a different subroutine facility, with parameters,
and different control flow][ocode].  Subroutine O100 runs from a line
saying `O100 sub` to a line saying `O100 endsub`, which can have `O100
return` inside of it, and it's invoked by saying `O100 call` with up to
30 positional arguments each in square brackets, which become #1, #2,
etc; parameters up to #30 are restored on return.

[ocode]: http://www.linuxcnc.org/docs/2.4/html/gcode_main.html#cha:O-Codes

LinuxCNC also makes its named variables local to subroutines unless
their names begin with an `_`.

LinuxCNC also uses O words for other control flow; quoting the docs:

    (draw a sawtooth shape)
    F100
    #1 = 0
    O101 while [#1 lt 10]
    G1 X0
    G1 Y[#1/10] X1
    #1 = [#1+1]
    O101 endwhile

There's also an undocumented do-while structure which presumably does
the test at the bottom, an if-else-endif structure, and a
repeat/endrepeat:

    (Mill 5 diagonal shapes)
    G91 (Incremental mode)
    O103 repeat [5]
    ... (insert milling code here)
    G0 X1 Y1 (diagonal move to next position)
    O103 endrepeat
    G90 (Absolute mode)

LinuxCNC lets you name your subroutines with names as `O<somename>`
rather than numbers.

[Tormach][tormach] seems to use the LinuxCNC features and even has
copied bits of the LinuxCNC manual on their web site without
attribution.

[tormach]: https://tormach.com/subroutines-reference

GRBL tries to be LinuxCNC-compatible but I don't think it has control
flow.

Marlin
------

Marlin's G-code dialect is very nonstandard.  It uses M30 (program stop)
for "delete a specified file from SD [card]", for example.  I don't
think it supports variables, and its syntax is very nonstandard.

Debugging
---------

UGS, Universal Gcode Sender, can visualize toolpaths as well as sending
the G-code to an Arduino Uno running GRBL or something running TinyG,
g2core, or Smoothieware.  There's also some proprietary program that
does toolpath visualization, and I think LinuxCNC also has some kind
of toolpath visualizer.  The most common debugging tool, though, seems
to be to set the coordinate system origin well above your stock and
run your toolpath in the air, ready to smack the E-stop button if the
machine tries to crash.

Perversions
-----------

It would be pretty fun to use G-code to program something unrelated to
machining.  All the coordinate systems and movement commands seem like
they'd be applicable to drawing graphics.  You'd probably need some way
to handle strings, and you could repurpose Z-coordinates or tools or
something for colors.

In the other direction, things like the Mandelbrot set should be pretty
straightforward to program in G-code.
