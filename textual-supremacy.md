Graydon Hoare said in 02014, [“Always bet on text”][0]:

> I figured I should just post this somewhere so I can make future
> reference to how I feel about the matter, anytime someone asks me
> about such-and-such video, 3D, game or “dynamic” multimedia
> system. Don’t get me wrong, I like me some illustrations, photos,
> movies and music.
>
> But text wins by a mile. Text is everything. My thoughts on this are
> quite absolute: *text is the most powerful, useful, effective
> communication technology ever, period*.

[0]: https://graydon2.dreamwidth.org/193447.html

He goes on to explain this at greater length, which is worth reading.

Peter Deutsch once told me he was skeptical of visual programming
languages, though his reasoning was somewhat simpler and less
absolutist: he said he’d never seen a visual programming language with
visual density that was competitive with text.  I’m somewhat skeptical
that visual density is as dominant a criterion as all that (if it
were, we’d write our code in paragraphs rather than in indented
blocks) it does matter, and Graydon explains some other advantages of
text, in contexts not limited to programming languages: stability,
durability, flexibility, efficiency, social usefulness, indexing,
searching, translation, diffing, etc.

I feel that the recent astonishing capabilities observed in
large-language-model neural networks represent another great advantage
for textual programming languages: LLMs can process text, while they
can’t do much with dataflow diagrams, Scratch block assemblages, or
Godot node trees.  If you ask GPT-4 how to do something in Verilog,
C++, Python, Scheme, Bash, or Kubernetes, there’s a good chance that
it can answer you, and you can copy and paste its answer to test it.
When I’m configuring Godot to enable 2-D glow or make the camera
follow the player character, it’s comparatively helpless.

I’ve been spending a lot of time over the last few weeks building
things in Godot, you see.  To make a 2-D platformer, you can create a
CharacterBody2D node with a script to handle keys and child nodes to
give it a hitbox and an appearance; for example, an AnimatedSprite2D
and a CollisionShape2D.  In my world.tscn file the node tree looks
like this (edited slightly for clarity):

    [node name="CharacterBody2D" type="CharacterBody2D" parent="."]
    script = ExtResource("1_xd74f")
    metadata/_edit_group_ = true

    [ext_resource type="Script" path="res://CharacterBody2D.gd" id="1_xd74f"]

    [node name="CollisionShape2D" type="CollisionShape2D" parent="CharacterBody2D"]
    position = Vector2(0, 2)
    shape = SubResource("RectangleShape2D_jef47")

    [sub_resource type="RectangleShape2D" id="RectangleShape2D_jef47"]
    size = Vector2(10, 12)

    [node name="AnimatedSprite2D" type="AnimatedSprite2D" parent="CharacterBody2D"]
    sprite_frames = SubResource("SpriteFrames_jojtn")
    autoplay = "default"
    frame_progress = 0.483333

    [sub_resource type="SpriteFrames" id="SpriteFrames_jojtn"]
    animations = [{
    "frames": [{
    "duration": 1.0,
    "texture": SubResource("AtlasTexture_aubw4")
    }, {
    "duration": 1.0,
    "texture": SubResource("AtlasTexture_2trdu")
    }, {
    "duration": 1.0,
    "texture": SubResource("AtlasTexture_1fy41")
    }, {
    "duration": 1.0,
    "texture": SubResource("AtlasTexture_ei7x0")
    }, {
    "duration": 1.0,
    "texture": SubResource("AtlasTexture_goo6q")
    }],
    "loop": true,
    "name": &"default",
    "speed": 8.0
    }]

    [sub_resource type="AtlasTexture" id="AtlasTexture_aubw4"]
    atlas = ExtResource("1_hl4j5")
    region = Rect2(0, 289, 16, 16)

    [sub_resource type="AtlasTexture" id="AtlasTexture_2trdu"]
    atlas = ExtResource("1_hl4j5")
    region = Rect2(17, 289, 16, 16)

    [sub_resource type="AtlasTexture" id="AtlasTexture_1fy41"]
    atlas = ExtResource("1_hl4j5")
    region = Rect2(34, 289, 16, 16)

    [sub_resource type="AtlasTexture" id="AtlasTexture_ei7x0"]
    atlas = ExtResource("1_hl4j5")
    region = Rect2(51, 289, 16, 16)

    [sub_resource type="AtlasTexture" id="AtlasTexture_goo6q"]
    atlas = ExtResource("1_hl4j5")
    region = Rect2(68, 289, 16, 16)

    [ext_resource type="Texture2D" uid="uid://c4gmd28od2dih" path="res://Tilemap/monochrome_tilemap_transparent.png" id="1_hl4j5"]

This is plainly not something you would want to type (the structure is
quite similar to a PDF file), and indeed I didn’t type it, I
interacted with Godot’s GUI.  To create the frames of the sprite, I
opened the sprite atlas, set the tile parameters to 16×16 with 1×1
separation, and clicked in sequence on the character frames I wanted.
To set `autoplay = "default"` I clicked on the autoplay button in the
animated sprite editor, though it took me a long time to realize that
there was an autoplay button.

The scene textual representation above is, I think, to enable Git to
diff and merge the scene sensibly; it’s not intended as an editing
user interface.  Note, though, that it omits parameters whose values
are just the default.

Godot comes with this canned character script, which I saved in
CharacterBody2D.gd (mentioned above); it gives you a simple platformer
without any further modification:

    extends CharacterBody2D


    const SPEED = 300.0

    # Get the gravity from the project settings to be synced with RigidBody nodes.
    var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


    func _physics_process(delta):
        # Add the gravity.
        if not is_on_floor():
            velocity.y += gravity * delta

        # Handle jump.
        if Input.is_action_just_pressed("ui_accept") and is_on_floor():
            velocity.y = JUMP_VELOCITY

        # Get the input direction and handle the movement/deceleration.
        # As good practice, you should replace UI actions with custom gameplay actions.
        var direction = Input.get_axis("ui_left", "ui_right")
        if direction:

            velocity.x = direction * SPEED
        else:
            velocity.x = move_toward(velocity.x, 0, SPEED)

        move_and_slide()

You could imagine a similar kind of representation for the node tree,
maybe something like this, which captures everything interesting about
the above node definitions:

    A = $spriteatlas size=[16, 16] sep=[1, 1]:
        path("res://Tilemap/monochrome_tilemap_transparent.png")

    $cbody2d metadata/_edit_group_:
        script("res://CharacterBody2D.gd")
        $hitbox pos=[0, 2] shape=rect(size=[10, 12])
        $asprite2d autoplay=&"default" frame_progress=0.483:
            animation loop name=&"default" speed=8:
                frames:
                    sprite A, 0, 17
                    sprite A, 1, 17
                    sprite A, 2, 17
                    sprite A, 3, 17
                    sprite A, 4, 17

This isn’t *quite* the same because it substitutes a $spriteatlas
node, with the information about the tile size and spacing, for the
Texture2D.  This might be a dumb idea (probably at least you should be
able to explicitly share the same Texture2D between multiple tilesets)
but it’s kind of a pain that right now I have to respecify the tile
size every time I create another animation using more frames from the
same PNG.  Anyway, something like this could work.

Note that this is *not a program*: it’s a static data structure
describing the initial state of some game data.  There are no loops,
conditionals, control flow, or subroutine calls.  Just because it’s
written in fixed-width text doesn’t mean it’s code.

This representation is probably not as reliably mergeable with Git,
because it will have big diffs if you move nodes around the tree.  But
it’s certainly a lot easier to read.  But you still can’t tell by
reading it whether it says what you want it to say.  You have to look
at the PNG with stuff overlaid on it to see if the frames are right
and if there are others you want to add, you have to watch the
animation to know if 8 is the right speed, and you *certainly* don’t
want to type “sprite A, 4, 17 RET” instead of just clicking on the
fifth frame of the animation on the eighteenth row of your texture
atlas.

Such IDE features could be provided *in addition to* the textual
representation, not *instead of* it.

And I think a textual representation like this could be useful for a
variety of purposes.  It makes it possible to see at a single moment
everything you have done to create a particular game object — or that
someone else had to do to create it.  And it would be a much more
useful medium for things like GPT-4.  When I asked it how to do this,
it answered, in part:

> Creating a player character that can run and jump in Godot involves
> several steps. Below is a step-by-step guide to help you set up the
> basic functionality using GDScript.
> 
> 1. **Create a new scene for your player:**
>     - Open Godot and create a new project.
>     - Create a new 2D scene.
>     - Add a `KinematicBody2D` node as the root of your player scene.
>     - Add a `Sprite` node as a child of the `KinematicBody2D` node to
>       represent your player visually.
>     - Add a `CollisionShape2D` node as another child of the
>       `KinematicBody2D`. Set its shape to match your player’s sprite
>       (e.g., RectangleShape2D).
> 
> 2. **Set up input actions:**
>     - Go to "Project" -> "Project Settings" -> "Input Map".
>     - Add new actions called "`move_left`", "`move_right`", and "`jump`".
>     - Assign keys to these actions (e.g., Arrow Left for `move_left`,
>       Arrow Right for `move_right`, Space for jump).
> 
> 3. **Attach script to the KinematicBody2D:**
>     - Select the `KinematicBody2D` node and click on the “Attach Script” button.
>     - Name it something like `Player.gd`.
> 
> 4. **Write the script:**
> ...

This is 168 words, more than twice as long as my strawman above, and
it doesn’t even explain how to create the Sprite.  (KinematicBody2D is
the old name for CharacterBody2D, so as far as I know, this is correct
as far as it goes for Godot 3.)  The worst part, though, is that you
can’t copy and paste it into Godot; you have to try to follow the
instructions yourself, and they leave a lot of detail out.  The
textual format for the game tree data leaves no detail out; it
contains everything you need.  The same problems that plague GPT-4
here also plague the Stack Overflow data it was trained on.

So, to learn about how to do things like this, we’re largely forced to
rely on video screencasts, which are, as Graydon pointed out,
unstable, ephemeral, inflexible, inefficient, hard to index, hard to
search, hard to diff, etc.

Suppose instead that we subscribe to Textual Supremacy, treating other
representations of our game object tree or whatever as second-class
citizens.  We center everything on a text editor, but when the text
editor’s focus is on, say, an animation, we display the animation in a
separate preview pane, where we have buttons and sliders and whatnot
for more direct manipulation of the animation; these gewgaws work by
altering the text, a modification which is reflected in the preview
pane.

In Textual Supremacy, you might have context-sensitive hotkeys to
interact with the preview pane, different pinned preview panes linked
to different parts of your text-editing buffer, and so on, but
fundamentally the underlying source code is text, and what you’re
doing is that you’re editing the text.

“Text” doesn’t have to mean fixed-width ASCII text.  You could include
Unicode, control characters, escape sequences, and so on.  Control
characters and escape sequences could affect its display by hiding
part of the text, changing its font, changing its layout, and so on.
To some extent, extra complexity like this reduces some of the
advantages of text: its gulfs of evaluation and of execution are
minimal, it’s easy to copy and paste text into email or web pages, it
can be indexed, etc.  But it’s vanishingly unlikely that the Teletype
Corporation hit on the optimal repertoire of graphic characters and
formatting operations in 01963, so we should keep an open mind.
