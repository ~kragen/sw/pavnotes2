I've been thinking about dynamically bounds-checked virtual machines
like Smalltalk-80 and the B5000 design, which strictly distinguish
between object capabilities (references) and binary data.  In the Lisp
memory model, where objects form a graph linked by pointers, this is
pretty straightforward; every once in a while you have an object that
contains bytes instead of references, and you can indicate that in any
of the usual ways: a pointer tag bit, an object header bit, segregated
memory ranges, or NaN boxing, for example.  This admirably covers
languages like Python, JS, Java, Smalltalk, OCaml, and so on.

In the most simplified version of what I've elsewhere called the
"COBOL memory model", memory is instead subdivided recursively into
arrays or records.  Primitive types are things like characters,
floating-point numbers, and binary (or originally decimal) integers of
fixed sizes; compound types are records (structs) and fixed-size
arrays, built up from smaller types.

The COBOL memory model has some real advantages, especially in constrained
memory and when it's augmented with pointers.  By physically nesting
records or arrays inside other records or arrays in most cases, instead
of linking them together with pointers, you gain the following advantages:

1. Less work for the memory allocator: fewer calls to the allocator,
   fewer objects and pointers for a garbage collector to trace if
   you're using one, fewer objects to track the reference counts of if
   you're doing that, and fewer calls to an explicit deallocation
   subroutine if you have one of those.  This also entails less
   bug-prone memory deallocation on error recovery paths when you
   aren't using a GC.

2. Less space usage, especially on modern machines where pointers are
   commonly 32 or 64 bits, because you don't have to dedicate space to
   the pointers.

3. Simpler arguments for type-correctness and avoiding uninitialized
   values.  If a Rect consists of two pointers to Points `struct Rect
   { struct Point { int x, y; } *start, *extent; };`, it's possible in
   many languages for those pointers to be nil or to point to an
   object of the wrong type, but if a Rect contains two Points nested
   inside of it `struct Rect { struct Point { int x, y; } start,
   extent; };, that can't happen.

4. Nested objects can't alias each other, eliminating some bugs.

Of course, when you *want* a "child object" to alias, be nullable, or
be dynamically typed, you still have to use a pointer.  But you can
use a pointer only in the cases where you need it instead of for every
relationship between two objects.  (In the Hadix system I'm working
on, you might also want to keep your mutable allocations small because
every access to them entails copying the whole allocation.)

In C you often end up with definitions like this, from jpeglib.h:

    typedef struct {
      int component_id;             /* identifier for this component (0..255) */
      int component_index;          /* its index in SOF or cinfo->comp_info[] */
      int h_samp_factor;            /* horizontal sampling factor (1..4) */
      int v_samp_factor;            /* vertical sampling factor (1..4) */
      int quant_tbl_no;             /* quantization table selector (0..3) */
      int dc_tbl_no;                /* DC entropy table selector (0..3) */
      int ac_tbl_no;                /* AC entropy table selector (0..3) */
      // ...
      JQUANT_TBL *quant_table;
      void *dct_table;
    } jpeg_component_info;

This struct has a bunch of integers in it and also two pointers.  It's
often desirable to represent the integers as binary data rather than
as tagged pointers or pointers to boxed integers; it means less work
for the garbage collector, it extends their range, it helps the
debugger display them properly, and it often means they take up half,
a quarter, or an eighth as much memory.  Handling this in languages
like C is awkward; you can have the compiler emit struct layout
information that is used by the debugger and perhaps the GC to figure
out which parts of an allocation have pointers and which don't.  And
Java does do that kind of thing, though it doesn't support nesting.

But for the virtual machines I'm thinking about, I think life will be
a lot simpler if we avoid thus intimately mixing pointers and binary
data like integers.  I want to put my pointers in one allocation and
my binary data in another, and I want to do it without giving up
entirely on nesting.

The straightforward thing to do, what I'll call Scheme A, is to
rewrite definitions like the above into all-pointer definitions like
this:

    typedef struct {
      struct _binary_jpeg_component_info {
        int component_id;
        int component_index;
        int h_samp_factor;  
        int v_samp_factor;  
        int quant_tbl_no;   
        int dc_tbl_no;      
        int ac_tbl_no;      
        // ...
      } *_binary_fields;
      JQUANT_TBL *quant_table;
      void *dct_table;
    } jpeg_component_info;

Then nesting works fine with all-pointer or all-binary records, just
as in C.  Each record field has an offset and a size, the offsets
being the prefix sums of (zero followed by) the sizes, and the size of
the whole record is the sum of its fields' sizes.  This allows you to
pack an arbitrarily nested data structure into a single memory
allocation as long as it's all-pointer or all-binary.

You still have the problem that an array of ten Foo objects that have
both pointers and a binary part split out into a subrecord linked by a
pointer will require eleven allocations (one of pointers, ten of
binary) instead of one allocation.

An alternative I'll call Scheme B is to store such data structures in
a *pair* of allocations, one for pointers and one for binary data.  In
a variant called Scheme C, these are lumped together into a single
object, as on the iAPX432, where pointers ("access descriptors") were
physically stored at negative offsets from the base pointer, and
binary data was stored at positive offsets.  On the iAPX432,
microcoded instructions ensured that only the correct kind of data was
fetched or stored to each side.  Smalltalk-80 had a special case that
handled method objects in a somewhat similar way: the method object
began with an object header, a method header, and zero or more
literal-pool references, and then the rest of it was binary bytecodes.
You could imagine every allocation having a "binary part" and a
"pointer part", and stacking up field offsets independently for the
two parts, or you could just use a pair of pointers to denote a
reference instead of a single pointer.  In either Scheme B or Scheme C
the "offset" of a mixed field is a pair of ints instead of a single
int.

I think you can handle the efficiency problem of arrays of mixed
records by changing your code to unmix the records when you run into
it, so this double-barrelled Scheme B/C approach seems worse to me
than just ghettoizing the binary fields of a mixed record (Scheme A),
but it would also work.

In the sorts of virtual machines I'm thinking about, all memory access
is dynamically bounds-checked.  There's a fetch(region, index)
operation to read memory XXX

Normally you size an allocation to fit a record or an array of some
definite length, and you can use a pointer to the allocation as a XXX

In Scheme A, if you want to pass a (non-root) record by reference to a
subroutine, you need to pass a *slice* of a memory region, which might
be realized as a type (bytes or pointers), a base address, and a
limit.  In Scheme B or C, you need two slices; the difference is XXX
