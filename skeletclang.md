Skeletal Tclish thing
=======



Yossi Kreinin makes a good case that
for a user interface language
it is better to type

    break yyparse

than "pop infix"

    break("yyparse")

with variables,
not literals,
being the thing bearing
special sigils:

    break $func

But $ is a rather rebarbative sigil;
something calmer would be better.
Lisp quasiquote uses `,`.
A related question is how to do
property access:

    moveTo(p1.x, p1.y);

It may be relevant
that property access is rarely useful on literals
because rather than `"hello".length`
you can just write `5`.
Perhaps you could use
a single sigil
to indicate variableness and property access,
echoing the Unix shell:

    moveTo p1/x p1/y
    print p1/
    end = window/rect/end
    lineTo end/x end/y

It may be hard to compromise
between user interface languages
and programming languages,
but very plausibly we can do better than Tcl,
a user interface language,
and Python, a programming language.

This does suffer from `p1/x/`
as an alias to `p1/x`,
and from `/` being a bit conspicuous,
but I think the traditional `.`
is too subtle at the end of the line.
Possible alternatives
include `•`, `|`, `:`, and `>`.
If the sigil leads
instead of trailing,
the above requires more characters
but could use even `.`,
like Tk or BLISS:

    moveTo .p1.x .p1.y
    print .p1
    .end = .window.rect.end
    lineTo .end.x .end.y

This seems like a very good solution
which also admits the possibility of infix operators
and hyphens.
Moreover, it's a Lisp-2:

    end .w = .w.end  # local subroutine def
    end = .w.end     # subroutine with no arguments
    .end = .w.end    # local variable def

Part of the Zen of Tcl
is being able to define
your own control structures.
Block arguments
provide a more structured way to do this.
Probably in 02023
we have to use `{}`and not `[]`, `()`, `<>`,
`⟨⟩`, `‹›` or `«»`.
But you definitely want them
to be able to take arguments.

    foreach .items .item => { print .item }

It may be a bit confusing to use `{}`
also for multiline subroutines:

    find .items .key = {
        foreach .items .item => {
            if .item.key == .key { return .item }
        }
    }

The leading sigil
and lexical scoping
makes it possible for aggressive ido-style
completion to work,
accepted potentially
by pressing space or period,
with a special key to indicate
that you really do want to use
an undeclared identifier.
This would allow common commands
to be a single letter and space,
without compromising their readability.
Grammars for more complex commands
might be written as PEGs
with nesting and literal tags for introducers.
This would permit all kinds of useful CLI
tab-completion integration.

Doing this for property names
requires some kind of static typing.
But I think Golang-style
explicit static typing at subroutine entry points
with type inference within each subroutine
may be more ergonomic anyway.
(For my `find` example
it may be a bit tricky!)

Data types might also perhaps be defined
as PEGs.
