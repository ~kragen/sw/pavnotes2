I compiled some code for RV32EC.  Here are 8 instructions of it
occupying 22 bytes.  What would this look like in a stack bytecode?

      38:   00158693                addi    a3,a1,1             a1 1 +                    3 bytecodes
      3c:   43d8                    lw      a4,4(a5)            a5 4 + @                  4
      3e:   08e6fa63                bgeu    a3,a4,d2 <.L15>     < if <.L15>               3 bytecode bytes including jump target
      42:   439c                    lw      a5,0(a5)            a5 @                      2
      44:   058a                    slli    a1,a1,0x2           a1 2 lshift               3
      46:   7179                    addi    sp,sp,-48           literal 48 stackframe     3
      48:   97ae                    add     a5,a5,a1            +                         1
      4a:   0007a303                lw      t1,0(a5)            @                         1

We can see that in a stack instruction set this would use about 20
bytes.  On the other hand, some of the code is occupied with doing
things like allocating a stack frame, which usually isn’t necessary on
a stack machine.
