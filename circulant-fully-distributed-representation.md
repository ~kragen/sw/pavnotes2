Thinking about low-power radio communications,
an idea occurred to me which I think
finally solved an algorithmic riddle that’s been puzzling me since
last millennium: how to efficiently decode retrieval results from
Pentti Kanerva’s “[fully distributed representation][Kanerva]”.

[Kanerva]: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.2.9479&rep=rep1&type=pdf

Kanerva’s fully distributed representation
------------------------------------------

Last millennium Pentti Kanerva proposed “fully distributed
representation”, in which you represent each atomic concept as a
random bitvector of some large size, such as 16384 elements (2
kibibytes).  There seems to have ensued a small field of
“hyperdimensional computing” or “vector symbolic architectures”, with
some 83 papers citing the FDR paper on Google Scholar.  There is a
survey from 02021 ([part 1], [part 2]) by Kleyko, Rachkovskij, Osipov,
and Rahimi, and Kanerva is apparently still active in the area.

[part 1]: https://arxiv.org/pdf/2111.06077.pdf
[part 2]: https://arxiv.org/pdf/2112.15424.pdf

Kanerva proposed two bitwise operations for combining these atomic
concepts; he called them ⊗ and [+], but for ASCII-friendliness let’s
call them / and +.

The / operation, Kanerva’s ⊗ or “binding”, is bitwise XOR:
A / B gives you the bitwise XOR of A
and B.  This combines the two values into a new value of the same
size, from which you can retrieve A given B, or B given A.  A/B/C
gives you a value from which you can retrieve A given B and C, or B
given A and C, or C given A and B.  (XOR is of course commutative and
associative, despite the choice of “/”, but as a compensating virtue,
it has a division-like cancelation property: (A/B)/(C/B) = A/C, and
(A/B)/(A/C) = C/B.)  Another interesting property of XOR is that, if B
has exactly half of its bits set, A/B has exactly zero correlation
with A: exactly half of its bits will be equal to the corresponding A
bits, while half will be unequal.

The + operation, which has lower precedence, is a majority-rule thing
Kanerva calls “thresholding” or “chunking”.
Where A and B have the same bit, A + B has that bit too, but where
they differ, + breaks the tie by producing a random value.  So, if A
and B are unrelated, A + B will be highly correlated with both A and
B: 75% of its bits will be equal to A’s bits, and 75% will be equal to
B’s bits.  You can extend this to combining several values, A + B + C
+ D + E, producing for each bit the majority vote for that bit,
breaking ties at random if there are an even number of values.
(Decomposing this as a chain of binary additions has much worse
results.)

So, if you have a value M computed as M = K1/V1 + K2/V2 + K3/V3, M/K1
has 75% of its bits equal to V1, and has almost exactly 50% of its
bits equal to K1, K2, V2, V3, etc.  Similarly, M/V1 retrieves a noisy
version of K1.  If you have a set of candidate values Ci that might be
stored in M under key K1, you can correlate M/K1 with Ci for each of
them and figure out which one it is.  So you can think of it as
storing a number of associations, potentially quite a large number,
because by using enough bits you can reduce the probability that
random chance will give rise to a spurious correlation of 75% or 62.5%
or 56.25% or 53.125% or 51.5625% to any arbitrarily low chance.

In cases where a retrieval result is too weak, you can get a more
reliable signal by attempting to recall it through multiple avenues.
If M = K1/V1 + K2/V2 + K3/V3, then M/K1 will have a significant
correlation with V1, but M/K1 + M/K2 will have a stronger correlation
with it.

In some cases you might want to set up a memory V to be retrieved when
most of a large set of relevant circumstances is present, say C1 + C2
+ C3 + C4.  In that case you can include V/(C1 + C2 + C3 + C4) as an
item in your memory M; it will associate V weakly with each of C1, C2,
C3, and C4 (M/C1, M/C2, etc., will have correlations better than
chance), but much more strongly with the conjunction of most or all of
them.

### Properties of Kanerva’s representation ###

This data structure has some interesting properties suggestive of how
brains function:

1. Like a Bloom filter, erasing any given part of it does not erase
any of the individual pieces of information stored in it; it only
increases the probability of error.  Every datum is stored in a sort
of holographic way across all the bit positions.  (Apparently it has
some sort of relationships with things called “holographic reduced
representation” and “spatter coding”.)

2. Storing an association in it many times reduces the probability of
error on retrieving that association.

3. Like a Bloom filter, storing more data in it doesn’t take more
space, but it increases the chance of error from retrieving previously
stored data.

4. The associations it stores are nondirectional; you can retrieve (a
noisy version of) V1 given K1, or you can retrieve K1 given V1, etc.

5. Many computations on it are potentially massively parallel.

6. This thing about recalling the same datum more reliably if you have
multiple retrieval paths to it is *damn* suggestive.

### Some error probability calculations ###

Taking 16834 bits as an example, correlations of 75%, 62.5%, 56.25%,
53.125%, and 51.5625% work out to being equal in 12288 bits, 10240
bits, 9216 bits, 8704 bits, and 8448 bits, respectively; the
probability of the first of these happening by chance is too small for
R’s `pbinom` function to calculate.  The others have calculable
chances, but all but the last are very small:

    > pbinom(16384*(1-c(75, 62.5, 56.25, 53.125, 51.5625)/100), 16384, .5)
    [1]  0.000000e+00 2.960905e-227  5.182207e-58  6.492011e-16  3.269142e-05

So, for example, the chance of 53.125% of the bits being equal by
chance is about 6e-16, or 6e-14%.  The chance of this happening to any
of 16384 atoms is about 1e-11.  The last correlation, 51.5625%, will
arise by chance in 3.3e-5 of all cases, so if you have 16384 atoms
then one of them will correlate with a given random bitstring at that
level with probability 0.41, but if you only have 1024 atoms then the
probability is only 0.033:

    > 1-(1-3.269e-5)**c(16384, 1024)
    [1] 0.41468301 0.03292102

If you are a 65th association being added to a group of 64 existing
associations, then whenever those 64 are exactly tied, you will get to
break the tie; those tie-breaking cases are the ones where your
correlation is created.  Considering a given bit position *i*, the
probability that they are tied is about 10%:

    > pbinom(32, 64, .5) - pbinom(31, 64, .5)
    [1] 0.09934675

I thought this meant we should expect the majority rule of 65 random
bitstrings (of any length) to correlate with each of them at a level
of about 60%.  However, this calculation is wrong, because the
following calculation with numpy yields an average correlation of
0.5497, 54.97%:

    assocs = random.random_sample((65, 1000000)) > 0.5
    votes = assocs.sum(axis=0)
    ((votes > len(assocs) // 2) == assocs).sum(axis=1).mean() / len(thresh)

(Changing 1000000 to some other number affects the calculation speed
and precision, but it stays around 0.5497.)  Upon reflection, the
reason is that half the 9.93% of previously tied bits were already
(randomly) in agreement with this 65th bitvector.  The correct
calculation is:

    > .5 + (pbinom(32, 64, .5) - pbinom(31, 64, .5))/2
    [1] 0.5496734

For 257, 1025, 2049, and 4097 random bitstrings being thresholded, the
expected correlations are 52.5%, 51.25%, 50.88%, and 50.62%:

    > xc <- function(n) (1 + pbinom(n/2, n, .5) - pbinom(n/2-1, n, .5))/2
    > xc(c(64, 256, 1024, 2048, 4096))
    [1] 0.5496734 0.5249096 0.5124639 0.5088144 0.5062331

The probabilities of a spurious correlation of these magnitudes on
16384 independent bits are:

    > pbinom(16384*(1-xc(c(64, 256, 1024, 2048, 4096))), 16384, .5)
    [1] 2.249933e-37 8.620205e-11 6.980353e-04 1.197721e-02 5.462478e-02

1.2% error per correlation from a memory containing 2048 associations
means that with more than 80 or so candidate atoms you’ll have more
false hits than true ones.  0.07% error per correlation from a memory
containing 1024 associations means that this won’t happen until more
than 1400 candidate atoms.  8.6e-11 error means that with 256
associations you can distinguish reliably among billions of candidate
atoms.

With four times as many bits (65536 bits, 8 KiB) the probabilities of
such large spurious correlations are orders of magnitude lower:

    > pbinom(65536*(1-xc(c(64, 256, 1024, 2048, 4096))), 65536, .5)
    [1] 3.142737e-143  1.433444e-37  8.899130e-11  3.213538e-06  7.078189e-04

This suggests that FDR can, with probably no errors, store a couple
thousand associations between a few dozen atoms in 2 KiB of memory, or
a thousand or so associations between a thousand atoms or so, or a few
hundred associations between billions of atoms; if this is correct,
this is a decent level of storage density, maybe a little higher than
you’d expect with traditional representations.  On
information-theoretic grounds it’s probably hard to do much *better*
than that but the surprising thing is that it’s not much *worse*.

### The decoding problem ###

However, we’re stuck with the problem of how to identify the atomic
concept, or concepts, that resulted from a retrieval operation.  How
do we efficiently figure out which of our various candidate atoms has
correlation 51% rather than 50.1% (if you have 1590 associations
stored), or 50.1% instead of 50.01% (if you have 159150 stored), or
50.01% instead of 50.001% (if you have 15.9 million stored)?

The correlation is a normalized dot product between the atom and the
retrieval output, so we could think of multiplying the retrieval
output through a matrix of all the atoms to calculate the dot product
for each atom.  We’d just like a way to do this without doing *NM*
operations, where *N* is the number of bits and *M* is the number of
atoms.  Kanerva says (p. 360, 3/8; p. 364, 7/8):

> We can think of **Pat’** as a noisy version of **Pat** that needs
  cleaning up.  For that purpose the system has a clean-up memory that
  keeps track of all valid codewords&mdash;ones that have been defined
  so far&mdash;and that takes noisy codewords as inputs and produces
  noise-free codewords as outputs. ... The clean-up memory could be a
  table of valid codewords and a procedure for finding the
  best-matching table entry for any noisy codeword, or it could be an
  autoassociative neural net that stores valid codewords as point
  attractors.  For the neural net to work well, however, it must have
  large and regular basins of attraction, because the amount of noise
  that needs cleaning up is easily 35% (see Fig. 4).  However, there
  is a limit to how much noise can be cleaned up reliably, which in
  turn limits the number of parts that can be chunked at once into a
  holistic record if such a record is to be analyzable.

As far as I know nobody has had success with the neural-net approach
yet.

After 22 years of thinking about the problem on and off, I think I
have finally found a solution.

Fourier diagonalization for linearithmic decoding of Kanerva’s representation
-----------------------------------------------------------------------------

It occurred to me tonight that the Fourier transform diagonalizes
circulant matrices, the fast Fourier transform can be calculated in
linearithmic time, and there are circulant matrices with orthogonal
rows, for example those where a row is the output of
an entire period of a maximal LFSR.
So if we use different shifts of such a period as our atoms, we can
simultaneously correlate a retrieval result with all the atoms by
performing Fourier convolution on it.  The output stream of a
maximal-length LFSR also has the desirable property of having almost
precisely half ones and half zeroes (specifically 2<sup>*n*-1</sup>
ones and 2<sup>*n*-1</sup>-1 zeroes, the “balance property” of
maximum-length sequences.)  Appending an extra 0 to the sequence to
get a power-of-2 size that’s especially simple and efficient to FFT
will not change its autocorrelation properties much; in fact, it may
even improve them.

That is, when we set up the system, we compute the discrete Fourier
transform of our LFSR result, getting an amplitude and phase for each
of its 16384 or however many frequencies.  We conjugate this to get a
time-reversed frequency-domain filter.  To analyze a retrieval result,
we first do the discrete Fourier transform on it, then do an
elementwise complex multiplication by the frequency-domain filter to
attenuate frequencies that carry little information and phase-shift
the others, and then finally do an inverse DFT on it to get a
“time-domain” signal.  This will have spikes at one or more locations
indicating atoms with which the retrieval result had a significant
correlation.  (Or, it won’t have large spikes, just low-level random
noise, if there’s no result to be retrieved.)

IIRC the DFT, forward or inverse, requires 2*N* lg *N* complex
multiplications for a power-of-2-sized signal of *N* samples.  In the
*N* = 16384 case, lg *N* = 14, so we need 28 complex multiplications
per sample per DFT, or 112 real multiplications, or 224 real
multiplications for both the forward and reverse, or 3.7 million
multiplications in total.  This is significantly less than a
millisecond of computation on a modern desktop computer.

In 01975 Cohn and Lempel found a way to do this correlation
specifically for LFSR outputs that’s one to two orders of magnitude
faster than the Fourier approach.  See file `m-sequence-transforms.md`
for details.

Incremental updates
-------------------

If you want to add a new association Kn/Vn to an existing memory state
M, you can flip some random subset of the bits in M that don’t match
the corresponding bits in Kn/Vn.  In some sense maybe it would be
ideal to flip them with a probability inversely proportional to the
number of already-stored items, but depending on what you’re doing it
might be better to use some other probability.

Erasing an association from the store can be done by flipping enough
of the bits coinciding with it to get the correlation back down to
50%, but of course this erasure operation increases the chance of
error, just as storing more data would.

These approaches will give you higher error rates than the approach of
computing batch-mode majority rule among a large number of bitstrings,
because even if you flip the same number of bits, you’re not going to
be able to flip exactly the bits where the other stored associations
were tied.  A possible approach to improving this situation still
represents each association as a bit string but replaces each bit in
the memory with, for example, a 4-bit counter which is initially 7.
To update the memory, you stochastically increase the counters
corresponding to bits that are 1 and decrease those corresponding to
bits that are 0; the probability of actually incrementing a counter
with value *v* if you’re trying to increase it is proportional to (1 +
*v*)(15 - *v*), and the probability of decrementing it if you’re
trying to decrease it is proportional to *v*(16 - *v).  The idea is
that the counters keep an approximate count of how “tied” they are,
and “more tied” counters are easier to move.  I haven’t analyzed the
performance of this variant.

Representational techniques
---------------------------

Most programming language data structure storage and retrieval can be
modeled with RDF-like triples: entity.attribute = value.  This can be
modeled in FDR by assigning two atoms to each entity, an “outer” atom
O and an “inner” atom “I”, and one atom to each attribute, A, and a
distinguished atom E.  The “outer” atom can occur as “values”, and it
is linked to the “inner” atom by storing I/E/O in the store.  Then
each triple I.A = V is linked by storing I/A/V in the store, where V
is probably the outer atom of some other entity.

This permits storing an arbitrary number of values for each I/A pair,
with no ordering between them, but with relative strengths that change
over time.  As mentioned above, though, erasure is destructive, so you
probably don’t want to store short-lived data in the same memory as
long-lived data.

Sequences can be stored in this way with “car” and “cdr” attributes,
but in many cases you’d want to store the actual sequence as the
leaves of a tree, so that nodes closer to the root can be stored with
higher confidence, rather than as a singly-linked list which can
easily break.

Reinforcement learning can be handled directly by strengthening and
weakening associations rather than by attempting to erase and rewrite
previously stored numbers.  More generally, since the strength of the
retrieved association is available, all kinds of prediction might be
able to abuse that strength as a sort of indicator of probability or
confidence.

Strong associations can be retrieved by doing the computation over
only a small part of the entire bitvector, while retrieving a weaker
association reliably might require examining a large part.  It’s not
clear to me how to generalize the Fourier (or Hadamard) approach to work directly on
a short segment of the signal, but you could reasonably maintain
several different sizes of FDR and update each of them separately (for
example, one of 2 KiB, one of 64 KiB, and one of 2 MiB); it’s worth
mentioning in this context that the bandwidth into human long-term
memory seems to be about half a bit per second.

Kanerva's intent seems to have been to store only a few associations
in each bitvector, despite the enormous space cost:

> The examples are not meant to suggest that traditional data bases
  should be encoded in this holistic manner.  These examples work
  because the records have few fields.  If the fields are too many,
  extracting them from a holistic record is unreliable.

He proposes that the "chunked" values should be added to the cleanup
memory as an additional valid codeword, which is of course impossible
with the circulant-matrix approach, since the cleanup "memory" is not
updatable.  Thus my introduction of the "inner"/"outer" distinction
above for associativity within a single memory.

Rubber-hose cryptography
------------------------

Another interesting feature of FDR is that it is difficult to find out
what might be in it.  If you don't know K1 or V1 then K1/V1 will
appear to be just random noise; indeed, even if you know K1 but don't
have a way to recognize V1 it also might, though if you're using an
LFSR this may not be very trustworthy.  The feature that its size
doesn't change when you store more data in it, only its error rate,
means that it is impossible to show that you have retrieved everything
possible from it.

An encrypted overlay of an FDR might XOR the stored data with the
output stream from AES-CTR (on both reads and writes); this encrypted
overlay acts much like the original FDR with respect to error-rate
increase from interference from other encrypted overlays and any
unencrypted FDR in the same space.  In this way even an attacker who
knows K1 or V1, or knows they are M-sequences from a given LFSR, will
have no advantage.

This may be useful for a filesystem that is resistant to "rubber-hose
cryptography": by its nature it has a lot of random noise, and there
is no way to know how much random noise it *ought* to contain, and
some of the noise may be due to data that is now so faded as to be
irretrievable.  Lowering the detection threshold sufficiently will
result in retrieving spurious memories, as in recovered-memory therapy
and hypnosis.  So if some of the random noise is due to data stored
under an encryption key that has not been recovered, there is no
reliable way to determine this.
