You are going to die.  So the idea that you have something to lose,
something that you could retain if it were not taken from you, is a
delusion.  There is nothing you can retain; everything you have is
ephemeral.  If you look into it deeply you will see that even before
you die it does not remain the same from one moment to the next; it is
constantly changing, and there is nothing anyone can do to stop that.

It is common for us to become angry because we feel that someone has
taken something from us, something we valued.  But the things we feel
have been taken from us are things we never had, things we could never
have had in the first place by their very nature.  So this is a
foolish reason to become angry.  It is not the fault of the other
person that we could not have those things.  It is just the nature of
existence.

As there is nothing to lose, there is also nothing to gain, nothing to
strive for.
