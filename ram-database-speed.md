I’m typing these notes on a handheld GPD MicroPC computer with a
quad-core 1.1-GHz Celeron N4120, capable of something like 8 billion
instructions and 32 billion floating-point operations per second.  In
100 milliseconds it can examine something like 3 gibibytes of RAM (and
it only has 8) (this was wrong, see below) or 40 million random memory
locations.  So, in theory, even a linear scan over one column of a
billion rows should be possible with keystroke-like latency, so nearly
all of the usual database queries over datasets that fit in its 8
gibibytes of RAM should be possible on a keystroke-by-keystroke basis.
And this is not a particularly fast machine by today’s standards.

Any data you can conceivably enter by hand fits in RAM easily.  If you
can punch in one number per second and store it in a 32-bit floating
point value, you need 2 billion seconds to fill this palmtop’s RAM,
which is just under 32 years, longer if you sometimes stop to sleep or
eat.

This suggests that the more important thing to think about, for
human-scale datasets at least, is not how to organize the data to be
*efficiently* searchable, but how to improve the *usability* of the
data-organizing process.  For that see `sieuferd.md`.  This note is
about efficiency and performance!

### An example data file and query ###

As an example, I have a tax data file here from the Argentine tax
authority, AFIP, listing every valid CUIT number, 6.26 million in all,
for every registered taxpayer from among Argentina’s 44 million
people.  (Mere employees don’t need one.)  It’s 325 megabytes of plain
text (325464672 bytes) with fixed-width, undelimited fields.  Without
reorganizing it into aligned columns, it takes 102 milliseconds to
search it for Daniel Osmar Maidana’s CUIT, on a single core, with this
C code:

    enum { recsize = 52 };
    char *mem = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, (off_t)0);
    char buf[recsize+1] = {0};
    char key[] = "23304663359";
    for (int off = 0; off <= size - recsize; off += recsize) {
      if (0 == memcmp(key, mem + off, sizeof(key) - 1)) {
        memcpy(buf, mem + off, recsize);
        printf("%12d %s", off, buf);
      }
    }

The output is:

       162732856 23304663359MAIDANA DANIEL OSMAR              AVN 10

This is still 20× slower than my estimate.  It isn’t obvious to me
why; the usual case for the inner loop gets compiled to this
decent-looking seven-instruction sequence:

        1260:  48 83 c3 34  add  $0x34,%rbx          ; decimal 52, recsize
        1264:  48 39 dd     cmp  %rbx,%rbp           ; loop exit check
        1267:  7e 5f        jle  12c8 <main+0x188>
        1269:  49 8d 04 1c  lea  (%r12,%rbx,1),%rax  ; load record pointer
        126d:  48 8b 08     mov  (%rax),%rcx         ; load first 8 bytes of rec
        1270:  49 39 4d 00  cmp  %rcx,0x0(%r13)      ; compare to “23304663”
        1274:  75 ea        jne  1260 <main+0x120>   ; continue if mismatch

That should rule out all but three records, so it should be fast, but
no, it only examines about 60 million records a second.

Valgrind cachegrind tells me it took 44 million instructions and 12.6
million data references, of which 5.1 million were D1 cache misses
(and basically the same number were LL misses), 40% of the total.  If
nearly every reference to a new record was a cache miss, that might
help to explain the slowness.  But I’d’ve thought this kind of strided
memory access was sufficiently predictable to prefetch, and the total
memory bandwidth achieved here is only 3.3 gigabytes per second.

Callgrind confirms that those 7 instructions each executed 6 258 936
times, while the following instructions executed 3 times.

Running 8 such processes concurrently, two per core, I got times from
180 to 346 milliseconds.  Running 32 of them concurrently, I got times
from 1126 ms up to 1229 ms, which would be about 35 to 40 milliseconds
each if they were run serially.  This suggests an effective parallel
speedup of about three.

### Some notes on the hardware ###

[Intel’s description of the CPU][0] says it
has four cores and no hyperthreading, so apparently the bottleneck is
really single-threaded performance on one core, but something else on
all four cores, like memory bandwidth.  Which is strange, because
that’s only 10.4 gigabytes of potentially-cache-missing data reads
among the 32 processes, and so only about 8 gigabytes a second of
memory bandwidth; a small fraction of what I expected to get.  Intel
says, “DDR4/LPDDR4 upto 2400 MT/s”; what does that mean?

[0]: https://www.intel.la/content/www/xl/es/products/sku/197309/intel-celeron-processor-n4120-4m-cache-up-to-2-60-ghz/specifications.html

[Wikipedia explains][1] that 2400MT/s DDR4 RAM runs at a 300MHz clock
with a 1200MHz I/O bus clock, 19200 MB/s of peak transfer rate (due to
8 bytes per transfer), and a “PC4-19200” “module name”.  `lshw` tells
me that mine is only 2133MT/s:

    description: DIMM LPDDR4 Synchronous 2133 MHz (0,5 ns)

WP says that should limit me to 17067 megabytes per second.  Which is
still twice what I’m getting!  On the other hand, if Cachegrind is
right about the number of cache misses, a single core can evidently
service about 50 million cache misses per second (not sure if CAS
latency makes some cheaper than others) and all four together can
manage about 150 million, which is a lot more than I expected.  I
don’t think the different processes were accessing memory sufficiently
close to lockstep to get a benefit from each other’s cache traffic.

[1]: https://en.wikipedia.org/wiki/DDR4_SDRAM#JEDEC_standard_DDR4_module

The loop is evidently not vectorizing the search at all.  The CPU
supports SSE 4.2 but not AVX (and the generated code actually does use
SSE instructions outside the hot search loop quoted above).

### `memcpycost` ###

I got somewhat surprising but consistent results from my `memcpycost`
program.  It takes 327-333 milliseconds to memcpy the CUIT database
into malloced memory once, of which 105-113ms is reported as user
time, suspiciously similar to my search program’s 101ms.  But
memcpying twice takes 394-396ms, only 62ms more; perhaps Linux is
spending the extra 200ms to zero and page-fault in the buffer, but why
only 60ms?  Setting the iteration count to three takes 463-464ms, 70ms
longer.  And 10 repetitions takes 939-944ms, 68ms per iteration.  100
iterations takes 7.056-7.087 seconds, which is also 68ms per
iteration.

325464672 bytes memcpyed in 68ms is 4.79 gigabytes per second, which
is presumably 4.79 gigabytes in and 4.79 gigabytes out, 9.58 gigabytes
per second.  This is consistent with the “about 8 gigabytes per
second” number I got above, but it’s on a single core.  I don’t know
if the lower single-core speed in the query program is a result of an
instruction dispatch bottleneck or the core stalling waiting on
memory.  I suspect it’s the latter because of the 3x parallel speedup.

Running `memcpycost` in four parallel processes makes it take
21.75-22.17 seconds instead of 7.1, which suggests its total
throughput increased by about a third (to around 13 gigabytes per
second in+out) when running on multiple cores.

### Sliced columns ###

What if you made a column database?  Because CUITs are 11 digits, you
can’t store them in 32 bits; I think you need at least 35 bits.  So if
you were to make a single array of aligned fixed-width CUITs to speed
up vector processing, they’d probably need 8 bytes each.  Which means
SSE could maybe test two of them at a time.  The column being
contiguous would cut down memory bandwidth from 52 bytes per row down
to 8 bytes, so you’d probably get about a 6× speedup, from 100ms to
15ms, if instruction dispatch could keep up.

The CUIT-column file built in this way is 50'071'488 bytes; generating
it takes almost an entire second.  Callgrind tells me its 3.5 billion
instructions are 660 million in main(), 860 million in fwrite(), and
1.9 billion in strtol(), so I could plausibly cut that down quite a
lot if I tried to optimize it.

Given that file, I tried writing the query just in C without any
vectorization:

      region records = mmap_filename(argv[1]);
      region cuits = mmap_filename(filenamebuf);
      long ncuits = cuits.len / 8;
      long *cuitp = (long*)cuits.buf;
      for (size_t i = 0; i < ncuits; i++) {
        if (cuitp[i] != cuit) continue;
        if (!write(1, records.buf + i * recsize, recsize)) {
          perror("write");
          abort();
        }
      }

This takes 24–30ms.  The inner loop is not vectorized:

        12d0:  48 83 c3 01  add  $0x1,%rbx          ; i++
        12d4:  49 39 dd     cmp  %rbx,%r13          ; i < ncuits?
        12d7:  74 37        je   1310 <main+0x170>
        12d9:  4d 39 24 df  cmp  %r12,(%r15,%rbx,8) ; cuitp[i] != cuit?
        12dd:  75 f1        jne  12d0 <main+0x130>

This makes the 15ms number with SSE seem plausible.

Also, though, it occurred to me that I could probably get some
parallel speedup by running it across multiple cores until you hit the
sequential memory bottleneck that `memcpycost` hit around 12GB/s,
which would be about 4 milliseconds.  I tried that, writing [my first
pthreads program][2], and without vectorization, it’s still taking 13–18
milliseconds, which is still a lot faster than 24–30.

[2]: http://canonical.org/~kragen/sw/dev3/findcuit.c

Running this same code on a 12-core 4.4GHz Ryzen 9 7900X3D virtual
machine, which *does* have AVX, (even AVX512,) it takes 4–12ms instead
of 13–18ms.

If you sliced the column more thinly, you might be able to get more of
a benefit.  For example, suppose you represented each CUIT in 8 bytes,
but made 8 separate arrays of 6.25 million bytes for each byte column.
Then you could use SSE to load 16 bytes of the low-order column and
test them against the corresponding byte of the search key all at
once.  Assuming an even distribution, you’d only have 24000 rows to
examine any other byte of, which I think would enable you to do the
whole query in about one millisecond for the initial linear scan and
half a millisecond for the second one.

### Sorting and indexing ###

As it happens, the file is not just fixed-width but also sorted, so we
could easily enough search by CUIT using binary search.  But what I
wanted to test here was the brute strength of the CPU to see what a
database could manage without any indexing.

With just binary search you’d find your record by examining about 23
records, which at 60 million records a second would take about 300ns
if it weren’t for random access latency.  (I guess I need to write a
microbenchmark for random access latency.)  A two-level B-tree would
have a branching factor of about 2500; a B-tree with 8-way branching
(to match the CPU’s 64-byte cache lines) would need 9 levels if all
the nodes were full, and thus up to 9 cache misses, which I guess is
about 700 ns.

### Some comparisons with Feather and CSV in Python ###

On the same MicroPC, I generated 999'999 two-column rows with integers
in them and wrote them to CSV and [Feather][4] files with Pandas by timing
these lines of code:

    df = pd.DataFrame({'A': range(1, 1_000_000), 'B': range(1, 1_000_000)})
    df.to_csv('test.csv', index=False)
    df.to_feather('test.feather')
    df_from_csv = pd.read_csv('test.csv')
    df_from_feather = pd.read_feather('test.feather')

[4]: https://arrow.apache.org/docs/format/Columnar.html

This took 2 seconds to write to CSV and 240ms to read it, and 68ms to
write to Feather and 36ms to read it.  Using `%timeit` to get more
reliable measurements, I got 29.8ms stdev 468µs to read the Feather
file, and 252ms stdev 17ms to read a 10-million-row version.

This compressed the Feather file with zstd, as per default, so it was
77MiB.  Turning off compression

    df.to_feather('test2.feather', compression='uncompressed')

made it 160MiB, within a rounding error of the space usage of my raw
columnar format.  The resulting file could be read faster: 219ms stdev
16.1ms.

Normalizing all these performance results to records per second:

- writing CSV from Pandas: 999'999 two-column records in 2 seconds,
  0.5 million records per second
- reading CSV from Pandas: 999'999 two-column records in 240ms, 4.2
  million records per second
- writing compressed Feather from Pandas: 999'999 two-column records
  in 68ms: 15 million records per second
- reading compressed Feather from Pandas: 999'999 two-column records
  in 29.3–30.3ms: 34 million records per second
- reading larger compressed Feather from Pandas: 10 million two-column
  records in 235–269ms: 37–43 million records per second
- reading uncompressed Feather from Pandas: 10 million two-column
  records in 203–235ms: 43–49 million records per second
- Single-threaded exhaustive search over the whole CUIT table mmapped
  in C: about 60 million records per second
- Multiprocess exhaustive search over the whole CUIT table mmapped in
  C: about 180 million records per second
- Single-threaded exhaustive search over a single mmapped column in C:
  6.26 million records in 24–30ms: 210–260 million records per second
- Multithreaded exhaustive search over a single mmapped column in C:
  6.26 million records in 13–18ms: 350–480 million records per second

It’s interesting and surprising that Feather is throwing away at least
90% of the computer’s performance, running almost a full order of
magnitude slower than I’ve achieved here.

The even more significant difference, though, is that although
[Feather is designed to support zero-copy operation with Pandas][3], I
haven’t been able to figure out how to get it to work.  So if you have
some kind of indexing scheme that allows you to process only a few
records of your data, (for example, the record for a particular CUIT,)
the zero-copy memory-mapping schemes can take submicrosecond amounts
of time, while Feather will always take an amount of time proportional
to the total amount of data, which can be six to twelve orders of
magnitude larger than the data needed for a given operation.  So in
such cases we should expect Feather to impose a slowdown of not a
single order of magnitude but seven to thirteen orders of magnitude.

[3]: https://arrow.apache.org/docs/python/pandas.html#memory-usage-and-zero-copy
