English has changed over the centuries, and one of the most obvious
examples is in its vocabulary.  I tabulated the word frequencies of
two works from Project Gutenberg, the King James Bible and
E.M. Forster’s _Room with a View_:

    In [43]: tabulate = lambda lines: collections.Counter(word
                          for line in lines
                          for word in re.compile(r"['’\w]+").findall(line))

    In [44]: %time words = tabulate(open(
                          '../netbook-misc-devel/bible-pg10.txt'))
    CPU times: user 353 ms, sys: 11.8 ms, total: 365 ms
    Wall time: 361 ms

    In [45]: %time rwords = tabulate(open(
                          '../library/forster-room-with-a-view-pg2641.txt'))
    CPU times: user 38.9 ms, sys: 0 ns, total: 38.9 ms
    Wall time: 38.3 ms

    In [54]: sum(words.values())
    Out[54]: 855432

    In [55]: sum(rwords.values())
    Out[55]: 70721

Seventy thousand words is a lot shorter than the Bible but still
probably covers a lot of modern English.  We can define the
“biblicality” of a word by how much the probability of being the next
word in the Bible exceeds its probability of being the next word in
Forster:

    In [78]: def biblicality(word):
        ...:     return (71/855) * (1 + words[word]) / (1 + rwords[word])
        ...: 

You’d think that this would give you numbers above 1 for words that
occur more in the Bible, but in fact words that occur once in the
Bible and zero times in Forster get a biblicality of 0.166:

    In [79]: rwords['Lycia'], words['Lycia'], biblicality('Lycia')
    Out[79]: (0, 1, 0.16608187134502925)

That’s just because a word that hasn’t turned up yet in 855000 words
is less likely to be the next word in that text than a word that
hasn't turned up yet in 71000 words.

This allows us to sort all our words by Biblicality:

    In [80]: %time modwords = sorted(set(words.keys()) | set(rwords.keys()),
                                     key=biblicality)
    CPU times: user 21.2 ms, sys: 7 µs, total: 21.2 ms
    Wall time: 20.7 ms

    In [87]: len(modwords)
    Out[87]: 18836

What are the *most* Biblical words?  These would be words that are
overwhelmingly more likely to be the next word in the Bible than in
Forster:

    In [85]: [print(line) for line in textwrap.wrap(' '.join(modwords[-200:]))];
    shewed father's goeth witness mercy camp sabbath sanctuary deliver
    Joab captain counsel Manasseh city Samuel Moreover delivered twenty
    fled hundred desolate commanded Thy Jeremiah seven heathen evil Zion
    cattle vessels fifty beasts east border silver therefore kings
    Benjamin reign 51 Spirit Moab Paul commandments 47 Ephraim sins throne
    119 commandment elders slain doth 49 unclean slew angel Jordan
    inhabitants 45 46 fruit lest flesh cubits sacrifice thyself wilt eat
    shew Joshua 48 sin cities begat Pharaoh therein ark Abraham 43 3 holy
    yea righteous lord prophet inheritance Jews prophets tribe disciples
    multitude priest Philistines 44 Father Wherefore Levites offerings O
    enemies 42 princes iniquity Solomon cometh Ye mighty king's Son
    Babylon covenant judgment righteousness GOD hosts 41 Aaron tabernacle
    39 nations dwell God toward Lord 4 40 Jacob children drink offering
    behold 30 altar 38 Saul priests 37 sword 8 thine 35 5 36 servant
    fathers 34 brethren 9 Christ Thou spake 7 33 Behold Egypt 32 2 31 king
    20 6 29 Jerusalem Judah Moses land 28 thereof 27 Jesus David hast 26
    sons 25 24 saith 23 22 21 17 19 18 shalt thou 16 15 14 13 12 11 10 thy
    hath Israel ye thee LORD unto

That sounds about right.  How about the *least* Biblical words?

    In [86]: [print(line) for line in textwrap.wrap(' '.join(modwords[:200]))];
    Lucy Miss Mr Cecil Beebe Bartlett Honeychurch George Freddy Emerson
    don’t Mrs Lavish Charlotte really Eager ’ Florence Vyse I’m can’t
    Chapter Windy Corner It’s Emersons has didn’t Minnie nice Lucy’s tea
    isn’t Alans towards spoke exclaimed girl tennis Harry remark Don’t
    clergyman Perhaps London replied won’t it’s Alan Summer idea clever
    I’ve Street events engagement suggested reply Bertolini music everyone
    Cissie Santa Cecil’s piano Croce comes exactly wouldn’t I’ll real
    yourself doesn’t That’s does Sunday yes violets Baedeker Beebe’s
    Eleanor fact across knows he’s scene pension minute nerves Arno
    realize explain cousin repeated Bartlett’s society conscious novel
    Italians annoyed says you’re What’s started photographs Very simply
    brain frightened boy shouldn’t muddle afternoon difficult smiled
    chaplain mother’s tiresome someone human broke Fate aren’t Thank
    there’s Poor one’s show pretended Dear cousin’s Wells splendid
    delightful Tunbridge absolutely realized Italy chair Yes Piazza
    Italian cad Otway neighbourhood dearest road Constantinople expect
    trying possibly Villa Signora usual murdered tired Weald nonsense
    ladies different You Greece couldn’t villas whatever probably tourists
    pond opposite tourist Phaethon vulgar Rectory shocked upset English
    listen Hullo interested serious anxious type leant dear felt bother
    interesting St influence baby glanced worried nervous Fiesole oughtn’t
    haven’t Pension irritation attempt interest

That also seems about right.  (The novel takes place in Greece and
Italy.)

How about the most popular words, ranked by Biblicality?  Let’s define
a popularity metric that should sum to about 1 across all words:

    In [92]: popularity = lambda word: (words[word] * (71/855)
                                        + rwords[word])/142000

    In [103]: sum(popularity(word) for word in modwords)
    Out[103]: 0.9982878428465795

    In [104]: [popularity(word) for word in ['the', 'and', 'tiresome', 'thy']]
    Out[104]: 
    [0.05752495675809242,
     0.035292521209126106,
     8.450704225352113e-05,
     0.002609966230129314]

That seems about right.  There are about 143 words that each
individually account for more than 0.1% of the corpus:

    In [110]: sum(popularity(word) > 0.001 for word in modwords)
    Out[110]: 143

So let’s see how they stack up by Biblicality:

    In [114]: [print(line) for line in textwrap.wrap(' '.join([
                 word for word in modwords if popularity(word) > 0.001]))];
    Lucy Miss Mr Cecil Beebe Bartlett Honeychurch She must It she could
    would very little been He her who you had at so what was about know
    like or see did more do a The again no if I on But should now am it to
    one but go any as say over we said have this down not an are by with
    is up your things were that he into us in me may for when out our
    after people Then all be from made there will his the man come and
    they him of before even which them came my their day men For saying
    hand went against son house 1 also upon And shall 3 God 4 children 8 5
    9 7 2 king 6 land thou 15 14 13 12 11 10 thy hath Israel ye thee LORD
    unto

So words like “is up your things were that he into us in me may for
when out our” are relatively about as popular now (or anyway in
Forster) as they were in the KJV.  Words like “9 7 2 king 6 land thou
15 14 13 12 11 10 thy hath Israel ye thee LORD unto” are much more
common in the Bible, because Forster doesn’t have chapter and verse
numbers.  Perhaps more interesting still are things like “hand went
against son house also upon And shall God children”, which are a
little bit subtler: “Also upon the hand of my son against the house”
sounds Biblical without using any words that sound entirely out of
place in late modern English.

Similarly, on the other end, “Lucy Miss Mr Cecil Beebe Bartlett
Honeychurch She must It she could would” are mostly words that are
quite specific to Forster or at least to modern speech, although
with “would” the KJV does have elocutions like this:

> And I said, Oh that I had wings like a dove! for then would I fly
> away, and be at rest.
>
> Lo, then would I wander far off, and remain in the
> wilderness. Selah.
> 
> I would hasten my escape from the windy storm and tempest.

But it’s the next line or so, “very little been He her who you had at
so what was about know like or see did more do a The again”, which has
more tells.  The capitalized He and The are probably because of the
greater tendency in modern books to use short sentences, resulting in
more capitals beginning sentences, whereas the KJV likes to say things
like this:

> Behold, I am against thee, saith the LORD of hosts, and I will burn
> her chariots in the smoke, and the sword shall devour thy young
> lions: and I will cut off thy prey from the earth, and the voice of
> thy messengers shall no more be heard.

I suspect the reason for “very” is that in the KJV it’s often a
literal word, meaning “actual” or “real”:

> Behold, is it not of the LORD of hosts that the people shall labour
> in the very fire, and the people shall weary themselves for very
> vanity?

> And it came to pass at the time of the going down of the sun, that
> Joshua commanded, and they took them down off the trees, and cast
> them into the cave wherein they had been hid, and laid great stones
> in the cave’s mouth, which remain until this very day.

But it still gets mostly used as a mere intensifier, though not all
that often:

> And while the flesh was yet between their teeth, ere it was chewed,
> the wrath of the LORD was kindled against the people, and the LORD
> smote the people with a very great plague.

While Forster uses “very” very often indeed, saying things like this:

> Teresa was very much annoyed, and left the table before the cheese,
> (...)

> Very charming, very charming.

> The feeling was very subtle and quite undogmatic, and he never
> imparted it to any other of the characters in this entanglement.

Forster does *occasionally* use “very” in the literal sense, an
elocution which sounds a little dated to me now:

> As you well remarked this very morning,

A longer “spectrum of Biblicality” looks like this:

    In [122]: [print(line) for line in textwrap.wrap(' '.join([
                   word for word in modwords if popularity(word) > 0.0003]))];
    Lucy Miss Mr Cecil Beebe Bartlett Honeychurch George Freddy Emerson
    don’t Mrs Lavish Charlotte really Eager ’ Florence Vyse I’m can’t
    Chapter has girl does Italy Yes You dear felt view She something quite
    Oh seemed mean room too No want talk think course never herself rather
    must Project Gutenberg always It kind thought she could last once just
    get Do going would very little asked only How most been better here He
    back understand her some ought mother still can Why though mind much
    There who remember A looked We tell look alone well you why had knew
    such at so They cried how what young What old right nothing told new
    while was about love know cannot like long or other began see whether
    world To did more gone where do book far than might As All In Of a The
    again no through door if sat I time on But My work should being now
    That am morning it turned to works life between one full round but
    This go any thing as say those over voice live we first off said same
    have fell If face whole this saw without good way When left another
    down woman eyes head own then not an ever are by whose seen two with
    is up your things keep under were heard So found that stood he into
    end taken away us yet in both gave speak me may night done for when
    himself out our word after people Then until these all water be great
    from many father hear called whom made there will because his the
    sight wife together make man Now come and words put Let given took
    place they take let nor themselves him of give soul high before even
    side brother three name which hands them came my heart their day days
    men For years set saying art hand answered went against every earth
    son house heaven law 1 forth among bring also mine blood Thus neither
    brought upon fire servants pass sent And thousand cast shall according
    city hundred evil therefore eat 3 O God Lord 4 children offering
    behold 30 8 thine 5 fathers 34 brethren 9 Christ Thou spake 7 33
    Behold Egypt 32 2 31 king 20 6 29 Jerusalem Judah Moses land 28
    thereof 27 Jesus David hast 26 sons 25 24 saith 23 22 21 17 19 18
    shalt thou 16 15 14 13 12 11 10 thy hath Israel ye thee LORD unto

