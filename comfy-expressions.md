I have a new, more concise syntax for control structures in Forth that
looks like Lisp M-expressions or Dijkstra’s guarded commands, and
which generates more efficient code than either traditional Forth
boolean handling or PostScript higher-order functions.  It’s also more
easily composable and requires less nesting.  It draws inspiration
from Henry Baker’s COMFY control-flow operators and Mizushima’s
efficient PEG parsing work.

Lisp M-expression conditional syntax
------------------------------------

The first `cond` notation in [McCarthy’s 01960 paper introducing LISP][0]
uses parentheses, right arrows and commas:

> sgn(x) = (x < 0 → -1, x = 0 → 0, T → 1)  
> n! = (n = 0 → 1, T → n·(n - 1)!)  
> p ∧ q = (p → q, T → F)  
> p ∨ q = (p → T, T → q)  
> ~p = (p → F, T → T)  
> p ⊃ q = (p → q, T → T)  
> sqrt(a, x, ε) = (|x² - a| < ε → x, T → sqrt(a, ½(x + a/x), ε))  
> gcd(m, n) = (m > n → gcd(n, m), rem(n, m) = 0 → m, T → gcd(rem(n, m), m))  

McCarthy produced [a nicer LaTeX version][3] before he died, with a
footnote saying, “Copied with minor notational changes from CACM,
April 1960. If you want the exact typography, look there.”  As for the
exact typography, in both versions the right arrows are almost as tall
as a lowercase “e”, but a bit over twice as wide, with open rather
than filled arrowheads.

The last example, gcd, illustrates that the commas to separate
conditional cases are somewhat visually confusing, especially in the
original CACM formatting, in which there is a line break before “= 0”.
A couple of pages later, he switches to semicolons and square brackets
when he first incorporates this conditional construct in his
“M-expressions (meta-expressions)”:

> ff [x] = [atom [x] → x; T → ff [car [x]]]

The same convention is used starting on p.5 of the [“LISP 1.5
Programmer’s Manual”][4] (dated “1962”, but reporting events from
01965 in Appendix I), though using somewhat smaller arrows, perhaps
with filled heads.  The manual includes a lot of text from the 01960
paper in lightly edited form.

[0]: https://dl.acm.org/doi/10.1145/367177.367199
[3]: https://www-formal.stanford.edu/jmc/recursive.pdf
[4]: https://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf

COMFY infix and prefix syntax alternatives
------------------------------------------

I’ve been thinking about how to retrofit Henry Baker’s COMFY
single-entry dual-exit control structures onto Forth
(see file `comfy-forth.md`).  In COMFY,
comparisons don’t evaluate to Boolean values, but rather conditionally
transfer control to a statically determined destination if they fail;
my idea for retrofitting this onto Forth is that the compiler has a
current-failure-destination state variable which determines that jump
destination and a syntactic control-structure stack which determines
how control-structure words affect that current failure destination.
(Most jumps are forward, so generally this is a forward reference.)

Baker presented two versions of COMFY in his SIGPLAN Notices column.

[In the first one, he uses infix operators][1] “|” (alternatives), “;”
or equivalently “→” (sequencing), “¬” (negation), and “<sup>∞</sup>”
(infinite loop).  He points out that “b → a | c” is equivalent to “if
b then a else c” if “a” cannot fail, “which is exactly BCPL syntax for
conditionals”.  And he points out that Lisp `(cond (b₁ e₁) (b₂ e₂)
... (bₙ eₙ))` can “often be emulated” as “b₁ → e₁ | b₂ → e₂ |...| bₙ →
eₙ” with his operators, but of course this is similarly subject to the
condition that the eᵢ cannot fail, because a failure in one of them
would execute bᵢ₊₁, which `cond` does not do.

[1]: https://dl.acm.org/doi/10.1145/261353.261356

[In the second one, published five months later, he instead uses Lisp
s-expressions][2] to provide a more practical set of compositional
operators: (**not** *e*), (**seq** *e₁ e₂ ... eₙ*),
(**if** *e₁ e₂ e₃*), (**while** *e₁ e₂*), (**alt** *e₁ e₂ ... eₙ*),
and a couple of minor irrelevant ones.
This corrects some defects in his previous version: loops can now
succeed (if *e₁* fails) or fail (if *e₂* fails) rather than always
failing, and conditionals can properly handle failure in *e₂* (by
failing the conditional as a whole).

[2]: https://dl.acm.org/doi/10.1145/270941.270947

A COMFY-based Forth conditional syntax
--------------------------------------

So I’ve usually preferred Baker’s second formulation when writing
about COMFY.  But it occurred to me today that something more like the
*syntax* of the first formulation might be better suited to a language
like Forth.

Rather than making “→” simply a synonym for sequencing “;”,
we can fix Baker’s broken infix semantics by making it part of a proper
ternary if-then-else construct, as in his second formulation.  Failure
before “→” transfers control to the “else” branch; failure after it fails
the conditional as a whole.  This is similar to [the “↑” cut operator
introduced by Mizushima et al. for PEG parsing][6], which of course comes
from Prolog’s “!” cut operator.  (See also [Redziejowski][7].)

[6]: https://kmizu.github.io/papers/paste513-mizushima.pdf (Packrat Parsers Can Handle Practical Grammars in Mostly Constant Space)
[7]: https://ceur-ws.org/Vol-1269/paper232.pdf

For a Forthlike syntax, we need words to introduce and terminate the
conditional; probably the `{ }` used in C-family languages and Tcl are
the choice that most clearly suggests control flow (though JS, Lua,
and Python users might be misled into thinking of associative arrays).
And I think “|” is a better choice to separate the alternatives than
McCarthy’s “,” or “;”.  So, limiting ourselves to ASCII, we have this simple
conditional:

    foo { bar -> baz | erm } anne

The intended meaning here is:

1. First, compile the sequence of words `foo`.  Failures therein will
   transfer control to the ambient failure handler.
2. Then, with `{`, begin a conditional construct.
3. Then, compile the sequence of words `bar` in a context where
   failures will transfer control to `erm`.
4. Then, with `->`, pop that failure handler off the stack, but don’t
   emit any new code, and compile the sequence of words `baz` in a
   context where failures will transfer control to the same ambient
   failure handler as with `foo`.
5. Then, with `|`, compile an unconditional jump from the end of `baz`
   to `anne`.
6. Then, resolve the jump destination to which possibly jumps were
   compiled in step 3, and compile `erm`, also with the same ambient
   failure handler as with `foo`.
7. Then, with `}`, resolve the destination to which the unconditional
   jump was compiled in step 5.
8. Finally, compile `anne`, also in the same control context as `foo`.

In this formulation, `bar -> baz` might seem to no longer be a
single-entry dual-exit structure, because it can jump either to `erm`
or to the ambient failure handler.  But, since it’s followed by `|`,
it cannot continue executing sequentially, so it still only has two
exits.  This continues to bother me, though, and I think it may be
best to not consider `bar -> baz` to be an expression in its own
right.

Adding disjunction
------------------

So far, so good.  What happens if we leave `-> baz` out?

    { bar | erm }

If we want this to be interpreted as logical OR (∨) of `bar` and `erm`,
we would `erm` to be compiled in the ambient failure context, and
`bar` to be compiled in a context where its failures transfer control
to `erm`, and it is followed by an unconditional jump to the `}`.
This turns out to be exactly what we get with

    { bar -> nop | erm }

and of course in Forth-like languages an empty sequence of words is a
perfectly valid `nop`:

    { bar -> | erm }

So if we just specify that `-> |` has the same effect as `|`, we get
disjunction for free.  And this is pretty easy, because `->` didn’t
emit any code; it merely re-established the ambient failure context
for the elided `baz`.  So we just have to be careful that `erm` is
compiled in the correct context.

Compiling flat if-elsif chains in Forth
---------------------------------------

What is that context, though?  Consider if-elsif chains like
McCarthy’s 

> sgn(x) = (x < 0 → -1, x = 0 → 0, T → 1)

which maybe we’d like to be able to write as

    { x 0< -> -1 | x 0= -> 0 | 1 }

Or possibly as

    { dup 0< -> drop -1 | 0= -> 0 | 1 }

In this case, we want the `0=` test to be compiled in a context where
failure transfers control to the `1`, not in the ambient context.  But
we don’t know that until we get to the following `->`.

But those jump destinations are forward references anyway.  We don’t
know what they’re forward references *to* until we reach `|` or `}`
and resolve the reference, so that failures in the condition before
the `->` jump to the code after the `|` or `}`.

Supposing we’re building up a linked list of jumps for a particular
forward reference, then, `{` or `|` starts a new such linked list.  If
we encounter a `->`, that linked list becomes the *condition* of the
conditional, whose failure jumps past the next `|` or `}`, and we
start compiling the consequent of that condition, in the ambient
failure context where `{` started.  If this new linked list never
encounter `->`, `|` and `}` act differently: `|` makes it a condition,
just as `->` would have, while `}` makes it a consequent, just linking
it into the ambient failure context.

This is sufficient for our example

    { x 0< -> -1 | x 0= -> 0 | 1 }

to have the desired effect.

Loops
-----

Next, let’s consider McCarthy’s [square root by Heron’s method][8]:

> sqrt(a, x, ε) = (|x² - a| < ε → x, T → sqrt(a, ½(x + a/x), ε))  

[8]: https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Heron's_method

We can write this recursion simply enough as a conditional

    { x x * a - abs ε < -> x | a x a x / + 2/ ε sqrt }

but what we really want here is a loop, where we reassign `x` and jump
back to the beginning.  In C we might say

    while (fabs(x*x - a) >= ε) x = (x + a/x)/2;

which is basically

    loop: if (fabs(x*x - a) >= ε) { x = (x + a/x)/2; goto loop; }

Seen this way, a `while` loop is almost the same as an `if`, it just
has an unconditional backward jump to the conditionat the end of its
consequent.  So perhaps the way to handle this is by using the same
`-> | -> |` machinery inside the loop, but establish the outer context
with different words:

    {* x x * a - abs ε >= -> x a x / + 2/ to x *}

(Here `to x` updates the value of `x`, as is traditional since the
introduction of `value`.)

Arguably the braces `{ }` should be reserved for such iteration, as
they are often used for iteration in extended BNFs (starting with
[Wirth syntax notation][17] from [01977][18] when he was
at PARC, though Wirth was already using them that way
by 01973; see file `wirth-syntax-notation.md`), relegating ordinary
conditionals to square brackets `[ ]`, analogous to their use for
indicating optional clauses in grammars like [the 01958 UNIVAC
FLOW-MATIC Programming System’s][10], which used ellipses for
repetition, but not [the Algol 60 report][19] ([reprint][20]).  [The
COBOL 60 report][11] also used square brackets for optional elements,
but used curly braces for sets of alternatives, one per line within
the braces.  The use of braces for repetition in EBNF [has been
proposed for standardization as part of ISO 14977][12], with emoticons
`(: :)` as a possible alternative, while [the W3C uses the prefixed
asterisk used in RFC 822][13] and [standardized as ABNF][15] in [RFC
5234/STD 68][16], and [the W3C’s XML standard uses a postfix asterisk
as regular expressions do][14].

This is similar to a deterministic version of Dijkstra’s 01975 [“guarded
commands”][5], in which the while-loop continues running as long as at
least one of the conditions is true.  As a notational note, Dijkstra
also uses “→” separate his guards from his commands, but he uses “**if
fi**” and “**do od**” for the brackets.  (In the typewritten original
he underlines these keywords and uses a vertical rectangle to separate
alternatives; it was [also published in CACM][9] with more elegant
typography similar to the HTML transcription on the U Texas site.
Dijkstra does use braces for optional repetition in his EBNF.)

At this point we have `while`, `alt` (or `or`), `seq` (or `and`), and
`if` (or rather `cond`).  And our `while` loops double as universal
quantifiers; if any iteration of the loop body (to the right of the
`->`) fails, the loop as a whole fails.

[5]: https://www.cs.utexas.edu/~EWD/transcriptions/EWD04xx/EWD472.html (EWD472; the original is typewritten)
[9]: https://dl.acm.org/doi/10.1145/360933.360975
[10]: http://www.bitsavers.org/pdf/univac/flow-matic/U1518_FLOW-MATIC_Programming_System_1958.pdf (p. 97 or p. 106/126)
[11]: http://www.bitsavers.org/pdf/codasyl/COBOL_Report_Apr60.pdf
[12]: https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form#Table_of_symbols
[13]: https://www.w3.org/Notation.html
[14]: https://www.w3.org/TR/REC-xml/#sec-notation
[15]: https://en.wikipedia.org/wiki/Augmented_Backus%E2%80%93Naur_form
[16]: https://datatracker.ietf.org/doc/html/rfc5234
[17]: https://en.wikipedia.org/wiki/Wirth_syntax_notation
[18]: https://dl.acm.org/doi/10.1145/359863.359883
[19]: https://dl.acm.org/doi/10.1145/367236.367262
[20]: https://web.eecs.umich.edu/~bchandra/courses/papers/Naure_Algol60.pdf

Negation
--------

We can easily implement negation by forcing failure, for example with
`1 0=`.  The negation of `foo` then is `{ foo -> 1 0= }`, which is an
interesting notational convergence because, in classical propositional
logic, ¬P is equivalent to P ⇒ False, using the truth-functional
interpretation of the material implication operator ⇒.

In Baker’s original paper, he writes of negation:

> Inversion [negation] can not be simulated by sequences,
> alternatives, *succeed* and *fail*, in an uninterpreted (pure) COMFY
> because both sequence and alternative are monotonic operations,
> composition preserves monotonicity, and inversion is not monotone.

This is fixed by the introduction of a proper if-then-else structure
instead of trying to simulate it with sequence and alternation.

I am undecided as to whether it is worth introducing a third form of
brackets, or perhaps an unary prefix like the Bourne shell’s `!`, for
more concise negation.

The top-level alternative
-------------------------

So far I’ve always shown either `{ }`
or `{* *}` around everything that could fail,
but the question arises, what is the top-level failure handler in a
new subroutine?  With the compilation approach I’ve been discussing so
far, it’s implicit that all subroutine calls always succeed, because
it’s implicit that the subroutine-call operation is fairly
conventional — the compiler doesn’t insert a check and conditional
jump after each one, or dynamically push and pop failure handlers on
the stack so a failed subroutine can return to somewhere other than
its callsite.  And that’s probably the right choice, both to permit
compiler optimization and to keep the code reasonably tractable.

So the top-level failure handler for a new subroutine needs to be
inside that subroutine.  The most reasonable thing is to just make it
either the end of the subroutine, so a top-level failure results in an
immediate return, or a compilation error.  If we take the first
approach, an immediate return, we have something like an implicit
`{ }` conditional block around the whole subroutine, so rather than
expressing McCarthy’s sgn conditional as

    : sgn ( n -- -1 | 0 | 1 ) { dup 0< -> drop -1 | 0= -> 0 | 1 } ;

we can just say

    : sgn ( n -- -1 | 0 | 1 ) dup 0< -> drop -1 | 0= -> 0 | 1 ;

which seems significantly superior to standard Forth:

    : sgn ( n -- -1 | 0 | 1 ) dup 0< if drop -1 else 0= if 0 else 1 then then ;

Remember, though, that the difference between `->` and normal
sequencing is just how it handles failures on the *right* side:
containing them at the `}` or propagating them beyond it.  In this
case we have no failures on the right side, so we could simplify this
to

    : sgn ( n -- -1 | 0 | 1 ) dup 0< drop -1 | 0= 0 | 1 ;

which has a nondeterministic-computing flavor more like META-II (one
of Baker’s main sources of inspiration), Prolog, or Icon/Unicon.

<style>
@import "et-book.css";
body {
    max-width: 35em;
    padding: 1em;
    margin: 0 auto;
    line-height: 1.4;
    font-size: 26px;
    color: rgba(0, 0, 0, 0.794);
    font-family: "et-book", "URW Palladio L", Palatino, serif;
}
pre, code { font-size: 80%; line-height: 0.9; color: rgba(55, 55, 55, 1) }
blockquote, pre {
    background-color: #eed;
    border: 1px solid #ccc;
    padding: 0.5em;
}
blockquote > p:first-child,
blockquote > ul:first-child,
blockquote > ol:first-child {
    margin-top: 0;
}
blockquote > p:last-child,
blockquote > ul:last-child,
blockquote > ol:last-child {
    margin-bottom: 0;
}
h1, h2 { margin: .75em 0 26px 0; border-bottom: 1px solid #aab }
h3, h4, h5, h6 { margin: .75em 0 .5em 0 }
h1, h2, h3, h4, h5, h6 { letter-spacing: 0.05em; color: rgba(136,0,0,.794) }
/* h1, h2, h3, h4, h5, h6, .metadata { font-family: Helvetica,Arial,sans-serif } */
h1:first-child, h2:first-child, h3:first-child,
h4:first-child, h5:first-child, h6:first-child,
h1 + h2, h2 + h3, h3 + h4, h4 + h5, h5 + h6,
h1 + p, h2 + p, h3 + p, h4 + p, h5 + p, h6 + p { margin-top: 0 }
h1 { font-size: 2em }
h2 { font-size: 1.59em }
h3 { font-size: 1.26em }
h4, h5, h6 { font-size: 1em; font-variant: small-caps; }
h4 { font-weight: bold; font-style: italic }
h5 { font-weight: normal; font-style: italic }
h6 { font-weight: normal; font-style: normal }
</style>
