Languages like Lisp and Smalltalk are commonly implemented with
pointer tagging, where you look at some tag bits in a word to tell
what type of value it represents: a pointer?  A small integer?  A
character?  A boolean?  Nil?  This involves sacrificing half or more
of the machine’s integer range, though generally there are clever
tricks to get back that range and more with bignums.

But in many cases you have a much more restricted situation: a data
field in your program that is not an `int` but an `Option<int>`, I
mean, either an `int` or `None`.  We might call this a “nihilistic
integer”.  For this purpose you ought to be able to sacrifice much
less of the integer's possible range, perhaps just a single value.
Rust apparently has a `NonZero` type such that [`Option<NonZero<u32>>`
is still only 32 bits][0], for example.  Unfortunately, that
eliminates the possibility of representing 0, which is usually the
most important value to be able to represent.

That suggests the question of which value to use to represent the
`None` value, and how to interpret other values.

[0]: https://doc.rust-lang.org/stable/std/num/struct.NonZero.html

`INT_MIN`
---------

For signed integers, the most reasonable thing to do would be to use
the most negative integer as the `None` value, leaving all the other
values to represent themselves.  This means that you can use normal
integer arithmetic instructions on values you have “unwrapped” by
verifying that they aren’t `None`.  However, detecting whether a value
is this `None` value is potentially a bit inefficient.  It has some
properties in common with zero: (x == -x), (x + x == 0), and ((x << 1)
== 0), for example, but to use one of these properties you must also
verify that it is not 0, which is a perfectly normal number.  On some
ISAs, such as ARM and Thumb-2, it’s easy enough to test ((x ^ (1 <<
31)) == 0) or ((x - (1 << 31)) == 0) or simply (x == (1 << 31)), where
31 may instead be 63 or 15, but others make it awkward to put large
numbers like that into immediate constants.

If you have an instruction to shift a number one bit left into a carry
flag, or equivalently add it to itself, this number is the only one
that will set the carry flag and leave a zero result: CF == 1 && ZF ==
1, whose negation is CF == 0 || ZF == 0.  Although [Azeria Labs claims
this last is exactly the “lower or same” condition on ARM][1], the
ARM60 Datasheet Issue 0.81 (§7.1, p. 17/83) says “lower or same” is “C
clear or Z set”, which makes more sense, since you’d expect Z to be
set when things are the same.  But you could use a two-instruction
sequence to jump on this negation:

        adds r3, r7, r7  @ set scratch reg r3 to 2*r7 and set the carry flag
        bcc 1f           @ branch if C == 0  (r7 is zero or positive)
        bne 1f           @ branch if Z == 0  (r3 is 0)
        @ None-handling code goes here
    1:

[1]: https://azeria-labs.com/arm-conditional-execution-and-branching-part-6/

A maybe more efficient variation of this might be to reverse the test,
so you don’t need to jump in at least the nonzero integer
case:

        adds r3, r7, r7  @ set scratch reg r3 to 2*r7 and set the carry flag
        beq 1f           @ branch to maybe handle None if Z == 1
    2:  @ non-None-handling code goes here
    1:  bcc 2b           @ go back to non-None-handling code if C == 0
        @ None-handling code goes here

This still takes us on a detour when our int is zero, but handling
other numbers barely slows down at all.

As I said, though, on ARM, including Thumb-2 but not Thumb, you can
just compare to an immediate, without even clobbering a register:

        cmp r7, #(1 << 31)  @ 4-byte instruction on either ARM or Thumb-2
        beq 1f

You can do almost exactly the same patterns on 8086/i386/amd64, but
`bcc` is spelled [`jae` or `jnc`][2], and `bcs` is spelled `jb` or `jc`
(backwards from the ARM interpretation for signed comparison because
ARM and 8086 use opposite conventions for carry bits from
subtractions), `beq` is `je` or `jz`, and `bne` is `jne` or `jnz`.  An
annoying thing is that you need an extra MOV instruction:

        mov rsi, rdi     @ copy rdi into rsi, a scratch register
        add rsi, rsi     @ set CF and ZF to detect None
        jz 1f            @ if result is zero, maybe it was None, better check
    2:  @ non-None-handling code goes here
    1:  jnc 2b           @ if no carry, it was a real zero, not a None
        @ None-handling code goes here

[2]: https://faydoc.tripod.com/cpu/jbe.htm

What does this representation look like on RISC-V?  I think you can do
the same `add r3, r7, r7` approach to generate a zero in the temp
register in only the `None` and zero cases; then you can branch on
that zero (by comparing it to the zero register), and branch on the
zeroness of the original register to separate `None` from zero.

This is less appealing for unsigned integers, because this value is in
the middle of the representable range.

Zero
----

Another possibility is to use zero to represent None, which maximizes
the efficiency of detecting it on most chips.  Then you have to
represent actual zero by some other value, such as 1.  For unsigned
integers, you could reasonably just shift everything up by 1.
Incrementing, decrementing, or adding or subtracting a standard int to
such a number is just a regular addition instruction.  Comparing two
such numbers is a regular comparison.  Comparing such a number to a
standard int requires converting them to the same representation.
Adding two such numbers requires decrementing the result, and
subtracting them requires incrementing it.  Multiplication and
division are probably best handled by first converting the operands to
standard ints.  The value sacrificed to nihilism is `UINT_MAX`, and
that hardly matters.

For signed integers, though, that is a bad approach, because the value
sacrificed is -1, quite a common and important value.

The all-ones value
------------------

In ones’ complement arithmetic, there are two representations for
zero: the all-zeroes value, and “negative zero”, the all-ones value.
Perhaps we can do ones’ complement arithmetic, but repurpose the
all-ones value as `None`?  I’m not sure.

This is even more appealing in the case of unsigned integers, where
it’s just normal arithmetic (no special tricks or fixups needed) and
the value sacrificed to nihilism is again `UINT_MAX`.

Detecting this `None` value is very simple: it becomes zero when you
increment it (with normal twos’ complement arithmetic), and CPUs are
generally great at incrementing things and detecting zeroes.
