A problem with relays is that they stop working, usually after well
under a million cycles, because their contacts burn out.  An inert-gas
atmosphere helps, but the contacts still get melted by the inert-gas
arcs.  Even in a vacuum, asperities on the contacts may develop into
hotspots, if they’re solid, and there’s still a field-emission “arc”
during opening; [vacuum relays][0] normally only have rated lifetimes of a
few million cycles.

[0]: https://search.abb.com/library/Download.aspx?DocumentID=9AKK107045A8149&LanguageCode=en&DocumentPartId=&Action=Launch "Jennings Technology: Vacuum and gas-filled relays overview: Relay selection guides"

It occurred to me that if the “contacts” are always separated by a
dielectric or vacuum, there will be no arcing and no hotspots.  But of
course there will be no conduction either, because they aren't
contacts; they’re noncontacts.  However, there is capacitive coupling.

Consider two such “noncontacts” separated by [10nm of SiO₂][1], with
effective dielectric strength of 1GV/m (though [there are techniques
to raise this][3]) and relative permittivity of 3.9 ([lower than
Si₃N₄][2]).  The dielectric can then withstand peaks of up to 10V.
Suppose the noncontacts are 1μm square.  Given the parallel-plate
equation *C* = *εA*/*d* we get about 3.5 femtofarads; if the
noncontacts are then separated by an additional 30nm, that’s
equivalent to putting an additional 0.30-femtofarad capacitor in
series, reducing the total capacitance to 0.28 femtofarads, more than
an order of magnitude drop.  It would be reasonable to think of
separating the plates by as much as 1μm, reducing the (large parallel
plate approximation of the) capacitance to 0.009fF, a factor of almost
400.

[1]: https://www.iue.tuwien.ac.at/phd/filipovic/node26.html
[2]: https://eesemi.com/sio2si3n4.htm
[3]: https://www.sciencedirect.com/science/article/abs/pii/S1359645413006769

Normally we think of femtofarads as insignificant capacitances, but at
1GHz 3.5fF is only 45kΩ, and at 10GHz it’s 4.5kΩ.  So a 20V p-t-p
signal, 7.07V rms, can pass almost two milliamps rms through it at
10GHz, quite a large current in this context.

These noncontacts could be brought together and apart by the Coulomb
force by superimposing a dc bias on the ac signal to be switched,
which is potentially a high-frequency power supply rail, or a separate
set of capacitive noncontacts could be used with mechanical coupling
to the switching noncontacts.  I think the first alternative would not
work very well because the gated ac signal would need to be much lower
voltage than the dc control signal — the ac voltage would work just as
well to move the contacts as a dc voltage.  The second set of
noncontacts used to bring the switching noncontacts together could be
of larger area and perhaps start closer together.

How big is this force?  Coulomb sez *F* = *q*₁*q*₂/(4*πε*₀*r*²).  The
charge on a 10V 3.5fF cap is 35 femtocoulombs.  If the plates were
point charges separated by 10nm, this force would be utterly enormous:
(35fC)²/(4*πε*₀(10nm)²) gives 0.11 newtons.  But in fact that is a
terrible approximation because most of the charge on each plate is
much farther than 10nm away from any given point on the other plate;
in fact, it’s closer to 500nm away.

Probably the right way to solve this is to differentiate the energy in
the capacitor when you move the plates closer together or further
apart.  This changes the capacitance (inverse-proportionally), which
gives you a different (proportional) voltage for the same charge,
changing the ½*CV*² energy proportionally.  In fact, as long as the
large-parallel-plate approximation is valid, that energy is perfectly
proportional to the distance, because the electric field from an
infinite uniform sheet of charge is uniform throughout all space.

In this case (10V, 3.5fF) the ½*CV*² energy is 17.5fJ, and increases
by 17.5fJ/10nm = 1.75μJ/m = 1.75μN.  But if you’re bringing in
secondary noncontacts, their area and thus capacitance might be
larger.  And this is the force that holds the thing *shut*; when the
contacts are further apart, you could leave a larger voltage on them,
but it’s probably better to try to stick within the 10V range, which
will result in less force.  (This produces hysteresis; maintaining a
voltage in an intermediate range will prevent any transitions.)

A square micron of gold foil (100nm thick, 19.3g/cc, thus 1.93pg) with
a force on the order of a micronewton applied to it will accelerate at
500 million meters per second per second.  If distance *x* = ½*at*²
then *t* = √(2 *x* / *a*) and we can move 30nm in √(2 30nm /
(500Mm/s/s)) in 11ns.

In this wise perhaps fit is possible to use such MEMS capacitive
relays in place of transistors for logic circuits.  An ac signal
passing through such noncontacts can be rectified by diodes and thus
gradually accumulate the charge needed to close the noncontacts of
another relay.  Proper flexural design should permit indefinite
lifespan this way.

