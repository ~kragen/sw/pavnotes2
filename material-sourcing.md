I want to source some pure materials for experiments with 3-D printing
rock.

See also file `glauber-production.md` and file `phosphate-sources.md`.

Grupo Ecoquímica
----------------

In file `material-observations.md` last year I found information about
a place called Grupo Ecoquímica at Avenida de Mayo 1761 in Ramos Mejía
(11-2418-2519) and noted some of their prices from Mercado Libre.  At
the time, July 31 02021, US$1 was apparently AR$179, but now it’s
AR$287.

    | material           | price then (AR$/kg) | equiv. AR$ now | US$/kg then |
    |--------------------+---------------------+----------------+-------------|
    | bentonite          |                  50 |      80.167598 |  0.27932961 |
    | mesh-80 CaCO₃      |                  80 |      128.26816 |  0.44692737 |
    | kaolin             |                 150 |      240.50279 |  0.83798883 |
    | infusorial earth   |                 167 |      267.75978 |  0.93296089 |
    | talc               |                 200 |      320.67039 |   1.1173184 |
    | MgCl₂              |                 210 |      336.70391 |   1.1731844 |
    | epsom salt         |                 220 |      352.73743 |   1.2290503 |
    | borax              |                 220 |      352.73743 |   1.2290503 |
    | boric acid         |                 275 |      440.92179 |   1.5363128 |
    | citric acid        |                 290 |      464.97207 |   1.6201117 |
    | sulfur             |                 300 |      481.00559 |   1.6759777 |
    | electrode gel      |                 360 |      577.20670 |   2.0111732 |
    | glycerin           |                 500 |      801.67598 |   2.7932961 |
    | dipropylene glycol |                 528 |      846.56983 |   2.9497207 |
    | soy lecithin       |                 600 |      962.01117 |   3.3519553 |
    | alum               |                 630 |      1010.1117 |   3.5195531 |
    | copper sulfate     |                 880 |      1410.9497 |   4.9162011 |
    | titanium dioxide   |                2100 |      3367.0391 |   11.731844 |
    | zinc oxide         |                4500 |      7215.0838 |   25.139665 |
    | cerium oxide       |               13200 |      21164.246 |   73.743017 |
    #+TBLFM: $3=$2*287/179::$4=$2/179

They also have things like jars for candles, mineral oil, barite
(labeled as “kaolin” and claimed to have a density of 1 g/cc), PET
preforms, and so on; they’re mostly oriented toward things like
candles and bath salts.  They seem to normally be open 09:00 to 18:00
and sometimes until 20:00.  The Boletín Oficial says they incorporated
in 02015 with shareholders Natalia Robaina Diano and Héctor Daniel
Coimbra, who live at Venezuela 1238 in Ramos Mejía.

A quick spot check finds epsom salt at AR$850/kg, more than double the
predicted price; dipropylene glycol at AR$4000/kg, almost five times
the predicted price; magnesium chloride at AR$700/kg, twice the price;
and borax at AR$700/kg, also twice the price.  Only kaolin is close to
its (inflation-adjusted) price from last year, AR$300/kg.  Barite is
AR$850 for 3 kg.

### Actual purchase ###

I went over there and bought some materials.

    | material   | adver- | adver- |    per | bought | bought |   per |  US$ |
    |            |  tised |  tised |        |        |        |       |      |
    |            |  price |     kg |     kg |  price |     kg |    kg |  /kg |
    |------------+--------+--------+--------+--------+--------+-------+------|
    | copper     |        |        |      0 |        |        |   0/0 | 0.00 |
    | sulfate    |    600 |  0.250 |   2400 |    420 |    .25 | 1680. | 5.89 |
    | bentonite  |    300 |      3 |    100 |    140 |      1 |   140 | 0.49 |
    | 80-mesh    |        |        |      0 |        |        |   0/0 | 0.00 |
    | CaCO₃      |    485 |      3 |    162 |    180 |      1 |   180 | 0.63 |
    | infusorial |        |        |      0 |        |        |   0/0 | 0.00 |
    | earth      |   1500 |      3 |    500 |    310 |      1 |   310 | 1.09 |
    | boric acid |   1100 |      1 |   1100 |    690 |      1 |   690 | 2.42 |
    | kaolin     |    300 |      1 |    300 |    190 |      1 |   190 | 0.67 |
    | potassium  |        |        |      0 |        |        |   0/0 | 0.00 |
    | alum       |    ??? |    ??? | #ERROR |    600 |     .5 | 1200. | 4.21 |
    |------------+--------+--------+--------+--------+--------+-------+------|
    | total      |        |        |      0 |   2530 |   5.75 |  440. | 1.54 |
    #+TBLFM: $4=$2/$3; %.0f::$7=$5/$6::$8=$7/285; %.2f::@15$5=vsum(@5$5..@14$5)::@15$6=vsum(@5$6..@14$6)

It’s interesting to note that the prices posted in the store were very
different, mostly much lower, than the prices published on Mercado
Libre.  The unit prices of the bentonite and chalk were more
expensive, but probably because I was able to buy them in smaller
bags.

The alum appears to be contaminated with water; it’s sort of syrupy.

The chalk, which is imperfectly sealed, weighs in at 1000 g, the
kaolin at 1002 g, the alum at 521 g, the infusorial earth at 992 g,
the boric acid at 1008 g, the bentonite at 1001 g, and the copper
sulfate at 252 g.

The infusorial earth [might be the wrong kind for food storage][9]:
apparently for filtering-aid use it’s heat-treated in a way that
results in 60% devitrification.  It’s gritty between my teeth in a way
that suggests quartz crystals have formed in it.

[9]: https://www.usaemergencysupply.com/information-center/self-reliance/food-storage-frequently-asked-questions/diatomaceous-earth "misc.survivalism FAQ by Alan T. Hagan, question G.1"

Nameco
------

In Villa Bosch, about 3 km northwest of Caseros, we have Nameco
Química, with things like camphor (AR$8420/kg), grape seed oil
(AR$1027/ℓ), colophony (AR$742/kg), stearic acid, cetyl alcohol,
citric acid, zinc oxide, oxalic acid, vegetable glycerin, polysorbate
80 (a nonionic surfactant often used in ice cream, and Zippy’s
favorite food!), castor oil, and naphthalene.  They largely focus on
soap and candles.  Their price for cosmetics-grade zinc oxide is only
AR$1936/kg, which is a lot lower than Ecoquímica even last year.

They seem to be at Del Kaiser 921, 11-4842-6287, 9:00–17:00 Monday
through Friday, near Plaza Manzanares.  Their website is
<http://www.nameco.com.ar/> but it’s almost totally worthless, but it
does indicate that they sell a lot of materials that aren’t allowed on
Mercado Libre.

Química Industrial Caseros
--------------------------

Other promising local sources for odd materials included Kubra,
Insumos Nahum, and Química Industrial Caseros, at Bartolomé Mitre 4405
in Caseros, which is at the intersection with Lisandro de la Torre.  I
bought calcium chloride at the last of these during the pandemic.
Phone 11-4750-9220; insumosquimicos@yahoo.com.ar;
<https://www.quimicacarabelli.com.ar> or
<https://www.laquimicadescaseros.com>.  They don’t list on Mercado
Libre but do offer a convenient Excel spreadsheet of their products,
which sadly does not include any price information.

They are a much more serious company, carrying not only all the
materials people are forbidden to sell on Mercado Libre but also
exciting materials like chromic acid, hydrofluoric acid, and sodium
cyanide.

More interesting to me, in addition to some of the things at Grupo
Ecoquímica, they have camphor, tartaric acid, activated carbon,
benzalkonium chloride, nickel chloride, EDTA, gum arabic, graphite,
neutral sodium metasilicate and silicate, naphthalene, pumice,
propylene glycol, aluminum sulfate, urea, and silica gel.

Metals
------

Metals of interest are magnesium, aluminum (and alloys), copper,
brass, bronze, tin, lead, type metal, titanium, zinc, nickel, zamak,
tungsten, and steel.

Many of these metals are more interesting for what their ions can do
than for their properties as metals.

### Tungsten ###

Tungsten is interesting because it’s almost the most extreme in every
way: a density as high as gold, higher than any metal except osmium,
iridium, plutonium, neptunium, rhenium, platinum, and likely dubnium;
has the highest melting point of all metals; the hardest of metals. XXX

### Aluminum: US$2/kg ###

Various aluminum alloys are sold in ingots for AR$500–800/kg
(US$1.70–2.80/kg).

### Nickel: US$200/kg?! ###

Nickel is sold as strips for welding up battery packs (“fleje cinta”),
as 50-centavo pieces from 01941, and as welding electrodes.  In one
sample case a strip of 100 μm × 5 mm × 5 m (2.5 cc) goes for AR$1200
(US$4.20).  At 8.908 g/cc this is 22 g and US$190/kg, which is almost
certainly an excessive price.

Much recent Argentine coinage is nickel silver, i.e., a white
nickel/copper alloy, and this should be separable by electrowinning.

It may actually be cheaper and easier to buy nickel in the form of
salts and electrowin it.

### Titanium: US$80/kg ###

One vendor sells five 1.6-mm-diameter 1-meter titanium rods (“ErTi-1”)
for TIG welding for AR$3634 (US$12.70).  This would be 10 cc or, at
4.506 g/cc, 45 g, for a cost of US$280/kg, rather excessive.  And you
cannot electrowin titanium, in water anyway.

A different vendor, four blocks from the Obelisk, named “Peu★”, sells
a 21-mm-diameter 1623-mm-long titanium bar for AR$56000 (US$195); this
would be 562 cc, 2.53 kg, and US$77/kg, perhaps a reasonable price.
Most of Peu★’s other sale items are pizza cutters with cartoons on the
blade.

### Copper: US$7/kg ###

As I mentioned in the earlier note, I was able to buy fairly pure
copper (electrical copper) from the recycling guy around the corner
for US$6.70/kg.

### Brass and bronze: US$20/kg ###

I don’t know how to buy or even price these.  Zeta Tec does sell brass
sheets (“chapa de latón”) at 1mm thickness and 100 mm × 400 mm (40 cc)
for AR$2276 (US$7.93), 70% copper, 30% zinc.  Google says 70/30 brass
weighs 8.53 g/cc so this would be 341 g or US$23/kg.

A related product from Metales Cabildo in Avellaneda is called “bronce
en fleje”, and it turns out “fleje” means “tiedown strap”.  They have
a website but it’s worthless crap.

Scrap brass and bronze goes for a lower price than scrap copper, so
probably recyclers will be willing to sell it.

I can’t find any actual bronze on Mercado Libre.

### Tin: US$120/kg ###

US$225/kg for 99.9% pure tin ingots from RODRI077 or SULFAMET in Villa
Devoto.  But the ingots are 23 kg, so that’s over US$5000!  And he
makes a couple of sales every month!

Impulsora Metalúrgica in Caseros sells “White Solder” brand 99.9% pure
tin ingots imported from Brazil for US$250/kg, and again they’re 23 kg
per ingot.  So evidently soldering is the intended use for these tin
ingots.  However, this is using the BNA fake dollar, so the real price
is about half of this.

### Lead: US$2/kg ###

Lead is mostly used in car batteries and accounts, I think, for most
of their cost.  It might be possible to buy it from recyclers as well.
FEBRESPORT in Posadas, Misiones, sells recycled lead ingots for
AR$650/kg (US$2.30/kg).  Pure lead is pricier.

“Perdigones” or “postas” is birdshot, much of which seems to be
tungsten now.  But “perdigones para recarga” of lead seem to be
available for AR$900/kg... from a vendor in Temperley.  In today’s
world of pervasive surveillance, it might be better to avoid buying
ammunition, but apparently “bead head cabeza tungsteno ranurado moscas
perdigones” are used for fishing.

### Magnesium: US$34/kg ###

Magnesium is available as a more or less pure metal in the form of
sacrificial anodes for protecting hot water tanks (solar, gas, or
electric) from corrosion.  A 20-mm-diameter meter-long bar costs
AR$5000 (US$17).  At 1.738 g/cc this should be about 500 g of
magnesium, so US$34/kg.  This is fairly expensive!  But pocket-sized
magnesium firestarters are even more expensive per kg, like
AR$1300–AR$4000.  And magnesium ribbon (“cinta de magnesio”) is
AR$6000 for 17 grams (880 cm).

Repuestos Jancar, among other vendors, sells these magnesium anodes on
Mercado Libre, which lists them as being in Isidro Casanova.

### Zinc: US$5/kg ###

Zinc is also sold as sacrificial anodes (“anodo de sacrificio”), in
this case for boat motors, but these are expensive because they are
imported from Japan.  (Aluminum is, too, but there are definitely
cheaper sources of aluminum.)

One vendor in José C. Paz (Zetatec Materiales) claims to sell 8kg zinc
ingots for AR$11770 (US$41), which works out to US$5/kg.

It might make sense to electrowin zinc from scrap brass or from zinc
salts.  The analogous process probably is not a useful way to get tin
from bronze, and I haven’t found tin salts being sold, except tiny
amounts of tin oxide for ceramic glazes.

### Zamak: US$5/kg ###

Zamak is interesting because it’s easily cast at low temperatures
(380–390°) but almost as strong as a steel.  Wikipedia tells me the
standard Zamak 3 alloy, with 70% of North American zinc die castings,
is 3.5–4.3% aluminum with the balance being zinc; its yield strength
is 208 MPa, UTS is 268 MPa, and Young’s modulus is 96 GPa.  (By
comparison, ASTM A36 steel has a minimum yield strength of 250 MPa, a
UTS of 400–550 MPa, and a Young’s modulus of 200 GPa.)  An addition of
3% copper gives you Zamak 2, with 20% higher strength but less
dimensional stability.  Lead impurities will inflict zinc pest on your
castings and eventually destroy them, similar to the bronze disease.

MV Metales in Caseros makes and sells Zamak 10 ingots for centrifugal
molding at AR$1350/kg (US$4.70/kg) and Zamak 5 for AR$1800/kg
(US$6.30/kg); I think the minimum order quantity may be 1000 kg.
Adrian Nacionale, also in Caseros, sells Zamak 10 ingots for
AR$1500/kg (US$5.22/kg) and seems to sell quantities as small as
100 kg; he also sells aluminum ingots.

### Type metal: US$4/kg ###

The interesting thing about type metal is that not only does it melt
at a low temperature, it doesn’t contract when it solidifies, so it
takes the form of the mold almost perfectly.

There’s *one* vendor on Mercado Libre still selling type metal,
Cajones Tipográficos, for AR$1000/kg (US$3.50/kg).  In Quilmes, with
terrible ratings because she’s canceled 8.3% of her sales.  But she
apparently still sells some every couple of weeks.

### Steel: US$0.30/kg ###

Steel scrap seems to cost about AR$90/kg (US$0.31/kg).
“Granalla/semilla de hierro para contrapeso” seems to be the phrase.
Kieva Industrial in Haedo, at Perón 3296, 8:00–17:00 Tuesday to Friday
and 9:00–12:00 Saturday, is one vendor.  In fact that seems to be the
only thing they sell.

Abrasives
---------

“Polvo esmeril” is, at least ideally, aluminum oxide, emery.  “ANDY
MUNRO” in Florida, aka “EGM Trader,” sells emery for “granallado”,
“blasting”, or “arenado”, AR$2420/4kg (US$2.10/kg), as well as glass
beads (“microesferas”), garnet, and carbon steel.  His “granalla de
acero al carbono” is $9170/25kg, which is US$1.28/kg, so it’s more
expensive than the sources of steel I mentioned earlier, but it’s
meant for sandblasting, with sizes from 420 μm up to 1300 μm.

He doesn’t, unfortunately, sell carborundum.

He seems to be at Francia 3520 in Florida Oeste, 15-5022-7210,
11-7712-2800, <https://www.egmtrader.com>, garcia-andres@hotmail.com,
administración@egmtrader.com.

Distal Distribuidora de Minerales in Flores *does* have loose
carborundum abrasive: “Imerys fused minerals” brand “carburo de
silicio verde”, “polvo esmeril”, for $1830/kg (US$6.38/kg).  They also
carry products such as dental plaster, bentonite, talc, quartz,
feldspar, dolomite, kaolin, pumice, blasting beads, red clay, emery
powder, etc.  Granaderos 843 timbre A, 15-2405-4874, 9:00–21:00 Monday
to Saturday, near Plaza Irlanda near Boyacá and Juan B. Justo.  Google
lists much lower prices.

Forben Ferretería Industrial S.C.A. isn’t on Mercado Libre but their
web site says they sell “Vire” brand aluminum oxide powder, though
they don’t dare to mention a price.  They’re at Saavedra 4151 in
Ciudadela, 11-4657-9181, Monday to Friday 8:00–17:30.

Downtown, Abrasivos Aapsa, which also isn’t on Mercado Libre, has the
widest selection of loose abrasives: black or green silicon carbide;
gray, pink, or white aluminum oxide; cerium oxide; diamond dust.
They’re at Mitre 3964 in Buenos Aires, 11-4982-9244, 10:00–14:00 on
Mondays, Wednesdays, and Fridays.

Abrasivos Gerardi at General Paz 699 in Villa Raffo (just across the
freeway from Villa Devoto) is reputed to have a good selection, but
they have no website.  11-3026-7931, 8:00–13:00 Monday to Friday.
Nothing to suggest they have loose abrasives but it might be worth
calling.

Oxide pigments for ceramics
---------------------------

MULUC Herramientas para Ceramistas in Chascomús sells a kit of nine
oxides for ceramics for AR$3590 (US$12.50): 25 g manganese oxide, 25 g
yellow iron “oxide”, 25 g red iron oxide, 25 g chromium oxide, 25 g
terrifying red potassium bichromate, 10 g cobalt oxide, 10 g black
copper oxide, 10 g nickel oxide, and 10 g copper carbonate.  They also
have smaller selections.

Artística Integral Casavieja in Lomas offers a similar kit replacing
the terrifying bichromate with a terrifying lead chromate and
replacing the yellow iron “oxide” with white zirconium silicate;
similarly, they also sell individual pigments, and also
carboxymethylcellulose.  They also have a branch in Nuñez around
Cabildo 3500, 5 blocks from the Congreso de Tucumán station, but it’s
not open to the public.  The Lomas de Zamora headquarters is Félix de
Azara 138, 11-4292-8708, Monday to Saturday 9:00–16:00, except closing
early at 13:00 on Saturdays and staying open late until 17:30 on
Thursdays.

The prices of the pigments are quite high.  For viridian (chromia)
they charge AR$830 for 50 grams, US$2.90, which works out to US$58/kg.
For 10 g of green nickel oxide they charge AR$1146, US$4.00,
US$400/kg.  25 g of teal “copper carbonate oxide” is $700, US$2.44,
US$98/kg.

Other minerals
--------------

I bought supposedly 30 kg of dirty beach sand for $540 (US$1.90 at
AR$290, 6.2¢/kg) on 02022-08-25 at a neighborhood hardware store.  It
has bleached clamshells and bits of organic matter in it, along with
probably a fair bit of saltwater.  I got a clamshell splinter stuck in
my palm making a handful of it.  Presumably larger loads like a cubic
meter would be cheaper per kg, and also cleaner, but I’m so weak I
could barely carry even the 30 kg; after a few blocks I put it down
and went home to get an appliance dolly to carry it the rest of the
way.

I’d previously bought 10 liters of off-white pumice (about 3.5 kg) in
10–40 mm pebbles from a garden store for US$2 (see file
`material-observations.md`).  Garden stores also carry perlite and
vermiculite, as well as fertilizers like green vitriol and diammonium
phosphate.

I just bought a bag of Bertinat Perlome brand expanded perlite from
the garden store for about US$1.  It weighs 617 g and claims to be 5
dm³, giving a nominal density of 0.124 g/cc.  This is at the high end
for expanded perlites.
