I was looking at a friend’s Tetris code today (he had a bug clearing
lines because his lines were aliasing because in Lua you use a table
of tables for a two-dimensional array) and the following amusing
thought occurred to me.  The line-clearing operation in Tetris (the
only thing that contains much of a loop) consists of three parts:

- Shifting existing lines down by one.
- Creating an empty line.
- Putting it at the top.

The first of these operations is constant-time in a linked list.  The
second is constant-time if you represent the lines with [the
well-known constant-time sparse-set structure][0], which additionally
supports a constant-time operation to count the number of elements in
the set, which tells you whether a line needs to be cleared or not.

[0]: https://research.swtch.com/sparse "Russ Cox’s explanation of the sparseset structure"

Of course, *actual* Tetris is a constant-size 10×20, so these
operations are constant-time *anyway*.  And if you have memory words
of at least 10 bits, representing your board as an array of 20 such
words is going to beat any such fancy operation; then shifting *n*
lines by one is *n* memory loads and stores, so on the order of 2*n*
machine-code instructions.

So this is only an actually winning strategy in an *arbitrary-sized*
variant of Tetris; let’s call it Asymptotris.  Moreover, it presumes
that whatever display device you’re using has its own memory, unlike
an HDMI display or a CRT, because otherwise your execution time is
going to be dominated by redrawing the display.  Furthermore, it
presumes that it supports windowed scrolling, where some of the lines
on the screen move down without having to be redrawn.  So it’s not a
very realistic problem!

But in Asymptotris you can represent the sequence of *m* lines with
*m*+1 next-pointers of bit-size lg *m*, each associated with a
sparseset of capacity *n* columns.  The sparseset contains array space
for 2*n* integers of bit-size lg *n*, plus a count variable also of
bit-size lg *n*.  So, for example, if you’re playing 1024×1024
Asymptotris, you need 10'250 bits for the next-pointers, 10'240 bits
for the counts, and 2'097'152 10-bit array entries totaling 20'971'520
bits.  This works out in total to 20'992'010 or 2'624'001¼ bytes.
Clearing a line involves changing 40 of these bits (the next-pointer
of the line cleared, its count, the next-pointer of its predecessor,
and the start-pointer), as opposed to potentially 1'048'576 bits in
the straightforward bitmap representation.  Determining whether a line
will be cleared involves adding a new piece to it (mutating its count
and two words per block, so up to 9 words, 90 bits) and checking its
count, as opposed to checking 1024 bits.
