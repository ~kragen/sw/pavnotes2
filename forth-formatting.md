In file `comfy-forth.md` I wrote this example code:

    : ascii-table 127 32 do i 7 and 0= if cr then  i emit s" : " type i . loop ;

This produces a kind of ragged ASCII table, reading in part:

    ...
    P: 80 Q: 81 R: 82 S: 83 T: 84 U: 85 V: 86 W: 87 
    X: 88 Y: 89 Z: 90 [: 91 \: 92 ]: 93 ^: 94 _: 95 
    `: 96 a: 97 b: 98 c: 99 d: 100 e: 101 f: 102 g: 103 
    h: 104 i: 105 j: 106 k: 107 l: 108 m: 109 n: 110 o: 111
    ...

You can program a three-digit zero-padded output pretty easily with
the standard [pictured numeric output][3] facilities, where you draw a
picture of your desired numerical format, except backwards:

    : %03u 0 <# # # # #> type ;
    : ascii-table 127 32 do i 7 and 0= if cr then
                            s"  '" type i emit s" ' : " type i %03u loop ;

[3]: https://www.jimbrooks.org/archive/programming/forth/forthPicturedNumericOutput.php

This produces properly aligned output in a fixed-width font but it
isn’t great:

    ...
     'P' : 080 'Q' : 081 'R' : 082 'S' : 083 'T' : 084 'U' : 085 'V' : 086 'W' : 087
     'X' : 088 'Y' : 089 'Z' : 090 '[' : 091 '\' : 092 ']' : 093 '^' : 094 '_' : 095
     '`' : 096 'a' : 097 'b' : 098 'c' : 099 'd' : 100 'e' : 101 'f' : 102 'g' : 103
     'h' : 104 'i' : 105 'j' : 106 'k' : 107 'l' : 108 'm' : 109 'n' : 110 'o' : 111
    ...

[The # word][0] doesn’t mention a version that will print a space
instead of a zero, so I thought maybe that was something I’d have to
hack up myself:

    : #0 2dup d0= if bl hold else # then ;   : %3u 0 <# # #0 #0 #> type ;
    : ascii-table 127 32 do i 7 and 0= if cr then
                            s"  '" type i emit s" ': " type i %3u loop ;

And this does work:

    ...

     'P':  80 'Q':  81 'R':  82 'S':  83 'T':  84 'U':  85 'V':  86 'W':  87
     'X':  88 'Y':  89 'Z':  90 '[':  91 '\':  92 ']':  93 '^':  94 '_':  95
     '`':  96 'a':  97 'b':  98 'c':  99 'd': 100 'e': 101 'f': 102 'g': 103
     'h': 104 'i': 105 'j': 106 'k': 107 'l': 108 'm': 109 'n': 110 'o': 111
    ...

Also, though, `<# #>` leaves the string length on top of the stack,
which is how `type` knows how long it is.  You could just compute the
digits and then insert the required number of spaces (by subtracting
from the field width) before printing the digits out:

    variable width  : .right width !  0 <# #s #> width @ over - spaces type ;
    : ascii-table 127 32 do i 7 and 0= if cr then
                            s"  '" type i emit s" ': " type i 3 .right loop ;

Or you could factor that into a right-aligning version of `type` and a
string formatter that just converts the number to a sequence of
digits:

    : leftpad over - spaces type ;  : %d 0 <# #s #> ;
    : .right >r %d r> leftpad ;

However, it turns out that Forth is not quite so Spartan as all that.
There’s [a word `.r` in the Forth standard][1] which does the same
thing as `%*d` in C printf, which I would have known if I’d been
reading [the Gforth documentation for simple numeric output][2]:

    : ascii-table
      127 32 do i 7 and 0= if cr then ."  '" i emit ." ': " i 3 .r loop ;

Also it’s silly I was using `s" ... type` when I could just `."`.

There are similar `u.r`, `d.r`, and (in Gforth) `f.rdp` words for
unsigned, double-precision, and floating-point values.

[0]: https://forth-standard.org/standard/core/num
[1]: https://forth-standard.org/standard/core/DotR
[2]: https://www.complang.tuwien.ac.at/forth/gforth/Docs-html/Simple-numeric-output.html#Simple-numeric-output
