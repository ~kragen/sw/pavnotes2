I was [thinking about FORTH
filesystems](https://news.ycombinator.com/item?id=39576667) and the
following occurred to me.

A foolish floppy filesystem for FORTHs
--------------------------------------

User whartung on the orange website relates that FORTH users in the
early-microcomputer era (around 01980) maintained their tables of
contents blocks manually.  But it seems like you could implement a
fat-like directory-maintenance system in less than one screen of code.
Like CP/M, you could maintain file sizes in blocks rather than in
bytes (but 1024-byte blocks, of course); like some DEC tape formats,
you could use four bytes for the filename with a weird encoding
(FORTH’s native base 36 instead of dec RADIX-50).

FORTHs of the time were mostly 16-bit, so a four-byte filename is two
cells, a double-precision number.  This can be conveniently passed
around on the stack, stored in a 2variable, etc.

Suppose you have a double-sided disk with 40 tracks of 9 512-byte
sectors per side; that’s 720 in total, or 360 FORTH blocks.  You can’t
fit a block number into one byte, so the simplest thing is to use two.
So if you want to make FAT-style linked lists, you need 720 bytes of
block numbers, pointing to the block number that follows each block.
That leaves you 304 bytes for filenames and starting block numbers; if
filenames are 4 bytes and starting block numbers are 2 bytes you have
room for 50 files, which is probably okay.

So I feel like maintaining a table of contents automatically would
have cost about 1% of the disk space (1 block for the code and 1 block
for the directory and allocation table), so it would have been a good
idea.

The interface might look something like this:

    open   ( d u -- flag )  create file d, size u blocks, returning 0 if ok
    extent ( d -- u )       return the number of blocks in file d, or 0
    delete ( d -- )         delete file named d if it exists
    filename ( c-addr u -- d )  convert ASCII filename to a double number
    .filename ( d -- )      print the filename d
    file   ( u -- d )       return name of the uth file in the directory or 0
    maxfiles ( -- 50 )      return the maximum valid file index
    index  ( d u1 -- u2 )   return the block number for block u of file d

And, from there, you can use the usual FORTH block words to read or
modify the contents of the files, such as BLOCK, UPDATE, LOAD, and
SAVE-BUFFERS.  Things like REFILL and THRU, which presuppose semantics
for the sequencing of block numbers, might benefit from file-aware
versions.  For example, you might supplement thru with a word OLD
which loads each block of a file, whatever sequence they’re in on
disk:

    2variable oldfile
    : old ( d -- ) 2dup oldfile 2! extent 0 ?do  oldfile 2@ i index load  loop ;

Or I guess that, if you can trust the code being loaded to not leave
crap on the stack between blocks, it might be justifiable to just
write this:

    : old ( d -- ) 2dup  extent 0 ?do  2dup i index load  loop  2drop ;

You might also want to list the directory of files:

    : stat ( d -- ) 2dup or if 2dup .filename ." " extent . cr else 2drop then ;
    : ls-l ( -- ) maxfiles 0 do i file stat loop ;

As for the implementation, the easiest part is the filename conversion
words, which encode up to six arbitrary case-folded alphanumeric
characters in your filename (but ignore leading zeroes):

    : filename  base @ >r  36 base !  0 0 2swap >number 2drop  r> base ! ;
    : .filename  base @ >r 36 base ! d. r> base ! ;

You’d want a subroutine that loads the directory block, something like
this, with which the file word is easy:

    : directory 0 block ;  : (file) 6 * directory + ;  : file (file) 2@ ;

And one that finds the index of a file in the file directory,
something like this, along with the parameters defining the filesystem:

    50 constant maxfiles  360 constant maxblocks  : (walk) ( d -- u )
      maxfiles 0 do  2dup i file 2= if 2drop i unloop exit then loop 2drop 0 ;

You need that to write open, index, extent, and delete, which also
need functions to walk the FAT chains.  Getting a pointer to the FAT
entry for block u is reasonably simple, as is finding the index of
a file’s first block given its index in the directory:

    : (fat) maxfiles 3 * + 2* directory + ;  : (start) (file) 4 + @ ;

Counting the length of the chain for extent is just a do-loop
following the linked list.  Let’s use a pointer to block 0 for “end of
file”, since above I’ve decided that’s the directory block.  If the
file is too big, we fall off the end of the loop and return 0.  This
internal routine walks the chain of block numbers.

    : (extent) 0 0 do dup 0= if drop i unloop exit then (fat) @ loop drop 0 ;

Then, we look it up in the filesystem directory.  If the file doesn’t
exist, we return 0 instead of attempting to walk the nonexistent
chain.

    : extent (walk) dup 0= if drop 0 else (start) (extent) then ;

INDEX is similar.  You’re not supposed to call it with an index off
the end of the file, but it’s probably important to not just return 0
in that case, because your buggy program would then access the
directory block, possibly overwriting it.  In this case, the pointer
we’re following is actually the thing we want to return if the loop
successfully terminates.

    : (index) 0 do dup 0= if drop -1 unloop exit then (fat) @ loop ;
    : index (walk) dup 0= if drop -1 else (start) (index) then ;

For OPEN and DELETE, we need two additional things: we need to decide
how to represent deleted blocks, which open consumes and delete
creates, and we need to call UPDATE to mark the directory page dirty.
Let’s say we use -1 to represent deleted blocks; then DELETE just
needs to overwrite each block entry with -1 before proceeding to the
next block.  So we define (bfree), which frees a numbered block;
(bnfree), which frees a numbered block and returns the block number
that followed it; (kill), which frees all the blocks of a file; and
finally DELETE.

    : (bfree) (fat) -1 swap ! update ;  : (bnfree) dup (fat) @ swap bfree ;
    : (kill) 0 0 do dup 0= if drop unloop exit then (bnfree) loop drop ;
    : delete (walk) dup 0= if drop else (kill) then ;

(This makes me think that really (fat) should raise an error if you
accidentally pass it 0 or -1, but I’m not sure how to do that.)

So OPEN needs to allocate a given number of blocks.  And this, for the
first time, requires us to know how many blocks there are.  Then we
can allocate the space with a simple loop.  It builds a linked list of
the requested number of blocks and leaves it in the variable TAIL,
returning 0 on failure or 1 on success.

    variable tail  variable needed
    : falloc needed !  0 tail !  maxblocks 0 do
        i (fat) @ dup -1 = if
          tail @ swap ! update i tail !  -1 needed +!
          needed @ 0= if  1 unloop exit  then  \ success
        else drop then
    loop  tail @ (kill) 0 ;

I’m not happy with that implementation.

OPEN also has to allocate a directory entry, so we have to find an
empty entry; remember that FILE returns a double-precision 0 for empty
entries:

    : emptydent maxfiles 0 do i file or 0= if i unloop exit then loop -1 ;
    : open >r 2dup extent if rdrop 2drop -3 exit then r>
      falloc 0= if 2drop -1 exit then
      emptydent dup -1 = if drop 2drop -2 exit then
      (file) dup >r 2!  tail @ r> 4+ ! update  0 ;

This implementation returns 0 on success, -1 if there are no blocks
available, -2 if there are no inodes available, and -3 if the file
already exists.

So here’s the whole thing:

    : filename  base @ >r  36 base !  0 0 2swap >number 2drop  r> base ! ;
    : .filename  base @ >r 36 base ! d. r> base ! ;
    : directory 0 block ;  : (file) 6 * directory + ;  : file (file) 2@ ;
    50 constant maxfiles  360 constant maxblocks  : (walk) ( d -- u )
      maxfiles 0 do  2dup i file 2= if 2drop i unloop exit then loop 2drop 0 ;
    : (fat) maxfiles 3 * + 2* directory + ;  : (start) (file) 4 + @ ;
    : (extent) 0 0 do dup 0= if drop i unloop exit then (fat) @ loop drop 0 ;
    : extent (walk) dup 0= if drop 0 else (start) (extent) then ;
    : (index) 0 do dup 0= if drop -1 unloop exit then (fat) @ loop ;
    : index (walk) dup 0= if drop -1 else (start) (index) then ;
    : (bfree) (fat) -1 swap ! update ;  : (bnfree) dup (fat) @ swap bfree ;
    : (kill) 0 0 do dup 0= if drop unloop exit then (bnfree) loop drop ;
    : delete (walk) dup 0= if drop else (kill) then ;
    variable tail  variable needed
    : falloc needed !  0 tail !  maxblocks 0 do
        i (fat) @ dup -1 = if
          tail @ swap ! update i tail !  -1 needed +!
          needed @ 0= if  1 unloop exit  then  \ success
        else drop then
    loop  tail @ (kill) 0 ;
    : emptydent maxfiles 0 do i file or 0= if i unloop exit then loop -1 ;
    : open >r 2dup extent if rdrop 2drop -3 exit then r>
      falloc 0= if 2drop -1 exit then
      emptydent dup -1 = if drop 2drop -2 exit then
      (file) dup >r 2!  tail @ r> 4+ ! update  0 ;

That’s about 1.4 kilobytes of Forth, two blocks, about a third of it
being OPEN.  And here are the non-core words mentioned above:

    : old ( d -- ) 2dup  extent 0 ?do  2dup i index load  loop  2drop ;
    : stat ( d -- ) 2dup or if 2dup .filename ." " extent . cr else 2drop then ;
    : ls-l ( -- ) maxfiles 0 do i file stat loop ;

A slightly better approach
--------------------------

Traversing a linked list backwards is not that hard if you got to
where you were by traversing it recursively.  With this structure, it
would be trivial to insert a new block at the beginning of the file;
adding a new block at the end isn’t that bad either.  Maybe I should
reverse the lists and allow zero-size files.  Then, at the user level,
you’d have operations of creating a zero-size file, appending a block
to a file, and deleting the file.  Also I think it might be better if
the name-to-inode operation (walk) was exposed; then user code would
be directly exposed to the case where the file doesn’t exist.

    filename ( c-addr u -- d )  convert ASCII filename to a double number
    .filename ( d -- )      print the filename d
    touch  ( d -- n ) create empty file named d, returning inode or -ve on error
    walk   ( d -- u )       return inode for file named d, or -1 if none
    iname  ( u -- d )       return name of file with inode d, or 0 if none

    imax   ( -- 50 )        return the maximum valid inode number
    length ( u -- u )       return the number of blocks in file with inode u
    rm     ( u -- )         delete file with inode u
    extend ( u -- flag )    append a block to file with inode u; return 0 if ok
    index  ( u1 u2 -- u3 )  return the block number for block u2 of inode u1

The new EXTEND needs to find the file, find a free block, find the
location of the end of the chain, and add the free block to the end of
the chain.  Alternatively, we could build the chains backwards, and
INDEX could call LENGTH to figure out what iteration to stop at; then
EXTEND would be something like this:

    : extend balloc dup if swap (file) 4 + splice update 0 else drop -1 then ;

This requires a SPLICE word which takes a block number A and an
address where another block number B (possibly 0) is located.  What we
want is for B to become the successor to A, and A to be stored at the
address, thus inserting A at the head of the linked list.  I think
this looks like this:

    : splice ( u a-addr -- ) 2dup @ >r (fat) r> swap ! ! update ;

And a BALLOC which finds the first free block or returns 0.

    : balloc maxblocks 0 do i (fat) @ dup -1 = if i unloop exit then loop 0 ;

This seems substantially simpler than the 6 lines of falloc above.

Then TOUCH needs to find an empty directory entry and create an empty
file there, no longer requiring the stack juggling that OPEN did:

    : emptydent maxfiles 0 do i file or 0= if i unloop exit then loop -1 ;
    : touch 2dup walk -1 <> if 2drop -3 exit then
      emptydent dup -1 = if drop 2drop -2 exit then
      (file) dup >r 2!  0 r> 4+ ! update  0 ;
