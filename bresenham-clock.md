I was thinking about a timing problem I want to solve, and came to
some startling conclusions about timer peripherals for
microcontrollers.  Most of those conclusions turned out to be wrong,
though.

Background: microcontroller timers
----------------------------------

I was just looking through the dadashit for the ATtiny45, because I
was trying to figure out how to implement LINbus-like auto-baud-rate
detection for a project I’m tentatively calling “Lindobus”.  This is
motivated by the LINbus’s ability to adapt to ±15% local clock rates
by keying off the master’s clock: to initiate a transaction, the
master first pulls the bus low for a long time (13 bits I think?) and
then transmits a “sync byte”, 0x55, which with a start bit and a stop
bit is 1 0101 0101 0, and so reliably contains 10 transitions.

A slave that wants to clock in later bytes and maybe reply can key off
the time required for those 10 transitions to figure out what the
current ratio is between the master’s clock and its own, or more to
the point, how many of its own clock cycles a bit time is, to within a
tenth of a clock cycle, or an eighth if it only times to the last
falling transition.  (Timing only the falling transitions is a good
idea because it’s an open-collector bus.)  It’s supposed to keep using
that timing.  As long as its clock doesn’t speed up or slow down by
more than a few percent during the transaction, it should work
reliably.

This is appealing to me because I have a passel of ATtiny45 chips,
which are probably ±10% (I haven’t measured, but that’s what the
dadashit says).  So it would be great to use this technique.  But I
want to see if I can do 500kbps instead of LINbus’s 20.  In theory
this should work fine, since the chip can run at 8MHz, and it has a
built-in serial interface involving a shift register that can be
clocked from its timer, and someone at Atmel even published an appnote
on doing a (half-duplex!) UART that way.

The problem with generating fast UART clocks from Atmel timers
--------------------------------------------------------------

But there’s a really irritating problem.  500kbps at 8MHz is 16 clocks
per bit.  You can set the timer to clock the shift register every 15
clocks, every 16, every 17, whatever.  But what if your initial count
of 8 falling bit transitions was 124 clocks?  That comes out to 15.5
clocks per cycle.  Clocking the shift register half a clock cycle
early or late on any *given* bit is not going to be a big deal, but if
you respond to each start bit by setting up the timer to clock in a
whole byte before interrupting you again, bing bing bing, you’re
clocking in the eighth bit 4 clocks early or late!  That uses up more
than half your timing error budget!  If you clock 7.25 bits early or
late, you’re clocking on an expected edge instead of in the middle of
a bit like a good boy!

Different AVRs do this variable-period timer thing in a couple of
different ways.  Apparently the more limited AVRs like the ATtiny26
just let you preload the timer-count register to something that’s
almost overflowing, like 240, and then interrupt you when it overflows
from 255 to 0.  But for PWM purposes they’re doing a
full-counter-width comparison for ordering every cycle anyway,
outputting the comparison result as the PWM signal.  More recent AVRs
(including the ATtiny25/45/85) have modes where the equality
comparison either reverses the direction of counting or resets the
counter to 0.  Their counters can also count either up or down.

Since using the comparison limit register (which they call OCRnA) to
reset the counter or reverse its direction makes it useless for
adjusting the duty cycle of a PWM waveform, there’s a second register
“OCRnB” that also gets compared with the count value on every count to
produce a variable-period, variable-duty-cycle waveform.

The Bresenham clock: a single-state-variable digital differential analyzer
--------------------------------------------------------------------------

At this point, I think it would be much more rational to just make a
single-state-variable digital differential analyzer!  A synchronous
up-counting register (as opposed to a ripple counter) involves a
half-adder on each bit, in addition to the 4T or 6T SRAM cell for the
bit itself.  I don’t know what a synchronous up/down counter requires,
but it can’t be as simple as an up-counter—if you had an up/down
counter that was as simple as an up-counter, you could tie its up/down
input high and algebraically eliminate some gates.  What Atmel has
built is an 8-bit up/down counter with *two* full-width comparators,
which I’m pretty sure is more logic than a DDA.

What I have in mind is a counter which does the following logic every
clock cycle:

    count += (count & 0x80 ? a : b);

Just as with the Atmel design, this involves three 8-bit registers; it
also involves an 8-bit 2-MUX (controlled by the counter’s sign bit)
and an 8-bit adder.  So for each of the 8 bit positions, you have a
2-mux, a full adder, and three bits of SRAM — plus the logic to
interface those 3 bits to a data bus.

This is more a PDM generator than a PWM generator, but in cases where
switching losses are low, that’s a big improvement.  The time that the
output is low is 128 cycles ÷ b; the time that it’s high is 128 cycles
÷ a.  So the high/low time ratio is b/a, and the duty cycle is
b/(a+b).  So you can get low-frequency PWM waves, but in a small
selection of duty cycles; for example, if a = b = 1, you get period
256 with a 50% duty cycle, and if a = 2 and b = 1, you get period 192
with a 67% duty cycle.  Its state space is concentrated in the
high-frequency range rather than the low-frequency range.  I think the
need for lower-frequency PWM waves can be adequately satisfied by
prescaling the clock to a lower frequency, though that will have more
jitter than the conventional comparator approach, and a wider
prescaler is needed.

To get a particular duty cycle, an easy approach is to choose b+a =
some constant such as 256, set b to b+a times the duty cycle, and
compute a from it.  For example, if we want a 37.53% duty cycle and we
use 256, b should be about 96, so a should be 159.

Hmm, that doesn’t work; it produces an almost perfect 50% duty cycle
alternating between 1 and 0 every clock cycle, with the occasional
repeated 0.  I’m clearly screwing something up here!

High-precision frequency generation
-----------------------------------

To satisfy my initial example of generating a baud rate clock with a
period of 15.5 clocks, we can for example take a = 16 and b = 17,
which gives us a period of 15.53 clocks — there’s still an error, but
it’s 17 times smaller than with the OCRnA/OCRnB approach.  It turns
out that, if you don’t care about the duty cycle, you can do much
better than this; with a = 100 and b = 9, the period is 15.502 clocks,
with a duty cycle of 8.3%.  That’s because, with these settings, the
output is high for an average of 1.280 cycles and low for 14.222
cycles.

(XXX that doesn’t work either, probably because it’s based on the same
faulty math.  16:17 gives us 7 0s, 8 1s, 8 0s, 7 1s, 8 0s, 8 1s, 8 0s,
7 1s, 8 0s, 8 1s, 8 0s, 7 1s, which is indeed almost precisely the
right period but has more 0s than 1s, except at the beginning.  9:100
gives us a 0, 7 1s, two 0s, 6 1s, two 0s, etc., which is off by a
factor of 2.  So the rest of this section is wrong:)

The imprecision this division scheme introduces into the output clock
speed in this example is 130 ppm, comparable to the imprecision due to
quartz oscillators.  The first few attainable periods in the 15–16
range are 15.0027 (9:164), 15.0048 (11:38), 15.0069 (10:58), 15.0075
(9:163), 15.0123 (9:162), 15.0173 (9:161), 15.0222 (9:160), 15.0273
(9:159), etc.  If you picked a random target frequency corresponding
to a period in this range and tried to hit it with 9:something, you’d
always be within ±170ppm.

And this precision, on average, increases dramatically with higher
frequencies, though coverage is quite inconsistent.  Here’s a section
of the possible periods around 4.98 cycles:

     35: 97 4.9767305
     43: 64 4.9767442
     41: 69 4.9770237
     32:131 4.9770992
     29:227 4.9776698
     45: 60 4.9777778
     40: 72 4.9777778
     36: 90 4.9777778
     30:180 4.9777778
     29:226 4.9801648
     30:179 4.9817505

We’re moving along nicely with increments of .000014, .00026, .000075,
.00057, .00010 cycles, and then we have four ways to get the same
period (with different duty cycles, which we’re presupposing here is
unimportant), and then there’s a giant gap from 4.9777778 to 4.9817505
of .004 cycles, a jump of 1000 ppm.

Phase-correct PWM
-----------------

One of the modes of the Atmel design is phase-correct PWM mode, in
which the counter alternates between counting up and counting down to
produce a triangle wave rather than a sawtooth.  With this design, to
minimize phase error, you can change the values of a and b when the
second-most-significant bit of the counter changes, which is to say,
when it is incrementing past 64 or -63; but unless they’re exactly
those values, you get a phase shift which you would have to correct by
setting the count register to the correct phase, which you can also do
with the Atmel design but are not required to.  This moves a little
more complexity from hardware into software, which I think is the
correct place for it.

Other modes of operation
------------------------

By connecting the mux and clock inputs to external lines instead of
the comparator and a clock, and setting a = 1 and b = 255 (-1 in
twos’-complement), you get a quadrature decoder, albeit one that only
counts full cycles rather than individual transitions.

By setting a or b to 0, you get a one-shot pulse generator.  This
nicely avoids a special case in PWM generation where your PWM doesn’t
go all the way to the rails.  Our PWM goes to 256!

By setting a = b + 128, you get a line rasterizer.  Suppose we’re in
the first octant.  Then b/128 is the fractional “error” number of
pixels to increment *y* for each increment of 1 in *x*.  Whenever bit
8 goes high for a cycle (before being cleared by a), you increment the
integer value of *y*.

Bresenham’s line algorithm
--------------------------

That’s not Bresenham’s algorithm, but the DDA algorithm, which
requires a division to initialize it.  I think you can also use it to
implement Bresenham’s algorithm.  Here’s a stripped-down version for
first-octant lines:

    for (x = x0; x <= x1; x++) {
      d -= 2*dy;
      if (d < 0) {
        y++;
        d += 2*dx;
      }
    }

If we rewrite that in conditional expressions, with an offset of -2*dy
to the original count value:

    for (x = x0; x <= x1; x++) {
      y += d < 0;
      d += (d < 0 ? 2*dx - 2*dy : -2*dy);
    }

it becomes apparent what to do: we simply set `a = 2*dx - 2*dy` and `b
= -2*dy`, and the sign bit of the counter tells us when to increment
y.  Visualizing this makes the Bresenham line algorithm much clearer
to me.

For drawing lines with Bresenham's algorithm, we normally initialize
our phase accumulator `d` (to dx, or dx - 2dy, in the previous two
examples) so that the line is centered on the desired endpoints.
That’s the reason for the multiplier of 2; if we can just us dx - dy
and -dy if we don’t need that.

Let’s try our 37.53% thing again with this approach.  Let’s take our
dx as 128, and we want our dy to be 37.53% of 128, which would be
about 48.  So let’s set a = dx - dy = 80 and b = -48.  This gives us:

    10100100101001001010010010100100101001001010010010100100101001001010010010

This looks about right.  The sequence turns out to be 37.5% 1s, which
is correct, unlike the previous attempt.  If we try for 5.3%, well,
that should be 6.8 out of 128, so if we pick 7, we get b = -7, a =
128 - 7 = 121, and we get

    10000000000000000010000000000000000010000000000000000010000000000000000001

a sequence which turns out to have 7 1s every 128 bits, as desired.
Moreover, it has 7 cycles every 128 bits.  I think that for the range
of -a/2 < b < 0, we always get -b cycles.

I’m not sure how to get the same proportions but a different
frequency.  I thought scaling a and b down would proportionally reduce
the frequency, but just treating b as unsigned for that calculation
doesn’t work.  To scale down 80:-48 = 80:208 by 4, for example, that
would give a = 20 and b = 52, giving

    00111110001111100011111000111110001111100011111000111110001111100011111000

which is more 1s than 0s.  Which, I mean, that’s what you’d expect,
right?  When we’re outputting 1 we’re incrementing by a, and when
we’re outputting 0 we’re incrementing by b, so when b is about twice
a, we’d expect to get through the 0 zone faster.  But something else
is weird, 128/20 is 6.4, so you'd expect the runs of 1s to be 6 or 7
bits long rather than 5, and 128/52 is 2.46, so you’d expect the runs
of 0s to roughly alternating 2 or 3 bits long instead of all 3.

    for i in range(256): count += a if count & 0x80 else b; print(1 if count & 0x80 else 0, end='')

