In writing file `deliquescent-cement.md` I realized that I had
considered the relevant properties and compounds of most of the
crustally-abundant elements, all the top 12 plus a few lower down the
list, except titanium, which is #9 on the list; 0.57% of the crust is
made of it.  Its oxide, ubiquitous as a white pigment, has Mohs
hardness 6.0-6.5 (or 5.5-6.0 as the metastable polymorph anatase),
doesn't melt until 1843°, and tends to form acicular crystals, and
thus seems like a promising ingredient for strong and refractory
ceramics.  The nitride is very hard (Mohs 9.0) and commonly used in
machine tools; the carbide is even harder and doesn't melt until
3160°.

So what are the possibilities of depositing titanium salts as cements?

Unlike the other common metals (aluminum, iron, calcium, sodium,
magnesium, potassium, manganese, nickel, and cobalt) titanium is very
often found in an oxyanion: ilmenite (Mohs 5.6-6, the main titanium
ore) is iron titanate, barium titanate is an important piezoelectric
(and so too lead titanate and lead zirconate titanate), calcium
titanate (melting point 1975°) is perovskite, though not the
bridgmanite and enstatite that make up so much of the mantle, and so
on.  Aluminum does this sometimes, but titanium is a real slut.  I
want to say it's "amphiphilic" or that its oxide is, but I don't think
that's quite right.

Titanium does act like a normal metal sometimes; for example, it has
chlorides!  But its most popular chloride is TiCl₄, which is not
ionic; it's covalent, and it's a volatile and reactive liquid which
fumes profusely in moist air.  You can react it with alcohols to get
[titanium alkoxides][0] which react with water to deposit the oxide.
So that's definitely a way to stick things together with titania.  It
has sulfides too!  They also hydrolyze to the oxide.

[0]: https://en.wikipedia.org/wiki/Titanium_ethoxide

In other normal-metal behavior, it does have a lactate salt, which was
used as a mordant for leather a century ago.  This makes me think it
isn't really the lactate salt.

Anyway, none of the easy double metathesis reactions you can use with
aluminum, magnesium, or zinc are applicable to titanium.  It doesn't
have a phosphate, a carbonate, or an acetate, and its hydroxide is,
loosely speaking, titanic acid.  It sort of has a nitrate and a kind
of sulfate, but they react violently with water.

What about giving up on titanium as a cation and making titanates,
such as ilmenite or perovskite?  This is complicated by the absence of
water-soluble titanate salts.  Sodium metatitanate exists but is
water-insoluble.  Bismuth titanate is made by heating oxides of
bismuth and titanium together; calcium titanate is made by heating
oxides of calcium and titanium past 1300°; zinc titanate similarly, or
at room temperature with a ball mill.

WP says, "Barium titanate can be synthesized by the relatively simple
sol–hydrothermal method," linking [a paper from 02015][x], but doesn't
go into any detail, except to mention that it can be attacked by
sulfuric acid.

But I looked at the paper.  As I understand it, it says to dissolve
titanium tetrachloride in hydrochloric acid in an ice bath, add
aqueous barium chloride dropwise while stirring, add lye dropwise
while stirring, then heat to 100° in a sealed autoclave for three
hours.  So I guess the tetrachloride doesn't *always* react violently
with water, just normally.

[x]: https://pubs.aip.org/aip/adv/article/5/11/117119/661543/Preparation-of-meta-stable-phases-of-barium

[Lead titanate][y] can also apparently be synthesized hydrothermally.

[y]: https://en.wikipedia.org/wiki/Lead_titanate
