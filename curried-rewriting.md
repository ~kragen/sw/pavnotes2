The Lisp S-expression `(a b c)` means cons[a, cons[b, cons[c, nil]]].
The parentheses are functional; `a` is an atomic symbol, `(a)` is a
list consisting of that symbol, and `((a))` is a list whose only
member is a list whose only member is that symbol.  This clashes with
standard semantics for parentheses, but for Qfitzah it caused another
problem.

I had wanted to be able to write rewrite rules with this notation:

    If Yes then else: Do then
    If No then else: Do else
    Not Yes: No

Interpreted as S-expressions, `Do then` is understood to be `(Do
then)`, i.e., cons[lit[Do], cons[var[then], nil]].  But then do we
interpret `No` as `(No)` or as `No`?  So I added parentheses and
dropped the colon, eliminating the ambiguity but impairing
readability:

    (If Yes then else) (Do then)
    (If No then else) (Do else)
    (Not Yes) No

I have been flirting with rose trees to eliminate this difficulty, but
today a different resolution occurred to me.  In combinatory logic
and, for example, ML and Haskell, we normally consider `a b c` to mean
`(a b) c`: an application of function `a` to argument `b`, returning a
result which is applied to argument `c`; although in fact this is
generally implemented by holding in abeyance the rewriting dictated by
`a` until enough arguments are present.

That is, juxtaposition is treated as a left-associative binary
operator, and parentheses are only ever used for grouping.

Handling variable-length lists
------------------------------

This would also solve the problem of `(No)`, since it eliminates
single-element lists, and it also resolves another problem in Qfitzah
that had been plaguing me, the handling of variadic lists.  Although
Qfitzah’s pattern-matching recurses only on cons pairs, there’s no way
to input a pattern that matches a variable-sized tail, like the Lisp
`(a b . cs)` syntax.  This means that you can’t write, for example,
mapcar in a natural way; you need to write things like

    (Cons a (Cons b (Cons c Nil)))

I think the combinatory-logic approach offers a solution.  I think Map
looks like this:

    Map f List: List
    Map f (xs x): Map f xs (f x)

In this way Map works its way from right to left along the list it’s
being applied to, adding more and more arguments it will never
examine, and when it reaches the beginning of the list (an atom named
List), it evaporates away, leaving only an ordinary list.  This will
go wrong awkwardly if applied to something that isn’t a list, getting
stuck at its leftmost token with no applicable rewrite rule, but
that’s not so bad.

If you wanted to check ahead of time, you could define a function Head
with two overlapping rules:

    Head x: x
    Head (xs x): Head xs
    
This relies on the rewrite engine applying only the second rule when
it’s applicable, never the first.  Using this function, you can check
whether the Head of a list is really List before starting to recurse
on it.  If your term-rewriting system doesn’t have the kind of
prioritization that definition relies on, but does have an AtomP
function to distinguish symbols and other atoms from applications, you
can write Head as follows:

    Head x: Head2 (AtomP x) x
    Head2 Yes x: x
    Head2 No (xs x): Head xs

We can convert arbitrary non-list things into lists by replacing the
head with List, here again implicitly assuming prioritization:

    Tail x: List
    Tail (xs x): Tail xs x

Filter works the same way as Map:

    Filter p List: List
    Filter p (xs x): If (p x) (Filter p xs x) (Filter p xs)

Reduce is easier, because it doesn’t depend on the weird argument-list
building trick:

    Reduce v f List: v
    Reduce v f (xs x): Reduce (f v x) f xs

And we can also easily enough do things like alists, supposing we have
some way to do equality tests; in fact, if we can have variables as
the leftmost item in a sublist in the way I’ve been describing above,
we can make alists executable:

    Match k v super k1: Lookup (= k k1) v super k1
    Lookup Yes v super k1: Found v
    Lookup No v super k1: super k1
    Empty k1: NotFound

This gets simpler if our pattern matcher can handle equality and
prioritized matching:

    Match k v super k: Found v
    Match k v super k1: super k1
    Empty k1: NotFound

Converting a list of alternating keys and values into such a
dictionary is super easy:

    Dict List: Empty
    Dict (xs k v): Match k v (Dict xs)

Boolean quantifiers are easy:

    All (xs No):  No
    All (xs Yes): All xs
    All List:     Yes
    Any (xs Yes): Yes
    Any (xs No):  Any xs
    Any List:     No

I don’t think we can define a truly variadic function (as opposed to a
function that produces and consumes variable-length lists), but we can
come close by defining a telescoping data type:

    Sum x y: Sum (+ x y)
    Mus (Sum x): x

This allows you to write an arbitrarily long sum without deep nesting:

    Mus (Sum 3 8 1 8 5 2)

Do
----

If we add a restriction that the left child on the right side of a
rule can be an application or a literal symbol (capitalized in the
examples above) but not a variable, then efficient compilation becomes
much easier, but replacements like `super k1`, `f x`, and `p x` are
not permitted.  Instead we end up with definitions like this:

    Map f (xs x): Map f xs (Do f x)
    Filter p (xs x): If (Do p x) (Filter p xs x) (Filter p xs)
    Reduce v f (xs x): Reduce (Do f v x) f xs
    Do (Lookup Yes v super) k1: Found v
    Do (Lookup No v super) k1: Do super k1
    Do Empty k1: NotFound

Unlike in Lisp, this Do doesn’t have to be a special magical operator
to work; we could say it’s just a method that lots of classes
override.  If it happens that we want to reduce with +, we can make up
a new symbol

    Do Add v x: + v x
    ... Reduce 0 Add ...

or we could even use the same symbol:

    Do + v x: + v x
    ... Reduce 0 + ...

Alternatively, it *could* be magical.

Non-variadic pattern-matching
-----------------------------

Fixed-arity heads are kind of the same as they would be in any other
term-rewriting system:

    Car (Cons a b):  a
    Cdr (Cons a b):  b
    Null (Cons a b): No
    Null Nil:        Yes

    X (Point x y): x
    Y (Point x y): y
    X (Polar r theta): * r (Cos theta)
    Y (Polar r theta): * r (Sin theta)
    Theta (Polar r theta): theta
    Theta (Point x y): Atan2 y x

    Not Yes: No
    Not No:  Yes
    And a b: If a b a
    Or a b:  If a a b

Here I’m using a primitive equality test in the pattern-matching
engine:

    Derivative x x: 1
    Derivative x (+ f g): + (Derivative x f) (Derivative x g)
    Derivative x (* f g): + (* f (Derivative x g)) (* (Derivative x f) g)
    Derivative x (Constant k): 0
    Derivative x (Sin y): * (Derivative x y) (Cos y)

If we want to be able to write `* 3 X` instead of `* (Constant 3) X`
we need some kind of primitive that distinguishes numbers from
symbols:

    Derivative x y: D2 (NumberP y) x y
    D2 Yes x y: 0
    D2 No x (Sin f): * (Derivative x f) (Cos f)

We could generalize that last pattern to look up the function in a
table of functions with known derivatives:

    D2 No x (g f): D3 (DTable g) x f
    D3 (Found g) x f: * (Derivative x f) (g f)
    D3 NotFound x f: whatever you try next

Here’s Peano arithmetic without the assumption of a primitive equality
test:

    = Z Z:         Yes
    = (S x) Z:     No
    = Z (S x):     No
    = (S x) (S y): = x y
    + Z x:         x
    + (S x) y:     + x (S y)

The parenthesized patterns with a literal constant head like `(+ f g)`
will check arity; `(+ f g)` won’t match `(+ 1 2 3)`.  By contrast, if
the literal constant is elsewhere, it can: `(f + g)` can match both
`(1 + 2)` and `(1 1 + 2)`, with `f` in the second case matching `1 1`.
But, as demonstrated above, the top-level pattern can match a leftmost
part of a longer expression; `= Z Z Z` will get rewritten to `Yes Z`.

Evaluation and compilation strategies
-------------------------------------

The above talks a bit about semantics alternatives: whether to have
implicit equality tests in patterns, for example, and how to handle
cases where more than one rewrite rule is applicable.  As I think
Wouter van Oortmerssen explains in his dissertation on Aardappel, the
common choices for handling such conflicts include:

1. Rejecting the program.
2. Prioritizing the rewrite rules by program order, either forwards or
   backwards.  Qfitzah currently prioritizes later rewrite rules over
   earlier ones, the idea being to facilitate interactive REPL use.
3. Prioritizing the rewrite rules by some notion of generality, so
   their order in the program doesn’t matter; in general this can
   fail, in which case you have the choice of which other strategy to
   pick.
4. Applying all applicable rewrite rules, creating an unordered soup
   of states.

In cases with prioritization, the prioritization can take the form of
using *only* the highest-priority rule or, as Prolog does, trying it
first and then failing over to the others.

So far I’m considering only alternative #2, prioritizing by program
order, with later rules taking precedence.

There’s also the question (also mentioned in the thesis) of whether to
rewrite top-down, bottom-up, or in some other order.  These more or
less correspond to normal-order and applicative-order β-reduction in
the λ-calculus; top-down rewriting tends to be less efficient but
provides laziness.  I’m assuming bottom-up rewriting, which
corresponds reasonably closely to expression evaluation in
conventional programming languages, with the major difference that its
data structures are of the same sort as the program text itself.

There’s also the question of what to do when *no* rewrite rule is
applicable.  Aardappel takes the approach of signaling an error if the
head has some rewrite rules attached to it, and otherwise treating it
as a data structure, reimposing the conventional partitioning between
programs and data.  I think I’d rather do the purer thing and just
return the unevaluated data; for interactive use I suspect that adding
a new rewrite rule will often remedy the situation when it is
unwanted, and the unevaluated data gives us ML-style (or
Schönfinkel-style!) implicit currying.

The Aardappel approach to compiling such things is to collect all the
rewrite rules for a given head symbol, compiling them into a single
subroutine.  I think it may be worthwhile to also remember the minimum
number of arguments for each head symbol before any rewrite rule is
applicable, so that you can construct curried closures without
incurring a function call and return.

This suggests imposing the somewhat artificial restriction that all
rewrite rules must specify a literal head symbol; this would be
entirely natural with rose trees, but not completely natural here.  It
probably helps a lot for efficient implementation.

If each node in the binary tree has a copy of its head and the
distance to the head, then invoking the reduction subroutine is
relatively easy; to construct a new application node in a context
where the head is not statically known, you try calling the reduction
subroutine.  When the head *is* statically known, we can check to see
whether we have enough arguments to reduce or not; if not, we can just
create the necessary heap nodes.

It’d be nice to just allocate each of these "lists" into a vector of
some known size, so that `If a b a` occupies the space of four or five
OOPs rather than three possibly non-contiguous OOP-pairs.

Metacircular interpreter
------------------------

Can you write a metacircular interpreter for this kind of rewriting
system?

If we represent application nodes as `App a b` and constant and
variable nodes as `Const x` and `Var y` it seems like it should be
reasonably straightforward.

Unfortunately, in that representation, this rule from the Peano
arithmetic above:

    = (S x) (S y): = x y

turns into

    Rewrite (App (App (Const =) (App (Const S) (Var x)))
                 (App (Const S) (Var y))) (App (App (Const =) (Var x))
                                               (Var y))

So a much more appealing approach would be to have a `IsVar` predicate
for detecting a variable and a special head for quoting lists.  Then
this could be

    Rewrite (' = (' S x) (' S y)) (' = x y)

assuming that this `'` constant isn’t magical in any way, just doesn’t
happen to have any rewrite rules defined for lists starting with it.
If you made it magical like in Lisp, you could write it as

    Rewrite (' = (S x) (S y)) (' = x y)

but this seems like a relatively minor notational improvement for a
major complication of the language semantics.

The current Qfitzah interpreter can’t deal with undeclared variables,
segfaulting when it encounters them, but literally inserting them into
the output seems like a fine alternative.

The core of a term-rewriting system is unification: attempting to
match a term against a pattern and returning bindings, or (if we want
implicit equality testing) attempting to match a term against a
pattern in the presence of some existing bindings and returning more
bindings.  The recursive case is something like

    Unify (p1 p2) (t1 t2) vars: Unify p1 t1 (Unify p2 t2 vars)

Here we’re taking the bindings (or failure) returned by the right
branch and threading them through to the left branch.  Which means we
probably need to thread through failure as a higher-priority case:

    Unify p t Fail: Fail

Constants should only match themselves; here we suppose the later
pattern is higher priority:

    Unify p t v: Uni2 (IsConst p) p t v
    Uni2 Yes p t v: Fail
    Uni2 Yes p p v: v

Variables can match anything if they’re not found, which adds a new
binding to the environment:

    Uni2 No p t v: Uni3 (IsVar p) p t v
    Uni3 Yes p t v: Uni4 (Lookup v p) p t v
    Uni4 NotFound p t v: Match p t v

But if they are found, they succeed with the same bindings iff the
term being matched is their binding, failing otherwise:

    Uni4 (Found b) p t v: Fail
    Uni4 (Found b) p b v: v

So we’ve covered the cases where the pattern is a constant or a
variable.  We could rewrite the earlier recursive case to only attempt
in cases where that turned out not to be true, and to fail if the term
is something other than an application; instead of

    Unify (p1 p2) (t1 t2) vars: Unify p1 t1 (Unify p2 t2 vars)

we have

    Uni3 No (p1 p2) t v: Fail
    Uni3 No (p1 p2) (t1 t2) v: Unify p1 t1 (Unify p2 t2 v)

Putting the whole `Unify` definition together in the proper order,
moving from more general to more specific cases:

    Unify          p       t       v:    Uni2 (IsConst p) p t v
    Unify          p       t       Fail: Fail
    Uni2 Yes       p       t       v:    Fail
    Uni2 Yes       p       p       v:    v
    Uni2 No        p       t       v:    Uni3 (IsVar p) p t v
    Uni3 Yes       p       t       v:    Uni4 (Lookup v p) p t v
    Uni4 (Found b) p       t       v:    Fail
    Uni4 (Found b) p       b       v:    v
    Uni4 NotFound  p       t       v:    Match p t v
    Uni3 No        (p1 p2) t       v:    Fail
    Uni3 No        (p1 p2) (t1 t2) v:    Unify p1 t1 (Unify p2 t2 v)

If by convention both the patterns and the terms in the interpreter
use `'` in this way, we don’t even need a special case for `'`.

This could be cleaned up a bit by consolidating the type dispatch for
the pattern and by invoking the variable bindings as a function:

    Cat p: Cat2 (IsVar p) (IsConst p)
    Cat (p1 p2): App
    Cat2 Yes No: Var
    Cat2 No Yes: Const
    Unify      p       t       vars: Uni2 (Cat p) p t vars
    Unify      p       t       Fail: Fail
    Uni2 Const p       notp    vars: Fail
    Uni2 Const p       p       vars: vars
    Uni2 Var   p       t       vars: Uni4 (vars p) p t vars
    Uni2 App   (p1 p2) t       vars: Fail
    Uni2 App   (p1 p2) (t1 t2) vars: Unify p1 t1 (Unify p2 t2 vars)
    Uni4 NotFound  p t vars: Match p t vars
    Uni4 (Found b) p t vars: Fail
    Uni4 (Found b) p b vars: vars

The definition of `Match` is as given earlier, but again with more
specific cases last:

    Match k v super k1: super k1
    Match k v super k: Found v
    Empty k1: NotFound

Given a variable-bindings object produced by `Unify`, we need to be
able to instantiate a template; for example, if `v` has the value
`Flug`, then `Found v` should be instantiated as `Found Flug`.  This
is much simpler than the unification algorithm itself, since it need
only look up each atom in the given environment to see if it has a
definition, replacing it if so.

    Inst (t1 t2) v: Inst t1 v (Inst t2 v)
    Inst a       v: Inst2 (v a) a
    Inst2 NotFound  a: a
    Inst2 (Found b) a: b

To apply a rewrite rule to a term, we should see if the pattern
matches, and if so, instantiate the replacement:

    Rewrite p r t: Re2 (Unify p t Empty) r
    Re2 v    r: Succeed (Inst r v)
    Re2 Fail r: Fail

To apply a series of rewrite rules to a term, we should iterate
through the list until we find one that doesn’t fail, or return the
original term (as a normal form) if that doesn’t happen.  It’s easiest
to iterate from the end of the list:

    Apply List         t: Normal t
    Apply (rules rule) t: Apply2 (rule t) rules t
    Apply2 (Succeed v) rules t: Redex v
    Apply2 Fail        rules t: Apply rules t

A redex might contain inner subexpressions that need further
rewriting, as well as itself being rewritable, while a normal form
presumably does not.  So when application results in a redex, we
should try to evaluate each of its items and then the overall redex.
I think this looks something like this:

    Eval rules expr: Eval2 rules (Apply rules (Evlis rules expr))
    Eval2 rules (Normal t): t
    Eval2 rules (Redex t):  Eval t
    Evlis rules a:           Eval rules a
    Evlis rules (head tail): Evlis rules head (Eval rules tail)

XXX is this correct?

XXX even if it is correct it is far more complex than it needs to be

All that’s left is to plumb the `IsVar` and `IsConst` predicates
through, which can be done by bolting two special cases onto `Apply`:

    Apply rules (' IsVar x): IsVar x
    Apply rules (' IsConst x): IsConst x

Here I’m supposing that the `Yes` or `No` returned by `IsVar` or
`IsConst` are the same constants as the ones in the value universe of
the language we’re interpreting; if not, you might need a mapping
function.

So those 35 lines of code make up a (probably buggy but essentially
complete) metacircular interpreter for this curried term-rewriting
language.

Local nested definitions
------------------------

This chunk from above is uncomfortably low in signal due to all the
duplication:

    Unify      p       t       vars: Uni2 (Cat p) p t vars
    Unify      p       t       Fail: Fail
    Uni2 Const p       notp    vars: Fail
    Uni2 Const p       p       vars: vars
    Uni2 Var   p       t       vars: Uni4 (vars p) p t vars
    Uni2 App   (p1 p2) t       vars: Fail
    Uni2 App   (p1 p2) (t1 t2) vars: Unify p1 t1 (Unify p2 t2 vars)
    Uni4 NotFound  p t vars: Match p t vars
    Uni4 (Found b) p t vars: Fail
    Uni4 (Found b) p b vars: vars

With nesting, we could write this as something like this, using local
definitions called `2` and `4`:

    Unify       p       t       vars: 2 (Cat p) p t
        2 Const p       notp:    Fail
        2 Const p       p:       vars
        2 Var   p       t:       4 (vars p) t
        2 App   (p1 p2) t:       Fail
        2 App   (p1 p2) (t1 t2): Unify p1 t1 (Unify p2 t2 vars)
        4 NotFound      t: Match p t vars
        4 (Found b)     t: Fail
        4 (Found b)     b: vars
    Unify       p       t       Fail: Fail

Or with an ML-like `match ... with ...` construct:

    Unify       p       t       vars: (match (Cat p) p t with
        | Const p       notp:    Fail
        | Const p       p:       vars
        | Var   p       t:       (match (vars p) t with
            | NotFound      t: Match p t vars
            | (Found b)     t: Fail
            | (Found b)     b: vars
        )
        | App   (p1 p2) t:       Fail
        | App   (p1 p2) (t1 t2): Unify p1 t1 (Unify p2 t2 vars)
    )

That’s assuming that repeated variables inside the match shadow outer
declarations, as in ML.  If, instead, they represent implicit equality
constraints, as in Erlang or arguably Prolog, we can make this a lot
terser (this time, with most specific cases first):

    Unify p t vars: (match vars with
        | Fail: Fail
        | _: (match Cat p with
            | Const: (match p with t: vars | _: Fail)
            | Var: (match vars p with
                | NotFound: Match p t vars
                | Found t: vars
                | Found _: Fail
            )
            | App: (match p t with
                | (p1 p2) (t1 t2): Unify p1 t1 (Unify p2 t2 vars)
                | (p1 p2) _:       Fail
            )
        )
    )

This is still the same 10 lines of code, if we don’t count the
trailing right parens, but now it’s 54 words instead of 82 words.
It’s a great deal less explicit about variable scoping, but in a
certain way it states the program much more clearly, because you don’t
have to make up bogus identifiers like `Uni2` or `2` for each new
conditional, and adjacency clearly delineates what is referred to
where.  And this can be regarded as just syntactic sugar for the
original form, where the extra arguments are added by the compilation
process, and a local identifier for each `match` is invented by the
compiler.

Right-associativity
-------------------

I suspect that if we treat juxtaposition as a *right*-associative
binary operator, so `a b c` is `a (b c)`, we can still make something
similar work.  I mean obviously we can make it work if we put our
function symbols on the right:

    List f Map: List
    (x xs) f Map: (x f) xs f Map

But I think maybe we can also make it work with function symbols on
the left, and maybe in that case we really can do variadic argument
lists.
