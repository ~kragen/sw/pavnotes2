Consider two parallel rigid plates, one with a row of holes with 10
holes per millimeter (100μm spacing) and one with a row of holes with
11 holes per millimeter (90.9μm spacing).  Once per millimeter, the
holes are aligned; the other holes are not aligned.  By shifting one
the two by 9.1μm, the alignment shifts over by one hole; now the
aligned holes are the ones next to the previously aligned holes, but
there is again one aligned hole pair per millimeter.

If you have a 48mm-long comb with one tooth per millimeter, sized to
fit through these holes, you can stick it through the aligned lines of
holes, thus maintaining them in a given alignment.  By pulling it out
(an operation which may be easier if the backbone of the comb is a bit
flexible, and of course if the holes are chamfered and the teeth are
chamfered or even tapered) and reinserting it at a different position,
you can lock the position of the plates at a different 9.1-μm
alignment.

My vague and possibly incorrect reasoning here is that because there
are 48 teeth and 48 holes in each plate, a total of 144, the final
positional error will be the average of the random, uncorrelated
errors in positioning of each one of those 144 things, and so about
1/12 of the random error in each of them.  If this is correct and you
scale this up to a row of 300 teeth, a row of 3000 holes, and a row of
6300 holes, I guess you can get this down to 1/30 of the positional
error with a pretty reasonably-sized device.  (I’m skeptical of this
reasoning in part because it suggests that, if you make the comb
perfect instead of subject to the same random errors as the holes, the
error should be about 1/9.8 instead of 1/12 of the random error in the
hole position.)

By reducing the detuning further, it should be possible to get smaller
steps, down to a micron or less for a handheld device.

An interesting thing about this setup is that, if the holes are
rectangular, each of these three pieces can be made purely by sheet
cutting.

You may be able to use multiple staggered parallel rows of holes in
each plate, and a comb with correspondingly multiple rows of teeth,
though this cannot be made by cutting a single sheet; this should do
an even better job of averaging out the uncorrelated errors among the
different holes and teeth, though how uncorrelated those errors are
will depend a lot on exactly how they’re made.

A variant on this idea has an undriven (idler) pinion moving along two
parallel racks, each half the thickness of the pinion and coplanar
with it, with slightly different tooth pitch.  At the position of the
pinion, it forces the two racks' teeth into alignment, so their
relative position depends on exactly where the pinion is along them.
The natural tendency for the racks to flee from the pinion when it
moves can be counteracted by putting teeth on both sides of the racks
and sandwiching them between two separate pinions.  It is feasible to
make the pinions such that many teeth at once are engaged with the
racks, say 9, which again provides some error averaging, making the
device higher precision than the tools that fabricated it.

Aside from the possibilities for precision, this is perhaps more
interesting as a relatively compact way to get large mechanical
advantages, at least for reciprocating linear motion.  Suppose your
two pinions have 90 teeth each with 1mm pitch; their pitch diameter is
28.7mm, so the whole pinion-pinch device is 57.3mm wide plus the width
of the racks.  Suppose you make it 65mm wide (leaving 7.7mm for the
thickness of the racks) with 120mm of travel, over which the racks
differ in pitch by half a tooth, 0.5mm.  This gives you a mechanical
advantage of 240.

If you make the pitch difference a whole tooth or more, then you can
add a “coarse adjust” mode where you release the compression between
the two pinions, allowing you to set the racks’ displacement freely,
before putting the pinch back on and rolling the pinions along for the
fine adjustment.  This is analogous to vise-grips where you use a
screw thread for the coarse adjustment and a toggle mechanism for the
fine adjustment.

Basically I hate screw threads and want them to die.

Another related candidate mechanism combining an actuating mechanism
with high mechanical advantage and multiple rows of holes uses three
pairs of rows of holes (this time with the same spacing in both
plates) and three separate combs with truncated triangular teeth.
Suppose that the duty cycle of the rows of holes is 2mm hole, 1mm
solid, repeat; then at perfect alignment the hole overlap is 2mm, and
at 180° out of phase, you have a 1mm web in the middle of your
top-layer hole with 0.5mm of bottom-layer holes on each side of it.
Assume for the moment that the plate thickness is negligible.
Consider these three phases:

- 0°: 2mm overlap
- 120°: 1mm hole overlap, 1mm web in hole
- 240°: 1mm web in hole, 1mm hole overlap

Suppose that the tips of the teeth are 0.6mm, their bases are 2mm, and
their lengths are 28mm, and the phase differences of the three rows of
holes are 120° out of phase.  For example, in the top plate the holes
are not staggered at all, but in the bottom plate there is a 1mm phase
shift from each row to the next.  If the middle row is in phase, its
comb can be inserted all the way, but the combs in the top and bottom
rows can only be inserted up to a thickness of 1mm.  Inserting the top
or bottom comb more deeply requires withdrawing both its counterpart
and the center comb by the same distance.  By thus inserting some
combs and withdrawing others, the plates can be moved in either
direction with positive displacement.

Suppose the bottom plate of holes is grounded and the combs are being
moved with reference to it.  Then as long as the combs are moved
correctly, the upper plate has no available degrees of freedom.  It is
pinned against the bottom plate by its points of contact with the
combs, leaving only the three degrees of freedom of planar movement.
At least one of the combs restrains it from translating in one
direction along the line of holes, while at least one of them
restrains it from moving in the other.  And it cannot rotate in the
plane or translate perpendicular to the line without shearing off the
comb’s teeth.  If the combs are withdrawn too much, it develops some
backlash along its line of movement, but in theory that is all.

The mechanical advantage of the whole mechanism is only the slope of
the teeth, 20:1 in this case.  Initially it is a line-contact
mechanism, but if it wears in, it will become a surface-contact
mechanism.

If the plates’ thickness is not negligible, the holes in the bottom
plate can be made correspondingly thinner.

The above assumes that the teeth move translationally; but with these
dimensions, this cannot be simply as by a prismatic joint
perpendicular to the plates of holes, because the hole overlaps are
generally not in the centers of the holes, so the combs have to move
longitudinally as well as into and out of the holes.  By increasing
the duty cycle of the holes to anything more than ⅔, such as ¾, you
can relax this and use a simple reciprocating motion of the combs.
Then, by modifying the shape of the teeth, it is possible to suit them
for actuation by in-plane rotation around a pivot instead, if the
pivot is not too close to the holes.

This has the potential advantage that you can in some cases eliminate
or greatly simplify the grounded bottom plate if the rigidity of the
combs themselves is sufficient for your purposes, because in a sense
the only purposes of the bottom sheet are to support the movable plate
and to support the teeth of the combs as they push along the movable
plate.

Of course the three out-of-phase rows of holes can be in any relative
position as long as they are parallel; for many purposes it may be
useful to put them in a single line, with the three phases of combs
coming one after the other.

Rather than a perpendicular plate with holes in it, you can use a rack
with tapered teeth in it, permitting the “duty cycle of the holes” to
vary continuously over a large range, and the contact to be full
surface contact from the beginning, without wearing in.  This works
better with translational motion for the combs, or rotational motion
around an axis parallel to the rack, rather than in-plane rotational
motion, because if you’re rotating the combs in-plane, the radius of
curvature on their teeth must vary with distance from the pivot, which
means they cannot make full surface contact from the beginning in this
way.

Of course it is equally possible to drive a toothed or perforated
wheel in the same way as a toothed rack or perforated strip, by
pressing three or more combs into its teeth or holes with the right
phases.  An involute tooth profile on the wheel will mesh properly
with truncated-triangle profiles on the combs, as is usual for rack
and pinion setups, though of course only with line contact rather than
full surface contact.

Returning to the question of decreasing the detuning in the original
device for setting a displacement, suppose our 48mm-long row of
100μm-spaced holes has another 489 such rows running in parallel with
it, all 100μm apart.  Now we reduce the detuning from 9.1μm per hole
to 10μm per row of about 480 holes, and we turn our comb 90 degrees so
that it engages one hole in every tenth row, since each hole is now in
phase with the hole 10 rows below it rather than 10 holes to the
right.  Each hole we move the comb to the left or right changes the
displacement by 20.8 nanometers, at least if effects like elasticity,
thermal expansion, backlash, and random error don’t waylay us.

So, by placing the comb in a set of 48 of a quarter of a million holes
that we locate in two dimensions to a precision of ±100μm, easily done
with naked-eye vernier scales, we can determine the displacement of
the two plates in *one* dimension to a precision of ±10.4nm.

A simpler way to take advantage of a second dimension of translational
freedom is to turn the line of holes into a striped area full of
parallel slots.  If the slots in one of the plates are slightly angled
from being parallel, you can slide the comb up and down for a second
level of fine adjustment.
