Consider a 16-bit processor with a 16-bit address bus but only a
12-bit stack pointer.  It has one-byte local-fetch/local-store
instructions that include a 3-bit or 4-bit offset field; to form the
effective address, the offset is *concatenated with* the stack
pointer, plus a trailing 0 in the case of 16-bit access instructions.
Call and return instructions increment and decrement the stack
pointer, giving each subroutine call 16 bytes of automatically
reentrant local variables without any extra context save and restore
instructions and without requiring an implicit addition for effective
address calculation.

This uses up 16 or 24 of the 256 single bytes of the instruction space
but is likely to substantially improve code density.

Unlike the SPARC’s register windows, this mechanism can’t be used for
argument passing and return, but traditional non-windowed registers
(or a 6502-style zero page) works fine for that.  If the return
address (link register) is stored in the caller’s stack frame instead
of the callee’s, it gives leaf subroutines an extra register or two
and can share space with a temporary (“caller-saved”) register.