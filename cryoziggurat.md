The Buenos Aires summer is harder for me every year; even 24° is too
much for me now when the humidity is high, and 26° is punishing.  The
other day it got to 39°, and it isn’t even summer yet.  I have a
portable air conditioner which looks like R2-D2, but it only helps if
you’re right in front of it.  So I want to build a little styrofoam
hut in my living room that I can keep cool, called the Cryoziggurat.

Why R2-D2 alone doesn’t keep me cool
------------------------------------

First, R2-D2 the portable air conditioner is only 2500 watts (or maybe
3000, I forget, possibly because 2500 watts is 2900 kilogram-calories
per hour, and kilogram-calories per hour is a common way to rate air
conditioners here: “*frigorías*”).  My apartment is 40 m², with about
4 m² of sunlight at times, and the only internal door is the one on
the bathroom.  4 m² of sunlight is about 4000 watts of heating, even
without conduction or convection.

Second, R2-D2’s airflow is suboptimal.  Because its cold air output
streams out a curvy surface on the front of it, you have to have it
*in* the space you want to cool; it sucks ambient air in the back,
cools it, and spews some of it out the front.

But the heat extracted and produced has to go somewhere.  So it uses a
large part of the air sucked in the back to cool the condenser; this
hot air blows rapidly through a hose you’re supposed to run, somehow,
to the outdoors.  This means that R2-D2 decreases the pressure in the
room where you’re operating it, sucking in air from adjacent
spaces — under doors, through plenum spaces, whatever.

My apartment’s only access to the outdoors is through a pair of
sliding-glass doors onto the balcony.  If I open the door enough to
route the heat outdoors, it has lots of air leaks, which is a terrible
combination with R2-D2 sucking air into the apartment.  It means the
35° air from outside replaces the 30° air that was left over in the
apartment from before sunrise.  Under those circumstances, the more I
run R2-D2, the hotter it gets in the apartment.  Sometimes I can use
duct tape to patch up the air leaks enough so that the incoming air
comes under the door from the hallway, from the core of the building,
rather than in off the balcony.  But it’s unreliable and still leaky.

So I want to build a little hut in the living room that will keep me
cool, and that’s the Cryoziggurat.

Basic outline of the Cryoziggurat
---------------------------------

The walls are 40-mm-thick styrofoam; anything thinner would probably
be too fragile.  The outer walls are 2 m × 3 m × 1.7 m for the outer
walls: 5.1 m² for each of the long walls and 3.4 m² for the short
walls, for a total of 17 m² of walls.  The roof is roughly pyramidal,
with trapezoid faces topping the longer walls, going up to a height of
2.3 m in the center, giving a hypotenuse distance from the center of a
wall along the roof to the peak of 1166 mm (a roof seked of 7 · 1 m /
600 mm = 11.7, which is a coincidence, or 31° from the horizontal).
This can be analyzed as four triangles of base 2 m and height 1.166 m
(1.166 m² each, 4.664 m² total) plus two 1 m × 1.166 m rectangles
(2.332 m² in total), for 3.498 m² of roof and 20.498 m² of total walls
and roof, say 20.5.

Previously I had thought of making it 2 m × 2 m, but that wouldn’t
really have room for a bed (1510 mm × 1900 mm, though that’s a little
small for me) or for another person.  At 2 m × 3 m I think I can fit a
bed and seating at the same time.

The ceiling and possibly the walls will have a mirror finish to give
the feeling of being in a larger space.  The floor will have a carpet
to reduce heat ingress through the floor, which is concrete and
porcelain.  Because sunlight brings too much heat with it, LED strips
will illuminate the interior.

R2-D2 will be inside, with cardboard ductwork connecting its air
intake to a giant hole in the Cryoziggurat wall, so it draws air from
*outside* the Cryoziggurat and creates a *higher* pressure inside of
it.  A new extra-long exhaust hose will connect its hot air exhaust to
the balcony.

Suppose the walls are 12 kg/m³, laminated together from two layers of
20-mm-thick sheets.  This is 0.82 m³ and 9.8 kg of styrofoam (±20%),
costing US$42 at US$1.03/m² or US$51.72/m³ (see file
`material-observations-02022.md`).  Assuming a thermal conductivity of
0.04 W/m/K, average for styrofoam (and probably high for such
lightweight styrofoam), and an interior-to-exterior temperature
difference of 15 K (e.g., 20° to 35°), the heat leakage by conductance
through the foam should be 308 W.  This is about a tenth of the
thermal power advertised by the cheapest portable air conditioners,
including R2-D2.

Ventilation
-----------

Approximating the Cryoziggurat as 2 m × 3 m × 2 m, it contains roughly
12 m³ of air and 14 kg of air, so doubling the CO₂ level from 400 ppm
to 800 ppm should take about 6 g of CO₂.  A person exhales about 1 kg
of CO₂ per day, so 6 g is only about 8 minutes.  Above 2000 ppm people
complain of headaches and drowsiness, which would take one person half
an hour, and above 5000 ppm you can get dizziness and nausea (1½ hours
with one person).  So maintaining ventilation is important!

Oxygen depletion take an order of magnitude longer than that, so
scrubbers might help but could be dangerous; ventilation is needed,
through regenerators if necessary, with pedal-powered fans.

This is another reason for wanting to make the space larger than the
2 m × 2 m originally planned.

1 kg CO₂ per day is about 12 mg CO₂ per second.  If we’re ventilating
out 1000-ppm air and replacing it with 400-ppm air, we’re effectively
removing 600 ppm, and at 600 ppm (by mass) 12 mg of CO₂ is 20 g/s of
air, which is 17 ℓ/s at STP (1.2 kg/m³) or, in medieval units, 35
“cfm”.  40 g/s, 33 ℓ/s, or 71 cfm would be needed for two people,
which works out to about 10 air changes per hour.  Without a
regenerator or recuperator, with a ΔT of 15 K (35° outside, 20°
inside) and 1 J/g/K, this is a heat loss of 600 W, which is
uncomfortably large.  So a regenerator would be helpful for when the
air conditioner fails.

Supposing that the air conditioner is emitting 15° air into the
cryoziggurat, sucking in 35° air from outside, running at 2500 thermal
watts at 1 J/g/K.  If so, it must be spewing out 125 g/s of air, about
three times the 40 g/s we need for two people, so if it's running a
third of the time we’ll be fine.  It’s annoying as hell to have it
running so much, but because the air it’s cooling starts out so hot,
it actually has to run a lot to keep the cryoziggurat cool under these
circumstances.  Suppose the air it’s driving out of the cryoziggurat
(under the walls or whatever) is 20°.  Then it’s replacing 125 g/s of
20° air with 15° air, which is only 625 W.  This is still twice the
conduction leakage of the walls but it’s no longer so far away as to
consider heat leakage through the wall minor; it’d have to run half
the time just to compensate for the heat loss through the walls.

Surroundings
------------

The living room where I want to put the Cryoziggurat is some 2.95 m
wide, 7.42 m long, and somewhere between 2.40 and 2.55 m high.  I want
to put it more or less in the middle of the room.  That means that,
unless I walk *through* it to get to the other side of the living
room, I can’t make it much more than 2 m on its narrow dimension
because 0.95 m is a pretty thin space to walk past it in.  (Mina’s
front door is 750 mm wide.)

The house’s bathroom is 1.52 m × 2.30 m, with a ceiling lowered to
2.27 m, and is comfortable enough to spend an hour in the bathtub
without feeling cramped (though now that I look at the ventilation
numbers above maybe I shouldn’t!).  This is a little over half the
space planned for the Cryoziggurat.

The house’s kitchen is 1.92 m × ≈4 m with the same ceiling height as
the living room, but only 1.34 m of that width is walkable; the other
580 mm is taken up by the stove, sink, and counter.  Nevertheless it's
wide enough for one person to be standing at the counter and another
to walk behind them.  So 2 meters is probably wide enough for the
width.

One of Alexander’s Patterns is the “six-foot balcony”, which he says
is just wide enough for two people to be able to face one another.

It might be a good idea to make the Cryoziggurat span the entire width
of the living room, with a door in the front and a door in the back.
Suppose it’s 2.95 m on the outside (to fit into the apartment width)
with 40 mm thickness of each wall; that leaves 2.87 m inside.  If we
figure on 6 m² of floor area, then we need 2.091 m of width, leaving
5.33 m of length in the living room, which could be comfortably
portioned into a 2.3-meter room facing the sliding glass door and a
3.03-meter room further back.  The plaster and concrete in contact
with the styrofoam will probably eventually provide a little
additional insulation.  Concrete’s thermal conductivity is about
1.6–3.6 W/m/K, so our 40 mm of styrofoam is worth about 200 mm of
concrete, which is probably thicker than the building's entire wall
and definitely thicker than what separates me from Sabrina next door.

The alternative, placing it parallel to the long axis of the living
room, would admit more sunlight to the back part of the living room,
but even with an uncomfortably small 750-mm-wide passage to the side
of it, you'd only have 2.12 m of internal width, so the same 6 m² of
floor would require 2.83 m of the living room’s length, leaving only
4.59 m — for example, 2 m in the room facing the sliding glass door
and 2.59 m further back.

Construction technique
----------------------

I can buy 1 m × 1 m expanded styrofoam sheets reasonably cheaply, but
I don’t yet know how to stick them together into larger wall modules.

A regenerator?
--------------