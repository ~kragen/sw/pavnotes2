Residue numbering systems can give you higher clock rates because you
don’t need such long carry chains.  What if you used an RNS throughout
a computer?  You could get error-correction coding that also detected
overflow.

Basic RNS proposal
------------------

Suppose the bases of your RNS are 32, 31, 29, 27, and 25, which are
relatively prime, and your word size is 25 bits, but your memory is
only 31 × 29 × 27 × 25 = 606825 words in size, for an effective 19.6
bits — a bit small by modern standards, but big enough for garbage
collection and a multitasking OS with a GUI.  We can convert a linear
memory address represented as an integer to a 25-bit word by reducing
it modulo each base (here using PARI/GP):

    ? bases = [32, 31, 29, 27, 25];
    ? repr(x) = [Mod(x, b) | b <- bases];
    ? [print(repr(x)) | x <- [1, 10, 100, 1000]];
    [Mod(1, 32), Mod(1, 31), Mod(1, 29), Mod(1, 27), Mod(1, 25)]
    [Mod(10, 32), Mod(10, 31), Mod(10, 29), Mod(10, 27), Mod(10, 25)]
    [Mod(4, 32), Mod(7, 31), Mod(13, 29), Mod(19, 27), Mod(0, 25)]
    [Mod(8, 32), Mod(8, 31), Mod(14, 29), Mod(1, 27), Mod(0, 25)]

Converting back involves Sunzi’s Chinese Remainder Theorem (or rather
either Aryabhata’s Pulverizer Algorithm or alternatively the 大衍術 of
Qin Jiushao 秦九韶, since Sunzi didn’t really publish a full
solution).  In reading the following, keep in mind that GP numbers
its vector indices from 1 and uses inclusive ranges.

    ? derepr(x) = if(#x == 1, x[1], chinese(x[1], derepr(x[2..#x])));
    ? [derepr(repr(x)) | x <- [1, 10, 100, 1000]]
    %22 = [Mod(1, 19418400), Mod(10, 19418400), Mod(100, 19418400), Mod(1000, 19418400)]

As is usual for residue number systems, the usual arithmetic
operations work elementwise, with the exception of division:

    ? derepr(repr(1234) + repr(765))
    %51 = Mod(1999, 19418400)
    ? a = repr(1234); b = repr(2); derepr([a[i] * b[i] | i <- [1..#a]])
    %52 = Mod(2468, 19418400)
    ? a = repr(1234); b = repr(2); derepr([a[i] - b[i] | i <- [1..#a]])
    %53 = Mod(1232, 19418400)
    ? a = repr(1234); b = repr(2); derepr([a[i] / b[i] | i <- [1..#a]])
      ***   at top-level: ...4);b=repr(2);derepr([a[i]/b[i]|i<-[1..#a]])
      ***                                             ^------------------
      *** _/_: impossible inverse in Fl_inv: Mod(2, 32).
      ***   Break loop: type 'break' to go back to GP prompt
    break>

That’s because 2 has a common factor with 32 (the factor being 2
itself), so 2 mod 32 doesn’t have a multiplicative inverse.  We can
still divide by a number that is relatively prime to all of our bases:

    ? a = repr(7777); b = repr(7); derepr([a[i] / b[i] | i <- [1..#a]])
    %55 = Mod(1111, 19418400)

But only if the dividend is divisible by it:

    ? a = repr(7778); b = repr(7); derepr([a[i] / b[i] | i <- [1..#a]])
    %56 = Mod(16645454, 19418400)

The ECC part is that, as long as our memory addresses don’t overflow
or otherwise get corrupted (for example by dividing by a non-divisor),
we can use any subset of four of the pieces to recompute them, which
means that we can recompute the missing one:

    ? mil = repr(1000)
    %43 = [Mod(8, 32), Mod(8, 31), Mod(14, 29), Mod(1, 27), Mod(0, 25)]
    ? mil[1..4]
    %44 = [Mod(8, 32), Mod(8, 31), Mod(14, 29), Mod(1, 27)]
    ? derepr(mil[1..4])
    %45 = Mod(1000, 776736)
    ? [derepr(concat(mil[1..i], mil[(i+2)..#mil])) | i <- [0..#mil-1]]
    %46 = [Mod(1000, 606825), Mod(1000, 626400), Mod(1000, 669600), Mod(1000, 719200), Mod(1000, 776736)]

Problems I don’t know of a solution for that make this impractical
------------------------------------------------------------------

Unfortunately, I don’t know of a way to do this that is anywhere close
to being efficient enough to compete with more traditional
error-correction approaches like Hamming codes.  However, those don’t
check your computations, just your data storage and transmission.

A different efficiency topic is that the 5-bit field that can only
hold values up to 25 is in some sense not being very efficiently used,
because it only holds 4.6 bits of information.  But the 25-bit word as
a whole only holds 19.2 bits of information if we’re using one of the
5-bit words as an error check, so the missing fractional bits are less
important.

The other serious efficiency issue with RNS computations is that,
while comparison for equality is just normal comparison, AFAIK
comparison for ordering requires converting back to regular numbers
with the Chinese Remainder Theorem, and there’s no fast way to do
that.  (I guess I should read the relevant section of Knuth.)

Casting out 9s and 11s is an alternative with most of the same virtues, and is practical
----------------------------------------------------------------------------------------

A more practical way of applying modular arithmetic to error-check
binary math would be to augment each 32-bit word with its value
reduced modulo 255, 257, or both; the reductions require,
respectively, three eight-bit additions (with ones-complement-style
end-around carry), and two eight-bit additions and a nine-bit
subtraction (likewise).  With carry-save addition this can be only a
few more propagation delays than a single addition.  This scheme
avoids the comparison-for-ordering-related inconveniences of residue
numbering systems, and checking the code is sufficiently fast to be
practical.  Even checks modulo 15 and/or 17 would not be unreasonable,
with slightly longer path length but less ECC bits.  Such checks can
catch errors in data storage and transmission, addition, subtraction,
or multiplication, and I think they can perhaps be extended to cover
bitwise operators.

These checks are arguably more powerful at error detection than things
like Hamming SECDED codes.  If a result checks modulo both 255 and
257, which are relatively prime, you know it checks modulo 255·257
= 65535.  This covers all one-bit errors, but also any errors that
affect only the left half or the right half of a 32-bit register.  It
is possible for a two-bit error to go undetected, it would have to add
some quantity 2ⁱ to the number, while subtracting 65536·2ⁱ = 2ⁱ⁺¹⁶, or
vice versa.

Because (2*ⁿ* + 1)(2*ⁿ* - 1) = 2²*ⁿ* - 1 < 2²*ⁿ*, you can
theoretically represent such a pair of almost-*n*-bit “check digits”
as a single 2*n*-bit field despite the possibility that one of them
wouldn’t quite fit in an *n*-bit field.  Say your number is 254 and
you want to generate check digits mod 15 and mod 17.  254 % 15 == 14,
and 254 % 17 == 15.  This is the highest these two check digits can
get (I computed 254 with the Chinese Remainder Theorem again).  I
think we can represent them by multiplying the second remainder, 15,
by the first divisor, which is also 15, giving 225, and adding the
remainder by that first divisor, 14, which gives 239.  This is
basically just treating the pair (15, 14) as a two-digit number in
base 15, and ignoring the fact that the most significant digit is
slightly too large to technically be in base 15, because the second
digit never will be.

As alluded to above, computing the pair (15, 14) from 254 doesn’t
involve actual long division.  To compute it mod 15 we add its hex
digits of 0xfe with end-around carry: 0xf + 0xe = 0x1d, 0x1 + 0xd =
0xe = 14₁₀.  To compute it mod 17, we subtract them: 0xe - 0xf = -1.
I’m not totally clear on how to handle that at the bit level...

To compute (15, 14) from their encoding 239, getting the 14 is the
same procedure: 239 = 0xef, 0xe + 0xf = 0x1d, 0x1 + 0xd = 0xe = 14.
Getting the 15 involves getting the integer quotient from dividing 239
by 15.  You can do this in 8-bit overflow-discarding math by
multiplying 239 - 14 = 225 by its multiplicative inverse mod 256,
which is, uh,

    ? Mod(15, 256)^-1
    %3 = Mod(239, 256)

it’s 239.  This is probably not a coincidence.  It does work:

    ? (239 - 14) * 239 % 256
    %6 = 15

I think you can multiply by the constant 239 in 8 bits with a shift,
an add, and a negation, given its binary representation:

    ? digits(239, 2)
    %3 = [1, 1, 1, 0, 1, 1, 1, 1]
    ? (0 - (225 << 4) - 225) % 256
    %15 = 15
    ? [(0 - (x*15 << 4) - x*15) % 256 | x <- [0..15]]
    %18 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

Possibly a better choice than 17 and 15 would be something like 17
and 13.  I suspect (but haven't checked) that that would catch all
two-bit errors.
