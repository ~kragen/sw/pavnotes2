Here are some notes about current sensing.  Although they are written
in a sort of authoritative voice, they shouldn’t be, because I haven’t
actually tried the stuff I’m talking about, except in simulation.

Basic high-side current sensing with a filter
---------------------------------------------

[I drew this circuit to demonstrate high-side current sensing for
measuring power consumption][0]:

    $ 1 0.000005 0.23009758908928252 50 5 43 5e-11
    R -112 128 -112 80 0 0 40 5 0 0 0.5
    r -112 128 -112 240 0 10
    r -112 320 -112 416 0 680
    g -112 416 -112 432 0 0
    r -32 320 -32 416 0 1000
    g -32 416 -32 432 0 0
    w -112 240 -32 240 0
    s -112 240 -112 320 0 1 false
    s -32 240 -32 320 0 1 false
    p 80 128 80 240 1 0 0
    r -112 128 -16 128 0 10000
    c -16 128 -16 240 0 2.2000000000000002e-8 1.1723955140041653e-13 0.001
    w -16 128 80 128 0
    w -16 240 80 240 0
    w -16 240 -32 240 0
    403 112 160 240 224 0 9_2_0_4098_0.0000762939453125_0.1_-1_1
    403 -272 160 -144 224 0 1_2_0_4099_0.0000762939453125_0.00009765625_-1_2_1_3

[0]: http://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWK0BMBmOBOA7AgHJpASnigiiApJSACxqUCmAtGGAFABKIrYFfeHmwp5qYutSTiYCdgCchfcCUUUUtcWEjzVINCmq8KtMADYoIU6PYBzXSfNG6+i9oXMX+wy4cWtcWx4fMyDjFzF2AHdddW81DSh2AGcYhKcvPxAAMwBDABskxmTQkFiSjOoIXIKigAcQUWVBRrKICPdhJqFzAT94bQBjbq7WczLqFFR+6emUFkEYeH0UFExlyGwwWhwENClYSA5o0a7G3u1jkLKWhIvhso94iI0GTrMJhJXaC0wAfRRfpBfhpMHhAdAwAhTGRwWBfqxfhwXjwUNh+KZDFtvl8-P9AcDIJg-jBIdCEODIDCEQC4Wh2EA

This is a simple high-side current measurement setup consisting of two
resistors, a capacitor, and some kind of differential voltage sensor
such as an isolated oscilloscope.  The 10Ω resistor on the left
generates 10 mV of voltage drop per mA of load current, and then we
measure that drop to find out what the load current is.

There’s a load at the bottom consisting of two resistors and two
switches.  By turning the switches on and off you can change the load
instantly; if you’re sampling the signal this could be a problem,
because quick current spikes or sags could pass unnoticed because
they’re between samples.  So the 10-kΩ resistor forms a single-pole RC
filter with the 0.022-μF capacitor with a 220-μs time constant τ.
This gives you a pretty flat frequency response up to about 4 kHz, at
which point the signal is attenuated by 3 dB, and drops off at 6 dB
per octave after that, so signals above 16 kHz are attenuated by I
think 15 dB.

Straightforward approaches to getting better precision
------------------------------------------------------

If you sample the output of the above circuit with a USB sound card at
48 ksps then current signals above 24 kHz will be attenuated by at
least a factor of 10 before being aliased back into the signal you
measure.  This is not super high precision!  If the interference
signal contains as much power as the signal you’re trying to measure,
it’ll be introducing a 10% error.  For some purposes that’s fine,
though.

0. Sampling the signal at a higher sampling rate, such as 96 ksps or
   1 Msps.
1. Using a sound card with a good antialiasing filter of its own,
   which most of them have.
2. Using a bigger capacitor like 0.22 μF, which would give you another
   factor of 10 attenuation by dropping the cutoff frequency by a
   factor of 10.  I picked 10kΩ on the theory that at 5V that’s only
   half a milliamp, so it won’t disturb the power supply too much.
   Unfortunately you might have to use a lower resistance like 1kΩ in
   order to keep a soundcard input from loading the filter too
   heavily, since [sound cards can have input impedances as low as
   600 Ω and rarely as high as 47 kΩ][2].  Oscilloscope inputs and
   digital voltmeter inputs will reliably have input impedances of
   1 MΩ or more, so this is less of a concern there.
3. [Using a second stage of passive filtering][1], if your voltage
   sensor has a high enough input impedance for the further weakened
   signal; here I’m using 100kΩ and 2200pF to get an additional factor
   of 10 attenuation by 24 kHz:

        $ 1 0.000005 0.23009758908928252 50 5 43 5e-11
        R -112 128 -112 80 0 0 40 5 0 0 0.5
        r -112 128 -112 240 0 10
        r -112 320 -112 416 0 680
        g -112 416 -112 432 0 0
        r -32 320 -32 416 0 1000
        g -32 416 -32 432 0 0
        w -112 240 -32 240 0
        s -112 240 -112 320 0 1 false
        s -32 240 -32 320 0 1 false
        p 176 128 176 240 1 0 0
        r -112 128 -16 128 0 10000
        c -16 128 -16 240 0 2.2000000000000002e-8 6.756373238658853e-12 0.001
        w 80 128 176 128 0
        w 80 240 176 240 0
        w -16 240 -32 240 0
        403 208 160 336 224 0 9_2_0_4098_0.0000762939453125_0.1_-1_1
        403 -272 160 -144 224 0 1_2_0_4099_0.0000762939453125_0.00009765625_-1_2_1_3
        r -16 128 80 128 0 100000
        c 80 128 80 240 0 2.2000000000000003e-9 2.4789059693830495e-11 0.001
        w -16 240 80 240 0

4. Using an active filter built around an op-amp in order to get a
   higher order of filtering without weakening the signal too much.

[1]: http://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWK0BMBmOBOA7AgHJpASnigiiApJSACxqUCmAtGGAFABKIrYFfeHmwp5qYutSTiYCdgCchfcCUUUUtcWEjzVINCmq8KtMADYoIU6PYBzXSfNG6+i9oXMX+wy4cWtcWx4fMyDjFzF2AHdddW81DSh2AGcYhKcvVxAAMwBDABskxmTQkFiSjPFcgqKAB3BscwF68zKICPdhZUFWRpVqfwCAYyFe7pCy6hRUeBnZuBQWQRg+SBRsMkh8PDwEbDA8cxg4DmjRLubz7VPJhLAG0oSrkYe4l8SNBgNBM2o0NBb1BZMAB9FDAyDAjSYPDg6BgBCmMiwsDA1jAjgfHhrfimQxgWi0UqA-qg8GQyCYEHLBFIo58BColFglFoHQ9c5nJr9WbsYaclRnCalaZzUVoFiYZBsTAIfQ4dY4Ux-WiHWCQE7PMqCx7sIA
[2]: http://hardandsoftware.mvps.org/sound_card.htm

Differential vs. single-ended measurements
------------------------------------------

If you were wiring this up to a sound card or oscilloscope to do the
measurement, there’s the risk that the ground of the circuit under
test is the same as the ground of the sound card.  What you ideally
want is for either the instrument or the device under test to be
“floating” so you can get a *differential* voltage measurement across
the current-measuring shunt.  Alternatively you can take two
concurrent measurements of the two sides of the shunt; if one is at
4.879 V and the other is 5 V, you can subtract them to deduce that you
have 121 mV of difference between the two sides, and thus (with a 10-Ω
shunt resistor) 12.1 mA of current.  This adds some imprecision: a 1%
error on either of these two single-ended voltage measurements will
give you a 40% error in the inferred differential voltage.

(As an aside, you really don’t want to feed 5 V into the input of a
sound card directly.  You can use a voltage divider to divide the
voltage by 20 or so to get to a safe level.)

[Here's a brute-force way to convert the differential signal to
single-ended][5]:

    $ 1 0.000005 0.23009758908928252 50 5 43 5e-11
    R -112 128 -112 80 0 0 40 5 0 0 0.5
    r -112 128 -112 240 0 10
    r -112 320 -112 416 0 680
    g -112 416 -112 432 0 0
    r -32 320 -32 416 0 1000
    g -32 416 -32 432 0 0
    w -112 240 -32 240 0
    s -112 240 -112 320 0 1 false
    s -32 240 -32 320 0 1 false
    p 176 128 176 240 1 0 0
    r -112 128 -16 128 0 1000
    c -16 128 -16 240 0 2.2e-7 0.0007340553004953065 0.001
    w 80 128 176 128 0
    w 80 240 176 240 0
    w -16 240 -32 240 0
    403 112 48 240 112 0 9_2_0_4098_0.15625_0.1_-1_1
    403 -272 160 -144 224 0 1_2_0_4099_0.15625_0.0125_-1_2_1_3
    r -16 128 80 128 0 10000
    c 80 128 80 240 0 2.2000000000000002e-8 0.0025311768509546084 0.001
    w -16 240 80 240 0
    a 432 144 544 144 9 15 -15 1000000 2.5012369168079163 2.5012369421189256 100000
    r 304 128 384 128 0 10000
    r 304 240 384 240 0 10000
    w 432 240 432 160 0
    w 384 240 432 240 0
    w 384 128 384 304 0
    w 384 128 432 128 0
    r 384 304 384 368 0 10000
    g 384 368 384 384 0 0
    r 432 240 544 240 0 10000
    w 544 144 544 240 0
    368 544 240 592 240 0 0
    a 224 144 272 144 9 15 -15 1000000 5.002473884237851 5.002523908976694 100000
    w 176 128 224 128 0
    w 224 160 208 160 0
    w 208 160 208 208 0
    w 208 208 304 208 0
    w 304 208 304 144 0
    w 304 144 304 128 0
    w 304 144 272 144 0
    a 208 256 288 256 9 15 -15 1000000 4.999942732698412 4.999992732125739 100000
    w 288 256 304 256 0
    w 304 256 304 240 0
    w 304 256 304 304 0
    w 304 304 208 304 0
    w 208 304 208 272 0
    w 176 240 208 240 0
    403 560 144 688 208 0 31_2_0_4098_0.15625_0.2_-1_2_31_3

[5]: http://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWK0BMBmOBOA7AgHJpASnigiiApJSACxqUCmAtGGAFABKIrYFfeHmwp5qYutSTiYCdgCchfcCUUUUtcWEjzVINCmq8KtMADYoIU6PYBzXSfNG6+i9oXMX+wy4cWtcWx4fMyDjFzF2AHdddW81DSh2AGcYhKcvPxAAMwBDABskxmTQkFiSjOoIXIKigAdwbHMBBvMyiAj3YWVBViaVSvh2AGMhPp6QsuoUVBZsZHhsNA0EBAxIWkxVyFMpWEgOaNFuluPtQ6mEsEbShLPRm7iHxI0GLtpBNqVqTAB9FB-ID8NJg8ADoGAEKYyGCwD9WD8OC8eChsPxTIYwLRaKV1H4-gCgZBML8YBCoQgwftofD-rC0Dpescjs0BoMRsyVEdJqVUPA+fy4CgWIIYILVmxGngqJtaOi8NjRfsovcylzbuwcs5+FjKDrMdjMOAkKwkP4+TyqHw0KZMGZRNhbaYGNNLegbbQUGxiJDwPydBhsc00PLTr7BgoA09g9juWa7vR4tQE+B0YlotGnsnJsqM0GQ5G7rmVMmWf78+s9PmrH4+YEM9bBPWQx0tU8EDrY7Xou3Azqe09tA3dTGEghMInXBqcb2Y6jwDrDRChKaBZQ9upFnh5ehsFKIAh1+Q0IQCI13WHBtErmNp6dlShcWYpkQUxFogZBE-Si+P2nvx8X0jX9CwrX9I31P9wJ1cD+hzCsIJRbUFSnX8yFaLdSh9RdjSXOM4DoaBiWJD1FhQG15SUWhCKI8dSL4BBFkXP13wwtC9FAn0QJjH0gPVdMOPMSMCzg7EgMAis7jA0CfznO5ryeVD1SRSFKh1KwAJFPRYX+QFgVBUlIWhGB-hpH40DpdggA

Probably someone who actually knows things could do this with a
Sallen-Key filter containing a single op-amp and no other active
circuit elements.

All this differential-to-single-ended stuff is a result of doing the
sensing on the high side.  If you put your shunt resistor on the
ground side of the load instead of the high side, the problem goes
away.  But it’s common for circuits to handle grounds at slightly
different voltages very poorly; for example, if you have a CPU
connected to a display, and you want to measure the power consumption
of just the CPU or just the display, they might have a hard time
communicating if their grounds are at different potentials.  So often
we prefer high-side current sensing despite its drawbacks.

(Of all the stuff in here that I’m being overconfident about, this
explanation is the one I'm least sure of.)

On shunts
---------

Current-measuring shunts are a little bit tricky.  You want the
resistance to be high enough to give you an easily measurable voltage,
but not so high that the load gets unhappy with its sagging power
rail; 10 ohms is suitable up to a few tens of mA, while for hundreds
of mA you probably want to use 1 ohm.  Off-the-shelf wirewound or
thin-film resistors may have significant parasitic inductance (an ESL
meter can tell you, or you can run a frequency sweep across it in
series with some other component, or a step).  As I understand it,
[the standard way to solve this problem is to make your own resistor
by taking a thin insulated copper wire a few meters long, folding it
in half so as to cancel the inductance, and winding it up to make it
compact][3].  At 58 Siemens m/mm² and 0.321 mm diameter, 28 AWG copper
wire should be 0.21 Ω/m; 32 AWG should be 0.201 mm diameter and 0.54
Ω/m.  So a 1-Ω shunt is about 2–5 meters of thin off-the-shelf wire,
and a 10-Ω shunt is about 20–50 meters.

[3]: https://www.youtube.com/watch?v=j4u8fl31sgQ

Well, 0.1 mm diameter wire would be 2.20 Ω/m, which would be a lot
more convenient for a 10-Ω shunt, and that would be 38-gauge wire, but
it’s hard to find it that thin.  Some magnetics vendors will sell you
magnet wire as thin as 42-gauge (0.063 mm diameter, 5.47 Ω/m).
However, lots of stranded cables in things like Ethernet are made of
wires about that size twisted together.  You can untwist one and paint
some of the hair-thin wire with whatever kind of plastic in order to
insulate it, then cut it to the length for the resistance you need.

Measuring the resistance of a 1-ohm resistor is a little bit tricky
because the usual multimeter technique, setting the multimeter to ohms
range and putting one multimeter lead on each end of the resistor,
doesn’t work, because the resistance of the multimeter leads
themselves, not to mention the connection to the resistor, can easily
be a large fraction of an ohm.  Instead you have to use a so-called
“Kelvin connection”: you connect *four* wires to the resistor, two of
them carrying no current and going to a voltmeter, and two of them
carrying a current which you are measuring at the same time (for
example by running it through a milliammeter).