I was reading an old post on Scott Aaronson's blog, and ran across
[this comment by John Sidles][0]:

> To bounce-back Bram Cohen’s post: “*I’s pretty clear that the reason
> why ‘nanotech’ is a bigger buzzword now than ‘microtech’ is implicit
> in the Fermi Question: ‘Which is larger, the number of stars in the
> causally connected universe, or the number of atoms in a child’s
> little finger?*”

[0]: https://scottaaronson.blog/?p=346#comment-11516

I was curious which was larger; [Wikipedia's Universe article says][1]
the observable universe is 46 billion light years in diameter, and
galaxies are typically 3 million light years apart, making the edge of
the observable universe about 15000 galaxies away.  [Later it says][2]
there are about 200 billion galaxies and 10²⁴ stars in the observable
universe.  Not all of this is "causally connected", though; because
it's expanding, most of these stars are not able to receive signals
from Earth anymore.  I'm not sure of even the order of magnitude of
how many are, but presumably it's in the range 10²⁰ to 10²³.

[1]: https://en.wikipedia.org/wiki/Universe#Size_and_regions
[2]: https://en.wikipedia.org/wiki/Universe#Composition

[A child's little finger][3] is about 13 mm in diameter and 56 mm
long, working out to 7.4 mℓ.  It is very close to the density of
water, 1 g/cc, so it's about 7.4 g.  It's mostly carbon (12 amu) and
oxygen (16 amu) by mass, though with significant amounts of calcium
(40 amu) and phosphorus (31 amu) in the bone.  But it has a lot of
hydrogens in it, being about 80% water (H₂O) by mass, with most of the
rest being either fat (roughly CH₂) or protein (also roughly CH₂;
[valine][4] is C₅H₁₁NO₂, [threonine][5] is C₄H₉NO₃, and [lysine][6] is
C₆H₁₄N₂O₂).  So water (18 amu, three atoms) should be a pretty good
approximation.

[3]: https://pubmed.ncbi.nlm.nih.gov/20399088/ "Lengths, girths, and diameters of children's fingers from 3 to 10 years of age, by B Hohendorff, C Weidermann, K J Burkhart, P M Rommens, K J Prommersberger, M A Konerding.  Ann Anat. 2010 May 20;192(3):156-61.  PMID: 20399088.  doi:10.1016/j.aanat.2010.03.002"
[4]: https://en.wikipedia.org/wiki/Valine
[5]: https://en.wikipedia.org/wiki/Threonine
[6]: https://en.wikipedia.org/wiki/Lysine

Well, so we're talking about roughly 0.4 moles of water (or similar)
and 1.2 moles of atoms, which is about 7 × 10²³.

So the observable universe probably contains slightly more stars than
the number of atoms in a child's little finger, but the number of
stars something from today's Earth could conceivably travel to is
significantly smaller than the number of atoms in a child's little
finger.

The number of cubic microns in a child's little finger is, by
contrast, only about 7.4 × 10¹², which is closer to the number of
galaxies in the observable universe, and only a little larger than the
number of stars in a single giant galaxy.
