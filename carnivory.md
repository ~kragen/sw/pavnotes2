Notes on trying a carnivore diet for 30 days.

I've been having diarrhea frequently for years, sometimes very
aggressive with little warning, and one of my objectives is to see if
this diet helps with that.  Concentration and mood control are another
objective.  Getting thinner, or at least ameliorating metabolic
syndrome, is a third.  Unfortunately I didn't take a baseline of
anything, except that I have been weighing 122 kg lately.

I expect that to the extent that I have any food sensitivities, they
will tend to disappear during this trial, unless they happen to be
meats or yerba mate.

02023-04-07
-----------

Starting.  As of this writing I forget what I ate, probably some brie
and meat.

02023-04-08
-----------

Bought 1.4 kg of *bife de chorizo* to share with Mina; ate most of it.
At my house I grilled some on the griddle; at her house we boiled the
remainder with some salt, herbs, and spices.  Also, at a café, had
some chicken thighs and drumsticks with bacon fat.

Had coffee and IIRC yerba mate.

Some terrible-smelling diarrhea in the evening and another episode in
the middle of the night, both fairly laid-back.  Since the water is
out at Mina's, this is pretty inconvenient.  I took some Mylanta
liquid (hydroxides of magnesium and aluminum) at night for stomach
pain.

02023-04-09
-----------

Slept 9 hours this morning.

Easter with Mina's family.  Two boiled eggs, plus bland beef and pork,
for lunch and dinner.  Drank mate.  Didn't poop.  Added some butter to
the meat.  Noted significant hunger, some obsession over foods; the
mate was in part an effort to suppress this.  No intestinal pain, a
little stomach pain after eating.  Bought 18 brown eggs for AR$800
(US$2.05).

02023-04-10
-----------

Slept some 7 hours this morning.

Starting these notes; what is above is backfilled.

Woke up at 8:00 with no hunger and boiled an egg; starting to get
hungry around 8:30.

I'm thinking that eggs, yolks in particular, may be crucial.  I need
to get about 70% of my calories from fat; supposing that's 2000 kcal
per day, I need 1400 kcal from fat and 156 g of fat, the other
600 kcal coming from 150 g of protein: roughly a 1:1 fat:protein ratio
by weight.  But I've had steatorrhea in the past at times, so I'm
worried about eating too much fat; my bile salts can only go so far in
making fat digestible, and I lose them when I have diarrhea.  Where
egg yolks come in is that they have lots of lecithin, a phospholipid
which can function as a surfactant.

I've also drunk some mate.

Amber tries not to eat egg whites because they contain [avidin][0], an
antinutrient that binds biotin (vitamin B₇) to keep bacteria out of
the egg.  I don't think I'm in much danger of biotin deficiency,
though.

[0]: https://en.wikipedia.org/wiki/Avidin

Here are some candidate foods:

- [pork chop/*bondiola*][1]: 15 wt% fat, 28 wt% protein
- [whole egg][2]: 10 wt% fat, 13 wt% protein (about 50–80 g per egg)
- [egg yolk][3]: 27 wt% fat, 16 wt% protein
- [sirloin (*bife de chorizo*?)][4]: 14 wt% fat, 27 wt% protein
- [butter][5]: 81 wt% fat, 1 wt% protein
- [chicken drumsticks][6]: 11 wt% fat, 27 wt% protein
- [mackerel (such as *caballa*)][7]: 18 wt% fat, 24 wt% protein
- [horse mackerel (such as *jurel*)][8]: 7 wt% fat, 16 wt% protein
- [sardines][9]: 11 wt% fat, 25 wt% protein
- [salmon][10]: 6 wt% fat, 22 wt% protein
- [tuna][11]: 1 wt% fat, 26 wt% protein

[1]: https://www.fatsecret.com/calories-nutrition/generic/pork-chop-ns-as-to-fat-eaten?portionid=50115&portionamount=100.000
[2]: https://www.fatsecret.com/calories-nutrition/generic/egg-whole-raw?portionid=51772&portionamount=100.000
[3]: https://www.fatsecret.com/calories-nutrition/usda/egg-yolk?portionid=56525&portionamount=100.000
[4]: https://www.nutritionix.com/food/top-sirloin/100-g
[5]: https://www.nutritionix.com/food/butter/100-g
[6]: https://www.fatsecret.com/calories-nutrition/usda/chicken-drumstick-meat-and-skin-(broilers-or-fryers-roasted-cooked)?portionid=57185&portionamount=100.000
[7]: https://www.nutritionix.com/food/mackerel/100-g
[8]: https://asturpesca.com/en/2021/07/13/the-atlantic-horse-mackerel/
[9]: https://www.fatsecret.com/calories-nutrition/generic/sardines-cooked?portionid=50746&portionamount=100.000
[10]: https://www.fatsecret.com/calories-nutrition/generic/salmon-raw?portionid=50737&portionamount=100.000
[11]: https://www.fatsecret.com/calories-nutrition/generic/tuna-canned-water-pack?portionid=50789&portionamount=100.000

As a side note, I'd prefer to keep my saturated fat intake down, which
is going to be difficult with animal sources and especially with
butter.  Also, all these websites of nutritional information are
amazingly shitty.

To get the requisite 150 g of fat per day from just eggs I'd need to
eat 1.2 kg of eggs, about 18 eggs a day.  Evidently I'm going to have
to work pretty hard to get enough fat without supplementing with extra
fat sources like butter.  But, in the short term, butter is probably
not going to give me a heart attack this month.

Post-egg, post-mate Wechsler reverse digit span results: 10.
That's... astoundingly good, especially on four hours of sleep.  I did
the same test in late January and got 8.  However, I was very
unreliable; I missed one of the two number sequences at 4, 6, 7, 8, 9,
and 10, while usually I'm fine up to 6.  This is not very good
evidence of a cognitive *boost* but at least it's evidence that I'm
not suffering major cognitive *impairment*, despite only getting 7
hours of sleep.

For lunch I fried three eggs in butter.  Later I had a snack of fatty
cheese.

The last time I tried to do a ketogenic diet I got dry itchy skin on
my legs.  Today I seem to be getting dry, flaky skin on my left elbow,
which I haven't had in a long time (weeks at least, maybe months), so
it's almost certainly due to the diet.  I put clarified butter on it,
and also on my calves just in case.

For dinner I fried three more eggs (11 total eggs for the day) and
cooked a bunch of mutton.  I bought 730 g of fatty mutton from the
halal butcher and cooked it up to share with Mina; also I fried her up
some empanada shells in the excess mutton fat.  I have... a lot of
indigestion now.  But I haven't pooped.

I have a little bit of a sore throat, which is alarming.  Earlier
today I had lung irritation from, perhaps, the perfume in the Uber
car.

02023-04-11
-----------

I woke up at 9:00 fairly vibrating with energy, which is a thing that
hasn't happened in quite a while.  And no anxiety.  Also, no hunger,
no pooping, no intestinal pain, and no indigestion.  Or maybe a little
bit of indigestion.

However, I might be a little bit dumb, and after being awake for an
hour (taking a shower and wanking) I no longer feel as great.

Ah, and at 10:30 (awake for an hour and a half) I pooped and then had
diarrhea.  It's not voluminous, and it's kind of yellow.  Later I took
an hour-and-a-half nap, after which I got a reverse digit span of 9
without modafinil.  I still haven't eaten anything today even though
it's 12:30, but I don't feel a lot of temptation.

For breakfast I eventually ate a mutton steak I'd cooked the night
before.

Lunch was two fried eggs and *bife de chorizo* at a restaurant for
$4000 (US$10.50).  My tongue has been tingling a bit as if it had
lidocaine or soap on it.  After paying the rent I had mild
steatorrhea, yellow with visible oil floating in the toilet water.
Spooked, I ate a small lemon ice.  This provoked pain on the left of
my chest and some later digestive pain.  I had a bit of headache.
Then I had an alfajor.  I don't know if I've been in ketosis (Mina
hasn't reported smelling ketones) but these carby foods don't seem
like an improvement.  Then I had a sugar-free Monster.

The steatorrhea has not been urgent and uncontrollable like my usual
diarrhea.

I was thinking that for dinner I might fast, but instead I ate three
eggs fried in the mutton fat, as well as some leftover bits of mutton
(cooked a second time just to be sure, and also to reduce the fat
content).

02023-04-12
-----------

Last night I had a headache which made it hard for me to sleep despite
taking melatonin.  I blame the sugar, but time will tell if that
correlates.

I slept until about 13:45 today, and awoke with the vibrating
sensation again.  With modafinil I got a reverse digit span of 9
today.  I feel like the day-to-day variations in this sequence (8, 10,
9) are mostly just noise, and I should depart from the Wechsler
protocol and use a better one; the test today lasted over three
minutes, and sometimes they last over four minutes.

My father reports that I sound happy, which is a very good sign given
that Mina and I had a terrible fight yesterday that I'm still sad
about.

In this context it's worth noting that, thanks to last night's rain
and today's clouds, the temperature today is cool (18°), while
yesterday it was hot (27°?).  It hadn't gotten hot when I did
yesterday's reverse-digit-span test.

It's 17:00 and I'm starting to get hungry, because all I've had to eat
today is some yerba mate.  I have some errands to do, so I'm frying a
couple of eggs in some of the leftover mutton fat.

I burned the eggs slightly, but they were okay.  I think I am going to
have a sugar-free Monster when I go out for errands anyway.

I had some loose stool, but today it seems like a more normal brown
color rather than yellow.  It didn't mix with the water the way
diarrhea does (and normal poop doesn't) but I think it may have left
an oily sheen on the water; my toilet is very suboptimal for observing
this.

The irritation on my left ring finger from the brass wedding ring I
was wearing until the 9th has begun to subside.  I've replaced it with
a new silver wedding ring, decorated with gold on the outside.

For dinner I've cooked up 915 grams of *bife ancho*, a single huge
steak.  This is kind of annoying to eat, maybe because I didn't cook
it enough, with chewy fibrous parts.  After eating about half of it I
put it back to cook some more, which does seem to have helped.

I put the brass ring on my right hand, and within an hour or two it's
already irritating me again.

At 0:30 after a small Coke Zero I got a reverse digit span of 10.

I didn't go to sleep until 5 AM, with another episode of diarrhea,
which was again brown rather than yellow.

02023-04-13
-----------

I woke up at about 12:30, thus getting about 7½ hours of sleep, with a
bit of intestinal discomfort.  Upon going to the bathroom, more
diarrhea, perhaps steatorrhea despite being brown.  Also the vision in
my good eye is pretty astigmatic; probably I deformed my cornea by
sleeping face down on the pillow for a while, and it will return to
normal in a while.  It seems to be doing so, though sometimes when I
blink it gets worse again, so maybe it's mucus in my eye?  I'll drink
something.

I'm not hungry at all.

Calculating the *bife ancho* as 915 g of "sirloin" above, 14 wt% fat
should be 128 g of fat, and 27 wt% protein should be 247 g protein,
for a total of 2140 kcal, on top of the two eggs (maybe 130 g, 10 wt%
fat, 13 wt% protein, 13 g fat, 17 g protein, 185 kcal).  Probably I
should aim to eat a bit less than that for the next few days; that's
too much protein for me to metabolize, and I should still be aiming
for a level of fat that doesn't give me diarrhea.  If that's the
cause.  It's not very aggressive diarrhea.

Under the new silver ring, my skin irritation seems to be subsiding.
And so far on the right ring finger the brass ring isn't producing a
pimple like it did originally on the left.

Disabling astigmatism still persists an hour after waking, though less
severe, despite a shower and drinking mate.  I've increased the text
size to compensate imperfectly.

Yesterday I had modafinil; today I will not.

The astigmatism is gone by 15:00, 2½ hours after waking up.

No stomach indigestion in the last couple of days, which is an
improvement.

I see that there's a burr on the brass ring which was probably the
thing that created the pimple.  I'll see if I can sand that down.

Around 15:30 I start to get a little hunger.

Talking to my parents I had a pretty dumb cognitive error: I thought
that the time since I ate last night at 5:00 had been almost 24 hours,
but actually it was almost 12 hours.

I got a sugar-free Monster (same as yesterday, and a dubious idea for
many reasons, among which are potential intestinal irritation and
tooth health) and 200 g of Swiss cheese (*pategras*).  Before eating
the cheese I got a reverse digit span of 9.

[Fatsecret claims Swiss cheese][12] is 28 wt% fat, 27 wt% protein, and
5.4 wt% carbohydrate, but only 1.3 wt% sugars.  So this is about 11 g
of carbohydrate.  The Monster also has about 5.7 g of carbohydrate,
supposedly.

[12]: https://www.fatsecret.com/calories-nutrition/usda/swiss-cheese?portionid=56451&portionamount=100.000

I weighed two of my eggs before frying them.  They weighed
respectively 61.43 and 66.34 g.  One of their yolks weighed 16.76 g
after frying.  This hopefully gives some kind of crude indication of
the sort of eggs I'm eating.

At night I had a bit more than a quarter of a chicken for dinner.

02023-04-14
-----------

So this is day 8.  I got up at 8:30 and started boiling an egg in
order to tune my circadian rhythms.  Mina reported that it smelled of
ammonia, which I evidently couldn't smell, a problem I've had since
before trying this crazy diet.  I clumsily knocked a jar of instant
coffee on the floor, where it broke, and I was upset because it meant
I couldn't make Mina her coffee (until after I went to the
mini-supermarket to buy more).

Later, hungry, I ate two more boiled eggs and a large wedge of Swiss
cheese Mina brought me.

I got a reverse digit span of 8 and haven't had modafinil.

Some formed poop, a little diarrhea, almost no fat floating in the
water.  Brown.

Later, had some diarrhea, then a couple of Monsters, a Diet Coke, and
200 grams of *tapa de nalga* beef with some salt.  Dinner was *bife de
chorizo* with spices and salt.

At night (0:30) I had some relatively aggressive diarrhea, which is
the first time since I started this crazy diet.  This suggests that if
there's an irritant causing this diarrhea, it's probably either the
soft drinks or meat or eggs themselves.

02023-04-15
-----------

Got up pretty late, like 13:00 (but maybe that's only 10 hours of
sleep).  Ate *bife de chorizo* and ground beef and fried eggs which
Mina made for me.  Drank yerba mate.  Took a nap.  Had more diarrhea.
Had another Monster.
