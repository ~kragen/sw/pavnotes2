I conjecture that we can solve the hinting problem in font rendering
with mixed integer linear programming (MILP) to minimize a
perceptually-weighted linear error metric by choosing a color for each
pixel, thus avoiding the need to hint each letterform manually.

Consider the Fourier or Gabor-wavelet transform of a continuous raster
scan line running horizontally through a line of infinite-resolution
text.  We could consider the “hinting problem” to be the problem of
approximating that raster scan line with a raster sampled at a
discrete set of pixels.

The naïve approach is to simply sample the infinite-resolution
text at those pixels, then try to return the sampled signal to the
continuous domain by convolving it with a linear-interpolation filter
or a boxcar filter.  With this approach,
we will get aliasing, in which frequencies above
the Nyquist frequency are converted into frequencies below it.  This
can impede the readability of the text.

“Hinting” is the name for strategies that try to avoid this problem.
Often we can improve the approximation by making some pixels the
“wrong” color, that is, not the same color that the
infinite-resolution text would be where they are.

(It’s purely a matter of conjecture on my part that the visual
disagreeability of “poorly hinted” letterforms is well explained by
Fourier-domain or Gabor-domain features.  We have some reason for
believing that Gabor wavelets correspond to some of the early levels
of neurological processing of vision, though, and visually coherent
scenes tend to be relatively sparse in these domains.  But aesthetics
and subjective experience are rarely easy to reduce to mathematical
terms, and my reduction to one dimension here is especially
questionable.)

The Fourier-domain or Gabor-domain signal is a linear function of the
pixel values.

An interesting feature of this problem, which I don’t think anyone
else has noticed, is that minimizing the L1-norm difference in the
Fourier or Gabor domain, subject to the constraint of each pixel being
within a finite range, is a bog-standard linear optimization problem
of the sort that the simplex method is good for.  Moreover, that’s
still true even if we apply a perceptual weighting function to give
greater importance to the spatial frequencies that are most
perceptually salient.

I believe that second-order cone optimization would allow you to
instead minimize the L2-norm difference, which might be perceptually
better than L1 minimization, and I understand that there are quite
good conic quadratic optimizers out there, but I don’t really know
anything about them.

However, given that conventional font hinting algorithms work one
letter at a time, even dumber optimizers might work for this problem.
If you’re solving a 10-pixel chunk of a raster, and you’re choosing
between black and white for each pixel, you can simply enumerate all
1024 possibilities and compute the L1 distance or L2 distance or
whatever you want.  Or you can branch and bound, maybe starting with a
greedy choice of pixels so that you can establish a good bound early
on.

You could do this computation of optimal rasterization for individual
letters at a given resolution and subpixel offset, for two- or
three-letter groups, or for an entire line at a time; orthogonally,
you could either precompute the optimization, compute it on demand and
discard it, or compute it on demand and cache it.

Separately, it may be worthwhile to precompute a sparse approximation
to the Fourier-domain or Gabor-domain representation of each
letterform.  This is resolution-independent and offset-independent (or
rather can be easily adjusted to any resolution and offset).  But it
doesn’t save you computation, I think, because you still have to
compute the distance across all the frequencies; it isn’t okay if your
rasterized letterforms have a large amplitude at some perceptually
salient frequency that isn’t present in the letterform you’re trying
to approximate.  It only saves you storage space.
