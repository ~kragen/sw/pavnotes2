What are the cheapest aggressive nonpolar solvents in Argentina?  This
is complicated by many innocuous solvents like acetone and MEK being
controlled as “drug precursors” — I guess that’s what happens if
ignorant luddites get control of the government.

My goal here is to solvent-weld styrofoam slabs together; see file
`sandwich-material-pricing.md`.

Ethanol is pretty cheap (like US$1/ℓ) and available in every pharmacy
and most grocery stores, but it doesn’t dissolve styrofoam.  I can
probably use it to dilute whatever nonpolar solvent *does* dissolve
styrofoam down to a level where it just barely works.

“Thinner” or “diluyente” [is a poorly specified mixture of
hydrocarbons and oxygenated organics][psmh], supposed to be mostly
toluene and methanol, and costs about US$4/ℓ.  I’d think this would
have the best chance of dissolving styrofoam of anything on the list
except ethyl acetate, which I’ve tried.

[psmh]: https://www.easy.com.ar/thinner-oro-1lt-1248948/p

“Aguarrás” is supposed to be turpentine, but “aguarrás mineral” is the
mineral spirits you use when you can’t afford turpentine, and that’s
what you universally get as “aguarrás” in the hardware store.  It’s
also a poorly specified mixture of hydrocarbons (this time aliphatic)
and costs about [US$1.10/ℓ][flrs] to [US$2.50/ℓ][agrs].  Mina tells me
it’s more obnoxious to work with than “thinner”.

[flrs]: https://articulo.mercadolibre.com.ar/MLA-686935637-aguarras-diluyente-pintura-fleminrras-x-4lts-pintumm-_JM
[agrs]: https://www.easy.com.ar/sinterras-4lt-1248947/p

“Trementina” is turpentine, aka “aguarrás vegetal”, and it’s sold by
art supply stores as an oil-painting accessory for [US$17/ℓ][trmt] or
more.  Mina tells me it’s less obnoxious to work with than “thinner”.
It does at least soften polystyrene and make it gummy, because “barniz
damar” does that, but I suspect it probably is one of the best things
on this list at dissolving polystyrene.

[trmt]: https://articulo.mercadolibre.com.ar/MLA-910297334-aguarras-vegetal-esencia-de-trementina-alba-500ml-_JM

Lemon oil is sold as [*terpenos de naranja*][tdnj] for aromatherapy
for about US$11/ℓ and is probably adulterated.  [3M Citrus
Cleaner][3mcc] is probably the real deal but only comes in spray cans
for US$34/ℓ.  Lemon oil is also sold as [“limpia diapasón” or “limpia
trastera” for guitars and basses][logi] for about US$60–80/ℓ and is
probably not adulterated.

[tdnj]: https://articulo.mercadolibre.com.ar/MLA-1203718335-terpenos-de-naranja-1-l-_JM
[3mcc]: https://articulo.mercadolibre.com.ar/MLA-917549666-3m-citrus-cleaner-removedor-limpiador-de-adhesivo-300-ml-_JM
[logi]: https://articulo.mercadolibre.com.ar/MLA-935849009-aceite-de-limon-limpia-diapason-para-guitarra-y-bajo-_JM

Ti tree oil is also more or less turpentine and is sold for a variety
of alternative-medicine uses: fungicide.  This is probably not
adulterated because it goes through the usual pharmaceutical supply
chain but it costs [US$160/ℓ][tto].

[tto]: https://articulo.mercadolibre.com.ar/MLA-778189280-aceite-esencial-arbol-te-tea-tree-50ml-oferta-_JM

Nail polish remover is now mostly ethyl acetate; its great advantage
over most of the above is that it’s nontoxic, so it’s pretty safe to
work with as long as it doesn’t catch on fire.  [You can buy it by for
about US$6/ℓ][nprm].  For some purposes the extra crap they put in it,
like keratin (!), would be a problem, but for gluing styrofoam
together it should be fine.  And it’s probably less annoying to work
with than “thinner” or turpentine.

[nprm]: https://articulo.mercadolibre.com.ar/MLA-870997993-quitaesmalte-profesional-litro-las-varano-sin-acetona-lefeme-_JM

Different sources disagree on whether gasoline works, but I don’t want
to have a bottle of gasoline sitting around the house.  Others say
kerosene and/or diesel fuel will work, and those would be more
innocent.  Gasoline (C₄–C₁₂ hydrocarbons) is about 50¢/ℓ, diesel fuel
(C₉–C₂₅) is about 70¢/ℓ, and kerosene (C₆–C₂₀) is about 90¢/ℓ.  Diesel
fuel might have too low a vapor pressure to evaporate in a reasonable
amount of time, but it might be gentler; people report that it does
soften styrofoam but rather slowly.

“Contact cement” uses acetone and toluene as solvents, and it would
probably be pretty easy to distill them out of it, but it’s kind of
pricey as a source of them.

[Amanda Turk and Rachel Yuengert did a science fair project on this at
La Reina High School in California in 02006][atry]:

> We selected 12 solvents … which should… dissolve polystyrene. …All
> but three solvents caused the EPS to progress along the same
> continuum of physical change during the dissolution process,
> although at varying rates. The results regarding percent mass change
> showed that the EPS absorbed large amounts of solvent over the
> course of dissolution. Xylene (the quickest), lemon oil, turpentine,
> and camphor oil completely dissolved all grades of EPS. The
> exceptions to the above patterns were water (the control, which had
> no effect on EPS), acetone, and diluted acetone.

Unfortunately they didn’t bother to list the other five to eight
solvents!  Xylene is also, if I recall correctly, tightly controlled
as a “drug precursor” here and as an air pollutant in California, so
it is no longer possible to do such science-fair projects there.

[atry]: http://csef.usc.edu/History/2006/Projects/S0815.pdf

Noting that dammar varnish soaks into the styrofoam without damaging
it, despite being mostly turpentine (see file
`material-observations-02022.md`, 02022-11-11), it occurs to me that a
possible alternative to diluting an aggressive solvent with a
different solvent is to dilute it with solutes, such as polystyrene.
