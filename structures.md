I’m thinking about a book called something like “Structures of
Computation” or “Algorithmic Structures”, explaining computation from
the bottom to the top, covering several different paradigms.  The
particular thing that most interests me is the variety of paradigms
for programming languages, and the tiny minimal calculi expressing
them, like the λ-calculus underlying most languages, the ς-calculus
that underlies Bicicleta, the term-rewriting system underlying
Qfitzah, μKanren, the π-calculus for communicating sequential
processes, etc.

I’d like to have graphical notation for each of these.

Allied interests are “higher-level-than-functional” paradigms:

- SQL and Datalog for database queries
- Large language models for code generation
- Hypothesis for testing (stochastic model checking)
- miniKanren for relational programming
- Alloy for bounded model checking
- PVS or ACL2 or similar for proving code correct
- SAT and SMT solvers
- Superoptimizers and superoptimizer database search

These are mostly all of an algebraic, logical, discrete flavor.
There’s another, fuzzier, analytic direction to go in, as well, with
things like gradient descent and Runge–Kutta integration, which enable
such machines both to handle uncertainty and randomness brought in
from the real world, and to make faster progress on problems of the
previous type, for example by doing branch-and-bound on conservative
analytic approximations.  Perhaps large language models really belong
under this rubric.

Another allied interest is “lower-level-than-assembly” paradigms:

- Turing machines, of course, but also
- Asynchronous NAND gates,
- Synchronous LUTs and registers (Moore machines, as opposed to Mealy
  machines),
- Reversible computation, and
- Quantum computation.

Connecting these levels requires translation and search.

But most of that is just a question of reviewing and reorganizing
existing material.  An actually novel thing I want to achieve is
establishing a substrate for deterministic, reproducible computations,
so that I can write down an arbitrary Turing-complete computation now
and have some confidence that it will be executable with the same
results in the future.
