There are very few Indian people in Argentina.  An Indian-Argentine
friend of mine made a film about this a few years ago.

Argentine culture is maybe uniquely repulsive to Hindu culture.
Consider three points:

1. In India, violence against cows is viewed so negatively that the
Gau Rakshaks lynch truck drivers who are found to be trafficking in
cow meat, lynchings tacitly supported by local police forces and which
were one of the major factors leading to the Partition 79 years ago,
one of the great humanitarian tragedies of history.  In Argentina, by
contrast, cow meat is the foundation of the national cuisine.

2. In India, romantic movies are enormously popular, but never show
kissing, because that would run afoul of obscenity laws.  In 02007,
Richard Gere was on stage with famous Indian actress Shilpa Shetty at
a fundraiser for HIV in New Delhi, and he kissed her several times,
overwhelming her evident resistance with bodily force; both of them
were prosecuted for public obscenity, Shetty finally defeating the
charges after a 15-year court battle.  In Argentina, not only do good
friends of both genders routinely kiss each other in greeting, but
it’s common to kiss someone you’re just being introduced to in a
social context if they’re of the opposite gender.

3. Hindu culture regards utensils or fingers that have been in contact
with the inside of someone else’s mouth, or food or plates that have
come in contact with such utensils, as “uchchhishta”, unclean.  “He is
a dog who eats uchchhishta.”  Hindus can get very offended if you
offer them uchchhishta food.  In Argentina, the most important social
recreational drug ritual — more important than drinking coffee
together — is drinking yerba mate, in which every person in succession
in the mate circle drinks from the same cup through the same metal
straw.

It’s almost like someone purposely designed Argentine culture to repel
Hindus!

I have had good Indian food here in Buenos Aires, though.  I still
haven’t found good Mexican food.
