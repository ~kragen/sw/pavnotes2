STMs, scanning tunneling microscopes, depend on an electrical
tunneling current between the probe tip and the sample being scanned.
This means you can’t scan things without an electrically conductive
surface, which would seem to exclude both nonmetallic things like
butterfly wings, bacteria, and pollen, and, in air, metallic things
that oxidize in air, which basically means all the common metals:
steel, copper, tungsten, and so on.

So in practice you need hard vacuum, which is considerably more
demanding than the rest of the STM.

The first restriction is not actually so bad because you can
vacuum-coat samples in gold under medium vacuum, but the second one is
pretty bad.

The AFM, or atomic force microscope, is an alternative that literally
just measures the mechanical force on the probe tip.  As I understand
it, this is universally done by mechanically deflecting a tiny mirror,
and measuring the mirror deflection optically.

It occurred to me that if the probe tip is vibrating, it will
propagate vibrations into the sample when it is touching, but if it is
out of contact, it will not, or rather it only will through the air
and the microscope frame.  If the vibration displacement is
significant compared to the distance of the atomic force interaction
(≈200 pm) harmonics will be generated, which will be much weaker when
the probe is fully out of contact or fully in contact.

Higher-frequency vibrations will be able to transmit more vibrational
energy with a smaller displacement, so it’s desirable to use as high a
frequency as possible.  10MHz should be straightforward with common
materials like PZT and tungsten; 100MHz should be achievable.

Transmitting more vibrational energy is important because the larger
the transmitted energy the larger will be the signal to detect.

Tungsten tips can transmit more energy at a given displacement both
because they are dense and because they are stiff.  But for maximum
energy transfer, you may need to match the acoustic impedance of the
sample as closely as possible.  Delivering the acoustic energy to the
tip will require attention to the signal path; conventional STM tips
are designed to maximize rigidity, which conflicts with the objective
of being a smooth acoustic transmission line.
