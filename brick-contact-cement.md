In file `baking-soda-cement.md` I wrote about the possibilities of
using baking soda to accelerate the setting of bulk concrete, for
example for 3-D printing in concrete foam.  But today it occurred to
me that there are perhaps even more interesting possibilities with
such an adhesion reaction for assembly of pre-shaped building blocks.

Basically the idea is that you put a glob of goo on a building block,
then press another building block against it in such a way as to
squash the glob of goo thin.  If one or both of the building blocks
are porous, this will expose the goo to much more rapid diffusion of
whatever is inside them — air, for example, or baking soda.  You can
design the system so that this causes the goo to set hard within a
second or so; I think 100μm is a reasonable adhesive-layer thickness
to shoot for.

This is pretty similar to what happens with common adhesives such as
cyanoacrylate, acetic-cure silicone, or wood glue on porous surfaces.
But there are some other interesting systems that might be made to
work this way.

- Portland cement and baking soda: normally you cannot use portland
  cement in a thin layer because it will dry out and then not harden
  properly.  Perhaps, especially if the building blocks are
  waterlogged, baking soda can accelerate the cure enough that you can
  use a layer of tens or hundreds of microns of portland cement
  between adjacent building blocks.  Such an assembly of building
  blocks could be both stronger and cheaper than conventional
  concrete, if shaping the building blocks is not expensive, because
  such a small fraction of its volume is made of cement, and the
  blocks can be made in interlocking shapes so that the cement is not
  loaded in tension.
  
- Waterglass and baking soda: baking soda should drop the pH of
  waterglass with the same reaction with which it makes portland
  cement set, which will also gel the waterglass.  I’m more certain
  about this one.
  
- Waterglass and other acidic buffers: it occurred to me that a
  relatively small amount of citrate, boric acid, or mono- or
  diammonium phosphate should also have the same effect.  However, I’m
  not sure that this is economical, because these materials are more
  expensive than waterglass.

- Lime cement and baking soda.  As I said in the other file, this is
  Ca(OH)₂ + NaHCO₃ → NaOH + CaCO₃ + H₂O.  This certainly produces a
  solid, but whether it produces a good bond, I don’t know.
  
- Calcium chloride and washing soda.  (Baking soda would outgas.)
  Calcium chloride makes a lovely goo, and washing soda precipitates
  it as CaCO₃.  Again, whether this can produce a good bond, I don’t
  know.

- Maybe some kind of phosphate-cement system, using a viscous aqueous
  solution of an acidic phosphate as the goo?  A pretty wide variety
  of metal oxides, hydroxides, and even silicates such as calcium
  silicate can form phosphate cements.

If the building blocks fit together with a kinematic coupling with
high enough pressure to displace the cement from the kinematic
coupling’s contact points — something much more plausible with
waterglass than with the other adhesives — they will be precisely
positioned with respect to one another as the adhesive hardens.  There
will be some error in position if the adhesive expands or contracts as
it sets, but it can be quite small, because the building block can
practically be thousands of times the thickness of the adhesive layer.
Once the adhesive is set, the relative positioning should be many
orders of magnitude more rigid than the kinematic coupling itself.

It is possible to shape the mating surfaces of the building blocks to
include leaf-vein-like channels for goo distribution which run out of
space once the blocks are closely mated, forcing the goo to spread
over the majority of the surface in a very short time.

Suitable building-block materials might include terracotta, sandstone,
limestone, concrete, cinder block, scoria, or wood.
