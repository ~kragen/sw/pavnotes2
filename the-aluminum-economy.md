Aluminum is a compact, safely transportable fuel of good energy
density, which can be burned inexpensively in high-efficiency,
very-low-cost [fuel cells][1] (>50%, compared to 25%–40% for ICEs;
more sophisticated aluminum fuel cells have much higher efficiency, I
think closer to 90%).  These fuel cells are already in niche use, and
the aluminum is already produced in massive amounts from renewable
electrical energy.  So we could imagine an “aluminum economy” in which
energy is stored in aluminum for later use, either stationary or
mobile.

[1]: https://en.wikipedia.org/wiki/Aluminum-air_battery

Aluminum provides [31MJ/kg][0] of energy, which is lower than gasoline
(46.4MJ/kg), diesel (45.6), jet fuel (43), or crude oil (about 42),
and in the same range as coal (24-35).  But the higher efficiency
means that it exceeds those fuels in density of *usable* energy,
unless you can burn them in a fuel cell.  And it’s more compact;
aluminum’s 2.7g/cc significantly exceeds [graphite’s][2] 1.9-2.3 and
[anthracite’s 1.3-1.4][3], and the other fuels are close to 1.

[0]: https://en.wikipedia.org/wiki/Energy_density#In_chemical_reactions_(oxidation)
[2]: https://en.wikipedia.org/wiki/Graphite
[3]: https://en.wikipedia.org/wiki/Anthracite

Aluminum has the disadvantage that its oxidation product is solid
rather than gaseous like hydrogen’s, so an aluminum-fueled aircraft
would be at a serious disadvantage: though perhaps it could carry less
fuel weight for a given energy content, it would get heavier instead
of lighter as it flew!  (Presumably it is unacceptable to drop spent
batteries, or even aluminum oxide, into the ocean or farmland.)

However, this assumes that you can effectively separate the oxide or
hydroxide product from the rest of the electrochemical cell in order
to replace the aluminum; current cells consist mostly of non-aluminum
components by weight.

Worth linking is [Alumapower’s US patent 10,978,758][11] by Geoffrey
Sheerin, in which the anode is a rapidly rotating aluminum disc riding
on a liquid “bearing” of electrolyte.

[11]: https://patents.google.com/patent/US10978758B2/en

The specific energy comparison is more favorable when competing
against other electrical batteries: the achieved energy density of
4.7MJ/kg greatly exceeds the [1.0 or so of lithium-ion][7], the [0.4
of alkaline primary batteries][8], and even the 1.6MJ/kg of [zinc-air
batteries][9].

[7]: https://en.wikipedia.org/wiki/Energy_density#Other_release_mechanisms
[8]: https://en.wikipedia.org/wiki/Energy_density#In_batteries
[9]: https://en.wikipedia.org/wiki/Zinc%E2%80%93air_battery

Unfortunately the [aluminum smelting cells][4] used to produce
aluminum are not suitable for use as [liquid-metal batteries][5] like
[Ambri’s][6]; the oxygen produced is consumed by oxidizing the
graphite cathode, a reaction which is heavily favored by the high
temperature of 940-980°.  No significant oxygen can dissolve in the
electrolyte at these temperatures to make its way to the molten
aluminum.

[4]: https://en.wikipedia.org/wiki/Hall%E2%80%93H%C3%A9roult_process
[5]: https://en.wikipedia.org/wiki/Liquid_metal_battery
[6]: https://en.wikipedia.org/wiki/Ambri_(company)

Today, recyclers typically buy dirty aluminum at retail for about
US$1/kg, which would work out to 15MJ/$ if burned in fuel cells at 50%
efficiency, or 6.7¢/MJ or 24¢/kWh, which is a fairly high price for
energy but not ridiculously so.  A barrel of crude oil is
conventionally supposed to yield 5.8MBTU (6.1GJ) and costs US$83
today; that’s 1.4¢/MJ or 4.9¢/kWh.  Even supposing you’re burning it
in a 30%-efficient ICE-powered generator, that’s only 4.5¢/MJ
electric.  At times oil prices have been high enough that 6.7¢/MJ beer
cans would be an attractive alternative, and they will surely trend
higher over time, while the abundance of cheap photovoltaic energy
drops the price of aluminum.  But it isn’t a killer opportunity to buy
cheap energy today.

Aluminum cells have the appealing property that they can scale down to
microscopic size without becoming inefficient, which is something ICEs
and other heat engines cannot manage.  In theory they can start or
stop with submicrosecond speeds even at much larger sizes, but
Alumapower’s patent explains that, in practice, interrupting the
current produces hydrogen bubbles at the anode which dramatically
reduce the cell’s current capacity.

An example system: an electric bicycle
--------------------------------------

If [a bicycle requires 300 watts on average][10], which is apparently
what pro riders manage on the Tour de France, it would spend 65mg/s of
4.6MJ/kg aluminum-air cells.  This works out to a 230-gram battery
pack per hour, about the mass of four 65-gram alkaline C cells each
hour (but enormously higher power than such cells could provide).
This seems like a perfectly reasonable amount of battery to burden a
bike with.

[10]: https://www.alpecincycling.com/en/pro-peloton/from-body-fat-to-power-output-anatomy-of-a-tour-de-france-rider/

To get the battery to 48 V you would want to put 40 1.2V cells in
series; for peaks of 600W you would then require 12½ amps, an order of
magnitude higher than a C cell.  It might be more practical to
supplement the aluminum-air batteries’ output power with a Li-ion
battery to shave the peaks off the bicycle’s electrical demand: a
“10C” Li-ion battery of 60 watt-hours would work.  For example, a
44-volt 12-cell series battery of 1400mAh 18650 cells rated for “10C”
discharge would work.  (18650s are commonly 2500mAh.)  This would
allow peaks of several minutes of regenerative braking (which the
aluminum-air batteries cannot manage) or 600-watt output peaks,
reducing the demand on the aluminum-air cells to the 300-watt average,
only 6¼ amps.
