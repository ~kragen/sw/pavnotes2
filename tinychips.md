I'm reading Weste and Money Harris's textbook on CMOS VLSI design.
They mention that at the time (02010) MOSIS subsidized a "TinyChip"
size of 1.5mm square, and the best process to use was an 0.3μm
process, in which λ = 150nm, so that's 5000λ × 5000λ, 25 million
square lambdas.  And they say (table 1.10, p. 52, §1.10.6) to estimate
the area of a random-logic transistor as about 1000-1500λ², a datapath
transistor as 250-750λ², SRAM as 1000λ²/bit, DRAM in a DRAM process as
100λ²/bit, and ROM as 100λ²/bit.  Mostly this is track widths and
spacing (minimally 3λ each, but the book recommends 4λ each); the
transistor diffusion layers are less limiting in size.

More promising now since 02020 is the [Skywater PDK][0] for their
130nm SCMOS ([scalable CMOS][14]) process from last millennium (with
actually 150nm being the minimum length transistor channel, and
supporting bipolar devices too).  Scaling rules start to break down at
thhis scale.  For this Tim Edwards recommends xschem, ngspice, gaw
(waveform viewing), openlane, magic from opencircuitdesign, netgen,
iverilog, qflow, IRSIM, xcircuit (!!), xyce, etc.  They do offer a
`sky130_fd_pr` library for "primitive devices/analog", and [the
Caravel test harness layout supports analog projects][5].  [Some PDK
documentation is available][1] in the Archive.

[0]: https://isn.ucsd.edu/courses/beng207/lectures/Tim_Edwards_2021_slides.pdf
[1]: http://web.archive.org/web/20220628150541/https://antmicro-skywater-pdk-docs.readthedocs.io/en/latest/index.html
[5]: https://github.com/efabless/caravel_user_project_analog
[14]: https://www.fceia.unr.edu.ar/eca1/files/LDCI/Reglas_%20de_Diseno_MOSIS.pdf

They have 5 layers of aluminum metal of decreasing thickness, a
titanium nitride local interconnect layer, and then polysilicon,
p-diffusion, n-diffusion, p-tap, n-tap, p-well, n-well, and deep
n-well layers.  Also you can make so-called "MiM" capacitors with
extra metal layers in between the metal layers.  Finally, on top of
the thickest aluminum is a redistribution copper layer.

Their reference layout RF 0.18V pFET is about 3.0μm square with a
channel length of 0.15μm due to having four fingers of gate.  Magic
has the option to generate devices from the Skywater PDK and do DRCs,
design rule checks.

Their [standard cell libraries][7] include gates ranging from NAND through
AOI up to adders.  They say:

> :lib:`sky130_fd_sc_hdll` - High Density, Low Leakage Standard Cell Library ...
> 
> Raw gate density (number of :cell:`sky130_fd_sc_hdll__nand2_1` gates
> able to fit in 1mm2) for :lib:`sky130_fd_sc_hd` is 266kGates/mm2 and
> 200kGates/mm2 for :lib:`sky130_fd_sc_hdll`. ...
>
> -  Routed Gate Density is 120 kGates/mm^2

[7]: https://foss-eda-tools.googlesource.com/skywater-pdk/+/refs/heads/fix-resistance-values/docs/contents/libraries/foundry-provided.rst

Their primitive parts include [things like NFETs][2], including up to
20V devices and ESD-protection devices, and SONOS flash memory cells
with 100k erases!

[2]: https://skywater-pdk.readthedocs.io/en/main/rules/device-details.html#v-nmos-fet

The user area in the [standard efabless Caravel harness][3] is 2.92mm
x 3.52mm with 38 I/O pads and 4 power pads.  This works out I guess to
about 24 million λ².  There's also a PicoRV32 on the side hooked up to
128 logic analyzer probes so you can debug your project.  Caravel uses
efabless's [OpenLane][6], "an automated RTL to GDSII flow based on
several components including OpenROAD, Yosys, Magic, Netgen, CVC,
SPEF-Extractor, CU-GR, [Klayout][17] and a number of custom scripts
for design exploration and optimization."

[3]: https://github.com/efabless/caravel
[6]: https://github.com/efabless/OpenLane
[17]: https://www.klayout.de/

A carefully done design in 130nm ought to be able to be clocked at
2GHz or more, but their standard cells have transition times of I
think 1.5ns.

Calculus Vaporis is about 1000 2-input NAND gates in 12-bit parallel
form, which would be about 4000 transistors.  If this were random
logic density per Weste and Money Harris, that would be 4-6 million
λ², which would roughly fit into the year-02010 TinyChip size, and 4-6
of them would fit into the Caravel space.

But if we take Skywater's word on their "Routed Gate Density" number,
those 10.3mm² work out to 1.23 million gates, 5 million transistors,
and 1200 instances of Calculus Vaporis.  This is off by a factor of
200 from my λ²-based estimate.

In SKY130 [the single-port 6T SRAM cells from OpenRAM are 1.2μm ×
1.58μm][9], so this same area would hold 5.4 million bits of
single-ported SRAM.  If 4 KiB of RAM is enough to make a computer
useful, why, that's 32768 bits, enough for 165 computers.  An instance
of Calculus Vaporis next to each such RAM chunk would reduce the
computer count to 145.  Like a GreenArrays GA144, without the Forth.

[9]: http://ef.content.s3.amazonaws.com/OpenRAM_%20FOSSI%20Dial-Up%202020.pdf

I think maybe the Tiny Tapeout thing is the one that was confusing me,
where you have digital muxes (?) between your project and the chip
pads and therefore you can't do analog parts (?).  The idea is to put
many projects on one chip, sharing a single supervising PicoRV32, to
get the price down to US$100!

My intuition so far has been that SKY130 is useless for digital work
because an FPGA in a current process node will be better in almost
every way than a SKY130 ASIC: faster, lower power, smaller, and
cheaper, plus all the ways that FPGAs are generally better than ASICs.
But, for analog work, it should be totally competitive, because going
to smaller process nodes really hurts analog performance.  With the
new information I got above, is this true?

One of the projects they've fabbed so far is an [OpenFPGA][10] with 64
CLBs, [each containing two 4-LUTs and a DFF][11].  So this is 128
4-LUTs, a tenth of Lattice's smallest FPGA, and maybe roughly
equivalent to 512 gates.

[10]: https://github.com/Manarabdelaty/Caravel-OpenFPGA-EF
[11]: https://openfpga.readthedocs.io/en/master/tutorials/arch_modeling/quick_start/#adapt-vpr-architecture

This is in the 10.3mm² of the Caravel "user area", which we estimated
above would be 1.23 million gates as an ASIC.  So the FPGA comes with
a surprising 2300× area penalty, about a 50× linear size penalty.  So,
if commercial FPGAs have a similar penalty, to get to the same areal
density as a SKY130 ASIC, the FPGA needs to be fabbed in a 3nm node,
which is indeed a thing Samsung and TSMC are offering.  But I don't
think Xilinx or Altera is using such a recent process node yet.

I'm going to crudely guess that the same is true of power consumption,
since at 130nm gate thicknesses had already approached the minimum, so
both leakage power and gate charge (and thus V²f losses) should be
roughly proportional to area.  So maybe current FPGAs will still use
more power than a SKY130 ASIC.

Also I think it's probably possible to beat the FPGA on speed by a
bit; [the Pentium 4 in 130nm][12] using [Intel's 0.13μm process
debuted in 02000][13] supposedly reached 2.2 GHz, and [an academic
chip hit 5 GHz in 130nm][15], while FPGA circuits basically never beat
500 MHz AFAIK.  However, Intel's 130nm process had a 60nm gate length
(and 1.5nm gate oxide, so about 150 atoms), while, if I understand
correctly, SKY130 has 150nm, so it's probably much slower.

[12]: https://www.7-cpu.com/cpu/P4-130.html
[13]: https://www.intel.com/pressroom/archive/releases/2000/cn110700.htm
[15]: http://www.ece.virginia.edu/~mrs8n/conf/01046084.pdf "5-GHz 32-bit Integer Execution Core in 130-nm Dual-Vt CMOS, by Vangal et al., 02002, doi 10.1109/JSSC.2002.803944"
