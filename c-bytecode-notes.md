Here are a few random C subroutines chosen from code bases that I
think are likely to be compilable for my dumb JIT on the Zorzpad.  The
objective here is to see what kind of performance impact is likely
with the bytecode JIT-compilation strategy I have in mind.

So far it looks like the slowdown factor compared to good native
Thumb-2 code should be about 5 (3.5, 4, 5.3, 7), but the bytecode
should be about 4 bytes per line of C (3.7, 4.0, 4.3), 60% the size of
the Thumb-2 code, or 50% the size of amd64 code (38%, 50%, 72%).
About 20% of the bytecode instructions executed have to access linear
memory, which is the part that requires bounds-checking and possible
page faults.

These numbers don’t count the extra cost of trampolining subroutine
calls and returns as Ben suggested; this is necessary to be able to
evict individual subroutines.  (See file `hadix-compiler.md` for
details.)

Doing even the dumbest register allocation in the JIT instead of using
a runtime stack seems like it would speed up the code by about a
third, as well as fitting a lot more into whatever size of cache it
uses for the compiled code, but it might also slow the JIT down a lot.

The example code fragments
--------------------------

From the Jove editor, a tiny Emacs clone:

    /* Fix marks after a deletion. */

    void
    DFixMarks(line1, char1, line2, char2)
    register LinePtr        line1,
                            line2;
    int     char1,
            char2;
    {
            register Mark   *m;
            LinePtr lp;

            if (curbuf->b_marks == NULL)
                    return;

            for (lp = line1; lp != line2->l_next; lp = lp->l_next) {
                    for (m = curbuf->b_marks; m != NULL; m = m->m_next) {
                            if (m->m_line == lp
                            && (lp != line1 || m->m_char > char1))
                            {
                                    if (lp == line2 && m->m_char > char2) {
                                            m->m_char -= char2-char1;
                                    } else {
                                            m->m_char = char1;
                                            if (line1 != line2)
                                                    m->m_big_delete = YES;
                                    }
                                    m->m_line = line1;
                            }
                    }
            }
    }

From pcc:

    /*
     * XXX workaround routines for block level cleansing in gcc compat mode.
     * Temporary should be re reserved for this value before.
     */
    static P1ND *
    p1mcopy(P1ND *p)
    {
            P1ND *q;

            q = xmalloc(sizeof(P1ND));
            *q = *p;

            switch (coptype(q->n_op)) {
            case BITYPE:
                    q->n_right = p1mcopy(p->n_right);
                    /* FALLTHROUGH */
            case UTYPE: 
                    q->n_left = p1mcopy(p->n_left);
            }

            return(q);
    }

From libpng:

    /* Build a single 8-bit table: same as the 16-bit case but much simpler (and
     * typically much faster).  Note that libpng currently does no sBIT processing
     * (apparently contrary to the spec) so a 256-entry table is always generated.
     */
    static void
    png_build_8bit_table(png_structrp png_ptr, png_bytepp ptable,
        png_fixed_point gamma_val)
    {
       unsigned int i;
       png_bytep table = *ptable = (png_bytep)png_malloc(png_ptr, 256);

       if (png_gamma_significant(gamma_val) != 0)
          for (i=0; i<256; i++)
             table[i] = png_gamma_8bit_correct(i, gamma_val);

       else
          for (i=0; i<256; ++i)
             table[i] = (png_byte)(i & 0xff);
    }

From qoa:

    /* qoa_div() implements a rounding division, but avoids rounding to zero for 
    small numbers. E.g. 0.1 will be rounded to 1. Note that 0 itself still 
    returns as 0, which is handled in the qoa_quant_tab[].
    qoa_div() takes an index into the .16 fixed point qoa_reciprocal_tab as an
    argument, so it can do the division with a cheaper integer multiplication. */

    static inline int qoa_div(int v, int scalefactor) {
            int reciprocal = qoa_reciprocal_tab[scalefactor];
            int n = (v * reciprocal + (1 << 15)) >> 16;
            n = n + ((v > 0) - (v < 0)) - ((n > 0) - (n < 0)); /* round away from 0 */
            return n;
    }

Notes on Jove `DFixMarks`
--------------------------

Here’s a reformatted version of the subroutine:

    void DFixMarks(line1, char1, line2, char2)
    register LinePtr line1, line2;
    int char1, char2;
    {
        register Mark *m;
        LinePtr lp;

        if (curbuf->b_marks == NULL) return;

        for (lp = line1; lp != line2->l_next; lp = lp->l_next) {
            for (m = curbuf->b_marks; m != NULL; m = m->m_next) {
                if (m->m_line == lp && (lp != line1 || m->m_char > char1)) {
                    if (lp == line2 && m->m_char > char2) {
                        m->m_char -= char2 - char1;
                    } else {
                        m->m_char = char1;
                        if (line1 != line2) m->m_big_delete = YES;
                    }
                    m->m_line = line1;
                }
            }
        }
    }

21 lines of code, 6 local variables, 4 ifs, two loops, 8 linear memory
reads (i.e., to things other than local variables), and 4 linear
memory writes.

### Stack code ###

I think in an untyped stack bytecode with goto-based control flow and
without any real optimization this looks something like this:

    sub DFixMarks args=4 locals=6  -- line1, char1, line2, char2, m, lp
        global.17 field.22 bnz.1f  ret -- b_marks is ≈22nd in global curbuf
     1: my.0 setmy.5
     1: my.5 my.2 field.1 beq.8f  -- l_next is 2nd field of struct line
          my.5 field.1 setmy.5      -- follow pointer to next line
          global.17 field.22 setmy.4 -- loop initialization
     2:   my.4 bz.9f  my.4 field.3 setmy.4 -- next mark
            my.4 field.0 my.5 bne.4f  -- points at this line?
            my.5 my.0 beq.4f          -- not line1?
            my.4 field.1 my.1 ble.4f  -- > char1?
              my.5 my.2 bne.6f  my.4 field.1 ble.6f  -- line2, char2?
              my.4 field.1 my.3 my.1 alu.sub alu.sub my.4 setfield.1
            b.7f
     6:       my.1 my.4 setfield.1
              my.0 my.2 beq 7f  lit.1 my.4 setfield.2  -- YES is 1
     7:     my.0 my.4 setfield.0
     4:     b.2b
     9:   b.1b
     8: ret

This is not quite right, but hopefully close enough to get a rough
idea of costs.  It’s 62 bytecode instructions in all.  The inner loop
(from label 2 to label 4) consists of 44 bytecode instructions, though
on any given execution only some of them will be executed.  Of these,
5 are `field.` instructions which will read linear memory, and 4 are
`setfield.` instructions which will write it.  Together these memory
access instructions are 20% of the instructions in the inner loop.
The `global.` in question is a variable that never has its address
taken, so it does not need to be allocated in linear memory, and
similarly for all the locals.

Of the 62 bytecode instructions, probably we will need an argument
byte on all the jump/branch instructions (11) and 4 of the other
instructions (global.17 and field.22); all the other arguments can be
packed into the bytecode.  So the bytecode of the subroutine is 77
bytes, plus probably 4 or 8 bytes of header, say 84 bytes with
alignment, 4.0 bytes per line of C.

This subroutine is 117 bytes of amd64 code in the version I have
compiled here, so this is probably competitive but not outstanding.
If I pack it all onto one line it’s 486 bytes of C, 565 bytes in the
original code.  So 84 bytes is not bad: 72% of the amd64 number.

### Speed estimation ###

The 44 bytecode instructions of the inner loop will almost always bail
out at the first test, `m->m_line == lp`, because for any given line,
most marks will not be on that line.  So it will normally execute 10
of those instructions, of which 2 are linear memory access
instructions.

My current sketch of a linear memory access instruction millicode
subroutine for ARM, from 02023-07-29 in my paper notebook but
translated to English, is as follows.  Here r0 is a scratch register,
r4 (not mentioned) is an operand-stack pointer, r5 is a top-of-stack
register, r7 is a bitmask for the size of the linear memory, and r8
(which should maybe be r6) is a pointer to the base of the page table.

        and r5, r7                 @ limit index range
        bic r5, #3                 @ align for word access
        lsr r0, r5, #8             @ isolate page bits
        ldr r0, [r8, r0, lsl #10]  @ load page pointer
        cbz r0, tramp_fall_pag_53  @ handle page fault if NULL
        lsl r5, #24                @ isolate index into page
        ldr r5, [r0, r5, lsr #24]  @ load word from page

This is too optimistic because neither of the fancy indexing modes
assembles for Thumb-2, so I need to rethink my approach a bit.  It
turns out that Thumb-2 added an “unsigned bitfield extract”
instruction which gives us what we want in only 9 cycles on the
Cortex-M4 that is the Zorzpad’s target platform (6 instructions plus 2
memory references of which one, to the page table, is probably
zero-wait-state):

        and r5, r7               @ as before, limit index range
        ubfx r0, r5, #10, #22    @ unsigned bitfield extract of 22 bits
        ldr r0, [r8, r0, lsl #2] @ load page pointer
        cbz r0, tramp_fall_pag_54
        ubfx r5, r5, #2, #8      @ implicitly discard problematic low bits
        ldr r5, [r0, r5, lsl #2] @ load desired word

To this we can plausibly add a couple of cycles for millicode call and
return, or 8 cycles for call and return if Cortex-M4 indeed flushes
the pipeline in those cases.  This is 22 bytes so we probably don’t
want to inline it every time a program accesses linear memory.

The `field.0` instruction can invoke this millicode directly;
`field.3` must first add the field offset, costing an extra cycle:

        add r5, #12

With the dumb JIT compilation approach I have in mind, other bytecode
instructions like `my.4`, `bz.1f`, and `setmy.4` should normally take
about 4 clock cycles on average, because `my.0` is always 4 cycles,
assuming the operand stack is in zero-wait-state TCM:

        stmia r4!, {r5}
        ldr   r5, [sp]

and `lit.1` is always 3 cycles:

        stmia r4!, {r5}
        movs  r5, #1

and `bge.1f` is 6 cycles if the branch is predicted successfully, 9 if
it isn’t:

        ldmdb r4!, {r1, r3}
        cmp r3, r5
        mov r5, r1
        bge 1f

So the average should be about 4.  So the inner loop here should be
normally about 32 cycles of regular instructions plus 21 cycles of
linear memory access, for a total of 53 cycles.

### Native code comparison ###

I feel like this fulfills my desideratum that the virtual memory
mechanism should not impose crushing execution-time overhead on the
compiled program.  However, is that only because the dumb JIT
compilation approach itself imposes a crushing execution-time
overhead?

I compiled it to native code for my target platform with
`arm-linux-gnueabi-gcc-10 -mcpu=cortex-m4 -fomit-frame-pointer
-funsigned-char -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fdata-sections
-ffunction-sections -fmessage-length=0 -Os -c marks.c`.  The resulting
`.text.DFixMarks` section was 96 bytes according to `size -A`, so
we’re actually 12% smaller than Thumb-2, which is pretty good.  I
recompiled again with `-S -fverbose-asm` to see what it looks like.
GCC hoisted the `char2 - char1` subtraction out of the inner loop and
stuck it in `r8`.  It informs me that I counted the fields in `curbuf`
wrong; `b_marks` is actually at an 80-byte offset into it, not 84 as I
had miscounted.  Here’s the 19 instructions of the inner loop:

    .L106:
    @ marks.c:243:                  if (m->m_line == lp
            ldr     r6, [r4]        @ m_48->m_line, m_48->m_line
            cmp     r6, r5  @ m_48->m_line, lp
            bne     .L102           @,
    @ marks.c:244:                  && (lp != line1 || m->m_char > char1))
            cmp     r5, r0  @ lp, line1
            bne     .L103           @,
    @ marks.c:244:                  && (lp != line1 || m->m_char > char1))
            ldr     r6, [r4, #4]    @ m_48->m_char, m_48->m_char
            cmp     r6, r1  @ m_48->m_char, char1
            ble     .L102           @,
    .L103:
    @ marks.c:246:                          if (lp == line2 && m->m_char > char2) {
            cmp     r5, r2  @ lp, line2
            bne     .L104           @,
    @ marks.c:246:                          if (lp == line2 && m->m_char > char2) {
            ldr     r6, [r4, #4]    @ _5, m_48->m_char
    @ marks.c:246:                          if (lp == line2 && m->m_char > char2) {
            cmp     r6, r3  @ _5, char2
            ble     .L104           @,
    @ marks.c:247:                                  m->m_char -= char2-char1;
            sub     r6, r6, r8      @ tmp132, _5, tmp138
            str     r6, [r4, #4]    @ tmp132, m_48->m_char
    .L105:
    @ marks.c:253:                          m->m_line = line1;
            str     r0, [r4]        @ line1, m_48->m_line
    .L102:
    @ marks.c:242:          for (m = curbuf->b_marks; m != NULL; m = m->m_next) {
            ldr     r4, [r4, #12]   @ m, m_48->m_next
    @ marks.c:242:          for (m = curbuf->b_marks; m != NULL; m = m->m_next) {
            cmp     r4, #0  @ m
            bne     .L106   @

This will normally execute the first three instructions up to `bne
.L102` and then the last three from `.L102` to `bne .L106`.  Assuming
the two memory accesses there are in 1-wait-state memory and the
branches are predicted, that’s 10 cycles rather than 53.  So the
slowdown is about a factor of 5 over good native code, assuming GCC’s
code here is good (and I don’t see any obvious boneheaded code there).

Here’s the disassembly:

      24:   6826            ldr     r6, [r4, #0]
      26:   42ae            cmp     r6, r5
      28:   d10d            bne.n   46 <DFixMarks+0x46>
      2a:   4285            cmp     r5, r0
      2c:   d102            bne.n   34 <DFixMarks+0x34>
      2e:   6866            ldr     r6, [r4, #4]
      30:   428e            cmp     r6, r1
      32:   dd08            ble.n   46 <DFixMarks+0x46>
      34:   4295            cmp     r5, r2
      36:   d10b            bne.n   50 <DFixMarks+0x50>
      38:   6866            ldr     r6, [r4, #4]
      3a:   429e            cmp     r6, r3
      3c:   dd08            ble.n   50 <DFixMarks+0x50>
      3e:   eba6 0608       sub.w   r6, r6, r8
      42:   6066            str     r6, [r4, #4]
      44:   6020            str     r0, [r4, #0]
      46:   68e4            ldr     r4, [r4, #12]
      48:   2c00            cmp     r4, #0
      4a:   d1eb            bne.n   24 <DFixMarks+0x24>

Notes on pcc `p1mcopy`
-----------------------

This function in pcc’s parser deep-copies an AST node, which may be
unary or binary.

Reformatted in the same way:

    static P1ND *p1mcopy(P1ND * p)
    {
        P1ND *q;

        q = xmalloc(sizeof(P1ND));
        *q = *p;

        switch (coptype(q->n_op)) {
        case BITYPE:
            q->n_right = p1mcopy(p->n_right);
            /* FALLTHROUGH */
        case UTYPE:
            q->n_left = p1mcopy(p->n_left);
        }

        return (q);
    }

That’s 13 lines of C.  It should compile to something like this:

    sub p1mcopy args=1 locals=2  -- p, q
        lit.40 call.xmalloc setmy.1 -- my guess at P1ND size; it’s 56 on amd64
        my.0 my.1 lit.40 memcpy
        my.0 field.0 call.cdope lit.14 alu.bitand -- coptype is a macro
        dup lit.8 bne.1f
          my.0 field.7 call.p1mcopy my.1 setfield.7
     1: dup lit.4 bne.2f
          my.0 field.9 call.p1mcopy my.1 setfield.9
     2: drop my.1 ret

This works out to 31 bytecodes of which 4 calls need probably 2
operand bytes each, 2 large literals and 2 branch instructions need 1
operand byte each, and the rest of which don’t need any extra bytes:
43 bytes, probably 48 bytes including the header, 3.7 per line of C.
Compiled for amd64 it’s 128 bytes; the bytecode is 38% of the amd64
size.

### Speed estimation ###

In the normal case roughly all the instructions will run; we can guess
that this is 26 normal bytecodes (at 4 cycles) and 5 memory-access
bytecodes (at 10 cycles), so about 154 cycles, plus however long it
takes to memcpy 40 bytes, which is probably about 30 cycles, and the
function entry and exit, which is probably about 4 cycles, so this
subroutine is probably about 188 clock cycles.

I haven’t tried compiling this for the ARM, but the amd64 version is
33 instructions, of which some are SSE instructions like `movdqu` and
`movups` which are unavailable on the ARM, copying 16 bytes per
instruction; this is counterbalanced somewhat by the data size being
larger.  So we could maybe estimate that decent native ARM code would
take something like 40 instructions plus 5 extra memory accesses,
about 50 cycles.  So here the penalty for my dumb JIT is closer to 3½
times than 5 times.

Notes on libpng `png_build_8bit_table`
----------------------------------------

    static void
    png_build_8bit_table(png_structrp png_ptr, png_bytepp ptable,
                         png_fixed_point gamma_val)
    {
        unsigned int i;
        png_bytep table = *ptable = (png_bytep) png_malloc(png_ptr, 256);

        if (png_gamma_significant(gamma_val) != 0)
            for (i = 0; i < 256; i++)
                table[i] = png_gamma_8bit_correct(i, gamma_val);

        else
            for (i = 0; i < 256; ++i)
                table[i] = (png_byte) (i & 0xff);
    }

That’s also 13 lines of C.

I think this is something like

    sub png_build_8bit_table args=3 locals=5 -- png_ptr, ptable, gamma_val, i, table
        my.0 lit.256 call.png_malloc  dup my.1 setfield.0  setmy.4
        my.2 call.png_gamma_significant beq.1f
        lit.256 for.3  -- 3 is index of local variable loop counter
     2:     my.3 my.2 call.png_gamma_8bit_correct
            my.3 my.4 alu.add storebyte
        next.2b b.3f
     1: lit.256 for.3
     4:     my.3 lit.255 alu.bitand  my.3 my.4 alu.add  storebyte
        next.4b
     3: ret

Here the `next` bytecodes increment the loop counter and return
control to the label given if it hasn’t reached the limit.  I’m not
sure exactly what the right bytecode design is there.

Anyway, this is 31 bytecodes of which 4 are large literals and 3 are
calls requiring 2 opcode bytes each, and 2 are branch opcodes and 2
are loop-ending opcodes requiring 1 opcode byte each.  This is 49
bytes and thus probably 56 bytes of bytecode in all, 4.3 per line of
C.  The amd64 version is 112 bytes, so the bytecode is 50% as much.

The inner loop here is either the calls to `png_gamma_8bit_correct` or
the bitand loop.  The latter is probably 8 bytecodes and about 40
cycles.  In amd64 it’s 4 instructions because the bitand gets
optimized out and the add is folded into a ModR/M byte, so it would
probably be about 6 ARM cycles, so the dumb JIT slowdown is about 7×.
The former is 7 bytecodes and 7 instructions on amd64, the additional
3 being the arguments and call to the other subroutine, but I’m not
sure exactly how to count that, but it’s probably closer to a 4×
slowdown.

How fast can the JIT be?
------------------------

As I said, my sketch of the implementation of `my.0` is

        stmia r4!, {r5}
        ldr   r5, [sp]

This is four bytes of output.

My thoughts on how you could efficiently compile this involves two
256-entry tables of pointers to code fragments to handle each of the
256 possible bytecodes: one for the word-aligned-output case and one
for the case where an odd number of halfwords have so far been output.

In the aligned case, the assembly code to append this to a buffer
pointed to by, say, r6, with the bytecode byte in r3, would be
something like

        movw r0, #0xc420  @ stmia r4!, {r5}
        movt r0, #0x9d00  @ ldr r5, [sp]
        and  r3, #0xf     @ turn my.5 into 5
        add  r0, r0, r3, lsl #8  # add offset: [sp, #20], say
        str  r0, [r6], #4

This is 6 cycles if r6 points into zero-wait-state memory.  Then you
need to dispatch on the next bytecode, which might look the same as in
a bytecode interpreter:

        ldr  r3, [r7], #1
        ldr  pc, [r8, r3, lsl #2]

This is probably 5 cycles, plus an unpredicted jump, so 14 cycles in
all.  In this case it took us 14 cycles to assemble a single bytecode
and 4 cycles to execute it.  I think my prototype interpreter needs 12
cycles.

However, the ARM isn’t guaranteed to implement unaligned 4-byte
writes, so we need to deal with the case where the code output is
currently unaligned.  We can dedicate, say, r1, to buffering the
partly-emitted instruction word; the handler for `my.whatever` in that
case would look something like this:

        movt r1, #0xc420  @ add the stmia to the buffer
        str  r1, [r6], #4
        movw r1, #0x9d00  @ clears the buffer
        and  r3, #0xf
        add  r1, r3
        ldr  r3, [r7], #1
        ldr  pc, [r9, r3, lsl #2]  @ unaligned table pointer in r9

This is still 7 instructions with two TCM memory accesses, one non-TCM
memory access, and an unpredicted jump, so it’s the same 14 cycles.
But note that we’re using a different table, because now we have to
call a handler for unaligned writing.  If we’d appended an odd number
of halfwords to the code, instead we’d cross back over to the other
side.

However, I’d prefer to write the compiler in a high-level language,
not so much for ease of writing as for ease of comprehension and
bootstrapping.  If our 5× slowdown estimate holds good (and it may be
overoptimistic), then instead of 14 clocks per bytecode it should
take 70.  Our Jove example of 62 bytecodes will be about 4300 cycles
to recompile, which is also about how long it will take to run if the
current buffer has 10 marks on 500 lines.  4300 cycles at 48 MHz is
about 90 μs.

To look at it another way, given 4 bytecode instructions per line
(slightly more than the 4 bytecode bytes per line measured above), our
hypothetical self-compiled compiler hypothetically needs 280 clock
cycles per line of C, and at 48 MHz that would be 192 kloc per second.
Recompiling all of Jove’s 31kloc (from bytecode) would take 160ms.
(Without the 5× slowdown it would compile close to a million lines of
code per second.  Or two million if we can run at 96MHz.)

However, all of Jove wouldn’t fit in the Apollo3’s TCM: only 124kB of
bytecode, sure, but this dumb JIT would surely bloat it up to 640kB or
so, so only 10% will fit in the TCM at a time (even if things like the
JIT compiler and the stacks weren’t sharing it).  640kB/160ms = 4
MB/s, so plausibly caching the compiled native code elsewhere (in
non-TCM RAM, in compressed RAM, or even in 133MB/s Flash) might be a
reasonable choice.

The 670 or so functions in Jove total 193kB of amd64 code; 10% of this
is 19.3kB, which is 399 functions, AllMarkReset and everything smaller
than that.  So it’s plausible that it would be reasonably usable.  If
denser code could get you 40kB, 20% of the amd64 code, that would be
80% of the subroutines in Jove.  Still, plausibly you’d want your main
text editor to be a lot more compact than that.

31kloc in 670 functions is a mean of 46 lines of code per function,
but of course the distribution is notably skewed, with some functions
an order of magnitude larger.  Out of the 193kB of amd64 code (average
288 per function), main() is 1400 bytes, MouseParams is 1700 bytes,
and, for no readily obvious reason, UpdLine is 3091 bytes.

The 5× slowdown may also be *pessimistic* for the self-compiling
JIT-compiler: maybe I can write it to use constructs that don’t
require run-time bounds checking, or add an optimization or two
targeted to its own code.  And maybe it won’t use trampolines for its
internal calls.

The worst-case slowdown is hard to calculate.  If we JIT-compile a
whole subroutine at a time, we can compile code that we never run.  In
the first example, `DFixMarks` begins by checking `curbuf->b_marks`
against NULL; if the check succeeds, it immediately returns, having
executed perhaps 30 clock cycles, which is a lot less than the 4300
cycles to recompile it with a HLL-compiled compiler or the 900 cycles
to recompile it with a hand-coded assembly compiler.

The relevant parts of the `pcc` C compiler are about 50 kloc, so it’s
only a bit more than Jove.  And it’s much less latency-sensitive: if
the compiler takes 20 seconds to compile a source file, which is
plausible, you don’t care that much if it had a bunch of
100-millisecond pauses in the middle due to JIT-compiler thrashing.
