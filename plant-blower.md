John Plant’s [new unidirectional blower design][0] is a big step
forward for the efforts he calls “Primitive Technology”.  It enabled
him to [smelt 51 grams of iron][1] in 81 minutes from about 1.6kg of
[bog iron][2] ore using about 5.8kg of charcoal.  The theoretical
yield of 1.6kg of FeO(OH) (his red iron oxide bacteria slime turned
black when he dried it) would be 1.006kg of iron, so this yield could
still be greatly improved, but it’s much better than his previous
efforts.

[0]: https://youtu.be/bS4_K5_tHbg
[2]: https://en.wikipedia.org/wiki/Bog_iron
[1]: https://youtu.be/fJUJ2DapLKs

Plant’s design uses a [volute centrifugal fan housing][3] with an
[arithmetic spiral][4] guiding air through a tuyere into the smelting
furnace.  He uses four radial (uncurved) fan blades; each consists of
a large fallen leaf folded six layers thick and cut to a 75mm × 62.5mm
rectangle with a stone, then wedged into a 250mm-long spoke made from
a split stick.  The spoke is lashed shut (using the fibrous retted
inner bark of a local tree) to clamp two opposing fan blades and the
axle, which is a fire-sharpened 500mm-long stick spinning horizontally
between two 1-meter stakes driven into the ground.  The axle is driven
with a rope made from the same bark, bow-drill-style, using a
750-mm-long straight “bow” stick; a single twist of the rope around
the axle gives good driving friction when the rope is taut, but allows
the axle to spin freely when the tension is released to move the stick
the other way.

[3]: https://www.hindawi.com/journals/ijrm/2016/4849025/ "Genetic Algorithm Optimization of the Volute Shape of a Centrifugal Compressor, by Martin Heinrich and Rüdiger Schwarze, January 02016. International Journal of Rotating Machinery. 2016: 1–13. doi 10.1155/2016/4849025.  Plant did not do genetic algorithm optimization of his design."
[4]: https://en.wikipedia.org/wiki/Archimedean_spiral

Plant’s housing design is geometrically fairly simple, built with
levigated red clay tempered with sand; he cuts a base into the volute
shape from a 400-mm-diameter clay disc, with his spiral expanding from
250mm diameter to 400mm diameter in a single turn.  100-mm-tall walls
are made from slabs of the same clay molded in a split-cane mold, I
think the same one he used for his roof tiles.  He didn’t measure the
thickness of the walls or base, but they look to be on the order of
15mm thick.  A lid is cut from another disc and later sealed to the
walls with wet clay, and both the base and the lid have 62.5-mm
circular air inlet holes cut into their centers.

He does not fire the clay housing, only drying it, so it is fairly
fragile.  It’s well enough tempered that it didn’t crack while drying
despite its lack of fibrous reinforcement.

Fibrous reinforcement is one obvious improvement that could be made,
allowing the clay to be lighter and less fragile and the blower to be
more portable.  Geometric optimization also offers a lot of
opportunities.

Running tensioned rope between the ends of the spokes might allow the
use of thinner spokes.  But it might not; the spokes would still have
to resist the same average moment, which can be fairly large, since
the torque is applied at the axle.

Applying the bow’s torque between two support bearings instead of near
a single bearing could enable greater power to be transmitted reliably
(that is, without inducing failures).  The trick would be to do it
without increasing the friction too much over the pointed-stick plain
bearing he’s using now.  Wet or oiled glazed clay on wood would
probably work well enough, and because the wood would only make
contact with the bearing when bent by the bow’s pulling force, it
would only make contact on one side.  A smooth stone wedged into a
hole in the additional support stake might be enough; perhaps a dense
but soft stone like marble would be best, so that over time the stone
would abrade to match the curvature of the wood.

Possibly an axial-fan design would be a better match to the
forge-blowing application, which I believe is fairly low pressure, or
at any rate can be made so by using the charcoal in larger chunks.  An
axial fan would also eliminate most of the housing.  Axial fan blades
would require a different, more complex construction technique,
perhaps lashing a clamp onto a spoke to permit free choice of blade
angles.

I suspect that you could lightweight the housing a great deal by a
different choice of materials, which after all only need to withstand
the low-pressure airflow.  Suitable non-primitive-technology choices
would include corrugated cardboard, paperboard, aluminum flashing, or
starched cloth, but these are much less appealing if you have to pulp
wood for the paper, smelt the aluminum, or spin the thread and weave
the cloth by hand.

Suitable near-natural materials might include:

- leaves on a wicker frame cemented together with slaked lime or dried
  clay (Plant [has made lime from snail shells in a wood fire][5] in
  an updraft kiln) or some other adhesive (see file
  `near-natural-adhesives.md`);
- tree bark;
- rawhide on a wicker frame, a frame which can be later removed
  (though hunting is illegal in Plant’s area, herding is not).

[5]: https://youtu.be/Ek3aeUhHaFY

An alternative in the opposite direction, involving handling a great
deal more mass but less materials processing, would be to dig most of
the blower housing as a pit in the ground, with the fan rotating
around a vertical axis, like Plant’s previous bidirectional
centrifugal blower design, rather than a horizontal axis like this
one.

