The Codec2 free-software package is a low-bandwidth voice codec
designed for low-bandwidth ham radio, with modes from 450 bits per
second up to 3200 bits per second.  [The author has posted listening
samples online][0].  It’s in Debian, but it [can also run on an
STM32F405 board][1] that the author is happy to sell you, or which you
can build to sell yourself under the TAPR Open Hardware License.  It
does need an FPU to run in real time.

[0]: https://www.rowetel.com/wordpress/?page_id=452
[1]: https://svn.code.sf.net/p/freetel/code/smartmic/SM1000-B/SCH-SM1000-REV-B1.pdf

Using `c2enc` and `c2dec`: 150–200 words per minute at 1200 bps
---------------------------------------------------------------

The `c2enc` and `c2dec` programs it ships with, included in `apt
install codec2`, are somewhat underdocumented, but they process raw
audio, expected to be 16 bits, signed linear, 8 ksps.  At nominally
1200 bits per second, this `sox` command:

    sox foo.wav -t raw -r 8000 -c 1 -e signed-integer -b 16 - |
         c2enc 1200 - - | c2dec 1200 - - |
         play -t raw -r 8000 -c 1 -e signed-integer -b 16 -

plays back speech audio fairly comprehensibly, comparable to LPC-10,
but the encoding in the middle is at half of LPC-10’s data rate, 150
bytes per second rather than 300.

To write the encoded data to an file and play it back from the file:

    sox lento.wav -t raw -r 8000 -c 1 -e signed-integer -b 16 - |
        c2enc 1200 - - > lento.1200c2

    c2dec 1200 lento.1200c2 - |
        play -t raw -r 8000 -c 1 -e signed-integer -b 16 -

In this case `lento.wav` was a 9.38-second recording of a woman
speaking slowly in Spanish (13 words plus some silence, so an average
of 83 words per minute and probably a peak of 100).  There was a bit
of clipping distortion from overdriving the microphone.  The encoded
file `lento.1200c2` was 1404 bytes.  It came out perfectly clear.  On
doubling the audio speed with `ffmpeg -i lento.wav -filter:a atempo=2
rapido.wav` the resulting uncompressed audio is fine, but running it
through the same 1200-baud pipeline garbles it.  At 1.5× speed it
comes through fine.  At 1.75× speed it also sounds fine to me, but I’m
not sure I’d be able to tell if it wasn’t fine, because already
knowing what it’s supposed to sound like might be biasing me.

So roughly I think Codec2’s 1200 bits per second is adequate for
somewhere around 150–200 words per minute.

Other bit rates that can be substituted instead of `1200` in the
`c2enc` and `c2dec` commands include `700C` and `450`.

Converting audio from a YouTube video with `ffmpeg`, `sox`, and `c2enc`
-----------------------------------------------------------------------

A thing missing from the above commands is specifying the number of
channels, which `sox` defaults to being the same on the output as on the
input.  This worked:

    sox annhardy.wav -t raw -c 1 -r 8000 -e signed-integer -b 16 - |
        c2enc 1200 - annhardy.1200c2

The .wav file in this case was larger than the downloaded video
itself.  I generated it as follows:

    ffmpeg -i Oral_History_of_Ann_Hardy_-_Session_1_Part_2-\[xIWMvtM02NA\].mp4 \
        annhardy.wav

The .mp4 (2 hours, 13 minutes, 29.74 seconds) was 354MiB, and the .wav
was 1.4GiB.  There’s probably an option to get `ffmpeg` to reduce to
8ksps and one channel, which would make it slightly smaller than the
mp4, and maybe a way to get it to write to a pipe.  On encoding it at
1600bps (for some safety margin) with Codec2:

    time sox annhardy.wav -t raw -c 1 -r 8000 -e signed-integer -b 16 - |
        c2enc 1600 - annhardy.1600c2

I got a 1.6MiB file in 16.89 seconds; with gzip, it compresses to
1.5MiB, and with bzip2 or lzip, it compresses to 1.3MiB, 165 bytes per
second with lzip.  This audio is considerably clearer than the
YouTube-generated subtitles, although it has very noticeable
degradation.

The fact that bzip2 or lzip can compress the audio by another 20% is
probably a result of the fixed-bit-rate nature of Codec2 leaving
significant redundancy in silent frames.  Not just for archival, but
also for sending store-and-forward voice messages, this might be a
useful mode.  But it doesn’t work well for kilobyte-sized messages;
gzip, lzip, bzip2, and ncompress all make the 1404-byte `lento.1200c2`
bigger instead of smaller.

One gigabyte at 165 bytes per second works out to almost 1700 hours of
audio; 175 words per minute is about 2.9 words per second, so 57 bytes
per word.  Plain ASCII text is about 5–6 bytes per word (this buffer
is currently 5.6 bytes per word) so this is about 10× plain ASCII
text, but with all the nuances of prosody and accent preserved,
including emphasis, intonation, and pauses.

Opus
----

Other alternatives for this sort of thing include Opus, but Opus can’t
really go below about 690 bytes per second (5500 bits per second) and
at that rate it sounds terrible, worse than LPC-10.  This produces a
5500-bit-per-second file, despite requesting 4800 bits per second:

    ffmpeg -i lento.wav -c:a libopus -b:a 4800 lento.48.opus

Using `-c:a speex` and a `.mkv` file rather than `.opus` doesn’t sound
quite as bad (surprisingly, because Opus is supposed to be a
replacement for Speex) but also refuses to go below about 5400 bits
per second.
