I recently read (most of) William Kamkwamba’s semiautobiography, where
he talks about the tools, materials, and techniques he used to build
his first windmill: hot nails and steel wires heated in a wood fire,
PVC pipes heated over a wood fire, paying the town welder to weld some
things, cutting rubber belts out of shoe soles dug out of the
household trash heap, etc.

He was working with really *very* little, but there’s a pretty
standard set of tools, materials, and techniques that seems to be
widely used for flexible fabrication in many places that are still
pretty poor.  This seems like it might be a good place to get
information about which tools, materials, and techniques have the
biggest bang for the buck, because they’ve become widely adopted among
people with very few bucks indeed.  YouTube videos from all over the
planet are a valuable, if sometimes untrustworthy, guide to this
toolkit.

In [Simple Tool for Cutting Sheet Metal][0] Fab2Ku constructs a
sheet-metal shear from the following:

- tools:
    - cellphone camera
    - angle grinder
    - cutoff discs
    - stick welder
    - dross hammer
    - machinist’s square
    - tape measure
    - abrasive chop saw with vise
    - pencil
    - center punch
    - heavy hammer (2 kg, maybe)
    - drill (press?)
    - rough concrete floor
    - dirt floor
- materials:
    - spray paint
    - scrap steel plate (sparks like mild, though hardened would work
      much better for the shear, and wouldn’t pose any difficulty to
      the abrasive cutting tools)
    - white-out paint marker
    - scrap wood
    - nails
    - spray lubricant for drilling steel (maybe just water?)
    - bent scrap structural tubing
    - welding electrodes
    - bolt and nut
- techniques, aside from the obvious ones:
    - white-out and pencil for layout for cutting and drilling the
      rusty plate
    - scrap wood as wasteboard for drilling steel

[0]: https://youtu.be/qoOg0yr4mC4

https://www.youtube.com/watch?v=PiuhDp-2SKU
