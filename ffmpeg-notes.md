FFmpeg is a useful program for manipulating video, but I always have
to look up how to do things with it.  So I thought I’d make some notes
here.

Background
----------

My cellphone can record decent video, including with the free-software
TimeLapseCam application from F-Droid, but it’s not very well
compressed, and it leaks a lot of privacy-invading identifying
metadata, including the Android version, phone model, and creation
time, [as displayed by `ffprobe`][2].

[2]: https://superuser.com/questions/1092951/how-to-delete-a-single-metadata-item-from-an-mp4-file

In all likelihood there is plenty of information available in the
video data itself to identify these things, though re-encoding at
lower quality may obscure it.

Matroska is the standard video container format, and supports Opus
audio, but Firefox doesn’t support Matroska.  MPEG-4 supports the
H.264 codec fine, and Firefox does support it, so that’s what you
should use for video for the browser.

H.264 (AVC1) is probably the best codec to use.  Because it was
published in 02004, it’s out of patent now, and FFmpeg contains a
high-quality implementation of it, the one from DarkTangent’s libx264.
H.265 (HEVC) arguably permits marginally better quality/bandwidth
tradeoffs but is much slower, sometimes has more objectionable
(realistic) artifacts, and is still burdened with patents because it’s
from 02013.  AV1 is even more patent-burdened and in FFmpeg it’s too
slow for most uses.

Particular FFmpeg recipes
-------------------------

### To see what a video is, `ffprobe f.mp4` ###

This produces output with useful information like this:

      Duration: 00:02:30.61, start: 0.000000, bitrate: 20179 kb/s
        Stream #0:0(eng): Video: h264 (Baseline) (avc1 / 0x31637661), yuvj420p(pc, smpte170m), 1920x1080, 20001 kb/s, 23.96 fps, 23.98 tbr, 90k tbn, 180k tbc (default)
        Metadata:
          rotate          : 270
          creation_time   : 2023-08-29T19:57:43.000000Z
          handler_name    : VideoHandle

### To set a bandwidth budget for video use `-b:v` ###

    ffmpeg -i input.mp4 -b:v 768k -c:v h264 output.mp4

768 kbps was too little for the original 1080×1920 video, which was
originally encoded at 17.1 Mbps without audio by TimeLapseCam, which
is also about what the standard LineageOS camera application uses for
normal videos, along with AAC mono audio in the 96 kbps range.

I think possibly I should have used `libx264` instead of `h264` here,
but I’m not sure if it matters.

### To set a bandwidth budget for audio use `-b:a` ###

Like `-b:a 48k`, which is probably okay even for some music with AAC
`-c:a aac` or especially Opus `-c:a libopus`.  Don’t forget the `k`,
though FFmpeg will detect your error and refuse to try to encode audio
at 48 bits per second.  48 kbps is grossly excessive for speech.

### To reduce bandwidth further, reduce the framerate and resolution and audio budget ###

I’ve done this before but don’t have a tested command line handy.  I
think resolution is `-s wxh` and framerate is `-r fps`.

### To crop video use `-filter:v crop=w:h:x:y` ###

However, 768 kbps was fine for the 640×1080 video I cropped it to with
`-filter:v` (not `-f:v`, which would specify the format, but I think
`-vf` might work too):

    ffmpeg -i input.mp4 -b:v 768k -c:v h264 \
        -filter:v crop=1080:640:0:640 output.mp4

That’s [width:height:x:y][0].  This produced a decent 4.8-megabyte
video from the original 113-megabyte video.

[0]: https://video.stackexchange.com/questions/4563/how-can-i-crop-a-video-with-ffmpeg

### To remove metadata leaks, [`-map_metadata -1`][1]; `-c:v copy` to not re-encode ###

    ffmpeg -i input.mp4 -c:v copy -map_metadata -1 output.mp4

[1]: https://superuser.com/questions/441361/strip-metadata-from-all-formats-with-ffmpeg

The `-c:v copy` prevents re-encoding of the video, so this command
runs almost as fast as `cp`.  Normally you should prefer `-c copy` to
copy the audio too, but I didn't know that, and this file didn’t have
an audio track.

There’s also the possibility of [removing a particular metadata tag
such as an incorrect rotation][3].

[3]: https://superuser.com/questions/1092951/how-to-delete-a-single-metadata-item-from-an-mp4-file

### General-purpose bandwidth reduction recipe ###

This is somewhat better quality than NTSC for most purposes, but at a
lower frame rate:

    ffmpeg -i input.mp4 -r 15 -s 540x960 -c:v h264 -b:v 512k \
        -b:a 48k -c:a aac output.mp4

Note that 540x960 is portrait-mode.  Also note that FFmpeg is happy to
squish or stretch your video into the wrong aspect ratio if you get
this wrong.  (See below for aspect ratio preservation recipe.)

This reduced a 5'44" video from 734 MB to 24.1 MB.  I suspect I may
have been using a too-high bitrate; [a post on Superuser suggests
using `-crf` instead to control the quality][4], “where sane values
range from 19 to 25 – lower means better quality, but also higher
bitrate”.  I haven’t tried this.  [Streaming Tutorial II][5] suggests
`-i_qfactor 0.71 -qcomp 0.6 -qmin 10 -qmax 63 -qdiff 4 -s 640x360 -b:v
1000k -b:a 56k`, which I don’t understand.

[4]: https://superuser.com/questions/438390/creating-mp4-videos-ready-for-http-streaming
[5]: https://www.willus.com/author/streaming2.shtml

On common PC hardware these settings can actually encode faster than
real time due to the decimation factor.

15 fps is pretty close to the lower limit for the illusion of motion.

#### Reducing size while preserving aspect ratio ####

You can use [-vf scale=640:-2][8] to avoid distorting the aspect ratio
on videos of unknown aspect ratio.  For example:

    ffmpeg -i input.mp4 -r 20 -vf scale=640:-2 -c:v h264 -b:v 512k \
        -b:a 48k -c:a aac output.mp4

[8]: https://stackoverflow.com/questions/8218363/maintaining-aspect-ratio-with-ffmpeg

### To both crop and scale, comma-separate filters in `-vf` ###

I tried two `-vf` options but it just ignored the first one, so I
ended up with this to crop off some 40-pixel-tall letterboxing black
bars someone had added to the video at some stage to make it 720x480:

    -vf crop=720:400:0:40,scale=640:-2
    
Note that the `crop` arguments are in terms of the unscaled
coordinates, which are being set to 640 x whatever preserves the
aspect ratio best without being odd (356, as it turned out).  I didn’t
try swapping the order around the comma but I suspect that would have
required me to use post-scaling coordinates.

### Extreme bandwidth reduction recipe: full-motion video over ISDN ###

This has a lot of visible artifacts:

    ffmpeg -i input.mp4 -r 15 -c:v libx264 -g 60 -s 320x180 -crf 30 \
        -b:a 8k -c:a libopus output.mkv

This is about 85 kilobits per second and good enough for the
particular model-building video I’m testing it on.  You can read the
marks on the ruler the guy holds up in the video.  Increasing the
[`-g` parameter][6] to 120 had absolutely no effect on the bandwidth,
and adding `-keyint_min 120` also didn't help.  It reported 555
I-frames in the first 19'30", which is every 2.1 seconds, four times
the frequency I was hoping for.  Possibly this is because libx264 is
identifying a scene cut every couple of seconds.  `-g 120 -keyint_min
120 -x264opts keyint=120:min-keyint=120:no-scenecut` did work to
reduce I-frames to 149 in the first 19'45" (every 6.8 seconds) but
only reduced the bandwidth to 82.5 kbps.  Switching to `-s 428x240`
increases bandwidth close to 110 kbps; replacing `crf 30` with `-b:a
4k -b:v 60k` yields worse-looking and -sounding video, with really
terrible audio artifacts at about 67kbps.  Going back down to `-s
320x180` and switching to `-b:a 6k -b:v 58k` yields a more acceptable
64kbps result.

So the final result, which looks about the same as the one produced
above but is only 65.7kbps:

    ffmpeg -i input.mp4 -r 15 -c:v libx264 -g 120 -keyint_min 120 \
        -x264opts keyint=120:min-keyint=120:no-scenecut -s 320x180 \
        -b:a 6k -b:v 58k -c:a libopus output-64k.mkv

This makes a 78-minute long video occupy less than 34 megabytes.  On
this four-core 2.4GHz Ryzen 5 3500U laptop it encodes at about 15×
real time.

I think you could probably get this below 64kbps or even 56kbps.

### Converting to WebM with `-c:v vp9 -c:a libopus` for streamability ###

WebM, like MPEG-TS and Matroska, has the advantage over MP4 that it’s
streamable, so you can play the file before it’s complete.  But WebM
is supported in browsers.  However, WebM doesn’t support either AAC or
H.264, but VP9 and Opus are okay.  IIRC the codec FFmpeg calls
`libopus` is more mature than `opus`.
[6]: https://superuser.com/questions/908280/what-is-the-correct-way-to-fix-keyframes-in-ffmpeg-for-dash

    ffmpeg -i input.mp4 -c:v vp9 -c:a libopus output.webm

For a given 540x960 15fps video (produced by the general-purpose
bandwidth reduction recipe above) this ran at about half of realtime
and defaulted to about 280 kbps.  I don’t think VP9 is actually that
much better than H.264, and I’m not sure it’s better than H.264 at
all, so maybe I was just encoding H.264 with the bitrate set way too
high for that resolution and frame rate.

This VP9 codec takes about three times as long to encode as H.264.

### Adding faststart data to an MPEG-4 file for streamability with `-movflags faststart` ###

This makes the MP4 file streamable by adding more information to it in
a second pass at the end of processing.

    ffmpeg -i input.mp4 -movflags faststart -c copy output.mp4

This enables a truncated or otherwise partial file to be played.

However, this isn’t useful for the case where I care about
streamability, which is that I want to be able to play back the
partial file to see if it’s totally shit and I should cancel the
conversion.

### Using a Matroska container for streamability and converting to MP4 as a final step ###

The above streamability problems can be mostly solved with .mkv, but
as mentioned before, browsers don’t support it.  To see how 256kbps
video looks while it’s being generated, for example:

    ffmpeg -i input.mp4 -b:v 256k tmp.mkv  # slow, run mpv tmp.mkv meanwhile
    ffmpeg -i tmp.mkv -c copy output.mp4   # instant

### Baking subtitles into a video file ###

    ffmpeg -i input.mp4 -i input.vtt -c copy output.mkv

This won’t work with an MPEG-4 output container; ffmpeg silently
unmaps the subtitles stream.  Maybe MPEG-4 doesn’t support including
subtitles.  Matroska works.

Note that `mpv` starts up without displaying subtitles.  You have to
hit `j` to turn the subtitles on.

I think it’s usually preferable to keep the subtitles in a separate
file; it makes them easier to edit, or to preserve when you delete the
video file.

### Concatenating videos ###

The [concat codec recipe by rogerdpack][7] looks like this:

    : gdpongmini; cat vidlist.txt
    file 'vidbody/pong2.avi'
    file 'vidbody/pong3.avi'
    file 'vidbody/pong12.avi'
    file 'vidarea/toojumpy.avi'
    file 'vidarea/bounce1.avi'
    file 'vidarea/bounce2.avi'
    file 'vidarea/bounce3.avi'
    file 'vidarea/bounceforever.avi'


    : gdpongmini; ffmpeg -f concat -i vidlist.txt -c copy tmp.avi

[7]: https://stackoverflow.com/a/11175851

This requires some cautions:

1. The order of these flags on the command line is inflexible, and if
   you get it wrong, you get error messages like `vidlist.txt: Invalid
   data found when processing input` or `Unknown decoder 'copy'`.
2. The paths must be relative or you get this:

        [concat @ 0x55dffd47a080] Unsafe file name '/home/user/gdpongmini/vidbody/pong2.avi'
        vidlist.txt: Operation not permitted

3. Similarly, you have to use apostrophes `''` and not quotes `""`.

This is pretty quick, like 300 milliseconds in this case for an
80-second 61-mebibyte video.

### Cutting a video (tempooral crop, extracting a segment) ###

This selects the first 12 minutes and 34 seconds:

    ffmpeg -i input.mkv -c copy -t 12:34 output.mkv

[There's an `-ss` flag][9] for selecting the start time but `-ss 0` somehow
was chopping off the beginning of the file.

[9]: https://superuser.com/questions/377343/cut-part-from-video-file-from-start-position-to-end-position-with-ffmpeg

### Doubling the speed of an audio file ###

    ffmpeg -i lento.wav -filter:a atempo=2 rapido.wav
