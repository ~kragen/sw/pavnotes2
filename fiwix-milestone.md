On 02023-05-08, rickmasters announced on IRC:

> A huge milestone has been passed today!
>
> I have live-bootstrap fully working, including bootstrapping all
> kernels from source, starting with a 200 byte binary seed! The
> initial seed is called builder-hex0-stage1 and boots as an MBR
> (after padding) then reads hex0 source from disk, compiles, and
> transfers control to the 3.5K builder-hex0-stage2 kernel which was
> used through to building and kexec of Fiwix, and Fiwix is used up to
> building and kexec of Linux, and Linux runs the rest of the way.
> 
> This is the culmination of over 14 months of work so it's pretty
> exciting.
> 
> I should mention I received a lot of help on this effort, especially
> from Mikaku, stikonas, fossy, and oriansj.
> 
> It might be a couple days before I submit a PR with updated packages
> and checksums.
> 
> One minor caveat: There is currently an open issue regarding
> insufficient disk space to complete sysc (which is unrelated to this
> work) so I have yet to complete building the final gcc 12.2.0
> package.
