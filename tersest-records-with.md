ML's data types are basically discriminated unions of records.  I
might thus define a type for lazy streams of, say, states:

    type stream = Cons of state * stream | Thunk of (unit -> stream) | Mzero

Then I can write functions that match on the type:

    let rec mplus (t : stream) = function (* Monad plus: merge two streams *) 
      | Mzero -> t (* If the second stream is empty, return the first stream *)
      | Thunk f -> Thunk (fun () -> mplus (f()) t) (* η⁻¹-delay and swap *)
      | Cons(e, s) -> Cons(e, mplus t s)  (* or simply recurse if eager *)
    and bind (g : goal) = function (* Monad bind: transform stream with g *)
      | Mzero -> Mzero
      | Thunk t -> Thunk (fun () -> bind g (t()))
      | Cons(e, s) -> mplus (g e) (bind g s)

(This is from [my port of the logic programming language μKanren to
OCaml][0], if you're curious.)

[0]: http://canonical.org/~kragen/sw/dev3/exkanren.ml

This is not a terribly unusual way to use pattern-matching.  ML's
pattern matching is quite powerful, but it's fairly common to see
simple pattern-matches like that, where you're only dispatching on a
single variant tag.

In most programming languages, instead of naming the fields of a
record type in each piece of code that uses them, we name them in the
definition of the record type:

    struct Cons { state &e; stream &s; };  // C++
    Cons = namedtuple('Cons', 'e s')       # Python
    (defstruct kons e s)                   ; Common Lisp
    (define-struct kons (e s))             ; Racket

Since most such record types are used in more than one place, in some
sense, naming the fields again in every pattern-match on that record
type is somewhat excessive.  And it has the potential disadvantage for
clarity and correctness that the fields are only identified
positionally.

JS adopted a `with` statement from, I think Visual Basic.  Though it's
deprecated, even current JS engines support this syntax:

    with ({x: 3, y: 4}) { console.log(x + y); }

This is equivalent to

    console.log(3 + 4);

This is pretty terrible in a dynamic language like JS because when you
say `with(v)` you have no way to know what fields `v` may or may not
have, and indeed it may vary from invocation to invocation.

But suppose we are instead in a language with record types.  Can we
rehabilitate `with` by using this very limited form of
pattern-matching?  It seems like it might make for terser code, even
without any static typing; here `on` is a `with` that dispatches on
the variant type tag:

    rec cons(head, tail);
    rec thunk(cb);
    rec mzero;

    // Monad plus: merge two streams
    mplus(s1, s2) {
        on(s2) {
            mzero: s1;  // If second stream is empty, return the first stream
            thunk: thunk(() => mplus(cb(), s1)));  // η⁻¹-delay and swap
            cons: cons(head, mplus(s1, tail));  // or simply recurse if eager
        }
    }

    // Monad bind: transform stream with g
    bind(g, s) {
        on(s) {
            mzero: mzero();
            thunk: thunk(() => bind(g, cb()));
            cons: mplus(g(head), bind(g, tail));
        }
    }

A disadvantage is that the names of the fields have a pretty large
scope, and if you're inside of several nested `with`s it may be hard
to figure out which one bound a particular local variable; you have to
look at the record definitions.  Also, if you want to do such nesting
with the same record type, you need to explicitly define new local
variables for the outer fields.

As a shorthand you could also write

    rec cons(head, tail), thunk(cb), mzero;

but I didn't want to do that for the example because I thought it
might look like I was listing the alternatives for a particular union
type, which wasn't my intention; I was thinking about a purely
dynamically typed system, like what you get by default with Common
Lisp `defstruct`.
