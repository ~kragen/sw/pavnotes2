Cheap off-the-shelf microcontrollers with quartz crystal oscillators
can manage 10-ppm timing precision and thus duty cycle variations
within 10ppm of linearity, and duty-cycle variations of some 41ns
(assuming 24 MHz I/O clock).  Moreover, they can manage sophisticated
pulse density modulation.  This can permit fairly sophisticated LCR
measurement, curve tracing, and component identification, in theory
with four or five significant figures.

Consider using something like the [US$5 STM32F103C8T6][21], with its
dual 12-bit 1Msps ADCs, 1.16–1.24V internal analog bandgap reference,
and 3.3V supply and I/O voltage.  If we approximate its internal
bandgap as 1.1V, that’s 270μV per count, so quantization error is half
that (disregarding figures like integral nonlinearity and differential
nonlinearity).

Ideally you’d have a gadget with two, three, four, or six leads that
you hook up to two or three leads of a component, and it instantly
tells you what it probably is and what kind of network it looks like,
and downloads a measured characteristic curve onto your laptop.
Earlier I've called this the “Tinkerer’s Tricorder”.
[21]: https://www.digikey.com/en/products/detail/stmicroelectronics/stm32f103c8t6/1646338

The LCR meter ought to use low voltages (0.1V) to avoid damaging
sensitive components, limit the current it can subject them to, and
balance the current it does subject them to over time to prevent
polarization of polarization-sensitive components like LCDs and
electrolytic caps.

Existing market products
------------------------

### Uni-T UT603 ###

[The Uni-T UT603][20], which costs US$110 here, can measure
resistance, inductance, and capacitance, but only one at a time.  It
can also test transistors and tell you their β, measure diode
threshold voltage, and beep on continuity.  It doesn’t have voltage or
current scales or any data connections other than the display.

- For inductors, its advertised precision is ±2%, 2mH to 200H.
- For capacitors, ±2%, 0.002μF to 600μF.
- For resistors, ±0.8%, 200Ω to 20MΩ.

[20]: https://articulo.mercadolibre.com.ar/MLA-717809264-medidor-lcr-uni-t-ut603-emakers-_JM

I think I can beat that by orders of magnitude.

### Uni-T UT622A ###

[The Uni-T UT622A][22], which costs US$1300 or [(from
Electrocomponentes) US$680][24], tests components at different
frequencies (100Hz, 120Hz, 1kHz, 10kHz), measures either of L, C, R,
or Z simultaneously with a secondary parameter of Q, ESR, *θ* (phase
angle), or D (dissipation factor), has a USB data cable, and can test
at 100mV, 300mV, or 1000mV RMS.  Its precision is given as 0.1%, or
1000ppm.  It includes a guard-electrode connector so you can isolate
the DUT from EMI.

[The English-language manual][23] goes into more detail: it can test
20 times a second and can measure 1nH to 10kH, 0.001pF to 0.1F, and
100μΩ to 100MΩ, though the DC resistance measurement is limited to
100μΩ to 1MΩ.  I don’t understand its specification of the precision
at all.

It has an enhanced version that also tests DC resistance, has a
controlled 100Ω signal source impedance I guess to prevent
reflections, supports four-wire Kelvin measurements, and has a trigger
button.  The trigger button can transmit a measurement (over the USB
cable?) to your PC, or you can just use a triggered mode to only
measure when you press it.  You can switch between measuring an
equivalent parallel resistance and an equivalent series resistance
with a “P↔S” button.  An “AUTO” button guesses what your component is,
and sets the secondary parameter to D (dissipation factor) for
capacitors, Q for inductors, X (I guess this is impedance; units are
ohms) for resistors, and *θ* for complex impedances.

It has a go/no-go mode where you can set a nominal value and tolerance
(of 1% to 20%), and it sounds an alarm when you measure an
out-of-tolerance component.  This also displays the error from nominal
as a percentage.  It counts the number of passed and failed
components.

It has a “recording” mode which lets you select min, max, or mean as
the aggregated value to display, but this mode cannot display the
secondary parameters.

It includes “open circuit clearing” and “short circuit clearing”
modes, which seem to be a tare or calibration function; short-circuit
clearing “can reduce the effect of contact resistance and test leads
resistance on measuring low-impedance components”.  Open-circuit
clearing is for high-impedance components to cancel things like test
lead capacitance.

Via the USB cable it not only charges its battery but appears as a
9600-baud serial device by default, supporting up to 38400 baud.  It
speaks “only a small portion of” a line-oriented protocol called the
“SCPI command system”, which is explained in the manual.  This
supports configuring the meter and polling its measurements.

[22]: https://articulo.mercadolibre.com.ar/MLA-1155977527-uni-t-medidor-lcr-portatil-ut622a-_JM
[24]: https://www.electrocomponentes.com/tienda/instrumental-medicion/medidores-lcr/medidor-lcr-portatil-uni-t-ut622a
[23]: http://unitrend.oss-cn-hongkong.aliyuncs.com/upload/file/20210530/UT622%20Series%20LCR%20meters%20User%20Manual.pdf

I think it should be possible to simultaneously beat this by about one
order of magnitude on precision and measurement speed (though I
suspect that might demonstrate a flaw in my reasoning) and by two
orders of magnitude on price.

### LCR-T4 M328 Transistortester ###

The so-called [LCR-T4 or M328][25] component ID tester is [Markus
Frejek’s open-source ATMega328-based design as enhanced by Karl-Heinz
Hübbeler][26].  It costs US$17, and does component ID (including
different types of transistors) as well as simultaneous LCR
measurement.  The seller doesn’t advertise a precision so it probably
sucks.  Bloggers who have tested them sometimes find errors as high as
10%, because it really depends on the build quality.

- Inductors: 10μH–20H
- Capacitors: 25pF–0.1F
- Resistors: 0.1Ω–50MΩ

[25]: https://articulo.mercadolibre.com.ar/MLA-900561300-tester-transistor-mosfet-tiristor-atmega-lcr-t4-medidor-_JM
[26]: https://www.mikrocontroller.net/articles/AVR_Transistortester

I think it should be possible to equal this device on price and beat
it by three orders of magnitude on precision.

An LC whack at the problem
--------------------------

If you hook up a 3.3-volt GPIO pin to a low-pass LC filter with a
resonant frequency that is slow with respect to that 41ns, you can
inject a fairly precise amount of energy into the filter as a timed
pulse.  This depends on the precision of the LC components, which in
turn depends on the temperature, which you can measure but only with
some error (probably around 1°) and some time lag depending on how
large the components are.  So it would be desirable to have the LC
values have a low temperature coefficient, ideally less than 10ppm/°.

Values that are a little high or low can be corrected by occasional
calibration, but components that have wide tolerances will also tend
to have high tempcos and high drift.

Say you pick 1 kHz as the 1/√(LC) resonant frequency, and you want to
use NP0/C0G capacitors for precision, but at reasonable prices those
only go up to 0.1μF, like the [TDK CGA5L2C0G1H104J160AA][0], 39¢ in
quantity 50, 50V, 1206 size, ±5%.  Or the [Murata
GRT31C5C1H104FA02L][1], 49¢ and ±1% but otherwise identical.  You can
reasonably put ten of them in parallel for 1μF.

[Murata’s datasheet][2] says these C0G caps are ±30ppm/°.

[0]: https://www.digikey.com/en/products/detail/tdk-corporation/CGA5L2C0G1H104J160AA/2672877
[1]: https://www.digikey.com/en/products/detail/murata-electronics/GRT31C5C1H104FA02L/16033721
[2]: https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRT31C5C1H104FA02-01.pdf

Unfortunately, this means we need a fairly large inductor of 25
millihenries, a large number that becomes larger at lower capacitance:

    You have: 1/((2π kHz)**2 * 1 uF)
    You want: mH
            * 25.330296
            / 0.039478418

Film capacitors are claimed by their manufacturers to be inferior in precision to ceramic caps
----------------------------------------------------------------------------------------------

Film capacitors are the standard old precision capacitor.  But nearly
all of the 785 0.1μF caps in stock at Digi-Key are ±5%, ±10%, or ±20%;
of the three that are ±1%, one is the [Vishay MKP1837410161F][3],
which is rated for 160V and costs US$1.60 in quantity 50.  [Its
datasheet rates it as -250ppm/°, *typical*][4], which means it will be
*worse* half the time.  This is a full order of magnitude worse than
the C0G.  Going down to the less exotic 0.01μF doesn’t help; the
[Vishay MKP1837310161G][5] has literally the same datasheet.  For new
designs Vishay recommends instead using the [MKP385][6] which has a
±5% precision rating and doesn’t really give a tempco, [not even in
the datasheet appendix][7].  The closest they give is an obviously
made-up plot of “capacitance as a function of ambient temperature”
showing 0–6% capacitance drop over 100°, which works out to *600*
ppm/°, *20* times worse than the C0G.  So I think film caps are
probably inferior to the C0G temperature-compensated ceramics.

They do have higher voltage ratings, though, and they’re easier to
recycle from broken TVs and stuff than NP0/C0G caps, which look just
like the much more common X5R caps.

[3]: https://www.digikey.com/en/products/detail/vishay-roederstein/MKP1837410161F/5393053
[4]: https://www.vishay.com/docs/26017/mkp1837.pdf
[5]: https://www.digikey.com/en/products/detail/vishay-roederstein/MKP1837310161G/5392896
[6]: https://www.vishay.com/docs/28174/mkp385.pdf
[7]: https://www.vishay.com/docs/28182/detailedratingsmkp385.pdf

Precision inductors are unobtainium
-----------------------------------

Precision inductors are much more difficult.  Most inductors are ±20%,
and in [Digi-Key’s “fixed inductors” category][12], there are only 38
in-stock inductors of ±1% (out of 30529 total in-stock inductors).
15013 of their in-stock inductors are ±20%.  Out of the 1209 ±2%
inductors in stock, the largest is 47μH.

[Rahat Husain wrote a paper in 01991][8] describing how to get your
precision inductor drift down to 0.01% per year, which is to say,
100ppm per year, but the biggest inductor he tested was 10mH.

[8]: https://www.sciencedirect.com/science/article/abs/pii/092159569180024A "Precision inductors: calibration and maintenance, DOI 10.1016/0921-5956(91)80024-A"
[12]: https://www.digikey.com/en/products/filter/fixed-inductors/71

[Ansari, Jyotsana and Saxena][9] at the National Physical Laboratory
measured a temperature coefficient of 30±5ppm/° on the precision
calibration inductors they bought from General Radio, stabilizing only
after 10–12 hours.  But apparently [precision inductors are normally
custom manufactured][10] for a particular circuit; quoting “chuckey”:

> The temperature coefficient of an inductor depends on many things
> which may be additive or subtractive over a given temperature range.
> If you are building a LC oscillator, the temperature coefficient of
> the whole circuit can be offset by using a negative temperature
> coefficient of part of the C.  If [it’s] for a filter, it is
> unlikely that the shift in the inductance would cause a measurable
> change in the filters characteristics[,] and if there is a change[,]
> it should be able to be compensated by the tuning of the filter.  If
> the inductance is being used as a choke, then its value is
> non[-]critical.

[9]: http://npl.csircentral.net/54/1/42.pdf
[10]: https://www.edaboard.com/threads/why-is-inductor-temperature-coeff-never-mentioned.327645/

On element14, [user neuromodulator documented a test][11] of inductor
tempcos; they heated up some standard KEMET inductors from 25° to 75°
over about 40' and saw inductance increases of 0.75% (7500 ppm) to
1.5% (15000 ppm), which works out to 150–300ppm/°.

[11]: https://community.element14.com/challenges-projects/design-challenges/experimenting-with-inductors/b/blog/posts/temperature-dependent-characterization-of-inductors-2-2

So it seems like precision inductors with a low temperature
coefficient do exist in the world, but they are not normal circuit
components.  The normal-circuit-component ones would hurt our
precision enormously.

An RC alternative
-----------------

Suppose that, instead of trying to cope with the limitations of
inductors, we use some of the excellent precision resistors easily
available, using an RC low-pass filter instead of LC.  We can protect
our microcontroller from injected overvoltages on the inputs with
resistors and external clamping diodes; even 1kΩ will limit the
current from 100V injected on the inputs to 100mA, and it’s easy to
find diodes that will cope with 100mA.

To keep leakage down to microamp levels, the clamping diodes should
probably be the PN junction type, not a Schottky type.

If we want an RC time constant on the order of 1ms with 1μF, 1kΩ would
be about right.  We can probably go with 1kΩ and 1μF.

Out of [Digi-Key’s 89589 in-stock surface-mount resistors][13] 73192
are ±2% or better.  More than half (49693) are ±1%.  4877 are 0.2% to
0.5%, 17319 are 0.1%, 830 are 0.02% to 0.05%, and 227 are 0.01%.

Of the 0.1%, the most abundant (with 1.29 million in stock) is the
[Susumu RG1005P-102-B-T5][14], a 1kΩ 0402-sized beastie with a rated
tempco of ±25ppm/°.  It costs 11.7¢ in quantity 100.  [In its
datasheet][15] it’s only rated for 75V because of, presumably, its
minuscule size.  A cheaper option is the [Panasonic ERJ-PB3B1001V
thick-film][16] at 8.3¢, which is also 1kΩ but is 0603, but with a
worse tempco of ±50ppm/°, and rated for 150V.

[13]: https://www.digikey.com/en/products/filter/chip-resistor-surface-mount/52?s=N4IgjCBcoLQJxVAYygFwE4FcCmAaEA9lANrggC6AvvjAEyIgqQY75GSkCsFlvQA
[14]: https://www.digikey.com/en/products/detail/susumu/RG1005P-102-B-T5/968752
[15]: https://www.susumu.co.jp/common/pdf/n_catalog_partition01_en.pdf
[16]: https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-PB3B1001V/6212755

Downshifting to the ±0.01% resistors, which are almost 100× less
varied than the ±0.1% ones, they’re all considerably more expensive,
but they’re still mass-market products available on Digi-key.  A
reasonable example is the [Stackpole RNCF0603TKY1K00][17], a 1kΩ
±0.01% 0603-sized thin film resistor with a rated tempco of ±5ppm/°,
six times better than the caps.  But it costs US$1.66 each in quantity
\10.

At 3.3V our initial voltage rise speed is 3300V/s, or 135μV per 41ns
timeslot.  Every 7.4 timeslots the voltage rises by a millivolt.
ln((3.3V-.1V)/3.3V) ≈ -0.03, so we reach 100mV after .03 time
constants, which is to say 30μs, about 700 of our timeslots (and 30
ADC samples).

### Self-calibrating the capacitance ###

0.01% is 100ppm, two orders of magnitude better than the precision of
the C0G/NP0 capacitors; by using our ±10ppm clock, with an open
circuit in place of a DUT, we can measure the RC time constant of the
capacitors to within about ±10ppm, which in turn allows us to measure
the capacitance to within the same relative error as the resistance,
after which point we’re much less concerned about the capacitor’s
manufacturing variability and aging drift, just variability with
things like temperature and voltage.

[17]: https://www.digikey.com/en/products/detail/stackpole-electronics-inc/RNCF0603TKY1K00/2269697

(In what follows I’m using 1.1V as the analog bandgap reference even
though the datasheet says 1.2V.)

Suppose that you do a series of 10-millisecond measurements on the RC
decay, averaging 100 such measurements totaling a million samples.
You measure that in 10ms ±10ppm, discharging through 1kΩ ±100ppm, the
voltage on the capacitor has discharged from 1.1V to 50μV, about 0.2
counts on the ADC.  The voltage measurements are subject to some
errror, too; let us suppose optimistically that we can do them
effectively ±270nV because of averaging all these samples; that’s
0.25ppm at 1.1V but 540ppm at 500μV.  The question is, how does the
measured value of the capacitor vary with these three sources of
error?  Because they’re so small, I think we can consider them
independently.

We start out with

> *v*₁ = *v*₀ exp(-t/RC)

and solve it for C as

> ln(*v*₁/*v*₀) = -t/RC  
> C = t/R ln(*v*₀/*v*₁)

- This varies directly with the measured time.  10ppm on the timer is
  100ns.  If the time is 10ms - 100ns, we compute 0.99999μF, 10ppm
  low.  If the time is 10ms + 100ns, we compute 1.00001μF, 10ppm high.
- It varies inversely with the resistance; a resistance that’s 100ppm
  high will give us a computed capacitance that’s 100ppm low, and vice
  versa.
- It varies a great deal less with the measured decayed-to voltage,
  because the derivative of ln(*x*) is 1/*x*, and here *x* ≈ 10;
  500ppm of error on those 50μV only produces 50ppm of errror.
- The error on the measured starting voltage probably doesn’t matter
  because it’s orders of magnitude smaller.

We can do better by measuring a shorter decay so the relative voltage
error is less.  At 6ms we expect to see that 1.1V has decayed to
2.7mV, at which level 270nV is 100ppm (giving 100ppm/6 ≈ 15ppm
errror), and at 3ms, we have 55mV, so 270nV is only 5ppm (giving
5ppm/3 = 1.7ppm error).  This suggests that we can actually afford
substantially higher voltage measurement errors.

However, naïve extrapolation suggests that by this method we can
reduce the effect of voltage measurement error to an arbitrarily low
level, which cannot possibly be correct.  Suppose we measure over 1μs,
during which time we expect to see the voltage decline by 1.0995mV.
If we still have 270nV of error (disregarding the error on the first
measurement for now) this might measure as 1.0992mV or 1.0997mV.
These would lead to extrapolating time constants that differ by
±250ppm, even though 270nV is only 0.25 ppm of voltage error.  I need
to think more about this.

It’s probably sufficient to measure over a long enough time for the
error to be small compared to other sources of errror, such as the
resistance error.

Theory of operation
-------------------

A couple of such RC circuits hooked up to different GPIOs allow us to
control the voltage on each end of the component; another resistor or
two to measurement pins allows us to measure how fast the capacitor is
charging or discharging.  By tristating all our pins we can let the
DUT float between two capacitors.

              R1                     R2
    GPIO1_/\  /\  /\___.__DUT__._/\  /\  /\___GPIO2
            \/  \/     |       |   \/  \/     
                       |       |
    GPIO3_/\  /\  /\___|       |_/\  /\  /\___GPIO4
            \/  \/     |       |   \/  \/     
            R3    C1 =====   ===== C2  R4
                       |       |
                      GND     GND

(Clamp diodes not shown.)

### Resistors ###

If you stick a large resistor between the two caps at slightly
different voltages and tristate all the pins, their voltage will
slowly equalize in an exponential decay curve.  We’re talking about
effectively 0.5μF here, so the time constant is as follows:

- 100Ω: 50μs, 50 samples (but keep in mind that this isn’t the time to
  drop to zero, it’s the time to drop to 1/*e* of the original value)
- 1kΩ: 500μs, 500 samples
- 10kΩ: 5ms, 5000 samples
- 100kΩ: 10ms
- 1MΩ: 100ms
- 10MΩ: 1s

Even with 10MΩ we should be able to observe the decay; if we charge
the resistor up to 100mV and let it sit for 100ms, it should decay by
about 98.02 millivolts over that time, about 365 counts if 1.1V is
full-scale.  Because this is 100000 samples, we can average a lot of
samples and get effectively 20 bits of precision, 1ppm of quantization
error from the ADC, so we should be able to detect sag even five
orders of magnitude less than that, about a teraohm.

Long before that, we’ll be mostly measuring the leakage bias currents
on the tristated input pins, but those should depend on the voltage
relative to ground at the DUT terminals (and the particular chip’s
characteristics, temperature, and supply voltage), not the voltage
relative to the other DUT terminal.

Noise will also be a problem.

Small resistances won’t be measurable in this way, because the decay
happens too fast to measure precisely, but we can measure them with a
voltage divider between our two 1kΩ resistors, for which purpose we
can safely apply the full 3.3V across the resistors as long as the DUT
doesn’t exceed 100mV.  This may be a reason to use expensive ±0.01%
resistors, because capacitance errror is irrelevant in this situation.

- 100Ω: 2.1V gives rise to a 100mV difference.
- 10Ω: 3.3V gives rise to a 16.4mV difference.
- 1Ω: 3.3V gives rise to a 1.65mV difference.
- 0.1Ω: 3.3V gives rise to a 165μV difference.  (Remember our ADC is
  270μV per count.)
- 0.01Ω: 3.3V gives rise to a 16.5μV difference.

Note that the four GPIO pins with their separate protection resistors
mean that we can make a four-wire Kelvin connection, so it’s plausible
to measure milliohm-level resistances without getting horked by
connection resistances.

In this mode we are flowing up to 1.65 mA, so we should probably stick
to a 5% duty cycle or less, using alternating polarity, to avoid
damaging delicate DUTs.  This means that in 100ms we only get 5ms of
samples.  Still, averaging over 5000 samples allows us to get an
effective resolution of 270μV/√5000 = 3.8μV.  This amounts to an error
floor of about 2.3 milliohms, which is probably the dominant source of
random error for resistances below about 10Ω.

### Voltage sources ###

If you hook the thing up to a battery, it will instantly charge the
capacitors and resist any attempt to discharge them or charge them
more fully.  Without the protection resistors and external clamp
diodes this would potentially destroy the GPIO pins — the STM32’s 3.3V
pins are rated for current injection of only up to ±5mA, corresponding
to 10V across the two protection resistors — but with them it should
be fine.

If the battery voltage is less than 3.3V, its equivalent internal
resistance should be measurable by the tricks in the previous section.
It’s probably okay to inject some current into a battery, especially
in the forward direction.  If you can forward-bias a 1.6V battery with
3.3V you should be able to get about 2.5 mA out of it, which ought to
provide a better estimate of internal resistance.

(I think you can switch the STM32 to use Vcc as its voltage reference
at, of course, a significant cost to precision.)

Larger batteries will exceed our 3.3V direct measurement capabilities,
and above about 4 volts, they’ll unavoidably discharge rapidly through
the input protection diodes.  However, this in itself provides us a
way to estimate their current capacity and voltage.  Suppose the
battery has its negative terminal on the left (GPIO1 and GPIO3 on the
above diagram) and is 6 volts, all our GPIOs are tristated, and the
voltage at that node is 3.3 volts.  The voltage on the right terminal,
connected to GPIO2 and GPIO4 through their protection resistors, is
9.3 volts.  The Vcc clamp diodes hold the voltages at GPIO2 and GPIO4
to about +4.0 volts, so there’s 5.3 volts across the two 1kΩ
protection resistors R2 and R4; the battery is supplying 10.6
milliamps, through those resistors, to the clamp diodes.

But where are those 10.6mA coming from on the other side of the
battery?  The GPIOs are tristated, so it’s coming out of the cap.  The
battery has instantly charged up the two capacitors to a *sum* of 6
volts, but now it’s discharging C1, the capacitor on its left
terminal, at 10.6 kilovolts per second.  So it would discharge it down
to 0 in about 300 μs, 300 samples of the ADC.

But it’s not a linear discharge; the voltage across R2 and R4 drops
from its initial 5.3 volts down to 2.0 volts, so by the end it’s only
discharging at 4.0mA and 4kV/s.

Seeing a discontinuity in the observed voltage followed by this rapid
discharge, we can conclude that an external voltage has been applied.

The end state here is that the grounding clamp diodes on the left
start to conduct as well, so we have -0.7 volts at GPIO1 and GPIO3,
+4.0 volts at GPIO2 and GPIO4, and voltage drops of 0.65 volts across
all the protection resistors, giving rise to 1.3mA of continuous
battery drain.

In this particular case, we could fight it.  If we set GPIO1 to output
and raise it to 3.3 volts, it will start charging C1 again, but I
think it will not reach a positive value.  [I tested more or less this
scenario in Falstad’s circuit.js][18], with a different diode model,
and that was the result. However, in the simulation, if we enlist the
heretofore read-only GPIO3, the two together can lift the battery’s
left terminal up above zero, to about 550mV, actually, sourcing about
2.75 mA each.

![(simulation result diagram)](./battery-test.png)

    $ 1 0.000005 1.4391916095149893 50 5 43 5e-11
    x -16 117 20 120 4 12 GPIO1
    x -14 241 22 244 4 12 GPIO3
    w 48 144 96 144 0
    g 96 320 96 336 0 0
    d 96 320 96 272 2 default
    w 48 272 96 272 0
    r 96 144 176 144 0 1000
    r 96 272 176 272 0 1000
    c 176 272 176 320 0 0.000001 0.5503234947593977 0.001
    g 176 320 176 336 0 0
    d 96 192 96 144 2 default
    g 96 192 96 208 0 0
    d 48 144 48 80 2 default
    R 48 80 48 48 0 0 40 3.3 0 0 0.5
    R 48 208 48 176 0 0 40 3.3 0 0 0.5
    d 48 272 48 208 2 default
    v 176 144 240 144 0 0 40 6 0 0 0.5
    d 368 272 368 208 2 default
    R 368 208 368 176 0 0 40 3.3 0 0 0.5
    d 368 144 368 80 2 default
    g 320 192 320 208 0 0
    d 320 192 320 144 2 default
    g 240 320 240 336 0 0
    c 240 272 240 320 0 0.000001 6.550296001698099 0.001
    w 240 144 240 272 0
    r 240 272 320 272 0 1000
    r 240 144 320 144 0 1000
    w 320 272 368 272 0
    d 320 320 320 272 2 default
    g 320 320 320 336 0 0
    w 320 144 368 144 0
    x 379 273 415 276 4 12 GPIO4
    x 381 147 417 150 4 12 GPIO2
    R 368 80 368 48 0 0 40 3.3 0 0 0.5
    r 176 144 176 272 0 0.01
    403 240 368 368 432 0 23_8_0_4355_10_0.003125_-1_2_23_3
    s -48 144 48 144 0 0 false
    R -48 144 -48 112 0 0 40 3.3 0 0 0.5
    403 48 368 176 432 0 8_8_0_4355_2.5_0.003125_-1_2_8_3
    R -48 272 -48 240 0 0 40 3.3 0 0 0.5
    s -48 272 48 272 0 0 false

[18]: https://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWc0AsBmAnGLA2SGEwUMAODNEBSSkdSgUwFowwAoADxGZ3DAHYQAJmphhtcIJABxAAoBJAPJtOzFEJQRBkwSjVrR0+QrSsA7rRLhdIDDyJrIrAOY2eaMbZBo0Pao4Amrl4ePIJ82iD+9ABmAIYArgA2AC5mFkLhQWGSjgBOQfbgfHbWIvCs+Z7ZRaGZZXCsAMY1GZL8bmJ+sPCQEDAIVO5oxCh8COQYfAIwcGwu7cEixV4+UFCsgZ7YklvWklFxSakuWxg7oZCWfhvphSiWJNT7MQkprABK6Y-p92vUKNQ0NAKH4oNAEB90sJLL8FqCAV5gX8wRDAr9qujLkJIi8jqwAG4tQo6ESlNYI3x-cE3HyWaq0oRY56HN6fBnQrw4Sxw8mApGg-o0rlWNQM77M17HRbgM7SjnXQLuESypUi7EHSXOdSAsQklaUxzNPXVPWqro9XogHDgqiCWyzHCkfAYMGzNJ64kI6p5bWtOV1cDlfIe6yqwr1RzmVX04XemliVXRzISvEuRMJhOra5RsSFBnhjhePgusIUDRIMI8fSSWSKFCFtAkCBEAQaARgKjiAy1hSCSFiwHC37wvkgqkQ-ILQoLb2utgAiim4UM9A5IRoAD6JA3kA36AGG7Au5mkDQogQG+YG8E183JgAzlxYdZnw41nFEvf6JDGK+n9yDBHRExy6CEF3SfNllXNZt23Xd9wvQRwR3boz0EC8rxvbcTE+X86UyPDfSAoEQJRVhH0IjF8LXagPy-VggA

Now, if you have both GPIO1 and GPIO3 driving nodes in output mode,
you don’t have a way to measure the voltage on the left side of the
battery, and on the right side you’re out of measurement range.  But
if you then switch GPIO3 back to input mode, you’ll be able to
peacefully watch the voltage leisurely fall down below ground, taking
in this case about 500μs (500 ADC samples).

The asymptotic level to which you can push this voltage tells you more
or less what the battery voltage is, though it’s vulnerable to errrors
from your estimate in the forward voltages of your clamp diodes, which
depends on their temperature.  In the simulation, for voltages of over
about 7 volts, you can’t win at all, though twiddling with component
values could fix this.

The speed at which the return below ground happens tells you something
about the circuit, like maybe the battery’s internal resistance, but
you might not be able to measure it with any precision.

### Charged capacitors ###

If your DUT is a charged capacitor, the resistors and shunt diodes
will fairly quickly discharge it to within a couple diode drops of
your power rails.  If you bang one end of it back and forth against
the power rails while leaving the other end open, for example by
tristating GPIO2, GPIO3, and GPIO4 and driving a square wave on GPIO1,
nothing bad will happen to a plain passive component like a resistor
or inductor; there’s nowhere for current to go through it.  But with a
capacitor, you might shove one end outside the power rails, activating
the clamping diodes and safely discharging it.

That won’t discharge it all the way; whatever charge remains on the
cap will create a persistent voltage difference between GPIO3 and
GPIO4, the sense pins connected to the opposite DUT pins.  That
difference will persist even as you bang one end of it back and forth,
though it will vary as charge sloshes back and forth between C1 and
C2.  Only leakage (in the capacitor, in your clamping diodes, and in
your microcontroller inputs) will diminish it persistently.

This is very different from the behavior of a voltage source or a
current source, which will produce a constantly increasing voltage
difference between the DUT terminals until limited by your clamping
diodes.

For capacitors that are similar in value to C1 and C2, this sloshing
action itself tells you how big the capacitor is.  If you drive
current in and out of GPIO1, that drives the voltage on C1 up and down
according to however much current the DUT isn’t stealing.  The
integral of that current, assuming GPIO2 and GPIO4 are tristated and
there are no ground faults, is measured fairly precisely by the
voltage on C2.  The voltage measured across the device under test, if
it’s a capacitor, will also be precisely proportional to that current
integral.  That is, the DUT and C2 form a capacitive voltage divider,
driven by GPIO1.

By this means we can measure the capacitance ratio of the DUT and C2
fairly precisely, within the limits of the ADC’s linearity; we can
also use GPIO1 and GPIO2 together to push the voltage relative to
ground up and down, so we can do the test at different distances away
from ground, in order to control the effects of integral nonlinearity
in the ADC.

Supposing that we are thus able to measure the capacitance ratio of C2
to the DUT for 100 milliseconds (100 kilosamples) with 9 bits of
precision per sample, we ought to be able to measure the ratio to
within about 6 ppm.

However, suppose the DUT is a lower-value capacitor, like 0.001μF.  A
change from -0.1 volts to +0.1 volts transfers an amount of charge
which will only affect the voltage on C2 by 0.2 millivolts, less than
1 count of our ADC.  Yes, by rattling the voltage on GPIO3 back and
forth by those 200 millivolts for 100 milliseconds we can average 100
kilosamples and maybe get about 8 bits of precision there, but that’s
only about two decimal places: a measurement floor of 4 picofarads.

(I’m starting to think that some diversity in the parts of the circuit
might be useful.  Right now swapping GPIO1/3 with GPIO2/4 gains you
nothing; the circuit is absolutely symmetrical.  (Except for component
value error.)  If C1 and C2 differed by an order of magnitude, though,
maybe one of them could be 0.33 μF while the other was 3.3μF, you
would get some extra precision in cases like this.)

For very-high-value capacitors you can measure them in the same way as
very-low-value resistors: you pull GPIO1 high, pull GPIO2 low, and
measure how slowly the DUT voltage grows.  Again in cases like this
the values of C1 and C2 are not important, so higher-precision
resistors could get you higher-precision capacitance measurements.

It might not be advisable, though, to send an arbitrarily large amount
of charge in one direction through something that looks like a short
circuit, even if the voltage never gets to 100mV.  If there’s an
electrolyte in the loop, you’re doing an arbitrarily large amount of
electrochemical work.

But maybe 100ms is safe.  Consider what happens if you send 1.7mA for
100ms through a large capacitor.  Its voltage changes by:

- if 1μF, 170 volts!  You have to stop and go into reverse.
- if 10μF, 17 volts; likewise.
- if 100μF, 1.7 volts; this is feasible, but you still get out of not
  just the 100-millivolt range I declared as safe above, but actually
  the 1.1V measurement range, so going into reverse is better.
- if 1000μF, 0.17 volts, so you should still stop and reverse course
  partway through.
- if 0.01F, 17 millivolts; this is still 63 counts on the ADC, but
  we’re starting to lose precision; even with 100’000 samples to run
  the regression on we still only have 14 bits of precision for the
  capacitor size, which is 100ppm error.

### Uncharged capacitors ###

This is just a special case of the charged-capacitor case.

### Diodes ###

At the ±100mV voltages I’m talking about above, a diode looks like a
leaky capacitor, with leakage increasing exponentially with forward
bias.

Even fairly small leakage currents should be detectable reliably.  The
[STM32F103C8 datasheet][19] says that within its regular voltage range
the input leakage current is ±1μA per pin, so probably ±2μA for two
pins, with 5pF of input capacitance.  2μA would be 2 V/s, or
200mV/100ms.  But, as I said before, we can calibrate the leakage
currents on these pins as a function of input voltage; by measuring
the leakage in an open-circuit condition we can estimate the leakage
when the diode is connected.  Moreover, we can measure the same diode
between 0V and 100mV, and between 1000mV and 1100mV.

[19]: https://www.st.com/resource/en/datasheet/stm32f103c8.pdf

Other sources of leakage, such as dielectric leakage in the
capacitors, clamp diode leakages, and leakage through the air, should
look an awful lot like input pin leakage, so probably the same
calibration takes care of them too.

If we assume as above that the smallest voltage change we can detect
over 100ms is 270nV, we should be able to measure diode leakage
currents with a precision of 2.7pA, if our calibration of the input
leakage current is perfect.  More realistically we probably won’t be
able to calibrate the input leakage current to better than 1nA, so our
precision will probably be more like 1nA.

A higher-voltage diode measurement mode, which the user would have to
enable explicitly in order to not damage more sensitive components
like tantalum caps, could trace the diodes’ E–I curves to ±3.3V, which
is enough for even many LEDs.

### Inductors ###

An inductor looks like a resistor that starts at ∞ and drops to 0 over
time and then goes negative.  In this circuit inductance will always
oscillate because there is no resistance within the LC loop.  Both the
second derivative of the curve and the period of the oscillation tell
us how big the inductance is.  For large inductances you want to use
the second derivative; for small inductances you want to use the
oscillation period.  A 100ppm error in the known capacitances of C1
and C2 will result in a 100ppm error in the inductance as calculated
from the frequency.

You could imagine that holding 100mV over a large inductor for a long
time could start it on some largish voltage excursions, but given the
1.65mA limit on what the GPIO pins can pump through the protection
resistors and given the clamp diodes, I don’t think it’ll be a
problem.

Perhaps more important in practice is the ESL of other components like
capacitors and resistors.
