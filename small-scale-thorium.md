It occurs to me that thorium “energy amplifier” reactors
ought to be able to scale down to the kilowatts range.
They are probably mostly suitable for stationary and
marine applications rather than transport because of the
high mass.

The basic plan ([Rubbia’s “accelerator-driven
system”][3]) is that you illuminate a thorium fuel pellet
with a proton or neutron beam, producing fission and
heat.  The heat from the thorium fuel pellet drives a
turbine, which drives a generator, which powers the
proton beam.

[3]: https://en.wikipedia.org/wiki/Energy_amplifier

[Thorium is about 9.6 ppm of the Earth’s crust][0] and is
considered a “lithophile”.  Chemically it’s similar to
the lanthanides, so it tends to be present in trace
quantities throughout most of the crust, rather than
being concentrated in rare deposits, though monazite
(historically the main ore of lanthanides, but now
disfavored due to its thorium content) is typically 1-10%
thoria by weight, but sometimes up to 30%.  (Thoria, in
turn, is 87.9% thorium.)

Monazite is easily separated by density from quartz in
placer deposits, where it commonly occurs with black
magnetite sands; it’s common for sand to contain about 1%
total heavy mineral (THM) content.  Sometimes a storm
will produce a new low-tide placer deposit by washing
away most of the quartz from a beach.  [Electrostatic
mineral separation][1] is a currently popular approach to
separating sands by conductivity after grading by size.
Magnetic separation removes magnetite sand.

[1]: https://en.wikipedia.org/wiki/Mineral_processing#Electrostatic_separation
[0]: https://en.wikipedia.org/wiki/Abundance_of_elements_in_Earth%27s_crust

Monazite also commonly occurs as a component of granite,
which is one source of indoor radon hazards, and
pegmatite, whose large crystals provide convenient
separation of minerals.  USGS Bulletin 1009-F says,
“Widely disseminated as an accessory mineral in granites
and gneisses.”  [Wikipedia claims][2] that *all* granite
contains thorium, as well as elevated levels of uranium,
and that the Conway granite of the White Mountains of New
England contains 56±6 ppm of thorium, while mafic rocks
such as gabbro (so-called “black granite”) contain much
less.

Thorium also commonly occurs in apatite and zircon
(ZrSiO₄, USGS says up to 13.1% Th; I guess this forms a
solid soution series with the isomorphous “thorite”,
ThSiO₄) and of course in pitchblende, which the USGS
Bulletin says can be up to 45.3% thorium.

[2]: https://en.wikipedia.org/wiki/Granite#Natural_radiation

Thorium-232 (99.98% of natural thorium) is the relevant
fuel, which transmutes (by way of thorium-233 with a
22-minute half-life, then protactinium-233 with a 27-day
half-life) into uranium-233.  The uranium is fissile upon
absorbing a second (thermal or fast) neutron (but has a
half-life of 160k years otherwise), releasing 197.9 MeV,
which works out to 81.95TJ/kg.

This gives the Conway granite a fuel value of 4.6GJ/kg,
100× the 45.6MJ/kg of diesel fuel, which seems pretty
feasible.  Moreover, it gives the earth’s crust at 9.6ppm
an average fuel value of 790MJ/kg.  Typical monazite sand
containing 5% thorium would be 4TJ/kg, heavy black sand
containing 10% monazite would be 400GJ/kg, and sand
consisting of 1% heavy black sand would be 4GJ/kg.  A
10kW reactor would consume 2.5mg/s of such 99%-quartz
sand and 2.5μg/s of monazite.

These numbers don’t account for the energy cost of
removing non-thorium elements so you don’t irradiate
them, which depends a great deal on the process.

The uranium is rather unfortunate, because it can sustain
a chain reaction and is therefore a proliferation risk.
India’s Shakti V was a U-233 bomb.  So is the 27-day
half-life of the protactinium intermediate.

The original thorium molten-salt reactor used thorium
fluoride dissolved in the molten salt, rather than a fuel
pellet, eliminating the risk of structural damage to the
fuel.

Rubbia’s original proposal was to use a cyclotron of
0.8–1 GeV directly as the particle-beam source and molten
lead as the coolant, but nowadays linear accelerators are
probably adequate.

The neutron beam could be produced by proton spallation
from a heavy element such as mercury.  Mercury in
particular has the advantage that it won’t sustain
structural damage from prolonged spallation and
transmutation because, being liquid, it doesn’t a have
structure to damage.  Molten lead would have the same
advantage, is easier to source, and carries less toxicity
stigma.  (Maybe all of this is unnecessary because you’ll
get the same amount of spallation from the thorium
itself.)

Lawrence’s first crude proton [cyclotron][4] reached
80keV, and within a decade he was hitting 16 MeV in his
60-inch cyclotron, which produced neptunium the next year
and plutonium the year after that.  This suggests that
spallation requires higher beam energies than 16MeV.

[4]: https://en.wikipedia.org/wiki/Cyclotron

Synchrocyclotrons, which reduce the frequency to account
for relativistic effects, should be trivial with modern
electronics, and can reach 350MeV, but only at very low
particle fluxes.  Isochronous cyclotrons can’t compete,
but synchrotrons can.

Modern electronics seem like they ought to help with
building a linear accelerator, but most of the job is
still probably microwave tubes.  A proton weighs about 94
MeV, so although a lumped-element linac can get the
protons started on their journey to GeV energies, they
have to acquire most of their energy from a
traveling-wave accelerator.

The lumped-element approximation is potentially valid
until our particles are traveling at a significant
fraction of the speed of light, at which point we really
have to worry about propagation delays in wires.  To
reach 94 MeV (at which point v²/¢² = ½ and so v = c√2 =
424Mm/s) vwith a lumped-element accelerator driven by a
2kV microwave oven transformer and magnetron, the protons
have to accelerate across 47000 2kV gaps.  This seems
like an annoyingly large number of discrete
lumped-element electrodes to fabricate and wire up, so
maybe it would be worthwhile to switch to radiofrequency
cavities much earlier, perhaps with a traveling-wave
field with a much slower group velocity imparted by a
helical electrode.  Or you could start with an initial
tiny cyclotron.

I believe a radiofrequency cavity has a maximum field of
the peak voltage over 1/τ wavelengths.  At 2.4GHz the
wavelength is 125mm, so 1/τ wavelengths is 20mm.  If you
have a bundle of protons moving along with a 1kV-peak
traveling wave and continuously gaining energy from it,
it will gain 1keV every 20mm, so only 50keV/m.  So you
really want much higher field strengths than this.  Using
higher frequencies would also help; for the same peak
voltage you’d get 500keV/m at 24GHz.

One of the first accelerators to reach the requisite
energies was [the UC Radiation Laboratory’s Bevatron][5]
in 01954, which was a synchrotron; construction began
in 01948.  Its injector was a 10-Mev linac; Lofgren
published a paper on it in 01950 in Science, “The Proton
Synchrotron”, volume 111, p. 295.  They considered a
“usable” intensity to be nearly a billion protons per
pulse, and the injected beam at the inflector was about
0.2mA.

[5]: https://www.osti.gov/biblio/877349 "UCRL-3369, Experiences with the Bevatron, Lofgren, April 5, 01956"

What beam current do we need?  Suppose we are targeting
10kW of output electrical power from our ADSR, perhaps
powering a steam turbine with 40kW of thermal power.  At
197.9MeV per fission that’s about 1.3e15 fissions per
second.  If we suppose that each proton whacking the fuel
pellet produces about 40 neutrons through spallation and
we can achieve 5% neutron efficiency, we need 6.3e14
protons per second, which works out to a beam current of
about 0.1mA, which seems to be in a practical range,
though whether you can build an 0.1mA GeV accelerator
efficiently enough to run on under 10kW seems like a
probably unexplored question.

Well, no.  If you’re putting in 1GeV per proton and
getting two 197.9MeV fissions out of it, you are losing
60% of your energy even right at that step, even with a
100% efficient accelerator made out of unicorn farts.  So
either your neutron efficiency has to be a lot more than
15%, you need a lot more than 40 spallation neutrons per
proton, or you need a different approach entirely.

The fact that the 16MeV 60-inch cyclotron succeeded at
nuclear transmutation of uranium into neptunium and
plutonium by 01941 suggests that the spallation phase is
not entirely necessary; an accelerator in the 16-20 MeV
range could conceivably give us our 40kW thermal with
beam currents as low as 0.2mA at 100% proton capture
efficiency, but the actual proton capture efficiency
might be terrible.

A different problem is that the neutron capture cross
section of thorium is about 1.2 barns for neutrons of
around 100keV, and less at higher and lower energies;
this means that you need about 8e27 atoms of thorium per
square meter to capture 1/e of your neutrons.  This works
out to 3200kg/m²; at 11.725g/cc that works out to 270mm
thickness of thorium to get reasonable neutron economy.

Now, I don’t know much about neutron spallation.  If you
could focus your proton beam on a small spot and launch
lots of spallation neutrons in the same direction, down
the length of a thorium rod, that would be fine; a 270mm
long rod of thorium or thoria or whatever that was, say,
4mm in diameter is totally reasonable.  But I suspect
that the neutrons go in every direction, so you need a
270mm-radius *sphere*, which is a totally different level
of headache.

It would be great if you could burn natural monazite, but
I suspect its lanthanide content would contain too many
neutron poisons.
