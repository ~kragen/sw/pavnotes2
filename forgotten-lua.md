I’ve just gotten back into programming in Lua after using it hardly at
all for several years.  Some aspects of the language are easy to
remember, but others have fallen out of my memory.  Because I like
making lists, here’s a list.

- Because by default every file loads into the same namespace, the
  standard way to implement a loadable Lua module is to create a
  single table and then stick your functions into it.

        utra = {}

        -- Literal.
        function utra.lit(s) ... end

- You load a file of code with `loadfile`, not `require`, `import`, or
  even `load`.
- `loadfile` (and `load`) return their error messages, so if you just
  call them at the prompt you don’t see them:
  
        > loadfile('fooxx')
        > 

    You need to print the result if you care if it succeeded:

        > =loadfile('fooxx')
        nil     cannot open fooxx: No such file or directory
- But `loadfile` doesn’t actually run the code; it just returns a
  function you can invoke, and so you have to do that:

        > =loadfile('microtrans.lua')
        function: 0x404a7358
        > =utra
        nil
        > loadfile('microtrans.lua')()
        > =utra
        table: 0x404a9e
        > =utra.lit('a')
        table: 0x404a9d70

- There’s no built-in recursive structure printing; this is maybe the
  most obnoxious thing about the Lua REPL.

Apparently you can get LuaJIT into an infinite loop that takes many
seconds to break out of with ^C, but it does eventually break out.
