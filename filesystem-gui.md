[Wercam][1] is a protocol for displaying a GUI.  In one direction it
sends keyboard and mouse events (as lines of text, using
URL-encoding); in the other direction it sends draw commands (also as
lines of text, using URL-encoding), which include an attached file
descriptor to a shared-memory buffer.  These are sent over a
Unix-domain `SOCK_SEQPACKET` socket.

[1]: https://gitlab.com/kragen/bubbleos/blob/master/wercam/README.md

It occurred to me that there’s a much simpler way to do this that is
probably adequately efficient on Linux, and can support more
composability.  The keyboard and mouse events get appended to a
logfile, which is periodically rotated in the way system log files are
(renaming it and creating a fresh, empty file), and the screen buffer
is just a file full of pixels in the same directory, which gets
replaced when the application wants to update its display.  You could
implement this approach with the traditional Unix API, which would be
slow and inefficient, but filesystem event notification APIs make it
fast and efficient.

Each open window has its own subdirectory in the compositor’s
directory.  The compositor uses the same protocol to communicate with
a display server.

This design has appealing similarities to the Plan9 window system 8½,
and it shares many of its advantages, but it has a very important
difference.  In 8½, the window system was a *file server*, accessed
over the Plan9 filesystem protocol 9P, but here the files we’re
talking about are stored in any ordinary filesystem.

(See also file `microui-recording.md` for some notes on a different
file format.)

Advantages of this approach over traditional ways to display a GUI
------------------------------------------------------------------

1. It’s easier to debug, because you can use commands like `tail` and
   `grep` on the history of filesystem events, and you can see the
   image that’s supposed to be displayed in a window just by reading
   the screen file.  (However, I think that for performance the screen
   file should be in a custom format that isn’t compatible with any
   existing software but does easily map onto current framebuffer
   hardware.)
2. Simple compositional applications like screenshotting, screen
   recording, keyboard macro recording and playback, screen
   magnification, and screen sharing can be written very simply, as
   long as they have the necessary filesystem access.
3. Existing filesystem permissions are expressive enough to support
   many common security policies.  Uncommon security policies can be
   implemented with simple proxy daemons.
4. Applications can be hacked together as shell scripts.
5. The window manager can run as an application in a window.
6. Access to remote applications can be provided simply and
   efficiently by synchronizing the relevant changes in the
   filesystem.  In theory you could do this simply by putting the
   directory representing a window on a networked fileserver, but
   existing networked failsystems often have many deficiencies that
   make them unsuitable.  But this can be handled by a small, simple
   proxy daemon connected over ssh or whatever other transport is
   suitable.
7. It’s very, very simple.
8. Things like opening multiple windows from the same application are
   trivial.
9. It can be implemented efficiently; if the application window is the
   same size and pixel format as the hardware screen, the window
   manager can point the display hardware at the memory pages the
   application wrote, enabling fully-zero-copy performance.

Filesystem event notification APIs
----------------------------------

Efficient, reliable notification of filesystem change events is
the key thing that makes this approach workable.

Linux supports asynchronous notification of firesystem events using an
API called [`inotify`][0], which supplanted `dnotify` and has been
more recently supplemented by a more general API called `fanotify`.
The `tail -f` command uses `inotify` to avoid periodic polling of the
filesystem to see if new lines have been added to the logfile you’re
tailing.  Generally `inotify` only notifies you that a file has
changed; you have to issue more system calls to actually get the new
data from the file.  Here’s how `tail` uses it (my prompt is `:
dev3;`):

    : dev3; touch a-log-file
    : dev3; strace tail -f a-log-file &
    ...
    openat(AT_FDCWD, "a-log-file", O_RDONLY) = 3
    newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=0, ...}, AT_EMPTY_PATH) = 0
    lseek(3, 0, SEEK_CUR)                   = 0
    lseek(3, 0, SEEK_END)                   = 0
    lseek(3, 0, SEEK_SET)                   = 0
    read(3, "", 8192)                       = 0
    newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=0, ...}, AT_EMPTY_PATH) = 0
    fstatfs(3, {f_type=EXT2_SUPER_MAGIC, f_bsize=4096, f_blocks=51622359, f_bfree=39528378, f_bavail=36887892, f_files=13189120, f_ffree=12662393, f_fsid={val=[0xb2fab1aa, 0xb2e9a4dd]}, f_namelen=255, f_frsize=4096, f_flags=ST_VALID|ST_RELATIME}) = 0
    newfstatat(1, "", {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x3), ...}, AT_EMPTY_PATH) = 0
    newfstatat(AT_FDCWD, "a-log-file", {st_mode=S_IFREG|0644, st_size=0, ...}, AT_SYMLINK_NOFOLLOW) = 0
    inotify_init()                          = 4

That sets up file descriptor 4 as an `inotify` event queue.

    inotify_add_watch(4, "a-log-file", IN_MODIFY) = 1
    newfstatat(AT_FDCWD, "a-log-file", {st_mode=S_IFREG|0644, st_size=0, ...}, 0) = 0
    newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=0, ...}, AT_EMPTY_PATH) = 0
    poll([{fd=4, events=POLLIN}], 1, -1
    
At this point the `poll` call hangs until `inotify` has a message for
`tail`, and I start appending to the log file:

    cat >> a-log-file 
    hello
    )    = 1 ([{fd=4, revents=POLLIN}])
    read(4, "\1\0\0\0\2\0\0\0\0\0\0\0\0\0\0\0", 27) = 16
    newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=6, ...}, AT_EMPTY_PATH) = 0
    read(3, "hello\n", 8192)                = 6
    newfstatat(1, "", {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x3), ...}, AT_EMPTY_PATH) = 0
    write(1, "hello\n", 6hello
    )                  = 6
    read(3, "", 8192)                       = 0
    poll([{fd=4, events=POLLIN}], 1, -1stuff
    )    = 1 ([{fd=4, revents=POLLIN}])
    read(4, "\1\0\0\0\2\0\0\0\0\0\0\0\0\0\0\0", 27) = 16
    newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=12, ...}, AT_EMPTY_PATH) = 0
    read(3, "stuff\n", 8192)                = 6
    write(1, "stuff\n", 6stuff
    )                  = 6
    read(3, "", 8192)                       = 0
    poll([{fd=4, events=POLLIN}], 1, -1bye
    )    = 1 ([{fd=4, revents=POLLIN}])
    read(4, "\1\0\0\0\2\0\0\0\0\0\0\0\0\0\0\0", 27) = 16
    newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=16, ...}, AT_EMPTY_PATH) = 0
    read(3, "bye\n", 8192)                  = 4
    write(1, "bye\n", 4bye
    )                    = 4
    read(3, "", 8192)                       = 0
    poll([{fd=4, events=POLLIN}], 1, -1: dev3; kill %?strace
    : dev3; strace: Process 26532 detached
     <detached ...>

    [2]-  Terminated              strace tail -f a-log-file

So basically each message needs two or three system calls: optionally
a `poll` or `select` or epoll notification to tell you `inotify` wants
to notify you, a `read` on the `inotify` file descriptor to find out
what file has changed, and then a `read` on the file to get the actual
data.  `tail` includes a `newfstatat` call (I think this is what
recent glibc uses to implement `fstatat`) as well, but this isn’t
necessary.  According to the not-always-reliable `time` command, all
this takes `tail` on the order of 100 microseconds of CPU (half user,
half kernel) per event delivered.  That’s a promising number for this
use.  Appending a hundred thousand messages to the file as a ping-pong
latency test took half a second, suggesting that the real overhead per
message is closer to 5 microseconds:

    : dev3; ulimit -f 390; time tail -1f a-log-file >> a-log-file
    File size limit exceeded

    real    0m0.498s
    user    0m0.045s
    sys     0m0.453s
    : dev3; exit

`inotify` handles event queue overflow by dropping events when they
exceed `/proc/sys/fs/inotify/max_queued_events` (16384 on my system),
and enqueueing an `IN_Q_OVERFLOW` event, at which point I guess your
application has to fall back to polling everything.  I think `epoll`,
introduced in Linux 2.5.45, handles event queue overflow (as I
understand it, by combining duplicate events, so that there is an
upper bound on the event queue size) more gracefully than `inotify`,
introduced a few years later in Linux 2.6.13, but that’s a different
rathole.

The fact that `inotify` doesn’t deliver the actual changed data, but
only a small 16-byte dirty message, helps it handle overload
conditions gracefully; if the file gets replaced ten times before the
application reads it once and gets around to rechecking the `inotify`
message queue, the application will never see the intermediate states
of the file.  And `inotify` does [do some coalescing of its own][1]:

> If successive output inotify events produced on the inotify file
> descriptor are identical (same `wd`, `mask`, `cookie`, and `name`),
> then they are coalesced into a single event if the older event has
> not yet been read (but see BUGS).  This reduces the amount of kernel
> memory required for the event queue, but also means that an
> application can’t use inotify to reliably count file events.
>
> The events returned by reading from an inotify file descriptor form
> an ordered queue.  Thus, for example, it is guaranteed that when
> renaming from one directory to another, events will be produced in
> the correct order on the inotify file descriptor.

[0]: https://en.wikipedia.org/wiki/Inotify
[1]: https://man7.org/linux/man-pages/man7/inotify.7.html

However, `inotify` generally does not work over networked filesystem
protocols; NFSv2 and NFSv3 in particular are hopeless, [NFSv4 is
almost as bad][9], and [SMB3.1.1 might support it someday soon][4].
Historically, SGI developed a thing called [FAM, the File Activity
Monitor][3], which provided file change notifications over Sun RPC and
could work in parallel with NFS.  It seems to have been abandoned,
replaced by a reimplementation of its API in GNOME called gamin, which
[doesn’t support the networked part][5], [even now][7], maybe
[motivated by security.][6] Fixing this was [a big topic at LSFMM last
year.][8]

[3]: https://web.archive.org/web/20170709121125/http://oss.sgi.com/projects/fam/faq.html
[4]: https://sambaxp.org/fileadmin/user_upload/sambaxp2023-Slides/French_sXP23_Accessing_files_remotely.pdf "(currently requires a private SMB3.1.1 specific ioctl)"
[5]: https://web.archive.org/web/20150323183942/https://people.gnome.org/~veillard/gamin/differences.html
[6]: https://web.archive.org/web/20150323193142/https://people.gnome.org/~veillard/gamin/overview.html
[7]: https://access.redhat.com/documentation/es-es/red_hat_enterprise_linux/6/html/deployment_guide/s2-sysinfo-filesystems-system_gamin
[8]: https://lwn.net/Articles/896055/
[9]: https://nfsv4.linux-nfs.narkive.com/vJgyTOl2/nfs-and-inotify

Window directory layout
-----------------------

The filesystem directory for a window `w` is divided into two
subdirectories, `input` (for communication from the window manager to
the application) and `output` (for the opposite direction).  The
complete layout is as follows:

- `w`, the arbitrarily-named root of the window directory, containing:
    - `input`, the directory the window manager writes and the
      application reads, containing:
        - `event.log`, a text file with a sequence of timestamped
          event messages; see below about the event log format.
        - `size`, a text file containing the window’s current width
          and height, in ASCII decimal, separated by a space and
          followed by a newline; for example, “640 480\n”.
    - `output`, the directory the application writes and the window
      manager reads, containing:
        - `fb`, the framebuffer file to which the application writes
          the contents of the window to display; when the window
          manager notices that the file has changed, it updates the
          window.  Normally, to get double-buffering, the application
          creates new frames in another file named something like
          `.tmp.fb` and then renames them to `fb`, deleting the old
          framebuffer file; such double-buffering is obligatory if
          you’re changing the framebuffer size.  For a variety of
          reasons, the image in this file may be of a different size
          than that specified in `w/input/size`.  See below about the
          framebuffer file format.
        - `title`, containing the desired title of the window, in
          UTF-8.  The application can change this at any time.

In summary:

- `w/input/event.log`: append-only input event log
- `w/input/size`: window size and shape
- `w/output/fb`: window pixel contents
- `w/output/title`: window title

I feel like the proper sequence is:

1. Application creates `w`, `w/input`, `w/output`, `w/output/title`,
   and `w/output/fb`, the last of which indicates the size it is
   requesting.
2. Window server creates `w/input/event.log` and `w/input/size`, which
   tells the application the size of the window it actually got, which
   may be different; for example, the window server may just be a dumb
   display server.
3. Application draws future frames with the new size.

I’m not quite sure how window resizing should work.  Should resize
requests be implicit in drawing frames of the wrong size?  What if the
application draws two frames of the same size at startup, maybe while
the window system isn’t running?

At some point I might add more files to these directories to support
things like copy and paste, application-requested full-screen mode,
and mouselook, so it’s important for both the application and the
window manager to tolerate additional, unrecognized files.

Window system directory layout
------------------------------

Suppose the window system is at path `ws`.  We set up the hierarchy as
follows:

- `ws`: the root of the window system, containing:
    - `windows`: a subdirectory with one subdirectory for each window,
      created with an arbitrary name.  On Unix [mkdir(2) is atomic][2]
      except on NFS, so it is safe for applications to create whatever
      directory they want and then, if successful, use it.  Each
      subdirectory is laid out as explained above.
    - `size`: the size of the screen, in the same format as
      `w/input/size` above.

So, if you have two windows named `term` and `term.1`, you might have
the following nine files present:

- `ws/size`
- `ws/windows/term/input/event.log`
- `ws/windows/term/input/size`
- `ws/windows/term/output/title`
- `ws/windows/term/output/fb`
- `ws/windows/term.1/input/event.log`
- `ws/windows/term.1/input/size`
- `ws/windows/term.1/output/title`
- `ws/windows/term.1/output/fb`

[2]: https://rcrowley.org/2010/01/06/things-unix-can-do-atomically.html

Framebuffer file format
-----------------------

Data that gets sent to the screen can be voluminous enough to tax the
computer’s total memory bandwidth; a 32-bpp 120Hz 3840×2160 “4K UHDTV”
display is 4 gigabytes per second, 32 gigabits per second.  Typical
memcpy bandwidths on current systems are under 20 gigabytes per
second.  So it’s important to reduce copying and per-pixel computation
as far as possible.

The format for the framebuffer file is intended to preserve not just
pixel alignment and cache-line alignment but also page alignment.  So
the pixel data starts at the beginning of the framebuffer file and is
uncompressed.

The pixel data is in BGRA8888 format, where each pixel is a byte of
blue, followed by a byte of green, followed by a byte of red, followed
by a byte that is nominally alpha but normally ignored by the display
hardware.  On little-endian hardware this corresponds to ARGB32.  This
pixel format is chosen because most common display hardware today uses
it natively, so no pixel format conversion is required, and it’s more
expressive than the formats supported by almost all display hardware.
For the same reason, the pixels are in conventional raster order,
starting with the pixel in the upper-left-hand corner, followed by the
pixel to its right, and so on to the end of the first scan line, which
is immediately followed by the first pixel of the second scan line,
and so on through the rest of the scan lines in the image.

Uncompressed BGRA8888 data facilitates partial screen updates; see
file `sprite-command.md` for one application of this.

At the end of the screen file, immediately following the last pixel,
there are 12 bytes of trailer data:

- width of each scan line in pixels, a 4-byte little-endian unsigned integer;
- height of the image in scan lines, which is to say in pixels, also a
  4-byte little-endian unsigned integer;
- the four-byte magic number '\xd9\x1aWC', which is to say, d9 1a 57
  43, which is the integer 1129781977 as a 4-byte little-endian
  unsigned integer.  I picked it from /dev/urandom.

I might add more fields to the beginning of the trailer later.

It would perhaps be more elegant to put the width and height somewhere
besides at the end of the file, since this means you need to look at
the end of the file to find out how to interpret its beginning.  They
are in the same file because the standard Unix filesystem provides
atomic renaming operations but does not provide any atomic multi-file
operations, and interpreting the pixel data with the wrong width would
result in an incorrect display, generally bizarrely so.  Even non-Unix
filesystems can typically guarantee that reading a file won’t mix data
from two different files together.

I might try mmapping the file to reduce the number of memory copies,
but this exposes the window manager to segfaults if the application
truncates its framebuffer file while the window manager is reading it.
This is forbidden application behavior, though, and installing a
segfault handler is not impossible, just extra complexity.

The event log
-------------

Keyboard and mouse events get appended to `input/event.log` in a
simple textual format.

The window system “rotates” this file when it gets too large by
deleting it and creating a new one.  At least on Unix, the application
can continue reading the deleted file until the end, but then once it
sees an EOF on the deleted file and some events in the newly created
file, it can safely close the deleted file.

In Wercam I sent each event in a separate packet, and there were five
types of messages: `window open`, which functioned to provide the
size, acknowledge draw commands, and inform the client of resizes;
`window closed`; `mouse`; `key`; and `error`.  Event boundaries were
packet boundaries.  Instead, I will use newlines for event boundaries.

Here are the example events from the Wercam README:

    window open 13 256 64
    mouse 131 22 0
    key up 56
    key down 56 B
    error Nothing can possibly go wrong. go wrong. go wrong.
    window closed

I think the thinking behind acknowledging draw commands was to enable
the app to reuse the buffer it had already sent.

There were no timestamps, and I think that including timestamps would
be a good idea.  An `event.log` file with Unix epoch timestamps might
look like this:

    1703847499.5849507 mouse 131 22 0
    1703847500.364472 key up 56
    1703847501.296242 key down 56 B
    1703847502.2394516 error Nothing can possibly go wrong. go wrong. go wrong.
    1703847503.4657748 window closed

This is about 40 bytes per event, and mouse events can conceivably
happen at about a kilohertz, which would be about 40 kilobytes per
second.  A typical application window framebuffer might be 1–32
megabytes; it’s okay if the event log is small compared to that, but
not if it’s larger.  Let’s shoot for a max of 524288 bytes: the window
server rotates the event log whenever it gets bigger than that.  This
is potentially only 13 seconds!  It’s easy to imagine an application
hanging for that time (because it’s running on a thrashing machine)
and losing input events.

What if we don’t try to guarantee input event delivery?  If you have
an application that doesn’t get input events for a long time, maybe
because of a temporary network failure, it maybe isn’t that important
to buffer up all the stray passphrases and mouse movements its frozen
window happens to collect over the next day or however long.  But in
that case probably moving the `error` and `window closed` events out
of the event log would be a good idea; those probably do need
guaranteed delivery.

(This also points out that you need some way to identify unresponsive
applications, since, lacking a network socket, the OS won’t tell you
if they crashed.)

Maybe it would be best to have both a minimum size and a minimum age
after which the event-log file gets rotated.  A single rotation won’t
normally result in any lost events, since the application still has
the old event log open and will read it to the end before reading the
new log.  But if there are *two* rotations before the application
wakes back up, it will miss events.  If we set the minimum age at five
minutes, then an application that hangs for less than five minutes
will be guaranteed not to miss events.  But the event log might bloat
up to 11 megabytes.

In a more typical case though, your mouse isn’t 1kHz and isn’t
constantly moving when the application window is unresponsive.  Your
mouse is probably 125Hz and you’re probably only moving it at most 10%
of the time.  524288 bytes is about 20 minutes of mouse motion under
those conditions.

Compositing with readv(2)
-------------------------

Suppose you have an `output/fb` framebuffer file in the format
described above, and as the compositor you want to put its pixels into
a frame that you are assembling to send to the video hardware.  (Let’s
assume that you’re not doing the compositing in the video hardware
itself using texture mapping, because in that case the problem becomes
trivial.)  And let’s say some of the pixels from the window you’re
reading are opaque, so you don’t need to blend them into anything
else; you want to copy them directly into the framebuffer (though
maybe you’ll alpha-composite something else onto them afterwards).

I claim you can do this in a zero-copy fashion with `readv`.  For each
*visible* portion of each scan line in the window of more than, say, 8
pixels, you create a `struct iovec` that points into the hardware
framebuffer.  These are interleaved with “dummy iovecs” which all
point at the same single-scan-line garbage buffer, which accepts all
the pixel data that doesn’t get copied into the hardware framebuffer.
Then a single `readv` call composites all of that pixel data into the
window.

[Some simple tests I did years ago][39] suggest that a read(2) call in
Linux takes about as long as copying 1800 bytes of data into
userspace.  So when the parts of the window’s pixel data that are not
used are more than about 1800 bytes, which is 450 pixels, it will be
cheaper to make multiple `read(2)` or `readv(2)` calls instead of a
single big `readv` copying lots of data into nowhere useful.

[39]: http://canonical.org/~kragen/sw/dev3/syscallovh.c

An *a la carte* option
----------------------

I was thinking about what the simplest, most ready-to-hand subset of
the above would be, and it occurred to me that you could start with a
program that just views a single image file and refreshes the window when
the file changes, also attempting to resize the window if necessary:

    liveview [{--feedback|-o} foo/] [-t titlefile] [--max-logsize 512K] img.png

The `--feedback dirpath` or `-o dirpath` part would specify where to
write the event log and the window size.  The `-t titlefile` part
would tell you where to get the window title.  The `--max-logsize`
argument specifies how big to let the logfile get before making a new
one.

Of course it would be more efficient to use the novel image format
described earlier, but supporting one or more standard image formats
would be a lot more convenient.

The name “liveview” is terrible, though, because it’s already used for
[a Sony camera app][10], a [way to incrementally update an HTML
view][11] from a long-running server-side web app [in Elixir][12], a
[Microsoft Windows app][13] for displaying streams from public
webcams, [some kind of photography app that doesn’t run on a
camera][14], a [brand of surveillance camera][15], and a [cellphone
peripheral for displaying notifications][16] on a 128×128 display.

Maybe a name made from synonyms like vigil, observe, ward, attend,
guard, follow, see, show, picture, image, pic, visualize, envision,
figure.  Of course as single words all of these are used:
[envision][17], [envision][18], [envision][20], [figure][21],
[figure][22], [visualize][23], [pic][24], [image][25], [picture][26],
etc.  Combinations of two or more might work, though.  `sorted((a+b
for a in w for b in w), key=len)` suggests ‘seesee’, ‘seepic’,
‘picsee’, ‘picpic’, ‘wardsee’, ‘wardpic’, ‘seeward’, ‘seeshow’,
‘showsee’, ‘showpic’, ‘picward’, ‘picshow’, ‘vigilsee’, ‘vigilpic’,
‘wardward’, etc.  [Seesee is taken][28].  seepic seems to be unused as
a project name but a common phrase.  [picsee][29] [is][30]
[common][31].  [Picpic is taken][32].  Wardsee, wardpic, seeshow,
showsee, picward, vigilsee, vigilpic, and picvigil are unused.
[Seeward][33] and [wardward][36] are two people.  [Showpic is
taken][34].  So is [PicShow][35], of course.

Of these I think I like `picvigil` best, in part because [“vigil” is
generally not accepted as a verb][37].

[10]: https://www.sony-latin.com/es/electronics/support/e-mount-body-nex-5-series/nex-5r/articles/00077692
[37]: https://www.grammarphobia.com/blog/2016/06/vigil.html
[36]: https://github.com/wardward
[35]: https://github.com/mikemedina/PicShow
[34]: https://github.com/ancientlore/showpic
[33]: https://github.com/seeward
[32]: https://github.com/MatrixAges/picpic
[31]: https://github.com/jessicaowensby/picsee
[30]: https://github.com/dlochrie/picsee
[29]: https://github.com/kzhiquan/Picsee
[28]: https://github.com/seesee
[26]: https://github.com/spatie/image
[25]: https://github.com/spatie/image
[24]: https://en.wikipedia.org/wiki/PIC_(markup_language)
[23]: https://github.com/TIBCOSoftware/js-visualize
[22]: https://github.com/fig-package-manager/fig
[21]: https://github.com/microsoft/figure
[20]: https://github.com/rartino/ENVISIoN
[18]: https://github.com/sitevision/envision
[17]: https://github.com/envision
[11]: https://hexdocs.pm/phoenix_live_view/Phoenix.LiveView.html
[12]: https://github.com/phoenixframework/phoenix_live_view
[13]: https://liveview.softonic.com/?ex=RAMP-1640.1
[14]: https://es.packshot-creator.com/foto-vista-tiempo-real/
[15]: https://www.lvt.com/
[16]: https://es.wikipedia.org/wiki/Sony_Ericsson_LiveView

I kind of like `ywatch` better, which is shorter and alliterates with
my existing `yv` program for viewing an image, but [that’s taken
too][38].  `yvigil` is available, though, and sounds cooler, and avoid
the misleading analogy with Linux’s standard watch(1).

[38]: https://github.com/prayzjomba/ywatch

<link rel="stylesheet" href="http://canonical.org/~kragen/style.css" />

<script src="http://canonical.org/~kragen/sw/addtoc.js">
</script>
