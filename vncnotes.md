Well, I am typing this on HeliBoard on my phone.  I am using AVNC to
remote-control my laptop, which is running Debian with tightvncserver.

Originally I was editing this file in ex, because Heliboard doesn’t
have control and alt keys.  But it turns out AVNC, the client I
installed on the phone from F-Droid, has a little overlay to bring up
with Ctrl, Alt, Tab, arrow keys, etc., on it.  So I’m typing this in
Emacs now.  It was pretty hard to get the Emacs window sized for the
screen, because mouse drags are hard to manage, and the drag targets
for resizing windows in my window manager are tiny.

The left and right drag on the spacebar in HeliBoard to move the text
cursor works great.  And I can zoom in and out on the screen with
two-finger pinch gestures.  But in the standard
send-keystrokes-to+server mode, HeliBoard’s autocorrection is
completely inoperative.

There is, however, a hidden feature where by dragging the Esc
etc. buttons to the left, you get a space to compose text in before
sending it to the VNC server.  This makes it possible to use the
autocorrect on the cellphone terminal before submitting your card to
Emacs.  This is a huge help!

I haven’t tried using vi in this fashion.  However, having Tab and
Ctrl should make the usual Unix command line interface fairly usable.
Too bad this local editing mode doesn’t have a command-line history!

I keep trying to scroll the Emacs window with a finger, but
unfortunately that scrolls the entire VNC viewer window.  What I kind
of want is a VNC server desktop that fits comfortably onto the
cellphone screen.

The VNC client also has a “touchpad” mode for the touchscreen, and
that does permit more precise mouse usage; it makes it practical to
resize windows with the simulated mouse, with some difficulty.  It
keeps the mouse pointer in the center of the screen when you’re zoomed
in, much as I’ve been considering for quasimodal mouse/keyboard UI
experiments.  I find I find this a little frustrating, because often
the window of text gets partway scrolled off the cellphone’s display,
but I’m not sure whether I would prefer hysteresis (I think so) or the
opposite.  It seems to take no notice of where you are tapping the
display, so I’m not sure how to do a middle or right click.  (Aha,
long press is right click!).  Part of the frustration is
touchscreen-specific: a one-finger drag does scroll, but in the wrong
direction.

This VNC client doesn’t seem to have file transfers, which I am pretty
sure got added to the de facto protocol at some point.  Being able to
upload screenshots as I am writing this would be very convenient.

I wonder if I can run Godot inside tightvncserver?  Nope, Godot
requires OpenGL in the X server, and specifically OpenGL 3.3.  And
apparently tightvncserver doesn’t support that.

Interesting, when the phone goes to sleep, I have to re-enter the VNC
password. I’m not sure if that’s a security feature on purpose, or if
it just lost its connection.

I’m now reading a PDF in evince in another window.  The arrow keys and
PgUp/PgDn keys on AVNC’s overlay are just enough to navigate the
document.

The arrow keys also work adequately as an on-screen controller for the
BubbleOS Tetris game, so the keyboard can be hidden.

At some point my X session on the console seems to have crashed. And,
possibly at the same time, so did evince and my terminals in this VNC
session. And the window manager. But Caja, I guess, respawned and put
icons on the desktop, and from one of those I was able to launch
xterm, and from xterm, marco, and then the rest of the session.  I
suspect there is some kind of dodgy interaction going on between
systemd and tightvncserver, because the session doesn't seem to have
gotten started quite properly inside VNC.
