15:12 < muurkha> hmm, this typewriter looks pretty neat: http://skrekkogle.com/projects/ascii/
15:13 -!- loggy [~loggy@tea.infomesh.net] has quit [Ping timeout: 240 seconds]
15:13 -!- saxo [~saxo@2001:19f0:6800:1102:5400:ff:fe11:39a1] has quit [Ping timeout: 240 seconds]
15:14 -!- sbp1 [~sbp@2001:19f0:6800:1102:5400:ff:fe11:39a1] has quit [Ping timeout: 240 seconds]
15:14 -!- sbp [~sbp@apache/doge/sbp] has quit [Ping timeout: 256 seconds]
15:15 < muurkha> it's a different Erika than https://www.ddr-museum.de/de/objects/1003600
15:15 < muurkha> "commonly used for carbon copies in Russian samizdat production" https://en.wikipedia.org/wiki/Samizdat
15:16 < muurkha> https://en.wikipedia.org/wiki/Jukka_Mallinen also talks about it
15:18 < muurkha> the original was made by https://de.wikipedia.org/wiki/Seidel_%26_Naumann
15:19 < muurkha> https://de.wikipedia.org/wiki/Datei:Erika_Typewriter_Seidel_Naumann_1912.jpg is a photo of the original 01912 typewriter
15:24 < muurkha> aha: "Starting in 1965, the computer electronics factory in Glashütte manufactured certain assemblies for the Erika . 
                 robotron Erika GmbH , formed after privatization in 1990, ceased production in 1991 and filed for liquidation in 1992. 
                 The trademark protection expired in 2004, which is why the Erika trademark was deleted in 2005 after 95 years."
