Here’s an embarrassing annotated interactive session of me
interactively writing an integer square root routine in Gforth:

    variable square variable guess  ok
    : sqstep square @ guess @ /  guess +  2/  guess ! ;  ok
    80346 square ! 1 guess !  ok
    sqstep  ok
    guess ? 70107476356745  ok

That doesn’t look right.  Is division doing some kind of double
precision thing?

    sqstep guess ? 70107476316572  ok
    sqstep guess ? 70107476316572  ok
    80346 1 / . 80346  ok

No, division looks fine.  Oh, I forgot the @ when I tried to read
GUESS.  (This is the kind of thing that makes me prefer VALUE over
VARIABLE.)

    : sqstep square @ guess @ /  guess @ +  2/  guess ! ; redefined sqstep   ok
    1 guess ! sqstep guess ? 40173  ok

Okay, that looks more reasonable, but does it converge to the square root?

    sqstep guess ? 20087  ok
    sqstep guess ? 10045  ok
    sqstep guess ? 5026  ok
    sqstep guess ? 2520  ok

This is getting tiresome, let’s run several iterations:

    sqstep sqstep sqstep sqstep sqstep guess ? 283  ok

Is that the actual integer square root of 80346?

    283 283 * . 80089  ok
    sqstep sqstep sqstep sqstep sqstep guess ? 283  ok

Well, it’s at least pretty close.  So let’s see about writing a loop
that continues until we hit a fixpoint:

    : newtsqrt  square !  1 guess !  begin sqstep guess @ dup * square @ - until ;  ok

Wait, how do you do absolute value in Forth?  Do I have to define
this?  I can just type in negative numbers directly, right?

    -3 . -3  ok
    -3 abs . 3  ok

Okay, so maybe if the absolute value of the error is smaller than the
actual square root itself, we can stop.

    : newtsqrt  square !  1 guess !  begin sqstep guess @ dup * square @ - abs guess < until ; redefined newtsqrt   ok

Oops, I’d better return the square root it computed.  Good thing I’m
doing this in Emacs and don’t actually have to retype the whole line.

    : newtsqrt  square !  1 guess !  begin sqstep guess @ dup * square @ - abs guess < until  guess @ ; redefined newtsqrt   ok
    3408 newtsqrt . 1704  ok

Uh, I think that maybe stopped after one iteration.  Because of course
after the first iteration our guess is very large compared to the
actual square root, so our error is smaller than it.  So maybe instead
we should check to see if we’re at a fixed point:

    : newtloop  begin guess @ sqstep guess @ - invert until ;  ok
    : newtsqrt  square !  1 guess !  newtloop  guess @ ; redefined newtsqrt   ok
    3408 newtsqrt . 1704  ok

Huh, that can’t be a fixed point, can it?

    guess @ sqstep guess @ . . 853 1704  ok

Nope.  Oh, wait, INVERT isn’t the right thing, that’s a bitwise
invert.  I wanted 0=.

    : newtloop  begin guess @ sqstep guess @ - 0= until ; redefined newtloop   ok

Gotta redefine NEWTSQRT to use the new definition of NEWTLOOP; this is
one of the downsides of using Forth purely at the REPL like this
without editable code blocks you can reload.

    : newtsqrt  square !  1 guess !  newtloop  guess @ ; redefined newtsqrt   ok
    3408 newtsqrt . 58  ok

Hey, that looks pretty plausible, is it?

    58 dup * . 3364  ok
    17314 newtsqrt . 131  ok

Cool.  But I wonder if our fixpoint termination logic always works;
maybe there’s a case where it will loop infinitely?  Like maybe right
near a perfect square?

    120 newtsqrt .   ^C
    :34: User interrupt
    120 >>>newtsqrt<<< .
    Backtrace:
    $7F865671A8D0 @ 
    $7F865671A998 newtloop 
    guess ? 10  ok
    sqstep guess ? 11  ok
    sqstep guess ? 10  ok

Uh, yep, it’s looping.

How about if we try the is-the-error-less-than-the-square-root
approach from earlier, but instead of working down from above, we work
up from below?  Paradoxically this works by setting the initial guess
to the square itself:

    : newtloop  begin sqstep guess @ dup * square @ - abs guess < until ; redefined newtloop   ok
    : newtsqrt  dup square ! guess !  newtloop guess @ ; redefined newtsqrt   ok
    120 newtsqrt . 60  ok

Uh, that didn’t work, did it?

    guess @ dup * . 3600  ok
    3600 square @ - abs . 3480  ok

That sure seems like it should have continued.  Oh, I forgot an @
again:

    : newtloop  begin sqstep guess @ dup * square @ - abs guess @ < until ; redefined newtloop   ok

Again, reload newtsqrt to pick up the definition:

    : newtsqrt  dup square ! guess !  newtloop guess @ ; redefined newtsqrt   ok
    120 newtsqrt . 11  ok

Yee-haw!  It can take the approximate integer square root of 120!

    121 newtsqrt . 11  ok

That looks good too!

    122 newtsqrt . 11  ok
    143 newtsqrt . 12  ok

And it doesn’t always produce 11!  I wonder where the dividing line
is?

    133 newtsqrt . 12  ok
    131 newtsqrt . 11  ok

About there.  Does it still work for larger numbers?

    3408 newtsqrt . 58  ok
    58 dup * . 3364  ok

Great!
