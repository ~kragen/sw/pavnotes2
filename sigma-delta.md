I thought I’d add some positive feedback to a simple class-B
totem-pole amplifier to get a Schmitt trigger and [accidentally added
negative feedback instead][0], resulting in a very nicely linearized
inverting amplifier, despite not containing any sort of differential
stage.  I guess this is like what people used to call an “op amp”
before they had transistors; you could make something similar with two
vacuum tubes.

![(schematic of circuit)](linearized-class-b-amp.png)

    $ 1 0.000005 10.20027730826997 50 5 43 5e-11
    t 144 240 192 240 0 1 -1.6145828392779107 0.7520962287454198 100 default
    t 144 96 192 96 0 -1 6.881240745261824 -0.7520801867148457 100 default
    R 192 80 192 48 0 0 40 5 0 0 0.5
    w 192 192 192 224 0
    r 144 96 144 160 0 1000
    r 144 192 144 240 0 1000
    w 192 192 304 192 0
    R 64 192 16 192 0 1 40 0.5 0 0 0.5
    R 192 256 192 288 0 0 40 -5 0 0 0.5
    r 64 192 144 192 0 1200
    w 192 160 192 112 0
    r 144 160 192 160 0 10000
    w 144 160 144 192 0
    w 192 160 192 192 0
    o 7 64 0 4099 5 0.0015625 0 4 7 3 6 0 6 3

[0]: http://falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWcMBMcUHYMGZIA4UA2ATmIxAUgpABZsKBTAWjDACgAXcGmkFGqmGIo+AqOBAtohMDQQE82YVmJhI5GBgRpihFCjwY5NIXmRUAJgwBmAQwCuAGw6duvXeGEgPVFiELQeHhg-OpyRGAEvEya2viQkYQYsnhy5GqWNg7ObABKniJ4gl40ZlRUYkjlUNAIbADuBU1CIvq8kGwATm7ehD1ghNUZHd2yvC09oeLDDc1euONeHfmEiyIDTYK05bXiO3X5EygIfUdBe9uSVXu1Xf5r-UvgaB2NEwPF6yFQd2Pgg80AYJ4K9+gC-hNQe9wV5IWwAPYgcircQCUjUGBwMAnY6opEgeh9Kh9bBsIA

My actual objective here was a sigma-delta or class-D sort of affair
which snaps quickly between positive and negative rails, biasing its
input with an error accumulator to reproduce the input signal on
average.  So it needs negative feedback, but delayed, plus rapid
positive feedback.  My [initial attempt to add positive feedback
through another inverting transistor][1] didn’t work well:

![(schematic with an inverting transistor)](failing-positive-feedback.png)

    $ 1 0.000005 0.26073472713092677 50 5 43 5e-11
    t 144 240 192 240 0 1 -9.026761448084777 0.7518099813586137 100 default
    t 144 96 192 96 0 -1 -0.5304473840864894 -0.7518759546430998 100 default
    R 192 80 192 48 0 0 40 5 0 0 0.5
    w 192 192 192 224 0
    r 144 96 144 160 0 1000
    r 144 192 144 240 0 1000
    w 192 160 304 160 0
    R -48 160 -96 160 0 1 40 0.5 0 0 0.5
    R 192 256 192 288 0 0 40 -5 0 0 0.5
    r -48 160 32 160 0 1200
    w 192 160 192 112 0
    r 240 320 128 320 0 10000
    w 144 160 144 192 0
    w 192 160 192 192 0
    w 192 192 240 192 0
    w 240 192 240 320 0
    t 128 320 96 320 0 1 0.5896703792714462 0.6588966340583529 100 default
    r 96 304 96 240 0 1000
    R 96 240 96 208 0 0 40 5 0 0 0.5
    R 96 336 96 368 0 0 40 -5 0 0 0.5
    w 32 304 96 304 0
    r 32 304 32 160 0 100000
    w 32 160 144 160 0
    o 7 8 0 4099 5 0.000390625 0 4 7 3 6 0 6 3

[1]: http://falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWK0BMA2SB2AzAFkypmNpAJzqaYgKTUi7bUCmAtGGAFAAu4uuIKXLTDkBQqOBAtSsCmjB8AHJEX5KyTAjDLSpRcQSL52KmDggAJkwBmAQwCuAGy7de-UmnCiPEtlJgIJHw4qipouIqk-CwwmtqapAi44SS6iuDmVnZOLgBKXiggygX06bS04kjlyAgcAO4lIoVNAoJQHABObiA+CvxgGBJmcJ3dLX1i1cOQ9Y2DQeCDM-ksEYu00p4DU-Tl0FUSARz5LSgIW6IoimUS4iwHe7Vdq+nbINjNS+AoIw3jg+MwIUZl1BLQPsIru8fkN4DM-nx1mNRPC5sJRC1UeNLuJMbMwSUCRD2jwgeliT5icIapE0JhICZyEQ+GhgdA0IZaWg8JBDNgEChSBlaFkHM5RpTIO5PAThPBjj0ZeIfD8bhVaA8agrKdhPJS0GrdlJNUcGh93lLFRb+CDoda7W85XDZua3hNHRwAPYgKg3IS6OgweDYUiQVkHfhURieWiebAcIA

It does produce reasonably rail-to-rail outputs, but the transitions
betweent them are not as quick as you’d like, and the main objective
of getting the output transistors out of the shoot-through region is
not achieved.  They’re carrying 400mA at peak in the simulation even
though less than 1mA is being used by the feedback transistor.  I feel
like I got some negative feedback in my positive feedback.  Dawg.

My attempts to add delayed positive feedback were not successful.

[I did simulate such an amplifier successfully with three op-amps][2]
instead of three transistors:

![(schematic of op-amp circuit)](./opamp-pwm.png)

    $ 1 0.000001 0.26073472713092677 50 5 43 5e-11
    R -96 224 -144 224 0 1 40 0.5 0 0 0.5
    a 176 224 336 224 9 15 -15 1000000 2.4763563052361026 2.476391339703696 100000
    c 336 224 336 288 0 1e-7 1.027090454109723 0.001
    w 176 240 176 288 0
    r 176 288 176 352 0 100
    g 176 352 176 368 0 0
    w 176 288 336 288 0
    a 416 240 528 240 8 15 -15 1000000 3.5034467593458256 3.614870379992346 100000
    w 416 224 336 224 0
    w 416 256 416 288 0
    r 336 288 416 288 0 10000
    r 416 288 528 288 0 100000
    w 528 288 528 240 0
    r -96 224 -32 224 0 100000
    r 128 288 -32 288 0 100000
    w -32 224 48 224 0
    w 128 288 128 208 0
    r 48 160 -32 160 0 1000
    w 48 160 48 192 0
    w 48 160 48 128 0
    r 48 128 128 128 0 10000
    w 128 128 528 128 0
    w 528 128 528 240 0
    w 528 288 592 288 0
    a 48 208 128 208 9 15 -15 1000000 1.363612611301336 1.3636373752147333 100000
    g -32 160 -32 176 0 0
    w -32 288 -32 224 0
    w 128 208 176 208 0
    r 592 288 656 288 0 100
    c 656 288 656 352 0 0.000001 2.012633126510095 0.001
    g 656 352 656 368 0 0
    r 656 288 720 288 0 1000
    c 720 288 720 352 0 1e-7 1.4992983670357865 0.001
    g 720 352 720 368 0 0
    x 69 166 185 169 4 12 error\smeasurement
    x 225 167 323 170 4 12 error\sintegration
    x 392 167 511 170 4 12 Schmitt\scomparator
    x 597 246 724 249 4 12 low-pass\soutput\sfilter
    x 392 184 490 187 4 12 generating\sPWM
    o 0 32 0 4098 0.625 0.00009765625 0 2 0 3
    o 2 1 0 4099 2.5 0.1 1 2 2 3
    o 12 1 0 4098 20 0.2 2 2 12 3
    o 32 32 0 4099 5 0.003125 3 2 32 3

[2]: http://falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3EYCYBskDsBmALOp6xNIBOVddEAVkipG0yoFMBaMMAKACURniUQkSbDzDZhg4TQjYaMSlAVz2AQ3Dp+EkJkwahIYuHmt5YePAHRcKTJWuRKSHadQWrmYoXfpIOvuDOQ7ADGWjoCetoaABxRCmAsFGCw+CSQ2JTYpsR4DDAI7ADuahoyxQIxUOwATmVIFWDqWg5xcOwA5mU2SJ0osbKFtRWR5X0qdGAlNA6xQjSxYEYL-gFa0NQ42CjolMQ4lFFItqsoolHemOjEV46by-ADmbrCw5qBRY8CRx91ozUvFd8KlIzNVxtFYtMRi0QUVIT8qHUBKVAjVeE8eJhuq87q0amBEfDmJiocCYRisXpsDM9G9wAT6gTIL86PM0OTwGzSbSqRyaDywKRKu9WXz5oiUSy6WLpdDWkV8TLIQqhQjFQTkQM4RUdligWMeUgmVKBEaDEtjDi4OBoDonKg2ERPPwkrbrFhtkhRFhtJb2uyJjQid0Gvx+kUg1CI68BsrDfNGnHKjUdVCUEd4aTgiA0+Ds0cuopYGYIEhYPjrJhy5RTCR5HlIBwOjmmt1mzo+km87m8DQM3csz2oYOC1IEtbsNdiFEdOdKOgommoEXGyBh81h71FOwAB7Zs0oZ1REwoAzCfEgRhVKoAeyqAB0AM4AW0YygfAFcqowXwA7AAuO7hMeFCYgwDR8nSF5Xrej4AJb-owbRVMof6wdeP6Ae4wZbFQbBqBB54AMpBAAFk+sF-n+j5BNeT4AA7KMhf63oBOwUEI-B4OIE50JBAA214FMwDEPg+j7Xu+f50ZJj4AGawXxf6XphgpgFEwgTlIZy8eebSMD+l4ofBbSPgACgA6gAsuw14KMSfIkH00AoIcS5mNkOaub2dk2QI4AKDIVwWHW0AQCWfmYL556IHQjkmkuWJ+eekW2cS9mxUFIVwJWrkMN0aXsEAA

[Then I improved it.][3] This version only has two op-amps.

[3]: http://falstad.com/circuit/circuitjs.html?ctz=CQAgjOCmC0Ds4GYB0AGAnLALGArAgTGCgBxjEBsEOKIOImCtMYYAUAEoj76ZcnixyfYiBoRMNFEjqTR01gEMuPZbwQIh3XmnB1oucCiPG5+WPjQNiKQhvI4MMpGYuZMeFrFgIwmQYeMUVgBjEHVNFXCuc1EuZ0CEhIQYHWgpfGMEDDsETHIEFH8pIzYAd1UuCQqCSVYAJ2qM6uJeMWNWAHNm3i16I1ig8ry1JuGuHCEghoQcfAqx3rb2hrGwNDmcfzW5pfbyzc0qg-B10Xr6chGaY5rYoj3afx5r-1upkEuaW9gUK7ujEIgH5-YFhFoDVCBCD4JDrYhaMD4LK-ajeOQlTpA35g3igiSyd6g77kERvAJBUKCUlNKk4u4weAwrCWPAIYhoNDkDC+dEoNhdWls3EkvoE1gADy46i4GDCPjC-V4iJAkDqdQA9nUADoAZwAlgA7AAukA6dQURr16oNEvoWwItDQdDAM3o4DmAGVggALAC2eqNRt1wXVvoADgpzUbNbbyJdonRYGhobAREq5gAbdWlaARnU63XqgCuRrDJd1ADM9RmTXVbX4hL4dJslbM3cqOpADaqLYaOrqAAoAdQAsqxylFbr03uOLn9J01BnP5vHtmclAsjk8qjoDPpnYlTOZLGybC64w5YM6kJgLOt1ChcjgyBNGwlbT4hGz4G4dMi3SoADCep1MEJZ1EWGYBuqrASIwvi8NY4yNuQBIAPrPvgaEoGhmAULA2FID4+A4IRgQYPY5AkWh+hoVhOEIGhACSBplkasGPmEbYkUIbjUrIWGYNhuGUGAdHSGRj6IqRtFYVhjGAWwcFAuCGgiMmcwMDs4DkBhiLCXhcYYZJuBUTJYlYWAumMR6kAhgaAAmlbVrWuo6kaCidhxjATF8IokmmDpiIxmEGRQulEIRQmyWhLpoYxABiIHuc5Naqm5HlecpDhzFZNDmDQiwgGguEGZQxB0ThxRGMiZk0RZaElYxADyJZsbqABu6o1p5kC2msdAzIyRBhKh7ZzOwbCSlkEBrD+KCaIq7ogOw+C2phYRuLQZibT+y3sJg638FZ8CzPBPzjStCC2mYu7sjKIhrumIBMYp60GGYp25FwSHPa9a3lJ8YRNDci6xvGFg0Oy0JoIwz0ABTRrqYaqv6QY6h1eo6kWChQQAXn2hZteWOoKB1PadgAlLOMxzNOGgVEEalcXTkRtmutTKfgxCPcq2CNkU9BiUJOESGgFUwqRyBYTF2DxcxilE6WJY0wzLhyl+YNAA
