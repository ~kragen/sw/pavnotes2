I was talking with Zachary Vance about programming languages, and got
to thinking about what I wish Scheme had, which is mostly structs and
polymorphism, and to thinking about compilation strategies.

A conventional Scheme raises type errors if you try to do something
like take the `car` of an integer or apply arithmetic to a pair, which
means that the usual-case code (open-coded or not) needs to be
preceded by a type check and a predicted-not-taken conditional
branch. Where the branch goes doesn't affect the performance of
happy-path code much. Which means that there's no harm done if it
branches off to a generic method dispatch call rather than directly to
an error handler, though it will bloat the compiled code size.

[SRFI 9](https://srfi.schemers.org/srfi-9/srfi-9.html) describes a
record definition facility which lets you write this, with the
expected effect:

    (define-record-type :pare
      (kons x y)
      pare?
      (x kar set-kar!)
      (y kdr))

I think a simpler approach would be to use CL's `setf` for write
accesses, invocation of the type object as a function for
instantiation, the argument names for accessor function names, and a
separate `is?` function for type tests; then you could just write
this:

    (type (kons kar kdr))

Or:

    (type (point xcoord ycoord))
    (type (polarpoint rcoord theta))

These define accessor functions `xcoord`, `rcoord`, etc., which are
inlineable and begin with a type test.  If we want to define fallback
methods for when the type test fails, we could say:

    (to xcoord polarpoint (p)
        (* (rcoord p) (cos (theta p))))

    (to ycoord polarpoint (p)
        (* (rcoord p) (sin (theta p))))

    (to rcoord point (p)
        (let ((x (xcoord p))
              (y (ycoord p)))
          (sqrt (+ (* x x) (* y y)))))

    (to theta point (p)
        (atan (ycoord p) (xcoord p)))

Since these add new cases to an existing function, the function needs
to exist first, but in cases where we want pure dynamic dispatch, we
could just declare the generic function initially with no cases:

    (define draw (message))

Or perhaps as a base case for a single type; list processing functions
could reasonably be defined as methods on pairs, plus a case for the
nil type.

Such a Lisp doesn't need cons, car, cdr, null, and atom; it can define
them with type, to, is?, and define.

    (type (cons car cdr))
    (type (nihil))
    (define nil (nihil))
    (define (null? x) (is? x nihil))
    (define (symbol? x) (is? x symbol))

    (define length (method cons c)
        (1+ (length (cdr c))))
    (to length nihil (c)
        0)

Mutators require setf.

    (define (rplaca a b) (setf (car a) b))
    (define (rplacd a b) (setf (cdr a) b))

The language doesn't really require conditionals, either, though
putting closures on the heap for every `if` statement would probably
be costly:

    (type (truth))
    (type (falsehood))
    (define if (method truth b then else)
        (then))
    (to if falsehood (b then else)
        (else)) 

Even a function call and return every time an `if` condition is false
seems likely to be overly costly, even if block arguments avoid
heap-allocating.
