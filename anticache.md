Very small, simple computers (under about 32768 transistors) generally
were not built with memory caching.  This results in a serious
performance bottleneck with the standard von Neumann architecture, and
I’m wondering about smaller, simpler alternatives to caching.

A 256-byte cache requires 2048 bits of memory for the cached data
(say, about 12288 transistors) and some kind of CAM hardware for
probing the cache: say, 8 27-bit tags for 8 32-byte cache lines.
That’s only another 216 bits, but they’re CAM bits, so it’s probably
nearly as many transistors as the 2048 dumb bits.  So we’re at around
20k transistors for a tiny 256-byte cache like the 68020 had.

Is there something we can do in less?  Like, 6k transistors, or 2k?
First I guess I need to explain what the problem is.

Harvard architectures
---------------------

Harvard computers have been considered obsolete since the advent of
the von Neumann architecture in the 01940s, but they do have a bit of
a speed and simplicity advantage over the von Neumann approach.  This
is because, when they execute an instruction that accesses operands in
memory, the instruction fetch is on different address and data buses
than the operand fetch or store.  So they can be concurrent, and the
data memory can be half as fast as a von Neumann machine for the same
CPU speed — or, when the memory bandwidth is the limiting factor, a
Harvard computer can be twice as fast as a von Neumann machine.

Partly because of this, many microcontrollers use a Harvard
architecture even today.  The AVR used in the Arduino line is the one
I’m most familiar with, but there’s also the PIC microcontroller, the
8048, the 8051, the famous Padauk 8-bit microcontrollers, and many
others.  These often use Flash or even mask ROM for the program (OTP
PROM for the Padauk parts), so they aren’t really giving anything up
by not being able to write to it freely the way you can on a von
Neumann machine.

The AVRs, at least, are capable of writing to their program memory as
well; they just use a different assembly instruction to do so (SPM,
store program memory), require it to be block-erased first since it’s
Flash, and take a long time.  Similarly, you can load operands from
program memory on an AVR with the LPM instruction, which is amusing in
Spanish.  So, even though the original Harvard Mark I could not write
to its program memory, you can still get the performance benefits of
the Harvard architecture most of the time even if you have the ability
to write to it.  I'm not sure you retain any extra simplicity, though.

C is not perfectly happy in a Harvard architecture, because on a
Harvard machine the same address can designate one piece of data in
program memory and an unrelated piece of data in data memory.  This
complicates C pointer semantics.  avr-libc by default papers over the
mess by copying all the constant data from Flash into RAM at startup,
but sometimes this is undesirable, because RAM is usually precious on
a microcontroller.  avr-gcc supports a funky `PROGMEM` pointer
qualifier that declares a pointer as pointing into program memory
rather than RAM, and so for example avr-libc has special functions for
operating on `PROGMEM` strings.  This sort of thing is probably why
most recent microcontroller designs are von Neumann rather than
Harvard.

Split caches are a variant of Harvard architectures
---------------------------------------------------

The IBM 801, a von Neumann machine from the user perspective, attacked
this problem by splitting its cache between instructions and data, an
approach that has since been taken by many microprocessors and has now
been standard for 30 years.  That way the main memory system only has
to handle the cache misses, while the smaller cache subsystem handles
the usual case.

As I said [on the orange website][0], I’m curious when the first
microprocessor with cache was, since the 801 wasn’t a microprocessor.
It might have been the 68020 in 01984.  [An article about the 801][1]
mentions the 68040 from 01990 (1.2M transistors) with a split on-die
L1 cache (4K+4K), but the 68030 in 01987 (273k transistors) had
256B+256B, and the 68020 in 01984 (190k transistors) had 256B icache.
The [RISC I][12] and [RISC II][13] don’t seem to have had caches;
neither did the ARM 1 prototype (25k transistors) or the ARM 2 (27k
transistors), but the ARM 3 did in 01989 (310k transistors, 4 KiB
cache).  The 80486 (1.18M transistors) followed suit with a 4-KiB
cache in 01989.

[0]: https://news.ycombinator.com/item?id=33059665
[1]: https://thechipletter.substack.com/p/the-first-risc-john-cocke-and-the
[12]: https://people.eecs.berkeley.edu/~kubitron/courses/cs252-F00/handouts/papers/p216-patterson.pdf
[13]: https://news.ycombinator.com/item?id=33060051

The 801 article says, “the first version had around 7600 logic gates,”
estimating that this would be 30k to 45k transistors, but I don’t know
if that included the cache.  Presumably it didn’t include RAM.

An example of the von Neumann bottleneck
----------------------------------------

(This is not the same as John Backus’s “von Neumann bottleneck”,
though not completely unrelated.)

A von Neumann RISC processor completely lacking an instruction cache
suffers a serious performance handicap on certain very simple
operations; for example a RISC-V loop comparing two arrays of 32-bit
integers might look like this in assembly language:

           li   a2, 0         # initialize loop counter in a2 to 0
    loop:  slli a4, a2, 2     # convert loop counter to byte index in a4
           add  a3, a4, s2    # index array in s2
           add  a4, a4, s1    # index array in s1
           lw   a3, (a3)      # load 32-bit item from s2 array
           lw   a4, (a4)      # load 32-bit item from s1 array
           bne  a3, a4, quit  # exit if they differ
           addi a2, a2, 1     # increment loop counter
           bne  a2, a5, loop  # test against iteration limit in a5
    quit:

Each iteration of this loop involves loading 8 bytes of data from
memory, but also loading 8 instructions from memory, which total 24
bytes with the RISC-V C extension.  So it’s easy for a simple RISC CPU
implementation to spend 75% of its time in this loop just fetching
instructions.

I think this can be improved a little; the above is a slightly edited
(and untested) version of what GCC generated.  Iterating with pointers
drops it to 6 instructions and 16 bytes in the loop body (untested):

           mv   a3, s1        # initialize a3 to point into s1 array
           mv   a4, s2        # and a4 to point into s2 array
           slli a5, a5, 2     # calculate offset of off-the-end sentinel
           add  a5, a5, a4    # and make a5 an off-the-end pointer
    loop:  lw   a0, (a3)      # load array item from s1
           lw   a1, (a4)      # and from s2
           bne  a0, a1, quit  # and compare them; if they were equal,
           addi a3, a3, 4     # advance the s1 pointer
           addi a4, a4, 4     # and the s2 pointer
           bne  a4, a5, loop  # and repeat if we haven’t reached the end
    quit:

On a von Neumann machine without a cache, this still spends 67% of its
time fetching instructions.

If you don’t have a cache, you can improve the situation by unrolling
the loop, but you still need two `lw` instructions and a `bne` per
loop index: 3 instructions totaling 8 bytes to process 8 bytes of
data, so you’re still wasting over 50% of your memory bandwidth
fetching the same instructions repeatedly.  And this optimization adds
significant complexity.

So, how can we improve that situation?

CISC string instructions
------------------------

Some CISC processors had string manipulation instructions like the
8086’s MOVS, CMPS, SCAS, and STOS.  These allow certain operations to
be applied to an arbitrarily large amount of data without any
instruction fetches intervening: with the appropriate REP\* prefixes,
MOVS copies an array, CMPS compares two arrays, SCAS searches for an
item in an array, and STOS fills an array with a constant.  

On the 80386 the above 6-instruction loop would just be a single REPE
CMPSD instruction and could spend all the memory bandwidth on the
actual data.  (The 8086 can’t quite do that because it only has CMPSB
and CMPSW, not CMPSD, but it can do the analogous thing in the 8-bit
or 16-bit case.)

Computers from the 01950s and 01960s, such as the IBM 1401 and its
clone the Honeywell 200, often had instructions that went beyond these
simple string operations, providing arbitrary-precision decimal
arithmetic in RAM, one digit at a time.

Perhaps the most notorious example of this approach is the VAX’s
[floating point polynomial evaluation instruction][8], which Doug
Merritt says was [significantly slower than doing the polynomial
evaluation in software][9], and which [an anonymous note
describes][10] as “essentially useless, except as a way of telling
implementations apart” by the release of the MicroVAX, due to hardware
bugs.

[8]: https://documentation.help/VAX11/op_POLY.htm
[9]: https://news.ycombinator.com/item?id=26308262
[10]: http://simh.trailing-edge.com/docs/vax_poly.pdf

These instructions only handle certain special cases, though.  If you
want to add two arrays element-by-element, for example, or compute the
index where they most differ, or find the maximum of an array, the
80386’s magic string instructions won’t rescue you.  And overall the
RISC approach helped a lot to get higher clock speeds.

John Mashey, one of the designers of MIPS, [wrote an
interesting high-level retrospective on the VAX in 02005][11].

[11]: https://yarchive.net/comp/vax.html "1122017456.365060.113060@g49g2000cwa.googlegroups.com et seq."

Loop caches: extended study of the CDC 6600
-------------------------------------------

The 68010 had a 6-byte instruction “cache” which allowed it to execute
two-instruction loops without fetching instructions, but I don’t think
that counts as a “cache”.  The [3-megaflops CDC 6600 from 01964, which
anticipated RISC in many ways,][2] had a somewhat similar feature.  WP
says the 6600 had about 400k transistors, which can't include the main
memory because its main memory was magnetic cores.

The CDC’s “loop cache” was called “the Instruction Stack”.  This has
nothing to do with the call stack (the 6600’s subroutine call facility
used self-modifying code rather than a call stack) and is first
mentioned in the reference manual on p. 1-5 (12/159):

> * 8 transistor registers (60-bit) hold 32 instructions (15-bit) or
>   16 instructions (30-bit) or a combination of the two for servicing
>   functional units.

[2]: http://ygdes.com/CDC/60100000D_6600refMan_Feb67.pdf "Control Data 6400/6500/6660 computer systems reference manual, Rev. D"

This was reduced to a 60-bit “Instruction Buffer register” on the
lower-end 6400 and 6500 models.

This is elaborated on p. 3-1 (20/159):

> Instruction Stack; holds eight 60-bit instruction words.

On p. 3-3 (22/159):

> Instructions are fetched from memory only if the instruction
> registers are nearly empty (or when ordered by a branch).

On p. 3-4 (23/159):

> The Central Processor reads 60-bit words from Central Memory and
> stores them in an instruction stack which is capable of holding up
> to eight 60-bit words.

And then there is a puzzling comment on p. 3-10 (29/159) which turns
out to be about instruction cache coherency:

> To preserve the integrity of an “in-stack” [in-cache] loop (in the
> event of an Exchange Jump [context switch]), it is illegal to modify
> the contents of any memory address which holds an executable
> instruction (or instruction word) contained within the loop.
> 
> [diagram omitted, but it has an instruction word at address “Y + 3”
> which executes first X₆ = X₂ + X₄ and then, following the notation
> “Assume Exchange Jump comes in at this point”, A₆ = Y + 1.
> Previously it had been explained that writing an address to register
> A₆ results in storing X₆ into memory at that address.]
> 
> These instruction words from stack (from memory locations ⟦Y + 1⟧
> through ⟦Y + 5⟧) constitute a loop.
>
> After executing the lower instruction at ⟦Y + 3⟧, the contents of
> memory location ⟦Y + 1⟧ differ from the contents of ⟦Y + 1⟧ in the
> stack.  If the Exchange Jump comes in as indicated, subsequent
> reentry [resumption of the process] will call up the modified loop
> from memory, rather than the stack loop in its original un-modified
> form.

(Chalkboard bold brackets ⟦⟧ are ordinary square brackets in the
original.)

I think I don’t understand why it matters *when* the context switch
happens during the loop, but I think that what’s being implied is
that, as long as execution remains within the five-word cached loop,
the word at Y + 1 will not be reloaded from memory, but rather be
executed repeatedly in the form that it was first cached.  But, if the
Peripheral Processor induces a context switch (“Exchange Jump”) in the
Central Processor during the loop, almost certainly the cache will not
contain the loop when we resume execution of the process later, so the
modified code will be run; and this results in unpredictable behavior.

On page B-1 (128/159) it says:

> After an Exchange Jump start by a Peripheral and Control Processor
> program, Central Processor instructions are sent automatically, and
> in the original sequence, to the instruction stack, which holds up
> to 32 instructions.
> 
> Instructions are read from the stack one at a time and sent to the
> functional units for execution.

Later, on page B-10 (137/159) it talks about the cost of instruction
fetches:

> When executing sequential instructions that are not in the stack,
> the minimum time is one word of instructions every 8 cycles.  The
> time of issue of the last parcel of an instruction word to the time
> of issue of the first parcel of the next instruction word (while
> executing sequential instructions that are not in the stack)
> requires a minimum of 4 cycles.  If the last instruction in an
> instruction word is a 30-bit instruction [which is two parcels, the
> alternative being a single-parcel 15-bit instruction], a minimum of
> 5 cycles are required from the time of issue to a functional unit of
> this instruction to the time of issue of the first instruction in
> the next word.

And finally, the actual functionality:

> When a branch out of the stack is taken, 15 minor cycles [1500 ns]
> are normally required for an 03ijk instruction [GO TO K if Xⱼ meets
> condition i: ZR NZ PL NG IR OR DF ID] and 14 minor cycles are
> normally required for other branch conditions ... 12. Nine cycles
> are required for 03ijk instructions when the branch is taken within
> the stack.  The next sequential word is recognized as within the
> stack.

And on page B-11 (138/159):

> \13. Eight cycles are required for 04ijk [GO TO K if Bᵢ = Bⱼ, EQ] to
> 07ijk [GO TO K if Bᵢ < Bⱼ, LT; two other relations are 05ijk NE and
> 06ijk GE] instructions when the branch is taken within the stack.
> The next sequential word is recognized as within the stack.
> 
> \14. Eleven cycles are required for 03ijk instructions when the
> branch is not taken (time from branch execution to issue of next
> instruction) if in the stack or if falling through to the same word.
> Out of the stack fall-through to the next word takes 14 cycles.

And on page B-12 (139/159):

> \20. An Index Jump instruction (02) [GO TO K + Bᵢ, JP] will always
> destroy the stack.  If an unconditional jump back in the stack is
> desired, a 0400K instruction [GO TO K if B₀ = B₀, EQ B0 B0 K] may be
> used (to save memory access time for instructions).
> 
> \21. A Return Jump instruction (01) will always destroy the stack.

So, here is my inference from reading the above, which might be wrong.
The 6600’s cache was a sort of ring buffer providing a peephole on
memory around the program counter.  When execution got to the end of
it, the earliest instruction word would be discarded, the window
advanced by 1, and a new word fetched from core memory.  There was
special logic for testing whether a (statically defined) jump
destination was within this window, in which case it would just switch
to issuing instructions from that point in the window; but jumps to
addresses computed at run time would empty the ring buffer.
Presumably jumps to addresses outside the ring buffer would also empty
it.

Incidentally, in the 6600, writes to pointer registers Aᵢ with the SAi
instructions (the only way to write to them except as part of context
switching) implicitly caused the corresponding “operand register” Xᵢ
to be fetched or stored.  So our RISC-V loop body six instructions:

    loop:  lw   a0, (a3)      # load array item from s1
           lw   a1, (a4)      # and from s2
           bne  a0, a1, quit  # and compare them; if they were equal,
           addi a3, a3, 4     # advance the s1 pointer
           addi a4, a4, 4     # and the s2 pointer
           bne  a4, a5, loop  # and repeat if we haven’t reached the end
    quit:

might have changed to something like this in CDC 6600 ASCENT:

    123456789 123456789 123456789 123456789 123456789 123456789 123456789 12
     LOOP     SA1  A1+1        .INCREMENT A1 BY 1 WORD, LOAD X1
              SA2  A2+1        .LOAD X2
              IX1  X1-X2       .COMPARE VALUES LOADED, LEAVING DIFF IN X1
              NZ   X1 QUIT     .LEAVE LOOP IF X1 IS NOT ZERO
              SB1  A1-B2       .SUBTRACT LIMIT VALUE IN B2 FROM A1 POINTER
              NE   B1 B0 LOOP  .IF DIFFERENCE /= 0 (B0 IS 0) REPEAT
     QUIT

Unfortunately this still ends up being six instructions!  The reason
is that, although the 6600 does have a “branch if not equal”
instruction, the NE, it only applies to the B registers.  So you can
neither compare the two values fetched from memory with an NE
instruction, nor can you use it to compare A1 to B2.  (I’m not sure
the floating-point zero test in the NZ instruction is correct for the
integer difference in IX1, either.)  And this actually takes *more*
memory space than the 16-byte RVC loop above: the SAi increment
instructions are 30 bits long (p. 3-24, 43/159) so they can have an
18-bit immediate increment field, and the NZ and NE instructions are
also 30 bits long so they can have an 18-bit destination address
field.  So we end up using three 60-bit instruction words for our 6
instructions, 22.5 bytes in modern units.

By contrast, the RISC-V loop compresses the `lw` and `addi`
instructions, leaving only `bne` uncompressed.

Nevertheless, this loop fits into less than half the “Instruction
Stack”.

(I don’t know what character set CDC used on their punched cards, but
the display console was documented as having only “26 alphabetic, 10
numeric, and 10 special” characters, so they probably didn’t have
lower case.  Three font sizes though.)

Jim E. Thornton, one of the CDC founders, published a book on the
6600, “[Design of a Computer: The Control Data 6600][4]”.  The book
bears a copyright date of 1970 but a LCCN of 74-96462, suggesting that
perhaps it didn’t actually come out until 01974.  Cray says in its
Foreword, “Mr. Thornton was personally responsible for most of the
detailed design of the Control Data 6600 system.”  This is the book to
which WP attributes the 400k transistor count.

[4]: http://archive.computerhistory.org/resources/text/CDC/cdc.6600.thornton.design_of_a_computer_the_control_data_6600.1970.102630394.pdf

It discusses how instruction fetch works starting on p. 120 (131/196),
referring you to Chapter V (pp. 57–116) for an introduction to the
instruction stack.  It provides some insight into how the cache was
physically implemented (p.121, 132/196):

> From odd levels of the instruction stack instructions enter U1 [I
> think this is the register for instruction decoding] directly.  From
> even levels of the instruction stack instructions enter U1 via
> register U0.  This form of logic is a result of the instruction
> stack data movement.  Instructions are entered at the bottom of the
> stack following a shift maneuver in the stack.  Whenever a new
> instruction fetch is initiated, the contents of the stack are
> “inched” upward, one register every half minor cycle [every 50 ns].

Then on pp. 122–123 (133–134/196):

> Instructions continue to be fetched sequentially from Central
> Storage until a Branch causes control to transfer within or outside
> the instruction stack.  If a branch is to one of the “old”
> instructions currently in the instruction stack, the sequence can be
> best described with the following example.
> 
>     Location LOOP        Instruction Word A
>     Location LOOP + 1    Instruction Word B
>     Location LOOP + 2    Instruction Word C Contains—GO TO LOOP IF...
> 
> This example shows a portion of a program contained in three
> instruction words A, B, and C located in relative location `LOOP`,
> `LOOP + 1` and `LOOP + 2`.  Instruction word C contains a branch
> instruction, `GO TO LOOP IF...`, which causes control to transfer
> back to instruction word A if the condition is satisfied.
> Instruction fetch of each of these instructions is initially
> accomplished from Central Storage.  In fact, because the first pass
> through the program “loop” requires that the first instruction
> contained in instruction word C be transferred to U2, an additional
> fetch is initiated.  The result is that an additional instruction
> word D is entered in the lowest level of the instruction stack
> before the Branch is taken.  After word D is entered into the stack,
> the “loop” can be held without further entries.  This could be
> called a form of “look behind”.

This suggests that while my guess above about the semantics of the
cache were correct, it was not implemented as a ring buffer but as 60
shift registers.

There is further information in Chapter V on pp. 112–113 (123–124/196):

> Only the conditional Branch instructions, 03 through 07, are allowed
> to jump within the instruction stack. Eight words may be held in the
> instruction stack.  These are loaded one at a time during the normal
> course of instruction fetch.  Each new instruction word is entered
> at the bottom causing the older words to move up.  Whenever a Branch
> instruction causes a jump out of the stack, all of the instruction
> words accumulated in the stack are declared void.  New instruction
> words are then brought in as described in Chapter VI.
> 
> Two values are maintained in the control system for the stack, a
> depth (D) and locator (L).  The depth (D) is a measure of the valid
> instructions in the stack.  This means that the value of D is set to
> zero after any Branch out of the stack and is increased by one for
> every new instruction word brought in.  When the stack is full, D
> remains equal to seven.  The locator L is used to specify the
> location in the stack of the instruction word currently in use.

This is sort of similar to having a single-line instruction cache with
a 64-byte cache line size.  As long as your inner loop doesn’t cross
cache line boundaries, it will run fast.  However, during
straight-line execution, that mechanism might not do very well at
prefetching instructions; you would need a second 64-byte register to
prefetch into, or you’d end up waiting for 64 bytes to transfer from
main memory every time you crossed a cache-line boundary in
straight-line code.

LDM/STM, the ARM memcpy
-----------------------

[Tony Finch recalled][3] that the original ARM had one very CISCy
feature which helped a lot with things like [block copy][6]: the LDM
and STM instructions, which load or store multiple registers to
memory, in theory up to all 16 (though one of them is the program
counter, so usually you only want 8).  This has pretty much the same
advantage as REP MOVSB on the 8086: no instruction fetches are needed.

[5]: https://news.ycombinator.com/item?id=33060514
[6]: https://keleshev.com/ldm-my-favorite-arm-instruction/

But this only helps a little bit if you’re doing a vector product or
string search or something.

RISC-V (and aarch64) opted not to have these instructions, despite
their heavy use in ARM.  [Bruce Hoult points out that implementing
them in millicode][7] eats up 96 bytes of your instruction cache,
assuming you have an instruction cache.

[7]: https://news.ycombinator.com/item?id=33065715
