I was thinking about how to replicate PHP's desynchronization-free liveness (see file `php-liveness.md`) for other purposes.

In particular I was thinking about Kartik Agaram's notes about freewheeling and convivial software, and my experience with living inside Emacs to some extent.  It seems very appealing to me to be able to guarantee that the source code I see is the source code that is running.

A simple sufficient but not necessary measure to guarantee this is to indirect all references from data structures to code through some kind of symbol table, so that when I update the code, the system can determine which new hunk of code is supposed to correspond to each of its existing code references.  It's also important (and perhaps sufficient) to distinguish assignments from initializations, like Lisp `defparameter`: when we load a modified codebase, we don't want to reinitialize the variables it defines, because that would result in losing the system state of the non-initial variable value — precisely what we're trying to avoid.

The simple assignment/initialization distinction could already go a significant distance toward reducing the frequency of these desynchronizations.  Consider this Lua sketch:

    if bread == nil then bread = {} end
    function bread.open(...) return ... end

The first line provides an initial value for `bread`, an empty table, but doesn't overwrite `bread` if it's already defined; this is nothing more than the assignment/initialization distinction, implemented by hand.  The second line destructively assigns to the `open` slot of the `bread` table, replacing any function that's already there.  This means that any future calls to `bread.open` will invoke the new code rather than the old code, even if they are using a previously stored reference to `bread`.  If you destructively update your "classes" as well as your modules this way, you have a Smalltalk-like pseudo-guarantee that any code invoked is up to date. (You also have to delete any no-longer-existing functions.)

But you still don't have the kind of strong guarantee you'd have in PHP or GW-BASIC, because it's still possible for a Lua object to contain a reference to outdated code.  That requires, I think, a layer of indirection through a sort of PLT or symbol table on every pointer from runtime data into compile-time-created code.  And that's a big problem for languages that support anonymous functions; how do you reliably identify a modified anonymous function? In [s-ol bekic's `alv`][1] this is solved by the editor assigning each new S-expression a serial number when it's created, one which persistently identifies it through further editing.

[1]: https://mmm.s-ol.nu/research/alivecoding/

One way to ameliorate this problem is to support anonymous functions within the handling of a single event, such as an HTTP request or mouse click, but not across handler boundaries.  This is how PHP does things. Downward-only funargs like Pascal, closures which can be passed as subroutine arguments but not stored in data structures or returned up the stack, also mean that no pointers to existing functions will survive a single event handler, and for many years Smalltalk blocks only supported this usage (though, unlike Pascal, they detected violations of it dynamically rather than statically).

If the event handler is run within the context of a software transactional memory, it can itself be safely aborted if it contains an infinite loop, and it can be run concurrently with other event handlers, perhaps scheduled according to a priority system.

Schema upgrade, where the structure of your program's data chagnes, is a big problem in general for changing systems live, in particular for moving a code change from one instance of a system to another (testing to production, for example, or laptop to cellphone, or from the change's original author's system to the machine of  someone else who wants to use it).  Current practice with things like Rails and Django is to add your schema changes to an append-only log of "migrations".  Using relational databases reduces this problem, because to a significant extent it decouples the program's interface to the data from the efficiency of data retrieval, both beccause of indices and because of views; but the schema upgrade problem still exists.

