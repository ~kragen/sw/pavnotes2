One difficulty with "energy-amplifier" subcritical nuclear fission
reactors (reactors that can't sustain a chain reaction, like those
using thorium), is where to get the neutrons.  If neutrons were free,
these would be an easy improvement over conventional critical-mass
reactors.

Spallation with fast protons is the usual source; traditionally this
is done with mercury to avoid concerns with structural damage to the
spallation target, but molten lead should work just as well, and is a
good deal cheaper, with less risk of toxicity.  Such nuclear
spallation can increase the baryons available for stimulating
reactions by over an order of magnitude, while also reducing their
velocity, which is typically also helpful.  I see that [the molten
lead target idea is already in use in these accelerator-driven
subcritical reactors][4].

[4]: https://en.wikipedia.org/wiki/Accelerator-driven_subcritical_reactor

As an example, the MYRRHA reactor at SCK CEN is planned to, in 02035,
use a 600 MeV 4 mA proton accelerator generating neutrons by
spallation from a liquid lead-bismuth eutectic target to provoke
fissions in uranium and plutonium.

(For neutron activation analysis ("NAA"), evidently sometimes people
use neutron sources made with an alpha emitter stimulating neutron
emission from beryllium-9.  Alpha-beryllium sources produce about 30
neutrons per alpha.)

Current spallation sources like Oak Ridge's Spallation Neutron Source
(using a mercury target) and the soon-to-be-finished European
Spallation Source (which will use a tungsten target) are sufficiently
efficient for actinide-based fuels, but still too inefficient to get
net energy out of more widely available fuels like lithium or
aluminum; I suspect this is in part because they use GeV protons,
which requires a great deal of costly precision.  I think but do not
know that the non-fissile actinide-based fuels require fast neutrons,
while I think more everyday materials could get by with thermal
neutrons, which can in theory be produced in larger quantities.

Lithium fuels
-------------

Lithium, the first element the humans fissioned ([on April 14, 01932,
by Cockcroft and Walton][0]), seems like a more promising fuel source
than the traditional thorium for two reasons: thorium is less abundant
in Earth's crust (perhaps 10 ppm vs. 20 ppm), in urban soils (50 ppm
vs. undetectable), in seawater (0.001 ppb vs. 180 ppb), and in the
solar system (2 ppt mole fraction vs. 10 ppt); and thorium is more
hazardous to humans.

[0]: http://rspa.royalsocietypublishing.org/content/137/831/229.full.pdf "J. D. Cockcroft and E. T. S. Walton, Experiments with High Velocity Positive Ions. II. The Disintegration of Elements by High Velocity Protons, Proceedings of the Royal Society A, vol. 137, pp. 229–242, 01932."

Lithium-6 fissions to tritium upon absorbing a neutron (fast or slow)
and yields 4.8 MeV; the more abundant (95% or so) lithium-7 can absorb
a fast neutron (>5 MeV) and produce tritium (and helium-4 and a slow
neutron), but it soaks up 2.5 MeV in the process.  Tritium, if not
consumed by fusion, spontaneously beta-decays to stable helium-3 with
a half-life of 12.32 years, but releases only 18.6 keV in the process;
thermal neutrons can convert helium-3 back to tritium, expelling a
proton.  Lithium-6's neutron cross section is 940 barns, compared to
lithium-7's 45 millibarns.  (I'm assuming these numbers are for fast
neutrons, not thermal neutrons, but for most elements that only
changes the cross section by a factor of 2 or so.)

So one kilogram of lithium contains about 50 g of lithium-6, about
5e24 atoms; if you can get 5 MeV per atom, that's 4 terajoules, and 4
TJ/kg is a pretty decent energy density.  If we multiply by the 20 ppm
of Earth's crust that's still 80 MJ/kg, which is a bit better than
diesel fuel.  But that concentration is quite variable; lithium is
3.7% (37000 ppm) of spodumene (LiAl(SiO₃)₂), for example, which is
mined for its lithium content at, for example, the Manono Lithium and
Tin Project.  At 4 TJ/kg lithium that's 150 GJ/kg spodumene.

Cockcroft and Walton's fission used a proton (at only about 300 keV I
think) to convert lithium-7 into two alpha particles, yielding, I
think, 17.2 MeV.  Oliphant later improved on their accelerator by
producing a 600-kV Cockcroft–Walton generator and was able to
reproduce their results, as well as synthesizing helium-3 and tritium
for the first time by fusion.

Cockcroft and Walton's reaction is by way of beta decay from
lithium-8, with a half-life of 839 milliseconds, to beryllium-8, which
then decays to two alphas in tens of attoseconds.  But in the Castle
Bravo test it was discovered that *fast* neutrons *promptly* fission
lithium-7 into an alpha, a tritium, and another neutron, yielding
15.123 MeV: less energy, but fast enough to contribute to the
explosion.  Oops.  The accidentally larger explosion that resulted
made about 1200 people sick and probably killed somewhere around 100
people, some of whom were studied in Project 4.1, and spread nuclear
fallout all the way around the world, resulting in an international
diplomatic incident.

With current electronics, maybe you could imagine a linear accelerator
(maybe something like a traveling-wave amplifier tube) of some 200 mm
focused on a molten-lead target, producing perhaps 20 neutrons per
proton (a typical lower limit), stimulating the heating of a lithium-6
target through fission; possibly the lithium could be contained in
lithium oxide (melting point 1438°), lithiophosphate (Li₃PO₄, not sure
if it's refractory), or spodumene (converts to β-spodumene at 900°)
without causing too much trouble with the neutrons.  Then the heat can
drive a heat engine to harvest the energy.

100 mg of lithium-6 would eventually yield 8 GJ of energy through such
a reaction.  If this were operated at 10 watts (thermal) it would take
25 years to exhaust the fuel.

I don't have a clear picture of how collimated is the neutron beam
from neutron spallation or how much thickness of lithium would be
required to absorb it, but I have the impression that the answer is on
the order of centimeters rather than millimeters or decimeters.  A
backstop of baryta or solid lead or something might be necessary.
[Wikipedia explains][1] the relevant math, but I haven't done the
calculation:

> Quantitatively, the number \\(dN\\) of [particles] absorbed, between
> the points \\(x\\) and \\(x + dx\\) along the path of a beam is the
> product of the number \\(N\\) of photons penetrating to depth
> \\(x\\) times the number \\(n\\) of absorbing molecules per unit
> volume times the absorption cross section \\(\\sigma\\):
>
> \\(\\frac {dN}{dx}}=-Nn\\sigma\\)

[1]: https://en.wikipedia.org/wiki/Absorption_cross_section

10 watts of 4.8-MeV fissions is 13 TBq; if we figure on a spallation
multiplication factor of 20 fissions per proton, that's a proton beam
current of 0.1 microamps, 6.5 × 10¹¹ protons/second, which would be 30
mW at 300 kV.  This means your proton accelerator needs to be at least
0.3% efficient, probably better than that due to neutrons that get
wasted on non-lithium atoms.

But can you get neutron spallation at all at such low energies?
Spallation wasn't observed in particle accelerators until 01947 (by
Cunningham at Berkeley); I think this may have been because Lawrence
built a 350 MeV 4.7-meter synchro-cyclotron in 01946, suggesting that
you just don't get spallation until you get close to a GeV.  See
[these notes on ADS neutron sources][7].

[7]: https://web.archive.org/web/20190303183952if_/http://pdfs.semanticscholar.org/ba08/30dcab221b45ca5bcc3cfa8ae82558d624e7.pdf "Krása, Antonín (May 2010). 'Neutron Sources for ADS' (PDF). Faculty of Nuclear Sciences and Physical Engineering. Czech Technical University in Prague. S2CID 28796927."

Interestingly, even a Farnsworth fusor (at Phoenix Nuclear Labs) has
reached 3 × 10¹¹ neutrons per second, and because that's from
deuterium–deuterium fusion, those are 2.45-MeV neutrons, so it might
be possible to get a larger number of thermal neutrons out of that by
spallation.  More typical figures for fusors are more like 3 × 10⁴
neutrons per second, though.

However, [Wikipedia's neutron source article][2] explains that current
spallation sources consume about 30 MeV per spallation neutron
produced, which is too expensive for such lithium fissions to be
profitable, though it is adequate for thorium-based subcritical
nuclear fission reactors.  Worse, this is the energy in the protons (1
GeV/proton ÷ 30 neutrons/proton) without accounting for other energy
losses in the accelerator.

[2]: https://en.wikipedia.org/wiki/Neutron_source

Subcritical reactors that generate their neutrons through fusion are
termed "hybrid fusion–fission" reactors, a concept Bethe "strongly
advocated" in the 01970s.  Neutron economy for breeding tritium from
lithium is considered a crucial design question for fusion reactors,
especially hybrids.  See [this 02009 conference proceedings about
hybrids][8].

[8]: http://web.mit.edu/fusion-fission/HybridsPubli/Hybrid_Fusion_Fission_Conference_A.pdf

01930s-style fusion reactions where you blast deuterons into a
metal-deuteride target at sub-MeV energies seem like they might be
more likely to work for these low-energy fuels than spallation.
Obviously I'm not the first person to think of this, though, so
probably there's an obvious problem I'm overlooking.

Conceivably even Munroe-effect-driven inertial-confinement fusion
might produce enough neutrons to get useful amounts of energy out of
such a subcritical reactor, but such reactors are unlikely to be
conveniently portable, safe, or long-lived.

Lithium-based reactions could maybe produce further useful energy for
the reactor through the Jetter cycle: deuterium reacts with lithium-6
to produce two alphas and 22.4 MeV (or 11.18 MeV?), and deuterium
reacts with lithium-7 to produce two alphas, a neutron, and 15.11 MeV.

Another consideration for human-scale or smaller power generators is
that neutron shielding requires close to a meter of atomic material,
so the significant neutron flux used to trigger lithium fission could
be a big drawback.

Aluminum as fuel
----------------

On the binding-energy curve, using the experimental convention where
hydrogen nuclei and free neutrons are at 0 and everything else is
positive, only lithium and a couple of other nuclides have much *less*
binding energy per baryon than a lower nucleon, helium-4 in this case
with 7 MeV per baryon, until we get past the iron peak.  So we can't
expect to start getting much energy from *fission* again until we get
past iron.  But maybe we can get energy by climbing incrementally
further *up* the curve toward iron?

Aluminum is enormously more abundant (80000 ppm in Earth's crust,
38000 ppm in urban soils, and 3'000'000 ppt mole fraction in solar
system, though only 2 ppb in seawater): neutron activation with one
neutron generates sodium-24, with a half life of 14.956 hours and a
beta decay energy of 5.514 MeV, decaying to the stable magnesium-24.
Garden-variety sodium-23 can also produce sodium-24 by capturing a
neutron.  Aluminum also has the advantage that its oxide is stable in
air and more refractory than that of lithium (2072°).

The magnesium product is not isoelectronic with aluminum, so this
transmutation may gradually liberate oxygen from the matrix, but the
remaining oxide is even more refractory than that of aluminum.
Perhaps the most likely outcome is that the sapphire will gradually
transmute into a magnesium oxide-peroxide, since the peroxide is
metastable at STP.

The long half-life of the sodium intermediate potentially poses
difficulties for reactor control, since the heating effects of an
increased neutron flux will be spread over the next few days.  It also
creates a non-negligible probability of a second neutron capture, but
I think that would just produce sodium-25, which decays to the stable
magnesium-25 with a half-life of 59 seconds.

Neutron activation of oxygen is relatively safe: oxygen-16 captures a
neutron, emits a proton, and becomes nitrogen-16, which beta-decays
back to oxygen-16 with a half-life of 7.13 seconds and 6.13 MeV, which
I suppose means that the neutron absorption is endothermic to the tune
of 6.13 MeV.  Oxygen-17 and oxygen-18 are also stable; three neutrons
are required to be captured before reaching oxygen-19, which
beta-decays to fluorine-19 (the stable isotope) with a half-life of
26.47 seconds.  The natural abundance of oxygen-18 (0.2%) is a bigger
potential concern than three-neutron activation.

Aluminum-27 is the only stable isotope of aluminum.  I'm not sure
exactly how you end up with sodium-24 by neutron-activating it; I'd've
expected aluminum-28, which decays (in 2.25 minutes) to silicon-28,
which is the abundant stable isotope.  Perhaps a fast neutron spalls
off a beta particle.

If we do the same energy density calculation for aluminum on the
assumption that we can ultimately get 5.514 MeV per atom (26.981'538
g/mol), disregarding the energy of the transmutation to sodium (which
I don't know), then the energy density of pure aluminum is about 20
TJ/kg, five times better than natural lithium used for only lithium-6.

Actually, I think we can do a better calculation than that: we're
starting with an aluminum-27 atom and a neutron (1.008'664'916
daltons), we get a sodium-24 atom and presumably an alpha particle,
and after beta-decay to magnesium-24 we have a magnesium-24 atom
(23.985'041'7 daltons), the alpha particle (4.002'603'254 daltons),
the beta particle (0.000'548'579'909'0 daltons), and some energy.  So
we're starting out with 26.981538 + 1.008665 = 27.990203 daltons of
rest mass and ending up with 23.9850417 + 4.0026033 + 0.000549 =
27.988194 daltons of rest mass, thus converting 0.002009 daltons to
non-matter forms of energy.  This is 1.871 MeV, which I suppose means
that the sodium transmutation ate up 3.643 MeV of the energy we
eventually get out.  This is also the first time I've ever used *E* =
*mc*² in anger.

This 1.871 MeV per atom of aluminum is 6.692 TJ/kg of aluminum, which
is not as optimistic as the 20 number above, but has the advantage of
being more likely correct.  At 8% aluminum, Earth's crust can thus
yield about 500 GJ/kg.  Gibbsite (Al(OH)₃) is 34.59% aluminum, thus
about 2.31 TJ/kg.  Kaolinite (Al₂Si₂O₅(OH)₄) is 20.90% aluminum, thus
about 1.398 TJ/kg.  Such a reactor might be able to burn natural
bauxite or clays directly as a source of power, but the other
materials present would produce many undesirable radioactive side
products, so purifying it first would be more desirable.

However, see below about neutrino losses, which would wipe out this
possible gain, which is probably a calculation error in any case.

If you were to somehow accidentally instead get aluminum-28, like if
maybe the sodium-24 spallation reaction only happens with fast
neutrons, its silicon-28 decay product weighs 27.976926535 daltons, so
your resulting rest mass is 27.976'926'535 + 0.000'549'580 =
27.977'476'115 daltons, which is even more favorable, so much so that
I wonder if I've made a calculation error; the energy inferred to be
yielded is 27.990203 - 27.977476 = 0.012727 daltons = 11.855 MeV; that
works out to 42.393 TJ/kg.

This is probably also a calculation error because moving up the
binding-energy curve from aluminum-27 to silicon-28 should only give a
tiny increase in binding energy, not over 400 keV per nucleon.

Aluminum forms a stable solid polymeric trihydride "alane", known
since 01947, which could be a reasonable matrix for deuterium, and the
less reactive lithium aluminum hydride (LiAlH₄) from which it is made
is a widely used reducing agent.  Both hydrides are strong enough to
reduce water.

Cobalt is less desirable as a fuel
----------------------------------

Cobalt-60 (59.933'195 daltons) is commercially produced by slow
neutron bombardment of cobalt-59 (the naturally occurring isotope) for
use in radiotherapy, food irradiation, and industrial radiography,
among other purposes, with a half-life of 5.27 years.  This goes well
beyond the 15 hours of sodium-24, so this is not so much an "energy
amplification reactor" as a sort of nuclear battery.  It beta-decays
to stable nickel-60 in an excited state, which relaxes by emitting a
gamma photon of 1.17 MeV and another of 1.33 MeV.

The other issue is that I think the cobalt-60 transmutation is
endothermic (see above about the binding energy curve), so it's not a
fuel anyway.

If you have pure cobalt-60, you get 17.9 watts per gram of gamma
radiation.  This is sort of nice as a gamma radiation source but not
great if what you want is heat; you need a lot of mass to stop those
gammas.

Strontium might work for betavoltaics but it's still probably not a fuel
------------------------------------------------------------------------

Strontium-90 beta-decays (half-life 28.79 years) at 546 keV into
yttrium-90, which beta-decays (half-life 64 hours) at 2.28 MeV, with
"negligible" gamma emissions.  So you could probably use a vacuum
strontium-90-driven betavoltaic as a compact megavolt source.
Historically strontium titanate (melting point 2080°) has been used
for radioisotope thermoelectric generators.

The binding-energy curve issue means that producing strontium-90
probably takes more energy than the beta decays give you, so again
it's more a battery.

Neutron decay isn't practical
-----------------------------

The neutrons from the spallation source are themselves unstable with a
half-life of some 880 seconds, decaying to protium (1.007825031898
daltons, 0.000'839'884 daltons = 782.347 keV less than the neutron),
so in a sense the lithium or aluminum fuels are sort of unnecessary,
though they do give you about an order of magnitude more energy per
neutron.  If you could just wait around for the free neutrons to decay
on their own, you'd get a substantial amount of energy.  The trouble
is that I think you can't confine them that long; they don't respond
to electric fields, and they'll definitely hit some nucleus before
they've been free that long.

Ultracold neutrons have been produced at the Paul Scherrer institute
by using a 5-kelvin deuterium moderator, allowing them to be observed
for "a few minutes", [according to Wikipedia][10].  That's the kind of
measures you'd apparently have to take, but of course running the
necessary cryopumps is pretty expensive, at least here on Earth.

[10]: http://localhost:8080/wikipedia_en_all_maxi_2022-05/A/Paul_Scherrer_Institute

Neutron supermirrors might make it possible to confine warmer neutrons
to a circular path for a sufficient period of time, but I don't think
so.

Other potential fuels
---------------------

The literature on NAA, in which typically 50 mg of an unknown material
is irradiated in a nuclear reactor with some 10¹⁶ thermal
neutrons/m²/s to provoke prompt emissions (gamma or otherwise), ought
to provide information about the characteristics of natural versions
of common elements under these circumstances.  Sometimes epithermal
neutrons or fast neutrons are used instead (ENAA and FNAA).

Wikipedia's page on the subject has this table, assuming *decay* (not
prompt) gamma rays and 10¹⁷ neutrons/m²/s, copied from a page at the
University of Missouri; there's evidently a missing denominator:

<table>
<tr><th>Sensitivity (picograms)		<th>Elements
<tr><td>1				<td>Dy, Eu
<tr><td>1–10			<td>In, Lu, Mn
<tr><td>10–100			<td>Au, Ho, Ir, Re, Sm, W
<tr><td>100–1000		<td>Ag, Ar, As, Br, Cl, Co, Cs, Cu, Er, Ga, Hf, I, La, Sb, Sc, Se, Ta, Tb, Th, Tm, U, V, Yb
<tr><td>1000–10⁴		<td>Al, Ba, Cd, Ce, Cr, Hg, Kr, Gd, Ge, Mo, Na, Nd, Ni, Os, Pd, Rb, Rh, Ru, Sr, Te, Zn, Zr
<tr><td>10⁴–10⁵			<td>Bi, Ca, K, Mg, P, Pt, Si, Sn, Ti, Tl, Xe, Y
<tr><td>10⁵–10⁶			<td>F, Fe, Nb, Ne
<tr><td>10⁷				<td>Pb, S
</table>

A problem here is that in cases where the heavier nuclide has a higher
binding energy (is in a lower energy state), presumably the lighter
nuclide won't absorb cold neutrons at all, because the reaction is
endothermic.  This crucial question determines whether these reactions
are even theoretically capable of producing more energy than they
consume.

Neutrino losses
---------------

Beta decay, like the decay of sodium-24 into magnesium-24 or the decay
of aluminum-28 into silicon-28, generally involves the emission of an
antineutrino.  The antineutrino cannot be confined in anything smaller
than a star, and therefore whatever energy it carries, typically about
half of the energy of the beta decay, is lost.

This is not a problem for the lithium reactions, but on the
aluminum-sodium-magnesium route we lose something like 2.7 MeV, which
is more than the 1.871 MeV we were supposed to get out, and for the
hypothetical aluminum-silicon thing I just made up that maybe nobody
has reported, we would lose 5.9 MeV, which would still be viable if
that reaction is even possible.

This also represents an energy loss in the otherwise harmless neutron
activation of oxygen, which I suppose puts a floor on the energy of
the neutron needed to activate oxygen.

Boron as a fuel
---------------

Boron-10 is sometimes used in bubble chambers as a neutron detector.
I don't know why this works; I'd think you'd just get stable boron-11,
but apparently no, the boron-11 is excited enough that it immediately
throws off an alpha and becomes lithium-7, and this is used for "boron
neutron capture therapy" for cancer, yielding 2.31 MeV right smack in
the middle of your tumor.  The triggering thermal neutron can be under
0.5 eV.  Boron-10 has a lovely huge thermal-neutron capture cross
section of 3837 barns.

Because it's an alpha decay, the neutrino problem doesn't apply; all
2.31 MeV is deposited right there, around the former boron atom.  All
you have to do is keep the gamma ray from escaping.

Natural boron is 20% boron-10, the rest being boron-11 (with an
irrelevantly small neutron cross section, like 0.001 barns), and
though boron is not super common (10 ppm of the earth's crust, 4 ppm
in seawater, 400 ppt mole fraction in the solar system), it's a bit
more available than thorium.  Pure boron-10 (10.0129369 daltons) would
then have 22.3 TJ/kg, which seems like a suspiciously high amount of
energy; natural boron would then be about 4 TJ/kg.  Boria (B₂O₃,
readily obtainable by calcining boric acid at only 300°) is 31% boron
and would then be 1.3 TJ/kg.  Boric acid is about US$3/kg and 17.5%
boron, so about US$0.000015/kilowatt-hour.  Seawater at 4 ppm would
still be 16 MJ/kg if it weren't depleted.  The USGS's Yearbook says
plant leaves are 25–100 ppm boron.

These numbers don't include the energy obtainable by further burning
the resulting lithium.

Crystalline boron is by contrast about US$5000/kg.  While it's nice
that it doesn't melt until 2076°, the price premium may not be worth
it.

So, if boron-10 is such a great fuel when burned with thermal
neutrons, then why do people use it as a neutron poison to shut down
nuclear power plants?  Because it produces two orders of magnitude
less energy per neutron than the actinides do, I guess.

Proton–boron-11 "fusion" has also been investigated as a candidate
aneutronic reaction; peak activity is at 600 keV, yielding three
alphas and 8.7 MeV, the cross section is 3.01 × 10⁻²⁷ m³/s/keV², and
the ignition temperature is 127 keV.  This gives substantially higher
energy densities for natural boron (11.009'305'2 g/mol, so 76 TJ/kg
for boron-11, or 61 TJ/kg for natural boron disregarding the
boron-10), and it is also potentially suitable for use in an
alphavoltaic battery, without a heat engine.

To be more concrete about this, gasoline is 46.4 MJ/kg and 34.2
MJ/liter, both thermal, and typical car engines are about 25%
efficient, so that's about 9 MJ/liter.  If you get 8 liters per 100
km, that's about 72 MJ per 100 km.  A kg of boric acid from the
hardware store contains 175 g boron, presumably natural boron; this is
about 11 TJ, which (if converted into work at perfect efficiency) is
15 million km, 370 times around the world, 17 years of nonstop driving
at 100 km/hour, equivalent to 300 kiloliters of gasoline.  If your
house uses 1000 kWh/month this is 247 years.  If the system is 6%
efficient (typical for betavoltaic batteries) then these drop to 1
year of nonstop driving, 22 times around the world, 19 kiloliters, and
less than 15 years of house operation.

The humans mine about 3 million tonnes of borates per year, mostly in
Turkey; 0.2 million tonnes of this is in the Puna region of Argentina
at the Tincaluyu and Sijes mines.  Most of this is borax,
Na₂B₄O₇·10H₂O, which is 11.13% boron, so we can roughly guess 0.3
million tonnes of boron per year.  At 61 TJ/kg this would be 580 TW,
roughly 30 times world marketed energy consumption.

A simple alphavoltaic generator sketch
--------------------------------------

Two cylindrical chambers 10 mm long at 1 Pa separated by a thin window
of mica or gold.  The upper chamber is a cation gun; it contains a
corona-source anode generating positive ions from a gas (hydrogen,
deuterium, or helium) with perhaps 1000 volts relative to the chamber,
using a sharp metal point or a bundle of carbon fibers.  A potential
gradient in the upper chamber guides the ions toward the window; a
magnetic lens focuses them on it.  Once through the window into the
lower chamber the ions are accelerated through a much larger
potential, around 1 MV, onto a target cathode containing one or more
of boron, lithium, deuterium, and beryllium.  Reactions provoked by
the 1 MeV ions launch alpha particles from the target onto the walls
of the lower chamber, fighting the 1 MeV voltage gradient.  The
alpha-particle current thus reinforces the negative voltage of the
target with respect to the walls.

The helium thus produced raises the lower chamber to 1 Pa as well, at
which point it must be pumped off.  This is about 300 picomoles of
gas, 1.2 ng of helium.  Otherwise the ion beam will not be able to
penetrate the lower chamber.  This is after about 1e14 reactions,
about 30 microcoulombs, or about 28 J at 1 MV.

When the corona source is not operating, almost no activity occurs in
the target, so you can turn the power up and down as desired; ideally
for every electron sucked out of the gas at 1kV at the corona source
we generate three alpha particles at the target, which impact the
walls of the lower chamber and carry the charge of 6 protons at 1MV.
So the power out is 6000 times the power in.  In practice some of the
ions will recombine, some will fail to make it through the window, and
probably half the alpha particles will be launched into the target
rather than away from it, so a factor of 1000 seems like a practical
upper limit.

The output terminals of the generator --- the target and the lower
chamber walls --- have a capacitance of some 0.1 pF.  Lacking the
minimal couple hundred kV in the correct polarity, the ion beam will
fail to be strike the target, or strike it with enough energy, and the
generator will fail to operate.  So about 2 mJ of bias energy is
needed for startup.

Conceivably a thin layer of hydrogen-rich material on the window, such
as polyethylene or polyethylene terephthalate, could multiply the
quantity of protons produced that can make it through the window, as
well as slowing down diffusion further.

For 1 watt of output we need 1 microamp of alpha-particle current at 1
MeV, which is maybe 0.3 microamps of ionic current, about 2 TBq.

Paschen may mug us!  If the gas density in the lower chamber is too
high, Townsend discharge will steal a significant fraction of our
output current and can even discharge the bias needed to provoke the
reaction; and a full-fledged spark discharge could of course discharge
the entire bias potential, which is what happened in Moseley's
original 01913 radon-driven betavoltaic battery.  But air at room
temperature and 1 Pa (0.01 hPa, 0.01 mbar, 8 millitorr, 8 microns Hg)
has a mean free path of about 10 mm, so we probably don't need to
worry; helium will be, if anything, better, and most thermalized ions
that arise will fly all the way to the chamber wall without
encountering any other molecules to ionize.  (But Moseley said, "In
the present experiments there can be no doubt that the process of
exhaustion was really exhaustion was really effective, as special
tests were made to guard against the possibility of mistake.  The
residual pressure was therefore so small that ordinary ionisation by
collision was out of the question.")

How big does the corona region need to be?  The gas in the upper
chamber (mostly hydrogen except for whatever helium may have managed
to diffuse through the window) has some 10¹⁴ molecules per cc at 1 Pa,
and being hydrogen its molecules will be going about 2 km/s.  This
works out to about 2 × 10²³ molecules per second passing through any
m² cross-section.  Dividing our desired 2 TBq by this number we get a
necessary cross-section of 10⁻¹¹ m², roughly a 3-micron-diameter
circle.

I had to look up how strong electric fields around sharp points work.
In classical electrodynamics, the electrical potential around a
uniformly surface-charged conductive sphere is the same as the
potential around a point charge at its center with the same charge:
*Q*/4*πrε*₀.  So the voltage falls off inversely proportional to the
distance from the center, and the field is its derivative.

If we're using just a single metal needle at 1000 V with a tip radius
of 1.5 microns, the voltage should then fall off inversely
proportional to the distance from the center of the tip: 500 volts at
1.5 microns from the surface of the tip, 250 volts at 4.5 microns from
the surface, etc.  So the derivative of voltage, and thus the field,
is proportional to -1/r².  Because I can't brain I just calculated
numerically that this is about 666 MV/m at the surface.

This is 200 times higher than the (avalanche) breakdown electrical
field strength of air and close to the field strength that would be
needed to produce field electron emission if it were going the other
way, so I'm guessing it's probably high enough to ionize hydrogen.  (I
know the necessary energy is 13.6 eV but I don't know how that relates
to volts per meter; a crude approximation is that a hydrogen atom's
van der Waals radius is 120 pm, and 13.6 V over 240 pm is 57 GV/m,
another two orders of magnitude higher.)

Interestingly, if we make the point smaller, it seems that the voltage
at the radius 1.5 microns will be lower, and so will the field
strength at that radius.  Around the smaller point there will be an
even higher field strength, but with a smaller cross section.  At
ordinary pressures this wouldn't matter because ions produced in the
smaller, stronger field would escape to create avalanche discharge in
the surrounding region, but we're making sure the pressure is too low
to allow avalanche discharge within our device, so that doesn't help
us.

At any rate, this simple needle ion generator seems like it would
probably be adequate.  If not, a bundle of needles, or a bundle of
carbon fibers like those used to provoke corona discharge in current
cheap household air ionizers, could increase the interaction area by
two or three orders of magnitude.

Operating this alphavoltaic generator at the aforementioned 1 watt, 1
microamp, and 1 MeV would produce probably 6 TBq of alpha emissions
within the target chamber, which works out to about 10 picomoles per
second of helium.  As I mentioned above, unless I've miscalculated
something, this would exceed our desired 1 Pa pressure within 30
seconds (assuming 10 mm diameter 10 mm long cylindrical space) so we
need to evacuate the working space continuously, unless we're in outer
space.

(We also need to supply new hydrogen to the ion gun, but that's an
easy problem to solve.)

This seems to pose real difficulties!  How can we remove the helium
from the target chamber?  Room-temperature sorption pumps probably
won't work because they have a reputation for being unable to
effectively pump helium even when cooled by liquid nitrogen.  Perhaps
a second ion gun in the target chamber could ionize the helium and
throw it through a second window into a large exhaust chamber
maintained at higher pressure.  Since it would be operating near the
potential of the target chamber wall, perhaps 1000 volts more
positive, it wouldn't affect the ion beam onto the target much; at
worst it would slightly repel it.  This would require about the same
current as the generator outputs, but at a much lower voltage.  The
main difference from the original ion gun is that it needs to pump
three times as much gas, and the gas is harder to ionize.

But if this ion-pump thing works, surely someone would have done it
already?  Apparently [there is an ion pump used in vacuum work][22]
but normally instead of throwing ions through a thin window it throws
them into a titanium cathode:

> A swirling cloud of electrons produced by an electric discharge is
> temporarily stored in the anode region of a Penning trap. These
> electrons ionize incoming gas atoms and molecules. The resultant
> swirling ions are accelerated to strike a chemically active cathode
> (usually titanium). On impact the accelerated ions will either
> become buried within the cathode or sputter cathode material onto
> the walls of the pump. The freshly sputtered chemically active
> cathode material acts as a getter that then evacuates the gas by
> both chemisorption and physisorption resulting in a net pumping
> action. Inert and lighter gases, such as He and H₂ tend not to
> sputter and are absorbed by physisorption. Some fraction of the
> energetic gas ions (including gas that is not chemically active with
> the cathode material) can strike the cathode and acquire an electron
> from the surface, neutralizing it as it rebounds. These rebounding
> energetic neutrals are buried in exposed pump surfaces.

[22]: https://en.wikipedia.org/wiki/Ion_pump_(physics)

This was published in 01958, patented in US patent 2,993,638 in 01961,
and you can buy them off-the-shelf from HP now.

Assuming single ionization and 1000 volts, 1 keV divided by
Boltzmann's constant gives us a 12-megakelvin temperature for the ions
being thrown at the cathode.

As I understand it, mica windows are commonly used for this sort of
thing, though the Geiger–Marsden experiment used gold leaf, but if
you're willing to sacrifice some small fraction of the window area to
a high-aspect-ratio grate, and you have sufficient geometric freedom,
you can support arbitrarily large pressures with arbitrarily thin or
thick grates.  There will be some diffusion back through the grate,
which will increase as the pressure difference increases, but the ion
gun only needs to exceed the speed of that diffusion to keep pumping.
(Detaching a thick anodization layer from aluminum may be a useful way
to get such a grate-supported window: the nanopores are typically
10–150 nm in diameter, while the coating is 500 nm to 150 μm in
thickness, and the pores can be closed once the coating is thick
enough, and usually are ("sealing").)

[The stopping power of all the elements is well characterized][24] for
Rutherford backscattering spectroscopy (RBS) and high-energy ion
scattering (HEIS) spectroscopy and is typically on the order of a MeV
per micron, so at only a keV you might need a nanometer-thick window.
This might explain why this is not a conventional approach: you
probably need several keV and a nanometers-thick window supported by a
reinforcing grate.  Implanting ions a few nanometers under the surface
of a block of titanium is a lot easier!

[24]: http://localhost:8080/wikipedia_en_all_maxi_2022-05/A/Stopping_power_(particle_radiation)

Mechanically connecting the target cathode to the chamber without
breakdown is potentially challenging.  The problem isn't leakage
current; a microamp of leakage at a megavolt would require a teraohm
of resistance.  Sulfur has about 10¹⁵ Ω m of resistivity, so a sulfur
ring separating a 2.5-mm-radius target cathode from a 5-mm-radius
target chamber casing might be 2.5 mm × 2.5 mm in cross-section with a
length of some *π* 7.5 mm = 24 mm, giving roughly 40 petaohms of
resistance, four orders of magnitude better than required.  Fused
quartz, PET, and teflon are even better.  But not breaking down under
a megavolt over 2.5 mm requires withstanding 400 MV/m (400
volts/micron), and PTFE is not up to that; of common materials, only
some types of fused quartz ([470–670 MV/m][25]) are adequate.  Ruby,
borosilicate, porcelain, and teflon are all about an order of
magnitude too weak, and I'm not sure about sulfur.

[25]: https://en.wikipedia.org/wiki/Dielectric_strength

All that megavolt nonsense needs to be contained inside a solid
insulating block with high dielectric strength, in order to work at
all, not to be safe.  Air's dielectric strength against flashover is
only 3 MV/m, so any two megavolt terminals exposed outside would need
to be separated by 300 mm, impossible on a 20-mm-long device.
Standard creepage allowances are 20–25 mm/kV, so you'd also need 20
meters of creepage, but that's excessive in this context; Van de
Graaff's 2-megavolt generator has its top terminals on pillars of only
6.7 meters.

What about capacity?  Suppose the target is a 2-micron-thick layer of
boron-11 on some kind of 5-mm-diameter metallic substrate, total 39
nanoliters; at 2.5 g/cc that's 98 μg.  That's about 9 μmol of
boron-11, which is about 5 × 10¹⁸ atoms.  If each atom gives us 6
protons (wasting half by shooting alphas further into the target)
that's 5.2 coulombs, 5.2 megajoules at a megavolt.  That's about 2
months of runtime at 1 watt.

But no buildup will be left by the reaction; all the reacted boron
that doesn't simply evaporate as helium will be sputtered off by the
12-billion-degree plasma.  So there's no reason the fuel layer on the
target need be only microns thick.  It can be 2 mm thick just as
easily, as long as it has enough embedded conductors for a microamp,
so instead of 5 MJ and 2 months we have 5 GJ and 160 years.  It's
likely that alpha-particle damage will render it inoperable before
that point, though.

For human-contact applications, electrical safety seems like not much
of a problem.  The stored electrical energy is just the bias energy at
the maximum bias voltage: 50 mJ assuming 0.1 pF, three orders of
magnitude too small to be fatal to a human.  It's similar to a Van de
Graaff generator in that sense.

Neutron and gamma safety may be a bigger issue, not only for
human-contact applications but also for repairs and recycling.
