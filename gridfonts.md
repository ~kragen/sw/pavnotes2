I enjoyed Douglas Hofstadter’s 26-lowercase-letterform “gridfonts”
when I read his _Fluid Concepts and Creative Analogies_, probably 30
years ago now, or possibly his 01985 [_Metamagical Themas_][36], which
reprinted his Scientific American column from September 01981 in which
he introduced gridfonts to the public (pp. 599 et seq. (601/847 et
seq.)).

Today I was looking around for compact stroke fonts (see file
`hershey-fonts.md`) and remembered them, and it occurred to me that
they might be a source of compact stroke fonts, so I went looking, and
ended up producing [a PDF with specimens of 287 gridfonts I found][2].
And I think that, quite aside from their potential utility in
investigating AI (something of which Hofstadter is [no longer such a
fan][4]!) this is potentially a really fertile approach to typography.

[2]: http://canonical.org/~kragen/sw/dev3/all-gridfonts.pdf
[4]: https://www.lesswrong.com/posts/kAmgdEjq2eYQkB5PP/douglas-hofstadter-changes-his-mind-on-deep-learning-and-ai
[36]: https://archive.org/details/MetamagicalThemas

Hunting down the lost gridfonts data, or some of it anyway
----------------------------------------------------------

I was hoping to find some kind of numerical listing of the coordinates
for each gridfont or something so that I could reconstitute readable
stroke fonts from them.

It turns out two of Hofstadter’s grad students did their dissertations
on gridfonts, John Rehling and Gary McGraw.  [What I guess was
McGraw’s web server used to have a page about his thesis][8], linked
from [Alex Linhares’s FARGonautica repo][12], which contains some
illustrations of gridfonts, but the server is gone now, and the
Internet Archive did not save the copies of the thesis he linked.

[8]: https://web.archive.org/web/20020610155828/http://goosie.cogsci.indiana.edu/farg/mcgrawg/thesis.html
[12]: https://github.com/Alex-Linhares/FARGonautica

Linhares says McGraw’s and Rehling’s code was written in Scheme and is
available.  After enough work I did finally find it; see below.

McGraw has a shiny home page [with a section about his thesis][13],
which seems to be a reskinned version of the old page and links to a
[403 Forbidden at IU][15], and another page about [his other academic
research][14] before he went into industry.  Going up the tree from
the 403 eventually gets to a redirect to [the Internet Archive’s copy
of the FARG tree.][16]

[13]: https://www.garymcgraw.com/technology/writings/thesis/
[14]: https://www.garymcgraw.com/technology/writings/pubs-archive/
[15]: https://www.cogsci.indiana.edu/farg/mcgrawg/thesis/thesis.ps.gz
[16]: https://wayback.archive-it.org/219/20150904155541/http://www.cogsci.indiana.edu/farg/

FARG was Hofstadter’s cognitive science group.  The FARG’s whole web
presence at IU has gradually fallen into disrepair, to the point that
I checked Wikipedia to see if Hofstadter was still alive (he is, at
78), but IU has entrusted the data to the Internet Archive, so I was
able to find [the archived page on gridfonts][0], which shows the 16
specimens from the book.  This temptingly says:

> John’s thesis, available in [the publications section of this
> site][3], is highly recommended for its coherent and complete
> analysis of the gridfont domain.

Well, the publications page linked doesn’t actually list John’s
thesis, but it says,

> Also, older theses and other publications are available from our
> public FTP server.

The “our public FTP server” link actually points to an HTTP server,
but the page has been 404 since 02015; but by looking further back in
the archives, I was able to find [a working copy][6], which has stuff
up to 01998, not including John Rehling’s thesis but including
McGraw’s earlier thesis, and a file named [gridfonts.data][7], which
contains 287 fonts!

[0]: https://wayback.archive-it.org/219/20220524142402/https://cogsci.indiana.edu/gridfonts.html
[3]: https://wayback.archive-it.org/219/20220524135835/https://cogsci.indiana.edu/papers.html
[6]: https://wayback.archive-it.org/219/20060606215619/https://www.cogsci.indiana.edu/pub/
[7]: https://wayback.archive-it.org/219/20060606215909/http://www.cogsci.indiana.edu/pub/gridfonts.data

There’s also a link to the [“full publication list” page][9].  It
links McGraw’s thesis in the /pub/ directory, but though it *lists*
Rehling’s thesis, it conspicuously omits any link to it.  There don’t
seem to be any links to datasets or code or anything.

[9]: https://wayback.archive-it.org/219/20220524151503/https://cogsci.indiana.edu/phard.html

Digging around the archived FARG tree, [McGraw’s directory][17] has
some code, but nothing obviously relevant, mostly just some Java.
However, his [Letter Spirit page][18] contains what seems to be the
original version of the gridfonts specimens printed in the book.  His
[thesis directory][19] has what seem to be the source code *for the
dissertation in TeX* but the only Lisp code is [a short script to open
some TeX files][20].  I suspect you could make a much better PDF file
from the TeX sources here, one with a table of contents and good
fonts, but I haven’t downloaded them to try.

[17]: https://wayback.archive-it.org/219/20150904155947/http://www.cogsci.indiana.edu/farg/mcgrawg/
[20]: https://wayback.archive-it.org/219/20150904162031/http://www.cogsci.indiana.edu/farg/mcgrawg/thesis/e-loader.l
[19]: https://wayback.archive-it.org/219/20150904161131/http://www.cogsci.indiana.edu/farg/mcgrawg/thesis/
[18]: https://wayback.archive-it.org/219/20150904161105/http://www.cogsci.indiana.edu/farg/mcgrawg/lspirit.html

[Rehling’s directory][21] has a link to his own [Letter Spirit
subdirectory][22], which includes [a different gridfonts specimen
sheet in PostScript][23] and some very tempting notes:

    grid.ps shows the grid on which gridletters are created
    majorFonts.ps is a collection of gridfonts designed by humans

    osu/ contains the gridfonts displayed at Ohio State University in the
    fall of 1999.

    samples/seeded contains gridfonts created by the Letter Spirit program
    when given a few seed letters which were created by a human to start
    it off; usually the seeds are {b, c, e, f, g}

    samples/self contains gridfonts created by the Letter Spirit program
    operating from scratch, without any seeds from a human.

    nyc/ contains a reconstituted version of some of the above, with a
    couple of new pieces mixed in.

    nyc/loader.html throws several gridfonts at you, in black and white,
    with no additional explanation, but is nice eye candy. Most fit the
    description given above for "samples/seeded"...

    Many of the examples are color-coded. The system of color-coding has
    changed throughout development, so a simple explanation of the scheme
    is not possible; roughly, darker colors are seeds, or have been
    rated highly by the program. The output here has been web-posted
    without great elaboration in an effort to make them available to
    interested parties as soon as possible.

    John Rehling
    Bloomington, IN
    August 16, 2000

[21]: https://wayback.archive-it.org/219/20150904155956/http://www.cogsci.indiana.edu/farg/rehling.html
[23]: https://wayback.archive-it.org/219/20150904154722/http://www.cogsci.indiana.edu/farg/rehling/lspirit/majorFonts.ps
[22]: https://wayback.archive-it.org/219/20150904154722/http://www.cogsci.indiana.edu/farg/rehling/lspirit/

And in fact there’s [a subdirectory with the Letter Spirit Scheme code
in it!][24] I haven’t verified that this is complete, but the files I
looked at do seem to be archived, and presumably that was part of IU’s
deal with Archive-It.  Somewhere in there, the file format must be
implemented in code, but I hadn’t found this directory yet when I
reverse-engineered it.  In [FILE-STRUCTURE][25] there's a section that
calls out fonts.ss (17K), gridfonts.ss (9K), and ls-out.ss (10K) as
“databases of gridfonts”.

[24]: https://wayback.archive-it.org/219/20150904155348/http://www.cogsci.indiana.edu/farg/rehling/lspirit/code-files/
[25]: https://wayback.archive-it.org/219/20150904155643/http://www.cogsci.indiana.edu/farg/rehling/lspirit/code-files/FILE-STRUCTURE

These are in Scheme.  [fonts.ss][26] contains 15 fonts, including
“Benzene Left”.  These are represented as sorted lists of integers;
see below.

[26]: https://wayback.archive-it.org/219/20150904155719/http://www.cogsci.indiana.edu/farg/rehling/lspirit/code-files/ls-out.ss

There are [osu/][30] and [nyc/][31] subdirectories which contain
graphics of gridfonts, including a [showcase page][32], but apparently
no machine-readable gridfonts data.  It would be possible to
reverse-engineer the gridfonts from the images.

[30]: https://wayback.archive-it.org/219/20150904155354/http://www.cogsci.indiana.edu/farg/rehling/lspirit/osu/
[32]: https://wayback.archive-it.org/219/20150904155752/http://www.cogsci.indiana.edu/farg/rehling/lspirit/nyc/loader.html
[31]: https://wayback.archive-it.org/219/20150904155353/http://www.cogsci.indiana.edu/farg/rehling/lspirit/nyc/

Rehling’s elusive thesis has [a subdirectory allocated to it][33],
and, at long last, [an actual PDF file!][34] It’s 429 pages.  Its
acknowledgments section offers a rare peek at the human dynamics
inside FARG.

[33]: https://wayback.archive-it.org/219/20150904155356/http://www.cogsci.indiana.edu/farg/rehling/lspirit/thesis/
[34]: https://wayback.archive-it.org/219/20150904155827/http://www.cogsci.indiana.edu/farg/rehling/lspirit/thesis/thesis.pdf

The gridfonts.data file format
------------------------------

McGraw’s papers say many more gridfonts existed (“about 600” as of
01992).  And Hofstadter’s 01985 book says that, at the time, gridfonts
were “a still-growing collection of (currently) about 300 skeletal
typefaces.”  Still, 287 is a pretty good start.  The fonts in
[gridfonts.data][7] are encoded in this format:

    font : bowtie
    creator : doug
    create date : Tue Feb 19 15:39:48 EST 1991
    last edit : 
    a 0A000000030060
    b 0A014000830060
    c 04000040090030
    d 05011000090430
    e 014000000C0060
    f 18000480C00600
    g 0140000A0C1060
    h 080140C08A0060
    i 800000808A0820
    j 80000014882822
    k 094141008C0060
    l 00014880820020
    m 080003C00C0080
    n 000000C00A0060
    o 020000000900E0
    p 028000220900C2
    q 014400080C1061
    r 000002800A0060
    s 06000000030060
    t 09002000090030
    u 00000C40050060
    v 000006000900A0
    w 00800E40020030
    x 00800A000600D0
    y 00400C0A051060
    z 09000000090030
    comment :

All these fonts seem to be from 01991 or 01994.

There was no evident documentation for this format, so I spent a
couple of hours reverse-engineering it, helped by understanding the
grid; [McGraw’s 01992 paper][10] explains:

> Thus the Letter Spirit grid was born.  The grid has 21 points
> arranged in a 3×7 array.  Legal line segments, called *quanta*, are
> those that connect any point to any of its nearest neighbors,
> horizontally, vertically, or diagonally.  There are 56 possible
> quanta on the grid.

[10]: https://wayback.archive-it.org/219/20060606220159/http://www.cogsci.indiana.edu/pub/mcgraw.letter_spirit.ps

The 14-hexadecimal-digit strings following the letters express the
letterform as a 56-bit quantity:

- the first 14 bits describe whether each of the 14 possible
  horizontal quanta are present;
- the next 18 describe whether each of the 18 possible vertical quanta
  are present;
- the next 12 describe whether each of the 12 possible “/” diagonal
  quanta are present (that is, from the lower left to the upper right
  inside each of the 12 grid squares);
- the remaining 12 describe whether each of the 12 possible “\\”
  diagonal quanta are present (that is, from the upper left to the
  lower right inside each of the 12 grid squares).

Each of these four arrays is divided into rows, ordered top to bottom,
and the quanta within the row are ordered left to right.  The arrays
consist respectively of 7 rows of 2 quanta, 6 rows of 3 quanta, 6 rows
of 2 quanta, and 6 rows of 2 quanta.  1 bits mean to draw a line; 0
bits mean to omit it.  The division into bitfields might be written as
follows:

    _ FFFC0000000000
    | 0003FFFF000000
    / 00000000FFF000
    \ 00000000000FFF

...and now that I've done that, I’ve finally found Rehling’s
dissertation (linked above), which describes this ordering of quanta
with a helpful full-page diagram on p. 25 (44/429).  That could have
saved me quite a bit of effort!  But it took me several times longer
to find Rehling’s dissertation than to do the reverse-engineering.
Rehling’s dissertation still doesn’t mention hexadecimal, though.

The Scheme format contains additional gridfonts
-----------------------------------------------

I suspect that Rehling’s dissertation, linked above, would be a good
thing to read if you wanted to dive into this code.

In [ls-out.ss][27] there are 15 fonts presumably produced by the
Letter Spirit program, with the letterforms represented as sorted
lists of integers.  One reads, in part:

    (benzene-right
     (b (5 8 14 17 20 22 23 36 39))
     (c (5 8 23 36 39))
     (e (5 6 8 23 36 37 39))
     (f (18 21 24 33 36 5))
     (g (5 8 12 22 23 25 28 36 39 43))
     (q (5 8 22 23 25 28 31 36 39))
     (r (5 23 36)) 
     ...)

These are presumably lists of the quanta that are to be drawn, the
same as the set bits in the hexadecimal bitvector.  None of the
integers seems to be less than 0 or more than 55, and their
populations seem consonant with that interpretation; “r” in Benzene
Right really does contain three quanta, all three of which are also in
“c”.  There are some fonts in here that aren’t represented in
gridfonts.data.  Nevertheless, I haven’t made the effort to decode
them yet.

The footer file in the code-files directory reads in part:

    Several functions allow different ways of running the program. The
    most basic and general is probably to call design-gridfont with
    seeds. For example,

    (design-gridfont '((4 5 6 7 8 9 22 23 25) (4 5 8 9 14 17 20 22 23
    25)))

    will take those two gridletters, expressed in terms of lists of
    quanta, and start Letter Spirit running on it.

The archived [fonts.ss][28] contains I think 23 gridfonts in the same
format, again including Benzene Right (this time with its letters in
alphabetical order), and [gridfonts.ss][29] contains something quite
different, more 14-digit hexadecimal strings, this time in
inconsistent case:

    (set! *psych-fonts*
          '((a "058002400b0000")
            (a "0B800140010040")
            (a "09800040030040")
            (a "0f800340010000")
            (a "0b000040010060")
            (b "04824b00090000")
            (b "08424940000060")
            ...))

These presumably represent letterforms in the same encoding as
gridfonts.data, but, again, I haven’t taken the time to decode them
yet.

[27]: https://wayback.archive-it.org/219/20150904155719/http://www.cogsci.indiana.edu/farg/rehling/lspirit/code-files/ls-out.ss
[29]: https://wayback.archive-it.org/219/20150904155707/http://www.cogsci.indiana.edu/farg/rehling/lspirit/code-files/gridfonts.ss
[28]: https://wayback.archive-it.org/219/20150904155659/http://www.cogsci.indiana.edu/farg/rehling/lspirit/code-files/fonts.ss

Doing something with the gridfonts
----------------------------------

I wrote a [quick kludgy Python program][11] to render the gridfonts
from gridfonts.data as PostScript, adding a line to a PostScript path
for each quantum and then stroking the path, which is how I got [the
specimens PDF linked above][2].  This is a good start for making
images, but not yet enough for making stroke fonts I can use for
rendering text.

[11]: http://canonical.org/~kragen/sw/dev3/gridfontparse.py

### Adding more letterforms? ###

All the gridfonts in their current form consist entirely of lowercase
ASCII letters.  This is enough for certain artistic purposes, like
composing logos, graffiti, or commercial signage, but you can’t
exactly use them to edit your Python or typeset a book.  But you could
probably manually expand the repertoire of any particular gridfont
from 26 characters to printable ASCII’s 95 within a day, depending on
how high your standards were.  Maybe you could train a neural network
to do it for you once you had a few examples.

### Generating stroke fonts? ###

Transforming gridfonts into stroke fonts such as Hershey fonts should
be straightforward; the simplest approach is to create a stroke for
each quantum in the gridfont, but it’s more efficient to join
collinear quanta into longer strokes, and it’s often desirable to join
strokes that share an endpoint into a longer polyline, for example to
avoid storing the same endpoint twice or to get better line joins when
the path is stroked.  These are fairly straightforward to do, but I
haven’t done them yet.

Hershey fonts do support sharing endpoints but don’t support closing
the path; PostScript paths support both.

The tileset-graph-rewriting approach described below is an alternative
way to generate a stroke font.

### Generating outline fonts? ###

This is necessary to get a TrueType font that can be used with most
current graphics systems.  See file `hershey-fonts.md` for different
approaches to converting stroke fonts to outline fonts.

### Graph rewriting with a tileset? ###

As I said above, [McGraw’s 01992 paper][10] includes a “pipeface”
version of Benzene Right generated by mapping each vertex to a “pipe
segment” tile from a tileset based on the local pattern at that
vertex: which directions have a quantum drawn in them.  There are 8
possible directions, so there are 2⁸ = 256 possible tiles, but Benzene
Right in particular uses only six of them, reducing the possible count
to 2⁶ = 64.  The table shown in the paper only has 21, and a 22nd is
the empty vertex, with no segments.  However, in four cases, two
different tiles are given, with serifs pointing in different
directions; I suspect that the choice is based on what part of the
grid you’re in.

The gridfont case is slightly different from standard Wang tiles (see
below).  Each “tile” corresponds to a vertex of the gridfont grid, and
its edge colors do match the tiles it shares an edge with, but where
four tiles meet at a vertex of the *tile* grid, each tile need only
match the tile on the opposite side of the vertex.  Moreover, the
shapes on the tiles may overlap; they aren’t necessarily simple
rectangles of pixels like in 2D video games.

The items on the tiles might not even be raster data; they might be
path segments to form a stroke font or outline segments to form an
outline font.

The minimal amount of work to define such a tileset would presumably
be to generate reflections and rotations automatically, so the 256
possibilities are reduced to, if I calculated right, 43; in
hexadecimal, the distinct cases are 00 01 03 05 06 07 09 0b 0f 11 12
13 15 16 17 19 1b 1d 1e 1f 25 27 2b 2d 2f 33 37 3f 55 56 57 5a 5b 5e
5f 66 67 6f 77 7b 7e 7f ff.

McGraw mentions previous work along these lines:

> The DAFFODIL program mentioned in [Nanard et al., 1986] uses very
> simple substitution methods to “create” letterforms.  By replacing
> sections of a “letter skeleton” with human-designed complicated
> curvy lines, DAFFODIL generates some eye-catching, fancy
> letterforms.  The problem [from the point of viw of AI research] is
> that the program itself is not doing anything interesting.  *People*
> create both the letter skeletons and the fancy fillers — the
> computer simply substitutes a fancy filler for a section of skeleton
> according to its built-in rules.
>
> (...)
>
> Letters created on the grid can be seen either as self-sufficient
> letterforms in their own right or as “skeletal” geometric
> representations of letter-concepts that can be “fleshed out” into
> true letterforms.  Figure 5, for instance, shows the relationship
> between a gridfont called *Benzene Right* and a derivative “dressed
> up” alphabet.  Using straightforward and totally mechanical
> shape-for-shape substitution, any gridfont can be converted into a
> more conventional typeface.  (...) We have chosen to leave out the
> step of adding “flesh” to the “bones” of a gridfont, and to work at
> the more basic level of the “skeletons”.

Nanard’s paper is “Declarative approach for font design by incremental
learning”, by Marc Nanard, Jocelyne Nanard, Marc Gandara, and Nathalie
Porte, a tech report from CRI Montpellier, an institution which
apparently doesn’t exist.  [The tech report does][49], and says it was
presented at the *Workshop on Font Design Systems* in May 01987 and
published in “Raster Imaging and Digital Typography: Proceedings of
the International Workshop, Lausanne, 1989, Volume 1”, edited by André
and Hersch.  Unfortunately I do not have a copy to read.

[49]: https://books.google.com.ar/books?id=mj09AAAAIAAJ&pg=PA71

#### Hexgrids ####

43 is a lot of tiles.  If you wanted to make this more tractable, you
could switch to a hexagonal grid of triangles, so that each vertex
only has 6 quanta connected to it rather than 8.  In binary, the
possible quantum states of such a degree-six hexfont vertex, up to
reflections and rotations, would be only 13 in number: 000000, 000001,
000011, 000101, 000111, 001001, 001011, 001111, 010101, 010111,
011011, 011111, and of course 111111.  But if you want tiles you can
rotate into six positions like that, you can’t have both horizontal
and vertical lines.  I think vertical lines are more important, but
then all your “skeletons” will come out looking kind of runic.

In a triangle grid, each new triangle you add adds 1½ edges, so 56
bits is probably a little less than 30 triangles.  A vertical strip of
8 triangles (4 pointing left, 4 pointing right) has 4 right-border
triangles and 12 others, 16 in all; three more such strips,
alternating in baseline, work out to 52 quanta.

### Maybe we can use geometric distortion? ###

Of course, you can adjust things like the x-height, the ascender
height, or the obliqueness of a gridfont simply by systematically
moving the points of the grid, giving rise to local coordinate
transformations.  To some extent you can do this with any stroke font
or outline font, but with gridfonts you usually know where the
ascender line, mean line, and descender line are, without having to
guess, though some gridfonts in the database are exceptions to the
rules.  And of course you don’t have to worry about distorting serifs
or stress the way you do with an outline font.

For an XKCD sort of look, you can instead locally distort the geometry
with simplex noise or something similar

### Highly-data-compressed scalable fonts? ###

In the past, starting with Janne Kujala’s design, I’ve designed a
[1-bit-deep 4×6 ASCII font which is a 356-byte PNG][52] and a
[1-bit-deep n×6 font which is a 406-byte PNG][53], using a 7th scan
line to indicate character widths, which [averages 3.6 pixels in
width][54].  Dividing by 96 printable glyph positions, we get
respectively 3.7 and 4.2 bytes per glyph.  I tried scaling up each
image with `ffmpeg -i 6-pixel-font.png -filter_complex hqx=4
6-pixel-font-big.png` or equivalent and got a sort of vaguely
acceptable result, but I wouldn’t really call them scalable fonts.  I
haven’t tried other [pixel-art scaling algorithms][55] like the
venerable EPX, [Scale2x][56], or 2xSaI.

If we compute the *uncompressed* sizes of these fonts, they’re 64×36
bits = 288 bytes and 374×7 bits = 327.25 bytes respectively.  So PNG
overhead is actually inflating the files significantly.

Uncompressed gridfonts are larger than that; if we go with the same
size, 56 bits is 7 bytes (672 bytes per 96-glyph font, so all 287 from
the data file would total 193K), and 7 bytes per glyph is more than
3.7 or 4.2.  But I suspect they’re probably pretty compressible; if we
interleave each of the 7 bytes with the corresponding bytes from other
glyphs from the same font, they’d be more so.  Then maybe with gzip or
ncompress they’d be *smaller* than the pixel fonts.

[52]: http://canonical.org/~kragen/sw/netbook-misc-devel/6-pixel-font.png
[53]: http://canonical.org/~kragen/sw/netbook-misc-devel/6-pixel-1-bit-proportional-font.png
[54]: http://canonical.org/~kragen/sw/netbook-misc-devel/propfontrender.png
[55]: https://en.wikipedia.org/wiki/Pixel-art_scaling_algorithms
[56]: https://www.scale2x.it/scale2xandepx

Some of the gridfonts are about as close as you can get to a minimal
readable font.  The “brown19” font reproduced below, for example:

    font : brown19
    creator : doug
    create date : Fri Jan 11 01:21:45 EST 1991
    last edit : Fri Jan 11 01:21:45 EST 1991
    a 01400040010000
    b 00800900000020
    c 00800000020000
    d 00800480020000
    e 02800100020000
    f 02000100080000
    g 00800080028000
    h 00000900000020
    i 00000080000000
    j 00000090000000
    k 02000900000020
    l 00000900000000
    m 02000180000020
    n 00000080020000
    o 00800080020000
    p 00800120000020
    q 00800090020000
    r 01000080000000
    s 01400000000010
    t 01000400000010
    u 00800180000000
    v 00000080000020
    w 00800180020000
    x 00000000020020
    y 00000090000020
    z 02800000020000
    comment : 

As can be seen even in the bit data, it contains very few strokes, 70
by my visual count, an average of 2.7 per glyph, close to the minimum
possible for a font that is at all usable.  5 glyphs cannot be traced
in a single continuous path, so you need a total of 31 paths; as
Hershey fonts these would thus require 101 coordinate pairs in total,
3.9 per glyph.  Even without the JHF format’s per-glyph overhead,
though, that would be 7.8 bytes per glyph represented with two bytes
per vector.

Basically you aren’t going to beat the bitmap approach by much.  All
278 fonts of 26 glyphs in the data file should work out to 50.6
kilobytes as raw binary without any compression.

Other related work
------------------

The gridfont framework is similar to the conventional 7-segment
display, the [fourteen-segment starburst display][57], [Michiel “Posy”
de Boer’s 6-segment display design sketch “X”, earlier independently
invented by Adam L. Humphreys][58], Carl Kinsley’s segmented display
from 01903 (US patent 1,126,641), the old Sharp cursive 8-segment or
9-segment numeric display, and the 8-segment Russian
electroluminescent IEL-0-IV/IEL-0-VI design.  These do of course
provide even more densely encoded fonts than gridfont bitmaps, though
at the cost of readability.

[57]: https://en.wikipedia.org/wiki/Fourteen-segment_display
[58]: http://www.michieldb.nl/other/segments/

Jason C. Reed has [his own gridfonts page][35], including images of
many gridfonts he designed himself or possibly transcribed from
Hofstadter’s book.  He hasn’t published any data that I can see.

[35]: http://jcreed.org/js/gridfont/

The site [FontStruct][37] is a tile-based font design program with
tens of thousands of Creative-Commons-licensed fonts available.  Its
tile choice is entirely independent of neighboring tiles and under
user control, unlike Wang-tile approaches.

[37]: https://fontstruct.com/gallery?filters=all&license=commercial-use&order=by-glyph-count

The DAFFODIL rewriting approach sounds like it’s similar to Wang
tiles, which video game developers commonly use for procedural
generation ([online demo in the Internet Archive][48], [book from
02009][50], [doctoral dissertation by the same author][51]); Wang
tiles are non-rotatable square tiles where each edge is labeled with a
“color”, which is required to be adjacent to another edge of the same
color.  In a game map, for example, the set of “colors” might be
{land, land-with-road, land-with-river, city}, as in the board game
[Carcassonne][41] ([tileset][42]).  Variations include using shapes
other than squares, coloring the corners instead of the edges (as in
[triominoes][43], sometimes called “corner tiles” rather than Wang
tiles), [using more than one shape of tile][45], and permitting tile
rotation and/or reflection.

Wang tiles are often painted so as to reduce the visibility of the
tile edges, similar to [Cyril Stanley Smith’s variant of Truchet
tiles][44].

There are at least two common video game approaches to procedural
generation with Wang tiles:

1. Using a “complete” Wang tileset which includes all possible
   combinations of tiles; this permits the colors of edges (or
   corners) to be generated without worrying about the tileset, then
   drawn with any complete tileset with the right number of colors.
2. [Using a Wang tileset to define procedural generation
   constraints][38], a subset of a larger class of [constraint-based
   tile generators][40].

Unfortunately [the extremely cool cr31 web page I learned about this
from is gone][47], though [it survives in the Archive for now][48],
but there’s a collection of [Wang tilesets on OpenGameArt][39] and
several [Wang tileset generators][46].

[38]: https://ijdykeman.github.io/ml/2017/10/12/wang-tile-procedural-generation.html
[46]: https://www.boristhebrave.com/2023/06/04/wang-tileset-creator/
[39]: https://opengameart.org/content/wang-blob-tilesets
[40]: https://www.boristhebrave.com/2021/10/31/constraint-based-tile-generators/
[41]: https://en.wikipedia.org/wiki/Carcassonne_(board_game)
[42]: https://en.wikipedia.org/wiki/File:Carcassonne_tiles.svg
[43]: https://en.wikipedia.org/wiki/Triominoes
[44]: https://en.wikipedia.org/wiki/Truchet_tiles
[45]: https://en.wikipedia.org/wiki/Girih_tiles
[47]: https://web.archive.org/web/20231026221613/http://www.cr31.co.uk/stagecast/wang/blob.html
[48]: https://web.archive.org/web/20230603150904/http://www.cr31.co.uk/stagecast/wang/stage.html
[50]: https://www.amazon.com/-/es/Ares-Lagae/dp/1598299654
[51]: https://lirias.kuleuven.be/retrieve/67304
