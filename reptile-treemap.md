I was thinking about breaking down the day in powers of 3: three
shifts of 8 hours, 9 slots of 2h40m, 27 cubies of 53'20", 81 pomodoros
of 17'46⅔", and 243 moments of 5'55.5̅" (see, among other things, file
`plans-sunday.md`).  It occurred to me that, in addition to the
current Sierpiński layout, you could use a treemap layout, which is
infinitely more space-efficient: divide a space into three horizontal
slices stacked on top of each other, each the shape of the original
space rotated 90°; then subdivide each of them into three vertical
slices one next to the other; then repeat.

For this to work, you want the sides to be in the relationship √3,
about 1.73:1, about 7:4 or 19:11.  A4 paper is 210 mm × 297 mm.
Assuming portrait mode, you have this ideal ratio if you use a
171-mm-wide column in the middle of the A4 paper, leaving 19-mm
margins on the left and right.  On a laser or non-fanfold inkjet
printer, you have to leave some margins at the top and bottom too; if
it's 10 mm then you have 277 mm left, and your column width is 160 mm,
leaving 25 mm on the left and right.

Then you can divide the day into three horizontal shifts each 92 mm ×
160 mm; you can divide each shift into three vertical 2h40m slots each
92 mm × 53 mm; you can divide each slot into three horizontal 53'20"
cubies each 31 mm × 53 mm.  These are already getting a bit cramped to
write anything in; 53 mm is about three or four words wide, and 31 mm
is about 7 lines.  So, at least if you write horizontally, it might be
most practical to do the next level of division again into three
horizontal 17'46⅔" pomodoros each 10.3 mm × 53 mm, about two lines of
text of three or four words.  You can reserve a little bit of space at
the left or right of the cubies for stuff that spans all three
pomodoros.

If you preprint this, you can preprint it with the conventional clock
times at each start time in a small font.  You can use color gradients
to provide contrast between adjoining table cells without wasting any
real estate on the table cell borders.  But keep in mind that, if
you're printing this on conventional paper, you're spending 29% of the
paper on margins!  It would make more sense to sacrifice the rep-tile
1.73:1 ratio so that the individual cubies and pomodoros are, say, 70
mm wide instead of 53 mm wide.

A different approach is to subdivide a larger sheet of paper, like A3,
into a number of sheets that isn't a power of 2.  A3 paper is 297 mm ×
420 mm (far out, dude).  Suppose you lose 5 mm around each edge;
you're left with 287 mm × 410 mm (less far out).  If you were to cut
it (landscape mode) into 30 sheets, cutting into 6 equal parts
vertically and 5 equal parts horizontally, these individual pages
would be 82 mm × 47.83 mm.  These differ from the desired √3 ratio by
only about 1%, but they're also about the size of one of those pocket
abridged Bibles.

If you instead cut it into 12 pieces, they can be 137 mm × 72 mm,
which is about 10% off from the desired ratio, but that's still a lot
better than the 22% off you get with the standard paper sizes.  It's
about the size of a pack of playing cards.

