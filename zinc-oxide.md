[Zinc oxide][0] (ZnO) has a modest standard enthalpy of formation of
-350.46 kJ/mol, but that’s still more than [that of water][1] is only
-285.83 kJ/mol.  This indicates that oxidation of metallic zinc with
oxygen from water is exothermic, yielding 65 kJ/mol.  The Gibbs free
energy is also favorable: -320.5 kJ/mol for ZnO vs. -237.24 kJ/mol for
water; but at room temperature this reaction is rather slow.
[DerSilberspiegel’s Ellingham diagram for various metals][2] shows
ZnO’s free energy surpassing that of water at 1200°.

[0]: https://en.wikipedia.org/wiki/Zinc_oxide
[1]: https://en.wikipedia.org/wiki/Properties_of_water
[2]: https://en.wikipedia.org/wiki/Ellingham_diagram#/media/File:Ellingham_Richardson-diagram_english.svg

As I understand it, this means that you can reduce zinc from its oxide
with hydrogen above 1200°, but below that temperature the reaction
will be opposite, and over most of that range it yields a fairly
consistent amount of free energy.

Zinc melts at 692.68°, boils at 907°, weighs 7.14g/cc at room
temperature, yields 7.32kJ/mol of heat of solidification and 115kJ/mol
of heat of condensation, and holds 25.470J/mol/K of heat at room
temperature, which naïvely extrapolates to 23kJ to heat it from room
temperature to boiling, one fifth the heat of condensation itself.
Its low boiling point makes it easy to separate from other materials
for mining purposes.

Does this create a risk that boiling zinc could combust rapidly if its
vapor were to come in contact with dense steam, releasing hydrogen?
Zinc’s 115-kJ/mol heat of condensation is larger than the 65-kJ/mol
heat of formation thus yielded, so probably not; the heat input
required to boil the zinc ought to prevent thermal runaway in this
reaction, leaving it confined to the surface of the liquid zinc.  That
is, unless the Coulomb-explosion dynamic that seems to accelerate the
water-driven combustion of most alkali metals could come into play.

But you can [pour molten zinc into water with a reasonable degree of
safety][5].

What about [zinc hydroxide][3]?  Normally metals being oxidized by
water get oxidized to the hydroxide.  It decomposes at 125°, though,
so despite its much more negative heat of formation (-642kJ/mol!) it
seems unlikely to be stable at the relevant temperatures.  However,
this reaction could be a major hazard with wet, [finely-divided
zinc][4] at room temperature: “Reacts with water and reacts violently
with acids and bases giving off highly flammable hydrogen gas. (...)
Reacts violently with fire extinguishing agents such as water, halons,
foam and carbon dioxide. The symptoms of metal fume fever do not
become manifest until several hours later.”

[3]: https://en.wikipedia.org/wiki/Zinc_hydroxide
[4]: https://training.itcilo.org/actrav_cdrom2/en/osh/ic/7440666.htm "ICSC 1205"
[5]: https://youtu.be/tj7S_DNFgEU "Pouring Molten Metals into Water. COOL!  (Aluminum, Thermite, Lead + More!) (5m42s, TheBackyardScientist, 02015-06-15)"
