PHP is, by all ordinary standards, [a terrible programming language][0].  Its author [knew so little about programming that he used `strlen` as a hash function][1].  It [didn't even have a reasonable IDE until PhpStorm was released in 02010][4].  Yet it's [#8 on TIOBE's list of the most used programming languages][2] and the programming language in which people have written the greatest encyclopedia in human history, the world's most popular social network, and Wordpress, the world's most popular CMS; all of which came out before PhpStorm.  Why would programmers competent enough to pull off such stunning achievements saddle themselves with a substandard programming language with no tooling support?  Moreover, why didn't PHP's flaws doom their efforts, leading to them being eclipsed by lesser programmers using better tools?
 
[0]: https://eev.ee/blog/2012/04/09/php-a-fractal-of-bad-design/
[1]: https://news-web.php.net/php.internals/70691
[2]: https://www.tiobe.com/tiobe-index/
[4]: https://www.jetbrains.com/lp/php-25/

Why I think PHP was so successful
---------------------------------------------

PHP has and had many virtues, none of which belong to the programming language proper.  It started out as a DSL for web page templates and later added built-in support for processing HTML forms (the "FI" in "PHP/FI") and MySQL databases, so it's easy to use that functionality, even for people who don't know how to program; they don't have to contend with writing subroutines or module systems and package systems.  So PHP became the Visual Basic of the Web.  Its interpreter isn't especially efficient, but at the relevant time (about 01999) alternative interpreters like Perl and Python were generally invoked by the web server through CGI, which added on the order of 100ms of work to each web page view, while the `mod_php` implementation that embedded PHP in Apache eliminated this process startup overhead. But unlike, for example, `mod_perl`, which embedded Perl in Apache similarly, PHP didn't abandon the stateless CGI model. In CGI, and in `mod_php`, each HTTP request handler runs independently, without sharing any state with the previous or next request, except by way of the database or filesystem.

This statelessness has several important effects, two of which enabled shared hosting and a third of which enabled hyperscalers like Fecebutt. Because there isn't any PHP-accessible state in the server process, that state can't be corrupted or otherwise modified by a request handler.  This means that you can safely run request handlers belonging to different users in the same process, one after another, without the users relying on one another not to be malicious. And it means that, generally, you can safely abort a request handler partway through execution; it might be partway through modifying the application state in the server-side database, but the usual database transaction solutions suffice to prevent state corruption in this case, and are needed to support multiple concurrent handlers at any rate. (Even MySQL eventually added support for fully ACID transactions.)  So you can safely kill PHP request handlers that run for more than, say, 30 seconds, without killing the whole Apache server process.  This is really important, because everybody writes infinite loops sometimes, but novice programmers write them a lot.

These two properties meant that PHP hosting was far cheaper, easier to set up, and more widely available, than hosting that could handle `mod_perl` or FastCGI or Ruby on Rails, and despite AWS EC2, that remained true until past 02009, if memory serves.  Consequently most new programmers from about 01997 to about 02009 learned to program on PHP, and lots of veterans used it too.

The reason this statelessness mattered for hyperscalers is that, because the only communication between handlers was through the MySQL database, which was accessed over a socket, you could scale up a PHP web service quite a bit just by adding more web servers all hammering the same MySQL server. So the same basic PHP approach would economically scale your system up from using 1% of a web server to using, say, 30 servers, though somewhere around 5 servers you probably wanted to stick a load balancer in front of them to hide server failures from users. Another factor of 30 was typically available with a bit more work by adding MySQL "readslaves"; typically about 99% of the MySQL server's work was read-only queries, and only 1% was updates, so by running a bunch of read-only replicas off the master database server's update log, the master only had to handle that 1% of the queries.

So with this "three-tier" architecture, basically the same one used by CICS in the 01970s, you got linear scaling from US$5 a month for 20'000 hits a day, up to half a million dollars a month for a billion hits a day, even using spinning rust and sub-gigahertz servers from the previous millennium.  Using other architectures with better micro-efficiencies, but inferior scaling properties, sank a number of web startups in the early 02000s, most famously Friendster.

(Not all PHP sites scaled so nicely; those that were write-heavy, that had queries MySQL sucked at, or that had strict consistency requirements, ran into PHP's scaling limits earlier.  Sharding and additional storage systems like memcached and Redis sometimes came into play.)

These scaling properties are less important nowadays because a single off-the-shelf server running efficient software (node.js, Golang, or something on the JVM) can now serve five billion hits a day, and you can buy CDN services from Fastly with instant cache invalidation; so an ever smaller set of sites need to scale up horizontally in the way that PHP enables, at least if they're not running on an inefficient interpreter like Python or PHP.  Instead, most projects focus on scaling *down* to smaller virtual machines.

PHP is a live environment, and what I mean by that
-----------------------------------------------------------------

But there's another result of this stateless-middle-tier architecture that I think is more conceptually interesting than most of the above, and that is PHP's liveness. By "liveness" I mean the property shared by environments like ITS DDT, Emacs Lisp, the MACLISP REPL, GW-BASIC, Erlang, FORTH, Jupyter, and Smalltalk: you can modify your program without losing any application state.  And I think it is this property, above all, which accounts for PHP's outstanding usefulness to novice programmers.

[Steve Yegge wrote a piece mostly about this in 02007 called "The Pinocchio Problem"][8]:

[8]: http://steve-yegge.blogspot.com/2007/01/pinocchio-problem.html

> I think the second most important design
> principle, really a corollary to the first,
> is that **systems must be able to grow
> without rebooting**. A system that can't
> grow over time is static, so it really isn't
> a system at all; it's a function. It might
> be a very complex function with lots of
> possible inputs and outputs. It might be a
> very useful function, and it might live for
> a long time. But functions are always either
> replaced or subsumed by systems that can
> grow. And I've come to believe, over nearly
> two decades of thinking about this, that
> systems that can grow and change without
> rebooting can live forever. ...  Living
> software is a little scary. It's no surprise
> that most programmers prefer writing
> marionettes instead of real people;
> marionettes are a lot easier to manage. But
> I think living software is more interesting,
> and more useful, and quite frankly it's an
> inevitability in any event, so we might as
> well strive to understand it better. To me,
> that means building it.

Yegge's desiderata go beyond just being able to modify a system without "rebooting" it; he lists a command shell (with "a command language, an interactive help facility, a scripting language, an extension system, a command-history facility, and a rich command-line editor"), advice, an extension language, a plug-in system, a killer app, and introspection.  But I think these are mosty a distraction from the concept of liveness; I'm just going to focus on changing a program without restarting it, which I think is inseparable from Yegge's definition of introspection:

>  You can poke around and examine them at
>  runtime, and ideally they poke around and
>  examine themselves as well. At the very
>  least they should have some basic health
>  monitoring in place. ...  System
>  administration tools and diagnostics are
>  one kind of introspection; single-step
>  debugging is another; profiling is yet
>  another. Dynamic linking is still another:
>  the system needs to be able to (for
>  instance) run bytecode verifiers to make
>  sure the code being loaded passes basic
>  security checks. Introspection usually
>  comes with a performance penalty, so many
>  programmers avoid it at runtime, or they
>  make do with whatever minimal introspection
>  facilities the underlying system provides
>  (e.g. RTTI or Java's reflection
>  facility). If you trade off introspection
>  for speed, you're carving years or even
>  decades off the lifespan of your software
>  system.

Such introspection is necessary for growing a system without rebooting it, because sometimes when you are doing that, you will get the system into a broken state.  If you can't introspect on the state to see how it's broken, you can't fix it, and your only option then is to restart it.

### So how does PHP fulfill these criteria? ###

It might seem to be at the opposite extreme: instead of never rebooting, it "reboots" for every single HTTP request!  But that's the only kind of rebooting it needs.  The MySQL server, the Linux kernel, and the Apache server never need to be restarted unless something goes quite wrong, like a disk crash or a security hole that needs patching.  Even then, if you have multiple servers, you can often do this one server at a time so it's transparent.

(And of course this has almost nothing to do with the PHP language; stateless-middle-tier is a pattern you can apply in any programming language, and so is verifying that the source code in the filesystem hasn't changed before you serve an HTTP request.  Django does a pretty good job of the first one, and its development server does a mostly passable imitation of the second one.)

Incremental programming
---------------------------------

Software development is often best done incrementally, because an incremental change gives you a lot more feedback while you're making it, so you notice at which point something bad started to happen, and which good things you're liking about it.  Novice programmers rely especially heavily on incrementality and continuous feedback because they don't yet have a good enough model of what the system will do internally to predict the effects of a big change.  Really, though, everyone programs incrementally, even if their feedback on their incremental changes comes from an internal mental model of the software.

We can argue the theoretical importance of this kind of incremental feedback on several grounds.  Herrnstein's matching law says that, above some small threshold, the intensity of reinforcement is inversely proportional to the reinforcement delay, so feedback that's twice as rapid will require half as many experiments to train us to do something that has good results, or not to do something that has bad results.  If we're doing the experiments one at a time, we also get twice as many experiments per unit time if the feedback is twice as rapid, so our learning (in the behaviorist sense) is four times as fast.  Incremental feedback is more specific: if you make eight changes before you see their effects, any one of those eight changes (or any of their other 247 subsets!) could be the cause of whatever effect you see.  And from cybernetic theory we know that homeostatic (negative) feedback can approach its set point very much more rapidly when its delay is short.

### Liveness strongly supports incremental programming ###

And incremental feedback is precisely what "liveness", using this definition of "being able to modify a program without losing application state", permits.  If you have to restart your program and lose all its state to see the effect of an incremental change, on the computer instead of in your head, you will necessarily have a much longer feedback cycle, and you will make larger changes before getting feedback than if you could incrementally make those changes to a live system and get feedback immediately.

Introspection and editing of system state
--------------------------------------------------

[Stallman wrote about what he likes about Lisp][16].  Somewhat to my surprise, he doesn't talk about commonly touted Lisp features like macros, efficiency, interactive debugging, or higher-order programming; he talks about read, eval, and print.

>  When you start a Lisp system, it enters a
>  read-eval-print loop. Most other languages
>  have nothing comparable to 'read', nothing
>  comparable to 'eval', and nothing
>  comparable to 'print'. What gaping
>  deficiencies!
>
> I skimmed documentation of Python after
> people told me it was fundamentally similar
> to Lisp. My conclusion is that that is not
> so. 'read', 'eval', and 'print' are all
> missing in Python. ...
> 
> While I love the power of Lisp, I am not a
> devotee of functional programming.

[16]: https://stallman.org/stallman-computing.html#lisp

Similarly, [in an interview with LinuxCare in
01999][256] ([better-formatted mirror][512]):

> LISP is the most powerful programming
> language, and if you want an interpreter,
> LISP is the best. None of the other
> languages come anywhere near LISP in their
> power. The most exciting things about LISP
> are read, eval, and print. If you look at
> other languages, they have no equivalent for
> any of those

[256]: https://lists.gnu.org/archive/html/help-gnu-emacs/2010-12/msg00744.html
[512]: https://www.karmak.org/archive/2003/01/12-14-99.epl.html

[A StackOverflow user adds a little more detail][17]:

> In addition to the obvious uses at the REPL,
> experienced Lispers use print and read in
> the code as a simple and readily available
> serialization tool, comparable to XML or
> json. While Python has the str function,
> equivalent to Lisp's print, it lacks the
> equivalent of read, the closest equivalent
> being eval. eval of course conflates two
> different concepts, parsing and evaluation,
> which leads to [problems like this][32] and
> [solutions like this][64] and is a recurring topic
> on Python forums. This would not be an issue
> in Lisp precisely because the reader and the
> evaluator are cleanly separated.

[16]: https://stackoverflow.com/a/12253402
[32]: https://stackoverflow.com/questions/988228/converting-a-string-to-dictionary
[64]: http://code.activestate.com/recipes/364469-safe-eval/

In theory, all you need to be able to grow a system without restarting it is to be able to feed it new code to run, like Python's `eval` or `exec`.  In practice, though, you want to be able to test your new code independent of the rest of the system, and, as explained above, to be able to introspect on, and edit, the state of the system.  These facilities are what Lisp's READ and PRINT provide: READ allows you to supply test data to code being tested or new system state, PRINT allows you to understand the results or inspect the system state, and the combination of PRINT, READ, and a text editor allows you to take a piece of the system state, edit it, and change the system state to the edited version.

### In PHP ###

In PHP (as conventionally used) the system state is in MySQL, so you can use SQL commands to inspect or edit it.  Moreover, because you can run other HTTP request handlers in PHP concurrently without fear, you can also use something like PhpMyAdmin (a set of PHP pages) to inspect or edit that state.

A set of tables is often a more human-readable and -editable representation of system state than a Lisp-style object graph, though for some kinds of data, such as math formulas or program code, it would be worse.

Desynchronization: the fatal flaw of many live systems
--------------------------------------------------------------------

However, there's a single drawback shared by many of these live systems which sometimes makes them actually worse for development than dead systems.  It's the desynchronization problem.

In [s-ol bekic's apology for his music livecoding language `alv`, he explains the desynchronization problem eloquently][128]:

> In repl-based environments, a scratch file
> is opened in a text editor. In it, commands
> are staged and can be added, removed and
> edited without consequence. The livecoding
> system generally has no knowledge about this
> scratch buffer at all. The user is free to
> select and send individual commands (or
> groups of commands) at any time and execute
> them by transmitting them to the server via
> an editor plugin.
> 
> Commands are incremental changes (deltas)
> that get sent to the server, which keeps an
> entirely separate and invisible model of the
> project. Generally no feedback about the
> state of this model is made available to the
> user.
> 
> Code is only executed when the user
> evaluates a block, although code run in this
> fashion may cause other code to execute
> outside of the user-evaluated execution flow
> via side effects, for example by registering
> a handler for events such as incoming
> messages or scheduling execution based on
> system time. These mechanisms however are
> implementation details within the code the
> user executed originally, and no uniform
> mechanism for noticing, visualizing or
> undoing these side-effects exists.
> 
> This design has the following consequences:
> 
> * The view of the scratch buffer is not
>   correlated with the code and state the
>   server is currently executing. This
>   results in overhead for keeping the mental
>   synchronized with what the system is
>   actually performing for the user, but also
>   makes it much harder for the audience to
>   follow along.
> 
> * Sessions cannot be reopened reliably,
>    because the state of the server depends
>    on the full sequence of commands that
>    were sent to the server in order, which
>    is not represented in the scratch buffer.
> 
> * If parts of the execution model on the
>   server have not been explicitly labelled
>   (i.e. assigned to a variable) in the
>   textual representation, often many
>   potentially important actions for
>   modifying the current behaviour are
>   unavailable: for example long-running
>   sounds may not be cancellable, effects'
>   parameters may not be adjustable without
>   recreating the signal chain, etc.

[128]: https://mmm.s-ol.nu/research/alivecoding/

All of these shortcomings are present (with minimal differences) to most of the live systems I listed previously: Emacs Lisp, Lisp REPLs, FORTH, and Jupyter.  So, if you observe some behavior in your live system, expected or not, you can never be quite sure that it's due to the current version of your program; it may be due to a previous version of the code that some object still contains a reference to, or to some invisible data.  Possibly the system is only broken, or for that matter working at all, because you haven't restarted it.

The upshot of this is that, when you learn to use such a desynchronization-prone live system, you learn that sometimes desynchronization manifests as bugs that are a waste of time to track down.  So when you hit an unexpected bug, before you try to debug it, your best option is to restart the system from scratch to see if the problem goes away, like a computer running Microsoft Windows.

PHP, among other things, doesn't suffer from desynchronization
---------------------------------------------------------

But they almost don't apply at all to ITS DDT, GW-BASIC, Smalltalk, or (as conventionally used) PHP.  In those systems, whenever you change the code, any future execution uses the new code, not the old code.  (The only exception is that, in Smalltalk, a block object can continue to refer to a block that no longer exists, or, I think, that has been replaced with a different version of the code.) The running system might have state that results from a previous version of the code, and that state might be bad or inconsistent or corrupt in some way as a result, but you can inspect that state and change it.

(I don't have enough experience with Erlang to know which group it falls into.)

Python sucks at this, because the semantics of executing a Python class or function definition is to create a new class or function object, and then assign it to a name, but objects instantiated from the old class object will contain references to that old class object and its old methods.

In ITS DDT, GW-BASIC , or PHP you have a pretty strong guarantee that code you've deleted or replaced with a new version won't ever run; that code just isn't anywhere anymore, and a stray reference to it can't cause it to be retained to be run later.  But they have it easy: their data models don't have function pointers at all.

In theory, in Python, you could mutate all the outdated function objects in the system by pointing their `__code__` attributes (`func_code` in Python 2) at something new, ideally the new version of the function or something that throws an error if the function has been deleted; but that requires you to be able to find them, and in Python they're all over the place.

Smalltalk is mostly immune to this problem even though it has pretty much the same memory model as Python, because in Smalltalk all the code is in method objects, all the method objects that can be invoked are in class objects, and all the class objects are in a global dictionary.  And in Smalltalk the usual way to do a call to code chosen at runtime is to send a message to a class instance.

The problem is not very severe in Emacs Lisp, which also has the same memory model as Python.  Emacs Lisp does support lambda-expressions, but it's much more common to invoke functions through a symbol, and the function binding of such a symbol is changed when a new `defun` form is evaluated for the symbol.  But it can't offer the same kind of strong guarantee of not running stale code that GW-BASIC or PHP do.

How to make a better live environment
-----------------------------------------------

See file `better-live-environment.md`.
