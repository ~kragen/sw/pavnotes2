For 14 years (since file `mechanical-computers`, posted to kragen-tol
on 02010-06-28, title “mechanical computation: with Merkle gates,
height fields, and thread”) I’ve been trying to figure out how to
construct mechanical digital computers out of Paleolithic materials,
and, in particular, thread.  This may have been inspired by a remark
Dave Long made when I made a post about the central importance of
hardening processes, such as firing clay, casting iron, or
heat-treating steel.  As I remember it, he said that perhaps the
development of soft materials such as the leather used in his horse’s
bridle was at least as important.  And surely it was sewing that
allowed our hairless ancestors to venture out of the tropics and into
the chilly Levant and the frozen steppes; ceramics and metallurgy came
much later.

One of the problems I’ve been facing with thread computation is
amplification.  How can a small amount of mechanical energy on one
thread control a large amount of energy on another?  Even solving that
problem isn’t enough if there’s some other restriction that prevents
complete chaos, for example if large displacements can only control
smaller ones, or large forces can only control smaller ones, or a
voltage going more positive can only ever make other voltages go more
positive.

This morning watching the sunrise and pondering the intricacy of my
woven shirt, the answer finally occurred to me.  It is an adiabatic
clocked-logic mechanism consisting of five threads, and it provides
memory, amplification of displacement, and amplification of force,
through static friction.  It’s similar to how the warp threads in a
loom, actuated by the heddles, trap the weft threads between them.

The threads are called X, X', X'', Y, and Z.  X is a thick cord
parallel to the X-axis; ideally it does not move throughout the
actuation of the mechanism.  X' is a thin thread stretched parallel to
X, but slightly displaced from X along the Y-axis and initially
slightly higher than X along the Z-axis.  It moves in the Z direction
(that is, parallel to the Z-axis)
during the actuation of the mechanism.  Y is a thin thread stretched
parallel to the Y-axis; its position along the Z-axis is in between
the Z-position of X and the Z-position of X'.  It moves in the X
direction during the actuation of the mechanism.  X'' is another
thread parallel to the X-axis; it is tied to Y with a knot near the
Y-positions of X and X'.  It moves in the X direction during the
actuation of the mechanism.  Finally, Z is a thread parallel to the
Z-axis tied to X' with a knot close to the initial X-coordinate of Y;
Z is analogous to the heddle of a loom, and the knot is analogous to
the heddle’s eye, but does not need to slide.  Z moves in the Z
direction during the actuation of the mechanism.

To use the mechanism to amplify displacement, the procedure is as
follows.

First, a small displacement is applied to Z to move X' from higher
than X on the Z-axis to lower than X on the Z-axis, using a
medium-sized force.  This pinches Y in between X and X'.  Second, a
small force is applied to X'', significantly smaller than the force
being applied to Z.  Because Y is pinched between X and X'', it cannot
move in the X-direction, and so X'' also does not move in the
X-direction despite this force.

If, instead, in the first step, the displacement applied to Z is
smaller or in the opposite direction, Y does not become pinched in
between X and X'.  So, in the second step, only Y’s elasticity (and
that of the frame it’s stretched over) are resisting the force on X'',
and so a small force on X'' can produce a large displacement.

The second step can be performed and reversed many times before
undoing the first step, thus providing not only amplification of
displacement (by perhaps an order of magnitude) but also an
arbitrarily large amplification of energy.  This is adiabatic unless
the small force is still being applied to X'' when the medium-sized
force on Z is released.

To use the mechanism for memory, the procedure is as follows.

First, a large displacement is applied to X'', which as before can be
produced by a small force, because X and X' are not pinching Y.
Second, a medium-sized displacement is applied to Z with a
medium-sized force, pinching Y between X and X' even though it is
farther than usual from the X'/Z knot.  Third, the small force on X''
is released.  Because Y is pinched between X and X', it does not
return to its neutral position; it remains displaced along the X-axis.
One detectable effect of this is that even a tiny force on X'' is
sufficient to remove the slack on it and return it to the stored
position; it does not even have to combat the spring force of Y, just
the slack weight of X''.

If, instead, the displacement applied to X'' in the first step is
zero, then Y will instead be pinched in its neutral position, and a
tiny or even small force on X'' will not produce any displacement.

As before, the memory can be read many times as long as the
medium-sized pinching force on Z is not released.  To erase the memory
adiabatically, the same force must be applied to X'' again, so that Y
will remain in its position when Z is released, rather than slipping.

A second detectable effect of Y being pinched in a position thus
displaced is that, in its neutral position, Y is very close to the
X'/Z knot, and thus blocks X' from moving further down the Z axis even
if a large force is applied to Z.  But if Y was displaced in the first
step before being pinched, applying a large force to Z will displace
X' further down the Z-axis, pinching Y harder.

In this fashion a small force applied to X'' is amplified into
controlling a much larger force applied to Z, and this force
amplification can be about an order of magnitude.  As before, this
alternation between a medium-sized pinching force on Z and a large one
can be repeated many times without reapplying the controlling force on
X''; therefore, like the displacement mode of amplification discussed
earlier, this force mode of amplification also provides arbitrarily
large energy amplification.

Because, when operated adiabatically, the mechanism doesn’t involve
any sliding contact, it should produce very little wear on the
threads.  This is not true of the “Thread power amplification through
braking” mechanism in my note from 02010, which therefore might suffer
from a short working life and frequent breakdowns.  Although it does
rely on braking, this new mechanism does not have that problem.

I’m pretty sure this is the breakthrough I’ve been seeking in textile
computation for 14 years.  It enables kilohertz digital computation
with Paleolithic materials and precision, providing a very convincing
demonstration that automatic digital computation is purely a
philosophical breakthrough, not a technological breakthrough as
conventionally understood.  Not only does automatic digital
computation not require electronics; it doesn’t even require *metals*.
And I’m fairly sure that this mechanism will work reliably enough that
it can do more than just tabulate trigonometric functions in between
breakdowns.

Some elaborations:

The thick cord X can be held in place by at least one additional fixed
thread Z', in or near the Z-direction, through a knot close to the
X-position of Y.  This means it doesn’t actually have to be thicker
than X' and Y in order to achieve the lower compliance presumed by the
above mechanism.  It is also possible to actuate X and X'
symmetrically by pulling Z and Z' in opposite directions to pinch Y,
but this is an unnecessary complication; grounding Z' to the frame is
sufficient.

If there are multiple Yᵢ/Xᵢ'' pairs of threads, any one of them
remaining at its neutral position will block Z from moving.  However,
in this situation you cannot always release the force on an Xᵢ''
thread adiabatically, because some of the Yᵢ may not be pinched.  It
should be possible to reliably pinch one Yᵢ on each side of a single
Z-thread.

Z and X'' can terminate at their knots; they do not need to extend all
the way through the mechanism, though that will be useful in many
cases.  In particular, if Z extends all the way through the mechanism,
then the position of Y determines whether Z can transmit force past
it.

If we tie multiple Xᵢⱼ'' threads to a single Yᵢ near X, generally the
resulting lateral displacement of Yᵢ will be the maximum of all the
Xᵢⱼ'' displacements, leaving others slack.  This is sufficient for
logical OR or, using negative logic, AND.  Positive-logic AND is also
available by running a single Z thread through a series of X/X'/X''/Y
mechanisms, using the second (force-amplifying) procedure.

It may be worthwhile to have an additional fixed X cord past X' on the
Y-axis to reduce the tendency of X' to slide along Y when it begins to
pinch.  And of course X can be a grounded, rigid part of the machine,
rather than a thread, though with Paleolithic fabrication technology
that would be worse instead of better.
