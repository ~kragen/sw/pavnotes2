The design of Pennoncel, a tiny architecture
============================================

Pennoncel is a new, tiny instruction set architecture balancing
several goals in an unusual way:

1. software implementability in very little code (a [Chifir-like][2]
   “fun afternoon’s hack” from a “one-page description”), with high
   likelihood of compatibility;
2. software implementability with reasonable performance and software
   memory protection without great complexity;
3. reasonably easy programmability, including a flat 32-bit memory
   space;
4. hardware implementability in very little hardware and reasonable
   performance (in-order pipelined implementations).

It leans a lot harder on #1 than most existing architectures, with
only a single addressing mode, four instruction formats, and 18
unprivileged instructions.

Unlike Nguyen and Kay, I don’t think a one-page description can be
complete enough, because you need at least some tables of test vectors
to clarify ambiguities in the spec.  Chifir is the first attempt at
this kind of thing that’s good enough to criticize, but it's still
flawed.

The Chifir paper says, “[E]mulation causes a slowdown of 2 to 4 orders
of magnitude.”  I think a more typical slowdown for emulating a
instruction set designed for software implementations, like
Pennoncel’s, with a simple switch loop, should be about 1 order of
magnitude; this should be reducible to about a 50% slowdown with a
simple QEMU-style JIT compiler.  QEMU emulates a 32-bit ARM on this
Ryzen 5 3500U with a slowdown of about 2–3, less than half an order of
magnitude.

[2]: https://archive.org/details/tr2015004_cuneiform

Some initial considerations
---------------------------

Having byte-aligned instruction fields is a big win for software
implementability.  So
Pennoncel has byte-aligned instruction fields, despite incurring a
heavy cost in code density.

With CPUs whose instruction sets are unduly orthogonal, like the ARM,
we have a sort of similar situation to the one I described in file
`scheme-implicitness.md`; in particular where the program counter is a
general-purpose register, where the stack pointer is, or where the CPU
registers are accessible as addressable memory locations.  It makes a
simple, low-performance implementation of the ISA easier to write,
while making higher-performance versions more difficult, because the
CPU has to rediscover information the programmer already knew.

(Of course, if you want a *minimal* ISA, maybe you don’t want to have
a register file at all — Chifir and the GreenArrays F18A don’t — but
it sure does help with software implementations.)

### Putting PC in a GPR ###

I think the first of these, making the PC a GPR, is probably a win for
a reasonably wide class of software implementations.  A very simple
software implementation can just store the PC in an in-memory array of
registers along with the other registers, while higher-performing
implementations can statically identify which instructions affect
which registers, so the performance cost is quite reasonable.  So the
Pennoncel program counter is a general-purpose register.

Making the CPU registers addressable via pointers, by contrast, or
requiring the automatic detection of self-modifying code, contribute
little simplicity and have heavy performance costs on implementations
more than the most basic.

### Oriansj’s thoughts on minimal instruction sets ###

Putting the PC in a GPR saves you the return instruction:

> 03:59 &lt;@oriansj> one would only need to encode load{8,16,32,64},
> loadu{8,16,32,64}, store{8,16,32,64}, add, sub, shli, shri, call,
> return, jz, jnz and a couple knight halcodes (push and pop would
> certainly make the lower stages much easier).  if we make a register
> be the PC, we can eliminate separate call, return instructions.  jz
> and jnz could be done with conditional add and conditional substract
> instructions.

Normally even machines where the PC is a GPR have a separate CALL
instruction, but they don’t usually have a RET.  You can implement RET
with an indirect jump.

I think oriansj’s list omits bitwise instructions and comparison for
order.

### The RISC-V design ###

The current RISC-V spec (version 20191213) has the following
instructions in the base RV32I instruction set,
40 by my count: addi, slti, sltiu, andi, ori,
xori, slli, srli, srai, lui, auipc, add, slt, sltu, and, or, xor, sll,
srl, sub, sra, jal, jalr, beq, bne, blt, bltu, bge, bgeu, lw, lh, lhu,
lb, lbu, sw, sh, sb, fence, ecall, and ebreak.  Pseudo-instructions
like j, li, la, mv, beqz, ret, and so on are synthesized from these
instructions, in many case using the zero register (see below).  There
is a single addressing mode for loads and stores: register plus 12-bit
immediate offset, which is not shifted.

RISC-V is not an attempt at a minimal set, but rather a reasonable
compromise among many different requirements.  10 of these 40
instructions are essentially optimizations of loading an immediate
constant into a register and then doing a register-register operation
with it.  The only one that would be eliminated by making PC a GPR is
auipc.  You can probably get by with two conditional jumps, so
Pennoncel has bne and bgeu rather than oriansj’s jz and jnz.  The fence
instruction is specifically for multithreading and can be implemented
as a nop in a single-threaded machine.

The absence of ARM’s ldm/stm instructions and
postincrement/predecrement addressing modes is inconvenient when
programming RISC-V by hand, and certain kinds of code become much less
efficient on simple in-order cores that do no op fusion, but the
implementation of interrupt handling is enormously simplified.

RISC-V defines an instruction called fence.i
outside of the basic RV32I set
which on a single-threaded CPU
ensures that you can run any newly generated instructions.
However, it has no parameters, applying to all of memory,
and it isn’t useful under common OSes because userland code
can be migrated to a different CPU core or other thread,
and fence.i doesn't guarantee anything then.

### Memory access instructions ###

Of oriansj’s 20 candidate instructions above, 12 are memory access.
Of these, 3 make sense only on 64-bit machines, and the
signed/unsigned distinction only makes sense for
smaller-than-register-sized accesses.  ARM got by for many years
([until ARM7 in 01994][3]) without halfword or sign-extending loads.
So I think we can cut down those 12 memory-access instructions to 4
for a 32-bit machine: load32, load8, store32, and store8.

[3]: https://en.wikipedia.org/wiki/Transistor_count

### Putting SP in a GPR ###

The ARM got by with putting the stack pointer in a GPR because it had
all kinds of crazy addressing modes (which could be used with any
register).  The current fashion is to move the stack pointer only on
entry and exit to a function, as if it were a frame pointer, and to
index your local variables off of it, as if it were a frame pointer.
The ARM EABI, for example, requires you to keep your stack pointer
8-byte aligned at all times, so you can’t push or pop just a single
item; it’s typical to only change the stack pointer at subroutine
entry and exit.  This means that you don’t need those crazy addressing
modes any more, just base-offset addressing, and there's no problem
putting the SP in a GPR.  So Pennoncel does.

In most programs, most memory accesses are to local variables.  A
large register set can avoid putting these in RAM, at the cost of
saving and restoring them on subroutine entry and exit.  In a simple
software implementation with software memory protection, accesses to
registers don’t involve any bounds checking.  What would be required
to get that same benefit for accesses on the stack?  I don’t know.

### Condition codes and instruction fields ###

Condition codes are a big obstacle to software implementations;
they add implicit effects to ALU instructions.  Without condition codes,
you probably don’t get to conditionalize add and subtract
the way Oriansj suggested.
So for software implementation, it’s probably better to have combined
compare-and-conditional-branch instructions like MIPS and RISC-V, and
as a happy accident, the currently fashionable belief seems to be that
this is also optimal for fast hardware execution, because the flags
register doesn’t introduce implicit dependencies for superscalar
execution, and a simple in-order implementation can do a single-cycle
compare and branch instead of needing two separate instructions.

Pennoncel follows RISC-V here.

### Zero registers ###

Many RISC processors, including RISC-V but not ARM, have a “zero
register” which always reads as zero, and consequently writes to it
have no effect.  This is especially easy to implement in hardware,
where you just replace the SRAM cells for that register with drivers
to zero, but it complicates simple software implementations somewhat;
they need a special-purpose conditional either for writing registers
or for reading them.

Ben Sittler points out that if it’s inconvenient to program without a
zero register, you can reserve a register in your ABI to always be
zero.

So Pennoncel does not have a zero register.

### Bitwise arithmetic ###

You probably need some kind of bitwise operations; the usual set is
AND, OR, XOR, NOT, and sometimes abjunction.  If you have less than
this, you need to emulate them in software; for example, instead of
writing [abjunction][0] as

        bic r0, r1    @ r0 &= ~r1

you have to do separate NOT and AND operations.  This is kind of a
waste if the underlying computational substrate is perfectly capable
of doing abjunction operations.  But bitwise operations are rare
enough that the overall cost of this is generally fairly low, so you
can prune this down to a minimal set, ideally just one bitwise
operation.

I’ve previously written about my love of abjunction.  Of these five
operations, it’s the only one that’s universal or [functionally
complete][1] by itself, the way NAND is — if you have a way to get -1,
anyway, without which it is “falsity-preserving” and therefore not
universal.  NOR is just as good as NAND.  But abjunction is the only
one of these three that is commonly useful on its own, so if you’re
going to have just one bitwise operation, I think it should be
abjunction.  (Chifir chose NAND.  [Wirth’s RISC][4] provides AND, OR,
XOR, and abjunction, and 34 instructions in all.)

[0]: https://en.wikipedia.org/wiki/Abjunction
[4]: https://people.inf.ethz.ch/wirth/FPGA-relatedWork/RISC.pdf
[1]: https://en.wikipedia.org/wiki/Functional_completeness#Minimal_functionally_complete_operator_sets

The Pennoncel userland instruction set
--------------------------------------

Our CPU has 32 general-purpose registers, 32 bits each; register 31 is
the program counter, and there is no zero register.  Instructions are
32 bits long.  Loads and stores have a single addressing mode, using
one GPR plus an 8-bit (unshifted) offset.  Memory is addressed in
units of 8-bit bytes.

Reading pc (r31) produces the address of the next instruction to be
executed, because that’s what happens naturally in a software
implementation.  Writing it performs a jump, with no delay slot.

(This still contains a lot of motivating material that would be
omitted from a short spec, but still omits explanatory text it would
need.)

### Constant instructions ###

- li16 rd, #w: sets rd to the 16-bit signed constant w, sign-extending
  it to 32 bits, almost [like ARMv7’s movw instruction][5], except
  that ARMv7’s instruction zero-extends instead of sign-extending.
- movt rd, #w: sets the high 16 bits of rd to the 16-bit constant w,
  leaving its low 16 bits untouched.  An arbitrary 32-bit constant can
  be loaded with a sequence of these two instructions.  This is
  exactly like ARM’s movt.

[5]: https://community.arm.com/arm-community-blogs/b/architectures-and-processors-blog/posts/how-to-load-constants-in-assembly-for-arm-architecture

### Memory instructions ###

In all of the memory instructions below, k is a signed 8-bit constant.
Addresses need not be aligned.

- load32 rd, [ra, #k]: sets rd to mem[ra + k].
- store32 rs, [ra, #k]: stores rs to mem[ra + k].
- load8 rd, [ra, #k]: sets rd to the byte mem[ra + k], zero-extending
  it.
- store8 rs, [ra, #k]: stores the low 8 bits of rs at mem[ra + k].
- clarify ra, rb: ensures that any code in the range of addresses ra
  (inclusive) to rb (exclusive) can be run successfully, [like the
  `__clear_cache` function on ARM Linux or FlushInstructionCache on
  Microsoft Windows.][6] In a hardware implementation of Pennoncel
  this may flush the instruction cache; software implementations using
  dynamic binary translation might rely on this instruction to know
  when to compile code.

[6]: https://community.arm.com/arm-community-blogs/b/architectures-and-processors-blog/posts/caches-and-self-modifying-code

### Arithmetic/logic instructions ###

- add rd, ra, rb: sets rd to ra + rb.  If ra and rb are the same
  register this performs a 1-bit left shift.
- sub rd, ra, rb: sets rd to ra - rb.
- bic rd, ra, rb: sets rd to ra & ~rb, their abjunction.  Either this
  or sub can be used to clear a register.
- addi rd, ra, #k: sets rd to ra + k, where k is a signed 8-bit
  constant.  This provides mov when k == 0.  When rd is pc, it
  provides register-indirect unconditional jump, including subroutine
  return.  When ra is pc, it provides pc-relative addressing.
- srli rd, ra, #k: sets rd to unsigned ra >> k, which is a constant.
  Perhaps it would be better to turn this into a rotate.

### Control-flow instructions ###

- bne ra, rb, #k: if ra != rb, adds the signed 8-bit constant k,
  left-shifted by 2 bits, to pc, thus executing a conditional jump
  between 512 bytes backward and 508 bytes forward, relative to the
  instruction following the bne.  That is, if k == 0, the next
  instruction executed is the one following the bne.
- bgeu ra, rb, #k: the same, but jumps if unsigned ra ≥ rb insead of
  if they are unequal.
  
### Subroutine call and multiplication ###

That’s 14 instructions, and it’s enough to write most programs fairly
straightforwardly and not absurdly inefficiently, if a bit
long-windedly.  I think the exceptions are subroutine call and
multiplication.

Without a special instruction, subroutine call would look something
like this:

            addi lr, pc, #4        @ load address of instruction after jmp
            addi pc, pc, #(dest-.) @ jump to entry point

Except that that only has space for an 8-bit offset, allowing you to
jump anywhere within ±2048 bytes.  For any farther call you have to
load the offset into a register, which is ridiculous:

            addi lr, pc, #12
            li16 ip, #:lower16:dest  @ this is ARM-style syntax
            movt ip, #:upper16:dest
            addi pc, ip, #0          @ this is a mov to pc

It would be dramatically better to have a `call` instruction with a
reasonable call range and which also stores the return address in the
link register.  For example, you could have a 24-bit jump offset.

Multiplication is just absurdly faster if you can use a hardware
multiplier.  Like, typically an order of magnitude.

So, bringing the instruction count to 18:

- jal #k: copies pc to lr (r30) and adds the 24-bit signed number in k
  to pc.
- jalr ra: same, but the entry point is in register rd.
- mul rd, ra, rb: sets rd = ra \* rb, the low 32 bits.
- mulhu rd, ra, rb: sets rd = ra \* rb >> 32, the high 32 bits, using
  unsigned multiplication.

### Instruction encodings ###

There are four formats: BBW, BRRK, BRRR, and BJ.

BBW encodes li16 and movt.
The first byte is the opcode, the second byte names
a register, and the final two bytes are a 16-bit immediate operand.

BRRK encodes addi, srli, load32, store32, load8, store8, bne, bgeu,
and jalr.  The first byte is the opcode, the second byte names a
destination register, the third byte names a source register, and the
fourth byte is an 8-bit immediate, treated as signed twos’-complement.
In the case of jalr, the third
byte is the register used.

I had considered making the immediate argument sometimes unsigned, but
concluded that the extra complexity was not worth the improved
expressiveness.

BRRR encodes clarify, add, sub, bic, mul, and mulhu.  The first byte is the
opcode, the second byte names a destination register, the third byte
names source register ra, and the fourth byte names source register
rb.  The clarify instruction does not use its destination register.

BJ encodes jal.  The first byte is the opcode; the other three bytes
are the 24-bit offset to add to pc after sign-extending them.

Byte fields encoding registers have bit 6 set (the 64 bit), so the
byte values naming the 32 registers range from 64 (ASCII @) to 95
(ASCII \_).

The opcode bytes are also printable ASCII:

- li16: # (0x23) (BBW)
- movt: $ (0x24) (BBW)
- addi: a (0x61) (BRRK)
- srli: > (0x3e) (BRRK)
- load32: @ (0x40) (BRRK)
- store32: ! (0x21) (BRRK)
- load8: ' (0x27) (BRRK)
- store8: " (0x22) (BRRK)
- clarify: c (0x63) (BRRR)
- bne: ~ (0x7e) (BRRK)
- bgeu: ? (0x3f) (BRRK)
- jalr: ^ (0x5e) (BRRK)
- add: + (0x2b) (BRRR)
- sub: - (0x2d) (BRRR)
- bic: \ (0x5c) (BRRR)
- mul: \* (0x2a) (BRRR)
- mulhu: x (0x78) (BRRR)
- jal: . (0x2e) (BJ)

I feel like 18 instructions, 4 instruction formats with 100%
byte-aligned fields, 32 32-bit registers, and one addressing mode is
something you could implement in an afternoon; and it also promises to
be a reasonably usable machine to program on, even if not as luxurious
as ARM.  Finally, a simple JIT compiler ought to be able to compile
code for this ISA to about 1.2 instructions per instruction on any
common 32-bit or 64-bit platform.

A sketch of the kernel instruction set
--------------------------------------

By adding some minimal additional mechanism, Pennoncel can support
multiple processes with protected memory.  We need:

- a kernel-mode bit, tracking whether the processor is currently in
  user mode or kernel mode;
- a mechanism to trap into kernel mode;
- an ecall or svc instruction for userland code to invoke such a trap;
- a way for the kernel to set up the trap handlers so they point at
  its own code;
- a base register and a bound register, delineating the segment of
  memory available to non-kernel-mode code;
- to offset all userland memory references by that base register, and
  trap into kernel code if they exceed the bound, so that buggy or
  malicious userland code cannot corrupt another process or the
  kernel;
- a watchdog timer that counts, one clock cycle at a time, until
  reaching a predetermined value,
  at which point it traps into kernel mode, so that an
  infinite loop in userland code will not hang the machine;
- at least a scratch register or two reserved for kernel code so that
  it can save the userland state safely when a trap is invoked.

For virtualizability, attempting to read or set any kernel-mode state
from userland (the kernel-mode bit, the base register, the limit
register, and the watchdog timer) should also trap into kernel mode.
If the kernel code then simulates the effect of the kernel-mode
facilities for the benefit of the user code, the user code can
recursively use the same facilities to run untrusted code of its own
at full speed.

Most of the above is needed anyway in many environments; for example,
you need an interrupt-handling mechanism that can save userland state
for interrupt-driven I/O, and if you’re simulating of this instruction
set on another architecture, you need base+bounds checking for
software fault handling.

