Right now the humans command on the order of 10²⁰ floating point
operations per second.

The Bitcoin network is computing 7'983'000 terahashes per second,
which is 7.98 × 10¹⁸ hashes per second.  This suggests that the amount
of computation being done in Bitcoin ASICs is also close to 10¹⁹–10²¹
word operations per second, depending on your definition.  While this
does not count as fully programmable computation, it gives us a fuzzy
upper bound: if the amount of fully programmable computation running
around loose were many orders of magnitude higher than this, then
Bitcoin mining on CPUs and/or GPUs would be profitable, and it isn’t.

Roughly speaking for US$1500 you can get a GPU rig with 10 teraflops,
10¹³ floating-point operations per second, of which half is the GPU.
The GPU market is about US$50 billion per year, but only [US$14
billion of that is high-powered “discrete” units like these][0],
though [a different source says that’s US$14 billion per quarter and
not per year][4], while [NVIDIA’s revenue is US$27 billion per
year][1].  This works out to something on the order of 20 million such
GPUs per year, or 2×10¹⁹ floating point operations per second per
year.  If we figure the total deployed compute power is about the last
two years’ worth, it’s closer to 10²⁰ flops.

[0]: https://www.tomshardware.com/news/jpr-q3-2021-desktop-discrete-gpu-shipments
[1]: https://webtribunal.net/blog/gpu-market-share
[4]: https://www.jonpeddie.com/press-releases/graphics-add-in-board-market-reached-13.7-billion-for-q321-showing-double-d

There are about 5 billion cellphones ([3 billion running Android][2])
each capable of about 2 billion floating point operations per second,
plus another comparable amount tucked away in data centers.  This
gives us 2 × 10¹⁹ flops.

[2]: https://www.theverge.com/2021/5/18/22440813/android-devices-active-number-smartphones-google-2021

This raises the question of the cheapest way to get access to a lot of
computational power.  For numbers up to 10¹⁵ operations per second the
standard answer is to just buy computers, though plausibly a criminal
answer that would work for some purposes is to just buy access to a
botnet, giving you cheaper access to otherwise unutilized computing
power (and sticking someone else with the power bill).

But consider the possibility of building your own computer.

CPUs with speeds of 10⁶ word operations per second with longevities of
10¹² operations or more can be reached by a wide variety of means:
MEMS electrostatic relays, vacuum tubes, discrete CMOS chips,
square-loop ferrite/diode systems, and probably Merkle buckling-spring
logic, microfluidics, and even macroscopic mechanical systems.
(Systems that transmit signals by wiggling atoms or ions around rather
than just electrons or photons are at a significant speed
disadvantage.)  It might be feasible to reach higher speeds such as
10⁸–10¹⁰ word operations per second per processor without the kind of
highly specialized equipment used in modern semiconductor fabrication,
but we know that at least 10⁶ is feasible in a variety of ways.

To be equivalent in power to a single modern GPU, you need about 10⁷
of these CPUs.

Supposing that each of these CPUs is built as part of a 3-D printer
capable of printing a copy of itself every month, after one month you
have 2 of them, not 10⁷.  And after two months, you have 4.  Reaching
10⁷ CPUs requires 24 months, two years.  (For a month-long
reproduction time, you need longevity of more than the 10⁶ seconds I
suggested above, because a month is 2.6 × 10⁶ seconds; if the
machines’ lifetime is only 10⁶ seconds you will need the entire
lifetimes of 2.6 of them to manufacture another, so your printing farm
will diminish over time rather than increasing.)

Reaching my estimate above of current human computational power, 10²⁰
word operations per second, requires 10¹⁴ CPUs rather than just 10⁷,
and thus an additional two years.

If each such 3-D printer weighs 1 kg, 10⁷ of them will weigh 10'000
tonnes, and require at least 10'000 tonnes of raw materials to build.
For scarce raw materials such as copper or gold this could pose a
difficulty, but 10'000 tonnes of silica at 2.65 g/cc is only 3800 m³,
a 9.7-m-radius sphere, or a 5-story 300-m² building.  But you probably
need to distribute them over a larger area to get the necessary
energy.

10¹⁴ such machines would weigh 10¹¹ tonnes, 38 billion cubic meters, a
sphere of 2 km radius.  Such an apparatus would still fit comfortably
in any of numerous ocean trenches without anyone noticing, though
scattered around the asteroid belt would probably be a more sensible
place.

Supposing that, instead of weighing 1 kg each, we make the 3-D
printers as small as we can.  Each computer consists of 32768 memory
bits and 32768 switching elements similar to transistors; each of
these bits and switches consists of 256 voxels, 1 μm on a side.  Then
the CPU consists of 2²⁴ voxels totaling 0.016(777,216) mm³; perhaps
the rest of the printer is 7× as big, for a total of 2²⁷ voxels, about
0.134 mm³, 134 μg.  This reduction in size would mean that 10¹⁴ such
machines now weigh just over 13000 tonnes.  However, weeding out
defective machines would become much more difficult.

If it takes a month to deposit those 2²⁷ voxels, that’s a little over
50 voxels per second, which seems like an undemanding deposition rate.
If machines with a larger number of elements can achieve
proportionally higher deposition (and computation) rates, it might be
preferable to design those instead, simplifying the weeding problem
and also allowing for easier programming.

The memory granularity question
-------------------------------

A computer with 32768 bits of memory can run things like a BASIC
interpreter or a chess game but not a WIMP GUI; it’s more like a “home
computer” from the 01970s than like a scientific calculator or a
Macintosh from 01984.

So we have this interesting question: if we have 10²⁰ operations per
second and 10²² bits of memory, what’s the granularity at which we
split them into CPU/memory pairs?  Do we have a single
10²⁰-operation-per-second CPU with 10²² bits of storage?  Ten billion
CPUs each with a terabit of storage?

Part of this is a question of how fast we can get the primitive
switching elements and bits of memory to run.

We can maybe lay out orders of magnitude of the memory granularity
scale like this:

- 1 bit per autonomous computing element: a flip-flop.

- 8 bits: a state machine like the one the Apple ][ floppy disk drive
  used to synchronize.  FPGA LUTs are a little bigger than this,
  typically having 16–64 bits of LUT state.

- 256 bits: a non-programmable four-function calculator with operator
  precedence and parentheses, with 10 digits and 6 levels of nesting.
  This would be plenty for some basic signal processing and PID
  control loops, but it’s probably too little for anything like a
  compiler.

    In theory you could build a programmable calculator this small,
    for example with four 32-bit stack items (128 bits), four-bit
    stack instructions (16 opcodes) with a program of up to 10
    instructions, and the possibility of a single loop.  But I don’t
    think anyone ever has.  The Olivetti Programma 101 reportedly had
    240 “bytes” of read-write memory (in a delay line), and the HP
    9100A had 32768 bits of ROM and [2208 bits of core][5] (19
    floating-point registers with 10-digit decimal mantissas and
    2-digit exponents).

    The Atari 2600 had 1024 bits of RAM plus a few miscellaneous
    registers.

    The individual computing elements in the GreenArrays GA144 chip
    have 1152 bits of RAM and 1152 bits of ROM, plus 324 bits of
    registers.  The GA144 did not see significant adoption because it
    was too hard to program.

- 65536 bits: that’s 8 kibibytes, and you only need half this for a
  home computer capable of running a BASIC interpreter or a chess
  program, or an HP 9100 (if we include the ROM, as we should for
  estimating fabrication complexity, though perhaps it should be
  derated a little), or a Nintendo (2048 bytes of RAM, 16384 bits,
  plus 2048 more of VRAM, and a few miscellaneous registers).  In 8
  kibibytes, even at sub-MIPS speeds, you can run a Forth IDE.  Simple
  network protocols like XMODEM are more feasible than complex ones
  like TCP/IP.

    Typically at this scale computers are programmed in assembly or
    Forth, though that may be in part a historical accident.  It’s
    practical to program them in C or C++ if you have a larger
    computer to run the compiler on.

- 512 kibibits: that’s 64 kibibytes, the maximum size of a PDP-11 Unix
  address space, a CP/M machine, or a 6502-based home computer like
  the Commodore 64, and it’s the smallest size of IBM PC.  This is
  enough for VisiCalc, which needed 32 KiB, or 262144 bits, or for the
  GEOS GUI for the Commodore 64.  Compilers and interpreters are
  common at this granularity of computing, and TCP/IP is an option.

    The smallest bacterial genome is that of *Carsonella ruddii* and
    consists of 159662 base pairs, almost 320 kilobits; but presumably
    the state of a living bacterium would require a great deal more
    information than that to represent.  *Carsonella* is an obligate
    endosymbiont; the smallest free-living organism’s genome is that
    of *Mycoplasma genitalium*, at 580070 base pairs, four times as
    big, more than a megabit.

    However, the granularity of the individual CPU/RAM/printer triples
    in the system could be smaller than the system design, if they
    network together to get the parts of the design they need when
    they need them.

- 16 mebibits: that’s 2 mebibytes, which is Unix or Windows 3.1
  territory.

[5]: http://www.hp9825.com/html/the_9100_part_2.html
