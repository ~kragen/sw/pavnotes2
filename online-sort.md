Suppose a sorting algorithm receives a large (but unpredictable)
number of input items to sort slowly, and once it get an end-of-file
on input, you want it to output all the input items in sorted order
quickly.

You can’t do better than this at outputting sorted items promptly
because the last input item might be the first one in sorted order.
So you can’t output the first output item before you know what the
last input item is.

A simple approach to this problem is to use a binheap: you insert each
item into the binheap as you receive it (in O(log N) time) and then
drain the binheap (also in O(log N) time per item).  But we can do
better than this; for example, if we insert into a trie or balanced
binary search tree (O(log N)) we can traverse the tree in O(1) time
per item, which is much better.
