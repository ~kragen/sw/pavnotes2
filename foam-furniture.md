I was thinking about laminated fiberglass foam furniture again.  I saw
some YouTuber presenting his technique for making #vanlife furniture
out of window screens, latex paint, and construction insulation foam.
Sometimes this is called “Poor Man’s Fiberglass”, though sometimes
that refers specifically to reinforcing the surface with canvas rather
than window screen.

“Literature”
------------

The video I remembered wasn’t [zhkopec][0] nor Jed IntoTheMystery13
who he cites.

zhkopec was using Owens Corning pink “Foamular” R-3.0 half-“inch” XPS
(extruded polystyrene) foam from Home Despot, cutting it to shape with
razor blades, assembling it with drywall screws and “Gorilla Glue”,
and including some wood blocks to support the foam from the wall over
a larger area than the screws can.  Then he reinforces the foam with
white latex “Glidden GPG-0000 Gripper” primer and fiberglass mesh
window screen, trimming the screen afterwards with scissors.  He also
reinforced the corners of his cabinetry with the kind of L-brackets
you use for making cabinets out of shitty particleboard, with holes
for drywall screws.  Then, to make it look vaguely wood-like, he
covers the surface with torn-up kraft paper coated in “Elmer’s
Glue-All”, which is a PVA glue, diluted 3:1 with water.  (I saved you
11 minutes but it might be worth skipping through the video to see
what it looks like.)  Then, to seal his water-sensitive
*papier-mâché*, he coats it with polyurethane paint.

[0]: https://www.youtube.com/watch?v=Zey86bX5L3E

No, the video I saw was [by NØMAD, “Foam Cabinet Prototype -
Improvements on Building Foam Cabinets for Camper Van”][1].  He
decorated his cabinets a bit more using metal latticework (the kind
used in 01960s space heaters, with little four-petal flower holes),
découpage with maps, ⅛" plywood with stain and polyurethane varnish
for facing, diamond-plate aluminum for a kickplate, and a stainless
steel countertop and sink, glued down with “Liquid Nails” glue.  More
importantly, though, he cut his foam panels into finger joints with an
electric hot knife, a four-“inch” “crafter’s knife” from “Hot Wire
Foam Factory”, and didn’t use “Gorilla Glue” for the foam joints
because it’s an expansive glue, which is counterproductive for
furniture dimensional accuracy.  Instead he recommends a glue called
“Foam Fusion”.  Also he was using R-6 1-“inch” foam rather than
½-“inch” (25mm rather than 12mm, in modern units).  Rather than
holding the joints together with drywall screws while the glue sets,
he holds them together with painter’s tape.  And he uses two layers of
(fiberglass?)  screening rather than one.

[1]: https://www.youtube.com/watch?v=IJv0pEjvC34

It turns out “NØMAD”, now with more gray hair, has continued refining
the technology and has a [YouTube playlist][3] from last year.  The
next-to-most-recent video, [“Foam Camper Van Screen & Skins -Foam
Building System Vol. 5”][2], is from early 02023, less than a year
ago.  He points out that wood glue also works adequately in place of
paint and demonstrates some cabinets découpaged with newspaper or
covered in thin carpeting or wood veneer (which he says he wishes he’d
used more of in place of the ⅛-“inch” (3mm) plywood).  He also
switched brands to “Foamular”; not sure if there’s a relevant
structural reason.

He apparently switched to “Gorilla Glue HD” at some point, so I guess
it didn’t turn out to be all that expansive after all, and used spray
adhesive to glue his carpet to the wood.  I guess you’d have to make
sure your spray adhesive wouldn’t eat whatever foam you were using.

(Saved you another 18 minutes.)

[2]: https://www.youtube.com/watch?v=A9wJfenHcOs
[3]: https://www.youtube.com/playlist?list=PLFv8fr7l-UlKGvgubWqwozEOunUz4M2Fl

In [his latest video on the technology][10], NØMAD explains how he
used XPS foam for the floor in his van, on top of the factory stamped
ribbed sheet steel floor, which isn’t really relevant to building
furniture out of it except that he had cut some slots in it to make
finger joints with his foam cabinetry, holding it in place.  He
covered it up with “chipboard”, which sounds like OSB but looks like
particle board, and then put down some kind of Pergo-like vinyl
plastic tongue-and-groove laminate flooring on top of that.  The
laminate (“HydroStop Old Blue Sea Floor Waterproof Click Lock Luxury
Vinyl Plank” from Home Despot) turned out to be super shitty.

(Saved you another 8 minutes.)

[10]: https://www.youtube.com/watch?v=cGvFEtWMW7M

His [first video of the “Foam Bones Building System”][11], from March
02023 (several years after the original videos) is pretty informative.

In it he points out that the flexibility of foam (when you don’t
stiffen it with too much fiberglass) is a benefit for #vanlife because
it induces less stress when confronting the unavoidable strains of the
highway, so the foam furniture is much more durable than equally
strong wood furniture would be.

He also gives a bunch of areal density measurements, such as “foam w/
2 screen sides: 5.10 oz / sqft”, which in modern units would be
1.56kg/m², a surprisingly high number for me.  Most of this is his
25mm-thick foam, “3.30 oz / sqft” = 1.01 kg/m², 40kg/m³, a very high
density.  He’s paying US$20 for a 4' × 8' sheet of the foam (2.97m²) =
US$6.70/m²; US$70 for 4' × 100' of the fiberglass screen = US$1.88/m²;
“Glidden Gripper” latex primer, US$24 per “gallon” (US$6.34/ℓ)
covering 400 square “feet” (37m²) = US$0.64/m².

He strongly recommends against using foam for cabinet doors and drawer
fronts, because they get too much wear and tear.  I feel like maybe
that’s a matter of the surface and the smoothness of the corners,
though.

Interestingly, he suggests using drywall anchors for driving screws
into the foam.  It turns out he’s talking about the plastic kind that
screws into the drywall with a very wide thread, held in place with
some expansive “Gorilla Glue”.  He also suggests making a scale model
out of cardboard with posable dolls to test the ergonomics of a
design.

He points out that you can curve the foam more easily if you melt cuts
into the inside of the curve.  I feel like this is a vastly underused
technique in his overall build, for two reasons.  Curving vastly
increases the moment of inertia of the panel, which is why my roof
here is corrugated sheet steel instead of flat sheet steel.  And
curving also eliminates sharp corners which will experience high
stresses on impacts.

He reports that he’s paying US$1.88 per “square foot”, about
US$20.20/m², for his white oak wood veneer.

Apparently “Gorilla HD” is “Gorilla Glue Heavy Duty”.

There, saved you 50 minutes.

[11]: https://www.youtube.com/watch?v=38DMX7s9-X8

[His second video in the “Foam Bones” series is about glues][34].  He
tests different kinds of drywall anchors, the screw-in “E-Z Anchor
Lite” spiral type and the metal-plastic-deformation type, held in with
different glues, concluding that the “Gorilla Glue” regular and “Heavy
Duty” work the best for this, probably because they expand out into
the foam.

“Gorilla Glue” [turns out to be a polyurethane adhesive][35] made from
mostly diphenylmethane diisocyanate.  He’s testing it against “Liquid
Nails for foam”, “Foam Fusion”, “Styrogoo”, and “Foam Fast”.  (It
occurs to me that a saturated solution of styrofoam in a nonpolar
solvent like acetone would probably work well.)

He screwed up the adhesion test and tested the glues in tension rather
than shear as he intended, but didn’t notice.  Also he didn’t control
the lever arm for the tension test.  He came out with 43 “pounds” for
the “Gorilla Glue”, 39 for the “GGHD”, 32 for LN, 29 for “Foam
Fusion”, 16 for “Foam Fast”, 11 for “Styrogoo”.  Since he only did
each test once it’s hard to evaluate how reproducible they are, but
they don’t look very reproducible.  The “Foam Fusion” and “Liquid
Nails” apparently didn’t have time to cure in 24 hours because they
weren’t exposed enough to air.

His screw pullout tests got 113.5 “pounds” on the “endgrain” of ⅜
“inch” plywood, 8.5 and 10.1 “pounds” for an unaided screw in the XPS
foam, 5.2 “pounds” for the expanding drywall anchor (or 13.8 held in
place with “Styrogoo”, or 20.9 held in place with “Foam Fast”, or 47.8
held in place with GG), and 18.5–32.1 “pounds” for the spiral drywall
anchors.  Except that the spiral drywall anchor held in with GG took
61.6 or 87.7 “pounds” of pullout, and with GGHD, 74.5.

He tried some metal spiral drywall anchors but “voted them off the
island” because the screws kept slipping out of the anchors in his
pullout tests.

It’s appealing to think that you can get reasonable attachment
strength to weak styrofoam by infusing it with much denser expanding
polyurethane foam, and I suspect this might work better with EPS,
because the PU could expand further into the foam.

(Saved you 27 minutes.)

[34]: https://www.youtube.com/watch?v=UY9glTnDDw0
[35]: https://en.wikipedia.org/wiki/Gorilla_Glue

[His third “Foam Bones” video covered foam anchor failure][41].  He
tried filling up a crater carved into his vertical support by a failed
“E-Z Anchor” with “Gorilla Glue”, which didn’t work all that well, but
in general the idea of hardening a dense expansive foam in a hole like
that seems very useful to me.  It was just too runny; his duct tape
dam didn’t work well enough at holding it in the hole.

(Saved you 10 minutes.)

[41]: https://www.youtube.com/watch?v=iGCjCnDTwh0

In his [fourth “Foam Bones” video][44], he covers the installation
sequence of the “foam bones” into the van after having assembled the
full mockup in his driveway.  This involves a lot of glue.
Interestingly, he claims that the “Gorilla Glue” (and also “Foam
Fusion”) is brittle once it sets.  But I thought GG was PU?  He says
the GGHD *is* flexible and rubbery when it sets, and doesn’t expand or
run.

(Saved you 16 minutes.)

[44]: https://www.youtube.com/watch?v=e7zYsPhWk-g

So that’s the whole building series, just not in order.

His [“foam bones van tour” video][42], made shortly before the “Foam
Bones” series above, reveals that the particular XPS panels he’s using
have a compressive strength of 15 “psi”, 103 kPa in modern units.
Most of the rest is demonstrating the very nicely designed living
space inside the van with movable countertops, drawers, composting
toilet in a drawer, etc.  This is possibly the most impressive, sci-fi
household-organizer tour video I’ve ever seen.  But it doesn’t talk
much at all about foam furniture.

[42]: https://www.youtube.com/watch?v=qWkTqKjxKNg

I was thinking about other binders and stiffeners (see below).
Portland and lime are too alkaline for normal fiberglass, but
[papercrete is a thing][4] (3 parts by volume wet paper pulp, 2 parts
portland cement, 1 part perlite, plus enough water to make a plastic
mass), so they aren’t too alkaline for paper.  And [cellulose fiber
such as “ULTRAFIBER500” is commercially sold for concrete
reinforcement][7].  Waterglass adds plasticity.  Also, if you’re a
stoner, [hempcrete][5].  Blow-in cellulose insulation, sawdust, straw,
and non-pulped paper (such as newsprint, or kraft paper, or old
invoices) seem like other appealing sources of cheap cellulosic
reinforcement.  Basalt fiber is also available, and portland cement
premixed with fiber reinforcement is sold commercially.

[4]: https://www.youtube.com/watch?v=YgIh8EfhnPg
[5]: https://www.youtube.com/watch?v=4TPFgJC1xx8
[7]: https://www.youtube.com/watch?v=pfT72GC7pmw

Geopolymer, phosphate-cement, or mineral-paint binders might also
work, and being less alkaline, could accommodate a wider range of
reinforcement types.

Mineral-paint binder is plausibly the cheapest kind of waterproof glue
(though waterglass alone may not be waterproof without treatment with
something like calcium chloride) and can be greatly diluted in water.
This is likely the cheapest way to attach cellulose reinforcement to
foam, and it would probably be okay for fiberglass too.

[The L Wood by Lucy][6] did some simple tests of the strengths of
different kinds of glues to connect two pieces of (XPS?) foam
together, though I feel like maybe it’s better to get the strength of
your joints from the surface sheets instead of the filling.  Wood glue
broke easily in all three of her tests, and also failed to set in 48h
because it depends on water evaporation to set, and her foam is a
closed-cell foam.  “Gorilla glue” did expand as NØMAD predicted,
opening up a space in the joint, but when clamped with masking tape,
the break line went through the foam instead of the glue.  She says
the *clear* “Gorilla Glue” does not expand, and in two of her three
tests, also broke through the foam instead of the glue.  “Foam
Fusion”, which she’s enthusiastic about, also didn’t break at the
glue.

(Saved you 11 minutes.)

[6]: https://www.youtube.com/watch?v=0Uz9YeehLU4

[Pascal][32] made a sequence of videos on using corrugated cardboard
rather than foam for his van build.

[32]: https://www.youtube.com/playlist?list=PLFEo_2JeQbGMw4mCGGH8pJv2nU4Dy2eav

Material-structural thoughts
----------------------------

Looking at NØMAD’s carpeting, it occurs to me that AstroTurf, *media
sombra*, aluminum foil or flashing, *friselina* (nonwoven polyester),
and fiberglass boat cloth might be good options, where by “good” I
mean “cheap”.  I’d have to compare their costs.

A thing he seems to be unaware of is the difference between strength
and stiffness; at one point he suggests using nylon screening as
interchangeable with fiberglass screening — evidently he hasn’t tried
it!  In a sandwich panel (see file `sandwich-theory.md`) the role of
the bread is stiffness, which causes the sandwich to be stiff in
flexion and therefore strong in buckling.  Its strength is irrelevant;
its stiffness is paramount.  Nylon sucks at stiffness but is fine at
strength.  (See below, though.)

It occurs to me that if the foam itself doesn’t provide enough
rigidity in shear (the cheese in a sandwich panel is stressed in
shear, transferring all the compression and tension to the
surface-sheet bread) you could make long, thin beams of the foam
rather than big panels, with bread on all four sides of the beams,
like rectangular or square structural tubing.  Then you can edge-glue
a bunch of these together like a wood floor or butcher block to get a
panel again.

For even more stiffness, you can make a sandwich where the surface
sheets are foam sheets, stiffened on the outside to resist tension and
compression, and the filling is mostly air with stiffened foam spars
crisscrossing it to resist shear.  Or curve/corrugate the panel.

If you want *cheap* stiffness, you can’t beat portland cement with
building sand, or maybe lime with building sand.  See above about
that.

Alabaster is almost as cheap as those materials, and almost as stiff,
and may have other benefits: a purer white appearance, much faster
curing time, and easy shaping by surface abrasion once it’s applied.

Possible alternatives to sodium silicate for making concrete cling to
sandwich panels as you’re making them, instead of dripping off, might
include food gums like pectin, carboxymethylcellulose, and
hydroxypropylmethylcellulose.  [“Shanghai Honest Chem” sells HPMC][8]
for such a purpose (for making ceramic tile mortar stick to walls),
and so does [“Shandong Sleo Chemical Technology Co., Ltd.”][9], so I
guess they think people will buy it.

[8]: https://www.youtube.com/watch?v=v4pwPqkH8Vw
[9]: https://www.youtube.com/watch?v=7hDtjyeY47o

It occurs to me that if you use a fusible facesheet material like
HIPS, you can hot-wire cut the already-prefabricated panel.  That
won’t work with paper or fiberglass.  Polyimide melts hotter than
polystyrene but also might work, but it has other disadvantages.

NØMAD’s parallelepiped approach makes design easier, but it has the
result of greatly reducing rigidity compared to designs that
incorporate curved walls and non-parallelogram spaces.  Much less
material, especially much less foam, could achieve the same rigidity
with a better design.

His mortise-and-tenon finger joints don’t take advantage of the
opportunity to pass screening through the mortises to hold the joints
together, which seems like it would be a good idea.

Local supplies
--------------

Here in Argentina, insulating foam panels for building are universally
expanded polystyrene rather than extruded, with vastly inferior
structural properties and slightly inferior insulation properties as
well, and rather small, at only 1m×1m.  Foil-faced sandwich versions
and polyisocyanurate versions are nonexistent.

But expanded polystyrene *is* available; sign shops use it to make
so-called “letra corpórea” or “letra polifan”,
non-flat letters for signage, using CNC
hot-wire or hot-needle sheet cutters.  (Sellers attempt to market it
for construction insulation as well, to no apparent effect.)  The
normal brand name is “Polyfan”, which is specifically 33kg/m³ extruded
polystyrene panels (so say [Batik][12], [Celulosa][13], [Bicha][14],
and [Polinorte][15]), and comes in many thicknesses but a single sheet
size, 600mm × 1250mm, which is ¾m².  The prices are fairly strictly
proportional to foam volume: Polinorte offers a 20mm sheet (the
thinnest) at AR$14660 (US$14.40), while a 50mm sheet (the thickest) is
AR$36540, which is AR$110 off from a simple linear scaling.  US$14.40
for ¾m² is US$19.20/m².  [Some other vendors sell it for about 30%
less][16], AR$10500 for a 20mm sheet from Tiendas Grafisur Avellaneda.

This is not only dramatically more expensive than the price NØMAD
reports, it’s dramatically more expensive than the price I got two
years ago for the EPS panels, which were US$0.55/m² to US$5.03/m² (see
file `material-observations-02022.md`, section 02022-11-09), with
thicknesses from 10mm to 30mm and densities from 12 to 25 kg/m³.
Likely those panels are still available, still cheaper, and still
lighter.  There are also a bunch more observations in that file and in
file `sandwich-material-pricing.md` about candidate sandwich panel
materials.

Window screen material here weighs on the order of 110g/m² and costs
on the order of US$2/m², which I guess means US$18/kg.

[Blowable cellulose insulation is available][17], sold for about US$15
per 2.6m³ bag.  I don’t even know how to estimate how much area that
could cover.

[*Media sombra* costs US$0.75/m²][18], which is surprisingly cheap.
[Sulfite paper is US$18][19] for a roughly 200-meter-by-400-mm roll,
23¢/m², actually sold by weight (8.5kg, so US$2.10/kg).  [Astroturf
(“cesped sintético”) is US$6/m²][20].  [220g/m² fiberglass roving
cloth is US$5/m²][21], which I guess works out to US$23/kg.  [110g/m²
fiberglass mesh][22] from Chemia Online is US$55 for a 50m² roll,
which works out to US$10/kg, which I suspect is soda-lime glass rather
than E-glass.  [700μm oak veneer][33] costs US$23 for 210mm × 2400mm,
0.504m², so US$46/m² or US$65/ℓ (and thus about US$65/kg), a bit more
than twice the price NØMAD reports.

There’s a flea market downtown I walked through today.  Plausibly, at
the flea market, used cotton clothing might cost less than US$2.10/kg.

In file `material-observations-02022.md` I had bought 10μm aluminum
foil for US$1.23/m², which works out to about US$45/kg.  Since
aluminum is comparable in density to glass, but floppier, I think that
high price leaves it out of the running.  However, [25μm aluminum
foil, sold to restaurants for wrapping burgers, is only US$16/kg][30],
about the same price per square meter.

Specifically aluminum’s Young’s modulus is about [68GPa][24] or
[70GPa][23], [paper’s about 20GPa][25], and glass’s [a bit over
70GPa][26], [only slightly higher for E-glass][27].  Glass or alumium
is about 2.5× the density of paper, so glass has about a 60% higher
stiffness-to-weight ratio, but since it costs 5× as much, the paper
probably beats it by about 50% on stiffness per cost.  However, glass
fiber can withstand about 2GPa, while paper is only 20–60MPa, 30–100
times lower, so glass is several times cheaper per strength.

Portland-cement concrete is interestingly also in [the same 20GPa
range as paper][28], but where paper costs US$2/kg and US$2/ℓ, [50kg
of gray portland cement is US$9][29] and will make about 150kg of
concrete, giving a cost of about 6¢/kg and 11¢/ℓ.

There are various brands of polyurethane adhesives available here,
including [Sikaflex 221][36] (US$25/300cc), [Kekol Pur KS-518][37]
(US$9.50/500cc), [3M PU550][38] (US$16/310cc), [Loctite Teroson PU
9092 for windshields][39] (US$17/310cc), [Saint-Gobain Tekbond Espuma
Expansiva][40], (US$16/500cc), etc.  Just not “Gorilla Glue”.  An
Argentine Youtuber reports [“Titebond polyurethane multi-surface
glue”][43] to be an expansive polyurethane glue activated by air
humidity (like the others), with “PUR” being the common name; he
demonstrates it foaming up between two clamped wood blocks.

Easily available and moldable polyurethane seems like it could be
useful for things like cellphone protection and laptop feet, so I
might see if I can get some of this stuff to try out.

[12]: https://batik.com.ar/p/polifan-60x125cm-3cm-de-espesor
[43]: https://youtu.be/D_fBoOfW4YM
[13]: https://celulosa.com.ar/polyfan/
[14]: https://www.bichapublicidad.com/portfolio/letras-de-polyfan-y-pvc/
[15]: https://www.polinorte.com/producto/polyfan/
[16]: https://articulo.mercadolibre.com.ar/MLA-651547407-placa-de-polyfan-polifan-20mm-espesor-letras-corporeas-_JM
[17]: https://articulo.mercadolibre.com.ar/MLA-819196095-aislante-termico-de-celulosa-proyectada-magoal-acustico-_JM
[18]: https://articulo.mercadolibre.com.ar/MLA-742834130-media-sombra-negra-clase-c-50-4x1mts-17kg-invernadero-_JM
[19]: https://articulo.mercadolibre.com.ar/MLA-1123194174-bobina-papel-sulfito-copsi-40cm-x-85-kg-_JM
[20]: https://articulo.mercadolibre.com.ar/MLA-737048168-alfombra-cesped-sintetico-10mm-economico-2-x-25-5-m2-_JM
[21]: https://articulo.mercadolibre.com.ar/MLA-615625188-fibra-de-vidrio-en-tela-roving-220-grsm2-multiples-usos-_JM
[22]: https://articulo.mercadolibre.com.ar/MLA-863958985-malla-de-fibra-de-vidrio-10x10mm-110g-1x50m-refuerzo-_JM
[23]: http://web.mit.edu/6.777/www/matprops/aluminum.htm
[24]: https://en.wikipedia.org/wiki/6061_aluminium_alloy
[25]: https://www.researchgate.net/publication/288775811_The_Young's_modulus_of_wet_paper
[26]: https://www.continentaltrade.com.pl/en/our-offer/technical-glass/types-of-materials/soda-lime-glass
[27]: https://www.azom.com/properties.aspx?ArticleID=764
[28]: https://www.engineeringtoolbox.com/concrete-properties-d_1223.html
[29]: https://www.mercadolibre.com.ar/cemento-avellaneda-50-kg/p/MLA24866840
[30]: https://articulo.mercadolibre.com.ar/MLA-605158930-rollo-papel-aluminio-x-1-kg-40-cm-gastronomico-grueso-_JM
[33]: https://articulo.mercadolibre.com.ar/MLA-1214608932-chapa-madera-roble-americano-reco-25m-largo-1ra-calidad-_JM
[36]: https://www.mercadolibre.com.ar/pegamento-sika-sikaflex-221-sellador-multiproposito-poliuretanico-color-blanco/p/MLA27312816
[37]: https://www.mercadolibre.com.ar/pegamento-adhesivo-poliuretanico-kekol-ks-518-500g-color-ambar/p/MLA26787632
[38]: https://www.mercadolibre.com.ar/sellador-adhesivo-poliuretanico-3m-550-cartucho-310ml-color-negro/p/MLA28620550
[39]: https://www.mercadolibre.com.ar/adhesivo-poliuretano-loctite-para-parabrisas-310-ml/p/MLA36156303
[40]: https://www.mercadolibre.com.ar/espuma-expansiva-tekbond-480-g500-ml/p/MLA26443302

Structural designs
------------------

### Loft beds ###

Think about a loft bed.  If you want an unsupported span of 2 meters
to hold, say, 300kg (3kN), well, the maximum moment (in the middle of
the beam) is [Fl/4][31], so you have 1500N·m.  If the beam is a sandwich
beam and is 100mm thick, normally the neutral axis would be 50mm from
the face sheets, so they must resist 30kN.  Let’s assume our sandwich
filling is adequately stiff in shear.  How much paper or glass do we
need to resist 30kN?  Suddenly the strength of the face sheets, rather
than merely their stiffness, comes into play, since we’re resisting a
bending load rather than a buckling load.  NØMAD turned out to be
pretty smart after all.

In short, to bear a weight, you want your vertical members to be
stiff, but your horizontal members to be strong.  So paper is fine for
vertical members, but horizontal members should use fiberglass.

If we assume the paper breaks at 20MPa, we need 1500mm² of paper in
the bottom face sheet; extended for two meters, this is 3 liters
(≈3kg, US$6) of paper.  If the bed-beam is 1.5m wide, it needs a full
millimeter (≈10 sheets thick) of paper in its bottom sheet, and
presumably also in its top sheet.

If instead we have an E-glass fiber mesh with a strength of 2GPa, we
only need 15mm² of cross-sectional area, or 30 cc of glass if the
bed-beam is 2 meters long, weighing about 80g.  In fact, I don’t think
you can buy window screen material that ephemeral; 110g/m² is still
55g/m² in the longitudinal direction, and we have about 3m² here.

[31]: https://www.engineeringtoolbox.com/beam-stress-deflection-d_1312.html

### Modular storage cubes ###

It occurs to me that modular storage cubes of a standard, convenient
dimension would be an especially useful way to use our composite foam
sandwich supermaterial.  Nomadic Furniture profiled one such system
from the 01970s using, I think, particle board, but I forget its
dimensions.  The 20ℓ buckets we’ve been using (430mm high, 300mm
diameter, thus 30ℓ external volume) may be a bit too large; a
20ℓ-interior cube would be 271mm on the inside.  The current boxspring
stands 320mm high at its top, the bedside tables 725mm and 575mm, this
desktop 798mm, this stool 430mm.  It would be nice to be able to
construct a range of such heights, for which purpose it might be
better to have two or more standard heights of cuboid, for example
140mm and 160mm:

- 160mm + 160mm = 320mm, the boxspring
- 140mm + 140mm + 140mm = 420mm, not quite the stool
- 140mm + 140mm + 160mm = 440mm, a bit over the stool
- 4 × 140mm + 160mm = 720mm, one bedside table
- 3 × 140mm + 160mm = 580mm, the other bedside table
- 5 × 160mm = 800mm, the desktop

(Also you might have 320mm double-high cuboids, which doesn’t change
the basic system.)

You have fairly sparse coverage of heights below 420, but at higher
heights you can reach any multiple of 20mm, which is probably
excessive resolution.  However, if you steal 20mm from the top and
20mm from the bottom for the structural foam, over a quarter of your
storage space is taken up by the foam.

For bricky composability and for being able to reach things under the
bed, a 2:1 aspect ratio in the footprint would be desirable.  If we
target 15ℓ of internal storage space for a nominal 150mm average
outside height, 110mm inside height, we get something like 260mm ×
520mm.

My single bed is 780mm wide; Mina’s double bed is 1340mm wide.  These
are 3× and 5× this basic 260mm footprint unit, so you could assemble a
single bed with drawers sliding out the sides and one out the end, and
use almost all the storage space under it.  The three main pieces of
storage furniture in the bedroom are 400mm, 450mm, and 500mm deep on
the outside.

A cuboid of 260mm × 520mm × 160mm would involve 0.52m² of external
surface area.  If you could build it out of 20mm US$1/m² 12kg/m³ EPS
(240g/m²), two layers of US$1.10/m² 110g/m² fiberglass mesh, and
50¢/m² latex paint (weighing, say, 60g/m²), and your labor were free,
it would cost you US$4.20/m² and US$2.20 in all, and it would weigh
580g/m² and thus 300g.  Its internal storage space (ignoring the fact
that there’s no access hole!) would be 210mm × 480mm × 120mm = 12.1ℓ,
a bit on the low side, particularly considering its external volume is
21.6ℓ — only 56% space efficiency.  A single-bed boxspring made out of
such bricks would contain 3×4×2 = 24 of them and would thus cost US$51
in materials and weigh 7kg.

Bigger containers would use less material, be less work to make, and
probably be stronger.  For a given volume per container, deeper and
less tall containers would facilitate accessing more space through a
single access surface (a wall of drawers or cabinets).  Perhaps it
would make sense to make a height of 320mm or 400mm the standard, with
a volume of *more* than 20ℓ, and provide half-height or ⅓-height
cuboids for dense materials.

It’s probably useful to give the containers feet so we have someplace
to put rubber to avoid scratching the floor, and those feet can fit
into indentations in the container below them if there is one.  This
sort of thing might provide better composability if there were a wider
range of angles they could be rotated into and still fit.
Unfortunately, in 2-D, the only options are rectangles, hexagons, and
triangles.

So let’s consider 420mm outside height, 20mm cuboid ceilings, floors,
and walls, a 2:1 footprint, and a target 30ℓ internal volume.  That’s
380mm inside height.  Suppose then our footprint dimensions are
210mm×420mm outside; inside that’s 170mm×380mm, which works out to
24.5ℓ, which is less than 30 but probably okay.  This cuboid has 0.7m²
of surface area, 35% more than the one we considered above, but more
twice the internal volume (66% storage efficiency).  One story of such
cuboids should make an adequate boxspring, especially if anchored
together on their outside walls, and three stories an adequate
workbench.  A whole single boxspring is 2×420mm = 840mm by 9×210mm =
1.89m.

I don’t know, I think you maybe end up needing a wider variety.

Alternative tilings of space like the rhombic dodecahedron with half a
face removed or its dual the cuboctahedron with a triangular face
removed might be stabler as storage cubbyholes.  You might need
truncated versions of them for the tiling to interface with continuous
horizontal surfaces such as butts, mattresses, or floors.  I think
it’s hard to compete with cuboids for storage.

The double layer of foam where two cuboids come together is hard to
swallow.  That’s where you *least* need either strength or stiffness;
where you need it most is around the outside of the aggregate of
cuboids.

A regular plastic stackable storage tub is considerably larger than
20ℓ, and those seem to be fine, so maybe I should go bigger still.
And curve the sides to enable them to support more weight without
buckling, perhaps enabling the use of very thin sides.

### Bike parts ###

I recently restored Mina’s old bicycle from last millennium; it’s a
bare-bones beach cruiser, lacking headlights, reflectors, caliper
brakes (just a coaster brake), chain guard, mud guards, etc.  I’d like
to have saddlebags and a pretty extensive mudguard on it, but like
most bikes made out of gas pipe, it’s already annoyingly heavy, so I’d
like to keep those things light.

I feel like thin EPS would work well as a sandwich-panel filling to
stiffen a mudguard, and probably polystyrene dissolved in a suitable
solvent would work well as an adhesive to attach facesheets to it.  Or
maybe I should use latex paint and window screen, since rain would
weaken sulfite paper.

### Air handling/HVAC ###

Air handling equipment often needs to be fairly large (because the
volumes of air important to humans are often measured in cubic meters
rather than cubic centimeters) but not very strong, and often benefits
greatly from thermal insulance, so even fairly thin foam may be a
useful alternative to conventional galvanized ducting here.
