How to do memory-safe programming?  Like, at the machine code level?
The Java approach was with bytecode and type checking, but Java also
ended up doing runtime type checks on every array indexing operation
until HotSpot.  What's the simplest way we could get reasonable speed
out of array indexing for things like matrix-matrix multiply or
matrix-vector multiply, while ensuring memory safety?

In practice the way this is done in things like FORTRAN is dependent
typing.  Here's the [declaration for DGEMM from netlib][0]:

    subroutine dgemm
 	    ( 	character  	TRANSA,
		    character  	TRANSB,
		    integer  	M,
		    integer  	N,
		    integer  	K,
		    double precision  	ALPHA,
		    double precision, dimension(lda,*)  	A,
		    integer  	LDA,
		    double precision, dimension(ldb,*)  	B,
		    integer  	LDB,
		    double precision  	BETA,
		    double precision, dimension(ldc,*)  	C,
		    integer  	LDC 
	    ) 	
        
[0]: http://www.netlib.org/lapack/explore-html/d1/d54/group__double__blas__level3_gaeda3cbd99c8fb834a60a6412878226e1.html

Omitting the possibility of transposing A or B with TRANSA or TRANSB,
A is supposed to be m×k, B is supposed to be k×n, and the result
matrix C is supposed to be m×n.  The default LAPACK implementation of
the usual case looks like this, abbreviated:

                   DO 90 j = 1,n
                       DO 80 l = 1,k
                           temp = alpha*b(l,j)
                           DO 70 i = 1,m
                               c(i,j) = c(i,j) + temp*a(i,l)
        70                 CONTINUE
        80             CONTINUE
        90         CONTINUE

I think there are implicit multiplications by LDA, LDB, and LDC hidden
in those array indexing operations.  So the resulting machine code for
the innermost loop and the initialization above it does something like
this, though hopefully a little better optimized:

        t0 = j * ldb
        t1 = t1 + l
        t3 = b[t1]
        temp = alpha * t3
        i = 1
    1:  t0 = l * lda
        t1 = t0 + i
        t2 = a[t1]
        t3 = temp * t2
        t0 = j * ldc
        t1 = t0 + i
        t2 = c[t1]
        t3 = t2 + t3
        c[t1] = t3
        i = i + 1
        if i <= m goto 1b
        
The problem we're looking at here is how to verify that t1 is always
within the bounds of b, a, and c respectively when it's used to index
them.  We can verify that, at least if m > 0, i is always in the range
[1, m], and from that we want to be able to verify that, for example,
`l * lda + i` is always within the range of valid indices for `a`,
given the appropriate bounds on `l` from the outer loop.

So your type system needs to be able to represent facts like these:

1. `a` is at least length `m`×`k`, or really `lda` × `k`.
2. `i` is in [1, m].
3. `l` is in [1, k].
4. `m` <= `lda`.
5. So `i` + `l` × `lda` is in [1, `m` × `k`] (or rather the correct
   version of this statement corrected for 1-based indexing).

The inference of `i` is especially interesting because `i` changes
over time, but in such a way that it is always in that range in the
code above.

In a function like this it would be perfectly okay to check some of
these things dynamically as long as you're not doing it in a loop.  In
fact it would probably be fine to do it even in the outermost loop.
But doing it in the inner two loops would be bad.

The assertions above don't need to be extracted from the machine code
necessarily; they can be produced by a compiler and packaged with the
machine code as a proof of its memory-safety.  A proof-carrying-code
loader can then verify the proof with a verifier and load in the code.

Other kinds of types are needed for other kinds of code, notably
structs and discriminated unions; the verifier needs to be able to
recognize the union discrimination field, whether it's in the tag bits
of a pointer or embedded as a tag field in a record in memory.

Lifetimes add another dimension to the verification problem.
