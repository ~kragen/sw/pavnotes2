There are a lot of weird things about the new president.

1. Brandishing an operating chainsaw in crowds of people at rallies.
2. He’s a tantric sex instructor.
3. He had a Rolling Stones cover band.
4. He raffled off his monthly salary as a political representative in
   Congress.
5. His cosplay persona is a superhero called “General AnCap”.
6. He doesn’t believe in global warming.
7. He claims to be a “libertarian” but wants to ban abortion.
8. He has dogs that are all clones of a dead dog.
9. He talks to the cloned dogs about spiritual matters.
10. When asked whether people should be able to change the gender on
    their official identification, he championed not only that policy
    but also the right to be addressed according to your fursona,
    giving as an example a puma (of either gender).
11. Public advocacy of human organ sales for transplants.
12. He was a professional football player before studying economics,
    after which he became the senior economist at HSBC Argentina.
13. Where most politicians wear suits in their campaign appearances,
    he wears a leather jacket and leaves his hair uncombed.
14. He’s opposed to marriage, [publicly advocating free
    love](https://web.archive.org/web/20171006041159/https://www.lanacion.com.ar/2069706-el-amor-segun-el-economista-javier-milei),
    because among other reasons, he says marriage makes men fat.  In
    that interview he justifies non-monogamy on the basis of the law
    of diminishing marginal utility.
15. He’s Catholic; of the Pope, he speaks in the most glowing terms:
    “a Jesuit who promotes communism”, “an unpresentable and
    disastrous character”, “a fucking communist”, a “communist turd”,
    and a “piece of shit”, “preaching communism to the world” and
    being “the representative of the evil one on Earth”.  The Pope
    responded, saying he was “terrified” of Milei.
16. He reads the Torah daily and visited the grave of the Lubavitcher
    Rebbe, but hasn’t converted to Judaism because he thinks observing
    the Jewish Sabbath would interfere with the presidency, which he
    says God has personally told him to assume.
