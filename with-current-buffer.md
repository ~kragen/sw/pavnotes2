I was thinking about machine-language calling conventions.

External-iterator efficiency
----------------------------

In file `iterator-pattern-matching.md` I recently wrote about a
calling convention for external iterators (the kind you call
repeatedly) which privileges repeatedly invoking the same iterator in
a tight loop, since that’s the usual case.  I suggested allocating
three callee-saved registers to the iterator itself, such as r12, r13,
and r14 in the amd64 SysV ABI, and returning each successive iteratee
in a caller-saved register, like r9.  This reduces the overhead of
late-bound iteration code, although on this Ryzen I found that it was
still significant, about 900 picoseconds per iteration, when a simple
4-instruction loop adding up an array of int64s one at a time is only
530 picoseconds per iteration.  (In theory the late-bound version is
running 8 instructions per iteration rather than 4, but it’s
disproportionately slower.)

The idea is to keep the entry point and hot state of an iterator
object in three registers so that the iterator code doesn’t have to
pay the cost of loading the iterator state from memory and then saving
the updated state back to memory.  A downside is that the caller
*does* have to pay the cost of doing so if they want to switch between
iterators, even if the iterator doesn’t need all that state.  Nested
loops, for example, require two such context swaps per iteration of
each non-innermost loop:

    mov [rsp + 24], r12   # Save outer loop iterator state in stack frame.
    mov [rsp + 32], r13
    mov r12, rbp          # Load inner loop iterator state from more
    mov r13, rbx          # callee-saved registers.
    call r14              # Invoke inner-loop iterator (register may vary).

Theoretically this kind of thing ought to be more efficient because
less entropy is created by the transfer of control back and forth;
subroutine argument registers are in effect erased by every call,
while these state registers are not.

Accessing a “current array” in Forth
------------------------------------

Somewhat relatedly, I recently wrote [a tiny roguelike in Forth][0], and
for that purpose I used these stateful array-access words:

    0 value array
    : at@ cells array + @ ;
    : at! cells array + ! ; 
    : iota 0 ?do i dup at! loop ;
    : ints 0 ?do i at@ . loop ;
    -1 value x
    : shuffle  0 ?do  i 1+ rand to x  x at@  i at@  x at!  i at!  loop ;

This was surprisingly helpful in the interactive interpreter because I
was usually doing a sequence of operations on the same array.  For
example:

    create foos 10 cells allot  ok
    foos to array 10 ints 6898498548719845375 -18027886663435067 572023091656390587 9223307990100017278 5301551499530403775 -35186116960420 -7854277973744713785 -504435046795542741 -3792175171645866497 3422447507238518521  ok
    10 iota  ok

See, without the statefulness, instead of `foos to array 10 ints 10
iota` it would be `foos 10 ints  foos 10 iota`.  At that point we’ve
just about broken even, but further interaction continues to be more
convenient:

    10 ints 0 1 2 3 4 5 6 7 8 9  ok
    5 shuffle  ok
    10 ints 3 4 2 1 0 5 6 7 8 9  ok
    10 shuffle 10 ints 3 0 4 1 2 7 9 8 5 6  ok
    17 0 at! 10 ints 17 0 4 1 2 7 9 8 5 6  ok
    7 at@ . 8  ok
    10 iota 10 ints 0 1 2 3 4 5 6 7 8 9  ok
    10 shuffle 10 ints 8 5 6 0 4 2 1 3 9 7  ok
    5 iota 10 ints 0 1 2 3 4 2 1 3 9 7  ok

If I want to switch back and forth between two separate arrays, I have
to be explicit about it, saying `foos to array` and `bars to array`
rather than just `foos` and `bars`, but I don’t have to say it on
every operation, just when I switch.

This is of course more error-prone than explicitly passing the desired
array to every operation, perhaps precisely because it’s shorter;
information that is implicit in the context rather than explicitly
provided is more likely to be wrong.

In an assembly-language environment, you might allocate a register to
that array base pointer rather than storing it at a memory address as
a Forth `value`.  Then instead of reducing finger typing, it would
reduce instructions to execute.

Evidently, this idea of reserving a register for “the current array”
is the same sort of thing as reserving three registers for “the
current iteration”.

[0]: http://canonical.org/~kragen/sw/dev3/wmaze.fs

Emacs Lisp command design
-------------------------

As the [“Mother of All Multics Emacs papers” by Bernie Greenberg][1]
explained:

> The implementation of multiple buffers was viewed as a task of
> multiplexing the extant function of the editor over several buffers.
> The buffer being edited is defined by about two dozen Lisp variables
> of the basic editor, identifying the current Editorline, its current
> (open/closed) state, the first and last Editorlines of the buffer,
> the list of marks, and so forth.  Switching buffers (i.e., switching
> the attention of the editor, as the user sees it) need consist only of
> switching the values of all of these variables.  Neither the interactive
> driver nor the redisplay need be cognizant of the existence of multiple
> buffers; the redisplay will simply find that a different “current
> Editorline” exists if buffers are switched between calls to it.
> What is more, the only functions in the basic editor that have to be
> aware of the existence of multiple buffers are those that deal with
> many buffers, switch them, etc.  All other code simply references the
> buffer state variables, and operates upon the current buffer.
>
> The function in the basic editor which implements the command that
> switches buffers does so by saving up the values of all of the relevant
> Lisp variables, that define the buffer, and placing a saved image
> (a list of their values) as a property of the Lisp symbol whose name
> is the current buffer’s.  The similarly saved list of the target
> buffer’s is retrieved, and the contained values of the buffer state
> variables instated.  A new buffer is created simply by replacing the
> “instatement” step with initialization of the state variables to
> default values for an empty buffer.  Buffer destruction is accomplished
> simply by removing the saved state embedded as a property: all pointers
> to the buffer will vanish thereby, and the MacLisp garbage collector
> will take care of the rest.
>
> The alternate approach to multiple buffers would have been to have the
> buffer state variables referenced indirectly through some pointer which
> is simply replaced to change buffers.  This approach, in spite of not
> being feasible in Lisp, is less desirable than the current approach,
> for it distributes cost at variable reference time, not buffer-switching
> time, and the former is much more common.
>
> One of the most interesting per-buffer state variables is itself a list
> of arbitrary variables placed there by extension code.  Extension code
> can register variables by a call to an appropriate primitive in the
> basic editor.  The values of all such variables registered in a given
> buffer will be saved and restored when that buffer is exited and
> re-entered.  The ability of Lisp to treat a variable as a run-time
> object facilitates this.  Variables can thus be given “per-buffer”
> dynamic scope on demand, allowing extensions to operate in many
> buffers simultaneously using the same code and the same variables,
> in an analogous fashion to the way Multics programs can be executing
> in many processes simultaneously.

This is the same consideration again: to distribute cost at
buffer-switching time rather than variable reference time.  Machine
languages and execution environments have changed since Greenberg
wrote this text in 01979, and now referencing buffer state variables
indirectly through some pointer may actually be cheaper than
referencing them “directly” by embedding absolute memory addresses in
your machine code, both because the memory addresses may not be so
absolute (in the case of a compiler generating position-independent
code like `gcc -fpic`) and because they may not fit into a single
instruction word.  Also, multithreading has become popular again.

But the Emacs Lisp language goes a lot further than that in this
approach to statefulness.  Consider this code from Greenberg’s
Appendix B:

    (defun bracket-word ()
           (forward-word)
           (insert ">")
           (backward-word)
           (insert "<"))

(I’ve replaced Greenberg’s `insert-string` with the `insert` used by
GNU Emacs, but it’s otherwise unchanged from 01979.)

In Multics Emacs I think you could only modify things on the current
line; I think [Mike Sperber is quoting Stallman in this quote][2]:

> ISTR that in Multics Emacs, NO buffer primitives let the user
> specify positions.  It was always and only operating at point. I
> found that too clumsy.  So Emacs Lisp has some primitives that
> operate where an arg says, and some that operate at point.

(This is part of the background work for [Monnier’s paper with him
about Elisp’s history][3] for HOPL ’20.)

But in this case it’s not clumsy.  Because the functions communicate
by moving the current point in the current buffer, they don’t have to
pass positions back and forth explicitly.  You could imagine writing
something like this:

    (defun bracket-word (bufpos)
      (let ((end (forward-word bufpos))
            (start (backward-word bufpos)))
        (insert ">" end)
        (insert "<" start)))

But this does not seem like an improvement, particularly if you
consider inserting the strings in the opposite order, so that the end
of the word moves to the right by one character before you try to
insert the angle bracket there.  A purely stateless approach would
solve that problem, but overall it’s even worse:

    (defun bracket-word (rope pos)
      (let ((end (forward-word rope pos)) (start (backward-word rope pos)))
        (concat (slice rope 0 start)
                "<"
                (slice rope start end)
                ">"
                (slice rope end))))

[1]: https://www.multicians.org/mepap.html
[2]: https://github.com/mikesperber/hopl-4-emacs-lisp/blob/master/notes.org#stallman-feedback
[3]: https://dl.acm.org/doi/pdf/10.1145/3386324 (Evolution of Emacs Lisp, by Stefan Monnier and Michael Sperber)

The current working directory in Unix
-------------------------------------

Of course `cd` in Unix is similarly a stateful user-interface feature
which permits you to use short, context-dependent filenames rather
than long universally unique ones.

Opening and closing things is different but related
---------------------------------------------------

You could imagine a filesystem access API with just two calls:

    err = read(filename, buf, offset, size)
    err = write(filename, buf, offset, size)

Unix instead originally had `open`, `close`, `read`, `write`, and
`creat`.  This is partly in order to support a common byte-stream
interface to files, disks, tapes, terminals, and pipes, but it also
allows expensive operations like searching through filesystem
directories and permission checking to be done at the time of `open`
rather than on every I/O operation.

However, you don’t have just a single “current file” in Unix, or even
a current input and current output; you have an arbitrary number of
open files, and Unix’s `read`, `write`, and `close` calls, among
others, take an argument to tell them explicitly which file to operate
on.  You could imagine redesigning the API so it was more stateful,
looking like this:

    fd, err = open(filename, flags, mode)
    old_fd = input(fd)
    old_fd = output(fd)
    nbytes, err = read(buf, size)
    nbytes, err = write(buf, size)
    close(fd)

This would be equally powerful (barring multithreading) and probably
more convenient to use interactively.  But the efficiency difference
would be minuscule unless the read() and write() operations could be
implemented in only a few instructions.  All it saves you, generally,
is validity-checking on the fd argument and possibly an indirection to
access the buffers.

In an implementation for very limited memories, you could imagine
using the `input()` and `output()` calls as hints to reallocate scarce
buffer memory from the old fd to the new fd.

This is generally applicable to opening and closing things that are
manipulated through some kind of descriptor, handle, or moniker.

Allocating callee-saved registers to concrete classes
-----------------------------------------------------

Fashionable ISAs like ARM64, RISC-V, and the IBM OpenPOWER ISA, as
well as many unfashionable ones like MIPS, have 31 or 32
general-purpose registers, which leaves open the problem of what to
use so many registers for.  If you have very large subroutines, you
can maybe allocate them to many local variables, and if you’re
emulating another CPU, you can use them to implement the emulated
CPU’s registers, or the most important ones at least.  You can try to
make your subroutines larger by inlining and unrolling loops, but it’s
unclear how far that can really take you.

Let’s generalize my iterator strategy above.

Suppose we decide to make most of these registers callee-saved, and
for each concrete class in the system, we choose 1–4 instance
variables to allocate registers to, one of them being the object’s
self pointer.  When a caller A wants to invoke some method M on an
object B whose concrete class it knows, it must ensure that, upon
entry to M, those 1–4 instance variables are in the right registers,
and after exit from M, the variables eventually get moved back into
memory.  In order to load those registers, A may need to save their
previous values — either into another object whose instance variables
values A had previously put in them, or into A’s stack frame so it can
restore them before returning to its own caller.  Immutable instance
variables such as the self pointer don’t need to be saved back to
memory.

The hope is that many times we’ll be invoking methods on the same
object many times, as is common with iterators and editor buffers, and
that many of those methods are mostly accessing the same few hot
instance variables.  It reduces the runtime overhead of cooperation
between objects by, in effect, making the method call and return
operations cheaper.

A risky aspect of this, especially with respect to mutable instance
variables, is aliasing.  If M calls some other method M' of the same
object, there’s no problem; both methods expect the same instance
variables to live in the same registers.  But, if M calls a method M''
on an object C of some other class, and M'' calls M on some object D,
B’s instance variables have not been stored back into B’s in-memory
representation when M'' needs to reuse those registers for D’s
instance variables.  So M'' stores them temporarily in its stack frame
so it can restore them upon return.  But, if D happens to be just
another reference to B, this inner call to M will see outdated values
for those variables.

I don’t know how to resolve this, exactly, although Rust has some
ideas.
