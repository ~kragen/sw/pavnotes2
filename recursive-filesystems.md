I was rereading
[Chispa](https://github.com/kragen/500lines/tree/master/search-engine)
and it occurred to me that in some sense its LSM-tree implementation
is recursive: it’s using the filesystem as a key-value store that
supports iteration over the contents of a subdirectory, and it
provides a key-value store that supports iteration over key ranges.
By storing each independently-readable chunk of a segment in a
separate file, it avoids the need to seek in a file or read part of
it.

Suppose you have a very simple key-value store implemented on top of
raw block storage, maybe one that doesn’t have very good scaling
properties.  Maybe you could use something like Chispa’s LSM-tree
implementation on top of it to get better scaling properties.

This appeals to my polymorphism and recursion tastes, but I’m not sure
whether it’s actually a good design.  It seems appealing for
bootstrapping purposes: you can start with a very minimal “filesystem”
that provides minimal functionality, then layer more functionality on
top of it.

Suppose we assume as our target a device with 256 MiB to 8 TiB of
Flash and 1 MiB to 1024 MiB of RAM.

The Flash might have 1 μs random 4096-byte read time, 3 ms erase time
to erase a block; on the 1 Gibit = 131072 Kibyte NAND chips I was
looking at, each erase block was 64 2048-byte pages, so the chip was
1024 erase blocks.  But that only gives a maximum erase bandwidth of
about 40 megabytes per second, and existing SSDs can write new data
much faster than that; and I calculated the time to *write* that block
of data as being another 28 ms.  (It can write a page at a time.)  I’m
told that it’s common for write time to be 10× page read time, and
block erase time to be 10× page write time, with block erase time
typically in the 128 KiB–2MiB range, so for 8 TiB we have between 4
mebiblocks and 64 mebiblocks for erase purposes.

Suppose we statically divide the Flash into hooshings of 8 MiB.  It
might contain up to 1'048'576 of them, so we can’t keep a catalog of
all the hooshings in 1 MiB of RAM.  A hooshing number is 20 bits, so a
name + number might be 16 or 32 bytes, so we need up to 16 or 32 MiB
for a list of hooshings, and we can fit 256 or 512 name+number entries
into a 4096-byte Flash block.  Alternatively we could store up to
131072 name+number pairs in RAM, or 16384 to be practical by leaving
space for an application, which would make the hooshings 512 MiB if
the Flash is 8 TiB, and 16 MiB if the Flash is only 256 MiB.

But even 16 MiB won’t fit in 1 MiB of RAM.  You could keep a sorted
catalog of hooshings in the first 4–8 hooshings of your disk, but then
you have to handle merging updates into the hooshing list, which sort
of pushes LSM-trees into the base level.

I think that this shows that any such ultra-simple recursive
bootstrapping filesystem implementation can’t get away with just
[Get() and Next()][0]; it really does need to provide access to
*parts* of a “file”, like Unix’s read(), write(), and lseek(), or like
mmap() or Forth’s BLOCK, or like pread() and pwrite().  A couple of
extra wrinkles is that on Flash you have to erase a block before you
can write to it (though then you can write pages anywhere in it), and
that 8 TiB is too large to memory-map with a 32-bit memory address.

[0]: https://github.com/google/leveldb/blob/main/doc/index.md

Another thing is that at least some minimal degree of transactionality
has to be plumbed all the way to the bottom layer in order to not
corrupt whatever higher-level filesystem you’ve built when there’s a
power failure.  The bottom layer also probably has to deal with some
degree of fault tolerance, since NAND chips have about 2% bad bits,
and an interrupted write can result in an unreliably readable block.

Still, the simplicity of the LSM-tree implementation on top of
filesystem files is appealing, 85 lines of Python (refactored,
untested):

    import gzip
    import heapq                    # for heapq.merge
    import itertools
    import os
    import shutil                   # to remove directory trees
    import urllib                   # for quote and unquote

    class Path:                     # like java.lang.File
        def __init__(self, name):
            self.name = name
        __getitem__  = lambda self, child: Path(os.path.join(self.name, str(child)))
        __contains__ = lambda self, child: os.path.exists(self[child].name)

        __iter__     = lambda self: (self[child] for child in os.listdir(self.name))

        open         = lambda self, *args:          open(self.name, *args)
        open_gzipped = lambda self, *args: gzip.GzipFile(self.name, *args)

        basename     = lambda self:     os.path.basename(self.name)

    def write_tuples(outfile, tuples):
        for item in tuples:
            line = ' '.join(urllib.quote(str(field)) for field in item)
            outfile.write(line + "\n")

    def read_tuples(infile):
        for line in infile:
            yield tuple(urllib.unquote(field) for field in line.split())

    def term_doc_ids(index_path, term):
        doc_id_sets = (segment_term_doc_ids(segment, term)
                       for segment in index_segments(index_path))
        return itertools.chain(*doc_id_sets)

    def index_segments(index_path):
        return [path for path in index_path if path.basename().startswith('seg_')]

    def segment_term_doc_ids(segment, needle_term):
        for chunk_name in segment_term_chunks(segment, needle_term):
            for haystack_term, doc_id in chunk_tuples(segment[chunk_name]):
                if haystack_term == needle_term:
                    yield doc_id
                # Once we reach an alphabetically later term, we're done:
                if haystack_term > needle_term:
                    break

    def segment_term_chunks(segment, term):
        previous_chunk = None
        for headword, chunk in skip_file_entries(segment):
            if headword >= term:
                if previous_chunk is not None:
                    yield previous_chunk
            if headword > term:
                break

            previous_chunk = chunk
        else:                   # executed if we don't break
            if previous_chunk is not None:
                yield previous_chunk

    def skip_file_entries(segment_path):
        with segment_path['skip'].open() as skip_file:
            return list(read_tuples(skip_file))

    # From nikipore on Stack Overflow <http://stackoverflow.com/a/19264525>
    def blocked(seq, block_size):
        seq = iter(seq)
        while True:
            # XXX for some reason using list(), and then later sorting in
            # place, makes the whole program run twice as slow and doesn't
            # reduce its memory usage.  No idea why.
            block = tuple(itertools.islice(seq, block_size))
            if block:
                yield block
            else:
                raise StopIteration

    chunk_size = 4096
    def write_new_segment(path, postings):
        os.mkdir(path.name)
        chunks = blocked(postings, chunk_size)
        skip_file_contents = (write_chunk(path, '%s.gz' % ii, chunk)
                              for ii, chunk in enumerate(chunks))
        with path['skip'].open('w') as skip_file:
            write_tuples(skip_file, itertools.chain(*skip_file_contents))

    # Yields one skip file entry, or, in the edge case of an empty chunk,
    # zero skip file entries.
    def write_chunk(path, filename, chunk):
        with path[filename].open_gzipped('w') as chunk_file:
            write_tuples(chunk_file, chunk)
        if chunk:
            yield chunk[0][0], filename

    def merge_segments(path, segments):
        if len(segments) == 1:
            return

        postings = heapq.merge(*(read_segment(segment)
                                 for segment in segments))
        write_new_segment(path['seg_merged'], postings)

        for segment in segments:
            shutil.rmtree(segment.name)

    def read_segment(path):
        return (t for _, chunk in skip_file_entries(path)
                for t in chunk_tuples(path[chunk]))

    def chunk_tuples(path):
        with path.open_gzipped() as chunk_file:
            for item in read_tuples(chunk_file):
                yield item

This is sort of “vertical recursion”: we’ve thrown a key-value store
layer over the whole disk, and another key-value store layer on top of
that, and maybe another one on top of that, etc.  A different approach
we could call “horizontal recursion” is how Colossus and BigTable work
at Google: BigTable stores its data in Colossus, and Colossus stores
its metadata in BigTable or something similar.  So most of the data on
the disk (including all user data) is just managed directly by
Colossus, while a small fraction is delegated to some other database
system, which maybe is using another, smaller Colossus elsewhere.
Ultimately this bottoms out in Chubby, which is extremely reliable but
also extremely expensive.

At the base layer, of course, the disk does not provide a key-value
store; it provides fetching and storing by page number, and perhaps
block erase.  Such a block store is easy enough to provide, for
example with a block file, so if a key-value store or filesystem or
database is going to be implemented on top of a key-value store or
filesystem or database, it’s probably okay for it to *also* require
such a block store.

A different angle on the question is what it would look like to
organize the operating system’s storage like Kafka, or most of it:
just a circular buffer of recently published events, or rather event
batches, which can be read randomly but only written to by appending.
This is low-level enough that you can implement it with a pretty
minimal amount of code on top of raw commands for read, write, and
erase.

You can selectively republish events that are about to expire in order
to preserve them, but a strict FIFO ordering on expiring events is
suboptimal in this sense, because events that have survived for a long
time are likely to survive for another long time.  Generational
garbage collection is a plausible and very simple solution: partition
the storage device into multiple feeds, and republish events that are
about to expire onto the next slower feed.  But on Flash this will
require some kind of rotation solution that ensures wear leveling; a
static partitioning between generations would wear it out prematurely.
But you could reasonably statically partition the Flash into, say,
16–64 pieces that are moved between feeds over time.

Of course it’s easy enough to partition a single event stream into
multiple event streams with an intermediating layer.