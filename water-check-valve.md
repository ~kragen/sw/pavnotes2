I was thinking about how to drive multiple airlift water pumps from a
single aquarium aerator pump, and I thought you'd probably want to use
two bubblers for redundancy.  But then you might need external check
valves, and that would add pressure drop, cost, and unreliability.  (I
think they have internal check valves, and they're unlikely to both
fail at once, so this might be unnecessary.)

They're pretty low-pressure, though, so the check valve doesn't have
to withstand much pressure, as well as low flow (3.5 liters per
minute, 60 ml/s).  It occurred to me that you could build a working
check valve for this kind of setup with water as the only moving part,
simply as a manometer or bubbler.

Suppose you need to withstand 10 kPa of backpressure, 1 meter of
water.  A 1 cm² vertical tube one meter long is sufficient for this if
it's full of water, at which point it contains 100 cc of water.  If
the tube is empty and its lower end is held 10 mm above a
113-mm-diameter, 30-mm-deep Petri dish, which is filled with water to
a depth of 20 mm, then when airflow goes down the tube, it will bubble
through the water, dissipating 10 mm water of head, 100 Pa.  When the
pressure goes the other way, the water level in the Petri dish will go
down, forcing water up the tube to a height proportional to the
backpressure; no air will be able to enter until the Petri-dish level
has dropped to the level of the tube, at which point the water will be
1 meter up the tube.

Two or more such tubes can share the same dish with no water level
reduction if the objective is to keep airflow from one of them from
forcing air into the other.

If a deliquescent material like muriate of lime is dissolved in the
bubbler water, as long as the humidity of the air passing through is
within the range where the material deliquesces (33% and up for
muriate of lime), the bubbler will not dry up over time, though its
liquid volume and consequently its maximum backpressure may increase
or decrease to reach equilibrium with the air's humidity.
