I was looking at Andy Sloane’s [Donut math: how donut.c works][a1k0n],
which has an animation of a shaded point cloud, which I thought was
super cool.  He explains this C code, which renders a rotating torus
as ASCII art:

                 k;double sin()
             ,cos();main(){float A=
           0,B=0,i,j,z[1760];char b[
         1760];printf("\x1b[2J");for(;;
      ){memset(b,32,1760);memset(z,0,7040)
      ;for(j=0;6.28>j;j+=0.07)for(i=0;6.28
     >i;i+=0.02){float c=sin(i),d=cos(j),e=
     sin(A),f=sin(j),g=cos(A),h=d+2,D=1/(c*
     h*e+f*g+5),l=cos      (i),m=cos(B),n=s\
    in(B),t=c*h*g-f*        e;int x=40+30*D*
    (l*h*m-t*n),y=            12+15*D*(l*h*n
    +t*m),o=x+80*y,          N=8*((f*e-c*d*g
     )*m-c*d*e-f*g-l        *d*n);if(22>y&&
     y>0&&x>0&&80>x&&D>z[o]){z[o]=D;;;b[o]=
     ".,-~:;=!*#$@"[N>0?N:0];}}/*#****!!-*/
      printf("\x1b[H");for(k=0;1761>k;k++)
       putchar(k%80?b[k]:10);A+=0.04;B+=
         0.02;}}/*****####*******!!=;:~
           ~::==!!!**********!!!==::-
             .,~~;;;========;;;:~-.
                 ..,--------,*/

As part of the explanation, there’s a JS rotating
torus rendered as a point cloud on a black background, with Lambertian
illumination on each point of the point cloud.  Because of the way the
point cloud is generated, the points form a grid on the surface of the
torus, and so the lines of the grid follow the curvature of the
surface.  Even though the points aren’t joined together, the lines of
the grid are visually apparent, which helps visually outline the
curvature, [as is commonly done in engravings][hatching].  But because
each point is shaded with Lambertian illumination, lighting also
provides visual cues to depth.

[a1k0n]: https://www.a1k0n.net/2011/07/20/donut-math.html
[hatching]: https://en.wikipedia.org/wiki/Hatching#Variations

This also reminded me of the animations of wavegrower, Frédéric
Vayssouze-Faure, such as his recent [Moon Dust][dust].  He generally
renders them in Proce55ing and usually uses only black and white
(bilevel, not grayscale, but antialiased).  In Moon Dust, he shades
the surface of a sphere with a nonuniform random distribution of
ellipses, each ellipse apparently being the viewing-plane projection
of a circle on the sphere’s surface, so the foreshortening of the
circle provides a visual cue to the three-dimensional orientation of
the surface.  Wavegrower frequently uses point clouds to display
three-dimensional forms ([traffic][traffic], [beams][beams] (in which
they are on a lattice), [breather][breather], [deep trip][trip]),
random foreshortened circles to display surface orientation
([chromatic rain][rain], [glass ceiling][ceiling]), or both together
([boule de bulles de neige][neige], [“S” tunnel][tunnel],
[opener][opener], [weightlessness][weightlessness], [into the
wave][into], [space party][party]).  Often he uses white points on a
black background.

[dust]: https://wavegrower.tumblr.com/post/173228945710/moon-dust
[traffic]: https://wavegrower.tumblr.com/post/186587065860/traffic-two-way-version
[beams]: https://wavegrower.tumblr.com/post/175408307630/beams
[rain]: https://wavegrower.tumblr.com/post/187263994235/chromatic-rain
[neige]: https://wavegrower.tumblr.com/post/187842530200/bulles-de-neige
[tunnel]: https://wavegrower.tumblr.com/post/188024942880/s-tunnel
[opener]: https://wavegrower.tumblr.com/post/660962713272532992/opener
[weightlessness]: https://wavegrower.tumblr.com/post/661416713744744448/weightlessness
[into]: https://wavegrower.tumblr.com/post/171766525070/into-the-wave
[party]: https://wavegrower.tumblr.com/post/169779191005/mirror-ball-planet
[ceiling]: https://wavegrower.tumblr.com/post/165937177995/glass-ceiling
[breather]: https://wavegrower.tumblr.com/post/163332890930/breather
[trip]: https://wavegrower.tumblr.com/post/168860531430/deep-trip

In one case, [Impacts][impacts], he places the ellipses on a grid and
varies their size in order to to shade the surface; in another, [the
hidden wave][wave], he does the same and additionally makes them
discs, in the sense of a short cylinder, incidentally using black on
white in that case rather than white on black.  In [worries bin][bin],
the point cloud of foreshortened circles on a grid outlines a curved
surface, but they do not vary in size.  In [flows][flows]
([math][flowsmath]), the white circles form a grid on the surface, are
foreshortened to show surface orientation, and additionally stripes
are cut out of the surface, but their size and color is not varied.

[impacts]: https://wavegrower.tumblr.com/post/652725981143007232/impacts
[wave]: https://wavegrower.tumblr.com/post/664320915025379328/the-hidden-wave
[bin]: https://wavegrower.tumblr.com/post/158026930270/worries-bin
[flows]: https://wavegrower.tumblr.com/post/165437799155
[flowsmath]: https://www.geogebra.org/classic/zr34NtJr

I’ve been tossing around ideas for a batteryless portable computer
called the Zorzpad, and one of my design choices is to use the same
memory-in-pixel LCDs used in the Playdate console because they use
twenty times less power than a traditional LCD while having
dramatically better contrast, especially in sunlight.  Also, it
supports frameless rendering, so partial display updates can be
incredibly fast.  But the display is purely bilevel: black or white
with no shades of gray.  And that’s limiting.

So one of the things I’ve been thinking about is how to display 3-D
graphics on these screens.  The same Andy Sloane has [written a
real-time 3-D renderer for the Utah Teapot][arduboy] on the 128×64
bilevel OLED screen used on the Arduboy, but it sacrifices an enormous
amount of the already limited screen resolution to the 4×4 ordered
dithering he’s using to shade the faces.

[arduboy]: https://www.a1k0n.net/2017/02/01/arduboy-teapot.html

The Zorzpad has much less stringent limits on CPU and screen
resolution, so the same approach there would not reduce clarity as
much, but it would still be nice to exploit its limited 2×400×240
screen resolution to the maximum.  In particular, it seems like the
non-photorealistic rendering approaches demonstrated by Sloane’s torus
and by Vayssouze-Faure could be straightforwardly applied.  And it
looks different from traditional rendering techniques, both pencil-and-paper and
computer.

The straightforward thing to do is, given a parametric equation for a
surface patch, sample it periodically in u and v, and plot an ellipse
with [Bresenham][bresenham] or DDA on the display at the projected
sample, foreshortened according to the surface normal (or
backface-removed if it points the wrong way).  The surface normal can
be derived by forward-mode automatic differentiation of the parametric
equation.  The size of the ellipse can be determined by the lighting
of the surface and by the ellipse spacing; automatic differentiation
of the parametric equation also gives us an estimate of the ellipse
spacing.

[bresenham]: http://members.chello.at/~easyfilter/bresenham.html

For many surface parametrics, a series of samples evenly spaced in u
with a fixed v value, or evenly spaced in v with a fixed u value, will
give us lines which follow the surface contours, but they may not be
at right angles on the surface.  However, if we take a sample at some
point (say the center of the surface patch) we can use autodiff again
to find the vectors of travel along u and v in 3-D space; if we
project one onto the other, we get the non-orthogonality error, which
we can then subtract from it to get a vector in u-v space which gives
us an orthogonal direction of travel at that point.  If the surface is
curved, it might not remain orthogonal as we follow the surface, but
in most cases it’s probably close enough.

I think that probably in most cases the surface contours will show
more clearly if we sample the surface regularly in such a way, and
perhaps more densely along one dimension than the other, rather than
randomly as Vayssouze-Faure often does.

Foreground/background relationships can be shown more clearly if we
outline object edges.  Sometimes these are boundaries of parametric
patches where they come together at a razor edge, but other times they
are just local extrema of the projected curved surface.  Tracing that
extremum around the screen should be pretty simple and efficient:
you’re interested in the place where the surface normal is
perpendicular to the line of sight, which (in an axonometric
projection) is when its Z-coordinate is 0.  So you can use autodiff a
second time on the surface normal to find the u-v gradient of the
Z-coordinate (or dot product with the line of sight).  Newton’s method
finds the zero, which is the edge, and the perpendiculars to that
gradient are the directions in u-v space that follow the edge.

If drawing white on a black background, it’s probably best to draw the
object edge in white, with an additional line in black next to it,
outside the surface, to keep background features from contacting the
object edge and visually interfering with it.

In the axonometric-projection case, the foreshortening of the circle
is determined by the surface normal projected onto the screen; when
its projection is null, the projection is a circle with relative size
1.  In all other cases, the major axis of the ellipse has relative
size 1 and is perpendicular to the projection of the normal; the minor
axis is parallel to the projected normal *n⃗* and has a relative size
of √(1 - |*n⃗*|²).
