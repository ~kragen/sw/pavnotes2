Scanning ECM (which is not a kind of ECM machining!) is a particularly
important kind of microscopy for a variety of reasons:

- It can electrodeposit.  Selectively.
- It can electro-etch, selectively.
- As a microscope, it gives you a 3-D model with submicron resolution.
- It works in water, not hard vacuum.  It’s water full of toxic salts
  in dc-SECM, but still, water, and [ac-SECM can even use tap water][7].

[7]: https://youtu.be/Dwu9OB_WsoM

The now-discontinued [BioLogic SECM150][0] [from 02020][1] might serve
as a rough representative of the commercial state of the art: 10-nm
positioning precision in all three dimensions (with movement of
200μm/s), 100pA/V current sensing with 0.5% error (with typical
currents ranging up to 100 nanoamps), 20 data points per second, and
the price was “request quote”, which I interpret as, “if you have to
ask, you can’t afford it.”

[0]: https://youtu.be/u52OVnm6YKE
[1]: http://web.archive.org/web/20230000000000*/https://www.biologic.net/products/secm150/

I feel like it might be hard to match those specs in a DIY hobby
project, but approaching them should be straightforward.  EEVblog Dave
Jones’s [02009 μCurrent analog current sensor][2] is designed for 0.2%
error, DIYable (though sold as a finished product) and produces
0–200mV output with selectable ranges of 1mV/mA, 1mV/μA, and 1mV/nA.
0.5% error at 100pA would be 0.5pA of error, which would be an error
of 0.0005 nA on that last scale.  So that would be probably a few
orders of magnitude worse, possibly bad enough to put SECM out of
reach entirely until you have a better circuit.  He was aiming for
hooking the μCurrent’s output to the 200mV input of a 2000-count
digital multimeter, which has a peak quantization error of 0.05 mV and
thus in this case 0.05 nA, 100× larger than BioLogic’s product, and
that’s before other sources of error are considered.

[2]: https://youtu.be/g7b5YZENvjY

But the μCurrent’s 1mV/nA input uses a 10kΩ current shunt resistor (to
non-mains-referenced chassis ground, on the input of a MAX4239
don’t-call-it-a-chopper opamp configured as a gain-100 single-ended
amp), and using a precision 1MΩ or 10MΩ resistor instead would be an
easy change, though one that increases Johnson noise.  The opamp
chosen has a specified maximum offset input voltage of 2.5μV, which is
another potentially larger source of error in the system, translating
to 0.25mV on the output, 5× the quantization error given above.  But
it claims the *typical* offset voltage is only 0.1μV.

The [MAX4239 opamp dadashit][3] mentions an input bias current of 1pA
and an input offset current of 2pA, which would probably introduce
more than 0.5pA of error, though conceivably you could calibrate that
out (they say it's the gate leakage of the input MOSFETs).  Still,
it’s in the ballpark of BioLogic’s product.  [Jones later suggested
that the almost-pinout-compatible OPA189 would be a better op-amp to
use][4], but requires a 5-volt supply and draws more than twice as
much power, 2.68mA instead of 1.31mA in his test, and doesn't deliver
the hoped-for reduced offset voltage (73μV on the output, dropping to
52μV, rather than 61μV in the test, corresponding to 730pV, 520pV, and
610pV respectively on the input; these are dramatically better than
the MAX4239’s 2500pV worst-case datasheet figure but dramatically
worse than the 100pV “typical” datasheet figure).

[3]: https://www.analog.com/media/en/technical-documentation/data-sheets/max4238-max4239.pdf
[4]: https://youtu.be/UVjLoJMVL0s "EEVblog 1328"

In his testing, though, he measured the original μCurrent’s output
noise at around 60nV/√Hz (and then, for some reason, 3.5μV/√Hz, except
near its 10–15kHz chopping frequency).  With the OPA189 it was closer
to 1μV/√Hz.  This sort of measurement implicitly points out that, in
the SECM application, where you can turn the current on and off and
where you can bandlimit the signal of interest by moving the probe
slowly (in the SECM150’s case, bandlimit it all the way to 10Hz!) you
could probably use [synchronous detection][6] or [lock-in
amplification][5] to improve on the op-amp’s raw noise figure by a few
orders of magnitude by chopping the signal to the SECM probe.  And
you’d probably have to, to compete with the SECM150.  [BioLogic’s
SECM470 does use lock-in amplification][a], and probably so does their
SECM150.

[5]: https://en.wikipedia.org/wiki/Lock-in_amplifier
[6]: http://www.edscave.com/synchronous-detection-part-2.html
[a]: https://youtu.be/Dwu9OB_WsoM?t=22m45s

If you can find a way to do the desired SECM current sensing, you
still need a way to move the probe around.  For lightweight probes and
small ranges of motion, piezo positioning seems eminently accessible,
(as used by the SECM470 for its Z-axis vibration for
intermittent-contact mode (ic-SECM),) but voice-coil positioners as
used in hard disks are another option.  In fact, literal hard disk
voice coils may be a reasonable option for DIY purposes.
