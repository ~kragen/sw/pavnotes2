I got a 20-liter bucket with a sealing top, the kind that paint comes
in, for about US$4.  It’s about 305 mm in diameter and 420 mm tall.
The mattress is 1330 mm × 1900 mm.  This is about 4.4 buckets by about
6.2 buckets, so a 4×6 array of buckets ought to support the bed just
fine with a little masonite on top.  This should cost about US$96 and
hold 480 liters of food or other materials.

Suppose rice costs US$0.40/kg (AR$117/kg at Día Online on 02022-10-23
at AR$289/US$; see `food-prices-02022.md`).  Rice is pretty close to
1 kg/ℓ, so 480 liters of it would be about US$190 and 480 kg.  It’s
about 80% carbohydrates (the rest being mostly protein and water) and
about 3.6 kcal/g.  So a day’s worth of rice at 2500 kcal/day is about
690 g, dry, and 480 kg would be 700 person-days, almost two years.

480 kg is a reasonable amount of weight to put on the floor.

What if we use smaller buckets?  10-liter buckets also come with
hermetically sealing tops and have the major advantage that they are
much easier for Mina to lift when full.  Supposedly these are 250 mm
in diameter and 280 mm tall.  1330 mm would be 5.3 buckets, and 1900
mm would be 7.6, so we're talking about 5×7 buckets rather than 4×6,
so 35 instead of 24.  [The badly edited listing I'm looking at offers
them for AR$1390][d] each, which at today's quotation of
AR$464–469/US$ would be US$3, so 35 buckets would work out to US$105,
pretty much exactly the same.  But it would have only 350ℓ of storage
space instead of 480ℓ, and the bed would be closer to the floor.

[d]: https://articulo.mercadolibre.com.ar/MLA-1124100340-balde-10-litros-negro-manija-tapa-hidroponia-compostera-x-10-_JM

(In both cases there are other vendors who offer similar buckets at a
lower price, and they might be adequate.)

Rodent-proofing
---------------

These polypropylene buckets are probably insect-proof but probably not
mouseproof.  A little hardware cloth or expanded sheet metal
(“galvanized lath”) might solve that problem; [reputedly 6-mm-mesh
hardware cloth is mouseproof][0] and [12-mm-mesh is not][1], but
expanded sheet metal is cheaper.  So far I haven’t had mouse or rat
trouble in Argentina, though, just moths, weevils, red flour beetles,
cockroaches, and ants.

[0]: https://groups.google.com/g/alt.home.repair/c/KV-GCUBazG8 "Caulki...@work.com on 02013-10-16, ‘Can mice get thru 1/4” hardware cloth?’"
[1]: https://www.beesource.com/threads/what-size-hardware-cloth-to-keep-mice-out.303633/ "Beesource Bee Forum, LampBurner on 02014-09-14, ‘What size hardware cloth to keep mice out?’"

Infusorial earth
----------------

I picked up some cosmetic-grade infusorial earth at the dirt store the
other day (US$1.09 for 1 kg; see file `material-sourcing.md`) which
might be a good thing to mix into stored dry food to kill any insects
that get in.  This seems more promising for things like beans and rice
than things like polenta and flour: although in theory it’s harmless
to ingest, it’s disagreeably gritty between the teeth, it definitely
isn’t nutritive, and it might be contaminated.  Rinsing it off before
cooking will reduce that risk.

The grittiness [might indicate that this is the wrong kind of
infusorial earth][9] for food storage, for which “one cup” per “five
gallon bucket” of grains is recommended: 200 mℓ per 20 ℓ, about 1% by
volume.

[9]: https://www.usaemergencysupply.com/information-center/self-reliance/food-storage-frequently-asked-questions/diatomaceous-earth "misc.survivalism FAQ by Alan T. Hagan, question G.1"

Oxygen scavengers
-----------------

An alternative approach is keeping the buckets free of oxygen with an
[“oxygen scavenger”][a] or “oxygen absorber”; the modern ones are
mostly the same thing as chemical hand warmers: iron or ferrous
carbonate powder, and sodium chloride powder as a catalyst.  WP says
1 g of iron will eventually absorb 300 cc of oxygen; if a 20-ℓ bucket
is 80% full of rice or beans or whatever, that leaves 4 ℓ of air and
840 cc of oxygen, thus requiring 2.8 g of iron, though it would be
desirable to include several times that much in order to handle
possible multiple openings of the bucket.

[a]: https://en.wikipedia.org/wiki/Oxygen_scavenger

Doing the math, Fe₂O₃ should be 159.688 g/mol, so two moles of iron
(111.69 g) oxidizing to it would absorb a mole and a half of O₂, which
should be about 24 liters per mole and thus 36 liters, which works out
to 322 cc of oxygen per gram of iron.  So that really is about 300 cc
of oxygen, not of air.  If this is the right calculation, it improves
our estimate to 2.6 g of iron per bucket.  If the end state is Fe₃O₄
then instad of 322 cc it would be 286 cc.  Probably better to use vast
overkill.

In hand warmers this process takes about an hour to run to completion,
and [apparently oxygen scavenger packets share this property][b],
becoming warm to the touch after opening and running to completion
typically in about 4 hours in a sealed container, though presumably
less sodium chloride or larger iron filings could slow this down to a
less dangerous speed.

[b]: https://theprovidentprepper.org/how-to-safely-use-oxygen-absorbers-to-extend-the-shelf-life-of-long-term-food-storage/

[Oxygen absorber packets are locally available in packs of 25 of 1.5
grams each][c] for AR$2950 (US$10.20), possibly of brand Sigev
Solutions, shiyuechu.taobao.com, or o2zero.com.  They’re advertised as
“absorbedor de oxígeno” and sold by a silica-gel vendor.

[c]: https://articulo.mercadolibre.com.ar/MLA-1125947679-absorbedor-oxigeno-unico-grado-alimenticio-x-25-u-_JM

In addition to preventing insect infestation, an oxygen scavenger
would also prevent rancidification.

Sulfites are commonly used as oxygen scavengers in food; making sodium
sulfite (E221) should be a trivial matter of bubbling SO₂ through
water with dissolved sodium carbonate, and calcium sulfite (E226,
hannebachite) even easier, by bubbling it through limewater.  CaSO₃’s
solubility is 4.3mg/100mℓ, CaCO₃’s 1.3mg/100mℓ, and Ca(OH)₂’s
170mg/100mℓ and retrograde, so most of the sulfite product will
precipitate, especially at low temperatures.  I don’t know how fast
the sulfite reaction with air is, but evidently it’s fast enough to be
used industrially as a source of gypsum, though [sulfur-oxidizing
bacteria accelerate it][b] to 5% per day, so it must take months
normally.  Weeks or months seems like a better timescale for this kind
of thing than minutes or hours.

[b]: https://pubmed.ncbi.nlm.nih.gov/28692835/ "Biogeochemical oxidation of calcium sulfite hemihydrate to gypsum in flue gas desulfurization byproduct using sulfur-oxidizing bacteria, Graves et al., 02017, 10.1016/j.jenvman.2017.06.013"

Experimental results
-------------------------

We have bought six such 20ℓ buckets, the last five for $2100 per
bucket (US$4.50).  They are polypropylene, made by
[SENASA](https://www.florek.com.ar/) One weighs 763g plus a 200g lid.
Another weighs 763g plus a 198g lid.  This suggests that their
manufacture is tightly controlled, to within about 1%, and all 24 of
them together should weigh some 23.1kg.  They have a bit of a smell
inside like fumes from soldering. I (119kg) was able to jump up and
down on one of the buckets with the top on with no detectable damage
or flexion except in the top.  The tops snap on with two levels of
sealing; the second level is difficult and uncomfortable to open by
hand, but probably something like a screwdriver would make it easy.

One of them seems to make a pretty reasonable stool; a stack of two
makes an adequate if slightly low desk.

Then we bought more!  The mattress does fit pretty well on 18 of them
(spread out a bit), but Mina wanted a traditional boxspring instead to
see if it would help her back pain.

msp430
autoincrement
pc-relative
pipelines
streams from memory
queues
crossbars
