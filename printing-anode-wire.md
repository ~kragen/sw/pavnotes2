Suppose you extrude a hair-thin wire from a positively charged moving
nozzle onto a sticky, negatively charged workpiece which is soaked
with an electrolyte; perhaps the wire is carried along by viscous
friction with a fluid being squirted out of the nozzle, or perhaps
it’s aerodynamically carried along by a current of air that makes it
wave like a pennant in the breeze.  Either way, within the nozzle, the
wire experiences no electrical field and thus no anodic dissolution,
but once stuck to the workpiece, it does experience anodic
dissolution.  Under diffusion-limited conditions, wherever it’s stuck
on the workpiece, it will anodically oxidize; if the resulting salt is
soluble, it will dissolve.  Little bits of wire will be left behind,
due to parts of the wire bottlenecking and cutting off their
downstream from electrical contact with the mother ship, but most of
it will oxidize into cations, possibly solvated cations.

If the electrolyte is slightly acidic, it may be able to finish eating
whatever bits of metal are left over without any applied voltage.

If the electrolyte in the workpiece has a mixture of different
solvated anions, some which form an insoluble precipitate with these
cations and others which form a soluble salt with them, the region
immediately around the wire will become depleted in the former, as the
cations precipitate them.  If there isn’t too much of this insoluble
precipitate, though, most of the cations will remain solvated and
continue to diffuse through the solution, even after the wire is gone.
These solvated cations will continue to diffuse until they can
precipitate some of the other anions, causing the precipitation zone
to grow.  Within a certain range of concentrations of the
precipitating anion, a gel will solidify while remaining nanoporous,
permitting solvent penetration and solute diffusion.  Insufficient
precipitate will fail to form a gel, and too much precipitate will
make the gel insoluble.

For the gel region to continue growing rather than solidifying while
remaining the same size, the solvated cations should diffuse much more
rapidly through it than the precipitating anions.

For example, with aluminum wire in a solution of sodium acetate with a
little bit of phosphoric acid, most of the anodically oxidized
aluminum will go into solution as aluminum acetate, while a small
amount of it will form phosphates: first monoaluminum phosphate, then
the insoluble dialuminum phosphate, and finally aluminum
orthophosphate.  (At the cathode, probably hydrogen is evolved and lye
is produced.)  Meanwhile, the aluminum acetate is diffusing out
through the gel until it encounters phosphoric acid, which steals the
aluminum and makes acetic acid.
