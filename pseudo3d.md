I was thinking about low-computational-cost ways of doing 3-D graphics.

Standard rasterization
----------------------

The standard approach, the one to beat, is to rasterize a pile of
triangles and then fill them.  We transform all the vertices into camera
space by multiplying them (an Nx3 matrix) by a 3x3 matrix and adding an
offset vector, and we also multiply the normals of the triangles through
the same 3x3 matrix to find the transformed normals.  We discard the
triangles whose transformed normals point away from the camera ("backface
removal"), perspective-transform the vertices by dividing each one by
its Z coordinate, and now we have a list of 2-D triangles to paint on
the screen.  (Because the perspective projection of a line segment is
always a line segment, the perspective projection of a triangle is always
a triangle.)

Filling the triangles can be done in different ways, but the most
common is the [Phong illumination model][Phong], which can be applied
by flat-shading them (painting the whole 2-D triangle a solid color),
Gouraud-shading them (using a normal at each corner to calculate a solid
color, then interpolating between these colors), or Phong-shading them
(using a normal at each corner to interpolate a normal at each point
on the triangle, then applying the Phong shading model to it).  This is
usually further enhanced by perturbing the surface color with a texture
map and perhaps perturbing the surface normal with a bump map.

Phong illumination just sums ambient, diffuse, and specular illumination;
the intensity of Lambertian diffuse illumination is computed simply
by taking the dot product of the surface normal and the illumination
direction, while specular illumination is approximated by taking some
power of the dot product of the view angle and the reflection angle
determined by Heron's equal-angle law of reflection.  You need one
diffuse and one specular term per directional light source; the diffuse
and ambient terms typically include the surface color as a factor,
while the specular term does not.

[Phong]: https://en.wikipedia.org/wiki/Phong_reflection_model

Phong illumination is not a very precise approximation of the BRDF of
real surfaces; the Fresnel equations give a much better approximation,
but even less precise approximations are often good enough to provide
useful visual information.

Some parts of some triangles may be hidden behind other triangles, and
so not every sample generated from a triangle in this way is necessarily
visible.  The standard approaches are [scanline rendering][scanline] which
calculates which triangle is visible in which interval of each scanline,
[Z-buffering][z] in which we maintain the Z-coordinate of each pixel and
discard all non-nearest samples, [the painter's algorithm][painter] in
which we draw the furthest triangles first and then overwrite them with
nearer triangles, and the reverse painter's algorithm in which we draw the
nearest triangles first and never take a new sample for a pixel that has
already been painted.  The painter's algorithm and its variants produce
incorrect results if the polygons intersect or have cyclic overlaps; to
solve this problem we sometimes cut polygons apart where they may
intersect.

[scanline]: https://en.wikipedia.org/wiki/Scanline_rendering
[z]: https://en.wikipedia.org/wiki/Z-buffering

This approach is slow for two reasons: first, it paints every pixel
on the screen (many times, if there are many layers of objects hidden
behind each other), and second, you need a large number of triangles
to get a reasonable approximation of a curved surface, resulting in a
large number of vertices and thus a large number of matrix multiplies.
However, since this is the standard approach, a lot of effort has
gone into optimizing it; by optimizing aggressively, [Andy Sloane has
demonstrated that the 240-face Utah teapot can be rendered more or less
this way at 30fps on a 16MHz AVR][a1k0n], and even slower machines like
the Sinclair ZX Spectrum have used simplified versions of it for
real-time 3-D graphics.

[a1k0n]: https://www.a1k0n.net/2017/02/01/arduboy-teapot.html

### Wireframe ###

One way to make this faster is by not filling the polygons:
just outline them with something like Bresenham's original algorithm
or run-length slice line drawing.  This enabled the game Elite to do
a real-time 3-D first-person space shooter on the BBC Micro and the
Sinclair ZX Spectrum, and similarly enabled the Star Wars arcade game
around the same time to do first-person perspective 3-D.

Although this mostly eliminates the need for a Z-buffer or overdraw,
if the objects are to appear solid, you can't just draw every triangle
that faces the camera; you need to do [hidden-line removal][hidden].
The brute-force approach to this problem is to test every line to be
drawn against every face to see if it's partly or wholly hidden by it,
then only drawing the visible part.

By making your objects polyhedral rather than curved, you can reduce the
number of polygons needed to represent them accurately as wireframe
drawings.

[hidden]: https://en.wikipedia.org/wiki/Hidden-line_removal

### Pointclouds and starfields ###

Going one step further in reducing the amount of drawing to be done,
instead of drawing lines between points, you can just transform some
points (rotating, scaling, and perhaps perspective-projecting) and plot
pixels on the screen at those points.  Then you don't need to draw
lines, and if you use the painter's algorithm, you don't need to do
hidden line removal.  You can still apply the Phong illumination model
or something similar at each point if you know the surface normal at
that point; if your point cloud is dense enough it will be opaque.

This approach is used, for example, in [Andy Sloane's donut.c][donut].
And the classic starfield screensaver also works this way, plotting
single-pixel stars in a different position every frame.

[donut]: https://www.a1k0n.net/2021/01/13/optimizing-donut.html

The first Star Control game had a 3-D star map which was displayed as a
rotating point cloud; when a star was selected, lines would be drawn to
other stars within travel distance, wireframe-like.

In some cases you can adjust the density of the pointcloud to give an
illumination effect without any explicit illumination calculations.
In particular, if you sample some irregular surface by intersecting a
bunch of lines with it that are either parallel or converge at a point,
and which are either uniformly randomly distributed or evenly spaced,
the density of the intersection points thus generated will approximate
Lambertian illumination.  The points of a heightfield representing a
mountainous landscape, for example, approximate illumination by a sun
directly overhead in a dark sky.

### Strength-reduction of operations ###

When you're rendering an animation, it's common for the changes from one
frame to the next to be incremental, offering some opportunities for
speedup, by computing data for the new frame by adjusting the previous
frame; similarly it may be possible to compute data for a point on the
screen by adjusting data for nearby points.

On machines with a lot of parallelism, like GPUs, you have to be
careful about intraframe strength-reduction, because it introduces
data dependencies between iterations.  It doesn't take a huge amount of
intraframe parallelism to keep most computers occupied, but it's easy
to accidentally eliminate all of it.

#### Interframe strength-reduced rotation ####

Sloane's optimized version of donut.c, for example, incrementally rotates
the pointcloud from its previous position through a fixed rotation
matrix on each iteration instead of producing a new rotation matrix for
a different angle and rotating the pointcloud from its original position
each time.  He unfortunately didn't know about Minsky's circle
algorithm, which provides a stable way to do this.

Aside from not needing to calculate a new matrix each frame,
strength-reduced rotation is often advantageous because a fully general
rotation of a point requires a 3x3 matrix multiply by a vector, which
is nine multiplies and six additions per point, while rotation around a
single axis requires only four multiplies and two additions per point.
Using Minsky's method, it requires only two multiply-accumulates per
point, which can sometimes be arranged to be mere bitshifts and adds.

#### Intraframe strength-reduction by adding fixed deltas ####

If your vertices are on lattice points and are fairly dense, you can
encode the whole point field as a tour around the grid, for example
with a string of letters drawn from "XYZxyzP", where X, Y, or Z means
to move by +1 unit in the specified dimension; x, y, or z means to move
by -1 unit in the specified dimension; and P means to place a point.
A figure encoded in this way can be rotated without doing any multiplies
per point; instead, you just add or subtract the appropriate column of
the general rotation matrix to implement the XYZxyz operations.

Heightfields can benefit from most of this optimization; supposing the
heightfield samples are evenly spaced in X and Y but have arbitrary
Z values, you can proceed from sea level at one sample to the next by
adding the X or Y column of the transformation matrix, and then multiply
the Z column of the transformation matrix by the heightfield value.
This means only 3 additions and 3 multiply-accumulates per sample instead
of 9 multiplies and 6 additions.

#### Interframe strength-reduced translation: starfields ####

[Kevin Stokes's "Inner Mission" screen saver from 01990][im] is one of
many starfield screensavers from the 01980s and 01990s.  A starfield is in
a sense just a random point cloud that is always translating toward you
in Z, so the delta between a star's old projected X and new projected
X is, say, x/z - x/(z + d) = (x(z+d) - zx)/(z(z+d)) = xd/(z(z+d));
similarly for Y.  When d is small compared to z, this scales linearly in
x, roughly linearly in d, and roughly inverse-quadratically in z.  So you
can get a good pseudo-3D approximation by giving each star a velocity away
from the center of the screen proportional to its initial position and
increasing that velocity over time; often they used linear or exponential
accelerations rather than the correct inverse-quadratic acceleration,
but visually it's hard to tell the difference.

However, it's pretty cheap to do this precisely: if each new star starts
out at the same Z-coordinate and your velocity never changes, they
all go through the same sequence of Z-coordinates and thus reciprocal
Z-coordinates, so you can just precompute them.  You still need to
multiply the X and Y coordinates by these reciprocals, though.

[im]: https://ftp.sunet.se/mirror/archive/ftp.sunet.se/pub/simtelnet/msdos/screen/im17.zip

#### Interframe strength-reduced translation: parallax ####

In the case where you're translating a scene only in X and Y, the Z
values of vertices don't change, and neither do their reciprocals, so
you don't have to recalculate the reciprocals.  And you can calculate
the new projected X and Y of each vertex by multiplying that reciprocal
Z by the change in your viewpoint X and Y.

Taking this to the extreme, you have [2.5D games with a 2-D foreground
layer scrolling over one or more 2-D background layers which scroll more
slowly][pscroll].

[pscroll]: https://en.wikipedia.org/wiki/Parallax_scrolling

#### Newton-Raphson iteration ####

I feel like there are probably a fair number of cases where a single
iteration of Newton's method from one sample to the next along some
dimension, either spatial or temporal, could do a good enough job of
approximating some attribute you want for rendering of 3-D objects.
An example might be the locations of specular highlights on a curved
surface.  I don't have this idea well enough developed, though.

Sloane uses this approach to renormalize his incrementally rotated
vectors, for example.

#### Polynomial strength reduction with divided differences ####

If you want to tabulate values of some order-N polynomial at regular
intervals, you can use the method of differences to calculate it with N
additions per value, based on Newton's calculus of forward differences.
It's one line of C, emulating the Difference Engine:

    for (size_t i = 0; i < N; i++) x[i] += x[i+1];

Each time you run this, x[0] receives the next value of an arbitrary
order-N polynomial specified by an array of N+1 values, which are a
linear transformation of the polynomial coefficients which you can
initialize with a vanishing triangle; for example, for x^3 - 2x^2 + x -
5, we can tabulate the values for x = 0, 1, 2, 3:

    -5  -5  -3   7
       0   2  10
         2   8
           6

and continue the tabulation from the [-5, 0, 2, 6] state:

    -5  -5  -3   7  31  75
       0   2  10  24  44
         2   8  14  20 
           6   6   6

And 31 and 75 are indeed the correct values for x = 4 and x = 5.

You can tabulate values from an order-N spline instead by replacing
`x[N]` at each knot.

### Axonometric projection ###

The most expensive part of the rasterization process per-triangle is the
perspective projection, since that involves finding the reciprocal of
each transformed Z coordinate.  Sloane's Arduboy code mentioned above
uses a linear approximation of the reciprocal.  But if you use an
axonometric projection instead of a perspective projection, you can just
drop Z entirely.  This is good enough for many uses, and it has a
synergistic effect with many of the above optimizations.

In particular, with intraframe strength-reduction for rotation,
axonometric projection allows you to incrementally add the *projections
of* the X, Y, and Z unit vectors, as is commonly done in manual
drafting; and in the case of heightfields representing landscapes, the Z
unit vector typically has a 0 component in X.  And of course panning
becomes simple.

_Congo Bongo_ and _Zaxxon_ were two very early 3-D video games that used
axonometric projection to do 3-D worlds on very limited 8-bit machines,
but they did not permit changing the viewpoint.  This allowed them to
use precomputed 2-D tiles.

### Precomputed illumination ###

Since the diffuse and ambient components of the Phong illumination
model do not depend on the viewpoint, if lighting doesn't change (or
doesn't change often), you can bake them into textures, thus reducing
the shading operation to just a texel fetch.  A lot of current games
also bake ambient occlusion computations into the textures in the same
way.

Ray-tracing
-----------

The standard alternative to rasterization is Turner Whitted's
ray-tracing algorithm, in which we iterate over screen pixels instead
of scene geometry.  By using a bounding volume hierarchy we can avoid
any consideration at all of scene geometry that doesn't come close to
being rendered, so although ray tracing is usually slower, in theory
it becomes faster when you have sufficiently complex scene geometry.
Raymarching using signed distance functions is a currently popular
variant of this.

2.5D and pseudo-3D
------------------

Wikipedia has [a page on "2.5D"][0], mentioning [axonometric
projections (including axonometric sprites)][2], [Wolf3D-style
raycasting][1] (which is similar to the [voxel-space terrain rendering
approach used in Comanche][3]), and other techniques.

[0]: https://en.wikipedia.org/wiki/2.5D
[1]: https://en.wikipedia.org/wiki/Ray_casting#Ray_casting_in_early_computer_games
[2]: https://en.wikipedia.org/wiki/Isometric_video_game_graphics
[3]: https://en.wikipedia.org/wiki/Isometric_video_game_graphics

### Pointclouds and voxels ###

The voxel-space terrain-rendering approach is related to the pointcloud
heightfield rendering approach I described above as an optimization of
the standard rasterization algorithm; it differs in three ways:

1. Each point is rendered, conceptually, as a vertical ray shooting
   infinitely downwards from the point's 3-D position, with a width wide
   enough to fill up the width of the point, not a single pixel.
2. As in raytracing (and Wolf3D-style raycasting) we iterate over
   screen pixels rather than scene geometry.
3. The reverse painter's algorithm is used to avoid too much overdraw;
   in the implementation I've seen, a "Y-buffer" keeps track of the
   boundary between already-drawn terrain and not-yet-drawn terrain in
   each screen column, but I think you could avoid this by making the
   outer loop iterate over screen pixel columns and the inner loop
   iterate over distances.  Then your "Y-buffer" reduces to a single
   value.

In the game _Comanche_ for which it was devised, a texture map with
precomputed lighting delivered astonishingly realistic landscapes.

### Wolf3D-style raycasting ###

Wolfenstein 3D came out in 01992, so it ran on what we now think
of as pretty low-end hardware; [a 16MHz 80286 with 1MiB of RAM was
adequate][vcfed].  To perform interactively on such hardware, it traced
a single ray for each screen column, tracking which wall it hit, and how
far away it was, in order to sample the wall texture at the proper size in
the middle of the column.  ([It didn't have floor or ceiling
textures][lodev], or it would have had to track which floor/ceiling
tiles it crossed as well.)

[vcfed]: https://forum.vcfed.org/index.php?threads/wolfenstein-3d-system-286-vs-8086.31815/
[lodev]: https://lodev.org/cgtutor/raycasting2.html

I think the Cube 1 FPS engine used a hybrid of this technique and the
Voxel Space technique; like Doom, it can't handle rooms over rooms, but
it can handle both floor textures and wall textures, as well as uneven
terrain, things arching over the floor, and shaped doorways, windows,
and see-through fences; and because you can walk on top of items, you
can fake room-over-room by making a "floor" item.

### Racing/shooting games with nonuniform raster skewing ("Pseudo 3D") ###

[Louis Gorenfeld's pseudo 3D page][4] describes how old racing games used
a raster shearing effect to get the illusion of a perspective-projected
road.  He points out that Street Fighter perfectly renders the
perspective-projected texture on the ground with sideways changes of
viewpoint simply by shearing a pre-perspective-projected view uniformly,
and that you can get an imperfect perspective view of a curving road by
shearing it nonuniformly.

[4]: http://www.extentofthejam.com/pseudo/

Nearly all of these games (like OutRun, Pole Position, etc.) composited
2-D sprites on top of the basic road in sizes that varied.

He points out that, as long as the ground is flat, you can precompute
a Z position for each scan line on the screen, so it's easy to avoid
what he calls the "oatmeal effect" of the road seeming to move slower
once it gets closer to you, which comes from using a parabolic
approximation to perspective.  And then you can warp this "Z map" to
simulate hills.

[Sheffield] writes about a pseudo-3D game he wrote in 02015, similar to
Space Harrier or OutRun, in which you can run over deer.

[Sheffield]: https://www.gamedeveloper.com/design/asset-placement-in-a-pseudo-3d-world-tricks-of-the-trade

### Vectorballs ###

Vectorballs, or "vector balls", were a staple of the demo scene [for years
after their release in 01989 by Tomsoft][vectorballs].  This is similar to
the 2-D sprite approach Lou discusses above and the pointcloud approach
I described earlier: you rotate and perspective-project your pointcloud
in order to find out where the centers of some spheres project to on
your screen, and then you composite pre-drawn sprites of spheres onto
the screen at those locations, using the painter's algorithm.  Because
spheres look the same from any angle, you can rotate an object made out
of spheres just by moving the sphere centers around on the screen and
making the spheres bigger or smaller.

When the spheres are close enough together in 3-space to intersect,
this produces the usual incorrect occlusion problem.

[vectorballs]: https://www.pouet.net/prod.php?which=33480

### Zdog ###

Dave DeSandro's [Zdog][5] (Danger! Page may eat all your RAM if you
visit it!  But [the GitHub repo is safe][6]) is a simple pseudo-3D engine
which models 3-D shapes out of spheres, toruses, and capsules, following
the example of P.F. Magic's [Dogz][7]; a capsule is two half-spheres of
the same size smoothly joined by a tangent cylinder of the same radius.
The silhouette of the axonometric projection of a capsule is a thick
line with rounded ends, and a sphere is just a special case where the
line has zero length.  The silhouette of the perspective projection of a
capsule would be a line whose length changes linearly.  

Zdog also supports polygons with rounded corners and thickness, which
are rendered in the corresponding way: capsules for the edges plus
a prism filling the middle, simply by drawing and filling a polygon.
The limiting case of a polygon made out of capsules is a circle, and
Zdog approximates toruses with thick ellipses.

As with vectorballs, Zdog just uses the painter's algorithm, with the
consequence that intersecting solids are drawn not quite correctly,
popping through one another when their transformed Z-coordinates pass
equality.

Zdog flat-shades all these things, making extremely lightweight vector
representations; I think probably a little bit of work on shadows and
highlights, or even lighting-driven gradients, would make the 3-D shape
a lot clearer in some cases.  But flat-shading everything makes it
easier to export a vector SVG of a 3-D scene.  Using the 2-D boundary as
a clipping path for a 2-D texture might also be an interesting approach.

[5]: https://zzz.dog/#what-is-zdog
[6]: https://github.com/metafizzy/zdog
[7]: https://www.youtube.com/watch?v=6lKSn_cHw5k

Some ideas I don't know if anybody has tried
--------------------------------------------

It occurred to me that a 3-dimensional parametric polynomial curve in an
axonometric projection becomes a 2-dimensional parametric polynomial, and
this could be a useful way to render the boundaries of a curved surface,
either an explicitly parametric surface or one that is defined vaguely
by being bounded by one or more parametric polynomials.  In particular
Bezier splines seem like an appealing way to do this: project their
control points and you've projected the curve.  (I don't think this
holds for perspective projections.)

If you can estimate motion in an animated scene, you can render an
approximation of a new frame by copying nearby chunks of pixels from
the previous frame, which is potentially a lot faster than rerendering
the whole scene.  If you then have some way to determine where the
approximation is likely to be the most wrong, for example in places that
have just been de-occluded or moved into the frame, you may be able to
focus rendering effort there.

A raytracer might be a lot faster if it could shoot a smaller number of
rays; the kind of two-dimensional raycasting done by Wolfenstein 3D and
Voxel Space is one approach to this problem.  A different approach is to
shoot one ray per 16x16 pixel tile and sample not just a single color
on the surface it intersects but the most visible Fourier coefficients
of the surface texture, having previously computed them.  Then maybe
you can transform this sparse representation as necessary (multiplying
by the scalar illumination level, rotating and scaling nonuniformly to
transform the texture according to foreshortening) and then finally do
the inverse Fourier transform to get an image patch.

This is somewhat related to some other notes I've made elsewhere about
wavegrower-style nonphotorealistic rendering using foreshortened circles
to show surface orientation; a fairly sparse set of samples might be
adequate to give a good impression of the 3-D shape that way.

### Uses for automatic differentiation ###

Automatic differentiation can compute arbitrary derivatives of
anything computable, though it can't tell whether you're actually at a
discontinuity and will give you a derivative there when it shouldn't.
Calculating a one-dimensional derivative, either a gradient or the
partial derivative of all your outputs with respect to one of the inputs,
takes roughly as much computation again as the original computation;
calculating gradients or Jacobians (reverse-mode autodiff) requires
either storing all the intermediate results or recalculating them.
This is central to current artificial neural network work.

So here are some ideas for how to use autodiff to make 3-D images
cheaper to compute.

#### Newton-Raphson iteration with automatic differentiation ####

Newton-Raphson iteration is easy once we have the gradient of a quantity
we're trying to find the zero of, which autodiff can give us, and it's
fast once we have that and a reasonably close approximation to the
solution.  But when do you need to find zeroes of functions to make 3-D
images?

The most obvious case is that the ray-surface intersection tests used in
every kind of raytracing (raycasting, raymarching, etc.) are typically
finding zeroes of a function.  The parametric for the ray gives you a
function f(t) = (x, y, z), which you can compose with, for example, an
implicit-surface function to get a function whose zeroes are precisely
the intersections.  In simple cases like spheres this is easy enough to
do in closed form, but in other cases we commonly resort to things like
SDF raymarching.

Transforming an explicit surface representation (e.g., z = x - y) into
an implicit one is trivial; we just subtract the two sides of the
equation (e.g., x - y - z), getting a formula whose zeroes are the
points where the original equation holds true.  A parametric
representation is not so easy; in general finding an intersection in
that case requires solving three simultaneous equations in three
unknowns g(u, v) = f(t).  Each coordinate gives us an equation.

Fortunately it turns out that Newton's method works for systems of
simultaneous equations too; you take the Jacobian of your error vector
g(u, v) - f(t) and, in theory anyway, multiply its inverse by your guess
to get the new guess.

Silhouettes are another interesting application.  Silhouettes of
curved surfaces are useful for three reasons: shadows, NPR, and texture
boundaries.  The silhouette of a shape such as an amphora as seen from a
light source defines the shape of the shadow it casts on other objects;
for nonphotorealistic rendering it is desirable to draw a line around the
silhouette as seen from the viewpoint, and possibly to add a null halo
around it as a depth cue; and if we are estimating textures from a small
number of samples, for example with color gradients or Fourier components,
the silhouette limits how far the estimated texture should extend.

A silhouette of a surface with respect to a ray field is the locus where
the surface normal is perpendicular to the field, i.e., its dot product
with it is zero.  So being able to take the gradient of that dot product
with respect to (X, Y, Z) allows you to use Newton iteration to find a
nearby point on the silhouette.

This assumes there's some reasonably regular way to extend the
surface-normal formula to points off the surface.  The easiest case is
an implicit surface, in which case the normal is just the gradient of
the surface's formula, which is defined everywhere.

In general you want a lot of points on the silhouette rather than just
one, and you want them to be actually on the surface.  Supposing you
have an implicit function for the surface, you can, I think, use
Newton's method for simultaneous equations in the same way as for
finding an intersection between a ray and a parametric surface.

Once you're on the surface, the cross product of the ray field with the
surface normal gives you the direction in which the silhouette runs
along the surface.  You can alternate between stepping in the direction
of the cross product and iterating Newton-Raphson to get back to the
silhouette.

Casting sharp shadows from one surface onto another is another case of
finding a zero of simultaneous equations: the projection of the silhouette
along the ray field and the surface it's falling on.

A thing that gives me some pause with respect to all of this is that,
although it's very fast when it works, the convergence guarantees for
Newton-Raphson are very squirrely.

#### Color gradient estimation with automatic differentiation ####

Color gradients are the derivative of color with respect to (X, Y)
position in the image.  It seems like there's a wide class of images
where you could sample rather sparsely at first, calculating the
gradient of color with respect to the pixel coordinates and also the
Hessian (the second derivatives), and use the Hessian to guess where you
need more samples.

I'm not sure how well this works if you used iterative methods in
computing the pixel color in the first place.

#### Motion estimation ####

For motion estimation, what we want to know is in which (X, Y, time)
direction the color of the pixels in an area will *least* change.
This is not the same thing as the gradient of color with respect to
(X, Y, T), which gives you one of the infinite number of directions
perpendicular to it.
