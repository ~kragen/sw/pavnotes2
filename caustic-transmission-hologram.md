I was looking at how the light transmits through some G.U.M. brand
flossers.  These are thin, nearly flat pieces of transparent green
plastic with striations molded into their handles (plus some dental
floss, which is irrelevant here).  At some angles, light passes
through them freely as if they were flat green glass, while at other
angles, it is blocked; this seems to relate to light being focused
within the material by the lenticular striations.

A bumpy dielectric surface illuminated by collimated light can produce
a caustic at some depth inside the dielectric; the angle of
illumination will displace the caustic and also slightly defocus it.
The first derivative of the focus depth with respect to the
illumination angle is zero; I think the second derivative can also be
adjusted to be zero, keeping the focus depth approximately constant
over a wider angle.  Two such surfaces on opposite sides of a thin
transparent dielectric sheet can be designed to produce their caustics
in the same plane, for example halfway in between them.

If we replace the second light source on the second side of the sheet
with an eye, the points that the eye can see are the same points that
second light could illuminate.  A hypothetical point source of light
at what would have been a point caustic from the second light — a
point at which all the rays over some nonzero area on the second
dielectric surface were focused — appears to the eye to be
illuminating an area on the surface of the material.  The surface
focuses all this hypothetical internal point source’s rays onto the
eye.  If the point source moves slightly to be off the caustic, it
goes dark from the eye’s perspective; those rays are being formed into
a beam going elsewhere.

The caustics formed by the first surface are in effect just such point
sources, at least if we disregard the diffraction limit to the finite
beam waist and stick to geometric optics.  And a change in the angle
of the eye can induce just such slight lateral shifts of the focal
plane.

This is similar to the “opacity holograms” I’ve written about
previously, where two opaque sheets with transparent windows in them
are placed parallel a short distance apart for parallax viewing; the
two caustic patterns are in effect the two sheets, with the caustics
being the holes or windows.  But this realization of the principle is
vastly superior, because opacity holograms discard the light that
misses the holes, and therefore can provide only a dim image, while
this approach instead focuses all the light through the holes.
Roughly all the light that goes in one side of the sheet comes out the
other, just in different directions.  (You may need some “junk images”
which serve to consume unwanted light that falls on an area that's
dark in every objective image.)

Rayform and others have demonstrated that simple optimization
algorithms suffice to design any desired caustic.  In this case, we
have an even more underdetermined optimization problem: we want to
produce a variety of different grayscale images from different points
of view.  This can probably be tackled as a two-step process of first
optimizing the two caustic patterns to be thus multiplied to
approximate those images, then optimizing the surfaces to produce
them.  But applying an optimization algorithm to the whole process
from beginning to end is likely to produce better results.

An especially interesting variant is where the two caustic patterns
are constrained to be identical except for an offset.  In this case,
you can realize the artifact in the shape of a single refractive first
surface on a second-surface mirror of half the thickness of the
corresponding transparent sheet.  The illumination angle is
necessarily not normal to the mirror in this case, or the problem
becomes infeasible, but at other angles it is quite feasible.

This permits the construction of white-light reflective grayscale
holograms with two directions of parallax and no rainbow artifacts,
and since it’s a purely geometric-optics design, it doesn’t require
micron-scale features.  Moreover, you can overlay parts of the surface
with color filters, for example in a RGBG or RGBW pattern like those
used on LCD screens, and get a white-light reflective full-color
hologram with two directions of parallax.  This is the holy grail of
holography, and it’s clearly achievable by this means, without so much
as a laser.

Right now the standard way to construct precise complex refractive
surfaces such as microlens arrays is two-photon stereolithography, in
which photopolymerization permits arbitrary 3-D shapes of transparent
resin with deep submicron precision.  But it isn’t necessary to go to
such lengths.

For mass production, you could make molds in the shape of the desired
refractive first surface and inject optically clear thermoplastics
into them, which is how the G.U.M. flossers are made; then you can
stick aluminum foil or white paper on the second surface.  Because
we’re only interested in where they are and are treating them as
isotropic sources, an approximation that’s good enough as long as the
NA of the lenticular features on the two surfaces isn’t far apart, the
direction of reflection from the point sources in the caustic doesn’t
matter, so an actual mirror isn’t necessary.  Printing the color
filters on the paper is probably cheaper than printing them on the
surface.

The required thickness of the dielectric sheet is the focal length of
those lenticular features, so there’s a tradeoff between the depth of
the features and the thickness of the sheet.  Shorter focal lengths
mean higher NA and therefore wider viewing and illumination angles,
but also lower brightness, because the incident light is spread over a
wider range of viewing angles.

The *minimal* thickness of the dielectric sheet, if you aren’t limited
by fabrication precision, is limited by diffraction.  The beam waist
size I mentioned above doesn’t allow you to scale this technique down
indefinitely.  The smallest caustics you can get on the focal plane
are roughly the same size as the smallest things you can see with
optical microscopy, and that causes bleed-over between adjacent
viewing angles.  A thicker sheet can to some extent accommodate more
viewing angles.  Viewing angle separation is also limited by the
diffraction-limited beam divergence at the lenticular feature that
focuses the caustic onto your eye.

It should be pretty easy to simulate all of this with computerized
ray-tracing, and except for the diffraction part, that’s how you’d go
about doing the optimization, probably with a differentiable ray
tracer.  You can draw the mechanism clearly in two dimensions, but
physically-based rendering simulation should give a reliable gauge of
the kind of image quality that can be expected.

A problem related to the holography holy grail is light-angle
decoding.  The scenario is that you have your archive amulet and
you’re hiding in a forest following an apocalypse, and you urgently
need to look up “where there is no doctor” advice for your child, but
your cellphone battery is dead and you have no way to charge it.  You
have sun and shade.  How can you design the archive amulet to store a
lot of information for such a purpose?

Well, one obvious answer is to write really small on one part of it,
unfolding another part of it into a 40× jeweler’s loupe.  But a
different approach is to hold the amulet in the sunlight reflecting
the light onto a shaded area, thus projecting readable text that
changes as you tilt the amulet slightly.  Perhaps you can manage 900
different tilt angles (the sun isn't a very good point source) each
with a page of text.

However, I’m not sure the holography design outlined above is capable
of making such an archive amulet.  The spot on the wall doesn’t really
care which spot on the amulet is throwing light on it.
