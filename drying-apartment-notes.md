Suppose a certain apartment is 40 m² and 2.6 m in height, thus
containing 104 m³ of air, which is at a temperature of 30° and a
relative humidity of 80%, and we want to reduce its relative humidity
to 40% with anhydrous muriate of lime.  How much do we need?

[According to the psychrometric chart][0] at 30° air can hold 27 mg of
water per gram of dry air, so 80% is 22 mg/g or 22 g/kg.  At
[1.2 kg/m³][1] we have 125 kg of air, so 2.75 kg of water.

> 22 g/kg × 40 m² × 2.6 m × 1.2 kg/m³ = 2.75 kg

[Muriate of lime, CaCl₂][2], weighs 111 g/mol anhydrous, and 219 g/mol
as the hexahydrate, and deliquesces to a saturated solution of
102 g/100 mℓ of the hexahydrate at 30.2°, which means 100 ml of water
per 0.466 mol, which is 100 ml of water per 51.7 g of the anhydrous
muriate.  Note that this means that each gram of the muriate absorbs
974 mg of water to get to the hexahydrate, but 1934 mg to get to the
saturated solution, nearly doubling its water capacity.

> 102 g/100 mℓ × 1 mol/219 g × 111 g/mol = 51.7 g/100 mℓ

So we need 1.42 kg of anhydrous muriate to absorb *all* that water.

> 2.75 kg × 1 cc/g × 51.7 g/100 mℓ = 1.42 kg  
> 22 g/kg × 40 m² × 2.6 m × 1.2 kg/m³ × 1 cc/g × 102 g/100 mℓ × 1 mol/219 g × 111 g/mol = 1.42 kg

[0]: https://upload.wikimedia.org/wikipedia/commons/9/9d/PsychrometricChart.SeaLevel.SI.svg
[1]: https://en.wikipedia.org/wiki/Density_of_air
[2]: https://en.wikipedia.org/wiki/Calcium_chloride

But actually in this case the objective was only to reduce the amount
of water by half, and the muriate's ability to absorb humidity peters
out below 30% RH anyway (varying slightly by temperature), so we only
need 710 g of anhydrous muriate to reach our goal of 40%.

Suppose we were instead using [muriate of magnesia][3] (E511;
bischofite in the hexahydrate form), another deliquescent salt which I
happen to have a large jar of, and the main component of
[bitterns][4].  Its common forms are the anhydrous form (95.211 g/mol)
and the hexahydrate (203.31 g/mol), and its solubility on an anhydrous
basis is given as 52.9 g/100 mℓ at 0°, 54.3 g/100 mℓ at 20°, and
72.6 g/100 mℓ at 100°.  If we extrapolate pessimistically that at 30°
we have 55.0 g/100 mℓ (rather than, say, 56.7 g, by interpolating from
the 100 mℓ number), then deliquescing into 1.38 kg of water would
dissolve 760 g of muriate of magnesia, only slightly more than you'd
need with muriate of lime.  In this case, though, a little more of the
water absorption would take place by hydrating the solid salt rather
than through deliquescence.

[3]: https://en.wikipedia.org/wiki/Magnesium_chloride
[4]: https://en.wikipedia.org/wiki/Bittern_(salt)

The deliquescence humidity of bischofite is [33.1%, according to
Saltwiki][5], only slightly higher than that of muriate of lime; the
same source gives its solubility as 5.75 mol/kg at 20°, which works
out to 117 g/100 mℓ on a hexahydrate basis or 54.7 g/100 mℓ on an
anhydrous basis, which is close to the number above taken from
Wikipedia.  The [solubility diagram in the Saltwiki page][6] shows a
region of stability for the octahydrate at standard pressure starting
a little below 0°.  Its deliquescence humidity varies from 34.1% at 0°
to 30.5% at 50°.

[5]: https://www.saltwiki.net/index.php?title=Bischofite&action=edit
[6]: http://web.archive.org/web/20220624215829/https://www.saltwiki.net/index.php/Bischofite

A possible concern with this salt is that Saltwiki says it "decomposes
at 116°–118°", which presumably yields acid, which could be a
practical inconvenience for returning it to the anhydrous state.
[Muriate of lime, by contrast, dehydrates from antarcticite][8] to the
tetrahydrate at 30° and the dihydrate (sinjarite) at 45°.  Sinjarite
rehydrates to the tetrahydrate at 9% RH, and the anhydrous form
rehydrates to sinjarite at 6%, so it is a very good desiccant indeed
for lab use, but this is probably not very relevant to household
climate control.

[8]: https://www.saltwiki.net/index.php/Calcium_chloride

Other exciting salts in the neighborhood include Epsom salt
(MgSO₄·7H₂O); CaMgCl₄; [tachyhydrite][12] ([Saltwiki][11])
(CaMg₂Cl₆·12H₂O); [carnallite][7] (KMgCl₃·6H₂O), which is naturally
abundant and very cheap, too cheap to ship in fact, often [treated as
an industrial waste][10]; potassium carbonate; dipotassium phosphate
(E340); tripotassium phosphate (also E340); diammonium phosphate; etc.

[7]: https://www.saltwiki.net/index.php?title=Carnallite&action=edit
[10]: https://ideas.repec.org/a/eee/appene/v265y2020ics0306261920302506.html
[11]: http://web.archive.org/web/20220501000000*/https://www.saltwiki.net/index.php/Tachyhydrite
[12]: https://en.wikipedia.org/wiki/Tachyhydrite

[Epsom salt][9] doesn't deliquesce at reasonable relative humidities,
but it can reversibly dehydrate and rehydrate through several
hydration states, so it might be suitable as a solid, non-deliquescent
desiccant.

[9]: https://www.saltwiki.net/index.php?title=Magnesium_sulfate&action=edit
