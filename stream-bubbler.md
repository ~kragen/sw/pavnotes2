As I was peeing into the water of a toilet today, it occurred to me
that this was a solution to the bubbler fouling problem.

Bubblers, for example aquarium bubblers, are a common way of achieving
a high surface area for interactions between a gas and a liquid, such
as mass exchange by diffusion.  But they suffer from the problem that
the pores in the bubbler through which the gas bubbles enter the
liquid can become [fouled][3] by the liquid or by reaction products, so
bubblers tend to be somewhat high maintenance.

[3]: https://en.wikipedia.org/wiki/Fouling

A stream of liquid traveling through gas into a pool of liquid
entrains gas, which become bubbles in the pool of liquid, providing
high surface area.  The two liquids need not be identical.  If the
stream isn’t vertical, the bubbles will rise out of the liquid in a
different location from where they entered it, thus potentially
releasing the gas from the bubbles in a different chamber, sealed from
the original chamber by the presence of the liquid (as with the other
kind of bubbler, which acts as a one-way valve for gas); aside from
actual interactions with the liquid, this is one way to compress gas
with a stream of water, similar in some ways to a [trompe][0] (such as
Charles Taylor’s famous [Ragged Chute air plant][4]) or an [aspirator
or ejector][2], though the latter is more often used to produce
vacuum.

[0]: https://en.wikipedia.org/wiki/Trompe
[4]: https://web.archive.org/web/20110324154749/http://www.cobalt.ca/index.php/ragged-chutes
[2]: https://en.wikipedia.org/wiki/Vacuum_ejector

The point of impingement of the stream on the pool is far away from
any solids, so it cannot foul.  The stream itself may be accelerated
by a variety of techniques; if it is dropped from a great height, it
can be poured from an open-top container, which will be almost immune
to fouling (at least from the liquid-gas contact), and if it is
ejected from a nozzle under high pressure, it can contain a pure
solvent such as water that is not prone to fouling the nozzle.

There is, of course, another solution to increasing liquid-gas
interaction area: [packed towers][1].  Packed towers also suffer from
fouling.

[1]: https://en.wikipedia.org/wiki/Flue-gas_desulfurization#Packed_bed_scrubbers
