I’m finally reading _The Unix Programming Environment_, about 35 years
late.  Surprisingly, I’m learning new things from it, even though I’ve
been using Unix mostly daily for about 32 of those years.  Some of
those things aren’t even historical trivia.

About a third of the way through, I realized that it represents a
thoughtful inventory of the most important capabilities of Unix: those
that were considered important enough both to add early on and to
explain in the book.  In a sense, it’s a recipe for building PWB, the
Programmer’s Workbench.  So I thought it might be useful to try to
write a kind of telegraphic outline of what those capabilities are.

There are of course things Unix couldn’t do then, some of which it can
do now, or problems in the environment where it existed, and which
were major faults at the time of the book.  Hindsight allows us to see
some of these faults.

Chapter 1: Unix for beginners
-----------------------------

This chapter introduces a *lot*:

- typing, with typeahead, and correcting typiong mistakes
- ^D
- aborting processes
- logging in
- preference for lower case
- `date`, `mail`
- `who`, buddy presence status
- `write`, instant messaging
- `!` for subshells
- `news` and netnews
- manual with permuted index
- `man`, `learn`
- `/usr/games/fortune` and other games
- `ed` (`a`, `w`, `q`, `1,$p`), `ls` (`-lutr`)
- `cat`, `pr` (`-3`, `-m`), `more`, `lpr`
- `cp`, `rm`, filenames
- `wc` (`-l`), `grep` (`-v`) with patterns without any metacharacters,
  `sort` (`-rnf`, `+3` (`-k3`)), `tail` (`-1`, `+3`)
- `cmp`, `diff`
- `pwd`, `cd`, `mkdir`, `rmdir`, the directory tree, `.`, `..`,
  pathnames, permissions
- `/bin`, `/usr/bin`
- wildcard filenames (including backtracking `*`, `?`, and character
  classes), `echo`, ASCIIbetical order
- I/O redirection (`<`, `>`, `>>`) and pipes `|`
- implicit stdin handling of most commands
- `;`, `&`, `wait`, `kill`, `ps` (`-ag`), `nohup`, `nice`, `at`
  (`3am`, `2130`, `930pm`)
- `stty`, `.profile`, shell variables, $variable interpolation, custom
  prompts, variables as macro filenames
- `bc`, `uucp`, `units`
- shell quoting

I count 39 shell commands in the above, plus 16 flags, a bit of `ed`’s
command language, six shell plumbing operators, the syntax and
semantics of pathnames, and the wildcard language.  All of this
produces a comfortable, polite environment with a uniform filesystem
namespace and a lot of functionality and composability that can be
invoked without much effort.  It’s almost enough by itself to be
usable for something beyond socializing, but not quite.  Like, with
what’s in this chapter, you can configure your Unix account to print a
fortune when you log in.

I think this is where $HOME, $MAIL, $PATH, $PS1, and $PS2 are
introduced.

A crucial feature, of course, is that many of the commands (`date`,
`who`, `man`, `ls`, `ps`, `wc`, `grep`, `sort`, `diff`, even `echo`
and `cat`) produce textual output that can be saved to files, input
usefully to many other commands (`mail`, `write`, `wc`, `grep`,
`sort`, `diff`, `more`, `lpr`, `at`), and edited with the text editor.

Faults:

- shell quoting
- no networking except `uucp`
- inconsistent command-line flags
- cryptic commmands (good for writing, bad for reading)
- no feedback on valid arguments (tab-completion or anything)
- inadequate interactive editing (^H, ^U and maybe ^W)
- text editing designed for teletypes (@, #)
- double-escaping: `#date`. `\#date`, `\\#date` did three different
  things
- no commmand-line history, logging, or editing
- no replying to mail!
- no integration of `who`, `mail`, and `write`
- more generally, having to retype command output (*e.g.*, from `ls`,
  `who`, `mail`, or `ps`) as new input
- no mood/away in `who`
- no buddy presence change notification
- computers were so slow that checking for email before each prompt
  was significant
- baud rates and uppercase-only terminals and misconfigured tab stops
- having to configure $TERM and backspace characters for your terminal
- table layout (of `who`, `wc`, `ps`, and `pr`, say) depends on
  fixed-pitch fonts
- no job control/multiplexing/windowing (just subshells and `&`)
- `write` can interrupt your lines with the other person’s (still
  true) and there was as yet no ^R rprnt to recover from this
- filenames were limited to 14 characters
- filesystem case-sensitivity provoking user errors
- 8-bit or even 7-bit characters
- the default field delimiter is a nonblank to blank transition, so by
  default sort fields can’t contain spaces
- more generally, the common text format is pretty bug-prone
- no ambient awareness (where am I?  did my last command fail?)
- single-column `ls` by default (awful on a CRT without scrollback)
- no search through previous output
- `.` in PATH, which is mostly a problem because of
- the total lack of POLA
- What happens if you have a file named  `-t`?

A kind of general difficulty with things like shell quoting and `at
3am <myscript` is that you don’t have any interactive feedback about
what is going to happen.  Maybe at 3 AM you’ll get an error from the
shell about an unmatched quote mark.  When you’re composing a command
there’s no way to tell if you’ve moistyped a firename or used the rong
flag letters until you submit it for execution by the shell.

You could imagine an environment, even on such limited machines, that
did a better job of providing rapid feedback and supporting
interactive experimentation with such pipelines, by virtue of
streaming command output, caching command output, interactively saving
cached command output to files, etc.  Something like Apache SPARK’s
“resilient distributed dataset”, without the distribution or probably
immutability.  As it was, the system they were using didn’t even
support `csh`-like `!! | sort`.

Chapter 2: The File System [sic]
--------------------------------

- a file is a sequence of bytes (not records, disk blocks, Unicode
  characters, etc.)
- pipes, magtapes, and terminals are also sequences of bytes, using
  the same APIs
- `od` (`-bxcd`)
- how control characters are handled by terminals and used in Unix
  text
- tab stops are standardized at 8
- system calls, `read()`
- `-u` in `cat`
- `^D` on a nonempty line
- `file` and the pure a.out magic number 0o000410
- weak file typing (just one kind of file) maximizes composability
- relative and absolute pathnames
- `du` (`-a`), `find`
- `cd ..`
- `root`
- `crypt`
- uids, gids, `newgrp`, `/etc/passwd`, PBKDFs, `/bin/passwd`, suid
- permission bits, `chmod`
- inodes with three times
- hardlinks, `ln`, `mv`, directory destinations for `mv` and `cp`
- `-ic` for `ls`
- `/boot`, `/unix`, `/dev`, `/bin`, `/etc/getty`, `/bin/login`,
  `/etc/rc`, `/lib`, `/tmp`, `/lib/libc.a`, `/usr/dict`, `/usr/man`
- `spell`
- device nodes with major and minor numbers
- `/etc/mount` (now in `/sbin`)
- `fsck`
- `df`
- the `tty` command
- `mesg n`
- `/dev/tty` for user interaction
- `/dev/null`
- `time`
- `/usr/dict/words` (196513 bytes)
- `egrep`

This chapter has another 16 commands for us, plus 8 new flags, but
mostly focuses on the interface to the filesystem and files, plus
general system lifecycle stuff.

These two chapters are almost adequate for file management purposes.
`sort` needs `-n` for `du` output, and `mknod` isn’t mentioned.  You
really need a decent text editor, though, and so far they haven’t even
shown enough of `ed` to make it usable.

Faults:

- the semantics of hardlinks to mutable files is dodgy, because
  updating files in place is dangerous if the program or system
  crashes or the disk fills up; reflinks are more sensible
- also of course cross-device hardlinks don’t work
- there was no sticky bit for `/tmp` yet, so asid from tempfile races,
  anyone could straight up delete anyone else’s tempfile
- more generally, the Unix file protection mechanism embeds far too
  much policy in the kernel, making it impossible to get right
- suid, maybe PBKDFs, octal file modes
- atimes create write traffic from reads
- the reification of `.` and `..` in the filesystem (still true)
- `od` defaults to octal (still true)
- the layout of filesystem directories, and thus the 14-byte and
  65535-inode limits, was exposed to every program that enumerated
  directory entries
- and it was binary (for updatability I guess) so you couldn’t grep or
  sort your directories
- `du` and `df` output block counts
- `rm` is forever (still true)
- accidentally overwriting files is forever (still true)
- `mv` overwrites files peremptorily (still true)
- `fsck` at boot
- grepping through the 196kB `/usr/dict/words` took about 10 seconds
  because the computer was so slow

Chapter 3: Using the shell
--------------------------

This is largely about shell scripting, even though Chapter 5 is the
one that is actually “Shell Programming”.

- `tee`
- `sleep`
- `^G` to sound an audible alarm
- `()` and `{}` in the shell
- `&&` and `||` in the shell
- more shell quoting: `''` vs. `""`, ` \ `, multiline strings, etc.
- `<<heredoc`s
- command interpolation with `` `cmd` ``
- shell scripts with $1, $2, etc.
- `sh` (`<scriptfile`, `scriptfile`)
- the special case of executable shell scripts (documented here as a
  kernel thing, but without the #! magic number that made it an actual
  kernel thing)
- `grep` as a PIM
- `-y` for `grep`
- `-t` and `-l1` for `pr`
- `pick` for interactive selection, not installed by default
- sourcing scripts with `.`
- per-command envvar=value settings
- `export`
- the standard error stream and the `2>f` and `>&` constructs
- `for` in the shell, looping over sets of globbed filenames
- how `for` works with pipes
- `-b` for `diff`
- `csh` and its command-line history

That’s 7 more commands, about 12 more shell combinators, a new control
character, and 4 more flags for commands.  But the main point of this
chapter is new ways of using previously presented shell facilities.

At this point we begin to see actual non-omphalogical applications
like a tea timer, a personal phone directory, and an email blast; as
well as a rather remarkable utility program they call `bundle`, an
early version of `shar` as an 8-line shell script, which they say was
independently written by Alan Hewett and James Gosling.

This is the chapter in which Unix emerges as a full-fledged
programming environment, though at this point it still lacks the
facilities for doing easy things like Fizzbuzz.

Nowadays instead of the `pick` command they use here (credited to Tom
Duff) I use `^X *`, which interactively expands a glob so I can edit
it.  I feel like shells could go further in providing feedback on
globs.

As they say, writing a program that creates a program (both in the
shell) is a powerful technique in any environment, but it’s especially
convenient in Unix.  (The quoting problems endemic to Unix make it
highly bug-prone, which the authors mention; also, they’ve so far kept
utterly mum about `m4`, unlike in Kernighan’s earlier _Software
Tools_.)  Also, the fact that the user’s main UI to system
functionality is scriptable means that anything the user can invoke
through it is also programmable.

To get some context on the complexity budget for the shell
implementation, I ran `cloc` on the 7th Edition Bourne shell
(/usr/src/cmd/sh in
<http://www.tuhs.org/Archive/Distributions/Research/Henry_Spencer_v7/v7.tar.gz>).
It says it’s 2522 lines of C, 572 lines of header file, and 27 lines
of Makefile, for a total of 3121 lines of source code.  `wc` says it's
4145 lines, and `pr` formats it into 91 pages of source code.  Some of
those lines do look like this:

    REP     IF trapnote&SIGSET THEN newline(); sigchk() FI
    PER (len=read(f->fdes,f->fbuf,f->fsiz))<0 ANDF trapnote DONE

but most of them are things more like this:

    TREPTR          i, r;
    {
    MSG             readmsg;
    prs(sysmsg[sig]);

The executable is 17.3 kilobytes.  It’s humbling to think Steve Bourne
could pack so much functionality into so little code.

Faults:

- backslashed backquotes: a sure sign things are going off the rails.
  Nowadays we have `$()` to avoid this.
- reparsing `$*` on embedded spaces, a terrible botch partly fixed by
  the ugly `"$@"`, explained in section 5.7.
- similarly, backquoted `grep` commands that work until a filename
  gets a space in it.
- and their `games` script uses `$*` in a way that will break as soon
  as you try to pass an argument containing spaces to a game.
- heredocs are by default scanned for things to expand, including
  backquotes, which is a very wrong type of error to favor; an errant
  `` `rm things` `` can really ruin your day.
- punning a permission bit as an executable-filetype bit is ugly, and
  was perhaps stimulated by defaulting to `.` in $PATH, which has been
  deprecated since the 80s for security reasons.
- echoing no argumennts was ugly; some versions of `echo` would
  produce no output.
- echoing without newlines, and flags to echo, is still ugly.
- multiline strings shouldn’t default on; separate constructs for
  multiline and non-multiline strings are less error-prone.
- pipelines were purely linear still; `>()` and `<()` didn’t exist
  yet.  The pi-calculus and SPICE netlists demonstrate that writing
  general labeled graphs as text is doable, though careless cycles can
  provoke deadlock.
- the parsability of things like `a ; b | c` is a usability error,
  erring too far on the side of terseness and not enough on the side
  of non-ambiguity.  You should require `(a ; b) | c` or `a ; (b |
  c)`.  In general this bias toward total precedence orders in
  language design causes grief.
- `diff` didn’t yet take a directory argument

Chapter 4: Filters
------------------

This chapter is largely about `awk` scripting.

- `fgrep` (`-f`)
- the full regexp languages of `grep` and `egrep`
- implicitly, the regexp language of `ed`
- `-nb` for `grep`
- `-f` for `egrep`
- `tr` (`-sc`), `dd`, `uniq` (`-cdu`)
- `sed` (`-nf`, `s/pat/rep/g`, `/foo/cmd`, `/foo/!cmd`, `3q`, `d`,
  `start,end` ranges, `$d`, `/pat/w file1`, and backslashed newlines,
  almost all of which are also `ed` commands)
- `awk` (`-F`, `/re/ { print }`, `$0`, `$3`, `$(expr)`, `NF`, `NR`,
  `FS`, user-defined variables, `printf`, commas in `print`, `~
  /foo/`, `!~ /foo/`, quoting, parentheses, `=`, `%`, `+`, `/`, `+=`,
  `>`, numeric constants, `int`, `split` `length`, `substr`, `BEGIN`,
  `END`, `if`, `for`, `while`, `break`, `next`, `exit`,
  (associative) arrays, `a[i]`, `{}` blocks, `>` and `>>` and `|` for
  `print`, etc.
- implicitly, the mbox format (`grep From`)
- `/usr/dict/web2`, the list of words from Webster’s Second
  International dictionary
- `-duo` for `sort` (previously `-rnf` and `+3` had been presented)
- `-s` for `ls`
- `comm` (`-123`)
- `|` at the end of a line in the shell

This is only 7 more commands, plus 16 command-line flags, but also two
new DSLs, `sed` and especially `awk`.

There’s a table of what wasn’t even the full set of `sed` commands at
the time: `[abcdilpqrstwy=!:{]`

More non-omphalogical applications here include checking a document
for common typos, listing all the words in English of six or more
letters where all the letters are in alphabetical order, listing the
most common words in a document, and reminding you of your calendar
entries.

There’s a bit of the semantics of `sed` I don’t understand in
`s/UNIX/UNIX(TM)/gw u.out`, which they say writes the changed lines to
u.out as well as writing all the lines to stdout.

`awk` is arguably the most table-oriented of all the standard Unix
filters mentioned so far, more even than `sort`, though `join` surely
beats it.

`awk` is also, for many purposes, an enormously more practical
programming language than either C or the shell, and C hasn’t been
introduced yet, which is a surprising observation for chapter 4 of a
book about the Unix *programming* environment, at a time when plenty
of mainframers were competing to see whose S/370 assembly routines
processed payroll fastest, and microcomputer software was still almost
exclusively assembly or BASIC.  Tables 4.3 and 4.4 list more `awk`
features: `FILENAME`, `OFMT`, `OFS`, `ORS`, `RS`, `-=`, `*=`, `/=`,
``%=`, `||`, `&&`, `>=`, `<`. `<=`, `==`. `!=`, implicit string
concatenation, `-`, `*`, `++`, and `--`.  Table 4.5 lists `sin`,
`cos`, `exp`, `log`, `getline`, `index` (string search), `split`, and
`sprintf`, aside from functions listed above.

7th Edition `awk` is 2702 lines of code, 59 pages, and has most of
these features (but of Table 4.5 I think only `sprintf` and `index`,
spelled `sindex`); the variables are in tran.c, the operators in
proc.c.  The Bourne shell was written without the benefit of TMG or
`yacc`, just using recursive descent, but `awk` is built with `lex`
and `yacc`.  Unlike GNU `awk`, though, and unlike `bc`, neither the
`awk` documented in TUPE nor the `awk` in v7 seem to support
user-defined functions.  The executable is 46 kilobytes.

7th Edition `bc` is just a 553-SLOC `yacc` grammar, and although I’m
not totally confident of my reading of the source, it seems that it
*does* support user-defined functions (with one-letter names).  The
executable is 13 kilobytes.

They were confident enough in their one-way encryption that purported
crypted passwords for `root`, `ken`, and `dmr` are printed in the
book, respectively `3D.fHr5KoB.3s`, `y.68wd1.ijayz`, and
`z4u3dJWbg7wCk`.  I wouldn’t be surprised if these had been cracked by
now.

Hilariously, the paragraph introducing `double` from from the Writer’s
Workbench contains the phrase “the the”, split across a page break for
subtlety.

The `double` program, like compiler errors and TAGS files, would have
greatly benefited from a convention for hyperlinks into the middle of
text files.  This was finally added to Unix in `acme` in Plan9, and
possibly earlier in `sam`.

This is the first mention of `lseek` or seeking, and thus the first
hint that you could do something with a file other than read or write
it sequentially, in the context of "very large files", here meaning
hundreds of kilobytes.

It’s interesting to ponder how things like `awk`, `sed`, and even
`grep` and `sh` could be redesigned as compilers to a common
interpretive or JIT language runtime.

The right-justification in the line-folding example is interesting, a
choice I hadn’t thought of before, but plausibly a good one:

    A somewhat longer li\
                     ne.

There was evidently no per-user `crontab` at the time of writing; they
recommend using a “self-perpetuating” `at` job to run a script every
morning.

This line reminds me of an unfortunate choice in `ffmpeg` which is
patched over by an uncontrollable fit of interactivity:

> The arguments specify *input*, never output (...) An early UNIX
> filesystem was destroyed by a maintenance program that violated this
> rule, because a harmless-looking command scribbled all over the disc
> [sic].

Faults:

- Shell glob character classes didn’t support negation `[^a-z]`.
- More generally, there are three different slightly inconsistent
  regexp dialects: shell globs, `grep`/`ed`/`sed`, and `egrep`, with
  more to come; I’m not sure if `awk`’s coincides with one of those.
- `ls -l | grep '^.......rw` clearly shows the bias for writability
  and experimentation over readability; how would you tell if you got
  the right number of periods?  The limited expressivity of the text
  format bears part of the blame here; if you could `| when o+r o+w`
  it would be potentially safer.  On the other hand, there are some
  real usability advantages to being able to figure out how to write
  the command by eyeing the `ls -l` output.
- Only `grep` supports backreferences (here called “tagged regular
  expressions”).
- `sed` apparently didn’t support escapes like `\t`, with the
  consequence that you had to include literal tabs in your command
  lines, further harming readability (since those looked like spaces
  on the terminal).
- The particular use of tabs there was to project the `du` output
  table down to the filename column, or the `who` output down to the
  username and login time columns, again suggesting that better
  support for tabular output would be useful.
- `awk` lines exclude the trailing newline, like `perl -l`; this is a
  case of making easy things easy at the expense of making everything
  more error-prone.
- Although `sed` and `awk` have somewhat cryptic command syntaxes for
  ease of interactive entry, they still offer many concessions to
  readability.  Probably immediate interactive feedback on the program
  thus constructed would ease the pressure for readability, permitting
  an even more ready-to-hand input syntax like that of dc(1), which
  has not been mentioned despite its historical role as the first
  non-game Unix application.  The pattern would be something like the
  inverse of bc(1), which parses infix input to produce postfix
  output; here you’d be parsing some kind of input language optimized
  for usability (visibility, feedback, rapidity of entry, etc.) while
  simultaneously producing infix output (as feedback) and execution
  results (as more feedback).
- They describe `` "'"`date`"'" `` as a “remarkable sequence of quote
  characters”, which it certainly is, and describe it as “required to
  capture the date in a string in the middle of the `awk` program”,
  which is also true, though “painful to debug” may be a more
  important descriptor.  More recent developments in Unix do afford
  slight improvements.  One reason for the difficulty is the
  requirement for `at` jobs to be written in `sh`, where here `awk`
  would have been a better choice.  The lack of `#!` in early Unixes
  also relegates `awk` (and `csh` and `perl`) scripts to a
  second-class status not suffered quite as badly by `sh` scripts.
- Evidently there was no reasonable way to do calendar math, such as
  calculating tomorrow’s date.  Arguably the Unix shell still doesn’t
  have a standard one, though, e.g., GNU `date` supports `date
  --date=tomorrow`.
- Evidently their `sed` didn’t have a `-e` option yet, so you had to
  use quoted multiline strings to get multiple `sed` commands into a
  command-line script.  And (still unfixed!) that was the only way to
  provide arguments to the `i`, `a`, and `c` commands.

Chapter 5: Shell programming
----------------------------

This chapter promises to be mostly about making shell programs meet
rigorous standards of robustness, which is a false promise because at
the time that was simply impossible.  It’s hard enough today, but at
the time (with no secure tempfiles, $IFS-dependent parsing, arbitrary
line length limits in all the shellutils, 8-bit-cleanness problems all
over the place, and `.` in everybody's $PATH) it was hopeless.

Introduced here:

- `cal`
- `case`
- `$#`, `$@`, `$-`, `$?`, `$$`, `$!`, `$IFS`
- `-v` and `-x` for `set`, which was previously presented for listing
  shell variables
- the pun of `sh`’s `set` to set shell command-line arguments and to
  parse into words
- `test` (`-r`, `-w`, `-f`, and on System V, `-x`)
- `-s` in `cmp`
- `-s` in `grep`
- command exit status
- `exit`

It’s somewhat troubling that the mere existence of process exit
statuses, and thus error handling, has been completely ignored up to
this point!  It does seem in keeping with the devil-may-care spirit of
Unix of that time, though.

They use the somewhat overcomplex

    echo $PATH | sed 's/^:/.:/
                      s/::/:.:/g
                      s/:$/:./'

when I think a simpler solution would have been

    echo ":$PATH:" | sed 's/::/:.:/g'

The leading and trailing `:` do not harm this approach (in today’s
bash):

    set $(IFS=:; (echo $PATH))

However, that doesn’t correctly handle embedded whitespace; the outer
`set` is parsing the output of `echo` with its own $IFS.  This variant
solves that problem but requires removing the leading `:` or the
resulting null argument, here done with `shift`:

    oifs=$IFS; IFS=:; set $PATH; IFS=$oifs
    shift; for i; do echo "- $i"; done

Poignantly:

> Some of the differences [between shell globs and `ed` regexps] are
> simply bad choices that were never fixed — for example, there is no
> reason except compatibility *with a past now lost* [my emphasis]
> that `ed` uses ‘`.`’ and the shell uses ‘`?`’ for “match any
> character”.

Faults:

- The poignant one above, mentioned in the chapter text.
- The “rigorous” “robust” `which` presented will totally fail if
  either the command name or any directory in $PATH contains
  whitespace.  Also, of couse, it fails to find shell builtins.
- A `test` in your current working directory would break shell scripts
  rather badly if it was in $PATH.  Also true of things like `ls`, I
  suppose.  It mentions that `test` was a builtin in SysV, so this
  problem was already getting less bad.
- There was no `type` command in sh, so they rather horrifically
  reimplemented a broken version of it as `which`.
- `cal` at the time required a year argument and did not accept month
  names, two problems they call out and which are since fixed.  Also
  its error messages sucked.
- `sh` has extremely limited facilities for handling arrays, which
  really shows in the backbends used here to get the current date.
