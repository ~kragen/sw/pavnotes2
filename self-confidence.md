At times we are exhorted to have self-confidence or self-esteem.  This
can seem impossible or counterproductive: what is the use of confiding
in something fallible, or more closely approximating narcissism?

What is meant by this is to take action without self-sabotage.  A
person who is confident in themself and esteems themself highly can
carry out their plans without stopping halfway through to change
course, and this is a very valuable attribute.  Narcissistic people
are in fact often quite good at this, though they can alienate others
in a way that amounts to self-sabotage in the long run.

But taking action is still a good strategy even when you are weak and
uncertain and know it.  A poor chess player does not become better by
attempting to take back their moves — not even within a single game.
Acting confidently on the best plan you can formulate still beats
carrying out no plan at all, or carrying out one plan for a few steps,
then abandoning it to attempt another.  Sometimes that can be more
educational, but it is less fruitful in the present, and costs more.

Self-esteem is optional, and sometimes impossible, but self-reliance
or self-confidence is in a certain sense unavoidable, forced upon us
by the nature of life.  I don’t mean that it’s so obviously the best
alternative that anything else would be foolish; I mean that the
apparent alternatives are in fact illusory.

Suppose Mary says that Douglas said a bad word.  You might, as a
consequence, believe that Douglas said a bad word; or you might not.
(For the time being let’s leave aside the question of whether this
change in belief is voluntary or not.)  To the extent that her telling
you so causes you to believe that Douglas said a bad word, we would
say that you have *confidence* in Mary, or *reliance* on her word.
If, rather, you still believe to some extent that Douglas may not have
said the bad word, to that extent you do not have *confidence* in
Mary; you are not *relying* on her word.  This is just a matter of
definition.

But in fact a second cause is also present, because you cannot directly
observe Mary telling you something.  You can only observe that you
*perceive* her to do so, which is to say, you can observe that *your
self tells you* that Mary tells you something.  But you yourself
are fallible.  You may be dreaming that you are conversing with Mary,
or you may be talking with someone you falsely believe to be Mary, or
you may be misunderstanding what Mary is saying — something that
happens so often that interpersonal conflicts are commonly euphemized
as “misunderstandings”.  So if your beliefs change because Mary
tells you something, you are not only relying on Mary’s perception,
but also in your own perception of Mary.

An even more crucial sense in which you are relying on your self when
you rely on Mary’s word is that, unavoidably, you do not rely on
everyone’s word; we all learn early in life that not everyone’s word
deserves our confidence, not all the time, because it is common for
people to lie or to be wrong.  When you rely on Mary, not only are you
relying on your self to interpret what Mary tells you and who Mary is;
you are also relying on your self telling you that Mary’s word is
reliable, at least right then.

So you can avoid relying on someone (though perhaps you cannot avoid
relying on anyone), but you cannot avoid relying on yourself, if only
to choose who to rely on.

And this is also true when we move from the realm of epistemic belief
to the realm of action.  Perhaps you can rely on your roommate Mary to
pay your electric bill, or perhaps you cannot; but it is unlikely you
can rely on her to do so if you ask her not to, especially if you also
hide the bill from her and don’t tell her the password on the electric
utility’s website.  Perhaps you can *predict* her actions (Douglas is
likely to pay his own electric bill even if you wish he wouldn’t) but
you cannot *rely* on them to be aligned with your values.  To the
extent that your actions run counter to your own values, you cannot
rely on anyone else to act in accordance with your values either.

And that is why it is so important to take action, and to take the
actions you think are best: because you can often avoid relying on
other people’s actions, but only rarely can you avoid relying on your
own.

This necessity of self-reliance in action is less absolute than in
belief.  Even children can come to believe in any error if their own
perceptions are unreliable, as they often are, but usually, they do
not need to rely on their own actions to have food tomorrow.  Their
parents can put food on the table tomorrow almost regardless of what
the children do.  But self-reliance in action is a necessary
prerequisite to freedom: by taking action and choosing who to rely on
to take action on your behalf, you can create a future aligned with
your values.  Without your actions, that will happen only by
coincidence if it happens at all.

XXX note fears of plans going wrong
XXX we are the fish’s water?
