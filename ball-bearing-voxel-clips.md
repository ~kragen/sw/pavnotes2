Two bearing balls in contact have a fairly precise distance between
their centers; when they’re minimally loaded in compression, it’s a
*very* precise distance.  Typical [bearing ball size tolerances are on
the order of a micron or two][0], even up to tens of millimeters.
[Hertzian contact][1] increases this error, but because the cheapest
bearing balls are made of hardened steel, it takes a fairly large load
before it deforms by a whole micron.  It’s possible to reach such
tolerances by machining with special high-precision machine tools, but
10 or 20 microns is a more normal error.  (ISO Class Normal ball
bearings made out of such balls have a [specified runout][2] (see
[also][3]) of up to 10μm for bore diameters of up to 10mm, and up to
20μm for bore diameters of up to 50mm, while Class 6 (P6) it’s up to
7μm runout for up to 10mm bores and up to 10μm runout for up to 50mm
bores, but I think that runout comes from somewhere else than the
balls, maybe the races.  Class 5 (P5) is up to 4μm for up to 10mm, up
to 5μm for up to 50mm.)

[0]: https://en.wikipedia.org/wiki/Ball_(bearing)#Grade
[3]: https://koyo.jtekt.co.jp/en/support/bearing-knowledge/7-1000.html
[2]: https://www.astbearings.com/bearing-tolerances-precision-levels.html
[1]: https://en.wikipedia.org/wiki/Hertzian_contact_stress

It occurs to me that you can clip two bearing balls together with a
plastic clip, and the clip itself doesn’t have to be very precise; it
just needs to be shaped so that the balls are actually in contact.  If
you clamp *four* bearing balls together in a tetrahedron, any three of
the balls precisely locate the fourth in all six degrees of freedom
(it doesn’t matter what the rotation of the ball is because its
sphericity error is much smaller than its diameter error).

One way this has been used is for high-precision dividing heads.  A
circle of 36 bearing balls held immovable, with each ball in contact
with the two balls on each side of it, has fairly precisely 10° per
ball.  Two such coaxial circles in contact with one another such that
each ball is in contact with two balls from the other circle will be
rotated to some multiple of 10° to a high degree of precision; in this
case size inaccuracies between the balls tend to even out.  Normally
such apparati have been made of steel, but you could 3-D print a
plastic form to hold the bearing balls in.  Eventually it will creep,
though perhaps annealing the plastic and adding a layer of latex can
delay this, but even then the balls should remain in contact.

A second pair of circles with 40 bearing balls each gives you 9°
instead; the sum of the two gives you any rotational multiple of 1°.
By rotating the middle rotor by one ball relative to the top and
bottom, you force the top and bottom to rotate by either +1° or -9°.

In a linear form, this same approach can give you a precision linear
measurement standard using two or four parallel rows of balls of two
different sizes.

The linear form enjoys the property that the first derivative of
length with respect to the displacement of each ball perpendicular to
the row of balls is precisely zero, though the second derivative is
negative.  This is good because this displacement is only resisted by
the imprecise clip rather than the precise ball contact.  For the
circular form, the first derivative (of the angle subtended around the
center of the circle by the displacement between the centers of two
adjacent balls) is not precisely zero, but it is small as long as the
number of balls is large.  Small balls are better for reducing this
first derivative in the circle, but it’s better to have big balls for
reducing the second derivative.

By clipping balls of a single size together in various permutations,
you can get quite precise distances.  Four parallel rows of balls form
a square beam, each ball in contact with two balls of its own row and
two balls of two of the other rows, but none of the
diagonally-opposite row.  This conformation allows the precise balls
to resist sideways displacements for the linear measurement rather
than relying on the clips to do so.
