I need to plan out a new (physical) notebook.

- Acid-free paper from nearby bookstore.  A4 for ease of printing.
  Ideally 55g/m² but probably realistically 75
- A6-size leaves
- page numbers in base 9
- 200 g of paper instead of 147 g total.  With 75g/m² paper this would
  be 2.667m² of paper and 171 leaves; rounding to 172 we get 344 pages
  in 86 cut sheets made from 43 uncut A4 sheets.  48 sheets in 96 cut
  sheets forming 192 leaves and 384 pages would be more pleasantly
  symmetric, weighing 225g.  No base-3 number of pages is conveniently
  close.  96 cut sheets would be 12 quires of 8 cut sheets or 24
  quires of 4 cut sheets.
- Distinguished primary/marginal pages
- Fields for title, ikonos, date on primary page?
- Preallocated table of contents.  I get about 23 entries on each ToC
  page and a bit over one item per primary page, so I might need one
  ToC primary page per 20 primary pages.  192 primary pages then would
  need 9.6 primary pages of table of contents, or perhaps 12 to be on
  the safe side.
- Small areas on each marginal page with decimal square dots and
  isometric dots for drafting
- Binary category code bubbles along page edge, with alignment lines
- reference material:
    - periodic table
    - salt solubility table
    - psychrometric chart
    - physical constants from definitions.units
    - phone numbers?
    - Briggsian logarithms
    - trig functions
    - local map
    - nomograms
- ruler and protractor
- vernier protractor if transparencies
- colophon page

Reference material in 105mm × 148mm at 600dpi in a 5×8 font is 496
characters wide and 437 characters tall.  That’s 6.2 80×66 pages wide
and 6.6 tall, so about 40 regular-sized pages of material per page.  I
think it would be reasonable to devote 10% of the notebook to this: 19
leaves with 38 sides, about 1500 conventional pages of reference
material.

To fit one word per 100-meter city block, which seems like a
reasonable level of detail for local maps, you need 6×8 = 48 pixels,
which is 2.03mm at 600dpi.  So 105mm is 51 blocks, 5.1km, and 148mm
7.3km.  This permits fitting 37km² per side.  Sadly, this would still
require 103 pages (52 leaves, 26 A5-size cut sheets) for all of Gran
Buenos Aires, so it still requires selection.

The [6th Edition Machinery’s Handbook][0] is 1610 pages, so it’s a bit
too large.

A Spanish dictionary or Spanish-English dictionary would be pretty
useful.  In Argentina, [copyright for corporate works is 50 years from
publication][1], so anything up to 01973 from an institution like the
RAE should be fair game.  The 01914 [Diccionario de la Lengua
Castellana][2] (1112 pp.) would work, but is too large; similarly
Lisandro Segovia’s [Diccionario de Argentinismos][3] from 01911 (1104
pp.), the [01899 edition of the DRAE (1096 pp.)][4], but perhaps not
Tobias Garzón’s [Diccionario Argentino][5] (544 pp.).  However, this
last only lists specifically Argentine words such as “chota”, “ñoqui”
(in its culinary sense), and “rajarse” (in the sense of “make a
mistake”).  The [01884 DRAE is 1156 pp.][6], and a [shitty Google scan
of Ramón Cabrera’s 01837 “Diccionario de Etimologias” is 1195 pp.][7].
The Archive has [a scan of the Diccionario de la Lengua Española from
01970][8] (1466 pp.), scanned last year, but no download available.

[0]: https://archive.org/details/machineryshandbo00indu
[1]: https://commons.wikimedia.org/wiki/Commons:Copyright_rules_by_territory/Argentina
[2]: https://archive.org/details/diccionariodelal00realuoft
[3]: https://archive.org/details/diccionariodearg00segouoft
[4]: https://archive.org/details/diccionariodela00acaduoft
[5]: https://archive.org/details/diccionarioargen00garzuoft
[6]: https://archive.org/details/diccionariodel00acaduoft
[7]: https://archive.org/details/diccionariodeet00cabrgoog/page/n342/mode/2up
[8]: https://archive.org/details/bwb_P8-AXE-885

Paper and materials
-------------------

I bought a ream of “Pampa” brand “papel alcalino” made by Celulosa
Argentina.  It’s 70g/m², which is slightly better than I was hoping
for, and cost me $8600, which at AR$1035/US$ is US$8.31, 1.66¢ per
sheet, US$0.27/m², or US$3.8/kg.  This is so cheap it’s probably fake,
because acid-free paper normally costs twice that much.  How can I
verify it’s not fake?

The [US National Archives and Records Administration specification for
acid-free paperboard archival boxes][9] has a long list of
requirements I don’t know how to verify and I don’t know are
applicable to regular paper as opposed to paperboard or boxes, but
this part seems relevant:

> Lining paper (where applicable) shall be made from new cotton, or
> linen pulp, or fully bleached chemical wood pulp, or a mixture of
> them.
>
> In addition, the Lining paper shall be free of:
>
> - groundwood
>     ‐ test negative in ASTM D1030 phloroglucinol test with X5 Spot
>       Stains (ASTM has withdrawn this standard in 2011. NARA is
>       following 2007 version)
>     ‐ or Kappa number ≤5 in TAPPI T-236 test.
> - alum rosin sizing (TAPPI T-408 or ASTM D 549).
> - reducible sulfur (TAPPI T-406, <0.0008%).
> - particles or other impurities such as:
>     ‐ metals,
>     ‐ waxes,
>     ‐ plasticizers (i.e. wet strength additives),
>     ‐ plastics,
>     ‐ residual bleach,
>     ‐ peroxide.
>
> ### Sizing ###
>
> Alkaline sizing shall be used (surface, internal, or both).
>
> ### Alkaline Reserve ###
>
> The paperboard shall contain an alkaline reserve of calcium
> carbonate, magnesium carbonate, or a combination of both, within a
> range of 3–7% (calculated as CaCO₃) when tested according to TAPPI
> T-553 or modified by slurrying the sample pulp prior to measurement,
> and shall be evenly distributed throughout all plies and
> layers. NARA will provide slurry method procedure upon request.
> 
> ### Hydrogen Ion Concentration (pH) ###
>
> The pH value of the paperboard shall be between 8.0 and 9.5 when
> tested according to cold extraction method TAPPI T-509 or modified
> by slurrying the sample pulp prior to measurement.

[9]: https://www.archives.gov/files/preservation/storage/pdf/acid-free-archives-box.pdf

I feel like this TAPPI T-509 and TAPPI T-553 tests might be relevant
and capture what I’m after.  [SGS-IPS Testing says][10]:

> ### Hydrogen ion concentration (pH) of paper extracts (cold extraction method) ###
>
> #### Scope ####
>
> TAPPI T 509 measures the hydrogen ion concentration of a cold
> aqueous extract (unfiltered) of paper, expressed in terms of pH
> value. It is suitable for writing, printing, and sized industrial
> papers, but is not intended for unbuffered types such as insulating
> and condenser papers. The determined values may not be exact in a
> fundamental sense and should not be interpreted in terms of solution
> theory. The pH values are empirically correlated with end use
> requirements and paper qualities. This method avoids change of
> acidity or alkalinity resulting from heat-induced hydrolysis. A hot
> water extraction method is described in TAPPI T 435. Surface pH
> measurement of paper is described in TAPPI T 529.
>
> #### Summary ####
> 
> This method consists of a cold water (25 ± 5°C) extraction of the
> specimen for one hour followed by a pH measurement with a commercial
> pH meter.
>
> #### Significance ####
>
> The pH determination measures the extent to which the paper alters
> the hydrogen-hydroxyl equilibrium of pure water. The pH (acidity)
> may be important because of its effect on the permanence of the
> paper. Although the acidity may be determined as the amount of water
> soluble acidity by titration with alkali (TAPPI T 428 “Hot Water
> Extractable Acidity or Alkalinity of Paper”), the hydrogen ion
> concentration (pH) is more indicative of the stability of paper than
> is the total acidity.

[10]: https://ipstesting.com/find-a-test/tappi-test-methods/tappi-t-509-ph-cold-extract/

[TAPPI’s proposed reaffirmation of T-509][11] repeats most of the same
text in its introduction, but specifies more details, such as,
“Reagent grade water used in this test method shall conform to ASTM
Specification D 1193, Type I or II, Standard Specifications for
Water,” (I’m guessing I can use deionized water from the auto parts
store) and talks a lot about standard buffer solutions for calibrating
your pH meter.

Before extracting you’re supposed to cut the paper up:

> Holding the sheets at one end, cut and cross-cut the other end into
> 5-10 mm squares. Do not include any papers touched with the
> fingers. From each quantity of cut paper, weigh a test specimen of 1
> ± 0.01 g.

And then it’s mostly a matter of wetting and waiting:

> Transfer the test specimen to a 100-mL beaker. Add about 5 mL of
> reagent grade water and macerate with a flattened glass stirring rod
> until the specimen is wet.
>
> Add more water to bring the volume to 70 mL, stir well, cover with a
> watch glass, and allow to stand for one hour at room temperature (25
> ± 5°C). The specimen may stand for three or four hours (2) if no
> contamination occurs.
>
> (...)
>
> (...) stir and measure to 0.1 pH unit the pH of the extracted
> unfiltered mixture according to the instructions for the type [of]
> pH meter used.

I feel like I can probably do an adequate job of this with
pH-measuring paper instead of a pH meter.

They explain why NARA specifies the cold-water extraction:

> The pH value of a cold water extract is usually different from that
> of a hot extract. The pH of a hot extract of paper sized with rosin
> and alum is typically 0.5 or 0.6 unit lower than that of a cold
> extract.  This difference is due to hydrolysis of the alum or basic
> sulfates retained in the paper (2). Proponents of the hot extraction
> believe that the hydrolysis simulates changes which might occur
> during natural aging (3,4).  Barrow (5), however, found that cold
> extraction correlated better with his accelerated aging tests.
>
> It has been found (6) that melamine-formaldehyde resin hydrolyzes
> during hot extraction and releases alkaline products which tend to
> increase the pH value of the extract. Consequently, when a paper
> contains both alum and melamine resin, the pH of the hot extract may
> be higher, or lower than, or equal to the cold extract value. Other
> materials are believed to have a similar effect. Thus a value
> obtained after hot extraction might be misleading.
>
> (...)
>
> Reproducibility (between laboratories) = ± 0.5 pH units.
>
> (...)
>
> Related methods: French AFNOR NFQ 03-005; British Standard 2924;
> Scandinavian Standard SCAN-P14; and Canada PAPTAC G.25; AS/NZS
> 1301.421; ISO 6588.

[11]: https://www.tappi.org/content/SARG/T509.pdf

This is considerably simpler than I was fearing, really.
