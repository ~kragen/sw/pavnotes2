I feel like the thermal coefficient of resistance of tungsten, and the
refractory nature of quartz-halogen bulbs, make quartz-halogen bulbs
an appealing way to do temperature sensing and perhaps heating in a
tiny DIY furnace.  They ought to be more accurate, more reliable, and
easier to wire up than thermocouples.

Quartz-halogen illumination bulbs have now been prohibited for most
uses as too energy-inefficient, but there was a significant period of
time when they had replaced cheaper conventional incandescents, but
had not yet themselves been replaced with more expensive compact
fluorescents and LEDs.  They’re still available for use as infrared
heating lamps and car headlights.

Measuring some lightbulbs
-------------------------

My multimeter (Megalite M890G) warns me its battery is low, and it
measures some through-hole quarter-watt metal-film 10kΩ resistors from
Mina’s kit at 9.85, 9.87, and 9.85 kΩ respectively, so I suspect its
readings are about 1.5% low.

I have just measured some bulbs I have here.  At room temperature
(20°) two OSRAM 240VAC 25W frosted E27 incandescent jobbies
(presumably halogen) measure 153Ω and 154Ω, which I guess I should
correct to 155Ω and 156Ω.  (240V)²/25W = 2300Ω, so the filament
resistance rises quite a bit by the time it reaches operating
temperature, which is around [3100 K][0] or 2800°.  If tungsten’s
temperature coefficient of resistance were constant over that
temperature range (it’s not, but this is a crude estimate) that would
work out to 0.77Ω per degree, about an 0.5%/° variation at room
temperature, and actually it’s [about 0.45%/° at room temperature][1].
That should be a readily measurable difference of 0.7Ω/°.

[0]: https://hypertextbook.com/facts/2003/ElaineDevora.shtml
[1]: http://hyperphysics.phy-astr.gsu.edu/hbase/Tables/rstiv.html

A 70W “Halo Eco BAW ETA Electro” halogen bulb measures 50.7Ω.

A couple of 15W E14 “France” bulbs, which aren’t even quartz-halogen,
measure 322Ω and 306Ω.  These are even more readily measurable; it’ll
be 1.4Ω/°.  I also have a 15W E27 no-name non-quartz-halogen bulb,
which measures 324Ω.  I think these are still legal for sale as
decorative lights.

Finally, I have a 12V 55W “Magneti Marelli” car headlight bulb.  I
wrote about it in file `material-observations`:

> The internal bulb in one of these halogen household bulbs can be
> heated to an orange heat in the butane torch without any softening,
> and even quenching in water does not crack them.  This strongly
> suggests that it really is quartz.  Too bad it’s burned out.  These
> bulbs are hard to find nowadays, though some vendors have some
> remaining stock for under US$1 each, and 150W–500W exterior halogen
> floodlights still cost only US$2–US$4.
> 
> The replacement headlight bulb I got from an auto parts store (US$3)
> has the same behavior, except that I haven’t tried quenching it,
> suggesting that it’s also a quartz-halogen bulb.  (Also, the box
> carries a pictorial warning against touching the glass.)  This is a
> Magneti Marelli “H7 12V 55W PX26d”.  Cold (10°?) it measures some 7Ω
> on a shitty multimeter that measures 3Ω short-circuit, demonstrating
> why household 240V quartz-halogen bulbs would be better for use as
> RTDs.  When reheated to orange-hot, it measures 2Ω but the meter’s
> short-circuit resistance has dropped to 1Ω, demonstrating that the
> meter is totally inadequate for this purpose.  For heating-element
> purposes such a bulb ought to be adequate up to nearly 1000°.

The current, less shitty multimeter measures it at 0.8Ω, and measures
0.0Ω to 0.1Ω short-circuit.  If we assume the same 14.9× increase in
resistance up to operating temperature, 0.8Ω would increase to 11.9Ω,
which is a bit too high for 55 watts at 12 volts, which would require
9.6Ω.  This is a low enough resistance that it’s tricky to measure
small variations in it.  It seems likely that you could use it as a
20-watt heating element in a kiln, though.  At 5 volts, assuming 10Ω
and thus 500mA, it would emit 2.5W.

Nichrome wire is easy to get, but it only works up to about 1250° or
even 1150°, which makes quartz-halogen bulbs seem like an appealing
alternative for higher temperatures.  I have an immersion heater here
that measures about 60Ω and a couple of heating coils from a broken
2000W caloventor that measure 50Ω and 51Ω.  There’s a hair dryer in my
electronics box for the same reason, but I haven’t taken it apart yet,
so I don’t know what the resistance of its coils is.

Circuit design and expected precision
-------------------------------------

The way I have in mind to measure their resistance is with a 4-wire
connection, a capacitor, a known resistor, and a microcontroller.
Once the capacitor is discharged, I’ll lift one end of it up to 5
volts with the microcontroller.  Its other end will initially also be
at 5 volts, but it’s in series with a known current-sensing resistor
of the same order of magnitude as the lightbulb, maybe 470Ω.  That
resistor is grounded through wire 1, the lightbulb, and wire 2.  Wire
3 goes from an analog input to the high side of the lightbulb; wire 4
goes from another analog input to its low side.

By measuring the current through the current-sensing resistor and,
through wires 3 and 4, the voltage across the lightbulb, we can
measure the resistance of the lightbulb with reasonably high
precision.  By sweeping that current across a wide range as the
capacitor discharges, we can cancel out any thermal EMFs that exist in
the four wires or other connections; we can isolate the purely ohmic
part of the resistance.  It also allows us to average out ADC
imperfections over much of the ADC range, I think.

The capacitor itself doesn’t matter very much, though it would be
convenient if it’s ceramic rather than electrolytic so we can use it
to drive currents in both directions.  If we want a time constant of a
millisecond with 400Ω of resistance, we need over 2.2μF, which
probably rules out non-electrolytics.  Also it might mean you need to
carefully discharge it before you yank it back to ground to avoid
injecting -5V on your ADC pins.

Here’s [a circuit.js simulation][2], showing the four microcontroller
analog input pins as voltmeters:

    $ 1 0.000005 0.8729138363720133 50 5 43 5e-11
    R 160 112 128 112 0 2 220 2.5 2.5 0 0.5
    c 160 112 240 112 0 0.0000022 3.767167796170537 0.001
    r 240 112 240 192 0 470
    p 288 112 288 192 3 0 0
    w 240 112 288 112 0
    w 240 192 288 192 0
    r 240 192 240 272 0 3.21
    x 276 238 445 241 4 12 wire\s1\s(unknown\sresistance)
    r 240 272 48 272 0 2.7
    r 240 272 240 336 0 328.2
    x 296 309 447 312 4 12 improvised\stungsten\sRTD
    r 240 336 48 336 0 2.2
    p 48 272 48 336 3 0 0
    r 240 336 240 416 0 3.3
    g 240 416 240 448 0 0
    x 280 381 449 384 4 12 wire\s2\s(unknown\sresistance)
    x 50 247 219 250 4 12 wire\s3\s(unknown\sresistance)
    x 50 359 219 362 4 12 wire\s4\s(unknown\sresistance)
    r 0 272 0 336 0 5000000
    w 0 272 48 272 0
    w 0 336 48 336 0
    x -65 310 -18 313 4 12 leakage
    o 3 2 0 4546 2.5 0.1 0 2 12 0

[2]: http://tinyurl.com/ymazwaps

If we have a single-channel single-ended 15ksps 10bit ADC, all this
gets a bit annoying because of the time lag, but supposing we get 8
bits of precision and can measure the four relevant voltages at 4ksps
each, we get on the order of 262144 resistance measurements per
minute, adding 9 bits to our precision; so we have a resistance
measurement with 17 bits of precision, precise to one part in 131072,
±4 ppm.  That’s about a hundredth of a degree, but probably larger
systematic drifts will swamp it.  A nicer ADC like the 2-channel
12-bit 1Msps one found on the STM32F103, or one with differential
inputs like the ATTiny45 has, could improve this significantly, like
by another 6 bits.

Systematic error that doesn’t drift is only a problem insofar as you
can’t calibrate your thermometer, which is something you have to do
anyway because you don’t know enough about your filament to know what
resistance it ought to have at 20° or whatever.  And error that
oscillates or is random during the period of a measurement will be
strongly attenuated by averaging over multiple measurements.

A particular kind of systematic drift that is likely to be a problem
is movement of the tungsten filament inside the bulb.  As it heats up
and cools down, the filament and its supports will have different
thermal expansion, and it’s possible that a vibration will suddenly
release the resulting strain.  Strain also affects resistivity.  As I
understand it, purpose-built RTDs are constructed to ameliorate this
problem.

Another kind of systematic drift happens as your reference
current-sensing resistor heats up or cools down, changing its
resistance.  With a precision resistor, this will be a small effect,
and one you may be able to correct for by measuring the resistor’s
temperature, or at least the temperature of the thermometer instrument
that has the resistor in it.

With the above design, the current through the precision resistor is
not too large, like averaging around 3mA, dissipating 4mW.  If we take
150°/W as the thermal resistance of an 0402 resistor package, this
might raise its temperature by as much as 0.6°.  Since this is much
smaller than the device’s intended operating range, at least if the
humans are going to use it, it’s nothing to worry about.  If it were
passing 10mA or more it would become a problem.
