5 years ago nobody had heard of covid (the big disease worry was
cholera in Yemen), the war in Ukraine had pretty much ended
(Eurovision was in Kyiv that year), Donald Trump had just become
president of the US, the big humanitarian crisis was potential famine
in Nigeria and East Africa, the UK was part of the EU, the big worry
in US-Russian relations (the Intermediate-Range Nuclear Forces Treaty
was still in effect) was the US blowing things up in Syria (where
Daesh was operating freely; Daesh also controlled Mosul until July),
the US puppet regime still ruled Afghanistan, Montenegro had only just
joined NATO, Jeremy Corbyn led Labour in the UK, North Korea did its
first ICBM tests, the humans detected gravitational waves for the
first time, Catalonia declared independence from Spain, Robert Mugabe
still ruled Zimbabwe, the humans had never observed an interstellar
object, monkeys had not yet been cloned, the Falcon Heavy had not
flown, the presidency of China and the leadership of the Kazakh
Security Council had term limits, people outside Russia hadn’t heard
of “novichok”, the US was still part of the Iranian nuclear agreement,
there was no GDPR and no cookie acceptance nagbanners, abortion was
illegal in Ireland and Argentina, the US was still part of the UN
Human Rights Council, marijuana was illegal in Canada (and selling it
was illegal everywhere except Uruguay), women weren’t allowed to drive
in Saudi Arabia, Eritrea and Ethiopia were still at war, the press
still loved Elon Musk and he’d never publicly called anyone a “pedo
guy”, no company had ever had a market cap of US$1T, Greta Thunberg
was still a private citizen, gay sex was still a crime in India,
Bolsonaro had never been elected president, no genetically edited
humans had been born yet, the Yellow Vests hadn’t yet appeared in
Paris, less than half of the world’s population were using the
internet, Ingvar Kamprad still ran IKEA, Stephen Hawking was still
alive, the US hadn’t yet imposed punitive tariffs on Chinese solar
panels, NAFTA was still in effect, the Armenian Revolution hadn’t
happened yet, Hong Kong still retained its pre-handover protections
for human rights, the 02019 uprisings across the Middle East hadn’t
happened yet, Cuba still had its Castro-era constitution, the
president of Chile was a Socialist, CRISPR had not yet been publicly
used on a human, the Notre Dame hadn’t burned, and Venezuela’s civil
war with US-backed Juan Guaidó hadn’t started

A lot can happen in 5 years.  It may not, but I’d expect the next 5
years to be more exciting than the last 5 years.

Oh, and Assange was still enjoying asylum in the Ecuadorean embassy.


13:46 < L29Ah> muurkha: what would you consider a more effective strategy for living in a libertarian country?
14:00 -!- Malvolio [~Malvolio@idlerpg/player/Malvolio] has joined #hplusroadmap
15:33 < muurkha> I don't know of an effective strategy for that, since nobody has managed to do it yet
15:34 < muurkha> but in general effective strategies in politics seem to be variants of "let's you and him fight"
15:35 < muurkha> the people who actually do the fighting — infantry troops, voters, members of Parliament, etc. — never win
15:35 < muurkha> it's the people who manipulate them into fighting who win
15:37 < muurkha> if you can manipulate all the non-libertarians into fighting with each other instead of with libertarians, and keep the 
                 libertarians from wasting energy on fighting (with each other or anyone else), the libertarians will probably win
15:40 < muurkha> but you have to keep in mind that any political faction who sees libertarians as significant potential opposition will 
                 work hard to divert the libertarians' energy into fighting — ideally, with each other, but at least with some other 
                 group.  right now the wokists are probably the most promising target; if the conservatives can waste the libertarians' 
                 and wokists' energy by getting them to 
15:40 < muurkha> fight one another, they can put their policies into effect, as they recently did in the US by striking down the Roe v. 
                 Wade prohibition on abortion
15:42 < muurkha> right now in the US anyone with even vaguely radical or socialist sympathies is freaking the fuck out about "fascists", 
                 and they identify libertarians (or "libertarians") as fascists; as in Ukraine, some of the libertarians oblige by using 
                 fascist rhetoric and symbology or even shifting away from libertarian policies toward fascist policies
15:44 < muurkha> the US is a good distance down the slippery slope toward a Cultural Revolution, and if that happens the victors will be 
                 Xi, Putin, and nobody in the US
