02022-01-15
-----------

I just glanced at some scrapped boards I have sitting around for a
while, things I'd picked up on the street.  One of them, evidently a
power supply (“MagneTek 3722-40-1”), has a 470uF 450V storage cap (50
joules!), some beefy diodes, and a row of six giant IRFP450
transistors.  I looked these up and they turn out to be N-MOSFETs
rated for 500V, 14 amps, 0.4 ohms, and 190 watts, that switch in about
150 ns.  So in theory one of them can switch 7 kW at 3 MHz while
dissipating 78 W if properly heatsinked.  I feel that this could be
useful for things like induction heaters if I can manage to desolder
them; although four of them are on the high-voltage side of the train
tracks, they're still attached to fairly large pours.

(14 A in 150 ns is 90 megaamps per second, relevant for driving coils.
But I think you can get a sharper spike from an avalanche pulse
generator, which can produce subnanosecond rise times with everyday
transistors.)

The board also has a row of five of what look like 6-pin
optoisolators, TCDT1102G.  Googling the datasheet confirms: 5000V
isolation test voltage, anode on pin 1, cathode on pin 2, collector on
pin 5, emitter on pin 4, max 60 mA, max 32 V bias and 50 mA on the
output (200 nA leakage at cutoff), 1.25-1.6V LED voltage, 50 pF input
junction capacitance, 110 kHz cutoff frequency, and at 5 mA, 4 us
delay time, 7 us rise time, 6.7 us fall time, 0.3 us storage time.

There are lots of beefy diodes, and lots of power resistors, including
several marked as .005 ohms 1%.

There are also what I thought were some other smaller (TO-220?) power
transistors: an IRF 32CTQ030 (30V 30A schottky common-cathode diode?
I see now that it's marked D9, not Q9 as I thought) and an ST
L4940V12, which is actually marked U10, not Q10, and turns out to be a
1.5-amp LDO (0.45V) which comes in 5-, 8.5-, and 12-volt versions.  It
can handle up to 17 volts input, is specced at ±¼V (2.1%) at 500mA,
±½V at 1500mA, and is internally limited to 2-2.7A output and thermal
overload so it's safe to short its output.  But you're supposed to
bypass it with 0.1uF on input and 22uF on output.

Aside from a 12-volt power supply, I feel like you could maybe use
this chip as a “transistor” for a super-high-gain, high-precision “PNP
emitter follower” with an inconveniently large Vbe of 12 volts.  But
from the datasheet I can't tell how high the gain is (I'm guessing 50
dB) or what the bandwidth is.  Also you could use it as a
temperature-compensated voltage reference, especially if you calibrate
out its 2% rated error.

There are a couple of other smaller transistor-shaped ICs (TO-92?) on
the board, U11, U25, and U15.  Plausibly the part number on all three
says “KA 431AZ”, though it's hard to read.  This matches the marking
diagram in the datasheet for the ON Semiconductor KA431A
three-terminal adjustable regulator, which will give you anything from
2.5 V to 36V when programmed with a resistor divider (to divide the
output down to 2.5V) and can sink up to 100 mA (1 mA minimum).  The
datasheet gives examples of using it as a shunt regulator (alone, on
the reference pin of a 7805, or with an outboard PNP amp), as a
current source, and as a current sink.

From a different point of view, the KA431A is an analog comparator
with a 2.5-volt offset (2.47-2.52 are the spec limits), an
open-collector output, and inconveniently shunting its output voltage
onto its inverting input.  I feel like, with some ingenuity, you could
use this in place of an opamp in a lot of places.

I presume that the larger chips on the board are less interesting
because less versatile: an ST L4981A, a Motorola (?) UC3845BN, which
at long last has a decipherable date code: 9809, 9th week of 01998, a
National (?) S9806AD (?), and a PCF8574P whose trademrk I don't
recognize at all.

The L4981A turns out to be a power factor correction chip with a
100-kHz oscillator set up to produce a variable PWM signal to drive an
onboard Darlington/DMOS 1.5-amp totem-pole gate driver for MOSFETs,
with lots of interesting signals brought out including a ±2% 5.1-V
voltage reference that can deliver up to 10 mA (internally current
limited).  The datasheet seems to be from 02001, so I wonder if I was
misreading the “9809” date code on the other chip.

If I'm understanding correctly, in a sense this is a class-D power
amplifier with a differential output and a large open-loop gain
(70-100 dB), with built-in voltage regulation (12-19 volts input), and
also an analog multiplier.  I feel like you could probably use this
chip to efficiently drive a speaker directly (better, through an RC
filter and a DC-blocking cap) at about 15 watts.  I might be wrong
about that because the datasheet is super confusing.

The Onsemi UC3845B is billed as a “high performance fixed frequency
current mode controller” and seems to be sort of a cut-down version of
the L4981A: a high-current (1 A) totem-pole gate driver, a 250kHz
oscillator generating a 0-50% duty-cycle PWM signal, a ±1%
temperature-compensated (voltage) reference, a current-sensing
comparator (which senses voltage actually), and a high-gain error
amplifier with its non-inverting input connected to an internal
2.5-volt voltage reference.  But it's a much simpler chip.  The idea
is that you build an SMPS with this chip by letting it sense the
inductor current with a sense resistor to ground; it slams the switch
shut in each cycle as soon as the current reaches the desired level
(some voltage level divided by whatever sense resistor you chose).
This voltage level, and thus the inductor current, is set by the
voltage on pin 1.  Its ±1% reference is 5.0 V and can source 20 mA
(!), and it runs off anywhere from 12 to at least 25 and maybe 36
volts.

So you can *definitely* use *that* as a class-D speaker driver in the
way I was suggesting.

The S9806AD has a sticker over the part number saying “IBM 400 E2”,
but I can't find any information about it.  It's a large DIP, probably
the central controller for the whole board, right next to the
“PCF8574P”.

There's a “PCF8574” Philips introduced in 01994 and still active as of
02013; it comes in a 16-pin DIP like this one that says “PCF8574P” on
it.  Also the logo I didn't recognize is actually a Philips logo.  As
it turns out, it's an 8-pin I²C I/O port, so it's actually *extremely*
versatile!  You hook up two of its pins to your 100kHz I²C bus and
suddenly you have 8 more I/O pins, including an open-drain interrupt
line.  It only uses 2.5 uA, runs on 2.5-6 V.  Instead of standard CMOS
totem poles, the I/O pins are “quasi-bidirectional”: if they output
“high” it's a 100-uA pullup (plus a temporary 1mA “accelerator” to
speed up transitions), while the “low” output can sink 25 mA.

These seem to still be around (Digi-key has them in stock from both
NXP and TI, which seems to second-source them since 02008) but I guess
they didn't really take off because they cost US$1-2, a high enough
price that you might as well just use a second microcontroller.  Maybe
it's because they're sort of inherently slow.

Oh, as it turns out, there's a bunch of surface-mount chips on the
back of the board!  One is in the high-voltage zone, an LM393D with a
logo I don't recognize, and in the low-voltage zone there are three
KA339Ds (originally I thought this was “KA3390”), another LM393D, a
Philips A8581CT, and three of what I think are an UC3902 (they say
9801 first but I think that's a date code).

The LM393 is a dual analog comparator chip: 2-36 V, typical 1 mV 3 nA
input offset, typical 400 uA power consumption, sensing down to
ground, with a 16 mA open-collector output, 200 V/mV gain, 1300 ns
response time.

The “KA339D” might be the old Fairchild KA339 quad analog comparator.
It’s pretty much identical to the LM393 but quad instead of dual and
an order of magnitude worse input offset current.

The NXP PCA8581C is a 128 × 8 bit I²C 8-pin EEPROM.  It has its SCL
and SDA pins wired to the penultimate and antepenultimate pins of the
PCF8574, which are its SCL and SDA pins, so I’m pretty sure I’ve
identified both chips correctly.

TI sells an 8-pin “UC3902”, apparently introduced in 02002, which is
for paralleling power supplies; the idea is that it adjusts all the
power supplies’ voltage feedback to ensure they supply the same amount
of current.  I suspect these UC3902s are a second source.

02022-08-25
-----------

I bought a couple of “Alic ECO A60” 14-watt 1300-lumen LED lightbulbs
today for I think AR$390 each (US$1.40 or so).  These are imported
from China.  The LED lightbulbs I’ve been buying leave a lot to be
desired when it comes to reliability; they run so hot that they stop
working in less than a year, despite great claims of longevity: this
package claims “15 años de vida util\*\*.  \*\* Vida útil promedio en
base a un uso de 1000 hs/año (2.7 horas p/día) ... con buena
circulación de aire.”

I popped off the polycarbonate frosted dome with a spoon.  The ones
I’ve popped open in the past have a parallel pair of current-sense
resistors which they use to regulate the LED current (I thought I’d
made some notes on this but I can’t find them), but this one
unfortunately has a single 15-ohm current-sense resistor.

The circuit is very simple: the line in goes to a bridge rectum-fryer
chip labeled “DB1 NO20195A” on the board, then to a through-hole
electrolytic capacitor mounted on the other side of the board (C1)
with a series pair of 300kΩ SMD bleeder resistors across it, a series
string of 15 white LEDs, and then a three-pin current regulator chip
labeled “U1 1431P” on the board, which has the sense resistor (labeled
RS1) next to it.  One pin of the current regulator is connected to the
diode string, and the sense resistor goes between the other leg and
“ground”, by which I mean the negative side of the capacitor.

(The chips have markings on them as well, but they’re too small for me
to read without assistance anymore.)

My multimeter measures the diode string at 258 V, which is probably
about right, though it would suggest that there are about 5 3-volt LED
chips in series inside each of the 15 LED packages.  When I
accidentally shorted the two non-ground pins of the current regulator
with a shaky multimeter lead, the LEDs brightened up noticeably but
did not burn out; so presumably the total voltage across the capacitor
is close to what you’d normally infer from this arrangement, about 340
volts, with the remaining 80 or so volts dropped across the current
regulator itself.

14 watts at 340 volts would be 41 milliamps, and if all of that is
flowing through the sense resistor it would be 0.62 volts, the Vbe of
a bipolar silicon transistor.

The rating on the package says it’s designed for 175–250 VAC; 175 V
RMS would give you a peak voltage of 247 V, so that’s probably the sum
of the turn-on voltages of all the diodes in the series string, while
they resistively drop an additional 11 volts at full power.

What I would like to do with this bulb is reduce it to a 7-watt or
4½-watt bulb by changing the sense resistor, on the theory that its
heatsinking is probably marginal at its rated wattage but might be
adequate at such a much lower wattage.  This is very easy for bulbs
where the sense resistor consists of two parallel resistors — you can
just break one off the board — but in this case I think I need to
solder on another resistor, something in the 22–47 Ω range.  I
anticipate this being difficult because the PCB is presumably aluminum
with a thin insulating layer of epoxy separating the aluminum from the
traces.  (And I don’t have a thermostatically controlled soldering
iron, just the obsolete kind.)

The second lightbulb appears identical inside.  My plan is to hack one
of them and then install them in different light sockets switched by
the same switch, so that I can observe the difference in both
brightness and longevity.

Unfortunately I seem to have misplaced my collection of various values
of resistors, so I guess I won’t be doing this tonight!
