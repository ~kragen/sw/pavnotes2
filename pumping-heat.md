I've been thinking about how to move around kilowatt quantities of
heat at around room temperature.  Specifically I'm thinking about a
solar-powered liquid-desiccant air conditioner with TCES energy
storage, as has been demonstrated by a number of workers.

Such a system can provide indoor heating, cooling, and humidity
control, as well as dehydration for food and moisture harvesting for
potable water, even during a power outage; and it can run mostly on
low-grade heat from, for example, a medium-temperature solar thermal
collector.  It eliminates the most costly parts of a conventional air
conditioning system (the compressor and high-pressure plumbing) and
substantially reduces the airflow necessary, thus further reducing the
necessary fans and system expense, as well as the noise.  However, it
is probably larger than a conventional air conditioner.

Overall system design
---------------------

Essentially this is similar in proportions and exergy consumption to
three swamp coolers.  It also contains a heater, four optional
intercooler heat-exchangers (two which have additional optional swamp
coolers), and an optional TCES energy storage reservoir.

A swamp cooler is essentially just a packed bed used for mass transfer
rather than catalysis, in which mass transfer occurs between air and
water, plus pumps for the air and the water.  Conventionally the
"packed bed" is "packed" with wood wool (thin wood shavings,
"excelsior"), so it's more like a fiber bed.

### Process flow ###

The final swamp cooler takes dehumidified air from a desiccator,
described below, and evaporatively cools it in the usual way swamp
coolers do, then blows it into the living space.  Measures must be
taken to prevent pathogen proliferation (fungi, Legionella) in this
cooler, and a HEPA filter in the airstream is also a good idea.

The water evaporating into the air to cool it need not be super pure;
ordinary well water or municipal water is fine.  But it should not be
recirculated through the beds until evaporation; after concentration
by a factor of 10 or so it should be drained off to avoid depositing
unwanted solids on the beds.

#### Desiccant circuit ####

The other two "swamp coolers" form the desiccant circuit, but both of
them actually heat the air instead of cooling it.

The first "swamp cooler", the desiccator, exothermically condenses
water from the intake air with a liquid desiccant.  The diluted
desiccant is then heated and fed into the second "swamp cooler", an
evaporator, which allows water to evaporate from the hot desiccant
into the outdoor air, thus "regenerating" the desiccant, before being
fed back into the first "swamp cooler".  Thus, the desiccant circuit
transfers humidity from the air to be cooled into outdoor air.  The
desiccator heats the air on the way through it, and the evaporator
probably does as well; the exhaust air output temperature is close to
the desiccant temperature on the output from the evaporator.

If the desiccant cools down too much in the evaporator due to too
prolonged air contact, it will tend to reabsorb water from the air
that is nominally being used to regenerate it, thus becoming less able
to desiccate upon recirculation, so there is a nonmonotonic
performance curve for contact time in the evaporator.  No such
nonmonotonic performance curve exists for the desiccator.

The heating stage before the evaporator can be powered from any source
of heat that reaches a temperature high enough to regenerate the
desiccant being used: solar heat, waste engine heat in a car or
similar machine, biomass heat, natural gas, a propane tank, an
electric heating element, a nuclear reactor, etc.  Many desiccants are
available that can regenerate at convenient temperatures like
80°–200°.

By operating the evaporator at a higher temperature, you can use a
much lower volume of air for the evaporator than the volume of air the
system cools.  Indeed, at temperatures over 100° (or a little lower at
high altitudes), the evaporator doesn't inherently need any air input
at all; its output can be pure steam.

As an alternative to heating the desiccant on its way into the
evaporator, you can heat the air on its way into the evaporator, as is
commonly done in wheel regenerator systems.  Heating the liquid
desiccant is probably easier, but heating the air has some significant
advantages:

1. The air volume can be increased until it has enough heat in it to
   regenerate the desiccant to any desired degree without changing its
   temperature.  Doing this by heating the desiccant separately may
   require recirculating it through the heater and evaporator multiple
   times.
   
2. The air can withstand higher temperatures than the desiccant
   without damage.  Organic desiccants degrade rapidly above 200° and
   even inorganic desiccants can crystallize if overheated.  Air is
   already in its lowest energy state and so is not damaged by
   heat.
   
3. If the desiccant does solidify due to overheating, this is much
   less catastrophic in the packed bed. XXX

Some filtration in the desiccant circuit is likely needed to keep dust
from building up in the liquid, and filtering the incoming air may
also be a good idea.

#### Intercoolers ####

An optional outdoor-air-cooled intercooler heat exchanger on the
desiccant circuit permits the desiccant entering the desiccator to be
cooler than the desiccant exiting the evaporator.  This heat exchanger
can optionally itself be cooled with a fourth fresh-water swamp
cooler, [as demonstrated on the YouTube channel Tech Ingredients][11],
to cool the desiccant to the dewpoint of the outdoor air rather than
merely its temperature.  Without this intercooler, there is a painful
tradeoff between the desiccant temperature at the evaporator exit
(ideally high) and its temperature at the desiccator input (ideally
low).

[11]: https://youtu.be/R_g4nT4a28U

Because the desiccator heats the air, a second optional
outdoor-air-cooled intercooler cools the desiccated air in the
indoor-air circuit to the temperature of the outdoor air (or, again,
its dewpoint if evaporatively cooled).  This air intercooler could be
an air-air heat exchanger (recuperative or regenerative) or two
air-liquid exchangers linked by a liquid coolant in a closed loop.
The liquid coolant would allow you to locate the desiccator, the final
swamp cooler, and the indoor-air side of the intercooler all indoors,
avoiding any need to pierce a wall with a bulky air duct.

This second intercooler may be worthwhile as a cooling apparatus on
its own, without any of the desiccant apparatus, because it has three
advantages over a regular swamp cooler.  First, it doesn't add water
content to the indoor air, though cooling the indoor air will increase
its relative humidity.  Second, because the mass exchange between
indoors and outdoors takes place through a room-temperature,
low-pressure liquid coolant instead of air, it can pass through thin,
uninsulated, flexible hoses rather than a large air duct (as with
portable air conditioners and swamp coolers) or rigid insulated copper
tubing (as in split air conditioners), so no big holes in the wall are
needed, and the cooling can be provided wherever it is convenient.
Third, either the indoor or outdoor heat exchanger can take whatever
form is convenient, such as a fountain.  As we will see below, the
bulk of the cooling available is attributable to evaporatively cooling
this intercooler.

The additional evaporative coolers used to cool the intercoolers can
use even fairly impure water; the Youtuber suggested seawater.

#### Distillation for moisture conservation and harvesting ####

Though it's irrelevant here in Buenos Aires, in the case where such
high water consumption is unacceptable, the humid air from the
evaporator can be run through a third optional intercooler to cool it
to outdoor-air temperatures, then a fourth intercooler that cools it
with the indoor-destined air from the final swamp cooler, thus
condensing most of the water out before finally exhausting the air.
(These two intercoolers cannot usefully use evaporative cooling.)  The
resulting distilled water can then be used for distilled-water
purposes or merely fed into the final swamp cooler to reduce its water
consumption.

Such water conservation reduces the efficiency of the overall system
dramatically, but though it may sound like a perpetual-motion machine,
it is not; it is rejecting heat to the outdoors at the high
temperature produced by the desiccant's absorption of water and then
absorbing it from the indoors at the lower temperature produced by the
evaporative cooling of the air.

Whether this results in a net gain or loss of water depends on whether
the remaining cooled evaporator-exhaust air contains more or less
water than the outdoor air that enters the evaporator.  Under
plausible conditions this system works to harvest water vapor from the
outdoors and produce distilled water.

#### Reservoirs ####

Reservoir tanks for saturated and regenerated desiccant decouple the
operation of the desiccator and the evaporator, allowing the
desiccator to operate usefully when the evaporator is not operating
and vice versa.  Unlike the reservoirs in a sensible heat storage or
latent heat storage system (PCM TES), these reservoirs do not need
thermal insulation to retain their stored energy, though it may be
useful to insulate them to prevent them from heating up when it's hot
outside.

If not insulated, the regenerated desiccant reservoir can itself serve
as the desiccant-circuit intercooler.

Here in Argentina, it's common for power outages to result in local
water outages as well; water is stored in roof-mounted storage tanks
to which it is pumped by electrical centrifugal pumps, often mounted
at ground level on a per-building basis.  Blackouts and sometimes
brownouts stop these pumps from running, so water runs out not long
afterwards.  (We just had a brownout as I was typing this that turned
Mina's computer off, and even now that it's recovered, the voltage is
only 206 volts according to my multimeter.)  If such a TCES system is
to be used to provide air conditioning during power outages, it will
need to also have a reservoir of water to be used for the evaporative
cooling.

#### Portable cooling ####

In a portable device, similar to an ice vest, the evaporator might
even be disconnected for a period of time; for example, it could be
left at home, while the desiccant tank is carried around in a
backpack.  Such a system might be able to dispense with all of the
final swamp cooling stage, instead merely circulating uncooled dry air
under a vest to evaporate the wearer's sweat (in the process
distilling the sweat out of the evaporator, evoking science-fiction
tropes like _Dune_'s "stillsuit"), or it might use the evaporatively
cooled air to cool a liquid coolant which could then be more easily
circulated through the vest, or a combination.

#### Flow flexibility ####

The incoming air for the desiccator can be sourced from indoors, from
outdoors, or a mix.  By controlling the relative flows in the
desiccant circuit and the final swamp cooler, the humidity of the
outgoing air can be adjusted to be higher or lower than that of the
incoming air.  By disabling the final swamp cooler (and the
intercoolers, if present) the system also serves to heat the air.

The hot, dry air directly from the desiccator can be useful in itself,
for example for preserving fruit, making raw-foodist flaxseed
crackers, and dehydrating garbage or sewage.  You could imagine a
house with one or more hot-dry-air sockets in the wall, like those
used by central-vacuum-cleaner systems, where you could plug in a
hot-dry-air appliance.  The output air from the appliance could be
routed into the evaporator, where heat and contact with the desiccant
will largely sterilize it, but at the potential cost of contaminating
the desiccant.

### System sizing ###

In desiccant-based air conditioning, the air is first dehumidified
with the desiccant, then (ideally) cooled to the ambient outdoor
temperature, and then rehumidified to cool it.  So, if the humidity is
the same, the amount of water that goes into the desiccant is the same
amount of water that goes into the air.  Water's enthalpy of
vaporization is 2257 kJ/kg at its boiling point and about 10% higher
at room temperature.  Thus each kg of water we evaporate to cool the
room air removes 2500 kJ and must eventually be absorbed into the
desiccant; if we are providing 4000 W of cooling, reasonable for a
smallish apartment, we must evaporate and desiccate about 1.6 g/s, 140
kg/day.  The quantity of desiccant required may be up to an order of
magnitude larger, depending on the desiccant-to-roundtrip-water ratio.

If our desiccant is being cycled through once a minute, we could get
by with only a kilogram or so of desiccant.  But the "storage" part of
TCES is precisely that sometimes we are running on stored desiccant
from the reservoir.  For example, right now it's 25.8° outside, which
is about 2° too hot for me, but it's 5:00 AM, so there hasn't been any
sun for almost 8 hours.  Handling that situation at a household scale
probably requires several hundred kg of stored, dry liquid desiccant
to absorb the 140 kg per day of water.

By contrast, a portable device like those described earlier could be
useful at much lower wattages than a house-cooling device, because a
person normally dissipates only about 100 watts, which is only about
40 mg of water per second.  So a system with 8 hours of autonomy would
need to absorb only about 1.2 kg of water in a few kg of desiccant.
This is comparable to the weight of an ice-based ice vest and vastly
smaller than the weight of a battery-based personal cooling system.
Unlike ice vests, the desiccant-based cooler requires minimal or no
insulation and can retain its stored energy for years when turned off.

Because the heat flow in the air and water flows of the system is
similar, but air has a specific heat of about 1 J/g/K and a density of
about 1.2 mg/cc, while water is 4.2 J/g/K and 1000 mg/cc, the volume
of air flowing through the system will necessarily be several thousand
times the volume of the water.  This means that the vast majority of
pumping power and ducting will be devoted to air rather than water,
despite water's higher viscosity.

This also means that the dimensions of the thing where mass exchange
and heat exchange happen between water and air probably need to be
determined primarily by the space necessary to permit adequate airflow
without undue head loss and thus exergy consumption; in standard swamp
coolers, this requires passing air through panels on the order of 1 m²
in area but only a few cm thick, traditionally made of excelsior but
nowadays sometimes made of honeycomb cardboard instead.

### Basic performance calculations: 1° to 22.5° of cooling, depending ###

Suppose the output temperature of the indoor side of the contraption
is 20°.  At 20°, air can hold 15 grams of water per kg of air, a
capacity which doubles every 12°, so that at 80°, it holds 500 grams
of water per kg.  If we suppose that the desiccator has reduced the
air to what would be 40% relative humidity at 20° (saturated muriate
of lime can reach 33%), which would be 6 g/kg, and the output air can
be 50% RH (optimal for comfort according to ASHRAE), or 7.5 g/kg, then
we can dump 1.5 g/kg of water into the air to cool it, which is about
3800 J/kg, a drop of 3.8°, which means we can reach 20° output only
from a temperature of 23.8° or lower going into the evaporative cooler
(ignoring the insignificant contribution from the thermal mass of the
water itself).

Without an intercooler at all, this 23.8° is the temperature of the
air coming out of the desiccator, so the input air (drawn, presumably,
from indoors) might be 21°–23°, providing almost no cooling.  With a
passive intercooler with no evaporative cooling that cools to the
outdoor air temperature, we can reach 23.8° if the outdoor air
temperature is lower than that, perhaps 23.5°.  But with an
evaporatively-cooled intercooler, we can reach 23.8° if the outdoor
air *dewpoint* is 23.5° or so.  For example, if the outdoor air
temperature is 30° with 70% RH, or 35° with 55% RH, or 40° with 40%
RH, though in that last case you'd probably just use a regular swamp
cooler.  The evaporatively-cooled intercooler can be considerably
smaller than the purely passive intercooler, too.

So, at 35° with 55% RH you'd be getting 11° of cooling from the
outdoor evaporative cooler and then a crucial final 4° of cooling from
the indoor desiccant circuit.

Picking a marginally acceptable output target of 24° and 55% RH, we
find that the moisture capacity of the air is 19 g/kg, so 40% RH would
be 7.6 g/kg, 55% RH would be 10.45 g/kg, the delta is 2.85 g/kg, and
we can get a 7.1° temperature drop, achievable from a starting point
of 31.1° or below: either 31.1° desiccator output temperature with no
intercooler, outdoor temperature with a passive intercooler, or
outdoor dewpoint with an evaporatively cooled intercooler.  A 31°
dewpoint might be 37° with 70% RH, 43° with 50% RH, or 47° with 40%
RH.  At 43° you'd be getting 12° of evaporative cooling from the
outdoor evaporative cooler plus 7° of cooling from the desiccant
circuit.

I've assumed above that you can only reach about 40% room temperature
RH at the output of the desiccator, not the 33% equilibrium of muriate
of lime, both because of the short residence time and because of the
heating of the air as it desiccates.  But this may be too pessimistic,
particularly if some degree of countercurrent mass and heat exchange
can be arranged with cooled desiccant.  And the results are very
sensitive to this: every extra percentage point of RH at 22° is 170
mg/kg of water in the air, which works out to about 0.4° more cooling.
At higher temperatures the difference is exponentially greater.  If we
could get all the way down to 33% at 24°, instead of 2.85 g/kg and
7.1°, you'd get 4.2 g/kg and 10.5° of cooling on the indoor circuit.

The fact that most of the evaporation happens on the outdoor
evaporator suggests that, for 4000 W of cooling with an intercooler,
you actually won't need to evaporate 1.6 g/s indoors or move the
corresponding 420 l/s of air; you might be able to get by with as
little as a third of that, with corresponding reductions in the amount
of required desiccant storage, desiccant flow, and desiccant
regeneration power.  However, I'm still doing the rest of my rough
calculations in this note with the more pessimistic numbers.

Even though it doesn't contribute the majority of the cooling, the
desiccation cycle is still useful for comfort because, if the indoor
air has the same water content as the outdoor air, cooling it to the
dewpoint of the outdoor air increases its relative humidity to 100%,
which will be uncomfortable at any temperature.

Aqueous and other solutions
---------------------------

We can move energy around as the sensible heat of water, but also as
the latent heat from hydrating the liquid desiccant, and in some sense
also as the enthalpy of vaporization of water.  We need to deal with
some amount of water being present in most liquid coolant systems
because the air is full of water and is either in contact with the
coolant or can diffuse at some rate through walls into the coolant,
but this is especially true for liquid desiccants, whose primary
function is to absorb water from the air.

To be quantitative, 120 liters per hour (2 l/minute, 33 ml/s, 33 g/s)
requires 0.13 W if against a 400 mm head, but if all of that water
evaporates after being pumped wherever it's going, it sucks up 80 kW
of heat, and standard swamp coolers do succeed in vaporizing a
significant fraction of the water on each recirculation through the
evaporation pads, so 40 kW is probably realistic.  If instead the
water is carrying only 20° of sensible heat (20 kcal/kg) it's 2800 W.
In the case of carrying a desiccant, the power thus conveyed is
probably in between these two limits, depending on the desiccant.

To provide our desired 4000 W of evaporation, we probably only need
about 3 ml/s (12 liters/hour) in the final swamp cooler and a few
times that in the desiccant circuit, perhaps as much as 15 ml/s.

### Desiccant choice ###

Suitable desiccants might be an aqueous solution of some mixture of
the following:

- anions:
    - muriate
    - sulfate
    - phosphate
    - nitrite
    - acetate
    - formate
    - hydroxide
    - citrate
    - carbonate
- cations:
    - sodium
    - potassium
    - ammonium
    - calcium
    - magnesium
    - zinc
    - choline
    - ferric
    - hydronium
- nonionic, nontoxic hygroscopic solvents:
    - ethanol (b. p. 78°)
    - isopropanol (b. p. 83°)
    - urea, as an ingredient of a deep eutectic solvent (thermally
      decomposes hazardously at 133°)
    - propylene glycol (b. p. 188°)
    - dimethyl sulfoxide (b. p. 189°)
    - formamide (b. p. 210°, but normally thermally decomposes at 180°)
    - dipropylene glycol (b. p. 231°)
    - methylsulfonylmethane (b. p. 248°, melting at 109°)
    - glycerin (b. p. 290°)

In particular the muriates of calcium and magnesium and the sulfate of
magnesium are widely used as desiccants, and the 1:1 mix of the
muriates, sometimes called calcium tetrachloromagnesate, reportedly
works even better for TCES purposes; [the binary system has been
intensively studied for decades][47].  [Muriate of magnesium has 33.1%
deliquescence humidity][46], almost exactly the same as that of
calcium, decreasing to 30.5% at 50°.  These salts are also soluble to
a significant degree in ethanol and DMSO even without water, so I
suspect that adding those diluents to the liquid would allow lower
amounts of water, down to no water, and perhaps thus increased water
absorption capacity.

[46]: https://www.saltwiki.net/index.php/Bischofite
[47]: https://nvlpubs.nist.gov/nistpubs/jres/70A/jresv70An4p305_A1b.pdf

I haven't been able to find solubility data for the other solvents,
but I tossed some flakes of muriate of lime into some hardware-store
(colorless) glycerin and heated it in a sand bath to somewhere well
above 100°, at which point it dissolved the flakes over the course of
about a minute, developing many small bubbles (perhaps from boiling
the glycerin, more likely from boiling out the water from the muriate)
and a fair bit of white fumes, so at least the solubility of that
combination is non-negligible.  The resulting mixture was perfectly
transparent but a honey-like yellow-brown color, suggesting that the
glycerin was oxidizing.  It remained free of precipitates upon cooling
back down for a few hours in the freezer, probaby to at least -10°,
acquiring the viscosity of thick honey, so I added some crystals of
undissolved muriate of lime to see if they would initiate
crystallization (though they're probably antarcticite, the
hexahydrate).  It had a sweet but nasty taste.  Repeating the
experiment with quantitative measurements for the temperature and
quantities, and better control over air exposure, would be very
worthwhile.

Since the quantities of desiccant required for household cooling may
be on the order of a tonne, the cost of the desiccant itself could be
a significant factor if it is an expensive material.  Choline
chloride, for example, is sold at AR$1300 for 100 g (US$63/kg).  In
this connection it is worth mentioning the low prices of minimally
processed minerals such as muriate of calcium ([10¢][2]–[18¢/kg][3]),
muriate of magnesium ([cheaper][4] at [11¢/kg][5]), sulfate of
magnesium ([18–46¢/kg][6]), hydroxides of sodium ([15¢–75¢/kg][7]
[echemi][8] [lca-net][9]) and potassium.  Also, though it's synthetic,
urea: right now [urea is in a price spike to US$1/kg][12] but normally
it costs about 25¢/kg.  Even [phosphoric acid][0] ([China][1]) is more
expensive than these super cheap desiccants (US$1/kg), glycerin is
more like US$2/kg (though I found a vendor named Federico Sanchez dba
Supascale/Natural Biopure offering dirty brown industrial glycerin as
a biodiesel byproduct for 33¢/kg, minimum 30 kg, and other vendors
selling for similar prices), and things like [propylene glycol are more
like US$10/kg][15].

[15]: https://www.chemworld.com/Propylene-Glycol-Bulk-p/pg-275.htm "US$9,599.99 for 275 gallons of propylene glycol"

Carnallite (KMgCl3) is promising in this connection, since it's a
naturally occurring deliquescent mineral, which you would think would
make it cheap, but I think it’s normally processed into potash (and
maybe magnesium?) onsite rather than being sold.  The one cost datum
I’ve been able to find says [the mining cost was US$4.88 per metric
tonne at the National Potash Corp. mine in Carlsbad, NM][10], which
works out to US$0.005 per kg, low enough that if anybody is selling
carnallite the price is probably almost entirely the transport cost.
The problem is finding someone who’ll sell it.  Even on Alibaba all I
find is carnallite crushing machinery.

I think in general you should be able to absorb more water per unit
volume of desiccant solution by using a combination of multiple
desiccants such as muriate of lime and potassium formate, barring
reactions.  The rule of thumb is that the solubilities are independent
(e.g., adding anhydrous potassium formate shouldn’t reduce the amount
of muriate of lime that can dissolve, and vice versa), but additional
solutes do further depress water's vapor pressure, though we are
outside of the realm of Raoult's law.

[0]: https://www.chemanalyst.com/Pricing-data/phosphoric-acid-1162 "The prices of phosphoric acid rose in the US rose from USD 615/MT (January 2021) to USD 645/MT (March 2021)."
[1]: https://www.echemi.com/productsInformation/pd20150901061-phosphoric-acid.html "11166.67 Yuan/mt, about US$1760 for what I assume is a tonne"
[2]: https://www.echemi.com/productsInformation/temppid160705011137-calcium-chloride.html "101.678 USD/MT"
[3]: https://www.cs.mcgill.ca/~rwest/wikispeedia/wpcd/wp/c/Calcium_chloride.htm "Millions of tonnes of calcium chloride are made each year in the US alone, and in 1990 the bulk price there was $182 per tonne."
[4]: https://www.cga.ct.gov/2014/rpt/2014-R-0001.htm "The department also found that the magnesium chloride (1) was more effective than the calcium chloride, especially in colder temperatures, and (2) cost less (75, rather than 99, cents per gallon)."
[5]: http://www.adirondackwild.org/pdf/pdf_adk_almanack/post-23_road_salt.pdf "Sodium chloride is “cheap.” It costs $42 per ton, calcium chloride $140/ton, magnesium chloride $111/ton and the product with the least environmental and corrosive impact, calcium magnesium acetate, costs $1,500 per ton."
[6]: https://prd-wret.s3.us-west-2.amazonaws.com/assets/palladium/production/atoms/files/myb1-2017-mgcom.pdf "USGS Minerals Yearbook 2017, Magnesium Compounds [advance release March 2020 (?)], p. 46.6: 52800 tonnes of magnesium sulfate shipped in 02017 at a value of US$23.9 million, which works out to 45.3¢/kg; p. 46.8: 15000 tonnes of magnesium sulfate, other, exported in 02017 at a value of US$6.96 million, which works out to 46.4¢/kg.  The corresponding figures for magnesium chloride give 76¢/kg.  On p. 46.10 5820 tonnes of kieserite magnesium sulfate were imported at a value of only US$1.07 million, giving a price of 18¢/kg."
[7]: https://www.quora.com/What-is-the-price-per-ton-of-sodium-hydroxide "300-700 USD per tonne on alibaba"
[8]: https://www.echemi.com/productsInformation/pd20150901041-caustic-soda-pearls.html "1437.8 yuan/mt, which works out to US$226 per tonne, with prices up to US$710 reported at American factories"
[9]: https://lca-net.com/files/naoh.pdf "Long-term market reactions to changes in demand for NaOH, Marianne Wesnæs and Bo Weideman, 02006-10-19: 'This [Solvay + soda-lime] looks okay today when caustic soda sells for around $350-375 per ton in the USA and Europe, but prices in Asia are only [US]$250 per ton or less. Of course under certain circumstances the costs I have mentioned can be lower, especially in locations where energy is cheap.  But it should be remembered that today prices for caustic soda are especially high, and during a recession they can fall to $150 per ton or even less.'"
[10]: https://mrdata.usgs.gov/mrds/show-mrds.php?dep_id=10126079
[12]: https://news.ycombinator.com/item?id=29840311

Solid desiccant options might include acetate of sodium, which doesn’t
seem to be deliquescent at common humidities (although I haven’t found
a curve of deliquescence humidity yet, people reporting that it dries
in air rather than deliquescing include [degroof][51],
[indigoandblack][52], and [Zsákos Ákos][53]) but the trihydrate melts
at 58°, dehydrating and resolidifying as you go higher.  CH₃COONa
weighs 82 daltons, while three waters weigh 54 daltons, so the
anhydrous can evidently take up 66% of its own weight in water, not
too shabby, even if not up to the standard of some of the other salts.
[Apparently its deliquescence relative humidity (DRH) is around
39–42%.][55]

[Ammonium acetate][54] (spirit of Mindererus) is easily prepared from
household chemicals and reportedly deliquescent; if it has a useful
DRH, its volatility may be an advantage in some circumstances but is
surely a disadvantage in others.  The high aqueous solubility of all
salts of acetate may be a complicating factor in using acetate salts
as desiccants in closed-circuit systems.

[51]: https://www.instructables.com/Sodium-Acetate/
[54]: https://en.wikipedia.org/wiki/Ammonium_acetate
[52]: https://www.instructables.com/Crystallization-of-homemade-sodium-acetate/
[53]: https://www.quora.com/What-is-the-process-for-making-sodium-acetate-crystals
[55]: https://acp.copernicus.org/articles/11/12617/2011/acp-11-12617-2011.pdf "Hygroscopic behavior of atmospherically relevant water-soluble carboxylic salts and their influence on the water uptake of ammonium sulfate, by Z. J. Wu, A. Nowak, L. Poulain, H. Herrmann, and A. Wiedensohler, Atmos. Chem. Phys., 11, 12617–12626, 02011, CC BY-SA, doi 10.5194/acp-11-12617-2011"

#### Corrosion ####

Urea and the alcohols, especially propylene glycol, may be useful not
only to absorb water but also to scavenge oxidizing species that get
into the desiccant, thus preventing corrosion.  And acetate or
phosphate can form buffers that prevent the pH from varying very much,
which could also make it corrosive to some materials, though chlorides
are very corrosive to most metals even at neutral pH.  There are also
reports that when muriate of lime is used in this way it gradually
produces the acid, maybe requiring some kind of buffering to remove
it.  A simple solution to this problem may be to include some calcium
carbonate stones in the desiccant circuit, such as landscaping marble
stones (US$5/25kg), a solution not available for conventional sealed
brine coolant circuits, since the reaction produces gas.

A difficulty with any such aggressive reducing agent is that the
desiccant is in intimate contact with the air, which has oxygen in it,
including when it's fairly hot, so anything with great affinity for
oxygen will oxidize.

Phosphate in particular tends to passivate metals pretty aggressively,
but for the same reason it's incompatible with most of the most
promising salts, such as muriate of lime, because they contain
divalent cations, all of which form insoluble phosphates.  Soluble
phosphates include those of sodium, potassium, ammonium, and hydronium
(phosphoric acid).  [Dipotassium phosphate ("DKP", sometimes mispelled
"DPK") is commonly used for corrosion inhibition in heat transfer
fluids][16] because it's food-safe and effective on cast iron and [on
steel][17] at about 1–2%.  For reasons I don't understand, though,
phosphates are ineffective on aluminum and maybe actually
counterproductive.  [DKP is used especially with glycerin and
glycols][28].

[16]: https://patents.google.com/patent/US20060131544A1/en "Expired US patent 7,435,359B2, from application US20060131544A1, corrosion inhibiting heat transfer materials, by Fred Scholer of Hercules Chemical Co."
[17]: https://www.tandfonline.com/doi/abs/10.1080/00960845.1966.12006088?journalCode=ujbc19 "Corrosion Measurements on Propylene Glycol Refrigerant in a Brewery, by B. P. Buckley,  Proceedings. Annual meeting - American Society of Brewing Chemists, Volume 24, 1966 - Issue 1: Proceedings of the Annual Meeting 1966, pp. 22-26, 10.1080/00960845.1966.12006088"
[28]: https://www.chemworld.com/Glycerin-and-Water-p/ivg-5.htm "This product is 70/29/1 ratio (Glycerin/Deionized Water/DPK)."

DKP is not available locally, but monopotassium phosphate (a
fertilizer) and potassium hydroxide are, from which DKP can be made
easily.  Buying the latter gets you Put On A List, though.  Other
available potassium salts include the nitrate and sulfate
(fertilizers), the citrate and gluconate (dietary supplements), the
alum, and the sorbate (a preservative).  One vendor (Natural Whey)
even sells potassium bicarbonate for US$5/kg!  That would be fine.

Glycerin [is sometimes used for inhibiting corrosion][14], but [Dow
reports that it breaks down easily][15] producing corrosive
byproducts; I think the products in question are acids, [as with
glycols][17] [and chloride brine coolants][25], so buffering the pH
may help, which is one of the benefits of DKP.  Nitrites (US$2.50/kg
for making sausages, though eBay has prohibited its sale since 02019),
[chromates][24], and molybdates are commonly used as more aggressive
antioxidants for inhibiting corrosion.

[14]: https://www.mdpi.com/2571-5577/1/2/12 "Crude Glycerol as an Innovative Corrosion Inhibitor, by Isam Al Zubaidi, Robert Jones, Mohammed Alzughaibi, Moayed Albayyadhi, Farzad Darzi, and Hussameldin Ibrahim, Appl. Syst. Innov. 02018, 1(2), 12; 10.3390/asi1020012"
[15]: https://patents.google.com/patent/WO2010008951A1/en "WIPO PCT patent application 2010008951A1, corrosion-inhibited propyleneglycol/glycerin compositions, by Kevin Connor and John Cuthbert"
[24]: https://www.osti.gov/biblio/82976-non-chromate-treatment-calcium-chloride-brines "A non-chromate treatment for calcium chloride brines, by K. Sotoudeh and I.M. Funderburg, in proceedings of Corrosion 94, OSTI 82976, report CONF-940222-TRN: IM9533%%434, 01994; mentions frequent additions of caustic as a current practice to prevent corrosion from these chlorides"
[25]: https://www.hydratech.co.uk/Technical/Brine-as-a-heat-transfer-fluid-and-antifreeze/0/33 "Chloride Brines as used in refrigeration systems include [CaCl2, NaCl, MgCl2, and LiCl]... Whilst the heat transfer properties and viscosities are good when compared to Alcohols, Glycols and Glycerine, the corrosivity is significantly worse. Chloride solutions require constant pH buffering and a high level of inhibitors to keep corrosion in check. [KAc and HCOOK] are much more corrosive, especially towards zinc and aluminium"

Potassium nitrite in particular is deliquescent and nearly impossible
to crystallize from aqueous solution, suggesting that it may have a
strong enough affinity for water to be a useful desiccant on its own.

Normally, water-based coolant loops need to be designed with careful
attention to preventing bacterial growth, especially if nitrite is
used to inhibit corrosion, but I think the desiccant loop here will
not have that problem.

[Tyfo reports][26] that their nontoxic calcium chloride brine coolant
for use below 0°, with corrosion inhibitors (but not borax,
phosphates, nitrites, phosphates, or amines), is pH 8.5–9.0 and is
compatible with non-alloyed steel and virtually every organic polymer
(butyl rubber, LDPE, HDPE, EPDM, PEX, epoxy, PP, fluorocarbons, PTFE,
natural rubber, nitrile rubber, styrene-butadiene rubber,
polychlorobutadiene, and hemp --- notably not including PVC), but
"must **not** be applied for installations...of aluminum, aluminum
alloys,... stainless steel ... galvanized pipes".  It becomes more
corrosive if diluted with water, and that exposure to air accelerates
the consumption of the corrosion inhibitors.  Also they warn that
mixing with, among other things, glycol/water mixtures can result in
precipitation or reactions.

By contrast, [Tyfo's technical information booklet for their TYFOXIT
F15-F50 potassium formate coolant][27] *does* list soft and rigid PVC,
as well as unsaturated polyester and polyamides, as compatible.  It
also lists two-week 88° ASTM D 1384 test results for TYFOXIT F40
(.0006 mm/year on copper, .17 mm/year on soft solder, no corrosion of
brass, steel, cast iron, or cast aluminum) and also 30% CaCl2 (.03
mm/year on copper, .11 on brass, .32 on steel, 1.04 on cast iron, 1.25
on cast aluminum, and 1.39 on soft solder).  [The usual limits][23]
are 2 mm/year, "completely destroyed within days"; 0.2–1.99 mm/year,
"not recommended for service greater than a month"; 0.1–0.19 mm/year,
"not recommended for service greater than a year"; 0.02–0.09, "caution
recommended, based on the specific application"; and under 0.02,
"recommended for long-term service", though note that that's the paper
that found 4 mm/year corrosion of molten CaCl2 hexahydrate on copper
at 80°, 0.005 mm/year corrosion on A36 carbon steel, 0.03 mm/year
corrosion on Al 5086, and 0.01 mm/year corrosion on Al 6061.  They
also found that lowering the pH below 4 reduced the corrosion rate on
aluminum, while raising it abov 5.5 reduced the corrosion rate on
carbon steel.

Anyway, so I think that from this I can infer that probably muriate of
lime *will* corrode PVC.

[26]: https://tyfo.de/downloads/Calciumchlorid-Spezial_en_TI.pdf
[27]: https://tyfo.de/downloads/TYFOXIT-F15-F50_en_TI.pdf

### Pumping ###

There are tiny cheap (US$5) 1.5-watt water pumps that are claimed to
pump 120 liters per hour against 40 cm of head, which, as explained
above, is 130 mW.  This is only about 10% efficient, but if the water
is acting as a heat transfer fluid, it can move enormously more heat
than the power, as described above: on the order of 3–80 kW.

A 3.5 l/minute aquarium bubbler pump ("aireador") claims to use 5W and
cost US$6.50.  I don't know how much pressure it can push against but
I'm guessing on the order of 40 cm as well; if used as an airlift pump
I think you might be able to get 40 cm of head out of it at the same
3.5 l/minute (a 50/50 mix of air and water), which is 210 liters per
hour and would be about 4900 watts at the same 20-degree temperature
difference.  A big potential advantage of pumping liquid coolants
around this way is that the pump isn't exposed to the coolant, just to
the air, so it can't be shorted out, overheated, or corroded by the
liquid.  Another is that a single air pump can power several liquid
flows through a pneumatic manifold.

Another way to achieve a similar result is the no-moving-parts
geyser/pulsejet approach used by coffee percolators.  You heat a
chamber at the bottom of a narrow water column above its
atmospheric-pressure boiling point, where it is held in liquid state
by the pressure of the water column above it.  Once the higher boiling
point is exceeded, steam forms in the chamber and starts to expand,
forcing the water up the column, where it exits through an aperture.
The reduced weight of the water in the column lowers the pressure on
the steam, thus reducing the boiling point further and accelerating
the water ejection.  Once the column is empty, the chamber and column
can refill with new water through a smaller orifice or orifices.  This
is the same mechanism that causes natural geysers and that drives
pop-pop boats, and I think it sometimes causes steam explosions when
dropping hot objects into shallow water.

In coffee percolators and geysers the resulting water is hot, but most
of it doesn't have to be; if the tube is narrow enough to prevent the
steam bubble from mixing into the water, it's narrow enough to prevent
natural convection in the water from distributing the heat much, so
only the steam and the water in close contact with it need to be hot.

Straightforwardly applied, though, this approach would seem to worsen
the corrosion and fouling problems rather than improving them, because
if you're pumping the desiccant mixture that way, you're heating up
the corrosive solution to almost boiling, and some of it may deposit
as lime scale or something.  I think this can be solved by refilling
after the pop through two apertures instead of one, one that refills
the steam chamber and a bit of the column with distilled water from a
distilled-water reservoir, and another that refills the rest of the
column with the fluid to be pumped.  This will dilute it, but I think
the dilution can be kept minimal.

### Reservoirs ###

The desiccant is likely to be somewhat corrosive, so polyethylene or
polypropylene tanks would be preferable over steel ones.  Used
200-liter steel drums are the cheapest per liter, costing about US$30
(4¢/liter).  Cheap 5-liter polyethylene bottles cost about 50¢
(10¢/liter), while premium stackable 20-liter and 30-liter HDPE
bottles from Landplast are US$4 and US$7 respectively, both coming in
at about 20¢/liter.  Even pricier are hermetically-sealing
polypropylene buckets from Landplast, US$4 for 10 liters or US$6 for
20 liters, respectively 40¢/liter and 30¢/liter.  Brand new steel
200-liter drums cost US$27, bringing the price back down to 13¢/liter,
while brand new virgin HDPE 200-liter drums from Landplast cost US$61
(30¢/l).

1000-liter polyethylene tanks from Eternit cost US$110 (11¢/liter),
cheaper than any option except cheap 5-liter PE bottles and steel
drums.  Industrially the standard version of this is the "rigid IBC"
or "bin", a 1000-liter polyethylene cube inside a steel bar cage on a
pallet, available used for AR$17000 (US$83, 8.3¢/liter), and some
vendors offer them even cheaper; Schütz is the popular brand around
here.

For a small-scale test, probably something like the 30-liter HDPE
bottles would be optimal.  A 120-kg reservoir costing US$28 could
contain 90 liters of regenerated desiccant or 90 liters of diluted
desiccant, or anything in between.  At any given time, a bottle can
belong to either of the two reservoirs, and its membership can be
switched between them when it is empty, using a microcontroller that
controls all the flows in the system.

With 200-liter drums or even 20-liter bottles, a desiccant breather is
probably not necessary; a small breather hole or tube is probably an
adequate diffusion barrier against water vapor to keep the desiccant
dry enough.  When a bottle or drum is full up to the breather tube,
the surface area of desiccant in contact with outside air is tiny.

If the regenerated desiccant gets hot, it will be less effective in
the desiccator.  To prevent it from heating up in the reservoir tank
or tanks, it may be desirable to wrap some kind of insulation around
it.

Even cheaper than these ruggedized stackable HDPE bottles would be
something like the 5-liter or 8-liter PET bottles in which drinking
water is commonly sold.  (Not the blue polycarbonate bottles for water
coolers, which I think are more reactive and are at any rate usually
considerably more expensive.)  PET is pretty inert but not quite as
inert as HDPE or PP, and it can't be fluorinated to improve its
inertness the way HDPE and PP can.  I've found a cleaning-products
vendor on MercadoLibre offering 10 such 5-liter bottles for AR$1000
(US$5), so they cost 50¢ each (10¢/liter), the same as the cheap HDPE
bottles.

There are PCO1881 and even PCO1810 bottles in PP and polyethylenes;
for example, the phosphoric acid I bought from the hardware store is
in such a bottle.  Also, though, Dahi brand yogurt comes in glass jars
(I presume soda-lime) of either 130 ml or 200 ml, with a hermetically
sealing unthreaded white plastic lid which I am guessing is LDPE.
They cost about 30¢.  Because the lid is large and unthreaded, it pops
off even on relatively small overpressures.

Household ammonia, ethanol, hydrogen peroxide, shampoo, and
conditioner are also conventionally sold in hermetically sealing
bottles, normally made of polyethylene, though a few brands of ethanol
use PET instead.  Though 18mm is a common cap diameter for these
bottles, they are mostly not PCO1810 or PCO1880 compatible; they will
engage a PCO1880 cap but not form a hermetic seal.  Bleach is often
sold in bottles that don't even seal hermetically when you buy them.

### Pipes ###

There are many kinds of water pipes and hoses available.  Here the
priority is probably the smallest possible size and adequate
resistance to reacting with the desiccants, and in some cases
resistance to heat.

Connecting the pipes to things often requires fittings, but if they're
made of a soft material, just jamming them into a hole may be
sufficient.  6mm is a common size in the below; as a test, I just
drilled four well-separated holes in a Powerade cap (polypropylene, no
gasket) with a 6mm electric drill masonry bit running backwards, so as
to melt the plastic rather than cutting it.  This produced nice round
holes with some ferocious burrs.  My caliper says a PCO1881 bottle
neck is 21mm on the inside, a Gatorade or Powerade neck is 34mm, the
neck of an Ivess 8-liter drinking-water bottle I had handy (also a PCO
type PET bottle) is 39mm, and these holes are 4.5mm.  I think I could
possibly get 7 such well-separated holes in a Gatorade-sized cap, 3 in
a PCO1810 or PCO1881 cap, and dozens in a Dahi lid.

For the evaporative cooler, the pump's flow rate might need to be
twice the evaporation rate to keep the mass exchangers wet.  Since
even a full-sized 4000-W house air conditioner would be evaporating
only 1.6 g/s, we're talking about maybe 3–4 ml/s.  Thinner tubes are
cheaper, stronger, and more flexible, but a 4-mm-ID tube is 12.6 mm²
in cross-sectional area, so that would be about 25 cm/s.  Therefore,
it may not be feasible to go too much smaller than that; you'd need
high-pressure pumps and high-pressure tubing.  The desiccant circuit
probably needs to run at a substantially higher flow rate, because
only a minority of the desiccant solution can be the water being
absorbed; the majority needs to be either actual desiccant or water
that remains in solution to lower the desiccant viscosity.

However, for smaller wearable personal air conditioners, thinner
tubing may be very useful.  A 100-watt system, as I said above, uses
40 mg/s of water; if this is provided through a 1-mm-ID tube, it would
be only 5 cm/s, which seems like it would be reasonable.

#### Irrigation microtube: 13¢/m ####

Extrusión Omar sells black 5×4 (5mm OD, 4mm ID) "microtubo de riego"
for 13¢/m in 10-m (AR$270) and 250-m lengths.  Rain Point sells a
similar 7×4 product for 19¢/m.  There's also a transparent ("cristal")
4×6 Ligom brand that costs 15¢/m.

Apparently this stuff is commonly used for irrigating hydroponic
gardens.  None of the vendors say what it's made of, but I suspect
it's plasticized PVC, in both the black and transparent cases.  That
probably means it's not useful above about 50°–80°.

#### Black polyethylene sewer pipe: 19¢/m ####

*Patgon Tuberias* or *Patagonia Shale Services* sells "Caño De
Polietileno 1 1/2 Pulgada K10 Cantidad 100 M" for AR$21800
(US$1.06/m), black HDPE, 40 mm diameter and 2 mm thickness, so
probably 44 mm OD, rated for 10 bar.  They say that’s the smallest
diameter they fabricate, and it’s considerably larger than the pipes
and hoses I’m considering, but other vendors do sell a similar product
at smaller diameters; Sanitarios Bernal Plástica, for example, offers
"Caño Polietileno De 3/4 X Rollo 100 Mt K4 - Agua" for AR$7300
(36¢/m), 25.4 mm diameter (which doesn’t sound like "¾" to me) and 2.2
mm thickness, so probably 30 mm OD.  ½" K4 also exists, but they’re
out of stock.  García Tortosa Distribuidora Sanitaria ("GT
Distribuidora") in Lomas de Zamora [has it, though][30], "Caño De
Polietileno 1/2 K4 Rollo X 100mts. Gt Distribuidora" for AR$3930
(19¢/m), 12 mm diameter, 2.2 mm thickness, probably 16 mm OD.

[30]: https://articulo.mercadolibre.com.ar/MLA-851388278-cano-de-polietileno-12-k4-rollo-x-100mts-gt-distribuidora-_JM

At the larger end of the spectrum, Patgon offers "Tubería Polietileno
10 Pulgada K10 Pead Espesor 14,8 Mm" for AR$8133/m (US$40/m), diameter
250 mm, 14.8 mm thickness (so I guess probably 280 mm OD).

I feel like there must be thinner and cheaper polyethylene tubing
around (used in squirt bottles, water guns, inkjet printer ink
handling, and I think ballpoint pens), but I haven’t been able to find
it.  I have a bit here I pulled off a broken squirt bottle head that’s
3.74 mm outside diameter and holds about 1.0 grams of water in its
250mm length, so its inside diameter must be about 2.3 mm.  It’s
translucent and the smoke fom its burning smells like candle wax, so
LDPE is the most likely material.

#### PVC wiring conduit: 20¢/m ####

I thought that possibly the cheapest ones were not really water pipes
at all, but PVC conduit for electrical wiring, which might be toxic
for drinking water but should be fine for most of the water
circulation in such a device.  Neighborhood hardware stores sell a
cheap no-brand inflexible version which is the cheapest pipe I’ve
seen, but the cheapest I can find on MercadoLibre are "caños
corrugados", which are flexible.  The smallest I can find is 20mm
diameter (not sure if that’s inside or outside diameter), priced at
AR$1000/25m, 20¢/m.

This turns out to be more expensive per meter than the irrigation
microtube stuff, but only barely.  If I turn out to need higher flow,
it might be a good option.

#### Polyethylene pneumatic line: 40¢/m ####

DMC sells 7-bar-rated polyethylene tubing in a variety of sizes
including 8×5.5 (8mm OD, 5.5mm ID, 70¢/m) and 6×4 (40¢/m).  This is
thinner, probably easier to connect to, more inert, and probably more
flexible than PVC wiring conduit.

HDPE’s maximum service temperature is commonly cited in the range of
70°–120°, though it doesn’t actually melt until significantly hotter.

#### Copper refrigerator capillary tubing: 70¢/m ####

AVAP, a refrigeration vendor in Boedo, is selling 3.5 m of 0.8-mm
copper capillary tubing for AR$500, which works out to 70¢/m.  This
tubing is used as an alternative to an expansion valve in conventional
refrigerators; copper is used for its resistance to corrosion, and of
course it’s good at hundreds of degrees higher temperatures than the
polymers.

I haven’t found other vendors selling just the tubing; everybody else
is selling it attached to a filter.  But I bet you could get it for a
song from a fridge junkyard.

[Thicker copper tubing for these refrigerant circuits][22] with a ¼"
ID (6mm) costs about US$2/m.  This seems to be the smallest available
size.

[22]: https://articulo.mercadolibre.com.ar/MLA-898129981-rollo-cano-cobre-14-refrigeracion-aire-acondicionado-x-15m-_JM

#### PEX heating pipe: 70¢/m ####

Polyethylene tube is popular for hydronic radiant floor heating,
usually PEX.  Servitek in Villa Devoto offers "Tubo Calefaccion Tiemme
Cobra - Pert Evoh Rollo 25 X 50m" for AR$10919 (US$1.07/m), diameter
25mm (not sure if that’s ID or OD).  They also offer "Tubo Pex Tigre
Para Calefaccion Piso Radiante 20mm X 400mts" for AR$76440
(US$0.93/m); another vendor (in Córdoba), DePlano Sistemas Sanitarios
(PILOT S.A., Fray Luis Beltrán 3281, B° Las Magnolias, X5008APO),
offers "Hidroflex Pex 20mm Rollo 400m Hidroflex Pex" for AR$55284
(67¢/m).

PEX piping is more inert than regular polyethylene and doesn’t melt;
its continuous service temperature is given in Wikipedia as 120°.
It’s degraded by sunlight and can be chewed through by some insects.

#### Aluminum *chopera* heat exchanger pipe: 80¢/m ####

Several vendors sell "serpentina para chopera", a 3/8" (9mm) pipe for
cooling kegs of beer to be served from a tap, typically for about
AR$2500 (US$12) for 15 meters.  Aluminum is not very inert at all, and
using it to heat liquids might not be a good idea (if it somehow runs
dry or boils dry, even locally, a fire would destroy it) but for
liquid-liquid and maybe even liquid-gas heat exchangers it might be a
useful material.

#### Teflon spaghetti: US$1/m ####

The paragon of flexible inert materials is teflon, PTFE, and there are
a number of vendors selling teflon hose in various sizes.  Industrias
JQ in Barracas sells a variety of plastic products, including teflon
balls for check valves, delrin blocks, round bar stock, etc.; their
thinnest variety is "Espagueti Ptfe Ø1.0/0.5mm Me", for AR$205.65/m,
which is US$1.  This has 0.5 mm inside diameter, so it would offer a
significant amount of resistance to liquid flow.  They also have a 3×2
"spaghetti" product at US$1.90/m, and their 6×4 product, "Manguera De
Ptfe Marca Teflon Ø 6/4 Mm", is US$2.60/m.

A potential advantage of such thin tubes is that they look like wires.

At the high end they offer teflon hose wrapped in braided AISI 304
stainless steel (US$7/m at 3mm ID), making it proof against bursting
as well as internal and most external corrosion.

Teflon’s maximum service temperature is commonly given as 260°–290°.

Strangely, these are categorized as "embalaje y logística".  Other
vendors sell teflon tubing for comparable prices as "componentes para
impresoras 3D".

#### Silicone hose: US$1.50/m ####

IDE Ingeniería in Caballito lists a 4×1 "manguera silicona atóxica" at
AR$310.51/m (US$1.50), and rates it variably for use at up to 120° and
220°.  Presumably one of these temperatures is a typo; the correct
temperature is probably 220°.  They also have 6×4 at AR$350/m
(US$1.70), 10×6 at AR$720/m and various other sizes.  Their suggested
uses: "máquinas de café, dispenser de agua, varios, electrónica,
medicina, bombas peristálticas."  Other vendors of similar products at
similar prices include Sysport in Bernal Oeste and Todo Goma in
Almagro.  These are classified in MercadoLibre as "mangueras
industriales".

(IDE is pretty interesting: they have glass microspheres, FR4 circuit
boards, carbide Dremel bits, potting epoxy, photosensitive film for
PCB printing, silicone for potting, electroless tin-plating solution,
a circuit-board prototyping service ("prototipos electrónicos" for
US$2.50), etc.  Really appealing stuff.  Worth a look later.)

Because people in Argentina generally have a pretty confused idea of
what "nylon" or "silicona" are, I can’t be very confident that these
are actually silicone; there’s a fair risk that an ignorant vendor
will try to pass off EVA or plasticized PVC as "silicona".  But I
think those three vendors are selling the real thing.

This is not as inert to corrosion as polyethylene or probably even
PVC, but it’s considerably less likely to contaminate the contents or
fail from heat, and it’s considerably more flexible.  Also, it comes
in smaller sizes.

### Solar collectors ###

It’s possible to regenerate the desiccant with low-grade solar heat,
avoiding the need to burn propane or electricity to power the system.
This is one of the biggest potential advantages of desiccant-based
refrigeration.

A serpentine or coiled pipe or tube is a traditional way to make a
solar thermal collector.  As explained below, heating the desiccant
itself and then later putting it in contact with air is probably not a
good way to regenerate it, because it cannot hold enough heat to fully
regenerate without boiling, so a sealed secondary coolant circuit
should probably be used for the solar collection.  This also permits
choosing a coolant for high-temperature stability and non-corrosivity
without worrying about its desiccant capabilities or stability in
contact with air.

(Alternatively, the desiccant could be circulated through the solar
serpentine and the evaporator several times before being used for
desiccation.)

The question remains of what kind of tubing to use, so here is a table
of tubing types in terms of cost per *area*.  You’d probably need on
the order of 30 m² to power the thermal side of the 4000-watt
household cooler purely from solar: sunlight is only 1000 W/m²,
sometimes it’s simultaneously hot and cloudy or hot and night, and
medium-temperature solar thermal collectors are hard to get much over
50% efficient.

<table>
<tr><th>Type                     <th>OD, mm  <th>US$/m  <th>US$/m²
<tr><th>Microtubo de riego       <td>5       <td>0.13   <td>26
<tr><th>Microtubo Rain Point     <td>7       <td>0.19   <td>27
<tr><th>40mm sewer pipe          <td>44?     <td>1.06   <td>24
<tr><th>12mm "sewer pipe"        <td>16?     <td>0.19   <td>12
<tr><th>250mm sewer pipe         <td>280?    <td>40.00  <td>140
<tr><th>Wiring conduit           <td>20?     <td>0.20   <td>10
<tr><th>Serpentina chopera       <td>9?      <td>0.80   <td>90
<tr><th>DMC 8×5.5 pneumatic line <td>8       <td>0.70   <td>90
<tr><th>DMC 6×4 pneumatic line   <td>6       <td>0.40   <td>70
<tr><th>Refrigerator capillary   <td>1?      <td>0.70   <td>700
<tr><th>Refrigeration copper line<td>7?      <td>2.00   <td>290
<tr><th>1×0.5 teflon spaghetti   <td>1       <td>1.00   <td>1000
<tr><th>6×4 teflon hose          <td>6       <td>2.60   <td>400
<tr><th>4×1 silicone hose        <td>4       <td>1.50   <td>400
<tr><th>10×6 silicone hose       <td>10      <td>3.50   <td>350
</table>

Of the above options, probably the 12mm “sewer pipe” is the best,
because not only is it cheap, it’s thin, which means that you space
out the coil to cover half of the area, points in the air spaces
between coils will be closer to a coolant line than if you use a
thicker line like the wiring conduit.

I’ve seen a Brazilian flat-plate thermosiphon solar collector design
using “corrugated plastic” (“corriboard”/“twinwall”/“coroplast”), that
stuff that looks like corrugated cardboard which replaces the flutes
with a bunch of parallel channels, usually made of polypropylene,
often used for things like roofs of bus shelters, political signs, and
the like.  Palacio Graficas in Montserrat offers 2 m × 1 m sheets
("corrugado plastico"), 2.7 mm thickness, for AR$815 (US$2/m²).  The
Brazilians melted slots in two PVC pipes, stuck the plastic board into
them, sealed with glue, backed it with styrofoam, and painted the
front black; they reported reaching temperatures of about 30°, but
presumably a transparent layer trapping an air space would improve the
efficiency significantly.

Such a transparent layer is needed to reach high temperatures with
nearly any unconcentrated solar collector.  [Plasticos Marajor in
Lomas del Mirador offers 2 mm × 1 mm × 0.5 mm PET sheets that look
like acrylic for AR$1590][36] (US$3.90/m²), which will probably last a
year or two in the sunlight at least; [Cole Parmer rates PET, HDPE,
and PP as having fair UV resistance][34], though worse than
polycarbonate and teflon and much worse than kapton, and I’ve
certainly experienced PP being dramatically weakened by a year’s
sunlight exposure.  ([Steve Zande at Service Thread says PET is almost
two orders of magnitude more resistant][35], though, surviving a year
in sunlight while losing only a third of its strength.)

But even just cutting a bunch of coke bottles in half with scissors
and putting them on top of the plate collector would probably be
adequate.  Or you could wrap some pallet-wrap cling film ("film
stretch embalaje", 25 microns and 6¢/m² before stretching) around the
panel, held up around the edges by some kind of frame so as to trap
air underneath.  In the case of a coil of round pipe or hose, the pipe
itself would form the air spaces, and the pallet wrap might serve to
hold it onto some kind of backing if convex.  Replacing the pallet
wrap every few months wouldn’t be a significant cost unless you fell
off the roof and broke your hip or something.  To get multiple layers
separated by air space, you could scatter gravel between them or just
use bubble wrap ("pluribol", 1-cm bubbles, AR$999 from Market Paper
for 50 cm × 50 m = 20¢/m²).

The obvious alternative material for the backing, other than
corrugated plastic, is styrofoam.  Foamcore ("foam board", not "cartón
pluma" as Wikipedia says) costs US$4/m² and probably isn’t water
resistant, while styrofoam is US$1/m² in 20mm thickness, the thinnest
I can find.  But spray-painting corrugated plastic would be a lot
easier than doing it to styrofoam, which requires a water-based spray
paint.  OTOH the styrofoam would insulate better as a backing; you
could paint it with latex paint before you put the tubing on it.
Topline latex paint costs AR$4999 for 10 liters in any color,
covering 70 m² (35¢/m²).

A layer of transparent silicone over the paint on the top of the
collector would also dramatically improve efficiency through
insulation, but it’s far more expensive than pallet wrap.  According
to file `material-observations.md` in Dernocua, ordinary
hardware-store 3M 420 silicone costs US$6/kg, and it’s about the same
density as water.  A square meter at a thickness of 5mm would be 5 kg,
so about US$30/m².

[34]: https://www.coleparmer.com/tech-article/uv-properties-of-plastics
[35]: https://www.servicethread.com/blog/the-uv-resistance-of-polypropylene-and-polyester-explained
[36]: https://articulo.mercadolibre.com.ar/MLA-674451941-placa-pet-transparente-cristal-simil-acrilico-2x1-mt-x-05mm-_JM

So, the stackup is maybe 20mm styrofoam (100¢), black latex paint
(35¢), 50%-spaced 12mm K4 HDPE "sewer pipe" (600¢), pallet wrap (2¢),
bubble wrap (20¢), pallet wrap (2¢), total US$7.59/m² and probably
about 50% efficient, so about 1.5¢/Wp or US$230 for the whole 30-m²
collector.  If you could incorporate a layer of "crystal polystyrene"
(GPPS) in there you could maybe drop its IR transparency a bit and
boost efficiency significantly, and spacing out the pipe a bit more
might give you a lower cost per watt, probably at the expense of solar
efficiency.

Since the majority of this cost is the pipe, you might be able to
improve it by a little solar concentration, at the possible risk of
melting and burning the whole contraption.  Some additional flat
styrofoam panels with aluminum foil or inside-out potato-chip bags
glued to them (or 20¢/m² aluminized mylar) could straightforwardly
increase the illuminance on the main collector to something like 2 or
3 suns with minimal risk of fire.  As well as reducing the cost per
watt, this would also reach higher temperatures with a given amount of
insulation.

Regenerating the desiccant opportunistically with solar heat rather
than continuously (with gas or electricity or whatever) has the
disadvantage that not only peak solar collection capacity but also
peak desiccation capacity needs to be several times the average
cooling power.  The 30-m² collector described above can produce 15 kW
of thermal power, but only about 27% of the time (and in many places
the capacity factor is even lower).  So the desiccant regeneration
exchanger needs to handle about 4 times the power of the cooling
circuit, assuming no losses.

### Non-solar heat sources ###

As mentioned below, common household instant hot-water heaters are
normally about 25 kW, and cost about US$160.  People who can’t afford
a real calefón often opt for an electric tank shower, which costs
about US$6 and consumes about 1200 watts, but the electrically
insulated heating element cartridge alone costs more like US$2.

Smaller gas burners are a plausible source, but surprisingly
expensive.  To measure the power of the one here, I put 718 grams of
room-temperature water (23° ±3°) in a small pot on the stove wrapped
in aluminum foil, and it took 6'50" to boil (100°) on max heat, which
comes out to 560 watts delivered to the water, maybe 600 or 700 watts
from the burner.  I seem to remember that it took about an hour on
this burner to boil dry 500 g of Coca-Cola for rust removal, which
would be about 300 watts, and that I wasn’t using max heat.  Many
electrical hot plates and stove burners are in the range of 1000–2000
watts, so we should expect gas stove burners to also be in that range.
You can get a gas stove burner for US$12, or US$27 with electric
ignition, and running off propane instead of natural gas you could
probably get 2000 watts out of it instead of 700.  [Larger burners are
available for around US$20–50 for cooking things like paella][45]
(bizarrely, classified as "*parrillas mecheros para discos de
arado*"), and a used empty 10kg propane bottle is about another US$40.
You’d need to rig up some kind of igniter system with a
solenoid-controlled gas valve and safety measures for when the igniter
failed to ignite.  You’d have a pretty high penalty for an inadequate
design, though.

[45]: https://articulo.mercadolibre.com.ar/MLA-616058195-quemador-anafe-mechero-47cm-gas-natural-y-envasado-garrafa-para-disco-de-arado-paellera-nicstore-_JM

You could of course regenerate your desiccant with heat from a wood
fire, a charcoal fire, or a fire made with some other fuel.

Probably the most practical solution to get started is to use electric
heating, perhaps with a steam circuit between the heating element and
the desiccant to prevent corrosion.

Air
----

Ultimately most of my objective is to cool the air, and to do that I
need to move it around.  And, as it turns out, most of the exergy for
the whole system will probably go into moving air around, so to
minimize exergy consumption we want to make it as easy as possible to
do this.

At a fixed power of 4000 W, cooling the air involves moving an amount
of it inversely proportional to the temperature drop.  If the air
coming out of the contraption is too cold for comfort, it will mix
with warmer indoor air and warm up, so we should shoot for the largest
temperature drop we can plausibly achieve, perhaps 20°.  But we might
get only 8°, and multiplying by 1.2 mg/cc and 1 J/g/K we get 9600 J/m³
or 9.6 J/l.  At this rate we need to move 420 liters (0.42 m³) per
second, or, in folk units, 900 cfm or 1500 m³/hour.

### Pumping ###

Moving around large quantities of air is harder than moving water.  A
US$1.50 3-watt AUB0612VH Delta computer case fan claims 29.3 CFM (13.8
l/s) or a head of 8.9 mm water (88 Pa), which would be 1.2 watts
output if it could do both at once.  With the same 20-degree
temperature difference, assuming 1.2 mg/cc and 1 J/g/K, this works out
to about 350 watts.  For US$10 you can get a 54.6 CFM (25.8 l/s)
2.2-watt ID Fans NO-12015-XT with RGB LEDs or an Anne Well WDF12038BL
which can do 80 CFM at no load (600 W of convection under the same
assumptions) or 40 CFM at 3 mm water (30 Pa) but which runs on 220V.
The [Hydra HY-VF100C][13] bathroom extractor fan is 12 W and claims 80
m³/hour (22 l/s), 500 W under the same assumptions, and costs about
US$12.

[13]: https://www.sodimac.com.ar/sodimac-ar/product/1650211/extractor-de-aire-80-m-h-12-w/1650211/

While this multiplier of 100–200 between the power to drive the fan
and the thermal power of the air it is pumping is quite large, it is
still one to three orders of magnitude smaller than the corresponding
multipliers for pumping around heat with water, so an efficient design
will focus on minimizing the air pressure load rather than the water
pressure load.

To reach the desired 420 l/s with fans like the above would cost on
the order of US$100 and 200 watts of electrical power and dozens of
fans for each of the air circuits, so I suspect that the answer is to
use larger, slower fans with a lower head.  Unfortunately the local
vendors selling these things usually don’t rate those fans for volume
or even RPM.

But this aspect of the system is not especially different from any
other air conditioner, so we can look at air conditioner parts.
MercadoLibre has a [section entitled "Repuestos y Accesorios Para
Aires Acondicionados"][19].  The current fashion for "split" air
conditioners, which have similar airflow needs, is to use a long,
narrow injection-molded plastic squirrel-cage cylinder as the blower,
though fed from louvers on its circumference rather than in the center
like a normal squirrel-cage blower.  Typically these are driven on a
6mm shaft by motors of 15–100 watts which sell for US$15–50.  One
current example ("Turbina Centrifuga Aire Split 835 X 107 Sirve Para
850 X 100") is 835 mm long and 107 mm in diameter, offered for AR$5880
(US$29), not including the motor.  It is advertised as a "REEMPLAZO
ECONOMICO PARA LOS BGH DE 4500 FRIGORIAS Y 6000 [FRIGORIAS][18]",
which is to say, 4500 to 6000 kilocalories per hour, or 5000–7000
watts.

[18]: https://es.wikipedia.org/wiki/Frigor%C3%ADa
[19]: https://listado.mercadolibre.com.ar/climatizacion-repuestos-accesorios-aires-acondicionados/

Half the circumference of this blower would be 168 mm; 420 l/s divided
by that and by 835 mm gives us 3.0 m/s airflow velocity, which seems
like it’s in the right range for how fast air comes out of these
blowers, but maybe a bit high.

Possibly the most reasonable thing to do would be to buy a broken air
conditioner or two, maybe from a junkyard, and use the air pumping
apparatus and heat exchangers from them.  Even punctured serpentine
heat exchangers might be usable, because a small leak in the desiccant
circuit just forms a drip that can be collected with a drain.

For whatever reason, the condenser (the outdoor unit) in a
conventional split air conditioner is always much noisier and occupies
substantially more volume than the indoor unit, and I think the
airflow through it is also substantially larger, in part because it
needs to reject not only the heat extracted from the indoors but also
the heat added by the compressor.  (One [axial fan replacement part
sold for US$50][20] is 490 mm diameter, 151 mm thick, and uses a 12mm
axle.)  This desiccant-based design might be able to avoid that; the
desiccator and the final swamp cooler are processing about the same
airflow as a conventional evaporator, while the evaporator (located
outdoors, the reverse of the conventional phase-change arrangement)
can process even less airflow if operated at a high temperature.

[20]: https://articulo.mercadolibre.com.ar/MLA-1115092792-helice-pala-condensador-aire-split-12-caa-eje-12mm-diam-_JM

I finally found a vendor that rates their large fans for volume!
[Sodimac’s cheapest ceiling fan is a 65-watt “Halux CB-56”, for
US$33][43], with 142-cm (56-inch) diameter, which works out to about
1.6 m², rated at 180 m³/minute, which works out to 3000 l/s and 1.9
m/s.  Also it weighs 5 kg.  Over this area our desired 420 liter/s
flow rate would be only 270 mm/s. [Normally fan flow rate is
proportional to speed, the pressure to the square of the speed, and
the power to the cube of the speed][44], so if we ran this fan at 14%
of its normal speed, we would expect it to use about 52% of its normal
power, 34 watts.  But we can reasonably guess that an otherwise
similar fan optimized to run at this lower speed would provide our
desired airflow at something more like 12 watts and perhaps a smaller
diameter.

[43]: https://www.sodimac.com.ar/sodimac-ar/product/1860968/ventilador-de-techo-cb-56-de-chapa-3-aspas-56/1860968/
[44]: https://beeindia.gov.in/sites/default/files/3Ch5.pdf "Indian Bureau of Energy Efficiency, chapter 5, pp. 93–112, Fans and Blowers"

### Ducting ###

Inflexible ducting is usually made from galvanized steel, though
probably that’s enormous overkill, while flexible plastic ducting
(“ducto flexible”) is commonly available from grow shops and HVAC
shops.  The Kaizen grow shop offers a 4-inch (100 mm) flexible blue
ducting for AR$350/m (US$1.75/m) and 6-inch (150 mm) for only
AR$400/m, while AVAP Boedo offers transparent at AR$1300/2m
(US$3.20/m), as well as larger sizes like 250 mm at AR$2300/2.5m
(US$4.50/m).  There’s also a non-plastic aluminum version of this
tubing which is slightly pricier but will hold a shape after it’s bent
into it.

When under positive pressure, it may be possible to use a tubular
non-rigid polyethylene bag for ducting the air around.

However, possibly a better option is to keep ducting to an absolute
minimum by putting airflow stages right next to each other, relying on
much cheaper and more convenient liquid coolant circuits to transfer
heat and water any distance of more than a few centimeters.

Heat and mass exchangers
------------------------

Even the minimal complete version of this contraption includes three
liquid-gas mass exchangers, probably best realized as packed beds (in
conventional swamp coolers these are lightweight [US$3/kg
excelsior][21], which local vendors claim is about 0.125 g/cc, so
that’s 38¢/liter), plus some kind of heat exchanger for heating the
desiccant.  More elaborate versions have up to five additional heat
exchangers, one of which needs to withstand direct contact with the
desiccant.  Where can we get these?

(On a totally unrelated note, excelsior may be a good alternative to
polyurethane or EVA foam for upholstery and in particular toolchests:
low VOC outgassing, no spontaneous degradation, somewhat higher
maximum temperature, less toxic combustion products.  But I think it’s
less mechanically resilient.  Similar shavings made from stable
plastics such as UHMWPE, delrin, or teflon might be a viable
alternative.  Probably I should just use polyethylene foam for such
things, though.)

[21]: https://articulo.mercadolibre.com.ar/MLA-868121746-viruta-madera-color-natural-1-kg-relleno-cajas-embalajes-_JM

### Packed beds ###

Many possible packing materials exist, aside from the traditional
choice of excelsior; among the cheapest lightweight options are pumice
and LECA (14¢–29¢/liter according to file `refractories` in Dernocua),
while construction gravel (well, “cascote”, which is recycled rubble;
gravel is “granza”, “grava”, or sometimes “gravilla”) is 1.6¢/liter.
As I understand it, broken rock is what is normally used in similar
packed-bed mass exchangers in sewage treatment plants.  Charcoal is
about 0.3 g/cc and about 30¢/kg, thus about 10¢/liter, and its easy
frangibility and low density would be advantages.  Crushed terracotta
or brick are commonly used for surfacing dirt walkways in plazas, so
they’re probably pretty cheap.  Broken walnut shells or peanut shells
may be available as waste and should work well if properly sized, but
[walnut shells sell for US$20/kg as a facial exfoliant here][46].
Snail shell fragments or clam shell fragments may be available and
would serve to buffer the liquid against acidification.

[46]: https://articulo.mercadolibre.com.ar/MLA-786877418-exfoliante-almendras-o-cascaras-de-nuez-x-100-grs-caba-_JM

Since low air head is a primary design consideration here, plate
columns are probably not a good option.  By the same token, crossflow
mass exchangers, as used in conventional swamp coolers, are probably
more practical than counterflow, because they offer the possibility of
making the air path much shorter than the water path.  A sequence of
crossflow exchangers with pumps between them can approximate a
counterflow exchanger.

Just as an order-of-magnitude estimate, we probably need about three
mass exchangers, each about 1 m² and about 50 mm thick, thus 150
liters of packing material, which would be US$60 of excelsior, US$30
of pumice or LECA, US$15 of charcoal, or US$3 of construction gravel
per exchanger.  (Traditional swamp cooler pads are perhaps 1 m² and 20
mm thick, and typically three or four of them are used to cool a
family house in the desert, so this is likely an overestimate.)

#### Packed-bed flow resistance ####

The Darcy–Weisbach pressure loss is *LλρV*²/2*D*, where *L* is the
pipe length, *λ* is the Darcy friction factor, *ρ* is the fluid
density, *D* is the (hydraulic) pipe diameter, and *V* is the mean
(linear) flow velocity.  *λ* itself depends in a complicated way on
the Reynolds number Re = *VD*/*ν* (*V* is flow speed, *D* is the
hydraulic diameter or other characteristic linear dimension, and *ν*
is the kinematic viscosity) and the ratio of relative roughness
relative to hydraulic diameter, but for a packed bed we’re probably
only interested in the turbulent flow regime, where *λ* remains
approximately constant with Reynolds number and increases rapidly with
relative roughness *e*/*D*; at high Reynolds numbers we have something
like this:

<table>
<tr><th><i>λ</i><th><i>e</i>/<i>D</i>
<tr><td>0.05    <td>0.07
<tr><td>0.02    <td>0.05
<tr><td>0.01    <td>0.04
<tr><td>0.005   <td>0.03
<tr><td>0.002   <td>0.025
<tr><td>0.001   <td>0.02
<tr><td>0.0005  <td>0.018
<tr><td>0.0002  <td>0.013
</table>

If you were to scale down the packing of a packed bed by a linear
factor of 2 while packing the same volume, its void fraction and total
void volume would stay the same, and so would *L*, *ρ*, *V*, *ν*, and
*e*/*D*, while *D* would halve.  Re would also halve, but if we’re
well into the turbulent regime that wouldn’t matter, though clearly a
sufficiently small pore size would force us into the microfluidic
realm where everything is laminar.  So the Darcy–Weisbach pressure
loss would double.  The number of particles would increase by 8 while
the area of each particle decreased by 4, so the area available for
gas/condensed-phase interactions would also double.  This suggests the
unpleasant inference that the available area and thus the reaction
rate is directly proportional to the pressure drop, so we cannot
improve our reaction rate at a given pressure drop by fiddling with
the granulometry.

However, there are three escapes from this miserable result, without
even leaving the random packing regime.

The first escape is that, without changing the particle size, packed
bed volume, or interaction area, we *can* make the packed bed longer
or shorter from the perspective of the gas phase, while making it
respectively narrower or wider.  If the head loss is too great from
traveling through a panel that’s 1 m² in area and 50 mm thick, well,
send the same volume of gas through a panel that’s 5 m² in area and
only 10 mm thick.  The residence time in the panel is the same.  In
the extreme we can fold up our packed bed into an accordion like the
paper in a a HEPA filter so as to get many square meters of gas flow
area into a small volume.

The second escape is that if you break up the packing of your packed
bed into smaller chunks (or, more relevantly to the problem at hand,
avoid doing so), you aren’t actually leaving the relative roughness
*e*/*D* unchanged; you’re changing *D* but probably not changing the
*absolute* surface roughness *e* much.  And, as you can see above,
changing the relative roughness by a factor of 2 commonly changes the
Darcy factor by a factor of 10.  So, within limits, by using particles
that are twice as large but no rougher, you can drop the pressure loss
by a factor of 10, and the pressure loss per interaction area by a
factor of 5.

The third escape is that you can increase the pore size relative to
the particle size.  Consider spreading the same particles further
apart: the area doesn’t change, but the diameter of the channels
through which air flows does.  In the limit where the particles are
far apart, the relevant diameter for the Reynolds number remains on
the order of the particle size, but the Darcy–Weisbach hydraulic
diameter continues increasing with the void fraction.  In such an
arbitrarily rarefied fluidized bed, the head loss ultimately converges
to the weight of the particles.  (The Youtuber mentioned above used
mist nozzles improvised from shower heads in his second prototype
rather than the packed bed he used in his first.)

To achieve this without a fluidized bed, you can increase the aspect
ratio of the particles, as is conventionally done in swamp coolers
with the use of excelsior.  This not only reduces the mass of the
packed bed relative to its surface area, it also allows the spaces
between the “particles” in a given direction to be more than an order
of magnitude larger than the “particle” diameter.  Mist eliminators
are normally made of rolls of knitted wire mesh to minimize airflow
resistance in the same way, though because of inertial impact their
“reaction” rate isn’t really proportional to their surface area.

### Porous honeycombs ###

Randomly arranged packed beds unavoidably have a pretty high flow
resistance per unit surface area, so things like car catalytic
converters and wheel-type regenerators commonly use honeycomb
arrangements of channels instead, where a solid has many small
parallel tubes through it, keeping the flow laminar rather than
turbulent.  If you want regenerative mass exchange, for example of
water to air, you can use a porous solid and soak it in a liquid
desiccant such as muriate of lime, perhaps doped with chalk to ensure
that no acid forms.

Four easy ways to form such honeycombs occur to me:

1. Rolling up corrugated cardboard parallel to its flutes turns each
   flute into an air channel.  Cutting strips perpendicular to the
   flutes and rolling up the strips produces many round honeycombs,
   which can then be packed together to form a larger honeycomb that
   isn’t round.  [Some commercial desiccant wheels, like those by
   Climate by Design International, are constructed in a similar
   way][50].
2. Coroplast similarly can be considered many parallel channels.
   Cutting coroplast into strips perpendicular to the channels forms
   rectangles of channels which can then be stacked to fill a larger
   channel.  We find an anomalously high specific heat in polyolefins
   such as paraffin ([3.260J/g/K][48] or perhaps [2.5J/g/K][49]), so
   Coroplast in particular is appealing because it’s made of
   polypropylene (a polyolefin with 1.92J/g/K); so, by packing a
   regenerator with polypropylene rather than some other material, you
   can transfer significantly more heat per unit mass per cycle than
   with materials like aluminum oxide (emery, 0.96J/g/K) or paper
   (1.34J/g/K).  (Hydrogels, however, should still beat it by a lot.)
3. Packing together many small, relatively stiff parallel tubes.
   These can be bought precut, such as plastic or paper straws, or cut
   from longer rods or coils.  I got this idea from some YouTuber who
   stacked together a bunch of small copper pipes inside a large PVC
   pipe to make a regenerator, though copper is a terrible material
   for this because of its high thermal conductivity.
4. Packing together many small parallel organic fibers or rods in a
   matrix of clay or similar, which is then fired to burn out the
   rods, leaving channels.  I think I’ve mentioned this in an earlier
   note.
5. Many hydrogels can be easily molded using the sol–gel process, and
   it’s easy to make a mold with a honeycomb of many narrow parallel
   channels if the material you’re molding is fairly soft, as many
   hydrogels are.

[48]: https://www.engineeringtoolbox.com/specific-heat-capacity-d_391.html
[49]: https://en.wikipedia.org/wiki/Table_of_specific_heat_capacities
[50]: https://youtu.be/MER3DtmUveQ

### Heat exchangers ###

For separated gas-liquid heat exchange, as required in the
intercoolers, it would be ideal to find an off-the-shelf part, but I
think this may be difficult due to the corrosivity of the desiccant.
(The Youtuber mentioned above used stainless-steel heat exchangers
from DI-water-cooled industrial machinery, and [an international team
investigating latent-heat storage][23] found that aluminum and carbon
steel resisted corrosion by muriate of calcium reasonably well, but
copper did not.)  Readily available such gas-liquid heat exchangers
include domestic air conditioner evaporator serpentines, domestic air
conditioner condenser serpentines, refrigerator serpentines of both
types, car air conditioner serpentines of both types, automotive oil
coolers, automotive radiators, and water-heating serpentines from
on-demand hot-water heaters ("calefones").  Maybe oil coolers and
radiators from other engines, too: chainsaws, generators, motorcycles,
lawnmowers, leaf blowers, air compressors.  Even electric air
compressors might include some kind of liquid heat exchanger.

These are commonly made of aluminum, copper, and steel.

[23]: https://www.lehigh.edu/~inenr/research/PDF/water-nexus-2.pdf "Corrosion testing of metals in contact with calcium chloride hexahydrate used for thermal energy storage, by S. J. Ren, J. Charles, X. C. Wang, F. X. Nie, C. Romero, S. Neti, Y. Zheng, S. Hoenig, C. Chen, F. Cao, R. Bonner, and H. Pearlman, Materials and Corrosion 2017, pp. 1–11, 10.1002/maco.201709432"

Home heating "radiators" designed for natural convection are commonly
made from aluminum alloys.  Servitek, mentioned above as a PEX vendor,
sells [the "Latin Pert Cenit 500"][31] for US$8, which looks like it
occupies about 1 m² of wall space and has about 10 m² of air–aluminum
contact area.  The ["Kairos 500"][32], which looks similar but a bit
smaller, instead sells for US$105, so one of these two prices must be
wrong; it is billed as 12 elements (6 appear in the photo), 56 cm
height, 8 cm depth, and 8 cm "width" (presumably per element, so 48 cm
total width), and 189 kcal/h (220 W).  Conceivably a combination of
such contraptions with forced air could provide a more reasonable
power density.

[31]: https://articulo.mercadolibre.com.ar/MLA-806609262-elemento-radiador-calefaccion-cenit-500-alta-calidad-_JM
[32]: https://articulo.mercadolibre.com.ar/MLA-811918443-radiador-calefaccion-x-12-elementos-kairos-500-_JM

Car radiators are more powerful; [a replacement Ford Fiesta radiator,
"Radiador Ford Ecosport 1.6 2.0 / Fiesta 1.4d 1.6 Max - Edge"][33]
costs US$45, 56 cm × 36 cm by probably 2.3 cm (though the listing says
23).  The "1.4" and "1.6" seem to be the engine's displacement in
liters, and it turns out that the Ford Fiesta 6 1.4 engine has 60–70
kW power output, taking 12 seconds to go from 0 to 100 kph despite
weighing only 1100 kg.  So probably the engine's thermal output is
about twice that, 140 kW, and the radiator is probably capable of
handling most of that, so perhaps 100 kW (0.5 W/mm²).  I don't know
how much cooling I need for this system but I'm sure it's not that
much.  But for whatever reason motorcycle radiators cost more than
that.

As another data point, an aftermarket ["Radiador Fiat Duna Weekend Uno
Fiorino Diesel 1,7"][42], part number 50005580, 5891711, 7549961,
7678729, or 7693062, costs US$40, 520 mm × 322 mm × 34 mm.

[33]: https://articulo.mercadolibre.com.ar/MLA-694840507-radiador-ford-ecosport-16-20-fiesta-14d-16-max-edge-_JM
[42]: https://articulo.mercadolibre.com.ar/MLA-618858561-radiador-fiat-duna-weekend-uno-fiorino-diesel-17-_JM

Some cars have smaller oil coolers in addition to or instead of
radiators; the [Chevrolet Sonic 1.6 16-valve engine oil cooler sells
for US$16][37] with the O-ring kit, and it seems to consist of a set
of parallel plates to cool the oil that flows through it with the
water-based engine coolant.  I don't have a good way to estimate its
heat exchange power if we run air through it instead of water, but it
might be in the few-kilowatts range that we need, given that it's
about a factor of 10 smaller than the Ford Fiesta main radiator
mentioned above.

[37]: https://articulo.mercadolibre.com.ar/MLA-897825659-radiador-enfriador-de-aceite-oring-chevrolet-sonic-16-16v-_JM

Perhaps the cheapest separated liquid/air heat exchangers are the
serpentine condensors on the backs of refrigerators; they're commonly
repurposed as cooking grills by street vendors.  Opposite from the
case with car radiators, these may be too weak for my purposes.  Casa
Antonio in Ramos Mejía sells ¼-horsepower condensors for [AR$5450
(US$27)][38] and 1/3-horsepower ones for [AR$5200 (US$25)][39], but
the thermal power is probably three times the compressor horsepower
quoted, so perhaps 700 watts.  Normally these are operated with
natural convection; presumably with forced-air convection and maybe a
larger temperature difference they could achieve higher powers.  At
least they have the benefit of coming in a rather large, thin size
which would provide minimal air resistance.

[38]: https://articulo.mercadolibre.com.ar/MLA-920189606-condensador-para-heladeras-14-hp-1250-x-10-_JM
[39]: https://articulo.mercadolibre.com.ar/MLA-920186065-condensador-para-heladera-familiar-13-hp-1350-x-14-canos-_JM

Some cars have a separate radiator for cooling their hydraulic fluids,
for example for the power steering.  Kolme Motors offers [a Ford
Ranger 9L55/3F780/AA/ replacement power steering radiator for
US$10][40], which seems like it would probably be about the right
power but physically too small and thus too high air resistance.

[40]: https://articulo.mercadolibre.com.ar/MLA-788308356-radiador-direccion-hidraulica-ford-ranger-completo-legitimo-_JM

I suspect you could get a pretty reasonable heat exchanger for this
purpose with the *serpentina chopera* 9 mm aluminum pipe I mentioned
above, particularly if you flattened it somewhat.

For heating the desiccant with gas, the obvious solution is to use a
"calefón" or on-demand hot water heater.  MercadoLibre touts "Calefón
Escorial Gas Natural 14 Litros Sin Llama Piloto A" for AR$32000
(US$160), capable of heating 14 liters per minute (230 ml/s) with
natural gas to about 40°, which works out to about 25 kW.  There are a
few obvious problems with this.  First, it's at least 10 times more
powerful than we need for a 1.6ml/s 4000-watt air conditioner, maybe
100 times, and more expensive than necessary as a result.  Second, it
doesn't heat the water hot enough.  Third, it probably wouldn't
withstand corrosion from the desiccant.

However, I think actually the correct solution is to heat the air
being used to regenerate the desiccant, not the desiccant itself,
because not only does that avoid the need for a refractory inert heat
exchanger, it can deliver considerably more heat to the desiccant
without boiling it.  If instead you regenerate the desiccant by
running it over a bed of pumice gravel with flame or solar-heated hot
air blowing through it, you can deliver any arbitrary amount of
thermal energy per cubic centimeter of desiccant, at least until it
stops releasing water and starts to heat up.  So the packed-bed
desiccant dryer, rather than a separate sealed heat exchanger, should
be used for this heat exchange.

If there's no alternative to heating the desiccant and allowing it to
evaporate separately, a possible solution is to recirculate it, like a
conventional swamp cooler: feed the wet desiccant into a sump from
which it is pumped to the heater and then the top of the evaporator
packed bed, from which it mostly runs back into the sump, while
collecting some fraction of the packed-bed output or the sump pump
output as the regenerated desiccant.  Thus the sensible heat capacity
of the desiccant can be used to evaporate its moisture content several
times to achieve a more thorough regeneration.

A possible alternative to running desiccant through an air-source heat
exchanger to cool it is to use a secondary refrigerant such as water
or motor oil to transfer heat to a desiccant serpentine made of
silicone or teflon.  Silicone and teflon are shitty at heat transfer,
but they are cheap, readily available as thin tubes, and suitable for
temperatures above 200°, though not direct flame exposure, and they
are perfectly inert with respect to the desiccants being considered.
And cooling the desiccant may not be important since its thermal mass
is so small compared to the enthalpy of vaporization of the water it's
absorbing.

LED growlights use huge honking aluminum heatsinks; [a typical example
is US$12, rated for 300 watts at 55°, and 13 cm × 30 cm × 4.2 cm, and
900 grams,][41] for use with a 120×120×38 mm fan.  You could maybe
bend some copper refrigeration line into a serpentine or something and
clamp this onto it.

[41]: https://articulo.mercadolibre.com.ar/MLA-883994794-disipador-aluminio-300w-led-cob-indoor-nuevo-13x30x42cm-_JM

### Non-aqueous packed-bed heat exchangers ###

An alternative way to do the air intercooler is simply with a liquid
that isn't water, running over a packed bed to exchange heat with the
air.  It will also exchange some water with the air, but lacking the
powerful heating on the desiccant circuit, this effect should be
pretty minimal, especially if it's a liquid with minimal affinity for
water.  If this liquid is itself cooled with a sealed liquid–liquid
heat exchanger (like the automotive oil coolers used on water-cooled
engines, or just a serpentine tube running through a water bath) the
net water flux necessarily averages to 0 over time.  It's desirable
for it to have a very low vapor pressure itself so as to minimize
contamination of the indoor air, even if most of it would otherwise
condense in the evaporative cooler, and for it to be nontoxic, since
you're going to breathe some of it.

Candidate liquids include gear oil (as for manual transmissions),
automatic transmission fluid, liquid silicones, deodorized frying oil,
mineral oil from the pharmacy, transformer oil, and of course
propylene glycol, dimethyl sulfoxide, formamide, dipropylene glycol,
and glycerin.

This liquid will tend to accumulate crap from the air as well, so it
needs to run through a filter, but in the process it provides
filtering.

A simple variation on this theme is to combine the desiccant
intercooler with the indoor-air intercooler by running the desiccant,
cooled to the outdoor temperature or dewpoint, through *two* packed
beds in the indoor airflow rather than one, thus getting a measure of
countercurrent yumminess despite the general crossflow nature of the
setup.  That is, the cool desiccant runs first through the
"intercooler" packed bed, then through the "desiccant" packed bed,
then through the outdoor evaporator, then through an outdoor cooler,
then back to the beginning, while the indoor air runs through those
two packed beds in the opposite order: first the desiccant bed, then
the intercooler bed.

### Outdoor evaporative coolers ###

Most of the evaporative cooling in this system is provided by the
evaporative cooler which is the last or next-to-last thing in the
indoor-air circuit, but by using an outdoor evaporative cooler, we can
use the outdoor dewpoint rather than the outdoor temperature as our
starting point.

The simplest form of an evaporatively-cooled intercooler might be a
shaded pool with a heat-exchanger serpentine running through it.  The
surface of the pool will tend to cool to the dewpoint of the outdoor
air, but for the equilibrium in use to be close to this endpoint, the
surface of the pool must be very large.  The rule of thumb for
swimming pools is that they usually evaporate at about 6 mm per day,
which works out to about 70 mg/m²/s.  At this rate, 4000 W or 1.6 g/s
is 23 m², and as I said before, I think about two thirds of the 4000W
of cooling will get rejected through this intercooler, so in practice
it might be 15 m².

Increasing the pool's surface area with fountains and waterfalls and
whatnot would be very useful for this, as would blowing air over the
pool, and of course things like those would tend to increase airflow
over the pool as well.

Of course, a packed-bed sort of arrangement would enable these higher
evaporation rates in a much smaller space.

Battery backup
--------------

To run the system when the power is out, you need not only these huge
tanks of desiccant, but also enough battery power to run at least the
liquid pumps and control system (small) and the fan that drives air
through the desiccator and the evaporative cooler (you don't
necessarily need to be regenerating desiccant, and if you do it
electrically, you can't).  The system will still work even if you
don't have the air intercooler running, so you might want to shut off
its fan, too.

As described above, the evaporative-cooler fan might require 20 watts
if designed for slow, large area flow, so perhaps the overall system
would require 30 watts.  30 watts for 24 hours is 2.6 MJ, 0.72 kWh in
folk units, 60 amp hours at 12 volts.  At the 20 kJ/US$ level of
lead-acid battery pricing, this would work out to US$130 of batteries.

20 watts divided by 420 liters/second gives a maximum air pressure of
48 Pa, 5 mm water in folk units.  So we need to keep the air pressure
drop through the heat exchangers below that.  I think this is feasible
because the indoor side of split air conditioners sometimes uses a
lower-power fan motor than 20 watts, and they aren't even designed to
work during power outages (their compressor power dwarfs the fan power
anyway).  Unlike the tiny fans I profiled above, industrial fans
commonly achieve 80% static efficiency at their optimal head, and
motors commonly 90%, so we can really only afford about 22 air watts
of fan output power, 34 Pa or 3.5 mm water.

Indoor operation with a chimney or water flow
---------------------------------------------

A system without intercoolers, just the desiccator, heater,
evaporator, evaporative cooler, pumps, and piping, can be run entirely
indoors if a chimney or water source and drain is available.  The
indoor-air circuit is as normal: desiccator, evaporative cooler, and
fan.  The air source for the evaporator must be the indoor air; the
evaporator should be run at as high a temperature as practical, at
least 80°, to minimize the indoor air that is thus consumed and must
be replaced by (presumably unacceptably hot) outdoor air.

If the heat source for the evaporator is not a fire, instead of
sending the evaporator output up a chimney, it can instead be cooled
by sending it into a chamber with a spray of water, which will
condense it into (initially very hot) water.  If the evaporator is
operated at or above 100° the only output will be this condensed
water, which can be flushed down a drain; otherwise some air will also
issue, which will be warm or cold according to the temperature of the
last spray of water it traverses.  This air can be exhausted into the
living space.

Cascading multiple stages
-------------------------

The above makes it clear that, up to a point, you can get lower output
temperatures by cascading more than one such system (as Mina says,
it's like a Wi-Fi repeater for coldness).  Consider the following
example.  An outdoor evaporator A reaches the dewpoint of the outside
air, say 30°.  Water from A passing through a heat exchanger B cools
air to, say, 31°; air from B is blown onto an evaporator C, which
cools to, say, 20°, whence the moisture-saturated air passes into a
desiccator D and then back to B in a closed circuit.  Cold water from
C cools a heat exchanger E to, say, 21°, whence it is blown through an
evaporator F which cools it to 19° and into the living space.  A
second desiccator G dries the air from inside the house on its way
into E.  All the desiccators in such a cascade can use a single
outdoor evaporator to regenerate their desiccant, and all the liquid
pumping can be done with airlift pumps fed from a single air
compressor, or a pair of air compressors for fault-tolerance with
check valves.

However, because of water's very low vapor pressure below 10°,
reaching temperatures much below 0° would probably require many stages
with very high airflow rates and be quite difficult.  Depressing the
freezing point of the water involved with solutes would make it
possible, but further decreases the vapor pressure of the water.
Lower-boiling-point polar liquids like methanol, acetone, diethyl
ether, isopropanol, ammonia, and especialy ethanol might usefully
substitute for water in lower-temperature circuits, perhaps reversibly
combining with a different set of "desiccants".

In a particularly interesting 1½-stage cascade configuration, we omit
evaporator F and desiccator G, so heat exchanger E directly cools the
living space without any mass exchange, reducing potential concerns
about bacterial and fungal growth and indoor-air contamination with
coolant vapors.

Though it's irrelevant here in Buenos Aires, this sort of cascade
configuration should dramatically improve the outlook for sealed
operation without water losses by virtue of allowing the
desiccator/evaporator circuit to take advantage of the full humidity
swing the desiccant is capable of, say from 33% to 99%, instead of
stopping at a comfortable level like 55%, and by virtue of removing
the limit on ΔT.  The desiccant-regenerating evaporator should be
operated at a high temperature and the resulting atmospheric-pressure
steam recondensed through sealed heat exchange with the outdoor
atmosphere, then returned to the sealed desiccator/evaporator air
circuit where it originated.

Strawman cost breakdown for a 4000W system
------------------------------------------

<table>
<tr><th>30 m² solar collectors (15 KWp) <td>US$230
<tr><th>2 1000-liter rigid IBCs         <td>US$160
<tr><th>2.6 MJ lead-acid batteries (0.7 kWh)
                                        <td>US$130
<tr><th>3 400 liter/s (850 cfm) fans    <td>US$99
<tr><th>2 refrigerator condensors       <td>US$54
<tr><th>300 kg CaMgCl4                  <td>US$30
<tr><th>Aquarium aerator for water pumping
                                        <td>US$7
<tr><th>3 gravel packed beds            <td>US$9
<tr><th>700 kg water                    <td>US$2
<tr><th>10 m 12-mm K4 HDPE flexible interconnect pipe
                                        <td>US$2
<tr><th>Total                           <th>US$743
</table>

As a point of comparison, the Sanyo KCS50HA4CPI split claims 5200
watts of cooling and costs US$675, but it doesn't run during power
outages.

If we eliminate the solar collector, IBCs, batteries, and 90% of the
salt, the price drops to US$176, but some sort of heat source needs to
be added to regenerate the desiccant.

Fountains
---------

An interesting variant of the design uses decorative fountains rather
than packed beds for mass exchange, though at a higher power cost.

Some indoor cooling is supplied by evaporation from greenhouse plants
or recirculating indoor waterfalls.

The outdoor evaporative cooling fountain recirculates water high into
the air while blowing bubbles into it in order to approach the outdoor
dewpoint as closely as possible.  Windcatcher sculptures locally
concentrate any wind onto the cooling fountain to speed its
evaporation, while shading the pool at its base from sunlight for most
of the year.  Mild chlorination prevents algal growth on the white
bottom of the pool.

Desiccant is cooled by circulating through a serpentine under the pool
of this fountain; it then runs through an indoor cold-desiccant
fountain where it absorbs heat and water with air bubbling through it
(well protected from splashing, perhaps with superhydrophobic
splashguards, or with splashguards periodically washed down with pure
water), then to the evaporator for regeneration.

The evaporator is not itself a fountain unless it can be installed
well out of reach of people (80° liquid desiccants are not something
you want your kids to trip and fall into), instead using a packed bed,
but its exhaust is bubbled through an outdoor warm pool or hot tub to
avoid a steam plume, while the temperature in that pool is limited to
safe levels by circulating its water through a second, warmer outdoor
fountain.

Review of related research
--------------------------



<script src="addtoc.js"></script>
