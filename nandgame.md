Olav Junker Kjær's [NAND game](https://nandgame.com/) is pretty neat.
Based on the "nand2tetris" course (_The Elements of Computing
Systems_) In two hours you can go through a simulation of "designing"
a general-purpose Harvard computer, starting from relays; after that
there's a similar amount of work on the software side, which I haven't
gone through.

The 27 hardware design exercises are pretty tightly guided; you work
strictly bottom-up, using components you've already built, to fulfill
a behavioral spec you are given.  None of them require assembling more
than 8 components, and most of them are 3-5 components.  None of them
took me more than 12 minutes (and that one was due to misinterpreting
an ambiguous specification), and many were under a minute.

My log of a playthrough
-----------------------

I worked through the whole hardware part tonight in just under two
hours.  This isn't my first time; I think it was a little slower the
first time.

Unfortunately I didn't record a screencast, which might have been
entertaining.  Maybe I'll do it again at some point.

05:54 start  
05:55 finished level 1, nand gate from two relays  
05:55 finished level 2, not gate from a nand  
05:56 finished level 3, and gate from a nand and a not  
05:57 finished level 4, or gate from a nand and two nots  
05:59 got level 5, xor gate, working, with two nots, two ors and an and  
06:01 got it working with an and, a nand, and an or, 6 nands in all ("but fewer nands possible")  
06:07 got it working with three nands in an and-or configuration and two input nots, 5 nands in all ("but it is possible to solve using fewer components")  
06:08 finished level 6, half adder with an and and an xor, 7 nands in all ("but fewer nands possible")  
06:10 finished level 7, full adder with two half adders and an or, 17 nands in all ("but fewer nands possible")  
06:12 finished level 8, 2-bit adder with two full adders, 34 nands in all ("but fewer nands possible")  
06:14 finished level 9, 16-bit incrementer with a 16-bit adder and an inverter ("2 components used. (Not counting 0 which does not contain any logic.) 273 nand gates in total.  This is the simplest possible solution!")  
06:17 finished level 10, 16-bit subtractor, with a 16-bit inverter and a 16-bit adder, plus an inverter to generate a 1 for the carry ("3 components used. (Not counting 0 which does not contain any logic.) 289 nand gates in total.  This is the simplest possible solution!")  
06:19 finished level 11, 4-bit zero tester, with three or gates and an inverter ("4 components used. 10 nand gates in total. This is optimal!")  
06:21 finished level 12, 16-bit negativity tester with a 16-bit splitter component  
06:22 finished level 13, 2-mux ("selector") with an inverter, an or, and two ands ("This uses the fewest possible components. (But it is possible to solve with a lower total of nand-gates.) ")  
06:24 finished level 14, 2-demux ("switch") with an inverter and two ands ("but fewer nands possible")  
x06:28 finished level 15, "logic unit" selecting between 16-bit and, or, xor, and not-x with three 16-bit 2-muxes, a 16-bit and, a 16-bit or, a 16-bit xor, and a 16-bit not, 560 nands  
06:35 got level 16 working, "arithmetic unit" selecting between addition, subtraction, increment, and decrement with three 16-bit muxes, three 16-bit adders, a 16-bit subtractor, 1490 nands ("But it is possible to solve with fewer components")  
06:42 finished level 16, putting a 16-bit mux on the output controlled by op1 to select between a 16-bit adder and a 16-bit subtractor, with the right operand selected as Y or constant 1 (using an inverter) with a second 16-bit mux controlled by op0 ("5 components used. (Not counting bundler and 0 which does not contain any logic.) 818 nand gates in total. This is the simplest possible solution!")  
06:54 finished level 17, "arithmetic logic unit" delayed due to misconstruing an ambiguous specification, with two 16-bit 2-muxes to optionally swap the X and Y inputs, a 16-bit 2-mux to optionally zero the X, and another 16-bit mux to select between the logic-unit output and the arithmetic unit output ("6 components used. 1890 nand gates in total. This is the simplest possible solution!")  
07:02 finished level 17, "condition" with a mess of ands and nots and ors. ("9 components used. (Not counting is neg which does not contain any logic.) 62 nand gates in total. (But it is possible to solve using fewer components.)")  
07:06 finished level 18, "latch" with two cross-coupled nands forming an R/S latch, two nands gating the R/S inputs, and an inverter.  ("5 components used. 5 nand gates in total. (But it is possible to solve using fewer components.)")  
07:13 finished level 19, "data flip-flop" with a master-slave flip-flop made of two latches and an and-gated clock.  ("4 components used. 13 nand gates in total. This uses the fewest possible components. (But it is possible to solve with a lower total of nand-gates.)")  
07:15 finished level 20, 2-bit register, with two DFFs.  
07:17 finished level 21, 16-bit counter, with a register, an inverter (to get a constant 1), a 16-bit 2-mux, and a 16-bit incrementer.  ("4 components used. 610 nand gates in total.  This is the simplest possible solution!")  
07:21 finished level 22, 2-word RAM (erroneously described as "2-bit"), with two registers, a 2-demux for the write enable line (called "st"), and a 16-bit mux for the output. ("4 components used. 549 nand gates in total. This is the simplest possible solution!")  
07:25 finished level 23, "combined memory", hooking up two registers and a RAM to three control lines ("3 components used. 416 nand gates in total. And 140544 for each kilobyte of RAM. This is the simplest possible solution!")  
07:31 finished level 24, "instruction", hooking up the ALU and condition code tester and a 16-bit mux to the "combined memory" outputs and control lines, the instruction register, a jump control line, and an "R" output ("3 components used. (Not counting splitter which does not contain any logic.) 2080 nand gates in total.")  
07:42 finished level 25, "control unit", using the "ALU instruction" unit from the previous unit and a couple of 16-bit 2-muxes and an inverter. ("4 components used. (Not counting splitter and bundler which does not contain any logic.) 2337 nand gates in total.")
07:48 finished level 26, "computer", wiring together the "control unit" from the previous level, the "combined memory" from level 23, the program counter from level 21, a "rom" component, and a "clock" component.  ("4 components used. (Not counting clock which does not contain any logic.) 3363 nand gates in total. (ROM storage not counted) And 140544 for each kilobyte of RAM. This is the simplest possible solution!")  
07:53 finished level 27, "input and output" with a "splitter", a "bundler", three AND gates, and the given button and lamp components.  

Later, I switched the XOR to a design with only 4 NANDs (a = x|y; b =
x|a; c = x|b; xor = b|c).  Then when I went on to the half-adder
level, the AND and XOR from before counted as only 6 NANDs, "but it is
possible to solve with a lower total of nand-gates".  Then the
"arithmetic unit" level 16 said, "5 components used. (Not counting
bundler and 0 which does not contain any logic.)  754 nand gates in
total. This is the simplest possible solution!"  754 is reduced from
818 above.

Then, reducing the half-adder to 5 NANDs gave the message, "5
components used. 5 nand gates in total. This is optimal!" even though
5 components is more than the 2 I was using before.  This reduced the
arithmetic unit further to 690 NANDs, and reduced the "computer"
level, (level 28), from 3363 NANDs to 3155, a reduction of 208 rather
than just the 128 of the arithmetic unit.

Some commentary
---------------

I found the game extremely engaging.

One of the weak points of nand2tetris is that it's done by people who
don't know anything about hardware except through the lens of
software.  Kjær's rendition is maybe even a little more extreme in
this sense.  The result is that, although in a logical sense the
design does work, it has a bit of the feeling of how an
extraterrestrial civilization might design computing hardware.  The
horizontal-microcode-like CPU instruction set is not entirely alien,
but it's been out of the mainstream for 35 years, resembling the DG
Nova or the PDP-8; the DG Eclipse MV/8000 Arithmetic/Logic Class (ALC)
instructions were its last gasp.

This has advantages and disadvantages.  The final CPU design (which I
think is the Hack design from the book) contains only 3155 NAND gates,
which would be 12620 CMOS transistors if you built it that way, and
this is reasonably small for a 16-bit CPU (though it is more than
three times as big as the 4000-transistor Intersil 6100, which
implemented the 12-bit PDP-8 in CMOS).  Using an especially simple CPU
design makes it easier to cover the whole scope of the design in a
single semester.  27 levels to get to a full design of 3155 NAND
gates — or, uh, 6310 relays — means that on average each level has
about 1.38 times as many relays as the level before.  Since sometimes
you use components from several levels ago this translates to the 3-5
components per level we see, which is a pretty manageable complexity
step.

A big disadvantage is that, although Kjær's alternative symbology and
terminology (some taken from the book, some original) aren't
especially easier to learn than the standard symbology and
terminology, the player finishes the game without learning the
standard symbology and terminology.  This means that if, at some point
in the future, they encounter descriptions or schematics of logic
circuits, they will not understand them, because the symbology and
terminology they learned were Kjær's.

One of the bigger departures from reality is the recursively
constructed RAM.  In real life, RAM uses buses: a bunch of memory bits
are attached to a single sense line (or sometimes differential pair of
sense lines), but only one of the bits drives it at a time; the others
do not.  This requires tristate or open-collector outputs, and
understanding those requires distinguishing voltage from current,
which the game does not do.  So the RAM in the NAND Game (and the
nand2tetris book) uses a tree of 2-way multiplexers made of NAND gates
instead.

While this is a memory design that could in theory work, it would
impose a punishing critical path delay on your circuit even for the
modest RAM sizes considered.  Critical path delays are another aspect
of logic design never mentioned in the NAND Game, although in level 19
("data flip-flop") your quest is to construct an edge-triggered
flip-flop, a task which requires reasoning about asynchronous logic
timing.

Speaking of that flip-flop, it's weird as hell: it has not only the
usual inputs D (labeled "d") and CLK (labeled "cl"), but also a third
write-enable input (labeled "st", and active high, though
traditionally write-enable inputs are active low; I don't remember
encountering any active-low inputs in the whole game).  I've never
seen a D flip-flop with a write-enable input in my life.

Because my screen is "only" 1366×768 I have to zoom the game out a
little bit for it to be comfortable to play; otherwise I can't have
both the inputs and the outputs on the screen at once, and have to
scroll back and forth to access both.  Worse, when the palette of
available components is long enough, it becomes a separate scrolling
pane; unfortunately Ubuntu in its wisdom has seen fit to hide
scrollbars by default, so there is no visible indication of the
missing components except when you're mousing over the pane.

The component palette is carefully chosen for each level.  On level
17, for example, "condition", it contains nand, not ("inv"), and, or,
xor, "is neg", and "is zero"; on level 18, "latch", it contains nand,
not ("inv"), and, or, xor, and a 2-way mux ("select").

One of the hidden treasures is the "replace with parts" menu option.
Since almost all the components except the NAND are built out of NAND
gates (or things built out of them), you have an option called
"Replace with parts" available on each node's dropdown menu, which
does precisely replace the node with its component parts.  The game's
autolayout in this case is really weak, so it may take some work to
get the circuit diagram back into comprehensible form, but this is
genuinely useful at times.

The game uses blue marching ants to indicate when a wire is in a
"high" ("1") state, while "low" is shown as a solid, unmoving black
line.  This is somewhat misleading because it makes you think that a
wire in a low state has no current flowing on it, which is part of the
general confusion between voltage and current I mentioned earlier.

Another thing that I suspect stems from the same confusion between
voltage and current is that the game sets all unconnected inputs to
logic 0.  This is occasionally convenient, and it's consistent with
the fairy tale about relay logic at the start of the game, where a
logic "1" means being connected to the positive voltage rail and able
to source current.

But it's an easy misconception to acquire that a disconnected input
generally is a logic 0, and it's a misconception that causes students
significant practical problems.  For TTL logic, an unconnected input
is reliably a logic 1, because TTL inputs source current into the
output that's driving them when it's low.  And, for CMOS logic, an
unconnected input can do all kinds of crazy things, including logic 0,
logic 1, inducing metastable states in flip-flops, inducing excessive
shoot-through power usage, rapidly cycling between logic 0 and 1 due
to EMI, and floating high or low enough to cause circuit damage,
especially in very-low-humidity environments.

If the game were to pursue its fairy tale about relay logic a little
further, a lot of levels would get a lot easier, because when "1" is
being connected to the positive power rail and "0" is being
open-circuited, you get wired OR for free.  For better or worse,
though, it doesn't allow you to wire together two outputs.

One final UI glitch is that eventually it adds "thick wires" which are
arrays of 16 wires, but you can still connect a 16-wire input to a
1-wire output or vice versa.  I did this by accident on the "control
unit" level, level 25, which was one of the reasons it took me 11
minutes; I had a single-wire inverter hooked up to the entire 16-bit
I(nstruction) register instead of just one of its lines.

Novice user
-----------

I offered the game to a highly intelligent person who didn't have
experience with digital logic, and she ran into a lot of user
interface problems.  She would try to connect inputs to other inputs,
became frustrated when she couldn't connect one input to two outputs,
had trouble with near misses when clicking on an output with an input
selected, and so on.  This synergized with a difficulty at a different
conceptual level, namely, that the main idea of the game was difficult
to understand: the causality of input changes affecting outputs, and
the idea of making a circuit that fulfilled the behavioral spec with
respect to all inputs rather than in a single input state.

This revealed a lot of usability problems that my preconceptions about
digital logic hid from me.

She pointed out that it would be very helpful to have some kind of
"undo" functionality, which wouldn't be out of place in this kind of
puzzle-solving game.  And it would be nice to have some kind of color
coding that helps to indicate what can be connected to what, *before*
you click on it.  Lots of node-and-arc environments in things like
Blender do this, so something like Nandgame could totally do it too.

Possible design alternatives
----------------------------

I don't think it would be out of the question to explain the
difference between voltage and current.  Yes, it's irrelevant to
computation at the logical level; 1 and 0 are just outputs from one
node that can be connected to one or more inputs of other nodes (or
the same node).  But if you want to transfer your NAND-game-playing
skills into the context of actually blinking some LEDs or something,
you will burn up much less hardware if you know the difference between
voltage and current, and that can make the difference between being
able to do a thing with the hardware you have left, and not being able
to.

Also it would probably be helpful to see a couple of worked examples
before being given your first design problem.

Also, I think the standard logic gate symbols are nicer than the
roundrects Kjær picked.

Left-to-right data flow would work better on laptop screens, and
gamers can evidently be convinced to rotate their cellphones 90°, and
it's also traditional.  Inputs on the left, outputs on the right.
Stand up, sit down, fight! fight! fight!

Rather than the Vietnam-War-era-flavored Hack design, it might be more
interesting to design ("design") a RISC-V processor, or maybe
something more outré like a 6502, a MuP21, or an ARM2.  Also, there's
more to digital logic than CPUs.

A logic-analyzer-like waveform viewer might be a nice addition to the
game: not only would it help players learn to use waveform viewers, it
would also help them understand what went wrong when a circuit had the
wrong behavior, by encouraging them to think about how a change to the
circuit affected its behavior for all inputs (or states), not just
one.

If you were to design something in the game (even just using schematic
capture) and then output it as a Verilog module, even just as a
netlist, that would enable both simulation with other simulation tools
(iverilog, etc.) and also execution on an FPGA.  Representation of
DAGs as Boolean formulas, Zhegalkin polynomials, etc., could also be
helpful.
