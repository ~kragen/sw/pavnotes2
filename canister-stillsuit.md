So suppose you have a canister of desiccant spheres (see file
`carboxymethylcellulose.md`) on your back and some air pumps and air
hoses under your clothing.  Can you keep cool?

Sizing a basic system, without regeneration
-------------------------------------------

Let’s consider a basic, but heavy, stillsuit design; after a 24-hour
cycle, you come back home and have a fully loaded canister of
desiccant beads to exchange.  The only power needed is for pumping air
through the system to exchange it between your body and the canister,
and perhaps between the canister and heat exchangers.

You need to dissipate about 100 watts to stay alive.  With
sufficiently thick insulating outer clothing, that’s all you’d need in
any environment, no matter how extreme, because the heat flux into
your body from the hot environment would be insignificant.  100 watts
is 44 milligrams per second of water evaporating (either sweat or
evaporative-cooling water); if we suppose that’s being recondensed in
the canister, it’s about 1300 grams per 8-hour shift, or 3800 grams
per 24-hour period.  If the spheres absorb their own weight in water,
you’d be carrying 4 kg of dried spheres at the beginning of the 24
hours, and 8 kg at the end.  This is a very noticeable amount of
weight, but well within average human capabilities.

### Running open-loop cuts requirements by 50% in reasonable climates ###

Even that is making the pessimistic assumption that you have to
recondense all the cooling water.  But if you can afford to spend it,
perhaps because you live on Earth rather than a fictional desert
planet, you can probably do better with an open-loop system.  Suppose
your desiccant delivers 35% relative humidity and the outside air is
68%.  After passing the dried air over your body to remove sweat and
heat, it’s at 100%.  So only half as much water needs to condense in
the canister as is evaporated from your body.

Supposing we [target a dewpoint of 12°][9], which is about [7mg water
per gram of dry air][10]; that’s 35% relative humidity at 28°, so in
this form the stillsuit only provides modest cooling.  But 35% RH is
still a dewpoint of below 18° up to about 37° temperature.  Air at 28°
with 35% RH has about an 18° wet-bulb temperature, so this amounts to
about 10° of cooling.

[9]: https://en.wikipedia.org/wiki/Dew_point#Relationship_to_human_comfort
[10]: https://upload.wikimedia.org/wikipedia/commons/9/9d/PsychrometricChart.SeaLevel.SI.svg

What’s the airflow required?  44 mg/s of water evaporating at 14mg
water per gram of air is 22/7 ≈ π grams of air per second, which is
roughly 2.6 liters per second or, in medieval units, 5.5 “cfm”, which
is about 20% of the airflow rate for a computer case.

### A daily house energy storage system ###

Suppose that instead we need to provide 3000 watts of cooling, as for
a small house, and we need to do it for 48 hours in case it gets
cloudy, so the solar panels stop producing, but it's still hot.  This
requires enough desiccant to absorb 230 kg of water.

    You have: 48 hours 3000 watts / water_vaporization_heat
    You want: 
            Definition: 229.7465 kg

That is an easy quantity of material to add to your house, and the
cost of this much desiccant is probably around US$500 if using the gel
spheres I was talking about above, maybe US$70 if using liquid
desiccant.  This is a very manageable cost, bracketing the cost of
conventional vapor-compression systems but adding energy storage
capacity.

### The need for heat exchangers ###

The heat-exchangers bit requires a little explanation.  All that heat
sucked away from your body by evaporating water gets deposited again
when the water is greedily sucked out of the air by the desiccant.
This heats the desiccant up, which has the disadvantage that it limits
how much water it can absorb.  Ideally you’d like to maintain the
desiccant at a temperature as low as possible.  Circulating a coolant
through a heat exchanger exchanging heat (but not mass) with the
outside air is the cheapest way to do this.  The best coolant is
probably the air itself.

Water will not condense in this heat exchanger because it would need
to cool the air down from 35% RH (or whatever the desiccant delivers)
all the way to 100% RH, which is to say, to the dewpoint.  But if it’s
just passively exposed to outside air, it won’t cool any further than
the outside air temperature, and if the system is working right, the
desiccant will be only slightly warmer than the outside air.

This is likely to require cycling the air between the heat exchanger
and the desiccant many times.  Probably the best way to do this is
intermittent flow: blow a canisterful of air from the desiccant under
your clothes, replacing it with fresh outdoor air, and then close
those valves and recirculate the new canisterful between the desiccant
canister and the outside heat exchanger many times, perhaps 20, before
repeating the cycle.  If you try to do it in a continuous-flow process
with a single canister, you inevitably end up with some hot, moist air
counterproductively mixed into the cool, dry air you’re trying to cool
yourself with.

Regenerating the desiccant to make water and reduce system weight
-----------------------------------------------------------------

Now, suppose we have a power source, such as a solar panel.  We can
use that to produce hot “regeneration air” so we don’t have to carry as
much desiccant; we seal off the canister from the body-cooling circuit
and the heat-exchanger-cooling circuit, and circulate air through the
heating element and the desiccant to move the heat into the
regeneration air, again recycling it through about 20 times to reach
equilibrium.  It’s reasonable to expect that we can do this at 85°
without too much trouble; air’s water capacity doubles about every
10°, so even 35% of the air’s water capacity at 85° is 20 times its
water capacity at 25°, so we should be able to get 95+% of the water
into the air.  Once we’ve moved the water into the hot air, we replace
it with a fresh charge of outside air, and do something with the hot,
wet air.

One obvious thing to do is to just dump this spent regeneration air,
perhaps mixed with enough ambient outside air that it isn’t dangerous;
this will produce visible steam.  But an alternative is to retain it
in a second, vacuum-insulated canister, so that on the next
regeneration cycle, the heat from this spent regeneration air can be
mostly transferred into the new regeneration air.  90% efficiency is
typical for countercurrent recuperator-type heat exchangers.

This condenses most of the water out of the air inside the heat
exchanger, causing our stillsuit to produce distilled water.

I must be omitting some first-order consideration, because this would
appear to mean that as a first approximation we can remove 100 watts
of heat from a person by condensing 50 watts of water out of the
ambient air and then providing 5 watts of heating to remove that water
from the desiccant, minus whatever losses are involved with the fans
and imperfect heat exchange with the environment.  That would be a
coefficient of performance of 10, or in effect 20 (because the sweaty
air coming off the person has more humidity than our intake air), but
that 10 almost certainly violates the Carnot limit, doesn’t it?

Carnot’s limit is that the efficiency η ≤ 1 - Tc/Th.  Supposing Tc =
18° and Th = 28° as in our above example; then Tc/Th ≈ 0.966 and 1 -
Tc/Th ≈ 3.3%.  So a heat engine operating off the output of our air
conditioner can’t be more than about 3.3% efficient, so coefficients
of performance up to 30 are thermodynamically possible.  But it’s
still wildly implausible, given that existing refrigeration systems
typically top out around 3; most likely I’m disregarding, as
insignificant, some factor that is in reality dominant.

Aha, I think I see the problem: the heat capacity of the incoming air
isn’t nearly enough to absorb the heat of vaporization of all that
water.  See below about economizer limits.

Anyway, whatever the actual coefficient of performance is, by using a
regeneration cycle time of only, say, 0.5 hours instead of 8 or 24
hours, we can reduce the weight of the desiccant canister to under 100
grams of desiccant.  If we have a power source to draw on, that is.

Air at 25° can hold about 20mg/g of water; from my rule of thumb
above, at 85° we should expect 64× as much, or 1280mg/g, which seems
almost sure to be wrong, because that would imply we were above
boiling.  [Suppose 840mg/g][12] is correct; 65% of that is 546mg/g.
100 grams of desiccant absorbing 100 grams of water would then need to
be regenerated with 183g of dry 85° air, which is about 6.1 moles, to
which you add the 5.6 moles of water.  11.7 moles of ideal gas at 85°
works out to a staggering 344 liters.

[12]: https://www.lenntech.com/calculators/humidity/relative-humidity.htm

But maybe we could do it, like, 100mℓ at a time.

Still, condensing 840mg of water releases 1.9kJ,
which is about 32 times as much heat as is
needed to heat a gram of incoming air up from
25° to 85°.  So in a normal countercurrent
setup you would only be able to transfer about
3% of the heat from the output air to the
input.

### The round-robin piecewise heat exchanger ###

Suppose instead that you have 20 vacuum flasks
of spent regeneration air.  Each one has a
distillation-like worm running through it for
fresh regeneration air.  Your outside air
temperature is 25°, and your maximum
regeneration temperature is 85°.  The flasks
are at 3° intervals: 28°, 31°, 34°, ... 82°,
85°.  New intake air is initially cycled
through the 28° flask and the desiccant until
the desiccant canister, the spent regeneration
air, and the new regeneration air are all at
26.5° and humidity equilibrium (35% in the
desiccant and new air, 100% in the old air).
Now you switch to the 31° flask.  It doesn’t
cool off as much because it has more water
vapor than the new air; it’s condensing three
times as much as the new air is evaporating.
Still, maybe it cools down to 28°, bringing the
new air up to that temperature.  (That’s not
quite right!)  Then you switch to the 34°
flask.  And so on.

Because the new air is in equilibrium with the
desiccant, it has in effect much more heat
capacity than mere air.  Still, until we reach
about 70°, I think it only cools the old air
down by about a third as much as it heats up
itself.  I feel like I have some kind of
confusion about this and am not reasoning it
through properly.

Once it reaches 85°, we dump the 26.5° air to
the environment and repurpose the old 28° flask
as the new 85° flask, using the now-spent air.

Somewhere along the line we need to add heat
with the heating element, but I’m not sure
exactly where.  And I don’t have a good picture
of exactly how efficient or inefficient this
proposed round-robin setup is.  My intuition is
that it ought to be possible to make it
efficient, but I don’t really understand it
well enough to analyze it.

### A less absurd heat exchanger: a series of worm tubs ###

Suppose that instead we route the hot, wet spent regeneration air
through condenser worms in a series of five sealed water tanks, which
are at respectively 79°, 67°, 55°, 43°, and 31°, so the air that exits
is 100% humidity at 31°, containing about 30mg/g of water vapor.  The
other 810mg/g has already condensed.

Now, to process a new charge of regeneration air, we start by
recirculating it through the 31° worm and the desiccant, bringing it
up to 35% humidity at 31°.  Then, we circulate it through the 43° worm
and the desiccant, bringing it up to 35% humidity at 43°.  And so on,
until we’re passing it through the 79° worm.  Finally, now that it’s
been passively heated to 79° with heat recovered from the previous
spent charge, instead of passing it through a worm, we pass it through
the heating element to get it up to 85°, before finally passing it
through the whole sequence of worms to recondense it and recover most
of the heat.

This avoids the whole problem of losing heat into the solid mass of
the vacuum flask that’s being repurposed from coldest to hottest, and
it enables our thermal reservoirs to be reasonably sized, though still
much larger than the desiccant.

It is clearly thermodynamically possible to do this with arbitrarily
large worm tubs, so the temperature of the tanks/tubs remains to a
good approximation unchanged through this process.  We’re still
transferring a great deal of heat from the hottest tanks to their
neighbors each cycle, though, because as we heat the air up, most of
the evaporation happens in the last step (67° to 79°), and nearly all
in the last two steps (55° to 79°); but when we’re going in reverse,
the first cooling step is entirely sensible heat, and we only get from
35% to 100% humidity once we’re further down the chain.

Maybe, instead of spacing the tubs along the thermometer at equal
*temperature intervals*, we should space them at equal *amounts of
water vapor*, say, 162mg/g.  So the second step after 85° and 840mg/g
would be 678mg/g, which is about 82°.  Then you have 516mg/g, which is
about 77°; 354mg/g, about 71°; 192mg/g, about 61°; and 30mg/g, about
32°.  But I think this makes the problem worse instead of better; 35%
RH at 85° is 295mg/g, so only the last two stages will condense
anything, so all the heat gets transferred to them.

### Limits on the effectiveness of such economizers ###

So maybe a better approach is to have the hottest tub at 68°, at which
point 292mg/g is 100% RH.  But this means — and this is an unavoidable
feature of this whole family of approaches — that only about 35% of
the heat used for regenerating the desiccant can come from condensing
the previous charge of regeneration air.  The other 65% must come from
the heating element.  But that gives us a coefficient of performance
of only about 1.54.

On the other hand, that’s only because we’re using such a powerful
desiccant as muriate of magnesia, which makes it correspondingly
difficult to regenerate.  Maybe if we used a wimpier desiccant, we
could get a higher efficiency.  Maybe I’ll consider that another time.

But, for now, suppose we’re doomed to a CoP of 1–1.6.  What kind of
power source are we driving this regeneration cycle from?

Possible power sources
----------------------

Of course, if you’re wearing solar panels on your back, you can use
them to run the heating element to heat up the regeneration air, at
least if you’re in the sun.  That might attract unwanted attention
here on Earth (however, so too might the necessary forced-air heat
exchangers).  Or you can just plug your stillsuit into a wall socket,
if you’re sitting in a café or something.  But what if you need to
carry stored energy with you?

Well, that depends wildly on the actual coefficient of performance,
because energy storage weight trades off against desiccant weight.
The heat of evaporation of water is 2.26MJ/kg.  If the desiccant
absorbs its own weight in water, the heat removed by evaporating that
water from elsewhere is 2.26MJ/kg of fresh desiccant or 1.13MJ/kg of
saturated desiccant.  (If the water evaporated is twice the water
absorbed by the desiccant, as estimated above, then the cooling is
4.52MJ/kg of fresh desiccant or 2.26MJ/kg of saturated desiccant, but
this is irrelevant here.)  This trades off against, for example,
[Li-ion batteries at 0.8MJ/kg][11] or so.

[11]: https://en.wikipedia.org/wiki/Energy_density#In_batteries

If re-evaporating the water absorbed by a gram of desiccant requires
more than a gram of battery, you’re better off carrying more batteries
rather than more desiccant.  But if the effective coefficient of
performance is around 10, as suggested above, each gram of battery
with its 0.8kJ can evaporate 8kJ of water, as much as 7 grams of
saturated desiccant.

If the real CoP is around 3, as for most vapor-compression systems,
batteries are arguably still a win, because the extra saturated
desiccant you’re carrying around at the end of the day by reducing the
battery weighs more than twice as much as the batteries do.  But the
batteries add weight all the time, while much of the extra desiccant
weight only happens if you reach the capacity limit of your system.
With a CoP of 3, it’s roughly a wash if you’re only considering the
weight of the fresh desiccant.

Burning 50 grams of butane keeps you cool all day
-------------------------------------------------

But a much more appealing portable power source
is *motherfucking butane*, at 49MJ/kg, or maybe
half that when you include the can in the
weight.  (That's the HHV, tho.)  The way this
works is that you burn the stuff to heat up a
ceramic pebble bed, then use the red-hot
ceramic as your heating element, rather than
some namby-pamby electric shit.  Circulating
your regeneration air (preheated by the
recuperator) through the ceramic spheres and
the desiccant is how you get your hot, wet
spent regeneration air.

Supposing the CoP is around 3, so each joule of
butane revaporizes (and then recondenses) three
joules’ worth of water from the desiccant, 100
watts of body cooling requires 50 watts of
desiccant absorption and revaporization, and 17
watts of flaming butane, about 0.35mg/s, which
is about 20% of the combustion rate of a
cigarette lighter (see file
`cigarette-lighter.md`).  12 hours of such
cooling would then require burning *only 15
grams* of butane, about ten cigarette lighters’
full.  If the CoP is closer to the 10 my crude
analysis suggested above, then it’s 5 grams of
butane.  If it’s 0.9, you need 50 grams of
butane.  This is a totally reasonable amount of
butane to carry in your backpack, about 90mℓ.

Consuming water
---------------

The above open-loop system condenses about half
as much water as it evaporates from your skin,
so you have to get that water from elsewhere,
like a water bottle, perhaps drinking it, or
perhaps spraying it into the air that’s being
distributed.

Suppose that you’re willing for the cooling
system to itself consume water.  The easiest
and most obvious thing to do is to wet the
outer heat exchanger, the one that’s used to
keep the desiccant cool as it’s drying out a
canisterful of air.  This is worth an extra few
degrees of cooling; at 70% humidity and 28°,
the wet-bulb temperature is 24° (see
psychrometric chart above).  This could mean
the difference between, in effect, 10° of
cooling, and, in effect, 13° or so.  It also
would help a lot in making the apparatus less
conspicuous, because the air volume needed to
carry away the requisite amount of water vapor
from the heat exchanger is much lower than the
air volume to carry away that same amount of
heat as sensible heat; even though in this
example the extra vapor is only about 2 mg/g,
that’s about 4.5 J/g, which would otherwise
require a temperature rise of 4.5° or so
between the air input and air output.  So we’re
looking at about an order of magnitude lower
airflow.
