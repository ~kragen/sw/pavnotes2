I was just thinking about the mergesort problem.  Mergesort is almost
the best comparison sorting algorithm: it’s simple, easy to implement,
average-case linearithmic, worst-case linearithmic, and stable, and
has terrific locality of reference and a decent constant factor,
although quicksort is usually faster.  Its main problem is that it’s
not in-place; for the final merge step, you need an auxiliary array
that’s I think half the size of the array you’re sorting.

The reason for this is that the output of the merge usually grows
faster than either of the inputs shrinks.  So you have to write the
output to a place where the inputs aren’t.  It’s good enough to write
the output at the beginning of the array you’re sorting while the
second input is withering away at the end.

Andrey Astrelin has written an in-place variant called [Grailsort][0].
Its constant factors are not as good.

[0]: https://github.com/HolyGrailSortProject/Rewritten-Grailsort

I was trying to figure out a way to sort of shuffle around the records
of the inconveniently located input to make room for new output
records, while still being able to efficiently traverse them in order.
But the ideas I thought would work (without requiring linear space)
didn’t.
