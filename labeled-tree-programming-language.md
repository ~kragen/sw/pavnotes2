I was thinking about Jevko today, and it occurred to me that it
something Jevko-like might provide a nice syntax for programming
languages.

Jevko, as I understand it, is a minimal Unicode notation for ordered
trees with tagged nodes and arcs.  (See file `rose-tree-jevko.md` for
more on Jevko.)

A tagged tree syntax
--------------------

Consider this example of a tagged tree syntax, which is not Jevko but
something like it:

    objectname {source {mediawiki-metadata}Historical population of Bulgaria}
    license {source {commons-templates}cc-by-sa-3.0}
    mime {image/svg+xml} mediatype {DRAWING}

The format is defined by this PEG:

    node = (c* '{' node '}')* c*
    c = [^{}\\] / '\' [{}\\]

That is, a tree node is a sequence of zero or more children, each
being another curly-brace-enclosed tree node preceded by a
possibly-empty tag, followed by a possibly-empty body.  Curly braces
and backslashes are otherwise escaped by preceding them with a
backslash.

The arc to each child is tagged to indicate the parent-child
relationship; the possibly-empty tag appears before the child.
Leading and trailing whitespace bytes in the tag is not significant,
but leading and trailing whitespace bytes in the body are (though some
applications may ignore them).  This non-significant whitespace
permits semantics-preserving automatic reformatting.

The idea here is to provide a recursively nestable data format that
pervasively permits decentralized extension.  The children of a node
are a sequence of tag-value pairs, like the attributes of an HTML tag
or the headers of an email message; most programs will ignore arcs
tagged with tags they don't recognize in that context, *i.e.*, unknown
properties.  This allows you to add new properties (tags) to nodes
without worrying much that it will break something that depended on
that property not being there.

In many applications the order of child nodes is not important, but in
others it is.

Some examples
-------------

### Markup

Here we have a sequence of children with the empty tag holding text
spans inside a block tagged with "p".  Character-level markup like
italics and links is attached to spans using attributes so that new
character-level markup can be introduced without breaking
compatibility.

    p {
        {link{/wiki/Seyyed_Hossein_Nasr}Seyyed Hossein Nasr}
        {argues that it is "reductive" to use a literal interpretation of
         his verses (many of which are of uncertain authenticity to begin
         with) to establish Omar Khayyam's philosophy. Instead, he adduces
         Khayyam's interpretive translation of }
        {link{/wiki/Avicenna}Avicenna}{'s treatise }{i{}Discourse on Unity}{.}}

### Configuration file

Here I'm supposing that newlines are treated as not significant.  This
is from /etc/mailcap on Kubuntu.

    application/pdf {
        name template {%s.pdf}
        test {test "$DISPLAY" != ""}
        okular %s
    }

    text/troff {
        needs terminal {}
        description {Man page}
        /usr/bin/man -l '%s'
    }

This breaks the extensibility rule; if someone adds a child here that
is tagged with something like `set` or `link` instead of a MIME type,
then anything that enumerates the MIME types handled by the
configuration file will include `set` or `link`.  The countervailing
benefit is that you can index into the data with a path like
`text/troff > description`.

### Configuration inspection

    wlp12s0 {
        flags {UP{}BROADCAST{}RUNNING{}MULTICAST{}4163}  mtu {1500}
        inet {192.168.0.23}  netmask {255.255.255.0}  broadcast {192.168.0.255}
        inet6 {prefixlen {128} scopeid {global{}0x0}2800:810:473:8852::1001}
        inet6 {prefixlen {64} scopeid {link{}0x20}fe80::ac69:6dd2:fed0:6ca9}
        ether {6d:d2:fe:d0:6c:a9}  txqueuelen {1000}  Ethernet {}
        RX {packets {915127}  bytes {GB {1.2}1245870073}
            errors {0}  dropped {1}  overruns {0}  frame {428455}}
        TX {packets {629693}  bytes {MB {67.1}67115883}
            errors {dropped {0} overruns {0}  carrier {0}  collisions {0}354}
            device interrupt {17}  base {0xc000}}}

A similar remark pertains here: you can directly address
`wlp12s0 > RX > bytes` instead of having to iterate over a sequence of
interfaces to find the interface you're looking for.

### Programming

    set {{bags} dict{}}
    for {each {line} in {sys{stdin}} do {
        set {{w} split {line}}
        set {{bags{w{0}}} {w{from {1}}}}
    }}

Or maybe:

    let {bags {dict{}}}
    loop {for {line} in {sys{stdin}} do {
        let {w {split {line}}}
        bags {at {w{0}} put {w{from {1}}}}
    }}

Or even, using tags like `bags =`:

    bags = {dict{}}
    for {line} in {sys{stdin}} do {
        w = {split {line}}
        bags {{w{0}} = {w{from {1}}}}
    }

More about this below.

### Package metadata

    pkg {jQuery-bgiframe} version {2.1}
    author {Brandon Aaron <redacted@gmail.com>}
    abstract {jQuery plugin for fixing z-index issues in IE6}
    license {mit, gpl} distribution type {plugin}
    requires {pkg {jQuery} >= {1.0.3}}
    provides {pkg {jQuery.bgiframe} ver {2.1} file {jquery.bgiframe.js}}
    keyword {iframe} keyword {zIndex} keyword {z-index} keyword {ie6}

As with `keyword`, a package that requires more than one dependency,
provides more than one virtual package, or has more than one author
can have multiple children with the `author` or `requires` tag.

Note that here we have a tag `>=` to indicate what kind of requirement
we have of jQuery.

### Relational database data

    player {name {singleplayer} pitch {3.49} yaw {84.6}
            posX {1525.92} posY {135.62} posZ {2952.11}
            hp {20} breath {11}
            creation date     {2022-06-05 02:41:17}
            modification date {2022-07-22 07:57:48}}

    inventory {player {singleplayer} inv_id {0} inv_width {0}
               inv_name {main} inv_size {32}}

    inventory_item {player {singleplayer} inv_id {0} slot_id {0}
                    item {default:pick_bronze 1 63336}}
    inventory_item {player {singleplayer} inv_id {0} slot_id {1}
                    item {default:shovel_bronze 1 38994}}
    inventory_item {player {singleplayer} inv_id {0} slot_id {2}
                    item {default:axe_steel 1 24388}}

Minetest stores this in SQLite more or less as given above, but this
could instead be handled as nested data:

    player {
        name {singleplayer} pitch {3.49} yaw {84.6}
        posX {1525.92} posY {135.62} posZ {2952.11}
        hp {20} breath {11}
        creation date     {2022-06-05 02:41:17}
        modification date {2022-07-22 07:57:48}
        inventory {
            width {0} size {32} name {main}
            item {default:pick_bronze 1 63336}
            item {default:shovel_bronze 1 38994}
            item {default:axe_steel 1 24388}
        }
    }

Programming with tagged trees
-----------------------------

In Lisp you normally program with ordered trees, but without tags on
the arcs or internal nodes, just the leaves.  To compensate for this,
most of the time you begin each list with an symbol that specifies its
type and, implicitly, the semantics of each of the successive child
nodes.  In Elisp:

    (menu-bar-mode -1)
    (tool-bar-mode -1)
    (ido-mode)
    (scroll-bar-mode -1)
    (display-battery-mode)

Let's recast that into tagged trees.  Function calls with zero or one
argument are pretty easy, and we can use spaces in our identifiers
now, as long as they're not at the beginning or end:

    menu bar mode {-1}
    tool bar mode {-1)
    ido mode {}
    scroll bar mode {-1}
    display battery mode {}

Normally a node like this that executes each of its children in
sequence would be tagged with `do`, like Lisp `progn` or Scheme
`begin`:

    do {menu bar mode {-1}
        tool bar mode {-1)
        ido mode {}
        scroll bar mode {-1}
        display battery mode {}}

Now we need some control structures.  These require more than one
argument.  The Common Lisp LOOP macro pretty much works like this:

    * (loop for i from 1 to 10 collecting (* i i))
    (1 4 9 16 25 36 49 64 81 100)
    * (loop for i from 1 while (< i 10) summing (* i i))
    285

This approach allows us to directly translate the Elisp

    (while (looking-at "#")
      (random-line)))

to

    loop { while {looking at {"#"}}
           do {random line {}} }

Thus:

    (defun random-noncomment-line ()
      (interactive)
      (random-line)
      (while (looking-at "#")
        (random-line)))

Might become, if `def` uses the tag on its first child to figure out
what to define:

    def {random noncomment line {}
      interactive {}
      do {random line {}
          loop { while {looking at {"#"}}
                 do { random line {} } }}}

Alternatively, we might put it inside a list of bindings like the MIME
type and network interface examples above:

    random noncomment line {fn {} {
      interactive {}
      do {random line {}
          loop { while {looking at {"#"}}
                 do { random line {} } }}}}

Consider this example in Scheme:

    (define (set-union xs ys)
      (cond ((null? xs) ys)
            ((member (car xs) ys) (set-union (cdr xs) ys))
            (#t (cons (car xs) (set-union (cdr xs) ys)))))

Let's set up `cond` as a keyword-driven thing like `loop`, where the
same tag can be repeated more than once:

    cond {if {null? {xs}} do {ys}
          if {member {item {car {xs}} in {ys}}}
              do {set union {xs {cdr {xs}} ys {ys}}}
          else {cons {car {xs} cdr {set union {xs {cdr {xs}} ys {ys}}}}}}

Here we're passing our arguments as named arguments to the `member`,
`set union` and `cons` functions, because they have two arguments.
And now we can define the whole function, with an analogous argument
list:

    def {set union {xs {} ys {}} do {
      cond {if {null? {xs}} do {ys}
            if {member {item {car {xs}} in {ys}}}
                do {set union {xs {cdr {xs}} ys {ys}}}
            cons {car {xs} cdr {set union {xs {cdr {xs}} ys {ys}}}}}}}

### Object-orientation?

That was noticeably worse than the Scheme definition, but significantly
more explicit.  But that's sort of to be expected if we are only
paying a tax of named arguments without getting any benefit in return.
Suppose that instead we are using a Smalltalk-like system where we can
add a new method to the class List, implicitly invoking methods on
self as in Self or C++.

    List { implement {set union {ys}} as {
      cond { if {null?} do {ys}
             if {ys {contains? {car}}} do {send {cdr} {set union {ys}}}
             send {car} {cons {send {cdr} {set union {ys}}}} } } }

This is shorter but maybe not actually better than the Scheme.  Here
I'm using "send" like "funcall" in Common Lisp: when you have to
evaluate an expression to get a receiver instead of just using a
variable that's bound in the environment.  If `car` and `cdr` instead
act like bound variables inside `List` methods, and are instead called
`first` and `rest`, this could be simplified to:

    List { implement {set union {ys}} as {
      cond { if {null?} do {ys}
             if {ys {contains? {first}}} do {rest {set union {ys}}}
             first {cons {rest {set union {ys}}}} } } }

Actually in a Smalltalky system you'd probably have separate classes
for Cons and Nil.

    Nil  { implement {set union {items}} as {items} }
    Cons { implement {set union {items}} as {
        cond { if {items {contains? {first}}} do {rest {set union {items}}}
               first {cons {rest {set union {items}}}} } } }

### Pattern-matching and free-form data

A Smalltalky system would be, at least conceptually, reifying these
lists of tag-value pairs that are being sent as messages.  What if we
have a pattern-matching facility that allows us to match a subsequence
of the tags, and another one that reifies a node?  Then we don't need
classes at all for Nil and Cons, or much of anything else; we can use
`{nil{}}` and `{car{a}cdr{d}}` for them.  Let's return to a more or
less functional paradigm and define some rewrite rules which can
consume *multiple* tokens of a do-block:

    def { {union {nil{}} with {items}} as {items} }
    def { {union {car {a} cdr {d}} with {items}} as {
        if {set {items} contains {a}}
            do {union {d} with {items}}
            else {make {car {a} cdr {union {d} with {items}}}}
        }
    }

The intended semantics here are that `make` reifies a tree node at
runtime with the tag sequence in its argument and values obtained by
evaluating each of the respective argument expressions, and `def`
describes a new evaluation rule using pattern-matching, in this case
for patterns that look like `union {x} with {y}`.  This presumes that
the following patterns are already defined:

    set {x} contains {y}
    if {a} do {b} else {c}

The second of these requires delaying the evaluation of its arguments
`b` and `c`, but the first could be defined Prolog-style:

    def { {set {nil{}}           contains {x}} as {make {false{}}} }
    def { {set {car {a} cdr {d}} contains {a}} as {make {true{}}} }
    def { {set {car {a} cdr {d}} contains {b}} as {set {d} contains {b}} }

### Query by example

This kind of pattern-matching could be a convenient way to query data
represented as this sort of term tree.  Suppose you have this data,
abbreviated and slightly restructured from earlier:

    player {
        name {singleplayer} pitch {3.49} yaw {84.6}
        inventory {
            width {0} size {32} name {main}
            item {qty {1} wear {63336}default:pick_bronze}
            item {qty {1} wear {38994}default:shovel_bronze}
            item {qty {1} wear {24388}default:axe_steel}
        }
    }

To find out whether the player "singleplayer" has
`default:shovel_bronze` in their inventory, you could match this
against the pattern:

    player {name {"singleplayer"}
            inventory {item {"default:shovel_bronze"}}}

and you should get a match.  However, this points out several
problems:

1. You probably want to know in which position within the inventory
   you got a match.  It's the fifth child node and the second `item`
   child node.
2. Probably you actually want "singleplayer" and
   "default:shovel_bronze" to come from variables, not be hardcoded in
   your code.  But this requires the pattern-matching facility to be
   able to distinguish between variables that are currently, in Prolog
   terminology, "instantiated" and thus constrain the
   pattern-matching, and variables that are "uninstantiated" and will
   thus merely receive results (or possibly identify equality
   constraints between parts of the pattern).
3. You probably don't actually care, in this case, that `name` comes
   *before* `inventory` in the `player` node.  But in other cases you
   do care about this kind of thing and even want particular child
   nodes to be adjacent.
4. You likely want to extract a reference to the inventory item so
   that you can enumerate its tags, which are `qty` and `wear`.
5. You're matching on the bodies of the item and the name for sure.
   But are you also matching on the body of the inventory?  Because as
   written above the inventory has a body containing `"\n    "`, and the
   pattern has an empty body for it.
