Paschen's law has a minimum discharge voltage at some nonzero gap
distance, which is proportional to the air pressure.  The minimum for
air at STP is about 7.5 microns and 327 V; the voltage for a breakdown
at narrower gaps rises sharply.

As I understand it, this is because air's mean free path is about 96
nm, but electrons, being smaller, have a longer mean free path of
about 540 nm.  Free electrons traveling along the gradient of the
electrical potential gain, on average, about 24 eV before hitting an
air molecule, which is roughly enough to avoid being recaptured;
occasionally it will dislodge a secondary electron, allowing the
number of electrons in flight to increase exponentially over a fairly
short time (typically well under a microsecond), until, after about 14
of these steps, a fairly large amount of charge in the form of
electrons strikes the positive electrode, while a large number of
cations are being accelerated in the opposite direction, and the
electrical field in the region they are rushing into has been
strengthened.

(sqrt(24 eV / half electronmass) gives about 2900 km/s; 540 nm / half
sqrt(24 eV / half electronmass) gives some 0.4 picoseconds, so we
should expect this whole process to take a few picoseconds, followed
by a much slower ionic streamer completing the circuit in the other
direction.)

If we have two spherical electrodes slowly approaching one another in
such an environment, with their voltage over this 327-volt magic
number, then the electric field remains too weak to create this
so-called avalanche discharge until it reaches roughly this magic
24-volt-per-mean-free-path number, at which point any stray free
electron (from a cosmic ray, photoemission from a metal surface, a
stray sodium atom, or whatever) can start the process up, and a spark
leaps.  But even when this is happening, the electrical field is only
strong enough at the point where the distance between the electrodes
is shortest, along a straight line.  Along the other paths, the
voltage drop is the same, but the distance is longer.

If the gap is increased in proportion to the voltage, each individual
electron's lived experience remains unchanged, but the avalanche has
more time to grow before smashing into solid metal, so the critical
field strength drops somewhat (that is, the voltage doesn't have to
increase quite in proportion to the gap).  But if the gap is
*decreased*, while each individual step is unchanged, the number of
steps is smaller, so the avalanche has less time to grow.  At this
point, on the negative-slope part of Paschen's curve, by increasing
the voltage it is possible to provide a strong enough field to make
breakdown possible along paths that are not the shortest as well; and
some of those paths will be long enough to get a good avalanche going.

A pseudospark generator consists of a series of discs with a round
hole through the middle, with enough voltage between them to produce
such a "pseudospark" traveling along curved paths that arc between the
edges of the holes, curving along the center of the hole in order to
get enough distance for a good avalanche.  This can be used to
accelerate particles along the axis of the hole.

Normally this is done at a fairly decent vacuum, but if you
manufacture the whole device at the micro-scale, with only a few
microns between one plate and the next, you can in theory get a
pseudospark generator that runs at atmospheric pressure.  I'm not sure
if field electron emission will screw this up: 300 volts over a micron
is only 300 MV/m, which I think is generally not enough to cause field
emission at room temperature.
