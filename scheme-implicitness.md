There’s a common pattern that bothers me in Scheme where a compiler
has to rediscover information the programmer had because there’s no
way for the programmer to express it in the language.  Worse,
maintenance programmers also must rediscover this information in order
to successfully modify the program.  Sometimes a compiler can get by
without the information at the cost of terrible performance.

Other languages have this problem to some extent, too, but it’s
especially severe in Scheme because of its design philosophy:

> Programming languages should be designed not by piling feature on
> top of feature, but by removing the weaknesses and restrictions that
> make additional features appear necessary.

This philosophy inevitably guides us toward a nearly minimal set of
extremely broadly applicable language features, at least to the extent
that this can be achieved incrementally; [`lambda` is famously the
ultimate everything][9], so how do we know which of all those things a
given `lambda` is?  It’s a question you could write a dissertation
about, which is [sort of what Guy L. Steele invented Scheme for][11],
but one wasn’t enough; [Olin Shivers did his on the higher-order
control-flow analysis problem][10], [the Orbit Scheme compiler was six
Ph.D. theses and one Masters thesis on mostly this topic][12], and so
on.

(Edited from a conversation on [a chat channel][7].)

[7]: http://swhack.com/logs/2023-07-04#T04-24-20
[9]: https://research.scheme.org/lambda-papers/
[10]: https://www.ccs.neu.edu/home/shivers/papers/diss.pdf
[11]: https://dspace.mit.edu/bitstream/handle/1721.1/6913/AITR-474.pdf "RABBIT: A Compiler for SCHEME (A Dialect of LISP), a Study in Compiler Optimization Based on Viewing LAMBDA as RENAME and PROCEDURE CALL as GOTO"
[12]: https://dl.acm.org/doi/10.1145/989393.989414

Implementing Schemes with conventional call stacks
--------------------------------------------------

One of the many dissertations about this is [Dybvig’s dissertation
about how he discovered how to implement first-class continuations
efficiently in Chez Scheme][0].

[0]: http://agl.cs.unm.edu/~williams/cs491/three-imp.pdf

Chez implements Scheme’s first-class continuations despite using a
conventional call stack for procedure invocations, which made it the
fastest Scheme implementation in the world.  An easier subset of this
problem is how to implement first-class closures despite using a
conventional call stack for procedure invocations, and he explains
that too.

### The approach taken for closures in my own toy compiler, Ur-Scheme ###

[Ur-Scheme][1] doesn’t have first-class continuations but does have
closures, and it uses a conventional call stack.  What I ended up
doing in Ur-Scheme is pretty similar to what Dybvig describes in §§4.4
and 4.5 (pp. 88–106, 98–116/190): a closure is represented as a
heap-allocated vector containing the pointer to the closure code and a
heap-allocated mutable box for each of the variables that it captures
from its lexical environment, so that mutations from the inner and
outer scope are visible to one another.  Here’s the code, which uses
Scheme as a sort of macro assembler:

    (define (push-closure label artifacts env)
      (emit-malloc-n (+ 12 (quadruple (length artifacts))))
      (mov tos ebx)
      (mov (const procedure-magic) (indirect ebx))
      (mov (const label) (offset ebx 4))
      (mov (const (number->string (length artifacts))) (offset ebx 8))
      (store-closure-artifacts ebx 12 artifacts env))

[1]: http://canonical.org/~kragen/sw/urscheme/

This necessitates that the compilation of the enclosing environment
box any local variables that at least one closure captures.

### Copying values that are never mutated ###

Dybvig points out that this is sort of excessive, because if there’s
no assignment (`set!`) to a variable within its lexical scope, you can
just copy its value rather than heap-allocating it in the parent and
copying a pointer to it, and moreover (p. 100, 110/190) that in
Cardelli’s ML compiler from which he took this “display closure”
technique (in which the closure code pointer and all the captured
variables are stuck into a single vector), this boxing is explicit: in
ML, to get the effect of a mutable variable, the programmer has to
explicitly create a ref cell which permits mutation.

The general pattern in Scheme
-----------------------------

A thing I decided was sort of suboptimal in Scheme when I was writing
Ur-Scheme is that there’s kind of a lot of this sort of thing.  There
are a lot of operations where Scheme provides a very simple, general
facility which in other programming languages require many different
facilities; but then, in order to compile it efficiently, the compiler
has to recover some implicit information from an extensive (possibly
global) analysis of the code.  Examples include:

- allocating a local variable, as explained above;
- calling a function;
- applying an arithmetic operation;
- instantiating an object or closure;
- capturing a continuation.

In this case, when you allocate a local variable, the compiler has to
decide whether it’s ever captured by a closure or not, and if so,
whether it’s ever mutated, and if so, it must box it on the heap.
And, when you create a closure, the compiler has to analyze the code
to figure out exactly which bound variables need to be captured.  (In
Ur-Scheme I just assumed that every variable was mutable, which is
more pessimistic than necessary.)

The thing that kind of bugged me about this is that the compiler
includes a significant amount of complexity (in this case, about three
printed pages of code, 9% of the 35-page compiler) to recover
information that the programmer almost surely knew when they wrote the
program, and which, if they had written it down, for example by saying
`x = ref 0` in ML instead of `x = 0`, would have made the compiler’s
job much easier, and also helped a lot in detecting bugs at
compile-time, and *also* made the code much easier to read!

Because, if the original programmer is wrong about which variables a
closure captures, or which ones are mutable, that’s probably a bug.
And, if a maintenance programmer is wrong about these same things,
they will have a harder time understanding the code, and will tend to
introduce new bugs.

In this case, the ML implementation also ends up doing some unnecessary
work, though less than Ur-Scheme, because although mutability is
explicit in ML, capturing variables in closures is not; so ML
generally ends up heap-allocating all of its mutable reference cells
even where Dybvig would stack-allocate them because they’re not
captured by any closures.

Scheme has a lot of other information that’s like this — known to the
original author, necessary for correctness, implicit rather than
explicit in the code, and necessary for efficient compilation — things
like the types of variables, whether a global function will be
rebound, whether a continuation will escape the context it’s created
in, whether a function call is a tail call, and arguably less
conventional things like the names of formal parameters (which are
implicit at the callsite) and lifetimes of objects in general.

This brings up a general problem with compiler optimization, which is
that, by its nature, it’s somewhat unpredictable; it breaks the
correspondence between the form of the source program and the
performance of the running code in somewhat surprising ways.  So the
maintenance programmer is faced not only with figuring out, for
example, that a given closure cannot escape the scope where it is
created, but also with guessing whether the compiler can figure out
that it cannot, and with guessing or measuring which modifications
will frustrate the compiler’s ability to compute this — since it is in
general a conservative approximation.  See below about
multiple-value-prog1 in Common Lisp.

Steele described this situation somewhat in [his Masters thesis on
Scheme in 01977][11]:

> A small number of optimization techniques, coupled with the
> treatment of function calls as GOTO statements, serve to produce
> code as good as that produced by more traditional compilers.  The
> macro approach enables speedy implementation of new constructs as
> desired without sacrificing efficiency in the generated code.
> 
> A fair amount of analysis is devoted to determining whether
> environments may be stack-allocated or must be heap-allocated.
> Heap-allocated environments are necessary because SCHEME (unlike
> Algol 60 and Algol 68, for example) allows procedures with free
> lexically scoped variables to be returned as the values of other
> procedures; the Algol stack-allocation environment strategy does not
> suffice.  The methods used here indicate that a heap-allocating
> generalization of the “display” technique leads to an efficient
> implementation of such “upward funargs”.  Moreover, compile-time
> optimization and analysis can eliminate many “funargs” entirely, and
> so far fewer environment structures need be allocated than might be
> expected.

Things that don’t quite fit this pattern: records, invariants, and garbage collection
-------------------------------------------------------------------------------------

I thought records might fit into this same mold, but they don’t,
because the information the maintenance programmer needs about records
to modify the code successfully only overlaps slightly with the
information the compiler needs to generate efficient code.  The
compiler cares what types of objects are stored in the fields so that
it can dispatch arithmetic operations efficiently.  The maintenance
programmer cares what the fields are for and what interface their
types support, and every dialect of Scheme is perfectly adequate for
communicating this information, just slightly more verbose and
error-prone about it than C:

    (define (subroutine-args x) (vector-ref x 3))

Records (structs) [have been in R⁷RS-small for ten years][13], taken
from [SRFI-9 records][14], which couldn’t be implemented in pure
Scheme but were often hacked into implementations of older dialects of
Scheme.

[13]: https://small.r7rs.org/attachment/r7rs.pdf#section.5.5
[14]: https://srfi.schemers.org/srfi-9/srfi-9.html

Invariants (of loops and of data structures) and object lifetimes are
also temptingly similar but not quite right.  Loop invariants are
important to correctness, but not to compilation.  Object lifetimes
are important to optimization (you want your program to not use too
much memory) but rarely to correctness, and generally you can get
quite good execution speed just by using a generational garbage
collector.

Dynamic typing does fit the mold, though
----------------------------------------

I can’t find the source now, but someone jokingly described dynamic
typing as being optimal where your program’s type correctness is so
obvious that you don’t need the compiler’s help to verify it, but so
subtle that proving it to the compiler is too difficult.

And type information does fit this scheme.  When you’re writing code
you need to know what types your variables are to write working code;
the compiler can produce vastly more efficient code if it knows; and
maintenance programmers need to know what the types are in order to
modify the code.

However, the programmers mostly about which operations are supported
by those types (their interfaces) and which invariants they uphold,
while the compiler cares mostly about which concrete implementations
to use for each operation (the implementation).  Scheme is fanatically
monomorphic, except for arithmetic, so the latter is often trivial,
except that you need a type check to signal errors.  For operations
supporting ad-hoc polymorphism, though, specializing an operation
invocation is usually a necessary prerequisite for further
optimizations such as inlining, constant propagation, and dead-code
elimination.

Function mutability
-------------------

Another thing that inhibits inlining is when the function you’re
calling might get changed later.  It’s quite rare for a function
defined at top level to be redefined at runtime, but like Common Lisp,
Scheme generally allows this.

This happens in Common Lisp too, but a little differently
---------------------------------------------------------

I had forgotten this, but [RPG’s Good News, Bad News][8] talks about a
similar problem a little bit in the context of Common Lisp.

### Type declarations ###

> This example is a mistake that is easy to make. The programmer here
> did not declare his arrays as fully as he could have. Therefore,
> each array access was about as slow as a function call when it
> should have been a few instructions. The original declaration was as
> follows:
> 
>     (proclaim '(type (array fixnum *) *ar1* *ar2* *ar3*))
> 
> The three arrays happen to be of fixed size, which is reflected in
> the following correct declaration:
> 
>     (proclaim '(type (simple-array fixnum (4)) *ar1*))
>     (proclaim '(type (simple-array fixnum (4 4)) *ar2*))
>     (proclaim '(type (simple-array fixnum (4 4 4)) *ar3*))
> 
> Altering the faulty declaration improved the performance of the
> entire system by 20%.

This is not quite the same thing because Common Lisp compilers do not
attempt to infer the dimensions of the arrays, and there *is* a way to
express the relevant information in CL, as shown above.  But it’s
similar in that the author had to know this information to write the
program, the compiler has to know it to produce efficient code, and
the maintainer has to know it to modify the code successfully.  The
difference is that, in CL, often you can express the information in
the language, which allows you to fix the problem without hacking the
compiler to do better program analysis.

This puts the responsibility on the application programmer, not the
compiler programmer.

### Multiple value returns ###

Gabriel also talks about cases where the compiler’s optimization
abilities are limited:

> The next example is where the implementation has not optimized a
> particular case of a general facility, and the programmer has used
> the general facility thinking it will be fast. Here five values are
> being returned in a situation where the order of side effects is
> critical:
> 
>     (multiple-value-prog1
>       (values (f1 x)
>                (f2 y)
>                (f3 y)
>                (f4 y)
>                (f5 y))
>       (setf (aref ar1 i1) (f6 y))
>       (f7 x y))
> 
> The implementation happens to optimize multiple-value-prog1 for up
> to three return values, but the case of five values CONSes.

In assembly language, it’s straightforward to “return” multiple
values; you just put each value into a different register, then return
to the caller, and the caller can use whichever values they care
about.  [Common Lisp multiple value returns][15] are similar to those
in assembly in that, if the caller is expecting only one return value,
or in general fewer values than are provided, the others will be
ignored.  This is typically implemented in the same way as passing
arguments to a subroutine: each value is placed into a different
register, and the number of values is also placed in a register.  (As
an example, SBCL on amd64 handles the special case of a single return
value by clearing the carry flag, but otherwise puts the number of
values into RCX; the first three values go into RDX, RDI, and RSI, and
then additional values are passed on the stack in memory.)

In general consing tends to be somewhat expensive, though less so
since the advent of generational GC, which was still newish when he
wrote this in 01991.

[15]: https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node94.html

Gabriel continues:

> The correct code follows:
> 
>     (let ((x1 (f1 x))
>            (x2 (f2 y))
>            (x3 (f3 y))
>            (x4 (f4 y))
>            (x5 (f5 y)))
>       (setf (aref ar1 i1) (f6 y))
>       (f7 x y)
>       (values x1 x2 x3 x4 x5))
> 
> There is no reason that a programmer should know that this rewrite
> is needed. On the other hand, finding that performance was not as
> expected should not have led the manager of the programmer in
> question to conclude, as he did, that Lisp was the wrong language.

Again, though, RPG puts the responsibility on the application code,
not the compiler.

[8]: https://dreamsongs.com/WIB.html

[Bit population count][17]: a C example
---------------------------------

[jcranmer claims:][16]

> These sorts of “grand” pattern-matches tend to be low-productivity
> optimizations for compilers (the speedups you get are not worth the
> extra compile-time). LLVM tends to have a higher tolerance for this
> than GCC. But even looking at LLVM’s source code, the “naivest
> possible popcount” \[1\] is not going to pass the loop idiom
> recognizer for a popcount. (...)
> 
> \[1\] I’m assuming by this that you mean iterating over all bits and
> individually checking each one for set/unset. Both gcc (since
> version 9) and LLVM handle the case where you iterate through set
> bits in a for (; x; x = x & (x - 1)) loop.

[16]: https://news.ycombinator.com/item?id=20918136
[17]: http://www.dalkescientific.com/writings/diary/archive/2008/07/03/hakmem_and_other_popcounts.html

I compiled this code with GCC 12 with `cc -g -S -march=haswell -Os
popcnt.c -fverbose-asm`:

    unsigned long popcnt2(unsigned long arg)
    {
      unsigned long i = 0;
      for (; arg; arg &= arg - 1) i++;
      return i;
    }

Sure enough, with the debug information stripped out, the resulting
assembly for the function is this:

    popcnt2:
            popcntq	%rdi, %rax	# tmp87, i
            ret	

But this function, which is equivalent unless I fucked it up, gets
compiled to the slow loop its structure implies:

    unsigned long popcnt3(unsigned long arg)
    {
      unsigned long i = 0;
      for (; arg; arg >>= 1) i += arg & 1;
      return i;
    }

This is another example where the author needs to know that they’re
doing a population count operation in order to get it right; the
compiler needs to figure out that they’re doing one in order to
implement it efficiently; and the maintainer needs to understand
that’s what the code does in order to know how to modify it.

A more reliable way to enable the compiler optimization is to use a
compiler intrinsic like this:

    static inline int popcnt(unsigned x)
    {
      return __builtin_popcount(x);
    }

If the compiler doesn’t recognize this, you’ll get a compile error
message pointing at the problem instead of mysteriously two orders of
magnitude worse performance.  (However, lacking the proper compiler
flag can also result in worse performance.)  And the maintenance
programmer can look it up in the GCC manual.
