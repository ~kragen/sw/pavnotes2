Many sexologists consider “female sexual dysfunction” (“FSD”) to be
very widespread, affecting perhaps 50% of women, by virtue of using a
broad definition, including not just things like vaginismus but
urinary incontinence and sometimes even things like not having orgasms
from penetration.  Quoting [Piao et al.][0]:

> The psychological fear of leaking urine during sexual activity is
> thought to contribute to sexual dysfunction [3]. Therefore, FSD is
> partly due to urine leaking [4]. Sexual dysfunction was more common
> in women than in men [5,6], accounting for 30 to 50% of women, and
> the incidence increases with age [7]. A previous study found that
> 57% of middle-aged women complained of sexual dysfunction and
> urinary frequency [8].

I thought I’d look a bit at the literature.

[0]: https://www.mdpi.com/2075-4426/14/9/938 "Effectiveness of Electrical Stimulation Combined with Pelvic Floor Muscle Training on Female Sexual Dysfunction with Overactive Bladder: A Randomized Controlled Clinical Trial, by JunJie Piao, Dongho Shin, MyeongKeon Moon, SaeWoong Kim and WoongJin Bae, CC BY, published 02024-09-03"

Intravaginal electrostimulation
-------------------------------

### Giuseppe, Pace, & Vicentini 02006 ###

In [their incontinence paper][5] these researchers explain:

> Transvaginal electrical stimulation (TES) is a conservative
> treatment option [for urinary incontinence], which does not make it
> difficult to use a surgical approach later, if necessary [6,7].  TES
> can be used to stimulate nerve fibers and muscles by modifying the
> frequency on the basis of conduction velocity of the nerve fiber
> type.  At 35–40 Hz, the pelvic floor muscles (PFMs) are stimulated
> through a pudendal nerve reflex loop. At 5–10 Hz, TES can also
> affect the detrusor muscle by reflex inhibition with a pudendal to
> pelvic reflex activation. TES is a conservative treatment option
> described more than 40 years ago. Randomized clinical trials about
> TES advocate the use of 50 Hz for stress urinary incontinence (SUI)
> and 10–20 Hz for urge UI (UUI). No side effects have been reported.
> A cure rate and clinical improvement of 60–80% have been reported
> [8].  According to some studies, 43% of patients were continent
> after treatment, with a significant decrease in daytime frequency
> and nocturia [9]. So TES is commonly recommended as a conservative
> therapy for women with UI, especially in less severe conditions;
> recently, TES has been proposed in the treatment of women’s sexual
> dysfunctions (WSD), but its curative effects are yet to be
> investigated [10–13].

They’re also interested in sexual dysfunction:

> Women’s sexual dysfunction increases with age and, according to the
> most recent statistics, affects 30–50% of women [14–17]. The
> disorder of one of the three phases (desire, arousability, orgasm)
> has to be responsible for personal distress to be considered a
> dysfunction, according to the recent classification of female sexual
> disorder [18].

There’s a strong connection:

> There are several reasons for the increase in sexual dysfunction in
> incontinent women, such as dyspareunia resulting from urinary
> dermatitis.  Depression and decreased libido, with a consequent
> reduction of interest in sexual activity, is mainly due to
> embarrassment and fear of urinary incontinence occurring during
> intercourse.
>
> Patients [in the study] attributed disinterestedness toward sexual
> intercourse to a strong uncontrollable desire to void, followed by a
> frequent leakage of urine. They also complained of reduced vaginal
> sensitivity and reduced vaginal lubrication, with lower localized
> pleasure during intercourse. UI occurring during penetration is more
> likely with SUI, whereas those women experiencing incontinence on
> orgasm alone have an increased incidence of UUI. All domain scores
> of the FSFI were significantly lower in incontinent women.  Women
> affected by SUI complain more frequently of sexual pain disorders,
> particularly dyspareunia and pain during penetration; those
> complaining of orgasmic phase difficulties were affected mainly by
> UUI.  They reported fear of reaching orgasm because of the risk of
> having an episode of leakage during intimacy [30].

They explained their treatment protocol:

> Electrical stimulation was conducted for 15–30 minutes, twice a week
> for a period of 3 months. Selected parameters included biphasic
> intermittent current, frequency 50 Hz for SUI and 20 Hz for UUI, 15
> Hz for MUI [mixed urinary incontinence], a pulse width of 300 μs,
> and an adjustable current intensity (0–100 mA) with
> individual-adapted cycles on the basis of each woman’s sensitivity,
> reaching, in the presence of a urologist, who manipulated the stim
> unit, the most individually tolerable intensity of stimulation that
> does not cause pain [27]. “On time” ranged from 0.5 to 10 seconds
> and “off time” ranged from 0 to 30 seconds. (...) The vaginal
> stimulation probe, the same size for all the patients, was made as a
> longitudinal cone with two electrodes shaped like rings at the end
> and in the middle of the probe itself (Figure 1) [8,28,29]. The
> equipment used for TES was an electrical stimulator with a
> microprocessor named “Le Reeducateur.”

Dimensions are not given, though there’s a low-resolution photo.  It’s
not cone-shaped but capsule-shaped, attached to a handle through a
narrowed neck which is evidently intended to be positioned at the
G-spot.

They got fairly dramatic improvements in sexual function among their
(mostly postmenopausal) patients, though they don’t show error bars
and didn’t have a control group (though they did have a
“cross-sectional control” of 43 women without urinary incontinence):

> The assessment of the median score of each domain of the FSFI showed
> that patients had a significantly lower desire (P < 0.01),
> lubrication (P = 0.01), sexual satisfaction (P < 0.01), and a higher
> sexual pain rate (P < 0.01) before than after TES. However,
> differences in the arousal domain and in the orgasm domain before
> and after treatment were not significant (Figure 3).

[5]: https://academic.oup.com/jsm/article-abstract/4/3/702/6890211 "J Sex Med 2007;4:702–707"

### Piao et al. 02024 ###

[Piao et al.][0] did an experiment on 40 women recruited from a
urology clinic due to overactive bladder and other FSD problems,
giving intravaginal electrostimulation to half of them, while giving
pelvic floor muscular training to all of them.  For the
electrostimulation, they used a Buheung Medical [“MK-000A Dr. Lady
device”][1], which appears to use two large cylindrical metal
electrodes inserted deep into the vagina.  Piao et al. write:

> (...) the electrical frequency was set to 40 Hz for 12 min. The
> action time was divided into specified cycles: 3 s resting cycles
> with increasing intensity, 4 s intensity maintenance, 3 s decreasing
> intensity, and 5 s rest periods. The maximum amplitude reached 22 V
> (±30%) and the maximum current was 6 mA (±30%). Concurrently, heat
> therapy at 35 °C to 40 °C and micro-vibration were
> administered. Before the electrical stimulation, the probe vibrated
> to indicate the start signal. In the control group, only a vibration
> signal was present at the beginning, with no further
> stimulation. Patients underwent treatment twice daily, with each
> session lasting 12 min, for a duration of 10 weeks. During probe
> insertion, a small amount of lubricant gel was applied to the probe
> surface.

Five of their 20 electrostimulation-group patients got vaginal
infections and were excluded.  They say they got significant
improvements in various measures of sexual function, but by
“significant” they mean *p* = 0.0394 in some cases, and all the error
bars on their bargraph appear to overlap to me.  They got very
noticeable improvements in overactive-bladder symptoms in both the
control group and the electrostimulation group, presumably due to the
Kegel training.

The device itself [is described as follows][2] by a “Special Report”
by “Blog Magazine of Korean products, brands and Goods” in 02019:

> The class III medical stimulator, MK000A Dr. Lady, is a
> non-implanted neuromuscular electrical stimulator to relieve urinary
> incontinence and a low-frequency stimulator. The probe is inserted
> into the vagina to strengthen the pelvic floor muscle and vagina
> using frequency stimulation from 2 to 1200 Hz, thermotherapy, and
> vibration. In addition to urinary incontinence, it helps to cure
> menstrual cramps, sexual dysfunction, vagina atony, leukorrhea flow,
> uterus mass, genital cold, uterine myoma, menstrual pain,
> hemorrhoids, etc.

[Buheung Medical describes itself][3]:

> We, Buheung Medical Co., Ltd, that was founded on December of 1980,
> have focused on the Healthy & Happy Life of Human.

And [they describe the product][4]:

> MK-000A Dr. Lady is Therapeutic Electro Device for Women who has
> Incontinence, Sexual Disfunction, Vagina Atony, Leukorrhea Flow,
> Uterus Mass, Genital Cold, Uterine Myoma, Menstrual Pain,
> Hemorrhoids (Piles).
>
> The insertion Style Probe has 6 kinds of combinational Program with
> 3 Functions (Electro Frequency, Heating, and also Fine
> Vibration). These functions’ combination makes treatment efficacy to
> Incontinence, Sexual Disfunction, Vagina Atony, Leukorrhea Flow,
> Uterus Mass, Genital Cold, Uterine Myoma, Menstrual Pain,
> Hemorrhoids (Piles).
>
> By Contrast, outer electrodes’ Programs are specialized in Pain
> reduce in Menstrual Period. It has 2 programs and the frequencies
> for each program are very different to each other.
>
> Uniquely, this Therapeutic Medical Device is wearable. Very small
> and light (63mm x 88mm x 29mm / 76gram) so you can treat with easy
> living activities, at any time & any place.
>
> Furthermore, it is hidden in wears as inner wear, T-shirt, sweater,
> neat or skirt. You don’t have to stay beside huge device and also
> you don’t have to mind other people when you treat.
>
> We, Buheung Medical is very pleased to launch MK-000A Dr. Lady, the
> state of art medical Device and serving happier life to Ladies in
> the world.

I don’t know what 63mm × 88mm × 29mm means, given that the device is
almost precisely cylindrical (like anything you’d want to insert into
your vagina) but maybe 29mm is the diameter, and it looks like each of
the two electrodes is a bit longer than they are wide, which would be
about 14cm² of cylindrical surface area.  This implies a maximum
current density of about 400μA/cm².

Using an order of magnitude lower current than the Giuseppe et
al. experiment might explain why they saw virtually no effect.

[1]: https://www.komachine.com/en/companies/buheungmedic/products/109247-dr-lady-mk-000a
[3]: http://www.buheungmedical.com/english/main.php
[2]: https://korean-products.com/2019/05/31/therapeutic-electro-device/
[4]: http://www.buheungmedical.com/english/main.php?m1=29

Electrically produced orgasm
----------------------------

In [Electrostimulation is an effective and safe method for semen
collection in medium-sized lizards][6], López Juri, Chiaraviglio, and
Cardozo report success with electroejaculation from *Tropidurus
spinulosus*:

> Mature males of *T. spinulosus* were captured and their testicular
> volume was evaluated via portable ultrasound scanning. The lizards
> were electrostimulated by performing standardized series of stimuli.
> Semen was obtained successfully in 94% of the males.

This turns out to be the *lagarto de los quebrachales* or *serrucho*
lizard from Chaco province.  They used a similar probe to the one used
in intravaginal stimulation of humans, but it was significantly
smaller, because these lizards are only 117 mm [from snout to
“vent”][7] (which means cloacal opening):

> The metal cloacal probe is 3.5mm in diameter and 14 mm in length,
> and longitudinal electrodes are separated by 2mm (Figure 2). The
> lizards were electrostimulated by performing three series, each one
> consisting of five stimuli and each stimulus lasting five seconds
> (in the first second the stimulus ramps up from 0 to 1 V and in the
> other four seconds the stimulus is stable at 1 V). A period of a
> minimum of five seconds was allowed between each stimulus and
> between each series during the electrostimulation procedure to allow
> muscles to relax and sperm to flow.

However, they don’t say anything about the currents or current
densities they used.

[6]: https://www.sciencedirect.com/science/article/abs/pii/S0093691X18302723
[7]: https://en.wikipedia.org/wiki/Snout%E2%80%93vent_length
