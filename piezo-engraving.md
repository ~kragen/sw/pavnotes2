I realized recently that commonplace piezo elements probably make some
really powerful ultrasonic tools possible, including hand tools,
lightweight high-precision machine tools, and low-cost nanopositioners.

Commonplace piezo elements: characteristics and pricing
-------------------------------------------------------

Common piezoelectric stacks for things like ultrasonic cleaners resonate
at [something like 30 kHz][0], commonly require only tens of volts to run
them (I think), and are rated for things like 30 watts, suggesting that
each vibration does on the order of a millijoule of work.  I'm not sure
how far the movement is but commonly PZT maxes out at a few millistrains:
a few microns of movement per millimeter of thickness.  It's a quite
rigid material, with a Young's modulus in the neighborhood of 100 GPa,
half that of steel, so a maybe typical stack of 10 mm radius and 10 mm
thickness should have a rigidity of some 30 MN/mm.

[0]: https://hackaday.io/project/4689-improve-the-haber-process/log/16986-analysis-of-the-ebay-ultrasonic-power-supply

[Used ultrasonic cleaners for dentists or fuel injectors seem to cost
about US$40 on MercadoLibre][1].  New ones can be nearly as cheap or cost
2-5 times as much, mostly in proportion to their size.  Typically these
are brands like Yaxun, Langee, Codyson, GDK, Pitarch, Baku, or CTBrand
(e.g., Ya Xun YX3560 or Baku BK9050), they're called "batea ultrasonido"
(ultrasound sink) or "lavadora ultrasonica", they run at 30-42 kHz, and
they use 30-50 watts to clean a bit less than a liter; the largest models
use 220 watts to clean 5 liters, and I think the larger ones contain
two or more 50-watt "vibration heads", i.e., piezoelectric transducers
and horns.  Vendors include Electro-Tools Argentina and Autech.

[1]: https://listado.mercadolibre.com.ar/salud-y-equipamiento-medico/odontologico/lavadoras-ultrasonicas/usado/_PriceRange_0-10000

[On Aliexpress the relevant transducers cost US$11][6] or [US$8][7].
This last one comes with some specifications: 38mm diameter, and from
the proportions on the photo it looks like the piezo part is about
10mm thick.

[6]: https://www.aliexpress.com/item/1005002735029939.html
[7]: https://www.aliexpress.com/item/1005002953424355.html

Nanopositioners
---------------

Recently I saw a nanopositioner using a piezo stack sort of like that
with a couple of NdFeB magnets.  By continuously varying the voltage on
the piezo stack using a DAC you could get tens of microns of movement
down to nanometer precision, as the magnets would remain firmly stuck
to the (steel) stage, so the piezo actuator end, the magnets, and the
stage all move rigidly as a single unit.  But, if you drive it with
a sawtooth voltage, the discontinuities accelerate the magnets hard
enough that the stage can't keep up, and they slip along it, allowing
indefinitely large movements, millimeters or more.  This gives a very much
simplified inchworm drive, though it presumably needs external servoing.
They were doing inchworm movements at 5 Hz.

Inverting the setup, a body can use two such actuators at right angles
to crawl anywhere on a smooth ferromagnetic surface such as sheet metal
with unlimited range and nanometer precision, at least if it has enough
energy available, for example by being plugged in.  A third such actuator
would be needed to control rotation.

Off-the-shelf three-axis nanopositioners cost [US$1700][9] or more if
they're name-brand.

[9]: https://www.aliexpress.com/item/1005004204592036.html

Piezo metal engraving
---------------------

Years ago I had a "Vibro-Graver" that reciprocated a conical hardened
steel tip at 60 Hz (or 120 Hz?) with a house-current solenoid, a fraction
of a millimeter.  I used it to write my name on my metal tools.

I was watching a Clickspring video in which the author made some files
by punching triangular grooves into room-temperature soft steel with
a hardened steel punch.  Crudely approximating, the width and depth of
the grooves seems to be about 300 microns, the length about 30 mm, and
the energy of the hammer blows creating the grooves about 1 joule, the
hammer moving at about 1 m/s and thus stopping in about 300 microseconds,
thus having frequency components up to about 3 kHz.

So I suspect that you could plastically form pits in room-temperature soft
steel with a diameter and depth of 300 microns with about 10 millijoules,
hitting the metal with an electrical pulse rather than a physical hammer
blow, and a 30kHz resonant frequency should be plenty.  A 30 kHz resonant
frequency means that the actuator can go from its maximum length to its
minimum length in about 30 microseconds, but then it will ring back and
forth many times, so a slower edge of 100 microseconds or more might
provide better fidelity.  Doing some pre-emphasis and inverse filtering
might enable you to get a real 30-microsecond edge, or close to it.

You might not need to do the whole 300 microns in a single blow, which
is good because if your PZT is 20mm thick and produces 2 microstrains,
you only have 40 microns of deformation.  However, it's important that
the 40 microns or whatever exceed the elastic limit of the steel you're
trying to stamp it into, which I think mostly just means that the point
needs to be sharper than that, which is easy to do.

Using the ballpark of 30 MN/mm above, compressing the stack by 40 microns
without electricity should require about 1.3 meganewtons, the weight of
130 tonnes.  A36 steel's yield stress is only about 250 MPa, so this is
enough force to exceed the yield stress of a 40-mm-radius bar of steel,
such a huge amount that I wonder if I might have miscalculated
something.

Off-resonance you probably need a higher voltage, as explained in
Peter Walsh's project log page linked above.  [He has some further
recommendations for ultrasound piezo power supplies][5] some of which
may be relevant.  He mentions that the transducers on eBay have low Q,
which would be ideal for my purposes.

[5]: https://hackaday.io/project/4689-improve-the-haber-process/log/182303-lessons-learned

40 microns in 300 microseconds is only about 0.13 m/s, which seems like
a pretty leisurely speed for a hammer blow.  In 30 microseconds it's 1.3
m/s, which is a reasonable speed for a hammer blow.  200 microns in 25
microseconds would be 8 m/s, quite a rapid hammer blow.  I suspect my
"Vibro-Graver" was only hitting the steel at something like 10-30 mm/s.

If the rise time is not sharp enough, perhaps an air space can be
incorporated in the apparatus to sharpen the edge by applying nonlinear
distortion to it: an open air space of 20 microns can have very low
stiffness, but once it is closed, the surfaces contacting one another
will have much higher stiffness; this is the usual hammering action
associated with hammers of all kinds since the Paleolithic.

Piezo single-point metal cutting (toolheads hammering at kHz)
-------------------------------------------------------------

A potentially more exciting use of this kind of thing than forming a pit
is engraving a groove progressively along the surface of the metal, like
ultrasonic knives.  In [Clickspring Chris's later video of engraving his
Antikythera Mechanism replica backplate][2] he drives his burin across
the brass a fraction of a millimeter at a time with hammer blows, each
of about 20-40 mm and presumably about 1 N, for about 10-50 mJ per blow.
It takes him about 90 blows to cut a single groove between the months,
about 15 mm, so he's advancing about 100-200 microns per blow.  So
advancing 40 microns per cycle might be enough, but if not you can
presumably stack up 2-6 such piezo elements to get to 100-200 microns.

[2]: https://youtu.be/iBRCL090PxA

This is a very general process for shaping metal; single-point cutting
with a lathe, a boring head, a fly cutter, or a shaper is basically
just making a single scratch very fast to remove an arbitrarily large,
precisely controlled amount of metal.  The hammering-toolhead approach
eliminates the side loading that compromises the precision in these
processes and requires very low compliance in the machine structure,
making it large and heavy.  Chris explains that he prefers hammering
rather than pushing on the graver, the objective in either case being
tight control.

Possibly even if you aren't hammering at 40 kHz you ought to be able to
do orders of magnitude better than the 5-10 Hz a person can manage with
hammer blows, thus advancing at many millimeters per second.  If you
*can* manage to hammer at more than 20 kHz you might be able to make the
process silent, which would be a huge benefit.

Advancing 200 microns at 30 kHz, which seems like the most we can hope
for with machines like the ones I'm describing, would be 6 m/s, 360000
mm/minute, or, in medieval units, 1200 surface feet per minute; this
would be plenty fast to compete with conventional single-point cutting,
which is normally done at 30-800 SFM, depending on the material and the
cutter.  Advancing 40 microns at 3 kHz, which seems like it should be
eminently doable if 40 microns isn't below the elastic limit, would be
0.12 m/s, 7200 mm/minute, or almost 24 surface feet per minute: a bit
slow but still fast enough to be practical if it has other compensating
advantages.

Other uses for piezo hammering
------------------------------

Shearing or nibbling sheet metal with an electric shear or nibbler driven
in such a way may also be a viable thing to do, as is hammer-drilling,
metal sawing, and screwdriving.  Screwdriving might be viable with just a
square wave; the direction the user twists the sonic screwdriver in the
slot or head of the screw determines which direction the screwdriver is
able to exert enough force to overcome the screw's stiction.

It would be great if you could do that for naildriving too but I think
nails (and wood screws) will normally require displacements too large
to exceed the wood's elastic limit for piezo actuators to be usable.
You'd need some kind of gearing-down mechanism that gives you over a
millimeter of travel for that, without introducing too much compliance.
