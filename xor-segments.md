I’ve been thinking about how to reduce bounds checking overhead from
software implementations of memory-safe virtual machine designs.  The
basic approach to safe virtual machine designs is to hand out access
to memory segments (arrays) as unforgeable capabilities rather than
raw memory pointers to which you can do pointer arithmetic, and
provide an array-indexing operation in the virtual machine.

Problem: a straightforward implementation of this approach requires an
index bounds check and a conditional jump on every memory access,
something like this:

    # indexed fetch: segment descriptor in %rdx, index in %rsi
    mov (%rdx), %rax          # load length from first word of segment
    cmp %rsi, %rax
    ja bounds_check_fail      # or jbe 1f; call bounds_check_fail
    mov 8(%rdx,%rsi,8), %rdx  # index into segment, offset to skip length

Rather than just:

    mov 8(%rdx,%rsi,8), %rdx

Maybe if your compiler is smart enough it can hoist the index bounds
check out of most of your inner loops, and static type checking can
eliminate the bounds check in cases where the index is statically
known and within the size of the type being accessed, but these
complicate a basic implementation enormously.

Possible alternative to static type checking: provide two array
indexing instructions, one with a statically known small index and
another one with a potentially large index, and then ensure that all
memory segments are at least, say, 8 or 16 words.  Then, the
implementation of the first instruction doesn't need a bounds check.

Problem: 8 words is a lot of overhead for something like a cons.  Even
one word is, actually.

Possible solution: include a 3-bit XOR "offset" field in the segment
descriptor which is XORed with the (statically known) offset.  This
allows you to 