I was looking at [my old notes about debugging executables under
FreeDOS][0] and found that I don't know how to do it any more, and the
old commands don't quite work.  So I went through the process again
and took notes.

[0]: http://canonical.org/~kragen/demo/fr-016.html

Installing `qemu-system`
--------------------------

Now the command is `qemu-system-i386` rather than just `qemu`.  This
involved `apt install qemu-system-x86`, which also installs an amd64
emulator, called `qemu-system-x86_64`.

Building an MS-DOS floppy disk image with `mkfs.msdos` and `mtools`
----------------------------------------------------------------------

I built a floppy disk image:

    $ dd </dev/zero >floppy.img bs=1k count=1440
    1440+0 records in
    1440+0 records out
    1474560 bytes (1,5 MB, 1,4 MiB) copied, 0,0127822 s, 115 MB/s
    $ mkfs -t msdos floppy.img
    mkfs.fat 4.1 (2017-01-24)
    $ mcopy -i floppy.img ~/dev3/morecircles.com ::morecirc.com
    $ mdir -i floppy.img ::
     Volume in drive : has no label
     Volume Serial Number is 8136-73DA
    Directory for ::/

    morecirc com        64 2023-07-01  21:48 
            1 file                   64 bytes
                              1 457 152 bytes free

The .COM file really is 64 bytes; it's a 64-byte demo I wrote some
years ago.  `mcopy` and `mdir` are from the `mtools` package, version
4.0.24; you can also use `mount -o loop` to use the FAT driver in the
kernel instead.

Installing FreeDOS
------------------

I downloaded the [FreeDOS 1.3 LiveCD][2] from the [FreeDOS download
page][1]:

    $ sha1sum FD13-*
    7fd855190b3798fe0f3dd75605c0a1c605c4f0c9  FD13-FloppyEdition.zip
    ca602f2aca912483fdb3474ec8e6654b3cb9b839  FD13-LiveCD.zip
    $ mkdir freedos13
    $ cd freedos13
    $ unzip ../FD13-LiveCD.zip 
    Archive:  ../FD13-LiveCD.zip
      inflating: FD13BOOT.img            
      inflating: FD13LIVE.iso            
      inflating: readme.txt              
    $ 

[1]: https://www.freedos.org/download/
[2]: https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/distributions/1.3/official/FD13-LiveCD.zip

This was sufficient to enable FreeDOS to boot using this command
cobbled together from the manual page:

    $ qemu-system-i386 --drive media=cdrom,readonly,file=FD13LIVE.iso 

I selected the option to boot FreeDOS in "Live Environment" mode, and
upon `cd games\flpybird` I could run `flpybird`, so evidently it more
or less works.  The FreeDOS `exit` command does not work; I quit by
typing ctrl-alt-2 to get the `(qemu)` prompt and typing `quit`.

Attempting to mount a directory with `-fda fat:floppy:.` fails with a
mysterious error about `isa-fdc`:

    $ mkdir floppy
    $ cp ~/dev3/morecircles.com floppy/.
    $ qemu-system-i386 --drive media=cdrom,readonly,file=FD13LIVE.iso -fda fat:floppy:./floppy
    WARNING: Image format was not specified for 'json:{"fat-type": 0, "dir": "./floppy", "driver": "vvfat", "floppy": true, "rw": false}' and probing guessed raw.
             Automatically detecting the format is dangerous for raw images, write operations on block 0 will be restricted.
             Specify the 'raw' format explicitly to remove the restrictions.
    qemu-system-i386: Initialization of device isa-fdc failed: Block node is read-only

However, if I try to mount the floppy disk with qemu-system's `-fda`
option, it insists on trying to boot from the floppy (here I used the
`-curses` option to qemu to get copy and paste, later `-display
curses`, but I couldn't figure out how to quit, and in particular ^A h
doesn't work, so I killed it from another terminal):

    SeaBIOS (version 1.13.0-1ubuntu1.1)


    iPXE (http://ipxe.org) 00:03.0 CA00 PCI2.10 PnP PMM+07F8CB00+07ECCB00 CA00



    Booting from Hard Disk...
    Boot failed: could not read the boot disk

    Booting from Floppy...
    This is not a bootable disk.  Please insert a bootable floppy and
    press any key to try again ...

[The qemu bootindex documentation][3] says that there is a `-boot
order=` option, and that if you give a `-drive` option you can set a
`bootindex` property, giving the following example:

    qemu-system-x86_64 -drive file=disk1.img,if=none,id=disk1 \
                  -device ide-hd,drive=disk1,bootindex=4 \
                  -drive file=disk2.img,if=none,id=disk2 \
                  -device virtio-blk-pci,drive=disk2,bootindex=3 \
                  -netdev type=user,id=net0 \
                  -device virtio-net-pci,netdev=net0,bootindex=2 \
                  -netdev type=user,id=net1 \
                  -device e1000,netdev=net1,bootindex=1

[3]: https://www.qemu.org/docs/master/system/bootindex.html

With this hint and with the error messages from attempting to use the
undocumented `-boot order=` option, I was able to get it to boot from
the simulated CD-ROM even though there was a floppy image:

    $ qemu-system-i386 --drive media=cdrom,readonly,file=FD13LIVE.iso \
        -fda floppy.img -boot order=d

For some reason my simulated floppy was on B: rather than A:, but it
does run the demo now.  (However, the demo crashes, but that's a
different issue.)

Connecting GDB to `qemu` and FreeDOS programs
----------------------------------------------

I used ctrl-alt-2 in qemu (4.2.1) to get to the qemu console and run
the `gdbserver` command, though picking a nondefault port because
something is already using 1234:

    (qemu) gdbserver tcp::1235
    Waiting for gdb connection on device 'tcp::1235'
    (qemu) 

This does not actually pause qemu execution, though.

Then I started GDB in a different terminal.

    $ gdb
    GNU gdb (Ubuntu 9.2-0ubuntu1~20.04.1) 9.2
    Copyright (C)...
    (gdb) target remote localhost:1235
    Remote debugging using localhost:1235
    warning: No executable has been specified and target does not support
    determining executable automatically.  Try using the "file" command.
    0x0000e82e in ?? ()
    (gdb) set architecture i8086
    The target architecture is assumed to be i8086
    (gdb) info registers
    eax            0x11100             69888
    ecx            0x7fd0001           134021121
    edx            0x10000             65536
    ebx            0xee00035a          -301989030
    esp            0xa16               0xa16
    ebp            0x90a36             0x90a36
    esi            0x62c               1580
    edi            0xa52               2642
    eip            0xe82e              0xe82e
    eflags         0x6                 [ IOPL=0 PF ]
    cs             0xf000              61440
    ss             0xd9                217
    ds             0xd9                217
    es             0xd9                217

(omitting most registers here, especially the SSE ones)

    mxcsr          0x1f80              [ IM DM ZM OM UM PM ]
    (gdb) display/i $pc
    1: x/i $pc
    => 0xe82e:	incl   (%esi)
    (gdb) ni
    0x0000e834 in ?? ()
    1: x/i $pc
    => 0xe834:	add    0x5a5e5dec(%ecx),%cl

After a few more `ni`s (though I think GDB is decoding the 16-bit-mode
instructions incorrectly) my FreeDOS prompt started working again and
I was able to run the demo, which crashed at a different point, and I
hit ^C in GDB.  But the registers look like total bullshit:

    ^C
    Program received signal SIGINT, Interrupt.
    0x00002181 in ?? ()
    1: x/i $pc
    => 0x2181:	inc    %ecx
    (gdb) bt
    #0  0x00002181 in ?? ()
    (gdb) info regs
    Undefined info command: "regs".  Try "help info".
    (gdb) info registers
    eax            0x1200              4608
    ecx            0xff3f              65343
    edx            0x1                 1
    ebx            0x0                 0
    esp            0x7aa               0x7aa
    ebp            0x0                 0x0
    esi            0x0                 0
    edi            0xfdcc0000          -36962304
    eip            0x2181              0x2181
    eflags         0x82                [ IOPL=0 SF ]
    cs             0xd88e              55438
    ss             0x422               1058
    ds             0x0                 0
    es             0x0                 0

It doesn't make any sense that IP would be 0x2181 in a 64-byte .COM
file, it doesn't make sense that DS would be 0, it doesn't make sense
that SP would be 0x07aa, it doesn't make sense that SS would be
different from CS.  But qemu itself confirms these:

    (qemu) p $eip
    0x2181
    (qemu) p $ds
    0
    (qemu) p $ss
    0x422
    (qemu) p $sp
    0x7aa

However, it also confirms that GDB is decoding instructions wrong:

    (qemu) x/20i $eip
    0x00002181:  41                       incw     %cx
    0x00002182:  4e                       decw     %si
    0x00002183:  49                       decw     %cx
    0x00002184:  43                       incw     %bx
    0x00002185:  3a 20                    cmpb     (%bx, %si), %ah
    0x00002187:  00 0a                    addb     %cl, (%bp, %si)
    0x00002189:  53                       pushw    %bx
    0x0000218a:  79 73                    jns      0x21ff
    0x0000218c:  74 65                    je       0x21f3
    0x0000218e:  6d                       insw     %dx, %es:(%di)
    0x0000218f:  20 68 61                 andb     %ch, 0x61(%bx, %si)
    0x00002192:  6c                       insb     %dx, %es:(%di)
    0x00002193:  74 65                    je       0x21fa
    0x00002195:  64 00 5e 43              addb     %bl, %fs:0x43(%bp)
    0x00002199:  0d 0a 00                 orw      $0xa, %ax
    0x0000219c:  00 2e 20 20              addb     %ch, 0x2020
    0x000021a0:  20 4e 49                 andb     %cl, 0x49(%bp)
    0x000021a3:  43                       incw     %bx
    0x000021a4:  3a 00                    cmpb     (%bx, %si), %al
    0x000021a6:  0a 53 79                 orb      0x79(%bp, %di), %dl

Versus the incorrect GDB decoding, which GDB used to get correct in
this configuration:

    (gdb) x/20i $eip
    => 0x2181:	inc    %ecx
       0x2182:	dec    %esi
       0x2183:	dec    %ecx
       0x2184:	inc    %ebx
       0x2185:	cmp    (%eax),%ah
       0x2187:	add    %cl,(%edx)
       0x2189:	push   %ebx
       0x218a:	jns    0x21ff
       0x218c:	je     0x21f3
       0x218e:	insl   (%dx),%es:(%edi)
       0x218f:	and    %ch,0x61(%eax)
       0x2192:	insb   (%dx),%es:(%edi)
       0x2193:	je     0x21fa
       0x2195:	add    %bl,%fs:0x43(%esi)
       0x2199:	or     $0x2e00000a,%eax
       0x219e:	and    %ah,(%eax)
       0x21a0:	and    %ah,(%eax)
       0x21a2:	and    %ah,(%eax)
       0x21a4:	and    %ah,(%eax)
       0x21a6:	and    %ah,(%eax)

I tried setting a breakpoint at the .COM file entry address, but
couldn't figure out how to reboot qemu (the qemu prompt command is
`system_reset`):

    (gdb) b *0x100
    Breakpoint 1 at 0x100
    (gdb) c
    Continuing.
    [Inferior 1 (process 1) exited normally]
    (gdb)

So I restarted qemu, with all the song and dance, and connected GDB to
it:

    (gdb) target remote localhost:1235
    warning: A handler for the OS ABI "GNU/Linux" is not built into this configuration
    of GDB.  Attempting to continue with the default i8086 settings.

    Remote debugging using localhost:1235
    warning: No executable has been specified and target does not support
    determining executable automatically.  Try using the "file" command.

    Program received signal SIGTRAP, Trace/breakpoint trap.
    0x0000e82e in ?? ()
    1: x/i $pc
    => 0xe82e:	incl   (%esi)
    (gdb) c
    Continuing.
    ^C
    Program received signal SIGINT, Interrupt.
    0x0000e82e in ?? ()
    1: x/i $pc
    => 0xe82e:	incl   (%esi)
    (gdb) b *0x100
    Note: breakpoint 1 also set at pc 0x100.
    Breakpoint 2 at 0x100
    (gdb) c
    Continuing.

However, that did not help at all; the program did not hit the
breakpoint when I started it.

### Trying to test with fr-016 (bytes) ###

Maybe my demo is crashing and FreeDOS is exiting it and not restoring
the video mode.  Let's try Farbrausch's fr-016 (bytes), which is a
16-byte demo:

    $ echo "oh, hello, worl" | xxd | tee fr-016.hex
    00000000: 6f68 2c20 6865 6c6c 6f2c 2077 6f72 6c0a  oh, hello, worl.
    $ vi fr-016.hex
    $ cat fr-016.hex
    00000000: b013 cd10 c42f aa11 f864 1306 6c04 ebf6  ................
    $ xxd -r < fr-016.hex | tee fr-016.com | xxd
    00000000: b013 cd10 c42f aa11 f864 1306 6c04 ebf6  ...../...d..l...
    $ objdump -m i8086 -b binary -D fr-016.com 

    fr-016.com:     file format binary


    Disassembly of section .data:

    00000000 <.data>:
       0:	b0 13                	mov    $0x13,%al
       2:	cd 10                	int    $0x10
       4:	c4 2f                	les    (%bx),%bp
       6:	aa                   	stos   %al,%es:(%di)
       7:	11 f8                	adc    %di,%ax
       9:	64 13 06 6c 04       	adc    %fs:0x46c,%ax
       e:	eb f6                	jmp    0x6

Hmm, looks like my objdump can still disassemble 8086 code, even if
GDB is broken somehow.

    $ mcopy -i floppy.img fr-016.com ::
    $ mdir -i floppy.img ::
     Volume in drive : has no label
     Volume Serial Number is 8136-73DA
    Directory for ::/

    morecirc com        64 2023-07-01  21:48 
    fr-016   com        16 2023-07-01  23:15 
            2 files                  80 bytes
                              1 456 640 bytes free

I am able to run this in FreeDOS but it crashes fairly quickly with a
message not like anything I've ever seen before, which is superimposed
on top of the MCGA mode 13h graphics screen; my possibly-erroneous
transcription:

    Interrupt divide by zero, stack:
    02D3 9F00 0883 D05B F000 0203 D002 F000
    DB80 0000 F832 0000 20CD
    dos mem corrupt, first_mcb=0282
    prev 0bbb:0000|4d bc 0b c3 92 1e fc 00 4
    6 52 2d 30 31 36 00 00 M*.*****FR-016..
    notMZ9e7f:0000|40 61 83 a6 ca ef 15 3c 6
    4 8d b7 e2 0e 3b 69 98 @a****.<d***.;i*
    
    PANIC: MCB chain corrupted
    System halted

The stack there has a suspicious CD 20 instruction in it.

FR-016 doesn't contain any division instructions and only writes to
video RAM, so I feel like this is probably a bug in FreeDOS or (less
likely) qemu.

### Finally success with a minimal .COM executable ###

Let's try this with just an infinite loop.  Can FreeDOS crash with
just a one-instruction infinite loop?  I think this is the only whole
8086 program I've ever written directly in hexadecimal, though I
embedded some i386 hex instructions in StoneKnifeForth.

    $ vi iloop.hex
    $ cat iloop.hex
    00000000: ebfe                                     x.
    $ xxd -r < iloop.hex | tee iloop.com | xxd
    00000000: ebfe                                     ..
    $ objdump -m i386 -b binary -D iloop.com 

    iloop.com:     file format binary


    Disassembly of section .data:

    00000000 <.data>:
       0:	eb fe                	jmp    0x0
    $ mcopy -i floppy.img iloop.com ::
    $ qemu-system-i386 --drive media=cdrom,readonly,file=FD13LIVE.iso \
        -fda floppy.img -boot order=d

It's worth noting at this point that all three of these .COM programs
do the right thing in DOSBox.

This program *does* successfully run in FreeDOS in `qemu`, and I can
connect GDB to it as before, getting a much more sensible-looking
register dump:

    (gdb) target remote localhost:1235
    Remote debugging using localhost:1235
    warning: No executable has been specified and target does not support
    determining executable automatically.  Try using the "file" command.
    0x00000100 in ?? ()
    (gdb) info registers
    eax            0x10000             65536
    ecx            0x7fd00ff           134021375
    edx            0x10bbc             68540
    ebx            0xee000000          -301989888
    esp            0xfffe              0xfffe
    ebp            0x9091e             0x9091e
    esi            0x100               256
    edi            0xfffe              65534
    eip            0x100               0x100
    eflags         0x202               [ IOPL=0 IF ]
    cs             0xbbc               3004
    ss             0xbbc               3004
    ds             0xbbc               3004
    es             0xbbc               3004
    fs             0xbbc               3004
    gs             0x4ba               1210

So the $ip is 0x100 as it ought to be, $ax and $bx are zero, $cx and
$dx and $bp and $si and $di have garbage in them, $sp is 0xfffe as one
might expect, and all of $cs, $ss, $ds, $es, and $fs (though not $gs!)
are set to the same segment!  This looks like a real register dump
from a .COM file.

The GDB `x` command does the wrong thing with $eip:

    (gdb) x/2bx $eip
    0x100:	0x59	0xec

Presumably that's at absolute 0x100, not 0x100 within $cs.  This is
better:

    (gdb) x/2bx $eip + $cs * 16
    0xbcc0:	0xeb	0xfe

### Different ways of exiting MS-DOS programs ###

So now I can finally ask the question I've been trying to ask.  What
is on the stack?!  Because a DOS program is entitled to terminate by
just invoking RET.

    (gdb) x/2bx $esp + $ss * 16
    0x1bbbe:	0x00	0x00

The stack has a 0 on it, okay.  And what's at address 0?

    (gdb) x/256bx $cs * 16
    0xbbc0:	0xcd	0x20	0x7f	0x9e	0x00	0x9a	0xf0	0xfe
    0xbbc8:	0x1d	0xf0	0x52	0x07	0xba	0x04	0x3f	0x06

...

    0xbcb8:	0x5c	0x00	0x00	0x00	0x00	0x00	0x00	0x00

CD 20.  Interrupt 20h.  That's the program-exit interrupt.

So if you RET or JMP 0 you go to a piece of code which exits the
program by invoking `int $0x20`, at least in FreeDOS.  That's the same
behavior as in emu8086.
