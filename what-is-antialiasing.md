What’s the general principle behind graphics antialiasing?  Is it
intuition or mathematics?

The general mathematical principles are the Shannon-Nyquist theorem
and the convolution theorem, although you might think it's just a
coincidence of terminology.

Basically, you can think of a pixel grid as a two-dimensional sampling
grid.  If each pixel is a tiny point of light at the center of a
square tile, you can adjust these brightnesses of light to approximate
your scene.  The Nyquist sampling theorem says that if you use the
actual brightness of the scene you're trying to draw at that point, as
if you were looking at it through a metal plate with a grid of tiny
holes in it, you can reconstruct the entire scene from that set of
samples — as long as the scene is *bandlimited* to a band of no more
than the Nyquist limit.

One way to do this, the usual way, is to restrict it to spatial
frequencies whose wavelength is less than twice the pixel spacing,
which means that your scene can't contain too much detail.  But actual
scenes can contain arbitrarily large amounts of arbitrarily fine
detail, which correspond (mostly!) to high spatial frequencies.  The
sampling operation (looking at the scene through a plate full of
infinitely tiny holes) “aliases” these high spatial frequencies down
into low spatial frequencies.

You’ve probably seen this effect with actual physical metal plates
with holes in them, or netting, or transparent cloth: if you have two
such regular patterns next to each other, you can get these big
“moiré” patterns, which look like large objects, sometimes moving at
unreasonable speeds or in the wrong parallax direction (as if they
were behind your eyes).

You can understand this mathematically through the convolution
theorem.  Looking at a scene through a black plate with holes in it
gives you the image you get by *multiplying* the scene pointwise by
the spatially varying transparency of the plate.  The convolution
theorem says that such pointwise multiplication in the spatial domain
gives you an image that is the inverse Fourier transform of the
convolution of the Fourier transforms of the two images you're
multiplying.  This is a continuous-space theorem; it's not limited to
discrete-space signals like sampled images.

As it happens, the Fourier transform of a grid of infinitely tiny
impulses of equal amplitude (your sampling array) is another infinite
grid of infinitely tiny impulses.  (“The Fourier transform of a Dirac
comb is another Dirac comb.”)  That comb or grid tells you *which*
baseband spatial frequencies the high spatial frequencies will alias
down to.  If you have detail that isn't periodically repeating, it
generally just looks like a random jumble, so it aliases down into
baseband noise, so it just reduces the quality of your image, but if
you have a repeating pattern (like a striped shirt or a grid) you can
get very noticeable moiré patterns.

So, if you would rather have your sampled image be a good
approximation of what you'd see with your eyes, you have to filter out
those high spatial frequencies from your scene before you sample it.
So, graphics antialiasing techniques all boil down to applying a
low-pass or blurring filter over your scene, so that each pixel gets
assigned a sort of area average color rather than the color precisely
sampled at a single point.

In general, calculating the average color over an area is more
expensive than calculating a single color sample, so the different
antialiasing algorithms represent different tradeoffs between
efficiency and quality, tradeoffs that have improved over time as
better algorithms have been discovered.
