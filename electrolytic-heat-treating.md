Diamond abrasion tools are made, as I understand it, by
electrodeposition of iron throughout diamond sand.  The iron fills up
the spaces between the diamonds, holding them in place and eventually
engulfing them completely.  I forget the name for this process.

Hovestad and Janssen call it “electroplating of metal matrix
composites by codeposition of suspended particles”, “particle
codeposition”, or “electrolytic composite plating”:

> It is well known that insoluble substances present in an
> electroplating bath codeposit with the metal and become incorporated
> in the deposits.  In conventional electroplating processes various
> bath purification methods, like continuous filtering and anode
> bagging, are employed to avoid incorporation of insoluble anode
> debris (oxide, alloying elements), airborne dust and solid drag-out
> from pre-treatment baths. These incorporated substances generally
> have a strong adverse effect on the deposit properties.  The
> advantages of second phase material incorporated in metallic
> deposits were realized only later.

See also their 10.1007/BF00573209, [“Electrochemical codeposition of
inert particles in a metallic matrix”][10].

[10]: https://pure.tue.nl/ws/portalfiles/portal/1584410/617544.pdf

Electrolytically making heat-hardenable wrought iron by carbon particle codeposition
------------------------------------------------------------------------------------

What if, instead of engulfing sand-sized diamonds, you were engulfing
tiny specks of amorphous carbon suspended in the electrolyte, of, say,
10μm diameter?  Perhaps you could adjust the mass fraction so that the
final product was 99.1% electrolytically pure iron and 0.9% amorphous
carbon dust, which means about 4% carbon dust by volume.

This puts the specks of carbon dust about 19μm apart (consider a
5μm-radius sphere of carbon at the center of a 15μm-radius sphere of
iron; it occupies about 4% of the iron sphere’s volume and has about
0.9% of its mass, and if two such spheres are touching, their carbon
cores will be about 19μm apart).  A packed pile of such spheres is a
reasonable approximation to our product.  So all of the iron is within
about 20–30μm of some carbon.

In this form, the iron should still be soft, like wrought iron,
because the carbon is not very reactive in the cold alkaline
electrolyte bath, so cementite phases cannot form.  This soft iron is
malleable and can be ground and otherwise cut.  It should be slightly
weaker than pure iron, as the carbon particles are weaknesses in the
composite, and they may give rise to stress concentrations.

However, if the iron is heated, the carbon will diffuse into it, since
the electrolytic iron is almost completely devoid of carbon, and the
total quantity of carbon is far lower than the 3.0–3.6% in [ductile
cast iron][0].  This will transform the iron into a hard, high-carbon
steel, in much the same way as case hardening does.  If this is done
above [the critical eutectoid temperature of about 727°][1], the
carbon is diffusing through an austenite structure in which it has
high solubility, and which can be hardened to martensite, pearlite, or
bainite by cooling to room temperature.

[0]: https://en.wikipedia.org/wiki/Ductile_iron
[1]: https://en.wikipedia.org/wiki/Austenite

This is essentially the opposite of precipitation hardening: instead
of hardening the material by precipitating hard crystals inside of it,
you’re hardening the material by dissolving crystals into it.  This is
possible because electrochemistry can easily make metastable material
mixtures with a very long lifetime.

Conventional case-hardening happens at almost 1mm/√hour.  As [Johannes
Nyman explains in his thesis][2], p. 5 (p. 12/48):

> The relationship between case depth and carburizing time can be seen
> as an inverse square relationship. To double the case depth obtained
> in four hours, the time needs to be approximately quadrupled. \[5\]

The plot he reproduces from his reference 5 shows, for example, a case
depth of about 0.06 “inches” after 12 hours of carburizing at “1600°
F”.  In modern units, this works out to 1.5mm at 870° and
0.44mm/√hour.

Reference 5, incidentally, is Rakhit and A. K. (2000). ‘Carburizing
and hardening gears’. _Heat Treatment of Gears, A Practical Guide for
Engineers_. OH, USA: ASM International.

[2]: http://www.diva-portal.se/smash/get/diva2:1352113/FULLTEXT01.pdf (Optimization of case-hardening depth for small gears, degree project in mechanical engineering, KTH, Stockholm, Sweden)

So, if we want each carbon particle to harden the iron around it to a
depth of 25μm, we could need to heat the metal to 870° for about
0.0033 hours, about 12 seconds (.44mm/√hour * √(.0033 hour) = 25μm).
This is a period of time long enough to permit conventional machining
operations, in which any individual piece of the metal is only heated
to tempering temperatures for a few seconds and only heated to glowing
temperatures for fractions of a second, but also short enough to be
very easily achievable, for example even with a blowtorch or induction
coil.

(1.5mm / .025mm = 60 times deeper case depth, meaning that the time
ratio should be 60² = 3600, which is indeed the ratio between 12
seconds and 12 hours.)

The standard quantity for this kind of thing is “[mass
diffusivity][8]”, measured in m²/s, which is said to follow the
Arrhenius equation in solids.  If we suppose that every 10° means a
factor of 2 reduction in time, our 12 seconds at 870° would become 2⁸⁵
times longer at a room-temperature 20°, about 15 quintillion years,
about a billion times longer than the current age of the universe.  So
it seems likely that this heat-hardenable composite would be, in
practical terms, stable.

[8]: https://en.wikipedia.org/wiki/Mass_diffusivity

So, is this a thing people are already doing?
---------------------------------------------

Powder metallurgy commonly uses electrolytically pure iron powder in
hot isostatic pressing; Höganäs is a popular supplier.  Do people mix
it with carbon powder to make steel?  I think so; [this paper by
Stamula and Pilarczyk][3] says:

> Carbon is a key element in powder metallurgy and is the basic
> alloying element in PM sintered steel. Carbon powders are also used
> as the carbon source to produce hard metals. Carbon allotropes such
> as graphite are mixed into iron or iron-based powders with lubricant
> powders and sometimes other additives during the preparation of
> powder mixes. Graphite is almost fully dissolved during sintering
> into the iron matrix \[1\].

[3]: https://yadda.icm.edu.pl/baztech/element/bwmeta1.element.baztech-0da90fe1-ef98-41b2-b732-6013124164d8/c/Stanula_jamme_2022_114_1.pdf

That’s clumsy enough to leave me doubting.  Their reference 1 is:

> R. Gilardi, L. Alzati, R. Oro, E. Hryha, L. Nyborg, S. Berg,
> L. Radicchi, Reactivity of Carbon Based Materials for Powder
> Metallurgy Parts and Hard Metal Powders Manufacturing, Journal of
> the Japan Society of Powder and Powder Metallurgy 63/7 (2016)
> 548-554. DOI: <https://doi.org/10.2497/jjspm.63.548>

Gilardi et al.’s paper is on ResearchGate and [on JStage][5], open
access under CC BY-NC-ND.  Its abstract begins:

> Carbon is a key element for powder metallurgy. For example, carbon
> is the basic alloying element in PM sintered steel, and carbon
> powders are used as a carbon source for the production of hard
> metals.

and that text is what Stamula and Pilarczyk plagiarized, which is
perhaps why Stamula and Pilarczyk’s paper seems like it doesn’t quite
make sense.

Gilardi et al. did two experiments, one to make tungsten carbide and
the other to make steel, starting with a prealloyed
iron/chromium/molybdenum powder and 0.5% of six different forms of
carbon, which they sintered in an inert atmosphere; carbon black was
totally inert up to 900° (failing to reduce surface oxides) and then
totally dissolved into the iron between 900° and 1120°.

[5]: https://www.jstage.jst.go.jp/article/jjspm/63/7/63_15-00089/_html/-char/en

[PM Review says][4]:

> Low alloy ferrous Powder Metallurgy materials are predominant in the
> Press/Sinter structural parts sector. In this sector, these
> materials are generally based on water atomised or sponge iron
> powders with elemental alloying additions.
>
> In common with wrought steels, a significant element in increasing
> strength is carbon, added as graphite.

But then it talks about how nickel, copper, and molybdenum diffuse
into the iron particles, not carbon.  It also mentions *why* they use
pure iron instead of pre-alloyed powder:

> A range of low alloy steel powder grades is available (...) where
> all alloying elements are fully pre-alloyed. These grades have been
> most commonly used in Powder Forging, where the lower
> compressibility in compaction is not a significant detriment.

So again I’m left doubting.

[4]: https://www.pm-review.com/introduction-to-powder-metallurgy/powder-metallurgy-materials/

Imerys Graphite & Carbon Switzerland sells [graphite powder for powder
metallurgy in order to reduce your iron oxide and diffuse into your
iron][6]:

> * Enhanced iron oxides reduction activity for primary synthetic
>   graphite compared to natural graphite, therefore carbon diffusion
>   starts at lower temperatures
>
> (...)
>
> KEY BENEFITS – PRIMARY SYNTHETIC GRAPHITE:
>
> IMPROVED MECHANICAL PERFORMANCE: Synthetic graphite diffuses
> completely before Cu diffusion, resulting in higher Sintered Density
> HV, TS [presumably hardness value and tensile strength, because
> tensile strength and Vickers hardness are plotted in bar graphsb
> below] when compared to natural graphite.
>
> (...)
>
> KEY BENEFITS – SYNERGIES BETWEEN SYNTHETIC GRAPHITE & CARBON BLACK:
>
> (...)
>
> * Unreacted graphite is visible on fracture surfaces for the coarser
>   grades, especially TIMREX® PG25 (...)
>
> * No carbon black residue is observed on the fracture surface,
>   indicating its full dissolution

[6]: https://www.imerys.com/public/2022-03/Specialty-Carbons-for-Powder-Metallurgy.pdf (Specialty Carbons for Powder Metallurgy)

So it seems like the idea is that you incorporate this graphite and/or
carbon black powder into your metal powder mix, and it mostly or fully
dissolves into the metal while you’re sintering it.  PG25 has a 25μm
“d90”, meaning that 90% of particles are less than 25μm, which is what
makes this a “coarser grade” than PG10, F10, or KS4, which are 10μm,
10μm, or 4μm respectively.

(Incidentally, looking for this, I also found someone talking about
making diamond grinding tools by powder metallurgy with diamond and
cobalt.)

[Zhao, Chen, Li, Peng, and Yan described a porosity-reduction
experiment][7] where they mixed varying amounts of graphite powder
with Höganäs Astaloy85Mo pre-alloyed iron powder containing molybdenum
and copper, HIPped it at 1120°, and then tried further densifying the
surface with a rolling process.  Presumably the idea was that
everything before the rolling process was standard powder metallurgy.

[7]: https://www.mdpi.com/2075-4701/8/2/91/pdf (Effect of Carbon Content on the Properties of Iron-Based Powder Metallurgical Parts Produced by the Surface Rolling Process, CC BY, Metals 2018, 8, 91; doi 10.3390/met8020091)

[Tarakci et al.][9] report a weird family of case-hardening techniques
using carbon-rich aqueous electrolytes, the “electrolytic
surface-hardening process” due to Luk: you insulate the workpiece with
a hydrogen film until the surface is hot, then cut off the power and
let the electrolyte quench it.  I found this paper looking for whether
other people were doing this carbon particle deposition thing already,
and at first I thought it might be relevant, but it’s something
totally different.

[9]: https://digital-library.tripod.com/journals/other/Plasma.pdf (Plasma electrolytic surface carburizing and hardening of pure iron)

### Nielsen et al. are directly depositing iron carbide with citric acid ###

[An article by Jacob Obitsø Nielsen, Per Møller, and Karen
Pantleon][11] reports codepositing iron from iron sulfate and carbon
from citric acid, but they’re directly depositing a high-hardness iron
carbide coating, not codepositing inert materials that can form iron
carbide through later heat treatment.

[11]: https://backend.orbit.dtu.dk/ws/portalfiles/portal/200743982/Electrodeposition_of_Iron_with_Co_deposition_of_Carbon_On_the_Nature_of_Nanocrystalline_Fe_C_Coatings_as_published_to_Orbit.pdf

Having glanced at the literature makes this seem more plausible
---------------------------------------------------------------

So it seems like carbon is very commonly processed this way in powder
metallurgy, but there it has the additional burden that the carbon is
separated from the metal particles by an oxide layer which must be
reduced first.  The electrolytic process would avoid this problem;
cathodic protection would prevent the iron oxide layer from forming.
Also, it wouldn’t necessarily suffer from the porosity inherent to
powder metallurgy.

The temperatures and times used in powder metallurgy are higher than
the ones I suggested above.  I suspect that this is not so much in
order to facilitate the diffusion of carbon into iron as it is to
soften the iron powder so it can be sintered.

Alternative fabrication
-----------------------

Greg Sittler pointed out that it might be possible to do powder-bed
3-D printing of such a mixture of iron and carbon powders using SLM
(selective laser melting); the time estimates above suggest that this
could be done without hardening the material.  This could allow
post-printing surface finishing to finer tolerances than the 3-D
printer itself can manage before a final heat treatment to reach full
hardness.

Alternative formulas
--------------------

I've written previously about weird hypothetical steels based on
zirconium and the like.  There are a lot of materials that are similar
to cementite (Fe₃C): borides, nitrides, carbides, and sometimes
silicides of iron, vanadium, tungsten, zirconium, hafnium, rhenium,
ruthenium, titanium, tantalum, chromium, molybdenum, niobium, and
occasionally even nickel, which are substantially harder than the
metal they are made from, generally because of strong covalent bonding
between the metal atoms and the nonmetal.  And I’m not going to bother
with cobalt or manganese.  That’s 52 candidate compounds to examine,
many of which are on the [list of ultra-high-temperature ceramics][18]
or [the list of superhard materials][20].

A bonus steel-like property that would facilitate the heat-treatment
would be if the nonmetal has high solubility and diffusivity in the
metal above a certain temperature, so you don’t need to use
inconveniently small particles of the nonmetal.  But I don’t think
this is essential; you can always make the inert particles small
enough.

We can probably discard titanium because you can’t deposit it
hydrothermally, not even electrolytically; it’s too reactive.  Other
practical considerations exclude beryllium.  Copper, on the other
extreme, is so noble that it’s very easy to electrodeposit, but
consequently doesn’t have any borides, nitrides, or stable carbides.
The motherfucker barely has oxides!

Nitrides could possibly work, but for this process, we’d need a
nitrogen source that we can codeposit in the form of inert particles
with our metal, and maybe one which remains solid at high
temperatures.  Fish meal or something might work, though you
unavoidably also get carbon, oxygen, and hydrogen embrittlement into
the bargain.  Boron nitride is probably *too* solid at high
temperatures; I don’t think you can get the nitrogen and boron to
diffuse into the metal.

For carbides and silicides you can use carbon, silicon, carborundum,
or possibly even silica.  For borides you can use boron.  However, all
of silicon, boron, and carborundum will be coated with an inert oxide
layer in exactly the way carbon isn’t; this could pose a barrier to
reactions.

The carbides are the easiest: there are lovely hard refractory oxides
of [zirconium][12], [titanium][13] (which we’ve already eliminated),
[hafnium][14], [vanadium][15], [niobium][16], [tantalum][17],
[chromium][19], [molybdenum][21], and, of course, [tungsten][22].

The silicides are mostly less promising.  [Tungsten disilicide reacts
violently with water][24], which seems less important if it’s formed
on the interior of a hunk of tungsten; the silicon particles we’d be
using are inert to water.  [Molybdenum disilicide][25] is used for
heating elements (when carborundum ain’t enough) but is brittle.
[Chromium disilicide exists][26].  There are [three iron
silicides][27], which seem to be unremarkable rocks.  Silicides of
vanadium, tantalum, and niobium are almost unknown.  [Zirconium
disilicide is fragile][28].  [Chromium has][29] four little-known
silicides.  [Nickel silicides protect nickel from wear][30].  None of
these seem like fantastic ways to make metal stronger, though maybe
they would help some.

As for the borides, [tungsten borides are superhard][31], [zirconium
diboride is an ultra-high-temperature ceramic][32], [hafnium
boride][33] is of course almost identical, [niobium diboride][34] is
another one, [tantalum borides are superhard][35], [rhenium
diboride][36] is too, [ruthenium boride][37] might be but is poorly
understood, [iron tetraboride is also superhard][38] (but [iron
monoboride is brittle][39] and [boron steel can be hot short][40]),
[trinickel boride is hard][41], and chromium has six borides of which
[at least one is extremely hard][42].  I don’t know about molybdenum
boride.

Nitrides are more variable and maybe less studied.  [Tungsten
nitride][23] decomposes in water but is hard, [zirconium nitride][43]
is used for cutting tools, [chromium nitride][44] is very hard and
used for corrosion- and wear-resistance coatings, [vanadium
nitride][45] increases the wear resistance of steels, [hafnium
nitride][46] is refractory and probably very hard, [tantalum nitride
exists][47] and is used as a passivating coating, [niobium
nitride][48] exists, [molybdenum nitride][49] is refractory, and [iron
nitrides][50] are somewhat unstable.

[12]: https://en.wikipedia.org/wiki/Zirconium_carbide
[13]: https://en.wikipedia.org/wiki/Titanium_carbide
[14]: https://en.wikipedia.org/wiki/Hafnium_carbide
[15]: https://en.wikipedia.org/wiki/Vanadium_carbide
[16]: https://en.wikipedia.org/wiki/Niobium_carbide
[17]: https://en.wikipedia.org/wiki/Tantalum_carbide
[18]: https://en.wikipedia.org/wiki/Ultra-high_temperature_ceramic
[19]: https://en.wikipedia.org/wiki/Chromium(II)_carbide
[20]: https://en.wikipedia.org/wiki/Category:Superhard_materials
[21]: https://en.wikipedia.org/wiki/Molybdenum_carbide
[22]: https://en.wikipedia.org/wiki/Tungsten_carbide
[23]: https://en.wikipedia.org/wiki/Tungsten_nitride
[24]: https://en.wikipedia.org/wiki/Tungsten_disilicide
[25]: https://en.wikipedia.org/wiki/Molybdenum_disilicide
[26]: https://en.wikipedia.org/wiki/Chromium(II)_silicide
[27]: https://en.wikipedia.org/wiki/Iron_silicide
[28]: https://en.wikipedia.org/wiki/Zirconium_disilicide
[29]: https://en.wikipedia.org/wiki/Chromium(II)_silicide
[30]: https://en.wikipedia.org/wiki/Nickel_silicide
[31]: https://en.wikipedia.org/wiki/Tungsten_borides
[32]: https://en.wikipedia.org/wiki/Zirconium_diboride
[33]: https://en.wikipedia.org/wiki/Hafnium_diboride
[34]: https://en.wikipedia.org/wiki/Niobium_diboride
[35]: https://en.wikipedia.org/wiki/Tantalum_boride
[36]: https://en.wikipedia.org/wiki/Rhenium_diboride
[37]: https://en.wikipedia.org/wiki/Ruthenium_boride
[38]: https://en.wikipedia.org/wiki/Iron_tetraboride
[39]: https://en.wikipedia.org/wiki/Iron_boride
[40]: https://en.wikipedia.org/wiki/Boron_steel
[41]: https://en.wikipedia.org/wiki/Trinickel_boride
[42]: https://en.wikipedia.org/wiki/Chromium(III)_boride
[43]: https://en.wikipedia.org/wiki/Zirconium_nitride
[44]: https://en.wikipedia.org/wiki/Chromium_nitride
[45]: https://en.wikipedia.org/wiki/Vanadium_nitride
[46]: https://en.wikipedia.org/wiki/Hafnium_nitrides
[47]: https://en.wikipedia.org/wiki/Tantalum_nitride
[48]: https://en.wikipedia.org/wiki/Niobium_nitride
[49]: https://en.wikipedia.org/wiki/Molybdenum_nitride
[50]: https://en.wikipedia.org/wiki/Iron_nitrides

Of my 11 remaining metal candidates (vanadium, tungsten, zirconium,
hafnium, rhenium, ruthenium, tantalum, chromium, molybdenum, niobium,
and nickel), which can be electrodeposited?  Nickel and chromium are
well-known for being easy to electrodeposit, but what about the
others?

Given that zirconium and hafnium are in titanium’s group, chances
don’t look good there.  Zirconium’s [standard electrode potential][52]
is -1.45V, even worse than titanium, and hafnium's is -1.724V, worse
than even aluminum at -1.662V.  You could probably electrodeposit all
four of them in some kind of non-aqueous electrolyte (DMSO?  MeCN?
Some deep eutectic system?) but not in water.

With respect to vanadium, I can only find discussion of
electrodeposition of its *oxide*, *e.g.*, [3D Vanadium Oxide Inverse
Opal Growth by Electrodeposition][51].  I was thinking [standard
electrode potential][52] was -0.08 volts for divalent vanadium
hydroxide, barely worse than hydrogen (or iron at -0.04), but it turns
out that’s not for vanadium *metal*, which is -1.13V, still worse than
titanium at -0.93V.

Like chromium, all of Zr, Hf, V, Ti, and Al participate in popular
oxoanions, but unlike chromium, I don’t think you can electrodeposit
them in water from those oxoanions, because the metal deposited won’t
survive contact with the electrolyte.

The real drop-dead voltage here is probably the potential for
producing H₂ directly from H₂O, which is -0.8277V, a potential you can
shift a little bit relative to the metals by altering conditions (pH,
pressure, temperature) but not very far.  The standard 0 voltage is
for producing H₂ from 2H⁺, which is a reaction I think you can
suppress by keeping the pH high — though with transition metals you’ll
tend to precipitate the metallic ions you wanted to electrodeposit as
an insoluble hydroxide.

So, from first principles at least, things look good for rhenium
(+0.300V), tungsten (-0.09V from WO₃(aq), -0.12V from WO₂), molybdenum
(-0.15V), tantalum (-0.45V from TaF₇²⁻, -0.6V from Ta³⁺), and niobium
(-0.7V from Nb₂O₅).  I’m not sure about ruthenium, but it's probably
even easier than rhenium.  Nickel is -0.257V, cobalt is -0.28V.

[51]: https://iopscience.iop.org/article/10.1149/2.0541514jes/pdf
[52]: https://en.wikipedia.org/wiki/Standard_electrode_potential_(data_page)
