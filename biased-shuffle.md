This non-Fisher-Yates shuffle code is biased:

    import random

    def shuffle(xs):  # naive intuitive shuffle
        for i in range(len(xs)):
            j = random.randrange(len(xs))
            xs[i], xs[j] = xs[j], xs[i]

To see this, you can run this:

    import collections

    def first_of(n):
        xs = list(range(n))
        shuffle(xs)
        return xs[0]

    print(collections.Counter([first_of(3) for i in range(100_000)]))

1 comes out first 25% more often than 2, but with a fair shuffle all
three should have an equal chance of being first.

After the first loop iteration does its random swap, these three
permutations are equally likely:

    0 1 2
    1 0 2
    2 1 0

The other three are impossible.  You might think these would be the
odd permutations, since we’ve done one swap and one is odd, but one
possibility was swapping a number with itself, giving the identity
permutation, which is of course even.  The second swap then gives us
three equally likely possibilities for each of these:

    0 1 2
        1 0 2
        0 1 2
        0 2 1
    1 0 2
        0 1 2 (second time)
        1 0 2 (second time)
        1 2 0
    2 1 0
        1 2 0 (second time)
        2 1 0
        2 0 1

So at this stage of the process, we have all six permutations, but 0 1
2, 1 0 2, and 1 2 0 are twice as likely as the other three.  (As
before, these favored permutations consist of one even permutation and
two odd ones, but I don’t know if that’s significant in any way.)  So
then we can calculate the number of paths to each permutation in the
third round from the second-round probabilities:

    0 1 2      2 paths in second round
        2 1 0  2 paths in third round
        0 2 1  2
        0 1 2  2
    0 2 1      1
        1 2 0  1
        0 1 2  3 (running total)
        0 2 1  3
    1 0 2      2
        2 0 1  2
        1 2 0  3
        1 0 2  2
    1 2 0      2
        0 2 1  4 (see note !@! below)
        1 0 2  4
        1 2 0  5
    2 0 1      1
        1 0 2  5
        2 1 0  3
        2 0 1  3
    2 1 0      1
        0 1 2  4
        2 0 1  4
        2 1 0  4

So the final totals are:

    0 1 2  4
    0 2 1  4
    1 0 2  5
    1 2 0  5
    2 0 1  4
    2 1 0  4

So that’s how 1 ends up being more popular in the first position with
this biased shuffle.

Note that there is no possible way to divide 27 equally likely
possible execution paths evenly among 6 permutations, because 27 is
odd.  This also means that my totals above are wrong, because they add
up to 26.  Here’s a way to get the right answer, passing in the
“random” choices as an argument:

    In [1]: import collections

    In [4]: def shuffle(a, swaps): 
       ...:     a = list(a) 
       ...:     for i, j in enumerate(swaps): 
       ...:         a[i], a[j] = a[j], a[i] 
       ...:     return a 
       ...: 

    In [5]: collections.Counter(tuple(shuffle(range(3), [i, j, k])) for i in range(3
       ...: ) for j in range(3) for k in range(3)) 
    Out[5]:

    Counter({(2, 0, 1): 4,
             (1, 2, 0): 5,
             (1, 0, 2): 5,
             (2, 1, 0): 4,
             (0, 2, 1): 5,
             (0, 1, 2): 4})

So actually (0, 2, 1) is also more probable.  The line above tagged
with !@! is where I made the mistake counting by hand.

I believe the naive shuffle code never becomes unbiased for arrays of
more than 2 items, but for longer arrays the bias can be more subtle.
For 4 items, though, it is even more obvious, with 1 0 3 2 occurring
15 ways out of 256 and disfavored permutations like 3 1 2 0 or 3 0 1 2
only 8.
