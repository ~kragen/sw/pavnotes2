One of the difficulties with ECM and selective electrodeposition is
the difficulty of limiting the area affected at any given time.
Meniscus confinement is one interesting standard technique.

It occurred to me that you can sharpen the border by doing the process
differentially, either by alternating polarities in time, or by using
a guard ring.

For alternating polarities in time, you alternate a very localized
phase in the desired sense with a somewhat less localized phase in the
opposite sense.  For example, for electrolytic material removal, you
might move the tip of a wire-shaped counter electrode to 1μm from the
workpiece and turn on the workpiece as the anode for a while; then, to
compensate for the unwanted dissolution at 2μm and more from the point
where cutting was desired, you turn off the field, increase the
distance to 5μm from the workpiece, and activate the field in reverse,
with the workpiece as the cathode, electroplating a larger area from
the same electrolyte.  This undoes some of the desired cutting, but
only slightly.  Then you flush the electrolyte and repeat.  The
repetition rate of this cycle might be in the kilohertz or megahertz.

For electrolytic deposition, for example, you might move the tip of
your wire-shaped counter electrode to 1μm from the workpiece and turn
on the workpiece as a cathode for a while, and then move it further
away and reverse the polarity to remove the material deposited on the
surface farther away than desired.

In both cases the voltages used might be different between the phases,
for example because you want one phase to be diffusion-limited and the
other to be voltage-limited.

In using a guard ring, you run both polarities continuously on two
different counter electrodes.

For example, for electrolytic material removal with a guard ring, you
might have a central cathode wire counter electrode of, say, 1μm
diameter, separated by a 1μm thick insulating layer from a coaxial
anode counter electrode of 3μm inner diameter, 5μm outer diameter.
The counter cathode is at a sufficiently negative voltage relative to
the workpiece to cause anodic dissolution in the workpiece that is
close to it, while the counter anode is only slightly more positive
than the workpiece, not enough to electroplate.  This confines the
negative-voltage region of the electrolyte to a very small volume
around the cathode.

Analogously for electrolytic deposition with a guard ring, for
example, the field of an inner counter anode that is dissolving might
be tightly confined by a larger coaxial counter cathode which is not
at a negative enough voltage to get electroplated, with the result
that electrodeposition happens only in a tightly confined region near
the anode tip.
