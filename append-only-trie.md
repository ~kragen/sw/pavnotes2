[Some discussion of OpenBSD ldapd’s append-only B-tree][0] from which
[Howard Chu’s LMDB][2] was developed pointed out that it has a lot of
write amplification, and there was some discussion of how to reduce
this; a “hitchhiker tree” or “buffer tree” or “fractal tree” in which
each non-leaf node bears a list of pending edits awaiting a ride to
their respective leaf nodes, batching up updates in an append-only
side file, and so on.

[0]: https://news.ycombinator.com/item?id=38805383
[1]: https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=5353ed87f7ec15c32d66f81a0ad9ba2f695f0855
[2]: https://github.com/LMDB/lmdb/blob/30288c72573ceac719627183f1058cad1dd08b74/libraries/liblmdb/mdb.c

A concrete terabyte scenario demonstrates 1000× write amplification
-------------------------------------------------------------------

I thought it would be good to quantify these costs a bit.  Let’s
presuppose that we’re not doing any in-place updates; all new data
must be written to a new location.  This is pretty much the case for
RAID4, shingled magnetic recording, and SSD, and a very rough
approximation for traditional spinning rust.

Suppose we
have a terabyte of 16-byte key-value pairs, so we have 64 gibipairs,
2³⁶ pairs; our maximum page size is 4096 bytes; our file pointers are
8 bytes; and we’re using a B+ tree which stores 16 bytes of key
material per key in each internal node.  (So I guess our values are
all the empty string.)  Then our branching factor, for full pages, is
170, maybe a conveniently round 128 on average, and our tree is 6
levels deep; the root node is much less full, containing only a single
key and two pointers.

This does have a lot of write amplification.  To insert or delete a
single 16-byte key-value pair, we need to write a new root node (32
bytes), updated versions of four pages on the path from the root to
the leaf (averaging 3072 bytes each), and a leaf node (also 3072
bytes), for about 15.4 kilobytes, about 1000× write amplification.

Let’s consider instead what happens if we reduce the “page” size to,
on average, four children: three keys and four pointers, 80 bytes.
Now instead of a 6-level-deep tree, we have a 20-level-deep tree, and
the new path up to the root is only 1600 bytes.  This does reduce the
write amplification, but not nearly enough to be competitive; it’s
still 100×.  And you need to sometimes gratuitously copy nodes you
aren’t mutating in order to prevent locality from decaying too far.
(20 levels would already be completely unacceptable for spinning rust;
that’s 2 seconds!)

I was thinking that you could usefully convert this structure from a
sorted B+ tree into a trie, thus eliminating the need to store keys
and to split and merge nodes.  But this doesn’t really solve the
write-amplification problem; path copying still has to copy the same
number of nodes.

Batching updates only helps slightly
------------------------------------

Just buffering up the updates in a side file doesn’t really help that
much either.  Maybe you buffer up 1024 random updates; flushing the
buffer involves creating 1024 new leaf nodes, 1024 new level-n-1
nodes, etc., saving nothing until you get close enough to the root
that the random updates share an ancestor.  With the tree described,
with a root with 2 children and 128-way branching in the rest of the
tree, you have 1 root node, 256 level-1 nodes, and 32768 level-2
nodes, so that amount of batching really only saves updates on the top
two levels out of 6.

LSM-trees on this example problem
---------------------------------

Suppose you want to use LSM-trees for this.  Your RAM is, say, 16
gibibytes, so one gibipair fits in RAM.  Let’s say we use 8-way
merges, so after we write 8 1-gibipair LSM segments to disk, we merge
them into an 8-gibipair segment, and after we do that 8 times, we
merge all 8 into a 64-gibipair segment.  This involves writing each
key-value pair to disk three times and reading it twice; that’s only
5× write amplification, which is a lot better than 1000×.  But lookups
are somewhat expensive: you may have 14 live segments to consult, so
you may need to hit disk 14 times to do a lookup.

Greenberg’s “hitchhiker trees”
------------------------------

The hitchhiker-tree approach does solve the write amplification
problem.  You reduce the branching factor of each node and use the
extra space to buffer up updates that haven’t yet been propagated down
the tree.  So maybe instead of 128-way branching you have 8-way
branching and a buffer space for 120 update messages.  Every 120
updates, you have to flush the root node, which will flush about 15
updates down to each of its children, causing 7 pages of I/O.  On
average, 1 of those children will flush an average of 15 updates down
to each of its own 8 children, and on average one of them will flush,
and so on.  Hmm, that seems to show that it provides no advantage over
conventional path copying, because on average each node flushed into
flushes to one of its children, so on average we’d expect to copy the
whole path.

It’s a bit confusing.  [David Greenberg describes][4] his [“hitchhiker
tree”][3] as “synthesizing fractal trees and functional data
structures, to create fast, snapshottable, massively scalable
databases”, and have been [discussed on the orange website][5], where
Rico Hermans/[Huijbers][7] mentioned [his own independent
reinvention][6] in libbruce, which stores your ordered key-value store
in an unordered key-value store provided by Amazon.

[3]: https://github.com/datacrypt-project/hitchhiker-tree/blob/master/doc/hitchhiker.adoc
[7]: https://rix0r.nl/
[6]: https://github.com/rix0rrr/libbruce/blob/master/README.md
[5]: https://news.ycombinator.com/item?id=12229906
[4]: http://www.dgrnbrg.com/hitchhiker

[Fractal trees][9] are another realization of this approach,
commercialized as Tokutek and later bought by Percona; there’s [some
slides from 02011 by Kuszmaul][8], citing “buffered repository trees”
(“Buchsbaum Goldwasser Venkatasubramanian Westbrook ’06, Brodal
Fagerberg ’02”) and [some flashier slides from 02012][10].  It seems
like maybe the difference from hitchhiker trees is that fractal trees
update in place, while hitchhiker trees are FP-persistent, with new
data always being written to new locations.  That is, hitchhiker trees
are precisely copy-on-write fractal trees.

[8]: https://www.percona.com/blog/wp-content/uploads/2011/11/how-fractal-trees-work.pdf
[9]: https://en.wikipedia.org/wiki/Fractal_tree_index
[10]: https://www.percona.com/blog/wp-content/uploads/2012/11/Fractal-Tree-Technology-and-the-Art-of-Indexing.pdf

The 02012 slides show a significant difference from what I described
above: each node contains not a single message buffer, but a message
buffer per child node.  So perhaps a node with 8 child nodes might
contain 8 16-message buffers; when one gets full, all 16 messages are
delivered to its child node.

But I don’t see how that changes things.  So if a 15-message buffer in
the root node drains into one of the children, it ends up depositing
about 2 messages into the queue for each of 8 grandchildren.  Each of
these 2-message deposits seems like it should have about a ⅛ chance of
filling up that queue, so the average number of propagations should be
about 1.  So it seems like there’s no asymptotic improvement.  Indeed,
the only real difference from how I initially sketched it is that each
node’s mailbox is physically located in its parent node instead of in
the node itself, which I guess is a good optimization for locality of
reference.

We do get a constant-factor improvement, though, of the factor of the
queue size.  We don’t need to update the root node on every message;
we can keep a message queue for the whole system which buffers up
pending updates to the root node.  With the length-16 queues suggested
here, we only need to walk down the tree once every 16 messages.  But
you can make the nodes, and the queues, much larger; the Kuszmaul
slides give as an example using a 1-megabyte block holding 8192
database rows of 128 bytes, so you have 90-way branching and 90
buffers each capable of holding 90 messages.  So, for up to about a
million records, you have a three-level tree, and each 90th update has
to update three blocks.

This works out to about 0.032 1-megabyte blocks written per update,
about 32 kilobytes.  This is a little less write amplification than
our initial setup: upserting a 128-byte row writes, on average, 128
bytes plus 32 kilobytes of data, which is roughly 256× write
amplification.

But you can probably do better than this by having more queue space.
Let’s take on our terabyte of 16-byte key-value pairs again.  What
happens if we go to the extreme of 2-way branching, still with
1-megabyte blocks?  Each block contains space for 65536 key-value
pairs.  In all except the leaf blocks, this consists of a pivot pair
and two queues of 32767 elements.  We have 524288 leaf nodes and
524287 internal nodes, more or less, so the tree is 20 levels deep,
which is pretty bad for reads, but whatever, it’s an extreme.  On
average, 32766 of every 32767 writes get buffered in the root node,
and then the queue gets flushed to one of the children, writing 32767
keys to it, which will on average result in flushing one of its
queues, etc.  So when we do flush, we end up writing, on average, 20
new nodes, 20 megabytes.  But because we do this so rarely, the
traffic per write is only 610 bytes, 38× the data we upserted.  This
is a lot better than 1000× or 256× but it still seems worse than an
LSM-tree.

Arge’s Buffer Tree
------------------

In 01996 Lars Arge wrote [“The Buffer Tree”][11], University of Aarhus
BRICS RS-96-28, which describes what initially sounds like a less
useful variant of the approach:

> Operations on the structure—updates as well as queries—are then done
> in a “lazy” manner.  If we for example are working on a search tree
> structure and want to insert an element among the leaves, we do not
> right away search all the way down the tree to find the place among
> the leaves to insert the element.  Instead, we wait until we have
> collected a block of insertions (or other operations), and then we
> insert this block in the buffer of the root.  When a buffer “runs
> full” the elements in the buffer are “pushed” one level down to
> buffers on the next level.  We call this a *buffer-emptying
> process*.  Deletions or other and perhaps more complicated updates,
> as well as queries, are basically done in the same way as
> insertions.  This means that we can have several insertions and
> deletions of the same element in the tree, and we therefore time
> stamp the elements when we insert them in the top buffer.  It also
> means that the queries get batched in the sense that the result of a
> query may be generated (and reported) lazily by several
> buffer-emptying processes.

[11]: https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=5353ed87f7ec15c32d66f81a0ad9ba2f695f0855

I thought this was less useful because delaying query results until
the relevant updates have trickled down to a leaf node seems pretty
suboptimal.  But you wouldn't have to delay them long; the query will
pull in those leaf nodes from disk pretty soon anyway, and you can
update them then before you evict them.  Maybe that wasn’t obvious
because I’ve internalized the cost function of Flash and FP-persistent
data structures where writes are much more expensive than reads.  With
the more symmetric cost function of spinning rust, maybe it really
would make sense to trickle updates down to a leaf node whenever a
query forces the leaf node to be brought in from the disk.

And, in a different sense, Arge’s work is more useful: it’s a general
approach (I hesitate to say “algorithm”) for transforming in-RAM tree
data structures into efficient on-disk data structures.  He discusses,
for example, segment trees, ordered binary decision diagrams, priority
queues, etc.  He seems to have done his doctoral thesis that year on
the topic; it was called, “Efficient External-Memory Data Structures
and Applications.”

The “buffered repository tree”
------------------------------

[Buchsbaum, Goldwasser, Venkatasubramanian, and Westbrook][13] turn
out to have published this in 02000, not 02006 as Kuszmaul’s slide
said.  They’re apparently applying Arge’s approach to the specific
case of using a balanced (2, 4) tree as a priority queue: “We use
Arge’s buffer trees [2] for the priority queues.”

[13]: http://cs.slu.edu/~goldwasser/publications/SODA2000.pdf.gz "On External Memory Graph Traversal"

Brodal and Fagerberg
--------------------

These guys seem to have published [their paper on on-disk key-value
databases][14] in 02003, not 02002.  They’re from BRICS, Arge’s own
institution, and six out of their 19 citations are to his papers.

[14]: http://perso.ens-lyon.fr/loris.marchal/docs-data-aware/papers/paper3.pdf "Lower Bounds for External Memory Dictionaries, ALCOMFT-TR-03-75"

They aren’t presenting a useful new algorithm they invented; rather,
they’re proving that no possible algorithm can do better than the new
lower bound they’re presenting, which is that if insertion costs
Θ(*k*), then queries can be forced to use lg *N* ∨
N/2<sup>Θ(*k*)</sup> comparisons.  I suspect that this could have
proven to me that I shouldn’t be looking for an asymptotic improvement
in fractal trees, and the best they could do was a constant-factor
improvement.  But right now I’m too sleepy.

Anonymous Quora answer poitns to “cache-oblivious streaming B-trees”
--------------------------------------------------------------------

In [a question about fractal trees][15], someone who said they worked
at Tokutek said:

> Fair warning: there's a lot wrong in this answer. One of these days,
> I'll clean it up. (...)  on our company blog, search for “write
> optimization: myths, comparisons, clarifications”. (...) The other
> differences require a more thorough discussion of the algorithms, at
> a lower level, so I will point you to the paper describing the basis
> for the Fractal Tree (known in the academic world as a
> Cache-Oblivious Streaming B-Tree) [[1]][16]. This is an extension of
> the Cache-Oblivious Lookahead Array, which may be more accessible,
> or will at least introduce you to the idea behind the Fractal Tree.

[15]: https://www.quora.com/What-are-the-major-differences-between-LSM-trees-used-in-BigTable-etc-and-Fractal-Trees-used-in-TokuDB
[16]: https://dl.acm.org/doi/10.1145/1248377.1248393

The linked paper is “Cache-oblivious streaming B-trees”, by Michael
A. Bender, Martin Farach-Colton, Jeremy T. Fineman, Yonatan R. Fogel,
Bradley C. Kuszmaul, and Jelani Nelson, in SPAA ’07: Proceedings of
the nineteenth annual ACM symposium on Parallel algorithms and
architectures, June 02007, Pages 81–92, DOI 10.1145/1248377.1248393.
Somehow I hadn’t found this paper before.

It uses a model where I/O happens by writing a block consisting of B
elements from the M-element internal memory to an arbitrarily large
disk and reading in another one (the “disk access machine” (DAM)
model).  It says that if you want to store N elements, a B-tree
supports searches, insertions, and deletions in O(log<sub>B+1</sub> N)
transfers, while Buchsbaum et al.’s “buffered repository tree” (BRT)
handles searches in O(log N) transfers and insertions need O((log
N)/B) transfers.  This sounds like my model above where you have some
small fixed branching factor F per block, like F = 2, and so each
“buffer” is B/F items.

(They also say Brodal and Fagerberg presented a tree called the
B<sup>ε</sup> tree, which I somehow failed to notice.)

They suggest that an interesting middle ground between the B-tree,
where the buffer size is some constant, and the BRT where the
branching factor is some constant, is to split the spoils of larger
blocks evenly between the branching factor and the queue size.  So, if
you have space for 65536 keys in a block, as in my model above, you
might use 256-way branching and 256-item buffers.  So your terabyte of
2³⁶ key-value pairs give you five levels instead of three or (as in my
model above) 20.  I think this means that, every 256 upserts, you end
up writing an average of five megs, which is 20K per block: back to
1000× write amplification again.  Ugh.  I was hoping for better,
especially since LSM-trees can deliver only 5×.

I guess I should really read this paper.

