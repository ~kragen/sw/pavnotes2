I woke up this morning dreaming about digging a tiny house into the
ground while living in it.  It's been punishingly hot here in Buenos
Aires these last few days, so the overriding concern in my dream was
escaping the heat.

Without any actual experience to go on, I'm estimating the digging
process as follows.  Digging by hand rather than using earthmoving
machinery, you can raise a shovelful of dirt about every 2 seconds,
and you can raise it about 2 meters at a time.  A shovelful is about 3
kg, and a cubic meter of dirt is about 2 tonnes, which is about 700
shovelfuls, and thus about 1400 seconds of digging, 23'20".  So, if
you're physically fit, in about an hour, you can dig a hole that's 1 m
x 1 m x 3 m.  That's deep enough you need a ladder to get out, and
probably ought to add support to the sides so it doesn't collapse on
you.

In my dream I was using a sort of dig-and-cover method where I dug a
pit and then would later put a roof on it, in the meantime stretching
a tarp over the pit to keep any possible rain out, and trenching
around the outside to keep rainwater from flowing into the pit along
the surface of the ground.  Evidently in the dream groundwater seepage
was not a concern, though here in Buenos Aires it certainly would be.

By the calculations above, in a ten-hour workday, one person can move
almost 26 cubic meters of dirt, as long as you aren't lifting them
more than two meters, or half that down to a depth of four meters.

A bootstrap burrow: the Minimal Viable Burrow
---------------------------------------------

With Minetest-style physics, where nothing collapses and so no support
is necessary, but nevertheless we are digging through soft dirt, ten
hours is enough to dig a comfortable bootstrap burrow on the following
plan, all a single meter wide:

         tarp   ground
    __ _------_ ______
      ~ ###### ~       ~  = rainwater diversion trench
          ##########   ## = cubic meter of space (total 13)
            ########
              ##@@@@   @@ = cubic meter of bed (total 2)

Five of these 15 cubic meters are within two meters of the exit and
could therefore be dug at full speed, without an additional step to
remove the spoil.  The other ten count double.  According to the above
estimates, this would be about 9 hours and 43 minutes, but the error
in the estimates is probably more than a factor of 2.

Once you had such a burrow you could add an alcove for an additional
person to the side of the main bed with only four additional cubic
meters of digging (about two person-hours):

                ####
                @@@@

This sort of thing suggests that the ideal number of people per burrow
is at least two or three; three people per bootstrap burrow would be
23 cubic meters, 18 of them counting double, for about 16 person-hours
of digging: a little under six hours per person.

The cost of adding support is commonly a dominant factor in tunneling;
Wikipedia says:

> In planning the route, the horizontal and vertical alignments can be
> selected to make use of the best ground and water conditions. It is
> common practice to locate a tunnel deeper than otherwise would be
> required, in order to excavate through solid rock or other material
> that is easier to support during construction.

For a small bootstrap burrow like this I think that is likely not to
be the case, but it depends on the support materials you're using.
Prefabricated supports would only need to be unfolded into place, and
could plausibly be made from lightweight galvanized steel studs cut to
length with tin snips and screwed together with self-tapping sheet
metal screws, put in place with an electric drill.

This points out that the above digging time estimates are for a purely
human-powered setup, but the application of various other power
sources has greatly increased the productivity of construction and
manufacturing over the last 250 years, and the initial application of
the steam-engine was in fact in mining.  In this case (returning to
the initial single-person bootstrap burrow plan) the fundamental work
being done is lifting dirt: three cubic meters by half a meter, five
cubic meters by a meter and a half, four cubic meters by two and a
half meters, and three cubic meters by three and a half meters. `(+ (*
3 .5) (* 5 1.5) (* 4 2.5) (* 3 3.5))` yields 29.5 m⁴ of dirt lifting;
at the estimated density of 2 tonnes per cubic meter this is 0.6
megajoules of work, 160 watt-hours.

So evidently our estimated digger is only doing about 16 watts of
final work, the rest merely being converted to heat; if they were
pedaling some kind of efficient automatic digging contraption instead
of throwing dirt with a shovel, they could dig ten times as fast.  And
if they were directing some kind of 10-kilowatt power tool, they could
dig hundreds of times as fast.  Probably well-drilling contractors are
the ones pushing the limits on what's feasible along these lines.

If you had a vertical or near-vertical slope to dig into, you could
reduce the Minimal Viable Burrow effort by more than half, digging
only 11 cubic meters, all at or above ground level:

          |          |  = vertical cliff face
          | ######   ## = cubic meter of space
          ########   __ = horizontal ground
     _____####@@@@   @@ = cubic meter of bed


A full-sized burrow
-------------------

In my dream, though, I wasn't just digging a minimal single-day burrow
to sleep in that night; I was working on a project to make a
full-sized tiny house.  If we suppose this is 50 m² of floor space
once you're through the entryway, a bit bigger than my current
apartment, with about 2.4 meters above each bit of floor, it's 120 m³,
8 times the size of the Minimal Viable Burrow.

A dome would be an efficient and aesthetically appealing way to
support the enormous loads of earth and rock above; 120 m³ is the
volume of a hemisphere of radius 3.86 meters.  This suggests a ground
floor of radius 3.9 meters with 48 m² of floor area, but the area with
a full 2.4 m of ceiling height is only radius 3.1 meters, 30 m², and
this would therefore the radius of the second floor.  However, with
that shape, *none* of the second floor could be a full 2.4 m in
ceiling height: though you could have 78 m² of floor area (minus
whatever intercourse existed between levels), only 30 m² would be
comfortable to walk around in.

A spherical dome isn't optimal for supporting an enormous overburden
anyway; the paraboloid has that distinction, and it grants us an extra
degree of freedom, as we can make the parabola as tall or as shallow
as we like.  (We could do that with a sphere by cutting it off at a
different latitude than the equator, too, but the paraboloid is more
appealing.)
