I'm skeptical of the prospects for the new fabs in the US. There have
been many cases where one or another locality tried to capture the
center of gravity of the electronics industry, but almost all of them
have failed at doing so, though many of them have succeeded in wasting
huge amounts of money. Deep expertise in solving difficult technical
problems is not something you can buy off the shelf just by spending
large amounts of money; much less is it something you can acquire by
denying that it exists in the first place, which is the approach your
government seems to be taking.

Large amounts of money *are* necessary, though, and it's unlikely
they'll be forthcoming in the US. The CHIPS act is only investing
US$53 billion in subsidies over five years, and TSMC is apparently
investing another US$40 billion. But it's even worse than that sounds:
most of the US$53 billion is just tax exemptions, and only US$11
billion is actual funding, most of which (US$8.5 billion) goes to
NIST, who as far as I know doesn't mass-produce any semiconductors or
solve any key research problems blocking their advance. Most of the
tax exemptions are going to companies like Solvay, Air Liquide, and
Corning, rather than semiconductor manufacturers.

Only US$2.5 billion of that US$53 billion is potentially going into
actual productive activity, and US$2 billion of that via the DoD, so
it's going to go mostly to Lockheed, BAE, SAIC, and the other usual
suspects, rather than TSMC and Intel. The other US$0.5 billion is
going to a DoE/DoD manufacturing research institute called
Manufacturing USA with, as far as I can tell, no expertise that will
be relevant in the next decade.

So I think it's mostly PR.

The US has some big semiconductor companies, but with respect to
expertise, IBM and Intel (and TI and HP) are not in the same league as
Samsung and TSMC, or even GloFo. There are lots of fabs in the US,
even SkyWater, they just aren't the ones pushing forward the state of
the art any more. And the US's sanctions and ITAR moves over the last
two years have ensured they never will be; even some NATO countries
will be reluctant to put their military avionics and physics research
at the mercy of such a capricious ally.
 
The covid-related supply-chain shocks don't make it more likely.  It
doesn't matter very much where the markets are because shipping
electronics is basically free. Supply-chain shocks favor geographical
clustering and even River-Rouge-style vertical integration, not
offshoring production from Taiwan to the US.
