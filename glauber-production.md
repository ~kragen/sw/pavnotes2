Glauber’s salt is a crucial material for phase-change energy storage.

Alabaster and baking soda
-------------------------

It can be made by reacting [alabaster][3] with baking soda; [chalk’s
solubility][0] is about 6.17×10⁻⁴ g/100mℓ, while alabaster’s is closer
to 0.255g/100mℓ (counted as the dihydrate).  The bicarbonate of lime
has reportedly higher solubility of 16.6g/100mℓ, and [Glauber’s salt
19.5g/100mℓ][1], so we might expect the alabaster to go into solution
as those two substances, but soon whatever goes into solution will
fall down as chalk.  I don’t know how to calculate the equilibrium,
though I’m pretty sure that’s a thing you can do from thermodynamic
data.  I’m guessing you end up with something like half chalk and half
alabaster in the defecate, but maybe you have to heat the solution to
encourage the disproportionation of the bicarbonate ions.

[0]: https://en.wikipedia.org/wiki/Solubility_table#C
[3]: https://en.wikipedia.org/wiki/Calcium_sulfate
[1]: https://en.wikipedia.org/wiki/Solubility_table#S

Ultimately the reaction is:

> 2NaHCO₃ + CaSO₄ → Na₂SO₄ + CaCO₃ + H₂O + CO₂

and the molar masses of baking soda, anhydrite, and Glauber’s salt are
84.0066g/mol, 136.14g/mol, and 142.04g/mol respectively.  So for
168.0132g and 136.14g of feedstocks we’d get 142.04g of product at
100% yield, or probably more like 70g in reality.  Also the form of
alabaster that’s easiest to buy is the hemihydrate, which is
145.14g/mol.  And the usual form of Glauber’s salt is the decahydrate,
322.2g/mol, but more like 160g at 50% yield.

So, per kg of product, we need about 12 moles of baking soda and 6
moles of alabaster, which work out to 1 kg and 870 g, respectively.
Probably it’s good to use a freaking huge amount of water to keep the
alabaster from setting up solid.  In terms of purifying the product,
it might be best to use a vast excess of alabaster in order to reduce
the amount of leftover baking soda contamination dissolved with the
Glauber’s salt.

Presumably you’ll eventually recycle the leftover alabaster and soda,
so for calculating feedstock costs, I think probably I can treat the
yield as 100%, so it’s more like 500g of baking soda and 440g of
alabaster.

Baking soda currently costs [AR$3900/kg][2] from Tienda Canela in
Villa Luro on the Sarmiento line, [AR$5100/kg][4] from Saiku in Villa
Luro, [AR$5795/kg][5] from Morashop (“Breaking Lab” brand; they also
sell petrolatum, citric acid, dietary supplements, bicycles, X-Men
figurines, and maxi pads), [AR$8058/kg][6] from Parvati Natural in San
Martín near General Paz (who also sells things like kaolin, bentonite,
and essential oils), or [AR$6499/kg][7] directly from Breaking Lab in
Munro–Vicente Lopez at three blocks from Norcenter (who also sell
things like sodium percarbonate, oxalic acid, xanthan gum, muriate of
lime, glycerin, magnesium sulfate, sodium alginate, ferrite, propylene
glycol, etc.).  The median here is AR$5795/kg, which at today's
exchange rate of AR$1335×1365/US$ (mid-market $1350), works out to
US$4.30/kg, or US$2.15 per kg of Glauber’s salt.

[2]: https://articulo.mercadolibre.com.ar/MLA-1426623515-bicarbonato-de-sodio-a-granel-fraccionado-1-kg-_JM
[4]: https://articulo.mercadolibre.com.ar/MLA-829230196-bicarbonato-de-sodio-1-kg-excelente-calidad-caba-e-belgrano-_JM
[5]: https://www.mercadolibre.com.ar/breaking-lab-bicarbonato-de-sodio-1000g/p/MLA20080109
[6]: https://articulo.mercadolibre.com.ar/MLA-1103615801-bicarbonato-de-sodio-1-kilo-_JM
[7]: https://articulo.mercadolibre.com.ar/MLA-1559136466-bicarbonato-de-sodio-1kg-1000g-maxima-pureza-y-calidad-_JM

The national standard for alabaster purity is IRAM 1607; Type I is 80%
pure, and Type II is 60% pure.  25 kg of Knauf brand costs
[AR$7261][8] (AR$290/kg), 2 kg of Caller brand costs [AR$2642][9]
(AR$1321/kg), and 30 kg of IRAM 1607 Type A (??) Tuyango brand costs
[AR$8999][10] (AR$300/kg).  Even Mega Products brand alabaster powder
for artists is only [AR$7125/10kg][11] (AR$712.50/kg).  AR$300/kg is
22¢/kg, or 36¢/kg if we assume that it’s only 60% alabaster with the
other 40% being inert but useless.  That’s 16¢ per kg of product.  So,
from an economic point of view, it probably makes sense to use an
excess of alabaster in the reaction, since wasting alabaster is much
cheaper than wasting baking soda.

[8]: https://articulo.mercadolibre.com.ar/MLA-836862766-yeso-tradicional-knauf-25-kg-_JM
[9]: https://www.mercadolibre.com.ar/yeso-tipo-paris-x-2-kg-caller-pintumm/p/MLA35239058
[10]: https://www.mercadolibre.com.ar/tuyango-yeso-blanco-bolsa-de-30-kg/p/MLA29279628
[11]: https://articulo.mercadolibre.com.ar/MLA-840282406-yeso-tipo-paris-para-esculturas-y-moldes-x-10-kilos-_JM

The total material cost estimate is US$2.31 per kg of Glauber’s salt,
or US$4.60 for the first batch.

The unusual melting point of 32.38° of the decahydrate should be a
clear and easy test that it has indeed been made and purified.

Epsom salt and baking soda
--------------------------

Wikipedia mentions this reaction for laboratory synthesis, and it
seems even easier, with no need to purify the product, although epsom
salt is not quite so easy to find as baking soda and alabaster.  Since
the alabaster contributes only 7% of the feedstock costs, this will
probably have more expensive feedstocks:

> 2NaHCO₃ + MgSO₄ → Na₂SO₄ + MgCO₃ + CO₂ + H₂O

Epsom salt is invariably sold as the heptahydrate, with a molar mass
of 246.47g/mol, so we need a slightly larger mass of it; I think we
need 765 grams per kilogram of product.

Epsom salt costs [AR$29480/5kg (AR$5896/kg) from Breaking Lab][12],
[AR$9383.44/kg][13] branded “4+ Sport” from Natural Whey,
[AR$9383.44/kg][14] from Natural Whey under their own brand,
[AR$6079.20/kg][15] from Breaking Lab, or [AR$114000/25kg
(AR$4560/kg)][16] from [ICASA (International Chemicals of
Argentina)][17].  All of these but the last are sold as purity 99.9%
USP.  The median here is AR$5896/kg, which is US$4.37/kg today.  These
prices are much higher than the epsom salt prices in file
`material-sourcing.md`.

ICASA says they’re near Independencia Station of the C line (retail
store, Carlos Calvo 1740, in Constitución), and also sell [hundreds of
materials][18] like copper sulfate, sodium sulfate, citric acid,
phosphoric acid, etc.

[12]: https://articulo.mercadolibre.com.ar/MLA-1141659901-sales-de-epsom-sulfato-de-magnesio-puro-999-5-kilos-pote-_JM
[13]: https://www.mercadolibre.com.ar/sales-de-epsom-sulfato-de-magnesio-puro-999-1-kilo-usp-4-fragancia-sin-olor/p/MLA32429524
[14]: https://articulo.mercadolibre.com.ar/MLA-709446004-sales-de-epsom-sulfato-de-magnesio-puro-999-1-kilo-usp-_JM
[15]: https://www.mercadolibre.com.ar/sales-de-epsom-sulfato-de-magnesio-puro-999-1-kilo-usp/p/MLA25929152
[16]: https://articulo.mercadolibre.com.ar/MLA-1341060582-sales-de-epson-sulfato-de-magnesio-x-25kg-_JM
[17]: https://www.ica-sa.com.ar/
[18]: https://www.ica-sa.com.ar/productos

This would work out to US$3.34 of epsom salt per kg of product.  Added
to the US$2.15 for the baking soda you’d get US$5.49/kg.

Although this is more expensive than the alabaster approach, it’s also
enormously less hassle.
