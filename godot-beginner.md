I’m trying out the Godot game engine for the first time, and these are
my notes.

Installation
------------

Apparently you can script Godot 4 in GDScript, which is their custom
Python-like thing, or in C#.  But the C# thing requires using the .NET
version of Godot, which requires installing .NET Core, and although I
guess that’s free software now, it’s not in Debian.  (Godot 3
supported C# with Mono, but Mono is kindof dead.)  I’m not sure what
the compilability of .NET Core is.  So I’m sticking with the GDScript
version for now.  Apparently you can [extend it in C or C++ if you
have to][20] (“GDExtension”).

[20]: https://docs.godotengine.org/en/stable/getting_started/introduction/introduction_to_godot.html

[Downloading][0] is trivially easy, providing a 45-megabyte zipfile
that unzips to a single 100-megabyte executable, which is an
unexpectedly pleasant experience.  On initial startup it prompts you
to download an example project from the [Godot Asset Library][1]; I
chose [Pong with GDScript][2], which [cloc][3] v 1.96 counts as 41
lines of GDScript.

[0]: https://godotengine.org/download/linux/
[1]: https://godotengine.org/asset-library/asset
[2]: https://godotengine.org/asset-library/asset/121
[3]: https://github.com/AlDanial/cloc

Godot stores its configuration in ~/.config/godot, and although it
doesn’t seem to take a project name on the command line, it does look
for a project in the current directory, and run it if present.  I made
a symlink called `godot` so I wouldn’t have to type or tab-complete
`Godot_v4.2.2-stable_linux.x86_64` all the time.

It’s happy to store your projects in whatever directory you want, and
it displays the directory in the project list.

Asset library
-------------

The asset library has 14 updates so far today and 6 yesterday, all
with clear license tags, including tempting-sounding things like
[WaveFunctionCollapse][4], [Inventory System][5], [Dialogue Nodes][6],
[Godot LLM][7] (using Llama, recommending
Meta-Llama-3-8B-Instruct-Q5_K_M.gguf), [In Game Building System][8]
(for 3-D buildings), [Ready Player Me Avatar][9], [Better Terrain
4.2][10], [Squiggles Fur material][11] (for yiff), [Basic Water
Material][12], [SDF for Visual Shaders][13], [Eyeball Shader][14], [3D
Planet Generator][15], [Boujie Water Shader][16], [Godot Git
Plugin][17], [Text-to-Speech Demo][18] (which uses a system TTS API,
here espeak), [Sketchy Renderer][19], [Top-down Action RPG
Template][23], [Shape Grammar Interpreter][29] (coupled with an
[editor][33]), [Starter Kit City Builder][30], [KayKit City Builder
Bits][31], etc.  KayKit has [a whole panoply of different
“assets”][32] under CC0: space base, adventurer, furniture, halloween,
etc.  [Dialogic][28] is still only for Godot 3.5, but also looks
really cool.  There are [35000 GDScript repos][34] on GitHub,
including [a CHIP-8 emulator][35].

[4]: https://godotengine.org/asset-library/asset/1951
[35]: https://github.com/vitoralmeidasilva/godot-chip8-emulator
[34]: https://github.com/search?q=language%3AGDScript&type=repositories&s=stars&o=desc
[33]: https://github.com/mathiasplans/grammar-editor
[32]: https://godotengine.org/asset-library/asset?user=KayKit%20Game%20Assets
[31]: https://godotengine.org/asset-library/asset/2123
[30]: https://godotengine.org/asset-library/asset/2174
[29]: https://godotengine.org/asset-library/asset/1871
[28]: https://godotengine.org/asset-library/asset/833
[5]: https://godotengine.org/asset-library/asset/1650
[6]: https://godotengine.org/asset-library/asset/2400
[7]: https://godotengine.org/asset-library/asset/2913
[8]: https://godotengine.org/asset-library/asset/2957
[9]: https://godotengine.org/asset-library/asset/2952
[10]: https://godotengine.org/asset-library/asset/2954
[11]: https://godotengine.org/asset-library/asset/2339
[12]: https://godotengine.org/asset-library/asset/212
[13]: https://godotengine.org/asset-library/asset/2691
[14]: https://godotengine.org/asset-library/asset/2640
[15]: https://godotengine.org/asset-library/asset/1615
[16]: https://godotengine.org/asset-library/asset/2070
[17]: https://godotengine.org/asset-library/asset/1581
[18]: https://godotengine.org/asset-library/asset/2763
[19]: https://github.com/Gyrth/SketchyRenderer
[23]: https://godotengine.org/asset-library/asset/487

Screencasts
-----------

`godot -e` opens the IDE instead of running the game.  And there’s a
`godot --write-movie foo.avi` option which allows me to easily record
a screencast (in mjpeg) of either the game or the IDE.  With respect
to the IDE it’s pretty imperfect because it doesn’t display the mouse
cursor, popup menus, or dialog boxes.

Unfortunately, even after aggressive recoding (see file
`ffmpeg-notes.md`) the videos produced are close to a megabyte a
minute, so I can only include a few seconds at a time in pavnotes2’s
space budget.  This depends somewhat on what’s on the screen, though.

Exporting (and Godot's lousy error handling)
--------------------------------------------

Building is called exporting.  I'm testing the exporting of a slightly
modified version of the 2D platformer example.  By default I guess no
“templates” are installed; apparently they all come in one bundle
(Linux, Android, MacOS, and all) which turns out to be 854 megs.  But
when I tried to install it, I got this message a lot of times:

      drivers/unix/file_access_unix.cpp:257 - Condition "fwrite(p_src, 1, p_length, f) != p_length" is true.

This makes me think maybe I didn’t have enough disk space, and the
unpacking failed.  And when I export, I get a zero-byte executable.
And in ~/.local/share/godot/export_templates/4.2.2.stable/ I see a lot
of zero-byte executables.  Deleting that directory (half a gig) and
restarting Godot (just in case) and re-attempting to export again
takes me to the “manage export templates” dialog, which is good.  I’m
going to see if I can free up three or four gigs right quick...

### Exporting a Linux executable ###

Okay, did that, so now it’s downloading the same 854 megs again.  This
time there are no error messages, and the files in the template
directory aren’t zero-length, puffing it up to 1.3 gigs.  And now the
resulting executable isn’t zero-length either.

However, there’s a .pck file sitting next to it, and if I move the
executable into another directory, it fails to run:

    Error: Couldn't load project data at path ".". Is the .pck file missing?
    If you've renamed the executable, the associated .pck file should also be renamed to match the executable's name (without the extension).

So I check Opciones → Binary Format → Embed PCK.  Now exporting
results in a 64-meg executable (instead of a 61-meg one) which works
properly!

It’s dynamically linked, though at 61 megs I don’t see why:

    : ~; ldd ./exectest.x86_64
            linux-vdso.so.1 (0x00007ffec4230000)
            libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007fb24878e000)
            libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007fb248789000)
            libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007fb2486aa000)
            libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fb2484c9000)
            /lib64/ld-linux-x86-64.so.2 (0x00007fb2487bf000)

There are architecture options for `x86_64` (selected), `x86_32`,
`arm64`, `arm32`, `rv64`, `ppc64`, and `ppc32`.  I do have ARM and
RISC-V emulators installed, but I don’t have the system libraries
installed for them, so although I can build the game for these
architectures, I can’t test it:

    : ~; file gdplatformer/exectest.arm32 
    gdplatformer/exectest.arm32: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux-armhf.so.3, for GNU/Linux 5.15.0, stripped
    : ~; gdplatformer/exectest.arm32
    arm-binfmt-P: Could not open '/lib/ld-linux-armhf.so.3': No such file or directory
    
### Different computer, same problem ###

I had the same problem again installing the templates on another
laptop.  I had proactively moved ~/.local/share/godot to a larger
disk, but Godot downloaded the giant zipfile to ~/.cache/godot before
unzipping it, and that step ran out of space.  I got lots of fwrite
error messages in the console, but the GUI just showed the download
progress bar freezing.

I also ran into some other badly handled error around there; I think
maybe the download never restarted after I switched Wi-Fi networks,
and I couldn’t cancel it either, or exit Godot, and had to ^C Godot
from the terminal.

### Wasm export ###

Exporting for “Web” gets me some web pages and wasm files (49 megs,
compressible to 12) and whatnot.  But on trying to serve them up from
a local stub web server, the resulting web page just shows these error
messages:

    Error
    The following features required to run Godot projects on the Web are missing:
    Cross Origin Isolation - Check web server configuration (send correct headers)
    SharedArrayBuffer - Check web server configuration (send correct headers)

Upon uploading them to my Apache web server, I get these enhanced
error messages instead:

    Error
    The following features required to run Godot projects on the Web are missing:
    Secure Context - Check web server configuration (use HTTPS)
    Cross Origin Isolation - Check web server configuration (send correct headers)
    SharedArrayBuffer - Check web server configuration (send correct headers)

Yeah, I guess I should use HTTPS.  But the others are more worrisome.

There’s [a closed GitHub bug report about this][21] which explains:

> Godot 4 is a multi-threaded engine, and multi-threading in
> WebAssembly requires SharedArrayBuffer support.
> 
> SharedArrayBuffer is now supported by all major browsers:
> https://caniuse.com/sharedarraybuffer
>
> It’s only hosting platforms such as itch.io which do not provide a
> good way to configure Cross Origin Isolation, but that’s an
> ecosystem problem that hosting platforms will have to solve.

[21]: https://github.com/godotengine/godot/issues/69020

And then Xananax explained:

> #### For local testing
> 
> Of course, you can just run the game from Godot (little "html5"
> icon).
> 
> But in case you need something custom and have npm installed, this
> is easy and painless:
>
>     npx local-web-server \
>       --https \
>       --cors.embedder-policy "require-corp"\
>       --cors.opener-policy "same-origin" \
>       --directory <my-game-dir>

I didn’t know about the little “html5” icon and still don’t know where
to find it.  Xananax also explained:

> #### Own Webserver
> 
> You need to have a certificate (use https). Then;
> 
> if you use Nginx:
> 
>     location some-location {
>       add_header "Cross-Origin-Opener-Policy" "same-origin";
>       add_header "Cross-Origin-Embedder-Policy" "require-corp";
>     }

Presumably the same thing is applicable to Apache, but they only
provided recipes for nginx and node.js Express, and then pijamarda
provided a recipe for Python.

> #### What does this mean?
> 
> Those headers restrict your page’s access, making it highly
> sandboxed. This unlocks ShareArrayBuffers. Browsers don’t give you
> access to this feature by default because it is easily exploitable.
> 
> When you have those headers, you will not be able to request any
> external resources to your domain. If you want to load a youtube video
> or some 3rd party javascript, the browser will block it.
> 
> But you only need those headers for the page hosting the Godot
> game. That page can be set in an iframe on another page with less
> stringent sandboxing requirements.

This is interesting and not something I’d ever heard of before.

Apparently [some games downgraded to Godot 3][22] to avoid this
problem.

[22]: https://github.com/dkopec/ferret-rescue/issues/20

The text editor
---------------

The Godot script editor isn’t Emacs or very Emacs-like, but it’s not
too bad.  Most of [the keystrokes][24] are the CUA keystrokes.  One
annoying feature is that alt-modified letters are just the bare
letters.  Some useful keystrokes (full list in Editor → Configuración
del Editor → Atajos):

- Ctrl-F1: switch to 2D view; Ctrl-F3 gets back to script.  Within
  these general categories you have Ctrl-TAB and Ctrl-Shift-TAB to
  switch between scenes.
- F5: run the game in debug mode.  F8 stops it.
- ^S: save the script (logs syntax errors to the console, though
  they’re displayed before that)
- ^F: modal incremental search.  ESC to exit, RET to jump to next hit,
  shift-RET to jump to previous hit.  You can use F3 or shift-F3 to
  jump to next/previous hits without using ^F, or ^F and you have a
  pending-delete copy of the previous search string.  You end up with
  the cursor at the end of the search string, which is pessimal.  This
  is a problem Emacs also has when searching forward, but Godot also
  has it searching backwards.
- Alt-G: select word under cursor.  Selection implicitly highlights
  full-word case-sensitive matches, and it sets the search string if
  you use ^F, so this is useful for navigation.
- ^R: find and replace (query replace, generally)
- ^D: adds an additional cursor for the next occurrence of the current
  selection, or the current word if none; this is useful for renaming
  a variable, especially a local variable, because you can do the same
  edit with each cursor, which is probably easier than ^R in most
  cases.  ESC cancels the multiple cursors.
- Shift-F11: toggle fullscreen mode for the Godot IDE window; however,
  this makes dialog boxes not work!  Dialog boxes have some real focus
  problems actually.
- Shift-Ctrl-F11: toggle sidebars; especially useful with the previous
- Ctrl-\\: toggle the list of scripts; especially useful with the previous two
- Alt-↑ and Alt-↓: move the current line or block of lines
- Ctrl-RET: move to a new blank line below
- ^K: comment or uncomment a line or group of lines
- ^+, ^-, ^0: zoom in, out, or normal
- Shift-^E: replace selection with its evaluation, including for
  things like `Vector2(3, 4) * 2`.  But not, apparently, using
  functions or variables defined in the script.

There’s a “bookmark” feature (Ctrl-Alt-B to bookmark the current line,
Ctrl-B to jump to the next bookmark, Ctrl-Shift-B to jump to the
previous one) but apparently no way to recover from a find motion.

There’s a “find symbol” right-button-menu option, which usually takes
you to the docs (Alt-← gets you back to the editor, from which Alt-→
gets you back to the docs).  It works for things defined in your own
script too, in which case it takes you to their definition.  I don’t
know how to use it from the keyboard yet, or how to move around from
the keyboard without the arrow keys.  Ctrl-left-click also does it,
though that’s still not using it from the keyboard.

For code folding, you have Alt-F to fold or unfold, and Alt-R to add a
new fold “region”.

Autocompletion knows about child nodes, their methods, and animation
names, so every token on a line like this can be fully autocompleted
except for the `$` and `.`:

    $AnimatedSprite2D.play("up")
    
Similarly it can autocomplete the node name in
`get_node("AnimatedSprite2D")` and the input action name in
`Input.is_action_pressed("ui_left")`.

[24]: https://www.youtube.com/watch?v=nbmlPkttHe8

GDScript
--------

It’s pretty neat to be able to say

    @export var color: Color

And get a color picker in the inspector sidebar as soon as you hit
Save.  Moreover, the color picker has hex and GDScript outputs to copy
and paste from; these are all valid:

    @export var color: Color = '7475c3'
    @export var color: Color = '#7475c3'
    @export var color := Color(0.455, 0.459, 0.765)
    @export var color = Color(0.455, 0.459, 0.765)

You’d think that last one wouldn’t give Godot enough type information
to make it show up in the inspector properly, but it does.

You can also do this to get a dropdown in Inspector Inspector:

    enum Type { TYPE_FOO, TYPE_BAR }
    @export var type: Type

You can’t do this, though:

    class Man:
        var name: String
        var level: int

    @export var man1: Man

The nested class definition is fine, but it says, “Línea 13: Export
type can only be built-in, a resource, a node, or an enum.”  But it
turns out [there’s a built-in dictionary type][25] which *is* supported for
export, so you can do this:

    @export var man := {
        name = "Bob",
        level = 3,
    }

And you can edit that dictionary in the inspector, no problem!

There is some limited type inference as you’re editing, so some type
errors get caught at compile time.

There’s a lot of built-in stuff you’d want for games, so you can, for
example, make a Node2D follow the mouse cursor very easily:

    func _process(delta):
        var error := get_global_mouse_position() - position
        position += min(error.length(), speed * delta) * error.normalized()
	rotation = lerp(rotation, error.angle() + PI / 2, .1)

Although that last line of course doesn’t quite wrap around rotation
the way you’d want!

And this line I screwed up from [the tutorial][27] doesn’t quite do what you want:

    position = clamp(position, Vector2.ZERO, screen_size)

The correct line, which does work, is:

    position = position.clamp(Vector2.ZERO, screen_size)

Apparently you have multiline lambdas and parametric polymorphism:

    var x = func(y):
        return y + 2
        
    var t: Array[int] = [1, 2, 3]

Though you invoke the lambdas with `.call` (and instantiate classes
with `.new`).

I don’t know how to find out what types it has inferred.

[25]: https://stackoverflow.com/questions/56022622/how-do-i-implement-structures-in-gdscript
[27]: https://docs.godotengine.org/en/stable/getting_started/first_2d_game/03.coding_the_player.html

Performance is acceptable, comparable to Python.  This takes 3.5
seconds, updating a global variable:

    for i in range(1000):
        for j in range(1000):
            for k in range(100):
                v = k - i

That’s roughly 30 million loops per second.

The debugger
------------

Provoking a type error, for example by adding a number to a color,
halts the game in the debugger, which shows you a stack trace pane and
a variable values pane, showing local and member variables, and a
pointer to where the current frame is stopped in the editor.  The
variable display seems to be a read-only version of the inspector
display, including things like colors.  Mousing over variables in the
source pops up tooltips with textual versions of their values.  You
can continue and ignore the error, allowing the game to keep running.
The debugger is a bit anemic in some other ways; there doesn’t seem to
be a way to jump backwards in the program or to change variables.

You can also click in the left margin to set a breakpoint while the
game is running.

However, *without stopping the game*, you can open the remote scene
tree in the IDE and live-inspect the properties of objects in it, and
change them!  This is super cool.  You can also pause and unpause it
with F7 when you’re doing that.  While the game is running, “Remoto”
and “Local” tabs appear above the scene tree, so you can switch
between editing the startup properties of your objects and viewing and
editing the live tree!  This is fucking amazing.  It’s almost on par
with web-browser functionality.
[`OS.set_window_always_on_top(true)`][26] may be a useful combination
with this.

[26]: https://www.youtube.com/watch?v=R5xNcB5Zbzs

Drag and drop
-------------

The IDE has a bunch of really neat drag-and-drop things.  You can drag
PNGs from the filesystem pane into a 2D scene to create a Sprite2D, or
a .tscn file to add a scene.  You can drag them into the node tree to
instance them.  If you drag them into a script, you get a filesystem
path pasted as a string; with Ctrl, you get a newly created constant
with the file preloaded, or for nodes, an `@onready var` for the node.
You can generally drag things into the inspector, too.  You can
apparently drag all kinds of things into animations, too.

List of things to explore next
------------------------------

- What’s the simplest game I can make?  Maybe Pong?  Or Dodge the
  Creeps?  Practice that from beginning to end a few times.
- How can I make a particle system?  Those have a great cost-benefit
  ratio.
- How can I make a 2-D glow effect?
- How can I make a tilemap?  I feel like a platformer with a tilemap
  should be easy enough.
- How about lighting?  What does it look like to light up a tilemap?
  Lighting can make even simple stuff look great.  Especially with
  HDR.
- Can I do a shader in 2-D?
- Can I build for Android and run a game on Android?
- Can I get the Wasm export working?  I bet I can do it on Tilde.
- What’s the easiest way to do a dialogue tree?  Can I make one
  Wikiable, so you can add stuff to it as you play?  I feel like this
  needs filesystem access.
- How does the animation feature work?  I feel like it’s a kind of
  weird meta-feature.
- What’s the simplest *3-D* game I can make?
- What’s the simplest 2-D skeletal animation I can make?  Maybe an
  inchworm?
- What does a simple fractal renderer look like?
- How do I draw text?
- What does network access look like?
- How can I simulate a liquid?

Some reflections on making a tilemap game
-----------------------------------------

So, I’ve written a simple tilemap game, something similar to a
platformer, with physics, particle systems, animations, and animated
sprites.  It may be a stretch calling it a “game”, since the only
interactivity is controlling the player character, who can fly.  There
were a lot of things that surprised me or were difficult.

* I was using a RigidBody2D for the player.  Trying to set its
  position (or velocity? I forget) directly in a `_process` method
  broke its physics.  This is documented in the RigidBody2D
  documentation but I hadn’t read it yet.  You’re supposed to use
  `apply_force` or `apply_impulse`.
  
* RigidBody2D doesn’t have `is_on_floor`.  That’s its sibling
  CharacterBody2D.  Likewise for `move_and_slide`.

* The tilemap is saved as a single long line of decimal numbers in
  worldmap.tscn.  Overall the attention to git-mergeability in the
  .tscn format is quite laudable; this is one of the few oversights.
  Most of the file looks like this, for about 1800 lines:

        terrain_set_0/terrain_0/name = "stone"
        terrain_set_0/terrain_0/color = Color(0.888591, 0.874796, 0.955521, 1)

* Upon opening the project from a fresh Git clone on a fresh computer,
  I couldn’t paint tile data (physics collision polygons, terrain
  data) until after restarting Godot.  I thought maybe something was
  out of date and got regenerated on restart.  (Later, though, I think
  I decided it was a problem with Godot's fullscreen mode interacting
  badly with Marco, the MATE window manager.)
   
* I was able to get the tilemap editor to paint platformer platforms
  (with initial, medial, final, and isolated combining forms) by
  setting each platform type as a “terrain” in the same edge-matching
  terrain set, and painting the matching edges and centers of the
  tiles with that terrain type, leaving the others set to “no
  terrain”.  At first I thought I needed to set empty space as an
  additional terrain, but I didn’t.  “Material” might have been a
  better term than “terrain”.

* I sort of regret having used the non-transparent 1-bit tileset
  (i.e., the version without a transparency bit).  That’s because my
  character sprites have little black boxes around them, and I can’t
  have multiple layers with parallax.  (But it turns out that the
  version with transparency doesn’t distinguish dark foreground from
  transparency either!)

* I really should figure out how to remove all the black space I’ve
  painted in my tilemap, both because it’s jarring when you go past
  its edge and into empty gray space (which can perhaps be fixed by
  setting a project-wide background) and because of the background
  parallax layer thing.

* The total amount of executable code in the game is 11 lines.  I had
  more before, but then I figured out that I could set an
  AnimatedSprite2D to automatically start at load time.  worldmap.tscn
  is about 1800 lines, but most of those are default properties on one
  or another tile in the tileset, lines like these:

        11:0/0 = 0
        16:2/0/physics_layer_0/angular_velocity = 0.0
        20:17/0/physics_layer_0/linear_velocity = Vector2(0, 0)

* The tilemap editing interface is pretty awesome.  You can draw in
  different materials (“terrains”) using Wang tiles to choose
  individual tiles, along with randomness; you can select a few tiles
  to scatter at random over a large area; you can copy and paste
  around the tilemap; and you can assign things like physics colliders, 

* The IDE’s Shift-F11 full-screen mode has serious problems with
  keyboard focus and alt-tab.  Ctrl-Shift-F11 (hide/show side docks)
  and Shift-F12 (expand/unexpand bottom panel) are more useful.  Also,
  and I just found this out, you can click on the currently open
  bottom-panel tab to hide the bottom panel altogether.  This will be
  great for editing code and repositioning nodes in 2-D on small
  screens; unfortunately it doesn’t work for editing tilemaps on small
  screens, because when you close the “TileMap” tab down below, you
  can’t edit the tilemap anymore.

* Related to the fullscreen problem, there’s a problem where the
  “Select a property editor” dropdown in bottom panel → TileSet →
  Paint simply hides Godot and displays some other fullscreen window,
  unless there is none.  This might have been the problem I had to
  restart Godot for, and I think it’s maybe actually the fault of the
  Marco window manager.
  
* On this MicroPC, it’s nice that I can increase the editor text size
  with Ctrl-= (though not Ctrl-+ for some reason, which inserts a +),
  but there’s no way to do the same thing for the documentation.

* You absolutely can edit the GDScript code in the middle of running a
  game and see the effect, and that is freaking awesome.  It’s
  especially helpful if you have a way to split the screen between the
  IDE and the running game, or run them on two separate monitors.  The
  1280×720 of this MicroPC screen is not enough, though; Godot refuses
  to be shrunk to less than 768×450 pixels.  Making the game “always
  on top” is about the best you can do.  (I wonder if you can do a
  remote deploy on a different GNU/Linux box and puppet it in the same
  way?  Then you’d have a monitor, keyboard, and mouse for the Godot
  IDE, and another set for the game.)

* There were a lot of tasks that seemed to require doing things in
  several apparently unrelated places.  Adding a terrain, for example;
  first you have to open the TileMap in the right-dock inspector, and
  within its TileSet (it always confuses me whether I have the TileSet
  open and am closing it, or have it closed and am opening it), you
  create the terrain set and some terrains, assigning colors that
  don’t seem obvious.  Then in the bottom panel you have to navigate
  from the TileMap tab (which doesn’t look like a tab) in the bottom
  right to the TileSet tab to its left, and then of the three tabs at
  the top of TileSet, you have to go to the “Paint” tab (rather than
  Setup or Select), and then you can select “Terrains” from the
  dropdown.

* Relatedly, I still haven’t figured out how to do animations by
  example, Flash-style, rather than by typing in new numerical values
  for positions on keyframes.  You’re supposed to be able to somehow
  get a key icon to show up to the right of every property on every
  node as you’re recording an animation, but I haven’t figured out how
  you do it yet.

* As with LÖVE, Godot games will use 100% of one CPU and run your
  battery down even when you’re doing nothing.  (Well, the Godot IDE,
  which is also a "Godot game", seems to be throttling itself down to
  about 20% of a CPU right now, doing nothing; this seems to jump to
  40% if it has the keyboard focus.)  Also they are 64–128 megabytes
  to download, and they take about a second to start up even from a
  warm cache.

What should I try next, now that I have tilemaps?
-------------------------------------------------

It would probably be good to start with a boringly clichéd platformer,
where you run and jump on some goombas, bang your head on some mystery
blocks to collect powerups, scroll a parallax background, collect
coins to make a meaningless number on the screen go up, animate the
player and the goombas, play some music and sound effects, and finally
reach a destination, or die because you’re out of time.

But the Lunar Lander dynamic I’ve come up with in my tile test game
seems promising: instead of jumping, you thrust upwards and either
left or right, which allows you to travel through a landscape with
some Flappy-Bird-like difficulty; the slow acceleration without any
solid friction makes the movement controllable, but only with
significant effort.  The inadvertent camera-shake effect when the
little guy falls over is also cool.  I’m not totally sure I want to
use the Godot physics engine at all, though, given how hard it seems
to be to keep from getting the ship stuck or fall so hard it clips
through a floor.

It would also be good to spend some time on making something more
visually striking, with effects like particle systems, camera shake,
2-D lighting, camera bloom, ACES tonemapping, etc.

I think trying Dialogic is also a pretty high priority.  And Cogito
also looks super cool.

The things that really excite me about Godot, though, aren’t making
visually impressive demos or Dialogic or platformers.  It’s more about
being able to whip up HCI experiments I can run on my phone, a
portable runtime where I can synchronize data between the phone and my
laptop, an algorithmic playground for exploring the possible
performance ceiling of modern hardware, displaying high-performance
simulations in a browser with Wasm, running calculations, and doing
3-D modeling of electromechanical systems, etc.  And, in some sense,
I’m going to have to find things to do with Godot that require a lot
of code, or hairy code, in order to play to my own strengths.

I’m also excited about the possibility of using Godot for network
communication, things like WhatsApp voice messages and Discord.  It
feels like I could write a network communication terminal application
in Godot and run it on Android as well as GNU/Linux.  Apparently [you
can access iPhone cameras in Godot][36], which is by far the
highest-bandwidth form of input, but not yet Android.  [Android
support via ARCore has been coming soon for four years now][37], which
is [getting moved to a plugin][38], while maybe we’ll get camera
support on other platforms soon.  There’s also [a plugin for getting a
single image on Android][39].

[36]: https://docs.godotengine.org/en/stable/classes/class_cameraserver.html "CameraServer and CameraFeeds"
[37]: https://forum.godotengine.org/t/how-to-use-webcam-cameraserver-camerafeed-in-3-2-and-windows/21891/2
[38]: https://github.com/godotengine/godot/issues/46531
[39]: https://github.com/Lamelynx/GodotGetImagePlugin-Android

So I guess the big unknowns to tackle there for self-empowerment are:

1. Can I get Android and Wasm builds working?
2. Can I build a GDExtension, scripting Godot in native code?
3. What can I do in a compute shader?  Can I, for example, run Euler
   integration of an analog circuit?
4. Can I do 3-D mechanical design in a Godot “game”?
5. Can I load new compute shaders or fragment shaders at runtime?  It
   must be possible because the IDE loads new fragment shaders.

But those have to be balanced with “demo features” like Dialogic,
platformers, particle systems, lighting, and sound.

2-D lighting
------------

I am sold on [using cheap effects to make things look cool][46].

[46]: https://www.youtube.com/watch?v=WO_kBYqAQwUf

I was able to get my 2-D platformer test “game” to have some lighting
around the player for a cavern-like atmosphere by adding a
DirectionalLight2D subtracting about 75% of the brightness, adding an
occluder node that covers a square in the center of each solid tile,
and adding a PointLight2D to the player, with a radial gradient.  When
I initially made the occluder fill up the entire floor tile, with
shadows turned on on the PointLight2D, that kept the tiles from being
illuminated at all; making the occluder not fill up the entire tile
allowed the surface pixels of the tile to be illuminated.  There’s
still some leakage in between the tiles, but it’s an acceptable visual
artifact, I think.

Probably if I add a background tilemap for a level, the occluders will
cast shadows across it, which will be super atmospheric (though maybe
too distracting).  But then the leakage through the cracks between the
tiles would be really annoying.  I’m pretty sure I can fix it by
giving each tile a different occluder polygon.

So far I haven’t been able to get [a bloom-glow effect][40] to work
properly in 2-D.  I’m not sure what I’m doing wrong.  I’ve enabled 2D
HDR, I’ve added a WorldEnvironment node, I’ve enabled glow in it, I’ve
tweaked another dozen settings I don’t understand.

[40]: https://docs.godotengine.org/en/4.2/tutorials/3d/environment_and_post_processing.html#doc-environment-and-post-processing-using-glow-in-2d

Oh!  I still had the `background_mode` in the WorldEnvironment set to
“Clear color” instead of “Canvas”.  Changing that now makes everything
a mess of glow from all the other settings I had turned up to try to
get the glow to work.  (So I turned them back down.)  Now I have
awesome glow!  I feel like it hurt my FPS, though, which is now like
18–28 mostly, but pretty janky and not consistent, and dropping over
time, which makes me think it’s maybe a thermal throttling thing.
Yeah, setting it back to “Clear color” put the FPS at 60 again, with
the occasional stutter down to 52.  So at least this approach to
getting 2-D glow is pretty costly to performance.  Turning the glow
off also gets the performance back, even with “Canvas”.

There’s a setting in Project Settings → General → Rendering →
Environment → Glow called “Upscale mode”, which defaults to “Bicubic
(slow)” and can be set instead to “Linear (fast)”.  This seemed to
help with the glow performance at first (I got 60fps glow for a while)
but then it went back to 20fps with random jank.

Za3k reports that on his laptop it is also janky and slow (40fps).

This is kind of bullshit I think.  Even without a GPU I feel like you
don’t need a lot of compute to add a Gaussian-filter glow effect to
thresholded pixels.

Some fun game ideas
-------------------

How about a Wang tile puzzle?  Maybe with triangles?  This would be a
good fit for Android.

How about Beltrán’s melancholy abandoned cathedral setting?

How about a digital logic satisfiability game?  Maybe a curve can be a
2-input NAND gate.

A conventional platformer that integrates dialog could be
interestingly novel.

The Lunar-Lander dynamic I have going on in my tilemap test game could
be interesting with a resource-management angle: fuel, fuel powerups,
avoiding impact damage, etc.

A platformer that incorporates a maze generator would be an
interesting twist; it could have higher replay value than a regular
platformer.  Even just a simple maze generator with walking would be
pretty interesting.

Can you make the Lorenz attractor into a game?  Maybe you could
randomly place bonuses and maluses on the attractor (say, by
generating them at random points in space, then integrating them
forward until they’re within epsilon of the attractor) and visualize
the character’s position ahead along the attractor with a
predicted-trajectory line, while the player tries to perturb their
trajectory in a way that will gain bonuses and avoid maluses.
Difficulty could increase over time by increasing velocity along the
attractor, changing the bonus/malus ratios, or shortening the
predicted-trajectory line.

How about an open-world game where you build things out of adobe?

How about a pixel-art editor?  Maybe there’s one already in Godot.

A stock-market simulator could use real-world historical data, but
maybe something like linear combinations of Brownian factors would be
more interesting.

I was thinking it would be cool to have a game where you learn to cast
magic spells by tracing Android-lock-screen-like glyphs; if you screw
up, it might damage you, and maybe if you succeed, it could transport
you or damage your opponent or something.  Nethack-like, a way to
limit knowledge carryover from one game to the next would be to
generate new random glyphs each game.  There are different possible
ways to acquire new glyphs: exploring a world to find artifacts,
talking with NPCs, experimenting to see if you hit on a useful glyph
by luck, etc.

I’d really like to do some kind of circuitry bench simulator.

I should *definitely* do a Tetris.

Some kind of PID-loop-tuning game seems like it could be interesting.

It occurred to me that you could do some kind of Towers of Hanoi thing
running around ferrying tokens between different stations; Star
Control 2 did something like this in a very compelling way, but it was
largely artifacts and information you were ferrying around.

David Ahl’s [101 Microcomputer Games][41], [recently dedicated to the
public domain][43], are each about a page of code in BASIC.  Some of
them might be worth implementing, at least as exercises.  He included
acey-ducey, a maze generator, guess the animal, awari, “bagels” (sort
of like Wordle, but for three-digit numbers), a line-printer banner
generator, a basketball game, a remove-objects-from-the-pile game
called “Batnum”, a 6×6 Battleship (plus a lamer variant called
Bombardment), Blackjack (which surprisingly occupies almost two whole
pages), a super lame WWII bomber game, a bouncing-ball simulator, a
super lame bowling game, a boxing game, a dumb game where you roll
dice to draw a bug, a fairly lame bullfight game, a fairly lame
dart-throwing simulation, a Playboy logo, a buzzword generator
(“Differentiated Creative Facility”), a calendar generator, a
cash-register simulator, checkers, a dumb program that forces you to
mentally approximate ratios, another cringey mental math exercise
program called “CHIEF” that always forces you through the same math
exercise, Scientific American’s game “Chomp”, a historical simulation
of the US Civil War, a lamer simulation of a hypothetical war (maybe
some kind of rock-paper-scissors dynamic?), craps, a lame game where
you try to cross a cube-shaped minefield, etc.  I should probably not
just list them indiscriminately, because that’s only about a third of
them.

Of these, a lot of the ones that have physical elements that were hard
to simulate in the 01970s could work very well now: artillery
trajectory simulations, bullfighting, dart throwing, bowling, etc.

[41]: https://annarchive.com/files/Basic_Computer_Games_Microcomputer_Edition.pdf
[43]: https://blog.adafruit.com/2022/06/16/david-ahl-places-all-his-classic-computing-publications-into-the-public-domain/

In [this video from last year, “Rungeon”][44] demonstrates how to
modify a tilemap during gameplay; he’s binding actions to the left and
right mouse buttons in the input map, then checking them in his
TileMap’s `_physics_process` method to modify tiles with the
`set_cellv` method using a Vector2 obtained from
`world_to_map(get_global_mouse_position())`, which is really an
admirable example of API design.  The tile-index argument to
`set_cellv` is maybe a little less admirable, using -1 to mean “no
tile”.  [This got a bit hairier in Godot 4][45] but it’s still not too
bad.

[44]: https://www.youtube.com/watch?v=d0Hb1Zs5GMQ
[45]: https://www.youtube.com/watch?v=Yo4wM-H_xsU

Some notes from reading the 2D manual
-------------------------------------

It says you should use CanvasLayer to separate HUD UIs and parallax
backgrounds from the rest of the scene so they aren’t subject to the
`Viewport.canvas_transform`.  By default things are drawn in layer 0,
but you can create a CanvasLayer node to draw them in a different
layer.

There's a `CanvasItem.make_input_local` function to translate input
coordinates (global?) to the CanvasItem’s local coordinates.  Not sure
exactly how this relates to `.to_local`.

Oh and GDScript has matrix multiplication built in because of course
it does:

    canvas_pos = get_global_transform() * local_pos
    local_pos = get_global_transform().affine_inverse() * canvas_pos

There’s [a note about how to fake input events][47], with the proper
coordinates.

[47]: https://docs.godotengine.org/en/stable/tutorials/2d/2d_transforms.html

Apparently [the 2D lighting system can even use normal and specular
maps!][48] They recommend making these maps with [Laigter][49] (GPL3,
[GH repo][50]).  And the right way to darken the scene is with
CanvasModulate, not a directional black light.  And you can use
Sprite2D’s Region to create a repeating background.

[48]: https://docs.godotengine.org/en/stable/tutorials/2d/2d_lights_and_shadows.html
[50]: https://github.com/azagaya/laigter
[49]: https://azagaya.itch.io/laigter

Okay, well, I just did some more fiddling with Godot.  I made a
Sprite2D with a normal map generated from a noise texture, which turns
out to only be able to generate about a million texels per second when
I keep tweaking its X offset, a particle system using noise textures
for the (square) particles, a body emitter that instantiates a
RigidBody2D scene called “Lunk” with preload() and .instantiate() and
a timer signal, and a StaticBody2D for the lunks to fall on and pile
up.  This is my first time using normal maps, noise textures,
programmatic scene instantiation, timers, and StaticBody2D.

I was surprised that I couldn’t set the .scale on the RigidBody2D.
Well, not usefully; it keeps getting set back to 1.  I settled for
setting the .scale on its child nodes (a Polygon2D and a
CollisionPolygon2D).

2D mouselook for quasimodal input
---------------------------------

I have this idea for how to do a UI using a mouse with the right hand
and with the left hand on the keyboard.  I’ll use mouselook in a
potentially large scrolling 2-D canvas (rather than a 3-D vision
sphere) to select objects to apply operations to, with mouse buttons
and keyboard keys to open up menus and properties and whatnot of the
object under the mouse.

So I set up a Godot project called `gdmouselook`.

2D mouselook turns out to be super easy:

    func _ready():
        Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

    func _input(event):
        if event is InputEventMouseMotion:
            $Camera2D.position += event.relative

Mouse wheel events are reported as `MOUSE_BUTTON_WHEEL_UP` and `_DOWN`
events, so this zooms in and out:

    if event is InputEventMouseButton:
        if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
            $Camera2D.zoom /= 2
        elif event.button_index == MOUSE_BUTTON_WHEEL_UP:
            $Camera2D.zoom *= 2

But to keep the perceived mouse speed constant, then you need this:

            $Camera2D.position += event.relative / $Camera2D.zoom

I felt like the zoom was a little too fast, so I did this to slow it
down, with a `mag_level` variable to prevent rounding errors.

        var down = event.button_index == MOUSE_BUTTON_WHEEL_DOWN
        var up = event.button_index == MOUSE_BUTTON_WHEEL_UP
        if up or down:
            mag_level += 1 if up else -1
            $Camera2D.zoom = Vector2.ONE * 2.0**(mag_level/4)

Existing widely used mousewheel zooming interfaces vary widely.
OpenStreetMap’s web site seems to use factors of 2, so there are 11
steps between Buenos Aires and the world.  Google Maps requires 32 to
get to half the world, so I guess they’re using a step of about ∛2 ≈
1.260.  Godot’s own editor requires 12 steps to get to a factor of 2,
so I guess it’s about 2<sup>1/12</sup>, the interval between 12-TET
notes, about 1.059.  FreeCAD uses about 23 steps to get a factor of
100, which is a step factor of about 1.22.  Firefox’s
control-mousewheel for changing the text size (the same as zooming if
it doesn’t relayout) isn’t exponential; it changes the zoom by ±10%,
so the same control-downwheel will take you from 50% to 40% or from
300% to 290%, which is a factor of 1.1 if you happen to be moving
between 100% and 110%.  [Mitxela’s model viewer][58] seems to use a
factor of about 1.11.  The median of 1.059, 1.100, 1.22, 1.260, and
2.000 is FreeCAD’s 1.22, so I picked √√2 ≈ 1.189.  Perhaps by
coincidence, this is the value I have set for Emacs face-remap.el’s
text-scale-mode-step, which is how Emacs scales your text with
control-mousewheel.  It defaults to 1.2.  I don’t remember setting
this, but apparently I did it in the last year.

[58]: https://mitxela.com/projects/model-viewer

I added a couple of ColorRects as children of the Camera2D as a way to
display crosshairs, although those unfortunately also zoom with the
camera zoom.  The camera is helpfully centered on its position, so I
can send an object that isn’t its child to the center of the camera
pretty easily too:

    func _process(delta):
        $Eye.position = $Camera2D.position

Finding out whether a key not mapped to an input is held down or not
is a little uglier.  This (in `_input`) mostly works, handling both
pressed and released events, but can fail when focus is lost to
alt-tab:

    if event is InputEventKey:
        if event.keycode == KEY_D:
            $Eye.scale = Vector2(2, 2) if event.pressed else Vector2(1, 1)

The keycode is mostly an ASCII uppercase letter in cases like this.
My caps lock key shows up as CapsLock (`KEY_CAPSLOCK`, a large 32-bit
value) even though I’ve mapped it to Ctrl.  (The released event shows
up as Ctrl+CapsLock.)

The [polling approach][52] works better for that.  I added a “bulge”
action in the input map and bound the D key to it, and then doing this
in `_process` solves the alt-tab problem:

    $Eye.scale = (Vector2(2, 2) if Input.is_action_pressed("bulge")
            else Vector2(1, 1))

[52]: https://docs.godotengine.org/en/stable/tutorials/inputs/input_examples.html#events-versus-polling

So then there’s the problem of how to find out what object in the
world is in the crosshairs (or what objects) and interrogate it for
applicable actions.  We could fall back on collision detection since
we’re moving $Eye around with the crosshairs, or even just iterate
over a collection of objects and test each one manually.  I’ve seen [a
devlog video][51] where the programmer used
[PhysicsPointQueryParameters2D][53] and
`DisplayServer.mouse_get_position()` to figure out what objects the
mouse was pointing at, and I feel like I could probably do the same
thing with the camera position.

[51]: https://www.youtube.com/watch?v=x8BO9C6YtlE
[53]: https://docs.godotengine.org/en/stable/classes/class_physicspointqueryparameters2d.html

It’s not immediately obvious how to call
`PhysicsDirectSpaceState2D.intersect_point` with it because I don’t
know how to get a `PhysicsDirectSpaceState2D`.  The [physics
introduction][54] also, temptingly, says:

> By default, areas also receive mouse and touchscreen input.

Which might sort of work since the mouse mode constrains the mouse to
the center of the screen.

Aha, the [ray casting tutorial explains][55]:

     var space_state = get_world_2d().direct_space_state
     # use global coordinates, not local to node
     var query = PhysicsRayQueryParameters2D.create(Vector2(0, 0), Vector2(50, 100))
     var result = space_state.intersect_ray(query)

[54]: https://docs.godotengine.org/en/4.2/tutorials/physics/physics_introduction.html
[55]: https://docs.godotengine.org/en/4.2/tutorials/physics/ray-casting.html#raycast-query

As it happens, another of its examples is precisely figuring out what
the player is pointing at with their mouse, using
`get_viewport().get_mouse_position()`.  So I did the point-query thing
corresponding to the above code:

     var q := PhysicsPointQueryParameters2D.new()
     q.position = $Camera2D.position
     q.collide_with_areas = true
     var space_state := get_world_2d().direct_space_state
     print(space_state.intersect_point(q))

Incidentally, this feels a little awkward to me; I'd rather do
something like this:

    print(get_world_2d().direct_space_state.intersect_point(
        position = $Camera2D.position,
        collide_with_areas = true,
    ))

But anyway, that gave me empty arrays when my crosshairs weren’t
pointed at anything, and when they were:

     [{ "rid": RID(622770257920), "collider_id": 23924311252, "collider": Table:<Area2D#23924311252>, "shape": 1 }]
     [{ "rid": RID(622770257920), "collider_id": 23924311252, "collider": Table:<Area2D#23924311252>, "shape": 1 }]

So, a nice thing about that is that the collider is not the
CollisionPolygon2D I defined, but the Area2D node that is its parent
(which I named Table, because it looks like a table).  So I ought to
be able to define a method in its script and call it here, and indeed
this does work:

    for intersection in space_state.intersect_point(q):
        var c = intersection['collider']
        if c.has_method('be_pointed_at'):
            c.be_pointed_at()

I just added a `func be_pointed_at(): ...` in the Area2D's associated
script, and the above code does successfully call it.

A little more work, with a `pointed_thing` variable in the node to
keep track of what we’re currently pointing at, gets us this.
Apparently, as in JS, you can’t call `has_method` on `null`.

    var new_pointed_thing = null
    for intersection in space_state.intersect_point(q):
        var c = intersection['collider']
        if c.has_method('focus'):
            new_pointed_thing = c

    if new_pointed_thing != pointed_thing:
        if pointed_thing != null and pointed_thing.has_method('blur'):
            pointed_thing.blur()
        pointed_thing = new_pointed_thing
        if pointed_thing != null:
            pointed_thing.focus()

For simple debugging, I set up my Table node with these methods:

    func focus():
        $Polygon2D.color = '#ff77ff'

    func blur():
        $Polygon2D.color = '#cccccc'

So then, to achieve the kind of thing I have in mind, I would have to
do several things:

- Display the pointed thing’s available operations on the screen, in
  some kind of popup HUD thing, I guess;
- Change the *effective* pointed thing to one of those operations
  rather than the node in the world when a key or mouse button is held
  down, so that the operation itself provides the new set of available
  operations;
- Revert to pointing at things in the world when you release the key
  or button invoking the quasimode.

Some kind of popup HUD thing for showing possible UI actions
------------------------------------------------------------

I looked at [a “Top Down Action RPG” example game][57].  When running,
the root (a Window, inheriting from Viewport) has a child named
Outside (a scene), which contains a ‘level’ node, which contains the
“Character” representing the player, which contains a Camera2D, so the
camera follows the player around the level.  But then there’s a
sibling of ‘level’ of type CanvasLayer, which contains various HUD
things, like a DialogBox containing text of a conversation, Healthbar
(an HBoxContainer) containing three TextureRect nodes for hearts, etc.

[57]: https://godotengine.org/asset-library/asset/487

I guess the thing I don’t really understand is what defines the scope
of the Camera2D.  It seems like maybe it’s just “everything except
CanvasLayers”?

I suspect that maybe just having a CanvasLayer by itself might be
enough to build a HUD.  There’s a `follow_viewport_enabled` property
which puts my health hearts into the world (near the origin of the
level) instead of on the display, but with `follow_viewport_scale` I
can get some parallax.  By setting it from the default 1 to 39 I get
giant hearts that are apparently right next to the camera.  If I set
it to 0.5, I get parallax suggesting that it’s behind the world, which
also benefits from setting `layer` to -1 instead of 1.  (The default
draw ordering based on the tree order and `z_index` doesn’t apply to
CanvasLayers, which aren’t CanvasItems.)

There’s [a tutorial about this][56].  It explains, “The Viewport’s
property `Viewport.canvas_transform`, allows to apply a custom
Transform2D transform to the CanvasItem hierarchy it contains. Nodes
such as Camera2D work by changing that transform.”  So that’s what
defines the scope of the Camera2D: it’s the descendants of the
Viewport that aren’t CanvasLayers.

[56]: https://docs.godotengine.org/en/4.2/tutorials/2d/canvas_layers.html

Great!  So I just created a Label in HUD (a CanvasLayer I just added
to my World scene), and in my handling of pointing to a new
`pointed_thing`, I added this:

    $HUD/Label.text = "Pointed: " + str(pointed_thing)

And that’s enough to display stuff in a HUD that depends on what
you’re currently pointing at.  Just by virtue of being in the
CanvasLayer it follows the camera around the scene.  The text is drawn
with a transparent background, which is maybe suboptimal, but probably
easy to fix.  (At least a drop shadow would be useful.)  I can set its
`modulate` property (in CanvasItem Visibility) to a partly transparent
color to make the text foreground also transparent.

Yeah, under `label_settings` I can set outline and shadow properties.
A 3-pixel black outline makes the text readable on bright backgrounds
as well.

Moving the crosshairs to the HUD CanvasLayer also eliminates the
problem where the Camera2D’s zoom makes them grow and shrink.  But I
have to position them in what will be the center of the view, instead
of at the origin.  I did this by centering them over the lower right
hand corner of the Camera2D node in the scene display.

So then all I’d really need to do is to query the pointed thing for
its available set of operations.
