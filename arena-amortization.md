Bump-pointer allocators are pretty efficient; typically they require
2-4 instructions per allocation, to increment the allocation pointer,
possibly copy it, check it against the end of the arena, and jump to
some kind of exception handler if it’s full.

But, for example, Chicken Scheme does a lot better than this; it
performs a whole block of allocations at once, typically by
subtracting a constant from the hardware stack pointer, because it
allocates its (CPS-transformed) activation records in the garbage
collector’s nursery (Cheney on the MTA).

I’m not quite clear on why Chicken is comparatively slow, but I
suspect it’s just bad at avoiding (relatively expensive)
procedure-call operations and at producing C subroutines that are big
enough for the C compiler’s optimizer to do a good job on.  Also, I
suspect Cheney on the MTA is not great for the data cache.

I think you could use a Chicken-like approach with an ordinary
generational-GC heap (that is, one separate from the call stack) and
get a significant speedup for many workloads.  Suppose that a Thing is
usually a subroutine; upon entry to a Thing you ensure that there is
enough space in the nursery for everything it might heap-allocate, and
if not, you run the garbage collector or expand the arena or whatever,
and allocate all that space before proceeding.  Then, within the
Thing, you don’t have to do any such checks or bump-pointer
incrementation.  (In cases where you’re heap-allocating inside a loop,
you need to split the loop body into a separate Thing so you can bound
its allocation activity.  You might want to do this to big
conditionals, too, because there’s a tradeoff between consuming the
nursery more quickly than necessary (due to conditional allocations
not used) and execution speed (due to additional checks).)

This is presumably a known technique, given the amount of work on
garbage-collected runtimes since the Java fad in the 90s.
