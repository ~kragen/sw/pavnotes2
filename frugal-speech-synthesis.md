In file `programming-junk.md` I was exploring the possibilities of
real-time speech synthesis as programmatic output from tiny
microcontrollers.  Intuitively I feel like this should be easy, given
the Speak & Spell, but the software I have at my disposal seems like
it might strain the memory capacities of most microcontrollers.

SAM, the Semi-Automatic Mouth, could produce comprehensible speech on
an Apple //e or I think a C64, so it's clearly feasible.  There's a
reverse-engineered SAM on GitHub now, but it's not free software.

Though some prefer Festival, eSpeak works well and produces much
better output than SAM, and it's free software.  On this amd64 palmtop
my /lib/x86\_64-linux-gnu/libespeak.so.1.1.48 is 311\_976 bytes, but
/usr/lib/x86\_64-linux-gnu/espeak-data is 2.9 MB.

I tried this:

    valgrind --tool=cachegrind espeak -w hello.wav \
      'Hello, world.  How are you doing?  Would you like to play a game?'
      
It generated a 4.11-second output file in 48\_271\_536 instructions,
11.7 million instructions per second, so it's using quite a bit more
CPU than SAM, but still on the order of 5% of an ESP32.  KCacheGrind's
CEst estimates that this is 41\_275\_686 CPU cycles.

It doesn't use the whole 2.9MB of data on every run.  Aside from
libraries and locales, strace says it opened the following files:

    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/espeak-data/phontab", O_RDONLY) = 3
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/espeak-data/phonindex", O_RDONLY) = 3
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/espeak-data/phondata", O_RDONLY) = 3
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/espeak-data/intonations", O_RDONLY) = 3
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/espeak-data/voices/default", O_RDONLY) = 3
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/espeak-data/en_dict", O_RDONLY) = 3

These are only like 600K:

    40K     phontab
    28K     phonindex
    400K    phondata
    4,0K    intonations
    4,0K    voices/default
    116K    en_dict
    592K    total

So with eSpeak you need about 900K of data and 12 MIPS to synthesize
reasonable, if somewhat mechanical-sounding, English from
conventionally spelled text.  phondata and en\_dict can be compressed
significantly with gzip or brotli but it isn't clear that that is
helpful when the problem is RAM.

To see how much of this data access is related to waveform synthesis
from phonemes and how much is related to converting text to phonemes,
I tried requesting no waveform, just the phonemes:

    strace -o espeak.qstrace espeak -xq
        'Hello, world.  How are you doing?  Would you like to play a game?'

This produced, if I'm counting right, 40 phonemes, 9.7 per second,
which is a bit slower than people normally talk but not much:

     h@l'oU
     w'3:ld
     h,aU A@ ju: d'u:IN
     wUd ju: l'aIk t@ pl'eI a# g'eIm

It still, however, opened the same set of files.

How much space should diphone data need?

I have a 13.01-second recording in LPC-10 format of my voice; it's
4046 bytes, 311 bytes per second (2488 bits per second; should be
2400).  It's almost comprehensible; 600 bytes per second should be
fine.  The GSM Full Rate codec is 13 kbps, 1625 bytes per second.

(The LPC-10 code in SoX I used to make that recording is about 50K
when recompiled with `gcc -shared -Os -fPIC lpc10/*.c -o lpc10.so`.)

I feel like a second of speech is enough time for about 10 diphones.
General American has 24 consonants and 14-16 vowels for a total of
about 40, and thus a maximum of 800 possible diphones, 80 seconds of
audio, encodible in 24 kilobytes at 600 bytes per second.

From looking at the code, `phondata` evidently does contain spectral
data, just 16 times as much as I am estimating is needed.  I am
inferring that the phon\* files all relate to making waveforms and
that I'm probably being naive.

I tried recompiling libespeak with dietlibc to see if that reduced its
size:

    espeak-1.48.04+dfsg$ debian/rules build CC='diet cc' LD='"diet ld"' \
        CXX='"diet g++"' -k

Instead it increased it, to 1.7MB.  I tried just using `-Os`:

    espeak-1.48.04+dfsg$ debian/rules build CFLAGS=-Os CXXFLAGS=-Os

This gave me a 309K libespeak instead of a 311K one.
