I’ve written previously about aluminum phosphate (especially
berlinite), filled systems, cements, chemically bonded ceramics, and
so on.  Here are some more thoughts in that vein.

Low-temperature heat-cured ammonia berlinite cement
---------------------------------------------------

Suppose you mix together powdered aluminum hydroxide (gibbsite) and
desiccated powdered diammonium phosphate (DAP), a fairly stable
ammonium phosphate (though not quite as stable as MAP).  Nothing will
happen, except perhaps imperceptibly at the surfaces of the powder
grains where a tiny amount of aluminum phosphates will form, releasing
water and ammonia.  I think this is self-limiting.  Although a variety
of compounds of aluminum, hydroxide, phosphorus, and ammonium are
possible, the ideal stoichiometric reaction produces aluminum
orthophosphate, the most strongly bonded member of the family, and it
is

> Al(OH)₃ + (NH₄)2HPO₄ = AlPO₄ + 3 H₂O + 2NH₃

(The room-temperature stable crystalline phase of aluminum
orthophosphate is berlinite, which is the term I’ll use below, even
when we might be talking about nanoparticles or amorphous stuff.)

If you wet the mixture, the reaction will happen much more rapidly,
but the aluminum hydroxide is insoluble, so each grain will acquire a
shell of aluminum phosphates that thickens more and more slowly,
limited by diffusion through the aluminum phosphates.  This depends on
grain size and nanomorphology, and plausibly if the aluminum hydroxide
is in the form of some kind of amorphous gel (hydrogel, xerogel,
aerogel) it will happen quite rapidly indeed.

Although the curing step here does involve baking, it’s more like
baking Fimo clay than it is like firing pottery.  Diammonium phosphate
releases ammonia quite rapidly above 155°, leaving behind monoammonium
phosphate, which decomposes at 200° to phosphoric acid, which will
attack aluminum hydroxide with great exothermic gusto.  However, the
resulting ammonia and water vapor outgassing may pose a significant
problem by mechanically separating the reagents before the reaction is
complete, and compromising or destroying the mechanical integrity of
the reaction results.

The ammonia vapor pressure of DAP is almost insignificant at ambient
temperature, but I believe rises in the usual way with temperature.

Berlinite [melts at 1800°][3] and [has a Mohs hardness of 6.5][4] in
part because it’s a network solid like quartz and sapphire.  This
melting point is in between quartz’s (1713°) and sapphire’s (2072°),
and consequently aluminum phosphate is widely used in refractories.
It weighs 2.65 g/cc in its usual crystalline form.

[4]: https://en.wikipedia.org/wiki/Berlinite
[3]: https://en.wikipedia.org/wiki/Aluminium_phosphate

### Calming the reaction with inert filler ###

Berlinite is quite similar to quartz, just alternating trivalent
aluminum atoms with pentavalent phosphorus atoms instead of just using
tetravalent silicon.  [Wikipedia says berlinite’s crystal
structure][0] is trigonal and trapezohedral in space group P3₁21 or
P3₂21 with [unit cell][2] parameters *a* = 4.941 Å, *c* = 10.94 Å, and
*Z* = 3, and I wish I knew what that meant.  And [it says quartz’s
is][1] trigonal and trapezohedral, space group P3₂21 (all for
α-quartz, not cristobalite), and with unit cell parameters of *a* =
4.9133 Å, *c* = 5.4053 Å, and *Z* = 3.

Although the *c* values here are quite far apart, I have this vague,
perhaps unjustified idea that this means that berlinite should be
*awesome* at sticking to silica, including amorphous silica and
mostly-silica structures like diatomaceous earth, sand, cullet, silica
gel, silica fume, fiberglass, rock wool, maybe some phyllosilicates
like clays and micas, and so on.  I think I got this idea from reading
Kingery’s dissertation, and [also there are some books about it by
now][5], even if WorldCat says the nearest copy to me of Wagh’s book
is in Asunción, Paraguay.  [There’s lots of active research in the
area][15], and [Anabella Mocciaro in La Plata][6] is doing research on
it, like [this paper about using mono*aluminum* phosphate (Al(H₂PO₄)₃)
as a binder for kaolinite clays][7].  (Monoaluminum phosphate forms a
viscous solution with water.)

The fiberglass thing seems like it works; [Katsiki explains][17]:

> More important is that phosphate cements have a neutral pH after
> hardening. This allows the use of low-cost glass fibers as
> reinforcement. The production of Textile Reinforced Cements [sic] is
> thus one of the important future applications.

[0]: https://en.wikipedia.org/wiki/Berlinite
[17]: https://www.tandfonline.com/doi/abs/10.1080/17436753.2019.1572339
[15]: https://downloads.hindawi.com/archive/2013/983731.pdf
[7]: http://sedici.unlp.edu.ar/bitstream/handle/10915/132569/Documento_completo.pdf?sequence=1
[6]: https://www.conicet.gov.ar/new_scp/detalle.php?id=44674&datos_academicos=yes
[2]: https://en.wikipedia.org/wiki/Crystal_structure#Unit_cell
[1]: https://en.wikipedia.org/wiki/Quartz
[5]: https://www.worldcat.org/title/950588735

If you mix silica or some other inert filler in with the reagents,
then, you will dilute the heat thus produced, reducing the maximum
temperature, and spread a given volume of produced gas over a larger
surface area, reducing the stress.  Some morphologies of filler may
also add porosity which allows the gas to escape less destructively,
though of course porous materials are not as strong as fully dense
ones.  (See below about gooey foam, though.)  Finally, if the filler
bonds well to the berlinite, depending on the filler, it may actually
make the composite material stronger than pure berlinite.

(At high enough temperatures, the silica can react with any excess
aluminum present to form mullite, which will greatly improve both the
strength and refractoriness of the ceramic.  Wang, Zhang, wei, Zhou,
and Li found that adding metallic silicon worked even better because
it increased in volume by oxidizing, compensating for the shrinkage of
other parts of their adhesive.)

Of course, you can also use berlinite itself as the inert filler, and
berlinite is guaranteed to be able to bond it.

Carborundum is worth a mention: it is available in fiber form, and
upon exposure to air its surface is passivated with a layer of
(amorphous?) silica.

Although silica is vulnerable to alkali corrosion, it’s totally immune
to ammonia corrosion as far as I know.

Another class of appealing functional fillers are metals such as
aluminum, steel, stainless steel, and bronze, either in granular form,
as chopped fibers, or as continuous fibers (along one, two, three, or
more axes).  If the berlinite binder can be induced to form at
temperatures below their melting points, the resulting solid should
retain the structure of the original filler, and perhaps much of its
strength and resilience.  Copper alloys are somewhat allergic to
ammonia, but ferrous metals should be fine with it, and apparently for
aluminum it actually has a protective effect.

A more extreme version of this design would combine berlinite
precursors, steel, and brass, using the berlinite precursors to hold
the steel filler in place up to the melting point of the brass, which
then brazes the steel together into a solid mass.  This might require
more aggressive ammonia control to keep the brass from being eaten.
Other solder alloys like 63/37 would work in place of brass, with a
lower melting temperature and no ammonia corrosion problem.

### Absorbing the ammonia ###

Instead of just diluting the powder mix with inert fillers, maybe you
could dilute it with a reactive powdered filler which would soak up
the ammonia.  [Sodium bisulfate][8] seems promising, because [ammonium
sulfate][9] and [sodium ammonium sulfate][10] are especially stable
ammonium compounds, the latter found widely in nature as
[lecontite][11], formed from guano deposits, while the former is the
rarer [mascagnite][12] found around some fumaroles.

Ammonium sulfate doesn’t decompose noticeably until 235°, and
lecontite seems to be even stabler, so my inference is that if you can
maintain the *outside* of the curing powder sample between 200° and
235°, ammonia will escape from the DAP and resulting MAP but be taken
up by the bisulfate.

(This is unfortunately far too cold for brazing.)

The stoichiometric reaction should be

> Al(OH)₃ + (NH₄)₂HPO₄ + 2 NaHSO₄ = AlPO₄ + 2 NH₄NaSO₄ + 3 H₂O

with mass proportions:

* 78g Al(OH)₃
* 132g (NH₄)₂HPO₄
* 240g NaHSO₄

The theoretical maximum yield from that mix should be 122g of AlPO₄,
with most of the rest being lecontite, but presumably some of the
aluminum will dally with the sulfate too.  The non-berlinite products
are all highly water-soluble and therefore should be easy to remove.
The lecontite waste should be recyclable into ammonia and sodium
bisulfate simply by heating.

This approach will presumably yield high porosity, since 73% of the
mass of the reagents gets washed away as lecontite and water.  See
below about gooey foam for when this might be desirable;
alternatively, it may be possible to repeat the reaction in the pores.

[8]: https://en.wikipedia.org/wiki/Sodium_bisulfate
[9]: https://en.wikipedia.org/wiki/Ammonium_sulfate
[10]: https://en.wikipedia.org/wiki/Lecontite
[11]: https://www.mindat.org/min-2364.html
[12]: https://en.wikipedia.org/wiki/Mascagnite

### Phase-change cooling ###

If you mixed in a filler that dehydrates or melts within the desired
temperature range, that could help to stabilize the reaction
temperature and prevent thermal runaway.  No obvious candidates come
to mind, though.  [Gypsum dehydrates in the 120–180° range][13] but
would weaken the final product quite a bit; similarly for [the
dihydrate of muriate of lime][14], which is also deliquescent and
would compete with the aluminum for the calcium phosphate.

[13]: https://en.wikipedia.org/wiki/Plaster#Gypsum_plaster
[14]: https://en.wikipedia.org/wiki/Calcium_chloride

### Using partly neutralized reagents to tame the reaction rate ###

As mentioned above, monoaluminum phosphate is often used in preference
to phosphoric acid for making berlinite, in part because it generates
less heat.  Monoammonium and diammonium phosphate can also be seen as
partly neutralized versions of phosphoric acid.  I think some authors
have also reported using aluminum oxide-hydroxides like boehmite in
preference to pure aluminum hydroxide for, I think, a similar reason;
or even less reactive materials like metakaolin.  [Argonne National
Lab’s Ceramicrete reacts magnesium oxide with monopotassium
phosphate][19], producing MgKPO₄, so-called K-struvite or
struvite-(K), and [BASF’s Set 45 rapid-road-repair mortar][20] reacts
dead-burned magnesium oxide with monoammonium phosphate to produce a
58-MPa compressive strength concrete, which Wagh deprecates as
“impractical” because it emits ammonia.  Dental cements often react
phosphoric acid with an amorphous calcium silicoaluminate frit
incorportaing fluorine and a small amount of zinc, achieving very high
strengths.

[19]: https://www.osti.gov/servlets/purl/752864
[20]: https://www.bestmaterials.com/PDF_Files/S45_tdg.pdf

[Wagh explains in his 02013 survey paper][15]:

> To produce CBPCs, the oxide component should be sparsely
> soluble. Often the solubility of the oxide, such as CaO, is too high
> to be exploited in the acid-base synthesis of CBPC formation. Very
> small sizes as that of dental cements may be produced but large
> sizes are virtually impossible. For this reason, it has not been
> possible to produce a calcium-based ceramic in a large size. In
> such cases, one needs to resort to use of a sparsely soluble
> compound of that oxide that may give the slow cation release rate
> needed to form ceramic.

(Kingery’s description of the problem was both more informative and
more entertaining.)

It occurs to me that other soluble aluminum salts can also precipitate
aluminum phosphates, and presumably more rapidly than the insoluble
hydroxide.  The usual water-soluble aluminum salts are the sulfate
(papermaker’s alum), the chloride, the nitrate, and other alums such
as potassium alum and ammonium alum.  All of these will release a lot
less energy reacting with something like phosphoric acid.

Of these the chloride is the most interesting for a couple of reasons:
it’s 20% aluminum by mass, which is better than the others (potassium
alum normally occurs as the dodecahydrate, which is only 5.7%
aluminum) and the other reaction products should be relatively
volatile.  Also it lacks the other risks involved with the nitrate.

> AlCl₃ + (NH₄)₂HPO₄ = AlPO₄ + HCl + 2 NH₄Cl

This should amount to 133g AlCl₃ and 132g (NH₄)₂HPO₄ and give 122g of
AlPO₄.

Sal ammoniac “sublimes” at 337.6°, so heating to a temperature in that
neighborhood or a bit beyond ought to be enough to drive the reaction
forward.  Though I haven’t done the thermodynamic calculations, I’m
pretty sure that at room temperature the equilibrium will strongly
favor going the other way, though, with HCl dissolving AlPO₄ into
phosphoric acid, as it does when AlPO₄ is used as an antacid for
dyspepsia.  Aside from the powder-mix processes discussed above, where
you make a green part and then bake it, local laser heating under
water is another interesting candidate way to provoke the reaction.

The HCl product could be bothersome, but aside from passing the
exhaust gas through a buffer bed to get rid of it, an excess of the
phosphate or any other source of ammonia could probably take care of
it:

> AlCl₃ + 2 (NH₄)2HPO₄ = AlPO₄ + 3 NH₄Cl + NH₄H₂PO₄

### Gooey foam ###

Foams are not ideal materials from a strength-of-materials point of
view; a microscopic trusswork lattice, like that in trabecular bone
(incidentally also a light metal phosphate), gives much more strength
and stiffness for the same mass.  However, foams are enormously easier
to make than trusses, and they do have a lot of virtues compared to
fully dense materials:

- Foam panels, and especially foam-core sandwich panels, give
  enormously more rigidity than the same amount of material deployed
  as a solid sheet.
- Air trapped in the foam often allows it to absorb enormously more
  impact energy than the bulk material would, whether elastically,
  plastically, or by crushing.  This is especially true of closed-cell
  foams but even true of open-cell foams.  This can give foams a much
  higher resilience-to-weight ratio than any solid material.  This is
  especially noticeable in vermiculite and styrofoam.
- As with fibers (and yarns, rovings, and woven and nonwoven fabrics),
  the thin walls of foam bubbles enable stiff materials to simulate
  being much less stiff.  This is a way foams are actually better than
  trusswork lattices.  This is particularly important for refractory
  materials, which usually need to survive rough handling hundreds or
  thousands of degrees below their melting point, where they are
  invariably brittle.  This is also especially noticeable in
  vermiculite and styrofoam.
- Foams tend to be good thermal insulation: low thermal mass and low
  thermal conductivity.
- Foams, especially open-cell foams, tend to have high loss
  coefficients for vibration, so they absorb sounds and damp machine
  vibrations.
- Foam bubbles form barriers to crack propagation, so brittle
  materials (like berlinite!) are often substantially less brittle as
  foams.
- When cutting foams, especially brittle foams, material displaced by
  the cutting process can move into the voids of the foam.  This helps
  a lot with dimensional precision, both because it avoids throwing up
  burrs from cutting, and because the foam tends to have a near-zero
  Poisson ratio.  This is also a great help to cutting.
- Speaking of cutting, foams require much less pressure and hardness
  to cut than a fully dense material, because the foam’s compressive
  strength is much less. You can usually cut a foam with a solid blade
  of the same material.
- Sometimes, for example for catalyst support, you want a material
  with a high surface-to-volume ratio.  Open-cell foams are your
  friend.
- Sometimes you want capillary action, like in a heat pipe or when
  washing dishes, and again, open-cell foams are your friend.
- Infiltrating a foam with a molten material can produce a different
  composite, for example a ceramic-reinforced metal-matrix composite
  via liquid metal infiltration.
- Finally, there are many circumstances where virtually any material
  that occupies enough space will have the required strength and
  stiffness, though new materials like aerographene are likely to
  change this.

Given the numerous advantages of foams for manufacturing, perhaps by
default we should make most things from foams, only using fully dense
materials for surface coatings, tensile members, electrical
conductors, and similar special cases.

So an interesting question is how we could form a berlinite foam.
[Magdalena Gualtieri has done this with phosphoric acid, metakaolin,
detergent, limestone, and a hexametaphosphate deflocculant][16],
getting a 70%-porous berlinite/quartz solid solution; the
room-temperature acid was apparently enough to liberate the CO₂ from
the chalk, which was only 4% of the quantity of the metakaolin.

[16]: https://iris.unimore.it/bitstream/11380/1085888/4/Paper%2Btabelle%2Bfigure.pdf

Lots of the above candidate reactions have the problem that they
produce gaseous byproducts, but if you want to make a foam, the
problem then becomes how to retain that gas in bubbles instead of
getting rid of it.  Gualtieri’s aqueous approach of using a surfactant
and a deflocculant seems pretty reasonable, but it might also be
worthwhile to add things to the solution to thicken and stabilize the
foam before the precipitate forms, so bubbles can’t coalesce or
escape; for example, glycerin, or dipropylene glycol, food thickeners
like carboxymethylcellulose or poly(ethylene glycol), or other
hydrogel formers.  [Polyethylene oxide/PEG is well known for its use
in making huge, long-lived bubbles][18], as are glycerin and
surfactants, but note that PEG is also used in foods as an
*anti*-foaming agent (E1521).

[18]: https://en.wikipedia.org/wiki/Polyethylene_glycol#Entertainment_uses

Hydrogen peroxide is sometimes used to foam bioceramics.

Another approach to getting gas into the mix is to just [mix it in
mechanically][23], for example with an electric mixer, probably after
adding stuff to stabilize the foam.  It might be wise to do this
foaming step before adding one of the reagents in case forming the
foam takes longer than the precipitation reaction.  In the case
described above of anisotropic reinforcement, you probably need to
infuse such mechanically aerated foam into the reinforcement matrix
after it’s been foamed.

[23]: https://en.wikipedia.org/wiki/Leavening_agent#Mechanical_leavening

If the foaming process produces annoying gases like NH₃ or HCl, they
will eventually diffuse out of the foam and be replaced by air, but in
a closed-cell foam this will probably take much longer than the
analogous process with things like polystyrene foam.  If the
closed-cell foam is made hot, cooling it down will tend to contract
the gases, in the case of NH₄Cl producing a fairly strong vacuum which
will put the foam under compressive stress from air pressure.  This
will tend to deform it, but may also strengthen it until air succeeds
in diffusing back into the foam.

Another interesting possibility is to react phosphoric acid directly
with powdered aluminum or magnesium to produce hydrogen gas, as acids
on metals generally do; the first step of the reaction is

> 2Al + 6H₃PO₄ = 2Al(H₂PO₄)₃ + 3H₂

and the fully stoichiometric reaction is

> 2Al + 6H₃PO₄ = 2AlPO₄ + 9H₂

thus producing 4½ moles of hydrogen gas per mole of berlinite gel.  At
1 atmosphere and 150° we should get 34.7 liters per mole of gas and
thus 156 liters per mole of berlinite, which weighs 122 g/mol, so we
get about 1.3 liters of gas per gram of material.  This is far too
much to form a closed-cell foam, because it would amount to 99.97%
porosity.  So in practice you would want to use only a small amount of
the reduced metallic element to get the desired amount of hydrogen,
maybe 1% or 0.1% metal powder to 99% or 99.9% other sources of the
metal such as the hydroxide.

### Gooey foam with fillers ###

In particular, combining berlinite foam with unidirectional continuous
glass fiber reinforcement should produce a material with properties
somewhat like wood, but much stronger, combining relatively easy
cuttability with great strength and stiffness along one axis.  If
instead of unidirectional fiber reinforcement we use layers of glass
fiber cloth or mats, we should be able to get a material more like
plywood.  Such anisotropic composites can be more useful than
isotropic ones.

If we estimate berlinite’s tensile strength as being the same as
E-glass’s, about 3500 MPa, and cellulose as being 600 MPa, we might
estimate that such a composite should be about six times as strong as
wood, though wood has a trabecular nanostructure like bone which
presumably strengthens it further than a random foam.  At the same
porosity, it should also be about 2.5 times denser, because both
berlinite and glass fiber weigh 2.5g/cc.  So in the end the ceramic
composite will probably be about as strong as wood of the same
density, just enormously more refractory.

Another possible way to make foam foamier and also maybe cheaper is to
incorporate foamy fillers such as exfoliated vermiculite, LECA,
perlite, or pumice, as [Karua and Arifuzzaman did with perlite
cemented with silica gel and boric acid][28].

[28]: https://www.metall-mater-eng.com/index.php/home/article/download/755/436 "Compressive Behavior of Perlite/Sodium Silicate
Composite Foam Modified by Boric Acid, Karua and Arifuzzaman, 02021, doi 10.30544/755"

An alternative approach to increasing porosity is by removing grains
of filler material from the composite after the berlinite has fully
set; these grains can be extremely small, down to submicron size,
potentially yielding a very fine foam, similar to aerogels.  Such
candidate sacrificial fillers might include:

- carbon or organic material (CHON) to be combusted to gas, which
  [Chung explains was a method in common use in 02003][24], using
  carbon, and which is commonly used with starch granules to make
  porous bioceramics;
- sulfur, which boils at 444.6°, though it tends to oxidize any
  available metals;
- hydroxides of calcium, aluminum, or magnesium, which readily react
  with mild acids to form soluble salts which can diffuse out of the
  foam;
- quicklime or magnesia, similarly, or even some forms of alumina;
- ammonium sulfate, which decomposes at 235° as mentioned above,
  though its water-soluble nature may pose problems;
- hygroscopic salts which can be induced to release their water by
  gentle heating;
- formates, carbonates, and oxalates, which usually decompose to
  release gas at relatively modest temperatures.  [Aluminum
  carbonate][21] doesn’t exist, but [magnesium carbonate][22] does,
  and forms a pentahydrate which dehydrates below 165° and decomposes
  to magnesia at 350°.  Formates generally decompose around 200°
  (though magnesium’s holds out until 500°) and have the advantage in
  this context that aluminum formate is insoluble.  Oxalates start to
  decompose around 300°; those of polyvalent cations are insoluble,
  like carbonates.  Like hydroxides, carbonates can also be decomposed
  with mild acids rather than heating.
- Sal ammoniac.

[21]: https://en.wikipedia.org/wiki/Aluminium_carbonate
[22]: https://en.wikipedia.org/wiki/Magnesium_carbonate
[24]: https://www.acsu.buffalo.edu/~ddlchung/Acid%20aluminum%20phosphate%20for%20the%20binding%20and%20coating%20of%20materials.pdf

Fine pore size tends to increase mechanical strength.

Platy or acicular sacrificial fillers could produce anisotropic
porosity if properly oriented.  In particular, they can be positioned
in the same way as the reinforcing fillers mentioned earlier, by using
continuous fibers; or they can be suspended in a hydrogel which is
then dehydrated anisotropically to reorient filler particles, like
glitter in drying glitter-glue.  This could provide a better
approximation to the trabecular structure of wood.  In cases of later
liquid infiltration with a new reinforcer such as metal, the pore
morphology then becomes the reinforcer morphology.

Even without anisotropic orientation, pores left by platy or acicular
fillers would produce much higher permeability, which is desirable for
some applications and undesirable for others.

If the final material has a significant quantity of silicon in it and
a sacrificial filler contains carbon, it may be possible to produce
silicon carbide reinforcement by heating.

Other foaming methods that have been used for phosphate bioceramics
include Schwartzwalder and Somers’s “sponge replica method”, where you
coat an organic sponge with a slurry, squeeze it out, and then burn it; 

### Putty ###

Most of the above is about powder mixes or aqueous solutions; the
powder-mix stuff relies on the powder staying dry to keep it from
reacting.  But powder is sort of inconvenient: it jams, it clumps, it
avalanches, you can’t shape it into shapes with overhangs, you can’t
injection-mold it, and so on.

Above I’ve talked about mixing the berlinite-polymer precursors with
“functional fillers” like glass fiber or mica to get a powder mix that
cures to something with desired properties.  But what if we use this
entire powder mix as a “filler” in a non-aqueous carrier that doesn’t
solvate ions?  We can get a putty or paint or injection-moldable resin
which we can put into the desired shape before we initiate the
polymerization reaction to make the berlinite.

Candidate carriers include petroleum jelly, lard, peanut butter,
polyethylene, polypropylene, pine pitch, asphalt, birch tar, beeswax,
silicone (with or without boric acid), unvulcanized rubber,
d-limonene, carnauba wax, polylactide, xylene, ghee, lanolin, toluene,
cocoa butter, paraffin wax, soap, hexane, shellac, chicle, mixes of
the foregoing, and so on.  Adding lighter oils commonly increases
plasticity; adding stearin and things like that commonly decreases it.

(I suspect that glycols like glycerin and propylene glycol would be
too good at solvating ions, though see below about insoluble
phosphates.)

Soft, gummy carriers like petroleum jelly permit forming the soft mix
into the desired shape like clay.  Carriers like polypropylene and
paraffin wax with relatively narrow melting ranges permit casting the
mix in a mold, perhaps just a block, and then possibly cutting the
casting.

All the carriers suggested above will melt at higher temperatures,
which leads to the question of how to prevent them from doing that
when you want to make the berlinite precursors react.  Possibly the
answer is to activate the berlinite system with water rather than
heat; alternatively you could cross-link the carrier with, for
example, electron beams, ultraviolet, chromate, or glutaraldehyde.

### Insoluble phosphate sources for granular dispersion in aqueous solutions ###

Most phosphates containing polyvalent cations are pretty
water-insoluble, even those without much covalent character; struvite
(NH₄MgPO₄·6H₂O) and the higher calcium phosphates come to mind.  I
suspect that if fine powders of these phosphates, even those not
containing ammonia, are sintered with fine powders of aluminum
hydroxide or oxide, the phosphate will be captured by the aluminum by
diffusion through the solid.  This is because, as I understand it, the
aluminum phosphate bond is almost purely covalent with little ionic
character.  Oxides such as those of calcium or magnesium would result
as a side product; if they are not desired they can be removed with
mild acids that do not attack the berlinite.

This suggests many additional possibilities: suspending these
particles in hydrogels to form putties or paints, for example, or
impregnating them into textiles.  If the particles can be
deflocculated into a colloid, the colloid can in many cases be made to
flow easily like liquid.  Thickeners like carboxymethylcellulose can
add thixotropic behavior.

### Diffusion-limited precipitation at interfaces in laminar flow ###

Suppose we take a different approach to the problem of too-fast
precipitation described above.  Instead of using “sparsely soluble”
oxides or not-very-energetically-favorable reactions to keep the
reaction under control, we could have a very-soluble, very-favorable
reaction that proceeds slowly because it’s happening at the interface
between two solutions, so it’s limited by diffusion.  This probably
requires the solutions to be viscous or even gels.

So suppose we are extruding two such gels from adjacent spinnerets,
like two tubes of toothpaste.  Where the two gels make contact, the
reaction proceeds rapidly, forming a diffusion barrier that limits the
reaction speed.  If we then take hold of the double-barreled
toothpaste and pull it out longer, it will get thinner, creating more
interface area.  If the gel is somewhat strain-hardening, we should be
be able to pull it out into a pretty thin thread without breaking it,
until the thread thickness is comparable to the diffusion distance in
the timespan that the thread has to react.  Pulling the thread through
a furnace should accelerate the reaction further and reduce the gel to
a xerogel, giving us a fine fiber of berlinite (or other reaction
product under other circumstances).

With two slabs of gel, the same approach should work to give us a thin
foil of the reaction product.

### Micron-scale precipitation ###

Most experiments done so far with phosphate precipitation have been
done at scales of grams to tonnes, centimeters to meters.  At these
scales, many phosphate reactions that could theoretically produce
strong, stable products produce so much heat that they move the
reagents apart.  Zinc phosphate dental cement is on the borderline; it
can only prepared on the scale of a gram at a time, with a
characteristic length of a centimeter, and even that involves a lot of
mixing it around in thin layers to keep it from overheating.

The volume inside a cube producing heat increases as the cube of its
side length; its surface area increases only as its square, and the
distance to the surface also increases linearly with the side length,
so the thermal resistance in W/° is inversely proportional to the side
length.  Crudely we should expect this to mean that the temperature
rise produced by a reaction increases as the square of the side
length: if it heats up 10° at the scale of 10 cm, it should heat up by
1000° at the scale of 1 m, all else being equal.  It’s rare that all
else is equal when you heat things up by 1000°, though.

Consider, then, the possibility of dispensing reagents under a
microscope using a micron-wide needle drawn from a glass tube.  A
reaction which would produce 100° of heating at the centimeter scale
should be expected to produce a hundred million times less: 0.000001°.
Consequently we should expect that many reactions that are far too
exciting to be practical at the centimeter scale will work beautifully
at the micron scale.  Indeed, we should go looking for precipitation
reactions which will happen as quickly as possible.

In the four orders of magnitude between the centimeter scale and the
micron scale, even the millimeter scale should permit the use of much
more rapid reactions.  A reaction which would produce 100° of heating
at the centimeter scale should produce 1° of heating at the millimeter
scale.  Millimeter-sized drops of liquid, perhaps ejected from a
syringe needle or inkjet printhead, or bits of powder are possible
candidates for such a scale.

A 600-dpi inkjet printer routinely deposits drops of ink in 42-μm
pixels.  At this scale, a reaction that would produce 1000° of heating
at the centimeter scale will instead produce 0.02° of heating.  So
plausibly even extremely exciting reactions like the oxidation of
aluminum or magnesium could be viable.

### Magnesium silicate for cations for translucent magnesium silicophosphate cement ###

Amorphous magnesium silicates of various molar ratios and fine
particle size [can be easily made][33] by mixing [waterglass][32] with
soluble magnesium salts, a process which typically takes on the order
of a second, optionally followed by washing to remove alkali salts,
screening, milling, and drying to get more precise granulometry.
Calcium silicates like wollastonite are commonly used as calcium
sources for preparing phosphate-bonded ceramics.  Analogously, the
magnesium in these amorphous magnesium silicates might have solubility
in the right range for macroscopic phosphate cement formation over
convenient time intervals: minutes to months, as opposed to
milliseconds or millennia.

[32]: https://en.wikipedia.org/wiki/Sodium_silicate
[33]: https://www.researchgate.net/publication/256679052_Synthesis_and_Characterisation_of_Magnesium_Silicate_Hydrate_Gels "They mixed 1:1 Na/Si waterglass with magnesium nitrate, getting almost perfectly amorphous gels with Mg/Si ratios from 2:3 to 1:1, in 02005, Cement and Concrete Research 35(1):85–98, by Brew and Glasser, 10.1016/j.cemconres.2004.06.022"

If so, the resulting cement ought to be hard, nontoxic, relatively
translucent (because it should be relatively amorphous, and the
refractive indices of the magnesium phosphate and magnesium silicate
phases are similar), and refractory.  Perhaps it could substitute for
transparent organic polymers in some uses, though of course it will be
far more brittle.

Low-temperature heat-cured waterglass aluminum cement
-----------------------------------------------------

Waterglass is a hell of a lot cheaper than phosphate, and it’s nice
and viscous already.

Waterglass precipitates silica gel in a few seconds if provided with
either polyvalent cations or mild acid such as CO₂, or more slowly if
allowed to dry.  The gel incorporates a great deal of water and
continues to soften at relatively low temperatures, allowing it to be
foamed fairly easily by simply heating, a process which has also been
emulated with some success by diffusing water into regular soda-lime
glass under heat and pressure.

(Potassic waterglass, if not crosslinked with polyvalent cations, can
be easily dissolved again with water, but sodic waterglass is fairly
resistant to this, requiring temperatures above 100° and thus pressure
to dissolve at a reasonable speed.)

I think what happens with the CO₂ is that it forms NaHCO₃ or similar
with the alkali ion, liberating a hydronium to replace the alkali ion
in neutralizing the negatively-charged oxygen terminating the siloxane
chain.

Unlike the phosphate reactions, cross-linking waterglass with
polyvalent cations is not noticeably exothermic.

I have heard that if the dried waterglass is in contact with silica,
it gradually loses its low softening temperature, but I have not tried
this.

Davidovits’s geopolymer stuff mostly involves mixing metakaolin with
waterglass so that, as I understand it, the trivalent aluminum from
the kaolin will gradually precipitate the waterglass, forming a
covalently bonded amorphous silicoaluminate network solid.

If you use just regular kaolin, this reaction will not happen,
although the waterglass will still stick the kaolin together into a
solid composite, and potters do regularly use waterglass as an
adhesive for repair of leather-hard clay.  I guess the hydroxyl groups
in the kaolin somehow protect the aluminum from reacting with the
waterglass.

But I suspect that if you bake objects made from this
kaolin–waterglass composite at the metakaolinization temperature
(550°–850°) you will form the same geopolymer ceramic as Davidovits.
I suspect that other clays will also work, perhaps at a higher
temperature.  This is a lower temperature than that needed for things
like terracotta.

It would be nice to have other easily controllable ways to supply
aluminum or other polyvalent cations to cross-link waterglass, too.  A
few that come to mind:

- Aluminum hydroxide, again.
- Sodium aluminate, in which the aluminum is confined within AlO₄
  oxoanions.  [Zhao et al. seem to report][25] that mixing sodium
  aluminate solution with waterglass does not produce a precipitate
  until aluminum sulfate was added.  I believe that this substance is
  only stable at fairly high pH, so mild acidification (for example,
  gassing with CO₂) should make the aluminum available for reaction
  with the waterglass.
- Aluminum formate or oxalate, which decompose and outgas on gentle
  heating as noted above.
- Magnesium carbonate, likewise, which would also decompose and form
  gas upon acidification.
- Natural iron oxides such as hematite and magnetite, or wüstite,
  which Wagh explains can be easily prepared by partly oxidizing
  native iron with hematite.
- Boric acid.  At room temperature it doesn’t have any immediate
  visible effect on sodium silicate, though it ought to buffer its pH
  down into the range where it gels, which [Obut and Girgin report
  does happen but usually takes several minutes][27].  Using borax
  instead might avoid this.  Interestingly, they also report that
  adding too *much* boric acid also prevented gelation if it dropped
  the pH below 8.
- Dead-burned magnesia.

[25]: https://pubs.rsc.org/en/content/articlehtml/2021/ra/d1ra06343j
[27]: https://link.springer.com/article/10.1007/s10971-006-8974-6

Most of these are insoluble in water but I think can be persuaded to
react with waterglass by raising the temperature or lowering the pH.
In some cases you might not even have to raise the temperature much;
Obut reports a slow borate reaction at room temperature.  The
sodium-aluminate and boric-acid alternatives stand out as being
entirely water-soluble and therefore possibly capable of producing a
fully amorphous glass product.

There are a variety of other oxometallates similar to aluminate which
might work in the same way, but [titanate][30] and [manganate][29] are
the only terrestrially abundant ones.  [Ferrate][31] exists but is
only stable at high pH in the dark.

[29]: https://en.wikipedia.org/wiki/Manganate
[30]: https://en.wikipedia.org/wiki/Titanate
[31]: https://en.wikipedia.org/wiki/Ferrate(VI)

Boric acid seems likely to enhance the tendency of waterglass to
soften with gentle heating, while the other ions seem likely to reduce
it, making the product more refractory.

Extremely alkaline waterglasses have a lower ratio of silicon to
sodium, and thus shorter siloxane chains, and ultimately a lower ratio
of silicon to the precipitating polyvalent cation.  Precipitating
these with Mg, Al, or Fe, washing out the alkali, and then baking out
the water should tend to produce high-strength nesosilicate minerals
such as Fe₂SiO₄ (fayalite olivine, Mohs 6.5–7.0), Mg₂SiO₄ (forsterite
olivine, Mohs 7), Al₂SiO₃OH (topaz, Mohs 8), Al₆Si₂O₁₃ (mullite), or
Al₂SiO₅ (kyanite, andalusite, or sillimanite, Mohs 4.5–7.5).  Slightly
higher silicon ratios might paradoxically tend to produce soft
phyllosilicate minerals like kaolinite/lithomarge (Al₂Si₂O₅(OH)₄, Mohs
2–2.5) and talc (Al₃Si₄O₁₀(OH)₂, Mohs 1), though enstatite/ferrosilite
(Fe/MgSiO₃, Mohs 5–6) is also a possibility.  Soft micas like
muscovite (KAl₂(AlSi₃O₁₀)(OH)₂ if no fluoride) and phlogopite
(KMg₃AlSi₃O₁₀(OH)₂ if no fluoride) are another phyllosilicate
possibility.

However, from looking at the [magnesia/silica/alumina phase
diagram][26] I feel like even larger amounts of silica will tend to
produce a stable tectosilicate mineral like cordierite, mullite, or
quartz, rather than phyllosilicates.  (Of these three, quartz is the
strongest, but the worst refractory, melting at a lower temperature
and also undergoing destructive “dunting” changes in crystal structure
with temperature, though amorphous fused silica is much better.
Impurities will tend to frustrate crystallization, which would be
desirable.)

[26]: https://www.researchgate.net/publication/314489924_The_Utilization_of_Waste_Magnesite_in_the_Production_of_the_Cordierite_Ceramic

Such desirable tectosilicate outcomes might require a low alkali
content, because sodium or potassium will push the outcome toward
phyllosilicates or even inosilicates (though synthesizing jadeite
would totally fucking rock).  I think that once an initial insoluble
aluminosilicate phase is formed, you can use acid to wash out the
alkali metals.
