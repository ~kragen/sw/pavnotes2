Normally an electrical transformer works by running electrical current
through coils that snake many times through a ferro- or ferrimagnetic
core, thereby getting a potentially large ratio of voltages between
the coils, and also a potentially large inverse ratio of currents.
Turns ratios might be as high as 20:1 in ordinary cases and over 100:1
in exceptional cases.

But normally the magnetic flux only makes a single round trip through
the large number of electrical coils.

In theory, you could do this the other way around: make a magnetic
coil that circles through one single-turn electrical coil many times
before returning to where it began, while only transiting the other
single-turn electrical coil once.  Performance would not be as good as
with the electrical turns ratio, because the relative permeability
*μᵣ* of suitable magnetic core materials is only up to about 4000 for
[ordinary electrical steel][2], 100'000 for [mu-metal][0] or
[permalloy][4], 800'000 for [supermalloy][1], and a million for
[annealed metglas 2714A.][3] At megahertz frequencies, none of these
are suitable (though metglas goes up to 100kHz, the others are only
good to a few hundred hertz), and only ferrites with relative
permeabilities of a few hundred up to a few thousand are available.

As I understand it, this means that if the flux path through an
electrical-steel coil (*μᵣ* ≈ 4000) is 1000 times longer than the path
outside it, you should expect about a quarter of the flux to
“short-circuit” or “shunt” without passing through the intended
many-turns path.  This might happen if you had 32 turns of a coil with
adjacent turns separated by 1 mm of low-permeability “magnetic
insulation”, and the total circumference of each turn was 32 mm (a
5-mm-diameter circle through which the electrical wire passes, for
example).

[0]: https://en.wikipedia.org/wiki/Mu-metal
[1]: https://en.wikipedia.org/wiki/Supermalloy
[2]: https://en.wikipedia.org/wiki/Electrical_steel
[3]: https://en.wikipedia.org/wiki/Permeability_(electromagnetism)#Values_for_some_common_materials
[4]: https://en.wikipedia.org/wiki/Permalloy

This doesn’t happen on the electrical side of the transformer because
common insulators have resistivities in the range of 10¹³–10²⁵ Ω·m,
while copper’s resistivity is 1.68×10⁻⁸ Ω·m, so instead of three to
five orders of magnitude between the materials involved, you have 21
to 33 orders of magnitude.  This staggering gap is why transatlantic
telegraph cables can enable batteries on one continent to successfully
actuate solenoids on another.

Still, it seems like you ought to be able to get at least a few turns
out of this approach; instead of getting a 20:1 ratio with 10 turns on
one electrical winding and 200 turns on the other, you could use 10
turns on one winding and 40 turns on the other, while using a 5:1
ratio of magnetic-core turns.  Plausibly you might be able to make
this smaller or at least lighter than the corresponding conventional
transformer, but maybe not.

The geometry might look like a conventional toroidal transformer in
inverse, with a tightly packed toroidal bundle of copper wires wrapped
with a helical magnetic tape; this allows the “magnetic insulation” to
be mostly provided by large air gaps while the much more compact
electrical insulation is within the coil.

A place where this is especially appealing is where the magnetic field
inducing the current isn’t itself produced by an electric current,
but, for example, by permanent magnets, or by the magnetic field of
the earth.  An energy-harvesting button that disconnects a small
permanent bar magnet from such a magnetic coil could plausibly use a
smaller amount of copper than conventional energy-harvesting
electromagnetic buttons.
