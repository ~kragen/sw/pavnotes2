Teletype interaction (or its simulation on a video display terminal)
benefits from some kind of prompt.  For half-duplex conversational
interfaces, particularly with local echo on your terminal, generally a
new request will not be answered until the previous one has been
finished, which can take a while.  (This permits keyahead.)  So it’s
useful to know when your previous input has been processed, the
previous output has finished, and the system is prepared to handle
more input.

GUIs generally do not have this problem because they can provide new
output at any time without interfering with your input.

Most ASCII characters, and quite a few that aren’t, have been used as
prompts in one system or another.  On older systems these are
rigorously kept short, because each additional character was 33ms or
90ms of annoying delay while the prompt was printed, literally
printed, on a sheet of paper.

### The empty string ###

This is notoriously the prompt for ed, the standard text editor.

### TAB ###

APL interpreters typically use a tab for their prompt, so input lines
start 8 spaces to the right of output lines.

### \# ###

    #

The Bourne shell uses ‘# ’ for its root prompt.

### $ ###

    $

The Bourne shell uses ‘$ ’ as its non-root prompt; perhaps it is
coincidental that ‘$’ is the ASCII character after ‘#’.

### % ###

    %

csh defaults to ‘% ’.  Perhaps it is coincidental that ‘%’ is the
ASCII character after ‘$’.

### \* ###

    *

SBCL uses ‘* ’ as its prompt.  So did Heathkit’s Benton Harbor BASIC,
an inferior alternative to the Microsoft BASIC also available for the
H8 and H89 kit computers.

### . ###

    ...

Python uses ‘... ’ to prompt for continuation lines for multiline
statements.

### : ###

    Eval:
    Find file:

GNU Emacs has a variety of prompts that occur in the minibuffer;
almost all of them end with ‘: ’.

    bwBASIC:
    
This is the Bywater BASIC prompt.

### > ###

This is one of the most widely-used prompt characters.

    > 

tcsh defaults to ‘> ’.  This is not ideal on a Unix system that
supports copy and paste in the terminal shell window; accidentally
pasting a line like ‘> ls’ will create an empty file called ‘ls’ in
the current directory, destroying any file previously called that.

    >

The Apple ][ came with an [Integer BASIC in ROM][2] which used ‘>’, without
a trailing space, as its prompt.

[2]: https://youtu.be/GD0pbyVRMuE

    >>>
    
The Python prompt (for non-continuation lines) defaults to ‘>>> ’;
browser consoles now ape this.  The current Firefox inspector uses a
prompt that looks sort of like ‘>>’ but is not in fact made of
characters.

    C>

MS-DOS and CP/M default to the current drive letter followed by ‘>’.
But invariably MS-DOS users would set their prompt to $P$G in
AUTOEXEC.BAT, so it would instead look like this, telling you what
directory you were in:

    C:\>

    gap>

The GAP system uses this as its prompt.

    irb(main):001:0> 

The interactive Ruby interpreter `irb` includes the current package

### ? ###

    ?

PARI/GP uses ‘? ’ as its prompt.  This is also the default prompt in
BASIC for an INPUT statement (in, say, the package Debian calls
python3-pcbasic).  Consequently this prompt showed up a lot in games
written in BASIC.

    foreach?

tcsh prompts for continuation lines for multiline statements such as
foreach this way.

### Alphabetic characters ###

    Ok

This (on a line by itself) was the Microsoft BASIC-80 and GW-BASIC
prompt.  Versions for machines without lowercase instead said “OK”..
I don’t know if this was copied from some version of Dartmouth BASIC,
but I suspect not.

    READY.

This was a [Dartmouth BASIC prompt early on][0], including [in 01964
when the first manual was written][1]; sometimes the “.” was omitted.
Some microcomputer BASICs copied this:

    Ready

This is [the Amstrad 464’s][4] prompt, but most home computers of the
time licensed Microsoft BASIC and thus got its prompt.

[0]: https://youtu.be/WYPNjSoDrqw "Birth of BASIC; see timestamp 28'10"
[4]: https://youtu.be/DykFhtNbgGk "Just CPC4ATX - My first Micro Computer has been Cloned! by Tech Nibble; see timestamp 8'5"
[1]: https://www.dartmouth.edu/basicfifty/basicmanual_1964.pdf

     ok

Forth systems don’t traditionally emit a prompt at startup, but they
do traditionally display ‘ ok’ after each successful evaluation.  They
don’t echo a newline after each line of input, so typically you end up
with an ‘ok’ at the end of every line.

    Gforth 0.7.3, Copyright (C) 1995-2008 Free Software Foundation, Inc.
    Gforth comes with ABSOLUTELY NO WARRANTY; for details type `license'
    Type `bye' to exit
    3 4 + . 7  ok
    variable x x ? 0  ok
    1 x +!  ok
    x ? 1  ok
    3 x +! x ? 4  ok

In the above, the text I typed was `3 4 + .`, `variable x x ?`, `1 x
+!`, `x ?` and `3 x +! x ?`.  The rest was prompts and output from the
Forth interpreter.

I don’t think this was copied from BASIC; Forth already had its
traditions before Microsoft BASIC, and I haven’t found an earlier
version that used “OK” as a prompt.

    M-x

This is Emacs’s command prompt, appearing when you type Alt+X.

### ] ###

    ]

This was used by the Apple ][ [and its descendants for everything that
wasn’t Integer BASIC][3], including Applesoft BASIC and Beagle
Brothers BASIC, replacing the “OK” prompt Microsoft BASIC normally
used.  You would type your lines of BASIC and your RUN command at the
‘]’ prompt.

[3]: https://youtu.be/PHfKCxjsmos "Coding Challenge: 3D on Apple II"

