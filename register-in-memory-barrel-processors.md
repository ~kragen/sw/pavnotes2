In general you can increase the bandwidth to memory arbitrarily high
by splitting it into more banks, but latency is more difficult.  The
Tera MTA used a “barrel processor” with 128 register sets on-die,
switching to a new thread every clock cycle, with the ability to issue
one write and two reads every cycle.  The memory was connected to the
CPU through a packet-switching network, so dozens of memory operations
could be in flight at once; each reference took 150–170 cycles to
complete.

A sort of unfavorable aspect of this approach is that the register
files are pretty large, physically.  A minimal CPU like the MuP21 is
about 7000 transistors, including a few 21-bit operand and return
registers (maybe 14, I forget).  I forget the size of the MTA’s
register file; let’s say it’s 16 64-bit registers.  A single register
file of 16 64-bit registers made of 6T cells is 6144 transistors, and
128 of them is 786_432 transistors.  (In practice you usually need
more ports on your register file, increasing the number of transistors
per bit, but perhaps in this case those could be pipelined).  It seems
unappealing to attempt to increase the utilization of a
5000-transistor ALU at the cost of increasing your register file to
100 times that size.  You’d need a pretty humongous execution unit for
that to be a win instead of a huge loss.

The idea behind barrel processors is that, with enough threads, the
memory latency is effectively zero.  But doesn’t that mean we can
basically dispense with register files altogether?  Some early
computers (and Chifir) are “memory-to-memory architectures”: each
instruction specifies the memory address of each operand, and there
are no architecturally visible registers at all.  A memory-to-memory
barrel processor might in theory need only a program counter register
and a context pointer register for each thread, though for things like
DGEMM it would be desirable to also have an accumulator and two or
three index registers.

This is starting to look hairy.  Ideally you’d like to be able to run
an instruction mix of mostly multiply-accumulate instructions, each
occupying the multiplier for a single cycle and updating an
accumulator, while the index registers get incremented in the
background. But the thing is that you have two incoming memory
operands here, which may arrive at different times, so you need some
way to store whichever arrives first, and possibly both; moreover,
they definitely will not arrive in the same cycle that this
instruction is initially issued.  You could get around this last
difficulty with a VLIWish approach where the instruction word contains
a multiply-accumulate and also two autoincrement fetches, but you
still need a couple of temporary registers to hold the fetched
operands until the next instruction can run.  So each thread context
needs at least registers pc, si, di, t0, t1, ac, and sp, 224 bits in
all if they’re 32 bits. (If you want more local variables you can
index off the stack pointer.)

Alternatively you could try to dispatch the fetches and the multiply
separately to different functional units; this design sketch is
getting awfully close to a conventional superscalar processor like the
CDC 6600, though.

A maybe more interesting variation would be to run a lot of small
MuP21-like processors with small local caches and a packet-switched
connection to a much larger, heavily multibanked memory.  If you need
1344 transistors just for the registers of a thread (224 6T bits) then
why not go ahead and add the other few thousand transistors necessary
to allow the thread to run instructions without stopping, at least
when it’s not blocked on some external resource?  Like, maybe
sometimes your thread wants to send a request off to a big multiplier
or AES engine or FPU or RAM or texture mapper or something, but the
rest of the time it could be executing instructions nonstop.
