As described in file `graphite-shift-gas.md` you can generate water
gas very rapidly from a pebble bed of hot graphite; as described in
file `heat-exchanger-intensification.md` you can also use even a small
pebble bed or similar regenerator to transfer heat from one fluid
stream to another, for example from the combustion gases of a fire to
relatively clean air.

Suppose you have a fairly ambitious but not implausible kiln for a
household: one meter cubed on the outside, 0.1 W/m/K insulating
refractory (see file `vermiculite-kiln.md` for some thoughts on how to
do that), 250 mm thick, inside temperature 1400°.  How much power does
that require to keep it hot?

Without doing the rather hairy math around the corners of the kiln, we
can approximate the outer walls as being a 500-mm-radius sphere that
has 250 times the thermal resistance it would have if it were 1 mm
thick.  This works out to *π* square meters and 1800 watts.  This is
an annoyingly large amount of power to get from the electrical grid,
so it would be nice to get it from charcoal, and control the speed of
the charcoal burning by controlling its airflow.

(Why not do the hairy math?  Because I’m interested in enough detail
to assess feasibility; I’ll be happy if these calculations are within
a factor of 2.)

Let’s suppose we can burn the charcoal completely, which I think can
be achieved reliably by adding hot air to the exhaust gas to remove
any stray carbon monoxide:

> C(s) + O₂(g) = CO₂(g)

The heat of formation of [carbon dioxide][0] is -393.5 kJ/mol, and the
[atomic weight of carbon][1] is 12.01 g/mol, so this gives us
32.76 kJ/g, in reasonable agreement with the [32.76 MJ/kg listed for
graphite][2], but detectably higher than the 26–33 MJ/kg listed for
anthracite.  Charcoal should be about the same.

[0]: https://en.wikipedia.org/wiki/Carbon_dioxide
[1]: https://en.wikipedia.org/wiki/Carbon
[2]: https://en.wikipedia.org/wiki/Energy_density#In_chemical_reactions_(oxidation)

Accordingly, 1800 watts is about 55 milligrams of charcoal per second
or 200 grams per hour.  [Air][4] is by volume about 20.95% [O₂, which
weighs 31.998 g/mol][3], so those 55 mg/s of charcoal correspond to
147 mg/s of oxygen, which is 4.58 millimoles per second.  A mole at
20° and 1 atm is 24.06 ℓ, so that would be 110 mℓ/s of pure oxygen, or
530 mℓ/s of air.  This works out to about 1.1 “cfm”, cubic feet per
minute, in the medieval units commonly used to rate fans.  Averaged
across a 25-mm-diameter pipe that would be 1.1 meters per second, so
this heater is quite a small apparatus next to the size of the overall
unit.

[3]: https://en.wikipedia.org/wiki/Oxygen
[4]: https://en.wikipedia.org/wiki/Atmosphere_of_Earth

By using fans, and possibly valves, under microcontroller control, we
can direct as much or as little heat to the kiln as desired.  By using
regenerators or other heat exchangers, we can separate the heat from
the reducing atmosphere produced by the charcoal; by injecting steam
into the charcoal-burning air we can obtain a more strongly reducing
atmosphere.  Regenerators can also recover the heat from air displaced
from the kiln interior by new air being pumped in.

It’s probably important to make the heat-pumping action intermittent;
a constant flow of 500mℓ/s of air will lose a lot more heat in the air
pipes than a flow of 4ℓ/s for one second every 8 seconds.

According to file `pumping-heat.md` charcoal costs about 30¢/kg, so a
24-hour firing in such a kiln might cost on the order of US$1.40 of
fuel.  30¢/kg at 30 MJ/kg is 1¢/MJ or 3.6¢/kWh.  This is a reasonable
price in the world energy markets, but fairly unfavorable here in
Argentina due to heavy subsidization of energy consumption in the form
of electricity and natural gas.

However, the relative independence afforded by the charcoal fuel may
be attractive, particularly for much higher temperatures or larger
kilns.  Our electrical supply at my house is nominally 240VAC 25A,
which is only 6000 watts; a larger kiln, or one with thinner walls,
could easily exceed that.  (And gas appliances are heavily restricted
for safety reasons.)  Also, electric kilns generally can’t do
reduction firing.
