One of the things I've been thinking about for cyclic fabrication
systems is SIMD operations: making many instances of the same part at
once, thus avoiding the duplication of control complexity.  In some
cases today this is as simple as ganging several oxy cutting torches
together to cut the same shape in different parts of a metal sheet.
But watching [this 01967 promotional video for the Monarch 220 N/CC
lathe][0], which featured numerical control, I was reminded of NC
machine tools as a different possibility.

[0]: https://www.youtube.com/watch?v=JplJAxX3buA

NC machine tools, as opposed to CNC, were controlled by a punched
paper tape.  (Previous "screw machines" were also under automatic
control, but by analog cams rather than digital punched paper tape.)
This allowed them to work without a computer inside of them; most
computations you wanted to do were done ahead of time when preparing
the tape.  The 220 N/CC, however, evidently had the capability of
setting "offsets" at run time to correct errors such as tool wear.

If the stream of instructions can run at the same speed on all
fabrication organs, it could also be generated by a central "control
unit" rather than (as in ribosomes and NC lathes) many copies of the
same tape.

Such a system, like Zyvex's "convergent assembly" approach to cyclic
fabrication systems in a different domain, could permit relatively
simple fabrication at each hierarchical level of complexity.  In file
`nandgame.md` I discussed Olav Junker Kjær's NAND game, a gamification
of the exercises from *The Elements of Computing Systems*, aka *NAND
to Tetris*.  After 27 levels of "design" I had constructed a computer
CPU out of 3155 NAND gates (or 6310 relays), not counting the SRAM; on
each level, I was connecting 8 components or less to form a new type
of component, components I'd already assembled on previous levels.

It follows that if we could build all the identical components
simultaneously, the whole assembly process would take less than 216
time steps.  In the first two time steps we would construct 3155
2-input NAND gates from 6310 relays; in the third time step, we would
configure a few hundred of them as NOT gates by shorting their inputs
together; in the fourth and fifth time step, we would configure a
smaller number of hundreds of NAND gates as AND gates by connecting
their outputs to one of the previously constructed NOTs; in the sixth,
seventh, and eighth timesteps we would build a few hundred OR gates by
connecting NOTs to the inputs of some of the NANDs; and so on.

Of course, after those &lt;216 time steps, what you would have would
be a computer and not a fabricator.  But perhaps we can take the same
approach to constructing a fabricator, starting with a largish number
of parts drawn from a small number of families.  In _My Life and
Work_, Henry Ford explains that "A Ford car" (by which he means a
Model T) "contains about five thousand parts—that is counting screws,
nuts, and all."
