I was thinking about assemblers and tiny monitor programs, and it
occurred to me that there’s a simple DDTish interface for
byte-oriented machines that scales smoothly from a tiny low-level
programming user interface to a fairly complete one.

Level 0: octal input
--------------------

At its most basic level, it has 9 keystroke commands and produces no
output:

- 7: shift accumulator left by 3 bits and add 7 to it;
- 6: shift accumulator left by 3 bits and add 6 to it;
- 5, 4, 3, 2, 1, 0: similarly;
- space: store low byte of accumulator at the open location, clear
  accumulator, and set the open location to the next location (*i.e.*,
  the open location plus one byte);
- $ (a literal dollar sign): call the current subroutine location;
  initially the open location is at the beginning of the current
  subroutine location.  When the subroutine is called, it is passed
  certain pointers in registers, such as the open location, the
  current subroutine location, and the monitor program itself, which
  it can use to modify the monitor program.  Moreover, it can change
  these registers to change the state of the monitor program.

Other keys are ignored.  This is sufficient to type in a machine-code
program in octal and run it, assuming you never hit space at the wrong
time.

This is about 16 lines of C:

    char subroutine[SIZE];
    int main()
    {
        char *cursub = subroutine;
        char *point = cursub;
        int acc = 0;
        for (;;) {
            int c = getche();
            if (('0' <= c) && (c <= '7')) acc = acc << 3 | (c & 7);
            else if (c == '$') ((void(*)())cursub)();
            else if (c == ' ') {
                *point++ = acc;
                acc = 0;
            }
        }
    }

Despite the use of “getche()”, it’s *intended* that it be possible to
get the input from an input file rather than from an interactive user;
this would allow this program to be used as a loader for an octal
machine code from a textual medium.

This is on the order of 20 instructions and 60 bytes of code.  For
AMD64, with getche() replaced by getchar(), GCC generates the
following machine code for the pseudocode above:

    0000000000001050 <main>:
        1050:       55                      push   %rbp                     # three-instruction prologue is unnecessary
        1051:       48 8d 2d e8 2f 00 00    lea    0x2fe8(%rip),%rbp        # 4040 <subroutine>
        1058:       53                      push   %rbx
        1059:       31 db                   xor    %ebx,%ebx                # zero accumulator in %ebx
        105b:       50                      push   %rax                     # unnecessary instruction
        105c:       e8 cf ff ff ff          call   1030 <getchar@plt>       # <-------------------------------------------\
        1061:       8d 50 d0                lea    -0x30(%rax),%edx         # subtract '0' from input char                |
        1064:       83 fa 07                cmp    $0x7,%edx                # check if it’s within octal bounds           |
        1067:       77 0a                   ja     1073 <main+0x23>         # go here if not                              |
        1069:       c1 e3 03                shl    $0x3,%ebx                #      |   shift accumulator left 3 bits      |
        106c:       83 e0 07                and    $0x7,%eax                #      |   mask off 3 bits from input char    |
        106f:       09 c3                   or     %eax,%ebx                #      |   merge input char into accumulator  |
        1071:       eb e9                   jmp    105c <main+0xc>          #      V   go to next iteration of main loop -/
        1073:       83 f8 24                cmp    $0x24,%eax               # check for $ (0x24)                          |
        1076:       75 09                   jne    1081 <main+0x31>         # go here if not $                            |
        1078:       31 c0                   xor    %eax,%eax                #      |   unnecessary instruction            |
        107a:       e8 c1 2f 00 00          call   4040 <subroutine>        #      |   this ought to be indirect XXX      |
        107f:       eb db                   jmp    105c <main+0xc>          #      V   go to next iteration of loop ------/
        1081:       83 f8 20                cmp    $0x20,%eax               # check for ' ' space                         |
        1084:       75 d6                   jne    105c <main+0xc>          # if not space, next iter --------------------/
        1086:       88 5d 00                mov    %bl,0x0(%rbp)            # store accumulator byte at point             |
        1089:       48 ff c5                inc    %rbp                     # increment point to next byte                |
        108c:       31 db                   xor    %ebx,%ebx                # clear accumulator                           |
        108e:       eb cc                   jmp    105c <main+0xc>          # next iteration of loop ---------------------/

I think you could do a lot better by hand-coding (*e.g.*, using
`stosb` on amd64 would be useful), and of course GCC’s optimization to
remove the `cursub` variable breaks the intended functionality, but
hopefully the idea is clear.

For Thumb-2 it also generates 24 instructions of code (plus a nop)
which occupy 64 bytes:

       0:   b570            push    {r4, r5, r6, lr}    @ uselessly save registers that are never restored
       2:   4c0e            ldr     r4, [pc, #56]       @ (3c <main+0x3c>)
       4:   4e0e            ldr     r6, [pc, #56]       @ (40 <main+0x40>) two redundant pc-relative constants
       6:   2500            movs    r5, #0              @ initialize accumulator to 0
       8:   447c            add     r4, pc              @ set r4 to point to buffer
       a:   447e            add     r6, pc              @ load pointer to subroutine into r6
       c:   f7ff fffe       bl      0 <getchar>         @   <--------------------------------------------\
      10:   f1a0 0330       sub.w   r3, r0, #48         @ 0x30, '0'                                      |
      14:   2b07            cmp     r3, #7              @ is it out of bounds of 0-7?                    |
      16:   d804            bhi.n   22 <main+0x22>      @ go here if not a digit (out of bounds)         |
      18:   f000 0007       and.w   r0, r0, #7          @     |  extract low 3 bits                      |
      1c:   ea40 05c5       orr.w   r5, r0, r5, lsl #3  @     |  OR it into r5 accumulator               |
      20:   e7f4            b.n     c <main+0xc>        @     v  and go back to top of loop -------------/
      22:   2824            cmp     r0, #36 @ 0x24      @ is the input character '$'?                    |
      24:   d101            bne.n   2a <main+0x2a>      @ go here if not '$'                             |
      26:   47b0            blx     r6                  @     |  invoke subroutine pointer in r6         |
      28:   e7f0            b.n     c <main+0xc>        @     v  and go back to top of loop -------------/
      2a:   2820            cmp     r0, #32             @ is the input character space?                  |
      2c:   d1ee            bne.n   c <main+0xc>        @ if not space, go back to top of loop ----------/
      2e:   4623            mov     r3, r4              @ copy buffer pointer into r3 (uselessly)        |
      30:   f803 5b01       strb.w  r5, [r3], #1        @ store accumulator into buffer, incrementing ptr|
      34:   2500            movs    r5, #0              @ clear accumulator                              |
      36:   461c            mov     r4, r3              @ move incremented pointer back into r4          |
      38:   e7e8            b.n     c <main+0xc>        @ now, back to top of loop ----------------------/
      3a:   bf00            nop                         @
      3c:   00000030        .word   0x00000030          @
      40:   00000032        .word   0x00000032          @

Here there are even more obvious improvements that could be made, but
this time GCC kept the current-subroutine call indirect through r6,
which means that the called subroutine can change it.

I tried this table-driven version using two parallel arrays and making
all the variables global:

    int main()
    {
      for (;;) {
        int key = getchar();
        for (long i = 0; keys[i]; i++) {
          if (key == keys[i]) {
            handlers[i](key);
            break;
          }
        }
      }
    }


On amd64 this function compiled to 13 instructions and 39 bytes,
barely smaller than the batteries-included version above:

    00000000004004c0 <main>:
      4004c0:       50                      push   %rax
      4004c1:       e8 ea ff ff ff          call   4004b0 <getchar@plt>
      4004c6:       31 c9                   xor    %ecx,%ecx
      4004c8:       89 c7                   mov    %eax,%edi
      4004ca:       0f be 91 40 28 40 00    movsbl 0x402840(%rcx),%edx
      4004d1:       84 d2                   test   %dl,%dl
      4004d3:       74 ec                   je     4004c1 <main+0x1>
      4004d5:       39 fa                   cmp    %edi,%edx
      4004d7:       75 09                   jne    4004e2 <main+0x22>
      4004d9:       ff 14 cd 40 20 40 00    call   *0x402040(,%rcx,8)
      4004e0:       eb df                   jmp    4004c1 <main+0x1>
      4004e2:       48 ff c1                inc    %rcx
      4004e5:       eb e3                   jmp    4004ca <main+0xa>
      4004e7:       66 0f 1f 84 00 00 00    nopw   0x0(%rax,%rax,1)
      4004ee:       00 00 

Because the state variables like `acc` are in RAM now, each of the
no-longer-inline handlers has bloated up to be humongous.  Remember
the previous handler for space:

        1086:       88 5d 00                mov    %bl,0x0(%rbp)            # store accumulator byte at point             |
        1089:       48 ff c5                inc    %rbp                     # increment point to next byte                |
        108c:       31 db                   xor    %ebx,%ebx                # clear accumulator                           |
        108e:       eb cc                   jmp    105c <main+0xc>          # next iteration of loop ---------------------/

By contrast, this version is 35 bytes:

    00000000004005f3 <enter>:
      4005f3:       48 8b 05 46 23 00 00    mov    0x2346(%rip),%rax        # 402940 <point>
      4005fa:       48 8d 50 01             lea    0x1(%rax),%rdx
      4005fe:       48 89 15 3b 23 00 00    mov    %rdx,0x233b(%rip)        # 402940 <point>
      400605:       8b 15 75 23 00 00       mov    0x2375(%rip),%edx        # 402980 <acc>
      40060b:       88 10                   mov    %dl,(%rax)
      40060d:       31 c0                   xor    %eax,%eax
      40060f:       89 05 6b 23 00 00       mov    %eax,0x236b(%rip)        # 402980 <acc>
      400615:       c3                      ret

I guess that’s totally a side effect of writing it in C, though.  In
assembly it could just be two bytes: `stosb; ret`.

Anyway, the thing to do at level 0 is to add the code to get to level
1.

Level 1: random octal access
----------------------------

Level 1 adds a single command, which is on the order of another 16
instructions, almost as much code as all of level 0:

- /: opens the location the accumulator points to, clears the
  accumulator, and prints out the byte at the open location.  So for
  example 400/ would open location octal 400 and show you what’s
  there.

The addition of this single command makes it possible to inspect
memory and change arbitrary locations in memory.  Adding the keystroke
command involves patching the main loop, for which purpose you need to
be able to move the point around first, so first you have to enter the
code for / and invoke it with $ until you succeed at patching the main
loop to invoke it instead.

The `cmp $0x24, %eax; jne 1081` sequence above would be 203 370 44 165
11 in octal (`(mapconcat (lambda (x) (format "%o" x)) (list #x83 #xf8
#x24 #x75 #x09) " ")`), 44 being the 0x24 ($) and 11 being the jump
offset, nine bytes.  Above, GCC generated the optimized `jne 105c`
sequence for the final conditional jump, but you probably don’t want
to do that; instead you’d like to conditionally jump to the
unconditional jump at the end of the loop so you can overwrite it with
another similar test-and-jump sequence for the new key you’re adding.

This requires an octal print routine.  An octal print routine is about
10 instructions on machines with a stack, even without a division
instruction or subroutine; for AMD64, GCC comes up with this:

    octprint: push %rbx         # save caller’s register (which has a digit)
              mov %edi, %ebx    # save our argument in a callee-saved register
              and $7, %ebx      # extract last octal digit
              sar $3, %edi      # compute remaining digits for recursion (wrong)
              jz 1f             # but only recurse if nonzero
                call octprint   # (recursive call)
        1:    or $'0, %ebx      # convert binary octal digit to ASCII
              mov %ebx, %edi    # move from callee-saved register to argument
              pop %rbx          # restore callee-saved register,
              jmp putchar       # then finally tail-call putchar

The right shift ought to be unsigned, which was a bug in the C I fed
it.

I thought that maybe if you pass the recursive octprint’s argument in
%rbx instead of %rdi (so putchar won’t clobber it) you could simplify
this a little, and yeah this is 9 instructions instead of 10, which is
arguably an improvement:

    octprint: push %rbx
              shr $3, %ebx
              jz 1f
                call octprint
        1:    mov %ebx, %edi
              pop %rbx
              and $7, %edi
              or $'0, %edi
              jmp putchar

Then the command code might be something like these 7 instructions:

    openloc:  mov %rbx, %rbp     # open location accumulator %rbx points to
              xor %ebx, %ebx     # zero accumulator
              mov (%rbp), %bl    # load byte to print
              call octprint      # note that this zeroes %rbx as a side effect
              mov $'\n, %edi     # newline or space to be less confusing
              call putchar
              ret                # return to main loop

Once you’ve patched the main loop so you can invoke the new command
with / instead of $, you can add more new commands.  If the
current-subroutine pointer is stored in RAM at a fixed location, you
can use the / command to open it so you can overwrite it with a
different pointer, so that you can test those new commands with $
before attempting to patch them into the main loop.

It may be tricky to patch the new command into the main loop entirely
interactively, since the space command only writes a single byte, and
overwriting a single byte of the jump instruction at the end of the
loop could easily result in mutating it into the wrong thing, such as
a jump to the wrong place.

Further levels (sequencing to be determined)
--------------------------------------------

The objective here is something that you can use more or less like a
normal assembler and debugger which gives you a live memory view
display when you’re in the monitor/debugger.  So, at some point, to
get basic assembler functionality, you need a symbol table, and in
order to make the symbol table work, you need to maintain a set of
relocations for each symbol, so that when you define the symbol, the
references to the symbol start working.  For interactive use, we’d
like to be able to *redefine* an existing symbol, too, so that
existing references to the symbol get pointed to the new definition,
which means that the relocations have to be idempotent.

Initially maybe you’d like the symbols to be fixed in size, like one
or two bytes, but probably not forever, and eventually you’d like to
have autocomplete for them.  In order to continue with the
keystroke-command approach, you probably want to have prefix
characters to introduce symbol definitions and symbol references;
symbols have to use a different input mode, where for example digits
don’t update the accumulator.  And you probably want different kinds
of symbol references, and which ones is architecture-dependent; for
amd64, for example, you need short 1-byte PC-relative jump targets,
longer 4-byte PC-relative jump and call targets, PC-relative data
addresses (with 4-byte offsets like the 0x2fe8 above), and I think
absolute (64-bit I guess) data addresses too.  ARM might need some
kind of constant pool, and RISC-V needs hi20 and lo12 relocation
types.

Fairly quickly you’ll also want some ways to recover from errors:

1. An interrupt key to halt infinite loops and return to the monitor;
2. Memory image save and load commands so that you can restart the
   monitor after a power failure or memory overwrite;
3. Fault handling so that things like accidentally running off the end
   of a chunk of code into hyperspace will also return control to the
   monitor.

You probably want the keybindings to be table-driven so you don’t have
to keep patching the main loop every time you want to add a new
command.  Also, you probably want to have a list of your own
subroutines to call every time the monitor refreshes the screen, to
provide further output, plus some way to prevent them from making the
screen refresh unusable.  (Maybe if a timer interrupt happens enough
times during one of them, it should be aborted and disabled.)

Keeping the screen constantly refreshed to display the state of
memory, at least when paused in the monitor, is a way that this could
be better than ITS DDT.  Visible menus is another.

If a smooth transition to a more usable assembly language syntax is
part of the goal, it might be best to use a character other than space
for the “update point and advance” command, though maybe still a
character that’s easy to type.  Apostrophe or minus might work, so
instead of typing “203 370 44 165 11 ” you’d type “203'370'44'165'11’”
or “203-370-44-165-11-”.  Then you could use whitespace in your
assembly language, which might be important.

It might be reasonable to shoot for something Forth-like where
normally you’re in a keystroke-command mode but 

IBM BIOS
--------

Suppose you want to try to bootstrap this thing on an IBM PC
compatible, real or emulated, using BIOS or MS-DOS?

[To read and echo a character with BIOS][1] with fasm (see also
[displaying characters with DOS or BIOS][5], [Ralf Brown’s Interrupt
List][6], and in particular [the int 16h docs from it][7]):

    mov ah, 0h   ;get character from keyboard
    int 16h      ;and store it in AL
    mov ah, 0eh  ;Display a character in AL
    int 10h      ;aka, echo it

Also, under MS-DOS, int 21h with AH=01h is “read keyboard character
and echo”, in AL, which supports I/O redirection.  Less useful for a
boot sector.

To display text in text mode, [put it at B800:0000][2] in [VGA text
mode][9] and its predecessors, [or B000:0000 for the MDA][3], or
[other addresses for other adaptors][4].  That last link also lists
the video modes to set [with int 10h AH=0h][8]; 03h is the usual
80×25.

> The data is arranged in cells, with each cell being two bytes long
> (one byte for the ASCII character to display, followed by an
> attribute byte indicating the colour of the character.) The data
> appears starting at the top-left of the screen (1,1) and works its
> way across to the right (80,1) and then wraps down to the next line
> (1,2).
[9]: https://en.wikipedia.org/wiki/VGA_text_mode
>
> The attribute byte contains two nybbles of data. The most
> significant four bits are the background colour, and the least
> significant bits are the foreground colour. This means the
> background and foreground colours can each be assigned a colour
> value between zero (black) and 15 (white) - see the table on the
> right for the full list of colours.

Graphics modes on the IBM platform are a real pain.  See file
`vga-modes.md` for more detail.

[1]: https://stackoverflow.com/questions/12445980/assembler-fasm-read-character
[2]: https://moddingwiki.shikadi.net/wiki/B800_Text
[3]: https://retrocomputing.stackexchange.com/questions/3053/how-to-write-directly-to-video-memory-using-debug-exe-in-ms-dos
[4]: http://www.delorie.com/djgpp/doc/rbinter/it/10/0.html
[5]: https://stackoverflow.com/questions/44747002/displaying-characters-with-dos-or-bios
[6]: http://www.cs.cmu.edu/~ralf/files.html
[7]: http://www.ctyme.com/intr/rb-1754.htm
[8]: http://www.ctyme.com/intr/rb-0069.htm

But what if you don’t have a character generator?
-------------------------------------------------

Lots of small embedded platforms don’t have hardware character
generators.  In fact, character generators are super out of style now.
They’re just about always done in software.

In [dofonts-1k][0] I wrote the following software character generator
in 325 bytes of JS:

    d = document
    function r() {
     t = d.body.innerHTML
     x = 0
     y = 0
     for(i in t) {
      c = t.charCodeAt(i)
      if(32<=c && c<128) {
       fi = c-32
       d.getElementById('c').getContext('2d')
       .drawImage(d.images[0],fi%16*4,~~(fi/16)*6,4,6,x,y,4,6)
       x += 4
      }

      if(x>508 || c===10) {
       x = 0
       y += 6
       if(y>=512) break
      }
     }
    }

[0]: http://canonical.org/~kragen/sw/dofonts-1k.html

This handles not just drawing a 2-D character buffer onto the screen
but actually figuring out where each character from the bytestream
goes.  Exactly how much of a headache this kind of thing will be
depends on the structure of the screen.  Here’s one approach to
rendering text into something particularly easy: a 1-bit-deep
framebuffer with a fixed-width 8-pixel-wide font.  We move 8 pixels at
a time as a single byte.

    void rendert(char *font, char *text, char *screen)
    {
       for (int row = 0; row < TEXT_HEIGHT; row++) {
         for (int off = 0; off < FONT_HEIGHT * LINE_SIZE; off += LINE_SIZE) {
           for (int col = 0; col < TEXT_WIDTH; col++) {
             *screen++ = font[off + text[col]];
           }
         }
         text += TEXT_WIDTH;
       }
    }

I’m supposing that all the capital-letter text is compile-time
constants, so we really only have six variables.  For one plausible
set of such constants, GCC came up with these 21 instructions in 74
bytes:

       0:   4c 8d 96 c0 12 00 00    lea    0x12c0(%rsi),%r10               # iteration limit := text + 4800
       7:   49 89 d1                    mov    %rdx,%r9
       a:   45 31 c0                    xor    %r8d,%r8d                   # off = 0
       d:   31 c0                           xor    %eax,%eax               # col = 0
       f:   0f be 0c 06                         movsbl (%rsi,%rax,1),%ecx  # load byte from text (probably shouldn't be signed!  Use movzbl)
      13:   44 01 c1                            add    %r8d,%ecx           # add off to text byte
      16:   48 63 c9                            movslq %ecx,%rcx           # unnecessary instruction, goes away if I declare off as unsigned
      19:   8a 0c 0f                            mov    (%rdi,%rcx,1),%cl   # load 8 pixels from font
      1c:   41 88 0c 01                         mov    %cl,(%r9,%rax,1)    # store 8 pixels into screen
      20:   48 ff c0                            inc    %rax                # col++
      23:   48 83 f8 50                         cmp    $0x50,%rax          # has col reached TEXT_WIDTH = 80?
      27:   75 e6                               jne    f <rendert+0xf>
      29:   41 83 c0 10                     add    $0x10,%r8d              # off += LINE_SIZE
      2d:   49 83 c1 50                     add    $0x50,%r9               # strength-reduced screen++ 80 times
      31:   41 81 f8 00 01 00 00            cmp    $0x100,%r8d             # has off reached FONT_HEIGHT * LINE_SIZE == 256?
      38:   75 d3                           jne    d <rendert+0xd>
      3a:   48 83 c6 50                 add    $0x50,%rsi                  # text += TEXT_WIDTH (80)
      3e:   48 81 c2 00 05 00 00        add    $0x500,%rdx
      45:   4c 39 d6                    cmp    %r10,%rsi                   # has our text pointer reached its end marker?
      48:   75 bd                       jne    7 <rendert+0x7>
      4a:   c3                      ret

Interestingly, it eliminated `row` entirely, instead computing that,
for my 60×80 screen, `text` (%rsi) would reach 4800 more than its
original value (0x12c0), and storing that limit in %r10.

I feel like it got a little too enthusiastic about strength-reduction
here; it invented a new variable %r9 so it can increment %rdx every
outermost loop by 1280 (0x500, 16 scan lines times 80 columns), only
to get copied into %r9, which is the starting address in the
framebuffer for each line of text.

For Thumb-2 it was 22 instructions but only 60 bytes.  In fact, the
following apparently more complex version which works like the JS is
also only 60 bytes, and 24 instructions (some of which are
multiplies).  This version maybe has worse locality of reference.

    void renderu(char *font, unsigned char *input, char *screen)
    {
      int x = 0,                    // in characters
        y = 0,                      // in pixels
        c;                          // character just read

      while (y < TEXT_HEIGHT * FONT_HEIGHT && (c = *input++)) {
        if (c >= 32) {
          for (int line = 0; line < FONT_HEIGHT; line++) {
            screen[(y + line) * TEXT_WIDTH + x] = font[line * LINE_SIZE + c];
          }
        }

        if (c == '\n' || x >= TEXT_WIDTH) {
          y += FONT_HEIGHT;
          x = 0;
        }
      }
    }

Anyway, either of these is comparable to the original micro-monitor
code in size; not insignificant by comparison, but also not dwarfing
the micro-monitor.  Even if you do have a character generator (or
serial line), it’s probably worthwhile to stuff it in somewhere around
level 2, just so you can edit the font and see the results instantly
on the screen.  Also, a screen is cooler than a serial port.

