Watching [Adam Booth machine a gearbox shaft to within about 5 microns
of cylindricity][0] it occurred to me that this micron-level precision
is capable of encoding a lot of information, like the stacked cams of
the Jaquet-Droz Writer, or like the sound in a record-player groove.
But how much information?

[0]: https://youtu.be/vAo0xmDQ-kI

Booth's shaft is about 100mm across (which would be 314mm
circumference) and about 1500mm long.  A cam follower could very
reasonably be 0.5 mm wide and long and follow slopes of up to 45°.  So
each 500-micron unit of circumference could plausibly encode about 10
bits of information in its radius change, anywhere from -500 to +500
microns (subject to the constraint that the total around a closed loop
be 0).  This crudely gives 628 10-bit units on a given track and 3000
tracks for a total of about 1.9 megabits.

Of course a disc with a similar surface area would provide a similar
degree of precision in much less volume.  And phonorecords record much
more information precisely because they are sensitive to much smaller
deviations in the surface and over a much shorter length.
