Suppose you have a six-bit field available in an instruction format to
indicate a source register for some operation, such as memory
indexing.  This would be enough to select one of 64 registers.  64
registers is an unreasonably large number of general-purpose CPU
registers (if you have RAM, anyway; Zuse's Z4 had 64 32-bit
random-access floating-point registers plus a two-register stack, but
no additional RAM).  Unless you have additional hardware support, it
slows down context switches between processes in exchange for a
usually insignificant speedup in user code.

An interesting possible alternative would be to have 16 registers,
like the ARM and amd64, and to use 2 bits of the 6-bit field to select
a "quadrant" of 4 registers, and the other 4 bits to select any or all
of the registers from that quadrant, which would be combined in some
specific way.  The most useful way would usually be to add them
together, but often that would be too expensive.  Cheap things you
could do include bitwise AND and OR.

Does this give you useful new functionality?  Not obviously.
