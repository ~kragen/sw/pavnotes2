Suppose your long-term memory can add information at half a bit per
second, you can handwrite 17 words per minute, and you can type 60.
How long would it take you to write down everything you know?

80 years works out to 160 megabytes, and the information content of
English is about 6 characters per word and about [0.91 bits per
character][0], so about 5.4 bits per word.  Then 17wpm is 1.53 bits
per second and 60 is 5.4.  The corresponding times are then about 26
years and about 7½ years.

[0]: http://prize.hutter1.net/
