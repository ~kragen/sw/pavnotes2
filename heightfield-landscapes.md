Watching a YouTube video by digital artist Posy about TerraGen, I was
reminded I had a note to write about Minetest, Cube, landscape
generation, splines, and sinc.

Minetest generates three-dimensional mapblocks of “nodes” on demand
from Perlin noise (or simplex noise, I forget) and stores them
compressed in a database; I think it uses [16×16×16 mapblocks with 4
bytes per node][3], thus 16 kibibytes per mapblock.  The database can
grow unwieldy, easily hundreds of megabytes and sometimes tens of
gigabytes, and the world still has rather limited resolution and
degrees of elaboration in order to keep it tractable: a node is about
a one-meter cube.  The limitations of the node structure also afford
the players creative freedom, because as with plain text, it’s
relatively clear how to get from the state you’re in to the state you
want to be in: break the nodes you don’t want to be there, and place
the nodes you do.

[3]: https://forum.minetest.net/viewtopic.php?t=28728

[The original Doom engine, aka id Tech 1, couldn’t handle “rooms over
rooms”][0] because its world, though apparently 3-D, was really just a
pair of heightfields.  [Wikipedia explains][1]:

> Although the engine renders a 3D space, that space is projected from
> a two-dimensional floor plan. The line of sight is always parallel
> to the floor, walls must be perpendicular to the floors, and it is
> not possible to create multi-level structures or sloped areas
> (floors and ceilings with different angles).

You could, however, have “furniture” in a room, including furniture
that looked like an opaque floor or ceiling.  [Fabien Sanglard offers
a walkthrough of the renderer][5].

[0]: https://www.doomworld.com/forum/topic/135198-why-cant-the-original-doom-room-over-room/
[1]: https://en.wikipedia.org/wiki/Doom_engine
[5]: https://fabiensanglard.net/doomIphone/doomClassicRenderer.php

In 01992 NovaLogic shipped the game Comanche with real-time 3-D
terrain rendering from heightfields using a rendering algorithm they
called “Voxel Space”.  This could render 3-D terrain in real time on
an [80386SX with 4 MiB of RAM and no floating-point hardware][4].
[Sebastian Macke has reproduced the algorithm in a few lines of JS][2]
with wonderful visualizations of how it works.

[2]: https://github.com/s-macke/VoxelSpace
[4]: https://www.mobygames.com/game/5078/comanche-maximum-overkill/specs/

TerraGen is deeply heightfield-based, using heightfields for all or
almost all of its rendering.

Wouter van Oortmerssen’s “[Cube][6]” FPS engine takes the heightfield
approach further by putting it in octrees and painting heightfields on
all six faces of game objects.

[6]: http://cubeengine.com/cube.php

If you have 256 terrain levels, as I think Comanche did, 16 KiB of
heightfield buys you 128×128, which is sort of 64 times larger than
16×16×16.  This, in turn, ought to mean you can handle something like
an order of magnitude more detail.  With a deterministic mapgen, you
can regenerate parts of the map on demand, so they don't all have to
be stored.  And you can design the mapgen for LoD rendering with LoD
mapgen.

What if you still quantize heights to ⅛m or so on a regular square or
hexagonal grid, but use a customizable kernel function to interpolate
between them?  This could give you an efficiently storable,
high-resolution heightfield, which the user could edit in-game with
somewhat predictable results.  In some sense the sinc function is the
ideal interpolator, but low-order splines may be both more efficient
and more predictable because of their finite support.  A zero-order
spline is what Minetest gives you, a first-order spline gives you
linear interpolation (neighborhood size 4), and a second-order spline
gives you parabolic curves with a bit of Gibbs ringing (neighborhood
size 16).

For structures like houses, caves, or giant mushrooms, you could have
three or more levels of heightfield in certain places.
