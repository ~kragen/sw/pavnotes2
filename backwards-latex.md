A latex has drops of a hydrophobic substance colloidally suspended in
a small amount of water, usually through the use of massive quantities
of an amphiphilic surfactant detergent; when some of the water
evaporates, the drops coalesce into a solid mass.

Tonight I was cleaning juniper pitch off my hands with table salt and
dishwashing detergent.  The detergent contains some water, but the
salt saturates it rather quickly, then serves as a kind of mild
abrasive, scraping the pitch off my skin.  Presumably the result of
this process is grains of salt coated with juniper pitch, suspended in
an aqueous solution whose amphiphilic detergent keeps them from
coalescing, somewhat like a latex.  But my next move is to wash the
stuff off my hands with more water, dissolving the salt immediately
out of the middle of each pitch grain and gradually reducing the
concentration of the detergent below the critical micelle
concentration.  (I’ve also used mayonnaise and hair conditioner in
place of dishwashing detergent with great success.)

As it happens, the suspended pitch grains remain suspended in the
water at least long enough to go down the drain.  But it seems like it
would be easy to arrange a related granular system where water would
coalesce them instead.

If you try to just granulate juniper pitch or a similar substance on
its own, the grains will stick together rather soon after coming to
rest in any position.  The standard approach to solving this problem
in industrial granulation systems is to coat each sticky grain with a
thin layer that isn’t sticky, maybe made up of much smaller grains.
This keeps the sticky material inside each grain from coming into
contact with the material in neighboring grains, so the granular
material stays pourable.

What if the dry surface material is water-soluble, but the sticky
material isn’t?  For example, as long as the humidity is low, you
could use table salt as the dry material, and juniper pitch or similar
as the sticky inner material.  What happens if you pour the dry
material into a mold, then pour water onto it?  I think it should set
up solid fairly immediately (or, at any rate, as solid as the pitch
normally gets) as the water washes the salt out from in between the
grains, but then only has ζ-potential repulsion between the grains to
keep them apart, rather than the solid’s shear strength.  For grains
that are large enough with significant roughness, this will result in
asperities contacting one another and adhering.

This is somewhat similar to the mechanism of granulated salt or sugar
clumping in the container from atmospheric humidity, which also works
by converting a slippery solid surface layer to a sticky liquid.  But
in this configuration I think it can give rise to much stronger shapes
because you can remove all but traces of the surface layer, so you
aren’t limited by its strength.

A potentially more interesting version of this approach (well, or
arguably of concrete-making) would use self-propagating
high-temperature synthesis (SHS) to convert the disconnected surface
layers into a strong, continuous cement phase binding inert filler
particles into a composite.  Assuming the surface layers are thin
compared to the filler grain size and their thermal densities are
comparable, the filler quenches the hot cement fairly rapidly, so it
only spends a very brief time at high temperature.
Back-of-the-enveloping, it should be possible to get an ignitable
surface layer under 10μm thick (though probably not from powders),
typical thermal diffusivities of solids are on the order of 10mm²/s
(though ranging from 0.082 for white pine to 1000 for diamond) so the
relevant timescale is as low as 10μs.  This suggests a reasonable
velocity of propagation on the order of a meter per second.
