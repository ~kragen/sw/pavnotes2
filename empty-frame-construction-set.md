I was watching [YouTuber Angus Deveson’s explanation of his
Polypanels-based robot][0], where [Polypanels is “Make Anything”
YouTuber Devin’s unfortunately named construction set design from
02019][1], which consists of mostly squares and triangles and
pentagons that snap together along the edges to form snap-fit hinge
joints, the same as Fillygons.  Any edge can connect to any other
edge, but the edges have a directionality, so each panel has an inside
side and an outside side; although you can make flexible structures,
you cannot make a Möbius strip.

(I say it’s unfortunately named because apparently “polypanel” was
already a common term for plastic foam sandwich panels used in
building construction.)

[0]: https://youtu.be/R-0VXWROl4I
[1]: https://youtu.be/6RamWwngvKg

Printing a construction set does seem like the best possible use of a
3-D printer, and I like the idea of a construction set designed around
joints with a remaining degree of freedom rather than rigid
connections.  There are just a couple of things I don’t like about
this system:

1. The size is about 20mm.  In human terms, that’s pretty small, which
   is probably a practical necessity due to the slowness of 3-D
   printers.  (Another YouTuber tried printing some out at 500% scale,
   like 400mm on a side, and [each triangle took over two hours][2],
   while on a different printer each square took over eight hours.)
   This means that things like coffee tables and vehicles that you can
   ride in are probably out of reach, although he’s made, for example,
   a purse.
   
2. Of the five degrees of freedom the assembled joints resist movement
   in, three of them are ways that you can assemble the joint or take
   it apart.  That is, the force required to disassemble a joint is
   just the joint’s strength; you disassemble it by overcoming its
   strength in one of its intended load directions.  This seems
   undesirable to me for a construction set; it would be better for
   the movement of disassembly to be orthogonal to the load
   directions, so that the strength of the joint in use can be many
   orders of magnitude higher than the force required to disassemble
   it.  This is maybe a bigger practical problem for furniture than
   the size thing.

3. In sewing, you sew one stitch at a time, and then all the stitches
   bear a load together, at the same time; the same principle is at
   play when you tighten one bolt at a time in a bolted joint, weld
   one millimeter at a time along a seam, or drive one nail at a time
   into a bookshelf.  To the extent that this is possible with
   Polypanels, it seems to be due to their flexibility, which for some
   applications is an imperfection.

4. You can only connect two panels together at an edge, although
   there’s a “square doubler” piece that allows you to connect two
   square panels back to back.

[2]: https://youtu.be/PcrGUSx9ZJo

However, a really interesting thing about some of the Polypanels
pieces is that they’re hollow.  That is, the square or triangle is
just an outline of a square or triangle, with nothing in the middle.
The result is that you can make a sort of lightweight truss by joining
together polygons at their edges; the hinge, if held in a non-planar
configuration by other parts of the structure, unites the edges of the
two polygons into a pretty rigid strut similar to an angle iron.
(Some others have, for example, a planar triangular or hexagonal
infill.)

This seems like a pretty promising principle for a 3-D construction
kit: build trusses not by connecting prefab struts to nodes but by
joining the edges of prefabricated, planar polygons into ad-hoc
struts.  An icosahedron made out of struts and nodes contains 12 nodes
and 30 struts; assembling it requires connecting a strut to a node 60
times.  Assembling it from prefab triangles only requires connecting a
face to a face 30 times.

(The IQLight lampshade design uses a similar principle, with its
fundamental unit being the golden rhombus of the rhombic icosahedron
rather than a triangle.  Still, IQLight-style lampshades are
surprisingly difficult to assemble successfully; their high degree of
symmetry makes them very confusing and disorienting.)

If we consider the regular cuboctahedron, which is a unit cell of a
close packing of spheres (Bucky’s “octet truss”), its exterior
consists of 14 faces (squares and equilateral triangles), 12 vertices,
and 24 edges.  But if you want to assemble them into a close-packed
truss, you’re probably better off with its 24 *interior* equilateral
triangles, each one joining one of its 24 edges to its center, which
are joined along the 12 line segments connecting a surface vertex to
the center.  Four triangles join along each such segment, so you’re
stuck making 48 connections between triangles.  (Polypanels can’t do
this, because each edge can only join two panels, not four.)  But
that’s still better than building it by joining struts to nodes, since
it contains 24 surface edges and 12 internal struts, making 36 struts
and 72 strut–node connections.

However, I suspect that once you proceed to a larger truss, the labor
savings start to evaporate.  In a configuration of close-packed-sphere
nodes, where each node is linked to 12 others (in effect, in the
center of its own cuboctahedron) adding a new node involves, on
average, 6 struts and 12 strut–node connections.

But how many triangles is that?  I think you can get by even if you
just fill space with cuboctahedrons with their 24 internal triangles
and none of their surface triangles.  Consider the orientation in
which the cuboctahedra are sitting on triangular faces, so their
equators are hexagons which tile a plane.  Consider one such
cuboctahedron, A, sitting on the ground.  A’s square faces must adjoin
the square faces of other cuboctahedra, thus forming an octahedron
between the two cuboctahedron centers out of eight triangles going to
their centers.  This implies that the other cuboctahedron’s center is
in the same horizontal plane as A’s top face, and its basal triangle
is in the same horizontal plane as A’s center, so that horizontal
plane isn’t hex-tiled with cuboctahedron equators; rather, it’s
hex/triangle tiled.  So every second row of vertices in that plane has
cuboctahedron centers in it, is half cuboctahedron centers.  The other
rows of vertices have none.  And every horizontal plane is like this,
so one out of four vertices is a center of one of the cuboctahedra of
our space-filling tiling.  And it’s responsible for 24 triangles.

Which means that we have 6 triangles per vertex, each with 3
connections to other triangles (but three of them rather than one), so
you need 18 connections between triangles per node.

So, probably it’s easier to build such a space-filling truss out of
just struts and nodes, even if they’re separate.

What if you just want to build a beam truss rather than a
space-filling truss?  I’m not even sure what the simplest
ommnitriangulated beam truss is.  Any three adjacent parallel lines
forming a triangular prism through the hexagonal-close-packed lattice
are not omnitriangulated; two of the three planes are strips of
equilateral triangles, but the other is necessarily a strip of
squares, so it’s floppy.  Probably you need to bring together two such
triangular prisms, joining those squares into octahedra, to get an
omnitriangulated beam.  Each unit of length added to the beam consists
of an octahedron (sharing two of its faces with pre-existing
structure) and two tetrahedra (sharing one face each with the
octahedron).  You have added four nodes and 14 struts, and thus 28
strut–node connections, or 10 equilateral triangles containing 30
triangle edges.  The prefab polygons don’t lose as badly, but they
still lose.
