I was thinking about different approaches to diagramming
logic circuits, and I came up with an interesting new one
consisting entirely of two-input NAND gates, each represented
by a line whose two ends are near its two inputs.  Here’s a
full adder, computing the Carry and Sum bits from input bits
A and B:

          C
      ╭───┼───╮
     ╭─╮  │ │─┼─S
     │╭─╮╭─╮│╭─╮
     ╰│A│───┼│B│╮
      ╰─╯   │╰─╯│
       │    │ ╰─╯
       ╰───────╯

Explanation of the notation
---------------------------

Inputs are represented by closed loops, which have no ends
and thus cannot be interpreted as NAND gates; here the inputs
A and B are joined by a horizontal line computing A NAND B.
Above it there is a short line curved so that both of its
ends nearly touch the A NAND B line; this inverts that result
into A AND B, the carry output C.  Each of A and B has a
similar inverter with both of its ends nearly touching the
loop for the corresponding input, and then there are lines at
the top and bottom representing (NOT A) NAND B and A NAND NOT
B respectively.  These two are joined by a vertical line that
computes their NAND, which is the XOR or mod-2 sum of A and
B, and is thus labeled S.

In order to make it clear which of the lines in a three-way
junction is intended to be the output, I leave a small space
between it and the other line so it cannot be misinterpreted
as a continuation of one of the other two.  This matters more
for hand-drawn diagrams than for computer-drawn diagrams.
This convention, in turn, strongly suggests drawing
crossovers between unrelated signals as simple crosses, as
where the A NAND B line crosses the vertical line computing
the final sum output, or where the C and S lines cross the
(NOT A) NAND B line.  (In most cases, in diagrams such as
piping and flow diagrams or circuit schematics, I prefer to
draw such crossovers by breaking one of the lines so that it
appears to pass behind the other, but with this visual
representation for input connections, that would make a
crossover look like two input connections.)

Every line must have two ends connected to inputs to
represent a gate.  A line with an end connected instead to a
letter can therefore be used to represent an output.  Lines
never branch; there are no three-way intersections.

Tiles for graphics
------------------

A complete tileset for drawing diagrams like the above
contains just 8 tiles, including blank, not counting the
letters:

     │   ╭ ╮

     ╰ ─ ┼ ╯

Further small examples
----------------------

Here is a two-input mux; Q is either A or B, selected by bit
S.

         ╭─╮
      ╭──│A│
     ╭─╮│╰─╯
    ╭│S││
    │╰─╯│───Q
    ╰─╯ │╭─╮
     ╰───│B│
         ╰─╯

With feedback connections you can produce sequential
circuits.  Here’s an RS latch with active-low inputs.

    ╭─╮
    │R│
    ╰─╯
     │─╮
     │ │─Q
     ╰─│
      ╭─╮
      │S│
      ╰─╯

When both R and S are high, Q remains either high or low.
Pulling S low forces Q high; pulling R low when S is high
forces Q low.  Adding an enable signal is fairly simple, and
with that you can assemble two such latches into an
edge-triggered master-slave flip-flop.

Here is a three-inverter ring oscillator, which is the
shortest one that reliably oscillates in certain digital logic
families:

    ╭───╮
    │╭│ │
    ╰│╰─│
     ╰─╯│ 
      ╰─╯

Here’s the example half-adder at the top redrawn a bit
awkwardly to have its inputs on the left and outputs on the
right, as is tradition:

       ╭────╮
      ╭─╮   │
     ╭───╮  │
     │ A │─╮│
     ╰───╯ ││
       │─╮ ││
       │ │─┼┼──C
       │─╯ ││
     ╭───╮ ││╮
     │ B │─┼╯│─S
     ╰───╯ │─╯
      ╰─╯  │
       ╰───╯

This includes an XOR, which is one of the hardest logic
functions to compute with NAND-based or NOR-based logic; many
historical machines have instead resorted to pass-transistor
logic, as explained in file `cmos-xor.md`.  Just the XOR part
of it looks like this:

       ╭────╮
      ╭─╮   │
     ╭───╮  │
     │ A │─╮│
     ╰───╯ ││
     ╭───╮ ││╮
     │ B │─┼╯│─S
     ╰───╯ │─╯
      ╰─╯  │
       ╰───╯

XNOR, bitwise equality, can of course be computed with an
additional level of inversion, but it’s not necessary to go
to such lengths; we can simply rejigger the connections
slightly:

       ╭───╮
      ╭─╮  │
     ╭───╮ │
     │ A │╮│
     ╰───╯││
     ╭───╮│┼╮
     │ B │╯││─E
     ╰───╯ │╯
      ╰─╯  │
       ╰───╯

Potential role in logic design
------------------------------

Two-input NAND gates are capable of computing arbitrary
logical functions and providing memory, but I believe that in
every actual physical logic family you can build one-input
and three-input gates, as well as NOR gates, very nearly as
easily.  The Skywater SKY130 PDK also includes 4-input NAND,
AND, OR, XOR, XNOR, AOI, OAI (or-and-invert), and-or, or-and,
adders, comparators, multiplexors, latches, flip-flops,
buffers, etc.  Moreover, standard-cell libraries often
include tristate buffers so you can have a bus with multiple
drivers, and multiple drive strengths so that you can
override a weak drive with a stronger one.

CPLDs and FPGAs don’t really provide gates at all, but sums
of products and LUTs, respectively.

So this is probably not a wonderful way to optimize your
circuitry in a close-to-the-metal way.  In practice you
probably want a representation that lets you say “XOR”
instead of composing the XOR out of NAND gates.

It’s also probably not a great high-level way to design your
logic. Functions like XNOR are hard to recognize in the
diagrams above, and the representation is not especially
suitable for things like normalization.

So I don’t know if it’s useful as a means to anything else.
It does look pretty cool, and it’s conceptually parsimonious.
