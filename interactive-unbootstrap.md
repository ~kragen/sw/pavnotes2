Suppose you wanted to interactively unbootstrap an interactive
computing environment in a virtual machine in a maximally fun way.
How could you design the process with that in mind?

Here are some things that might help:

1. Using a Forthish approach for early stages of the unbootstrap.

2. Graphics.  With color.  And a mouse.

3. Built-in text, even if it's simple.  In combination with graphics,
this means you probably want a framebuffer and some kind of font that
gets rendered into it.

4. Some kind of built-in undo, so that when you horked the virtual
machine state you wouldn't get too discouraged.

5. Sound.  PCM.  Because getting to livecoding would be great.

Having the built-in instruction set be Forthish might or might not be
desirable, but you do want to have some sort of built-in monitor.  The
virtual machine doesn't need to be easy to fabricate in silicon, so it
can incorporate a built-in monitor written in whatever you write the
virtual machine in, such as C.

The appeal of a Forthish development environment is that, even though
Forth code is hard to read and bug-prone, in a few hundred lines of
code you can get an interactive low-level programming environment,
with the ability to inspect memory, run bits of code to see what
happens, script execution, and recursively construct a system.  The
"three instruction Forth" (Brad Rodriguez?) really is the essence:
peek, poke, and call.  If you aren't infallible you also need the
ability to break into infinite loops and get back to the monitor
(which means the virtual machine needs interrupts) and the ability for
the monitor to be invoked on illegal instructions.

A Forthish instruction set also makes compilers easy to write, and
compilers are a thing you want pretty early on.  The appeal of a
register instruction set is partly that it makes things easy ---
assembly language for a register instruction set is easier to *read*;
there are half as many instructions and you can remember where things
are.  But it's also that it makes things hard --- compiling to a stack
machine feels like cheating, while compiling to a register machine
avoids glossing over the main considerations in compiling for a
non-virtual machine.

A virtual machine could reasonably provide image save and restore
facilities, which provides a sort of undo, but I think you don't get
very far into the unbootstrapping before you want a text editor and
compiling from source code so that you can change your mind about
things.

The initial monitor program running inside the virtual machine doesn't
have to pretend to be a TTY.  A reasonable alternative would be for it
to pretend to be a text editor where you can move around with arrow
keys, insert and delete characters (maybe in a fixed-size
non-scrolling buffer using a fixed-width font which initially contains
only a few characters), and invoke commands at the cursor.  Initial
keybindings are maybe backspace, self-insert-character, up, down,
left, right, enter, peek, poke, call, and break.  Peek and poke parse
the memory address to the left of the cursor; peek puts the output to
the right of the cursor, while poke parses the data to store at that
location from the text to the right of the cursor.

I think I'll explore that angle a bit further; see file
`blackstrap.md`.

Poul-Henning Kamp suggests that maybe the ideal interface to a Flash
disk consists of three operations:

1. Store this blob and tell me its name.

2. Read blob X.

3. Erase blob X.

This is a pretty easy set of operations to provide on top of either a
Flash disk or a traditional filesystem, and it's sort of close to
traditional Forth block access.  It's not quite enough because it
doesn't provide a way to mutate any state; a persistent mutable memory
cell to store the name of the root blob in solves that problem.