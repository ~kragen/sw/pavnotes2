Praaprixa, a transactional store interface
==========================================

I was thinking about QEMU’s qcow2 format and similar things, and I
thought that perhaps transactional copy-on-write FP-persistent layer
was something you could reasonably provide as a library layer used by
many applications.  But once you have FP-persistence, so you can read
old snapshots of the file, you’ll inevitably want the ability to fork
new versions from those old snapshots, and name those versions, so
before I knew it, the API looked like a transactional filesystem API.
Moreover, it’s a *remotable* filesystem API, and with only a little
bit extra, it supports asynchronous event notification and concurrent
serializable transactions, though not two-phase commit or anything
similar for participating in distributed transactions.

For the moment I’m calling this system “Praaprixa.”

Introduction
------------

The basic types in the API are Filename, Bytes, Int, Inode,
Etag, Snapshot, Latest, Transaction, and List of
Filenames.

In a typical interaction, you start a Transaction, open Latest,
navigate from it to an Inode using zero or more Filenames, read and/or
write chunks of Bytes to the Inode, and commit the Transaction, which
either fails or creates a new Snapshot.  In a language like Python
with methods and exceptions, without a convenience façade or any error
handling, this might look like this:

    fo = open('./some.db', 'rb+')
    db = praaprixa_open(fo)
    t = db.begin()
    f = transaction.get_latest().walk(b'trunk').walk(b'mimi')
    f.write(f.size(), b'these are some bytes to append')
    transaction.commit()

With a convenience façade:

    with Praaprixa('./some.db') as db:
        with db.begin() as transaction:
            with transaction.open(u'/trunk/mimi', 'a') as f:
                f.write(u'these are some Unicode characters to append')

(Normally in situations with possible concurrency you would want to
retry this on transaction conflict; see below.)

Relationship with standard filesystem semantics
-----------------------------------------------

Praaprixa is an API, and it is very similar to a filesystem API.

Like traditional POSIX filesystems, Praaprixa provides a rooted tree
of Inodes, each of which is either a mutable directory consisting of a
set of (Filename, Inode) pairs in which the Filenames are unique, or a
mutable byte stream.

Praaprixa provides several things traditional POSIX filesystems don’t:

1. Access to past history.
2. Strong transactional guarantees.
3. Efficient, reliable ways of detecting filesystem changes, even
   after system restarts and recovery from errors.
4. Efficient copy-on-write updates of any size, even entire
   subdirectory trees.
5. Efficient batching of large groups of updates, and in particular it
   has very little overhead per file.
6. Safe temporary file creation.

Unlike Praaprixa, traditional POSIX filesystems track modification times,
and track and enforce quotas, ownership, and file permissions, and
they provide symbolic links and mutual-exclusion locks.  Current POSIX
filesystems also provide overlay mounts and bind mounts.

### Access to past history ###

Initially, as shown in the introduction, a Transaction provides a
read-write view of the current state of the Praaprixa store as of the
beginning of the Transaction.  But it is also possible to access any
past Snapshots that may have been retained.  (Retention policy is
outside the scope of the current note.)

    latest = transaction.get_latest()
    snapshot = latest.get_snapshot()
    while snapshot:
        print(snapshot.date())
        snapshot = snapshot.get_parent()

Any Inode from past history can be read, and using `clone_at`, a
copy-on-write deep copy of it can be cheaply added to the current
state:

    new_clone_location = latest.walk('old_foobar')
    snapshot.walk('foobar').clone_at(new_clone_location)

The Latest object is analogous to Git’s “index”, while Snapshots are
similar to Git’s “commits”.  They’re not called “index” and “commits”
because “index” was always a terrible name, and “commit” is the name
of the action that creates a new Snapshot.

`clone_at` can also be used on Inodes from Latest rather than from a
Snapshot, making a cheap copy-on-write copy of them.  This, in turn,
can be used to provide access to a frozen copy of some subtree without
having to paw through the snapshot history.  This is closely analogous
to how branches and tags worked in Subversion.

XXX maybe call `clone_at` `link`? And use it for creating files, too?

In a possibly confusing abuse of terminology, the Inode you get by
walking to the same place in two different Snapshots, or in Latest and
a Snapshot, is not the same even if the file wasn’t changed between
them.  That is, the Inode includes the information about which
Snapshot it belongs to.

### Strong transactional guarantees ###

The view of the Praaprixa store’s current state within a given
Transaction never changes except due to explicit requests from inside
the Transaction, so as long as the Transaction remains open, you can
be assured that you are accessing a consistent point-in-time state.
This means that, for example, a text editor can safely load only the
part of a text file that it is displaying, without the risk that the
rest of the file will be modified out from under it.  This guarantee
is available from the Linux kernel for some ordinary POSIX filesystems
under the name of “reflinks”.

Praaprixa is designed to make it straightforward to write reliable,
correct filesystem updates without race conditions by providing
filesystem-wide optimistically-synchronized Transactions.

Read-only Transactions always succeed, working from a consistent
snapshot of the store.  Write-only Transactions also always succeed.
Read-write Transactions can only commit if none of the inodes they
read from the current state were changed by other Transactions that
committed in the meantime.  This means that read-write Transactions
must be prepared to retry sometimes.  By the same token, they probably
should not retry indefinitely:


    with Praaprixa('./some.db') as db:
        for retry_count in range(4):
            try:
                with db.begin() as transaction:
                    do_stuff(transaction)
            except UpdateConflict:
                pass
            else:
                break

Reading from past Snapshots does not provoke such conflicts, so it is
possible to ensure that Praaprixa does not detect a conflict and roll
back your Transaction by only reading from a past Snapshot instead of
Latest.  This makes Praaprixa act more like a normal Unix filesystem,
in that you’re likely to have lost-update bugs from concurrency, but
it will never force you to retry.

In order to provide the consistent-access guarantee described above to
applications, if a Transaction has such a retry conflict, it does not
prevent you from doing further reads through the Transaction.  You may
want to open a second, concurrent Transaction to enable you to save
your work.

### Detecting changes ###

For things like incremental software recompilation and full-text
indexing, it’s very valuable to be able to tell what has changed since
some point in time in the past, even if the system wasn’t running
during that time.

To support this, there is a version tag called an Etag associated
with each Inode, which is an opaque string which is guaranteed to
remain the same across system restarts and restoring your database
file from backups.  This is similar to Git’s object IDs.  Unlike Git,
it is not guaranteed to be a SHA-1 hash or anything like that, so it
doesn’t support decentralization.  It’s just guaranteed to be 64 bytes
long, and to change if the file contents change (and possibly if it
doesn’t, though that makes it less efficient), and to be different
between any two Inodes that have different contents.  As in Git, a
directory’s contents are considered to have changed if anything in it
has changed.

That means that by comparing the Etags of the corresponding Inodes in
two versions, you may be able to efficiently verify that a
whole directory tree is unchanged:

    snapshot = latest.get_snapshot()
    prev = snapshot.get_parent()
    if not prev:
        print("No previous snapshot")
    elif prev.walk('src').etag() == snapshot.walk('src').etag():
        print("src tree unchanged from previous snapshot")

You can get Etags from Inodes in Latest, too, which counts as reading
the corresponding Inode for purposes of conflict detection.

### Efficient copy-on-write updates ###

As mentioned above, you can make a cheap copy of an Inode with
`clone_at`, cheaply as if by Unix `link(2)`, and that copy is
copy-on-write; but this produces a new Inode, and changes made through
one Inode are not visible through the other, even when they both
belong to Latest, which is a difference from Unix `link(2)`.
Liedtke’s EUMEL got a lot of mileage out of an analogous mechanism, as
do SSD FTLs, though they lack an efficient way to perform the cloning
operation.

### Efficient batch updates ###

The per-update overhead of Praaprixa is very much smaller than a Unix
system call.

<link rel="stylesheet" href="http://canonical.org/~kragen/style.css" />
