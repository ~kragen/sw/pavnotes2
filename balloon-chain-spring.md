I was thinking about absorbing shocks in a rope or cord without much
stretchiness, for example for a Dyneema hammock.
Rubber is the usual approach, but I calculated roughly
from some rough tests with a rubber band (12 N, measured by lifting a
1580-gram coke bottle on a scale) extends a 700mg band from 150mm to
1200mm before breaking it) that it can soak up about 9kJ/kg. This
means that a kJ impact, common for furniture, would require 110g or so
of rubber, which is a significant amount to carry around with you, and
about one or two orders of magnitude higher than the weight of a sling
or hammock sufficient to support a person once they're seated.

A different alternative is to try to use some kind of air spring,
because you don't have to carry air around with you.  I was thinking,
for example, of a string of near-spherical, atmospheric-pressure 10
balloons each of 100mm diameter, each made of a thin layer of
aluminized mylar (10 microns) within a bag made of flexible netting
of, say, 100-micron-diameter Dyneema.  Each strand can theoretically
withstand 24N of tensile force, so 256 strands with a total
cross-sectional area of 2.0 square millimeters should withstand 6kN.
Around the 314-mm circumference of the balloon you have 1.23mm between
strands and 2.45mm between netting knots, so the balloon is well
supported and unlikely to bulge out through the holes of the
netting. When the ends of the netting are pulled, the sphere deforms
into a spindle shape and the netting compresses the air inside the
balloon.  If the balloon were to collapse to zero volume, it would
extend to 157 mm, 57%; supposing that a third of this is reasonable,
we get 19 mm of elongation, or 190mm for the string of ten balloons.
Without trying to calculate the adiabatic compression of the air, 
this seems plausibly sufficient.

It's probably better to start with the balloons in a somewhat
spindle-like shape so that, when the netting pulls away from the
balloon on the ends, the unsupported balloon material there is
smaller and less likely to break.

Using spheres, though, each sphere is 314cm² of mylar, for a total of
3140cm² for all ten.  With 10 microns this gives us 3.14mℓ of mylar,
which I think is about 5g.  This is a great improvement over the
hypothetical 110g of rubber.  And the spindle balloons should be only
slightly heavier.

Without any safety factor, 119kg weighs 1170N which is, at 3GPa
tensile strength, an 0.7mm diameter cable of Dyneema; a 3-meter-long
hammock with this cross-sectional area would be 1.2mℓ and thus 1.2g.
With the 2.0mm circular section diameter posited earlier, it would be
9.4mℓ and thus 9.4g and almost 8× as strong.  That's probably enough
to support a person's weight safely if balloons or something are
absorbing the shocks.

This could allow you to carry around a 15-gram hammock in your wallet
to sleep on; it would weigh less than a cigarette lighter, USB cable,
or six condoms (see file `packing-list.md`).
