I was thinking about adding more language bindings to Yeso.  (See also
file `yeso-vnc.md` for thoughts about backends.)

Looking at <https://www.tiobe.com/tiobe-index/> the top 10 languages
this month are Python, C, Java, C++, C#, VB.NET, JS, PHP, SQL, and
(astoundingly) assembly.  The next 10 are Delphi, Golang, Scratch,
Swift, MATLAB, R, Rust, Ruby, Fortran, and old Visual Basic.  Perl is
down at #24, Objective-C is at #25, Lua at #29, Julia #30, D #32,
Kotlin #33, Haskell #36, Scheme #38, Prolog #43, ML #45, the Bourne
shell #46 (and #49, which combined would boost it to #37), and
amusingly Forth at #47.

(Other sources of programming language popularity data are
[RedMonk](https://redmonk.com/sogrady/2022/03/28/language-rankings-1-22/),
[the Octoverse
report](https://octoverse.github.com/2022/top-programming-languages),
and [GitHut's pull request
stats](https://madnight.github.io/githut/#/pull_requests/2023/1).
These do differ significantly in detail but agree in broad terms,
except for suggesting that Ruby is perhaps underrepresented.)

It's pretty easy to invoke C libraries from C++, assembly, D,
Objective-C, and I think Fortran.  Java involves wrestling with JNI,
which is a reasonable thing to do.  C# and VB.NET (and F#, etc.)
involve the .NET CIL.  Calling C from JS depends a lot on the
execution environment; node.js/npm has its set of tools for that kind
of thing that sort of assumes you live in npm.  Perl has Inline::C,
though XS is an older alternative.

C# and VB.NET
-------------

[Calling C libraries on Mono with the CIL seems to be pretty
easy](https://www.mono-project.com/docs/advanced/pinvoke/); here's
[James deBoer's
example](http://www.huronbox.com/~james/techdemos/cs-callback.html),
consisting of t.cs:

    using System;
    using System.Runtime.InteropServices;

    public delegate string CallBack();

    public class T {

        [DllImport("t")]
        public static extern void set_cb(CallBack x);

        [DllImport("t")]
        public static extern void call_cb();

        public static void Main()
        {
            CallBack mC = new CallBack(T.Ashley);
            set_cb(mC);
            call_cb();
        }

        public static string Ashley()
        {
            System.Console.WriteLine("Ashley called.");
            return "Ha, you wish";
        }
    }

And t.c, which gets compiled into t.so to provide `call_cb` and
`set_cb`:

    #include <stdio.h>

    static const char*(*func)(void);

    void set_cb(const char*(*f)(void))
    {
        func = f;
    }

    void call_cb()
    {
        printf("%s\n", (*func)());
    }

I was able to build and run this with (corrected from deBoer's
command):

    gcc -shared -o libt.so t.c
    mcs t.cs
    LD_LIBRARY_PATH=. mono t.exe

This does indeed produce:

    Ashley called.
    Ha, you wish

There are of course some complexities in the handling of pointers and
their lifetimes, which is explained in the Mono documentation.  Yeso's
`yw_open` function allocates a struct (of some size that varies
depending on the Yeso backend) behind the scenes and passes back a
`ywin`, which is a pointer to it.  It might be best to register a
finalizer that invokes `yw_close` if it hasn't been called already.

[Mono does seem to let you pass structs by
value](https://www.mono-project.com/docs/advanced/pinvoke/#structure-marshaling),
so most of the Yeso interface should be pretty simple.  There is a bit
more info in the [Mono embedding
documentation](http://docs.go-mono.com/index.aspx?link=xhtml%3Adeploy%2Fmono-api-embedding.html).

GForth
------

Although Forth is not really a practical programming language
nowadays, I have a certain affection for it, so I'm pleased to see
that [GForth has a C
FFI](https://www.complang.tuwien.ac.at/forth/gforth/Docs-html/C-Interface.html),
one which [permits including C header
files](https://www.complang.tuwien.ac.at/forth/gforth/Docs-html/Declaring-C-Functions.html),
which amusingly

> invokes GCC at run-time and uses dynamic linking

I like the idea of hacking together a graphical program in an
interactive Forth, although it seems like it would be hard to make it
work to modify the program in the Forth terminal window while it's
redrawing.

Unfortunately:

> Passing and returning structs or unions is currently not supported
> by our interface.

Though you can kind of make it work for a particular C calling
convention.  For Yeso this is a big problem because just about
everything is a struct.

PHP
----

People love to hate PHP, but it's still the main programming language
for a *lot* of people.  It'd be cool to have a PHP-running program
that gives you web-server-like liveness: maybe it would embed SQLite3
for your persistent data and re-execute the PHP file it was running
whenever it changed, reinitializing its local variables for every
event.

PHP [supports runtime loading of so-called
extensions](https://www.php.net/manual/en/function.dl.php) with code
like `dl('sqlite.so')`.  This [involves using a script called
`phpize`](https://www.phpinternalsbook.com/php5/build_system/building_extensions.html#building-extensions-using-phpize
"CC BY-NC-SA") which is, on Ubuntu, part of the `php7.4-dev` package.
[A simple extension is four lines of config.m4 and 25 lines of
C](https://stackoverflow.com/a/32575493), which seems pretty
approachable.  [Zend has a more comprehensive
guide](https://www.zend.com/resources/php-extensions/building-and-installing-php-extension),
though they've broken previous links.
