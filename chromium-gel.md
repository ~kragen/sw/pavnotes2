With a variety of organic ionomers, such as polyacrylate or
carboxymethylcellulose, you can get gels (as opposed to sols) with
water contents of 96% and higher.  What happens if you electrodeposit
chromium and/or iron in such a gel, then heat the result?

A lot of these common gels are basically CH₂O; xanthan gum, for
example, is (C₃₅H₄₉O₂₉)ₙ; carboxymethylcellulose is a polymer of
glucose (C₆H₁₂O₆) with varying degrees of functionalization with
carboxymethyl groups (C₂H₂O₂); and polyacrylate is (C₃H₃O₂)ₙ.  CH₂O is
40.002% carbon by weight.  So a hydrogel that is 96% water and 4% CH₂O
is 1.6% carbon.

But, if you have a layer of such a gel on a surface, and you
electrodeposit chromium (7.15g/cc) or iron (7.874g/cc) in its pores,
the metal will be denser than the water it’s replacing.  4% by weight
of CH₂O in the hydrogel would be only about 0.5% by weight if the
water is replaced by electroplated metal.  This would be 0.2% carbon.

A really interesting question is to what extent the electrodeposited
metal crystals will tend to occupy the pore spaces of the gel, where
ions can move easily, or instead grow densely on the surface of the
electrode, pushing the gel out of the way.  Ways to encourage
interpenetration might include a cathode made of a fine metal mesh,
larger dielectric reinforcement mixed into the gel (perhaps fibers,
grains of phyllosilicates such as clays, or acicular crystals), and
doing the opposite of the usual practices for achieving a bright
finish.

Supposing you can get the metal to grow in the nanometer-sized pores
of the organic gel, embedding the organic in the metal, then, after
electrodeposition and possible other shaping operations such as ECM,
you can heat the metal object to drive out the hydrogen ([hydrogen
embrittlement doesn’t happen below 150°][0] and, though
[high-temperature hydrogen attack][2] can happen in steel above 400°,
there isn’t enough hydrogen here to convert all the carbon into
methane, even if it wasn’t more tightly bound to oxygen), convert the
oxygen either into water or metal oxides, and most importantly, form
metal carbides from the carbon and the metal by thermal diffusion.
Some of the carbon will be lost to the oxygen, but some will surely
form carbides, especially with chromium.

[0]: https://en.wikipedia.org/wiki/Hydrogen_embrittlement
[2]: http://localhost:8080/viewer#wikipedia_en_all_maxi_2023-04/A/High_temperature_hydrogen_attack

Nickel, cobalt, manganese, tungsten, vanadium, and molybdenum are
other metals that may be useful in strengthening the result and can be
electrodeposited in this way.  My understanding is that in some cases
you need one anode per metal ingredient in order to regulate their
relative dissolution rates and, in some cases, to get them to dissolve
in the correct oxidation states.  (Chromium normally has to be added
as chromate or dichromate salts instead.)  Titanium, zirconium and
hafnium might also be very desirable for some purposes, but I’m not
sure they can be electrodeposited, especially in an aqueous bath.
Silicon can easily be electrodeposited, but not from an aqueous bath,
and anyway its main use is to deoxidize steel and lower melting
points.

Though both iron and chromium form hard, refractory carbides, the
chromium electrodeposition processes are all acidic as far as I know,
while getting iron to electrodeposit requires a fairly basic (high-pH)
bath.  So you might not be able to plate them both at once.

Increasing the carbon content further, up to at least 20%, should be
fairly easy; as far as I know there’s no lower limit to how much water
can be absorbed in such a carbohydrate gel and still be mobile, but
maybe at some point the carbohydrates will salt out the electrolytes
you’re trying to electrodeposit.

Maybe it won’t work that way, since the ions we’re trying to keep
mobile are cations, and they should help to neutralize the cationic
moieties of the ionomers.  If that works against us instead of for us,
though, polyvinyl alcohol might be the cure.

Making the gel from a protein like gelatin would contribute nitrogen
as well as carbon, similar to cyanide case-hardening, just without the
cyanide.  (This is presumably why medieval blacksmiths used leather
for case hardening.)  Gelatin is about 30% glycine (NH₂CH₂COOH,
C₂H₅O₂N), 15% proline (C₅H₉O₂N), and 10% hydroxyproline (C₅H₉O₃N), so
it has on the order of a nitrogen for every 3 carbons.  The nitrides
of iron, vanadium, titanium, zirconium, chromium, and presumably
hafnium are hard, refractory, and fairly stable; tungsten nitride is
hard but decomposes in water.  I think molybdenum might also have a
strong nitride.  I don’t know of nitrides of nickel, cobalt, or
manganese.

In [plasma nitriding of stainless steels][1] temperatures at 420° or
below apparently avoid the formation of chromium nitride, forming
mostly iron nitride.  This is said to prevent corrosion, though
chromium nitride is also used as a coating material to provide
corrosion resistance, even on medical implants.

[1]: https://en.wikipedia.org/wiki/Nitriding#Plasma_nitriding
