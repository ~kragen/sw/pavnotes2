A well-known [introduction to fractal image compression][0] from 01994
says:

> Later in the [01980s] Michael Barnsley, a leading researcher from
> Georgia Tech, wrote the popular book Fractals Everywhere. The book
> presents the mathematics of Iterated Functions Systems (IFS), and
> proves a result known as the Collage Theorem. The Collage Theorem
> states what an Iterated Function System must be like in order to
> represent an image.
> 
> This presented an intriguing possibility. If, in the forward
> direction, fractal mathematics is good for generating natural
> looking images, then, in the reverse direction, could it not serve
> to compress images? Going from a given image to an Iterated Function
> System that can generate the original (or at least closely resemble
> it), is known as the inverse problem. This problem remains unsolved.
> 
> Barnsley, however, armed with his Collage Theorem, thought he had it
> solved. He applied for and was granted a software patent and left
> academia to found Iterated Systems Incorporated (US patent
> 4,941,193. Alan Sloan is the co-grantee of the patent and co-founder
> of Iterated Systems.) Barnsley announced his success to the world in
> the January 1988 issue of BYTE magazine. This article did not
> address the inverse problem but it did exhibit several images
> purportedly compressed in excess of 10,000:1. Alas, it was a [sleight]
> of hand. The images were given suggestive names such as “Black
> Forest” and “Monterey Coast” and “Bolivian Girl” but they were all
> manually constructed. Barnsley’s patent has come to be derisively
> referred to as the “graduate student algorithm.”
> 
> Graduate Student Algorithm
> --------------------------
>
> - Acquire a graduate student.
> - Give the student a picture.
> - And a room with a graphics workstation.
> - Lock the door.
> - Wait until the student has reverse engineered the picture.
> - Open the door.
> 
> Attempts to automate this process have continued to this day, but
> the situation remains bleak. As Barnsley admitted in 1988: “Complex
> color images require about 100 hours each to encode and 30 minutes
> to decode on the Masscomp [dual processor workstation].” That's 100
> hours with a _person_ guiding the process.

[0]: http://www.compression.ru/arctest/descript/fractal.htm "Fractal Image Compression: What’s it all About?"

Masscomp?
---------

The Masscomp workstations [might have been 68020s][1]:

> Masscomp recently introduced two 68020-based computers for
> scientific and laboratory applications.  The systems, called the
> Scientific Laboratory Station-01 and -02, run entry-level color
> graphics for two users, (...)both include a 16.7 MHz 68020, 4
> megabytes of RAM, (...) The SLS-02 has a high-speed
> analog-to-digital converter and includes Lightning, Masscomp’s
> proprietary floating-point math accelerator.

[1]: https://books.google.com.ar/books?id=bDwEAAAAMBAJ&pg=PA33&lpg=PA33&dq=masscomp+dual+processor+workstation+68020 "Infoworld, 01986-12-15 issue, article 'Company unveils 2 computers for scientific, lab applications'"

A [Masscomp 5600/5700 with a 16.7-MHz 68020 is rated at 2.7 Dhrystone
MIPS][2].  The [Concurrent/Masscomp 5600 had a dual-68020
configuration with “4MB” of memory][4] tested for defense applications
in 01990.

[A core of the quad-core Raspberry Pi 3 is 3536 Dhrystone MIPS][3],
1300 times faster, so 100 hours of dual-processor computing is now
maybe a minute, or 12 seconds on one core of an i7 or similar.  But
how do you fit 100 hours of grad student grinding into 2 minutes?

[2]: https://netlib.org/performance/html/dhrystone.data.col0.html
[3]: http://www.roylongbottom.org.uk/dhrystone%20results.htm#anchorAndroid
[4]: https://apps.dtic.mil/sti/tr/pdf/ADA223415.pdf "AD-A223 415, AVF Control Number: NIST89CSC545_1.10, Ada COMPILER VALIDATION SUMMARY REPORT"

GPT-4 Algorithm
---------------

I feel like this is the kind of problem artificial neural networks
would be good at.  You could get some distance on the fractal
compression problem just by applying today’s optimizers directly to
it: Adam with automatic differentiation, stuff like that.  But also
you ought to be able to also use some of the crazy neural net stuff to
run an IFS, "look at" the resulting image, adjust some parameters, and
repeat.
