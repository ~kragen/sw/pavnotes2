Could you make an 8080-sized CPU that would be good at running C or
Smalltalk?  I think some inspiration from the PDP-8 might help.

Background
----------

There were a few blunders in the 8080 design, launched in 01974.  The
interrupt system ate up 1/32 of the opcode space, wasted 32 bytes at
the beginning of memory, and made interrupts slow.  The little-used
conditional call and return instructions were redundant with the
conditional jumps.  The conditional jumps took up 3 bytes instead of 2
because the address field was 2 bytes instead of 1.  There was no
practical way to do position-independent code, and despite a hardware
stack, re-entrant procedures were expensive.

Part of this was that the 8080 only had 4500 transistors, barely more
than the 3500-transistor 8008, so additional adders to provide the
base+offset addressing modes normally used for PIC would have been a
significant cost.  I think a 16-bit adder is about 360 transistors in
CMOS and similar in the 8080’s NMOS.

I think it’s possible to do substantially better in substantially less
transistors.  The MuP21 in 01994 was 7000 transistors including an
NTSC-generation coprocessor and a 21-bit-wide internal register stack,
running at 20 MHz and about 20 MIPS, similar to 10 MIPS with a more
conventional design.  But compiling C for it was miserable, as was
compiling C for, for example, the 6502.  What would a 6502-sized chip
that was *good* for C or Golang look like?

The PDP-8
---------

However, the PDP-8 (launched in 01965) *did* do PIC, and the Intersil
6100 implemented a PDP-8 on a chip in 4000 CMOS transistors, less than
the 8080.  (The original PDP-8 had only about 1400 transistors, but
many thousands of diodes and resistors.)  Its addresses and
instructions were 12 bits; the instruction word included a 7-bit
address field and a zero-page bit.  If the zero-page bit was set, the
high 5 bits of the effective address were 0; otherwise they were the
high 5 bits of the PC.  Consequently instructions could directly refer
to the 128 words on the zero page and the 128 words on the page where
they were executing, and a page of code could therefore be installed
anywhere without affecting its internal references.  There was also an
“indirect bit” which used the word at the given address as a
12-bit-wide pointer to the operand instead of itself being the
operand.

I’m not a fan of the indirect-bit approach, because I like
single-cycle instructions.  And some other aspects of the PDP-8, like
being non-reentrant by default, are distasteful.  But I think the
independent-pages approach of the PDP-8 has merit, and it requires a
lot less transistors than indexed-offset addressing.  On an 8-bit
processor the obvious approach would be to make a page 256 bytes
rather than 128 12-bit words.

I’d like to separate instructions from data, though, because I’d like
to use the same instructions for multiple different pieces of data.
The PDP-8 had two 3-bit registers, “instruction field” (originally
“program field”) and “data field”, which added extra high-order bits
to addresses, making a 15-bit address bus and potentially putting
instructions and data on separate physical memory pages, but that
approach is very limited.

The 6502
--------

The 6502 (launched in 01975, the year after the 8080) has [3510
enhancement-mode NMOS transistors and 1018 depletion-mode pullup
transistors][6], so arguably it slightly more complex than the PDP-8,
on par with the 8080.  (An NMOS NAND or NOR is two enhancement-mode
transistors plus a pullup, so 3 transistors rather than CMOS’s 4.)  It
is about three times faster than the 8080 at a given clock speed, in
part because it avoided some of the blunders mentioned above.  [Its
conditional jumps, BCC, BCS, BEQ, BMI, BNE, BPL, BVC, and BVS][2],
were only 2 bytes long, while its two JMP instructions (direct and
indirect) are 3 bytes long.  Unlike the PDP-8, [it actually did do a
full 16-bit addition with the branch offset,][3] which must have
consumed a significant fraction of its total transistor budget.  Its
memory space is usually considered to have consisted of 256 pages of
256 byte each.

[2]: https://www.masswerk.at/6502/6502_instruction_set.html#BCC
[3]: http://www.6502.org/tutorials/6502opcodes.html#PC
[6]: http://www.righto.com/2013/01/a-small-part-of-6502-chip-explained.html

Like the PDP-8, the 6502 had rapid instructions for accessing its zero
page, and even had base+index addressing modes, including two
PDP-8-like indirect addressing modes which supported indexing with its
two 8-bit index registers.  Its instructions were variable length,
requiring a minimum of 2 clock cycles, 3 if accessing memory.

Its internal registers are very sparse: a 16-bit program counter, four
8-bit registers (the accumulator A, the index registers X and Y, and
the stack pointer) and some condition-code bits.  We can think of the
zero page as being a 256-entry general-purpose 8-bit register file,
though it also be indexed by a register, unlike typical register
files.  [In 02021, Bill Mensch said while being interviewed by Stephen
Cass][7]:

> Rod and I already knew what was successful in an instruction
> set. And lower cost was key. So we looked at what instructions we
> really needed. And we figured out how to have addressable registers
> by using zero page [the first 256 bytes in RAM]. So you can have one
> byte for the op code and one byte for the address, and [the code is
> compact and fast]. There are limitations, but compared to other
> processors, zero page was a big deal.

[7]: https://spectrum.ieee.org/q-a-with-co-creator-of-the-6502-processor

A transistor-by-transistor reverse-engineering and simulation of the
6502 [has been published][5].  PragmatIC [has made a flexible
thin-film version of the processor in 02021][8] as a demo of their
thin-film semiconductor fabrication technology, though their press
release was careful not to say how fast it runs.

[5]: http://www.visual6502.org/
[8]: https://www.pragmaticsemi.com/news/pragmatic-semiconductor-re-invents-the-iconic-processor-that-changed-the-world

Unlike the PDP-8, 6502 procedures could easily be made reentrant,
because its call and return instructions used a stack, in a dedicated
stack page with an 8-bit stack pointer.

However, the 6502 had shitty code density, and it is not well suited
for C.

A promising approach
--------------------

You could have a “self pointer register” which points at a page of
read/write data, a program counter which implicitly points at a page
of read-only data, an on-chip operand stack of between 2 and 10 items,
a “far pointer register” or two which can be loaded from the operand
stack and used for off-page accesses, and an in-memory call stack,
used by push, pop, call, and return instructions.  The methods for a
single class can normally be packed into the same page so they can
call each other easily, and their local variables can be allocated
space in the “self” page, just like the object instance variables.  In
cases where a method may be called recursively, it can be compiled
specially to save its local variables on entry and restore them on
exit.

This suggests the following set of memory access instructions:

1. Store via far pointer
2. Load via far pointer
3. Retarget far pointer
4. Load from self page (takes index)
5. Store to self page (takes index)
6. Load from class page (takes index)
7. Load from global (zero) page (takes index)
8. Store to global page (takes index)
9. Call via far pointer
10. Return
11. Call on class page
12. Set self page pointer
13. Push on return stack
14. Pop from return stack
15. Push sign-extended immediate constant (takes “index”)

It would be highly desirable for the load and store instructions to
usually be a single byte even when they take an index.  If they
contain a 3-bit index field in the instruction byte, they could index
up to 8 positions with it: bits 0, 1, and 2 of the address would come
from the instruction, bits 3 to 7 would be 0, and bits 8 and up would
come from the relevant base pointer (PC, self, or zero).

Prefix instructions
-------------------

As an alternative to having longer immediates follow the instruction
to permit a full 256-byte page, you could have a *prefix* instruction
which provides bits 3 to 7 in a 5-bit field.  Suppose the prefix
opcode is 010; then the instruction byte 010 11011 would store “11011”
in a “prefix register” which would be used to supply bits 3 to 7 of
the effective address computed by the following instruction.  So if
the next instruction’s operand field is “001”, for example, the low 8
bit of the effective address would be 11011 001 rather than 00000 001
as it normally would be.  This might help to permit executing a
consistent one instruction per clock cycle or per two clock cycles.

In-memory stack windows
-----------------------

The SPARC, inspired by Berkeley RISC, uses on-chip “register windows”
for procedure call and return.  Its large 32-bit instructions have,
usually, three 5-bit register fields, indexing 32 available registers:
8 “global” registers shared across the whole program (like normal
registers), 8 “input” registers shared with the caller, 8 “output”
registers shared with any callees, and 8 “local” registers not shared
with either.  Upon procedure call, an internal stack pointer called
the “current window pointer” advanced by 16 registers; upon procedure
return it retracted by 16 registers.  Before advancing to a full slot,
an exception handler was invoked to spill that slot to RAM, and before
retracting to an empty slot, another exception handler was invoked to
reload it from RAM.

This mechanism makes most procedures automatically reentrant without
running any instructions to save and restore registers on most
procedure entries and exits and without indexing local-variable
accesses off a stack pointer.  In modern architectures it’s mostly
been obsoleted by a split instruction and data cache, superscalar
execution, and better compiler optimization.

[John Bayko writes of the TMS 9900][4]:

> It had a 15 bit address space and two internal 16 bit registers. One
> unique feature, though, was that all user registers were actually
> kept in memory - this included stack pointers and the program
> counter. A single workspace register pointed to the 16 register set
> in RAM, so when a subroutine was entered or an interrupt was
> processed, only the single workspace register had to be changed -
> unlike some CPUs which required a dozen or more register saves
> before acknowledging a context switch.
> 
> This was feasible at the time because RAM was often faster than the
> CPUs. A few modern designs, such as the INMOS Transputers, use this
> same design using caches or rotating buffers, for the same reason of
> improved context switches. Other chips of the time, such as the 650x
> series had a similar philosophy, using index registers, but the TMS
> 9900 went the farthest in this direction. Later versions added a
> write-through register buffer/cache.

[4]: https://www.cl.cam.ac.uk/teaching/2006/CompArch/documents/all/trends/cpu_history.html#9900

Somewhat analogously, the 65816 16-bit update of the 6502, used in the
Apple iiGS and the Super Nintendo, had a zero-page offset register and
a stack-pointer-relative ALU register; so, if we think of the 6502
zero page as being the general-purpose register file, the 65816 was
doing the same thing as the TMS 9900.  Except that SP, PC, A, X, and Y
weren’t in the zero page.

Suppose that we want to optimize call and return in a SPARC-like way
while using as few transistors as possible.  A possible way to do this
is to put register windows in RAM, replacing the instructions to push
and pop from the return stack with index-containing fetch and store
instructions which index off the stack pointer.

We’re talking about very small stack frames here, maybe 8 or 16 items
of 8 or 16 bits.  Suppose they’re 16 registers of 8 bits, 8 shared
with the caller and 8 shared with any callees.  Then 3 bits of the
byte address come directly from the index field in the instruction and
one bit is added to the low bit of the frame pointer register to get
the next few bits of the effective address.  An 8-bit stack pointer
register would then permit a stack segment of up to 255 call levels
and 2048 bytes, probably excessive for an 8-bit processor, and the
conditional increment logic to form the effective address would
require about 200 transistors in CMOS.

The stack pointer could have additional bits that maybe just didn’t
participate in the summing so that the stack wouldn’t have to be
hard-wired to a particular place in memory like it was on the 6502.
This would be very useful for multitasking.

So you might have, say, a 13-bit stack pointer register.  The call
instruction would store the program counter in the stack frame,
increment the stack frame pointer, and set the program counter to the
call destination; the return instruction would decrement the stack
frame pointer and load the program counter from memory.

This mechanism would take a lot of the pressure off the self page and
operand stack and automatically make subroutines re-entrant.  It would
use significantly more memory for the stack than a normal 8-bit
computer, but not outrageously so; a simple call and return requires 2
bytes, and this might increase that to 4 or 8.

Transport-triggered architecture
--------------------------------

In Dernocua in file `veskeno-exploration.md` and file
`c-stack-bytecode.md` I explored what kinds of operations programs
consist of: mostly loops, conditionals, assignments, comparisons for
equality, call, return, constants, memory navigation (in machine code,
this is pointer dereferences, constant offsets from base pointers, and
array indexing, while in Lisp it’s car, cons, and cdr, in Python it’s
getattr and setattr, and in Perl it’s hash indexing and list
construction), and the occasional addition or subtraction, and even
more rarely multiplication, bitwise, or bit-shift operations.  For
loops that count up by 1 toward a limit are a particularly common case
of arithmetic.

It might make sense to omit subtraction, multiplication, bitwise
operations, bit-shift operations, etc., from the main instruction set,
instead mapping them as I/O devices --- either in the usual memory
map, for simplicity of silicon, or in a separate I/O port space, for
efficiency of emulation.
