Shannon finds that a random code is close to perfection; there just
isn’t a good decoding algorithm.  Can we make a close enough
approximation of a random code by multiplying sparse or small binary
matrices?

The Fast Hadamard Transform can be thought of as deriving the
Walsh–Hadamard matrix by multiplying a bunch of sparse matrices.  I
think we can do this either in ℤ/2ℤ, using AND and XOR, or in ±1, with
-1 corresponding to 1 in ℤ/2ℤ so that multiplication corresponds to
XOR.  The 2*ⁿ* rows of the matrix (considered in ±1) are perfectly
uncorrelated.

The Lempel–Cohn algorithm for reducing an LFSR M-sequence circulant
matrix to the Hadamard matrix (see file `m-sequence-transforms.md`)
relies on a similar factorization.  The
rows of the circulant matrix are almost perfectly uncorrelated; the
dot product of any two distinct rows, considered in ±1, is -1.  This
gives us a code linear in ℤ/2ℤ that is still almost perfect.

If you multiply an *n*×*m* matrix A by an *m*×*p* matrix B, the
product AB has size *n*×*p*.  If *m* is much smaller than *n* or *p*,
then multiplying a vector through this large matrix can be
accomplished much more efficiently by multiplying it through the two
matrices individually.  What if *n* is the number of bits in our code
and *p* is the larger number of possible messages?  By multiplying a
one-hot representation of our message through the matrix, we select
one of the bitvectors from the non-materialized product matrix.  Can
we get those bitvectors to be a good code?  Does this give us a
speedup?  Not usefully, I think.

Consider the case *p* = 1024.  The minimal possible *n* would be 10,
but that wouldn’t give us any redundancy.  Say *n* is 20 or so.  What
could we make *m*?  Probably not much smaller than 20 itself.  Maybe
10 or 5.  But that’s not a significant speedup; multiplying through a
5×1024 matrix is only four times faster than multiplying through a
20×1024 matrix.

Potentially more useful would be if we could use a regular binary
encoding for messages, say *p* = 10, *n* = 20.  But then there’s no
real benefit to finding a small factorization.  The place where a
small factorization would help is if *p* and *n* are each fairly
large, like 64 or much more, and *m* can be smaller, like 8.  But that
only allows you to *encode* the message efficiently; it is of no help
in decoding it.
