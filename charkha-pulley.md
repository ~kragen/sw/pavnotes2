In Hindi the word for "spinning wheel" is "charkha", but the
traditional Indian spinning wheel is structured a little differently
from the European type.  Both types have a large pulley which the
spinner spins, which uses an endless belt (usually of thread) to turn
a small pulley at a much higher speed.  But in the European type the
large
