(All code in this note is untested.)

ARM32 has load-multiple and store-multiple instructions which, in both
ARM state and Thumb state, load a sequence of registers from RAM or
store them to RAM, for example to save and restore context on entry
and exit to a subroutine.  You can also use them, for example, for a
block copy, or a slightly modified block copy.  They always store the
registers in address-ascending order.  So, for example, you can copy 9
words from the address in r9 to the address in r10 thus:

    ldmia r9!,  {r0, r1, r2, r3, r4, r5, r6, r7, r8}
    stmia r10!, {r0, r1, r2, r3, r4, r5, r6, r7, r8}

On most microcontrollers this will theoretically take 20 clock cycles
if accessing zero-wait-state memory, which is 1.8 bytes per cycle,
which is pretty decent, 90% of the memory bus’s bandwidth.  It updates
r9 and r10 to point past the end of the copied block.

Due to what is probably a design error in a project I’m working on at
the moment, it occurred to me to wonder about how to copy a sequence
of words *in reverse* from one place to another.  In Thumb-2 and ARM,
there are `stmdb` and `ldmdb` that count downwards instead of up, but
you can’t simply `ldmia` them from one place and `stmdb` them into
another, because `stmdb` and `ldmdb` still store the lowest-numbered
registers in the lowest-numbered memory locations.  The
straightforward approach is to load them all at once and then store
them one at a time:

    ldmia r9!,  {r0, r1, r2, r3, r4, r5, r6, r7, r8}
    stmia r10!, {r8}
    stmia r10!, {r7}
    stmia r10!, {r6}
    stmia r10!, {r5}
    stmia r10!, {r4}
    stmia r10!, {r3}
    stmia r10!, {r2}
    stmia r10!, {r1}
    stmia r10!, {r0}

On most ARM hardware, though, this is significantly slower: 28 cycles
instead of 20, under the same assumptions.

An interesting variation of the simple block-copy code above is that
you can transpose a 3×3 matrix of words fairly efficiently:

    ldmia r9!,  {r0, r1, r2, r3, r4, r5, r6, r7, r8}
    stmia r10!, {r0, r3, r6}
    stmia r10!, {r1, r4, r7}
    stmia r10!, {r2, r5, r8}

This converts the matrix from row-major order to column-major order in
(theoretically) 22 clock cycles, 1.64 bytes per cycle; the transformation is self-inverse in the sense that if we transform the output data again in the same way we get the original input.

This code can just as easily reverse the order of the output rows (or
input columns):

    ldmia r9!,  {r0, r1, r2, r3, r4, r5, r6, r7, r8}
    stmia r10!, {r2, r5, r8}
    stmia r10!, {r1, r4, r7}
    stmia r10!, {r0, r3, r6}

Now, suppose we compose this row-reversing transformation with itself:

    ldmia r9!,  {r0, r1, r2, r3, r4, r5, r6, r7, r8}
    stmia r10!, {r2, r5, r8}
    stmia r10!, {r1, r4, r7}
    stmia r10!, {r0, r3, r6}
    subs  r10, #36
    ldmia r10!, {r0, r1, r2, r3, r4, r5, r6, r7, r8}
    stmia r11!, {r2, r5, r8}
    stmia r11!, {r1, r4, r7}
    stmia r11!, {r0, r3, r6}

We have reproduced the input, except with both the rows and the
columns in reversed order.  Which is to say, the input is in reverse
order.

However, this does twice as many memory references, 36 instead of 18,
so it actually takes even longer than the straightforward approach: 45
cycles instead of 28, under the same speed assumptions.  It’s one
instruction shorter, and I think two bytes shorter in Thumb/Thumb-2,
but that probably doesn’t matter.

There might be some system where treating an array as a matrix and
doing two such reversing transposes is a better way to reverse it, but
I don't think the machine code of any ARM32 chip is such a system.
