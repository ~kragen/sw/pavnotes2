To make [small tenorite particles][0], which [kill cells][1] (though
metallic copper is probably better at that), I propose this super
ghetto process:

[0]: doi:10.1016/j.ijantimicag.2008.12.004 "Characterisation of copper oxide nanoparticles for antimicrobial applications"
[1]: doi:10.1021/tx800064j "Copper Nanoparticles are Highly Toxic [in human epithelial lung cell line A549]"

1. Electrolytically dissolve copper in a chloride electrolyte such as
   sodium chloride.
2. Concentrate the copper chloride, for example by recrystallization,
   and make a saturated solution.
3. Mix with a saturated sodium carbonate solution in a blender set to
   “brutal” with both solutions at [near boiling][2].
4. Decant the resulting fine verditer particles, recycling the
   recovered sodium chloride.
5. Ball-mill the wet verditer paste at 70° in the presence of a
   calcium chloride desiccant to reduce it to a fine verditer powder.
6. Fluidize the bed of verditer powder with the minimum required
   amount of airflow, heated to 300° to decompose it to tenorite
   without risking agglomeration of particles (which would prevent
   them from being small), stirring to prevent clump formation.

[2]: https://www.researchgate.net/profile/Abdelkader-Bouaziz/post/Method_for_the_synthesis_of_colloidal_copper_oxide_nanoparticles/attachment/631a4c1b0c295f1f9ad54b16/AS%3A11431281083622667%401662667803254/download/CuO+1.pdf "A brief review on synthesis and characterization of copper oxide nanoparticles and its applications, amazingly shitty paper by Singh, Kaur, and Rawat"

I think the size of the verditer particles formed in step 3 will
determine the size of the tenorite particles you end up with, and
these can easily be in the submicron range.

This is, I think:

> Cu + 2NaCl + 2H₂O → CuCl₂ + 2NaOH + H₂  
> 2CuCl₂ + 2Na₂CO₃ + H₂O → Cu₂CO₃(OH)₂ + 4NaCl + CO₂  
> Cu₂CO₃(OH)₂ → 2CuO + H₂O + CO₂  

Or, in summary:

> 2Cu + 4H₂O + 2Na₂CO₃ → 2CuO + 2CO₂ + 4NaOH + 2H₂

(Note that the chloride doesn't appear; probably acetate, sulfate,
chlorate, perchlorate, or nitrate would work just as well.)

The four products are each separately useful and are emitted at
different steps of the process or in different states of matter, so
they can be easily separated.  Except for the tenorite itself, they
are all nontoxic.

NaOH has a tendency to rapidly absorb CO₂, even from the air, which I
think is:

> 2NaOH + CO₂ → Na₂CO₃ + H₂O

If we recycle the CO₂ and NaOH as well, the overall process equation becomes:

> 2Cu + 2H₂O → 2CuO + 2H₂

However, Na₂CO₃ is readily available and easy to handle, and the NaOH
could be useful for other purposes.

If the chloride electrolyte is instead muriate of lime or magnesia,
its concentration can be much higher, and step 2 should be much
easier, because the resulting hydroxide will have very low solubility.
You might still lose a little copper to copper hydroxide.

A possible alternative for step 2 is to precipitate the copper out of
the solution by raising the pH, then wash the precipitated (but
coarse) copper hydroxide to eliminate the soluble residues from the
first step; then redissolve it with a mild acid such as vinegar or
something.

It'd be nice to be able to just sonicate the small particles to break
up any agglomerations, but I don't have a super ghetto sonicator, and
the particles have to be wet to sonicate them, dry particles are the
objective, and drying wet particles would probably just stick them
back together.  Unless you use supercritical drying, which, again, not
super ghetto.

Lanje, Sharma, Pode, and Ningthoujam (“Advances in Applied Science
Research, 2010, 1 (2): 36-40”) report an even simpler synthesis of 5-6
nanometer particles with copper acetate, acetic acid, and lye:

> Aqueous solution of copper acetate (0.02 mol) is prepared in round
> bottom flask. 1 ml glacial acetic acid is added to above aqueous
> solution and heated to 100°C with constant stirring. About 0.4 g of
> NaOH is added to above heated solution till pH reaches to 6-7. The
> large amount of black precipitate is formed immediately. It is
> centrifuged and washed 3-4 times with deionized water. The obtained
> precipitate was dried in air for 24 h.

I would have expected blue copper hydroxide from that preparation, not
tenorite.
