OMeta is an extension of PEGs to pattern-match on trees rather than on
strings.  It occurred to me just now that PEG pattern-matching on
strings is actually enough, as long as none of your patterns can match
unbalanced parentheses; you can simply apply PEG patterns to an
S-expression stringification of an operation tree in order to perform
instruction selection.

Moreover, for [Cattell’s simple top-down maximal munch instruction
selection][0], PEG ordered choice can be pressed into service simply
by putting the “more maximal” patterns earlier in the list of choices.

[0]: https://apps.dtic.mil/sti/tr/pdf/ADA058872.pdf

Consider the S-expression

    (set (index (local a) (constant 3)) (+ (local x) (constant 1)))

representing a source-code line like

    a[3] = x + 1;

The general pattern for handling `(+ <rval-1> <rval-2>)` as an rvalue
might invoke an `add` instruction.  But, for a CPU like RISC-V, we’d
like to compile `x + 1` to an instruction like `addi t1, t1, 1` rather
than a sequence like

    li t2, 1
    add t1, t1, t2

We can do this simply by having patterns for `(+ <const> <rval>)` and
`(+ <rval> <const>)` which are earlier in the list of PEG alternatives
than the generic `(+ <rval 1> <rval 2>)` pattern:

    <rval> = ...
        | "(" "+" <const> <rval> ")" -> { addi2($rval, $const) }
        | "(" "+" <rval> <const> ")" -> { addi2($rval, $const) }
        | "(" "+" <rval-1> <rval-2> ")" -> { add($rval-1, $rval-2) }
    ...

Similarly, if we have a store instruction with an immediate offset
field, we might have a pattern for `(set (index (local <var>) <const>)
<rval>)` that comes earlier than, and therefore takes precedence over,
the generic `(set <lval> <rval>)` pattern.

I want to emphasize that I’m talking about *literal string parsing*
here.  Doing the pattern-matching on trees would surely be more
efficient, but as long as none of the productions ultimately invoked
such as `<const>` or `<rval>` is capable of consuming an unbalanced
set of parentheses, it isn’t strictly necessary.

Maximal munch doesn’t always produce an optimum tiling of an operation
tree, but in most cases it’s close enough to produce acceptable code.

What’s the point of having an intermediate representation if it’s just
going to be a string anyway?  Well, it can save you a lot of
cross-product explosions where there is more than one way to generate
a single underlying operation (such as integer addition) and more than
one way to compile it.
