To get a minimal bootstrap compiler working on the Cortex-M4, what do
I need?

Well, at least prologue and epilogue.  These really just need to push
and pop lr, but to maintain EABI alignment they should also push and
pop something else.  And the arguments, too.

Okay, I have that working!  And now I also have the following working:

- conditional jumps;
- pushing small literals;
- pushing local variable values;
- setting local variable values;
- returning a value;
- addition.

Unsatisfactory current state
----------------------------

Currently the prototype compiler in C compiles the two-argument
function "\x10\x11\x01\x02\x10\x02\x11\x02", which means `my.0 my.1
bge.2f my.0 ret 2: my.1 ret`, to the following somewhat disappointing
21 instructions:

       0:   4628            mov     r0, r5             @ prologue, loading arg 0
       2:   3c04            subs    r4, #4
       4:   6825            ldr     r5, [r4, #0]
       6:   4629            mov     r1, r5             @ prologue, loading arg 1
       8:   3c04            subs    r4, #4
       a:   6825            ldr     r5, [r4, #0]
       c:   b507            push    {r0, r1, r2, lr}   @ end of prologue
       e:   c420            stmia   r4!, {r5}          @ my.0
      10:   9d00            ldr     r5, [sp, #0]
      12:   c420            stmia   r4!, {r5}          @ my.1
      14:   9d01            ldr     r5, [sp, #4]
      16:   3c04            subs    r4, #4             @ bge; note the stack
      18:   6823            ldr     r3, [r4, #0]       @   effect is wrong
      1a:   429d            cmp     r5, r3
      1c:   da02            bge.n   0x24
      1e:   c420            stmia   r4!, {r5}          @ my.0
      20:   9d00            ldr     r5, [sp, #0]
      22:   bd07            pop     {r0, r1, r2, pc}   @ ret
      24:   c420            stmia   r4!, {r5}          @ my.1
      26:   9d01            ldr     r5, [sp, #4]
      28:   bd07            pop     {r0, r1, r2, pc}   @ ret

Okay, I’ve now improved this to these 19 instructions, incidentally
reversing the order of the arguments:

       0:   f854 0d04       ldr.w   r0, [r4, #-4]!     @ pop first argument
       4:   b561            push    {r0, r5, r6, lr}   @ frame both arguments
       6:   f854 5d04       ldr.w   r5, [r4, #-4]!     @ pop second argument
       a:   c420            stmia   r4!, {r5}          @ my.0
       c:   9d00            ldr     r5, [sp, #0]
       e:   c420            stmia   r4!, {r5}          @ my.1
      10:   9d01            ldr     r5, [sp, #4]
      12:   f854 3d04       ldr.w   r3, [r4, #-4]!     @ bge
      16:   429d            cmp     r5, r3
      18:   f854 5d04       ldr.w   r5, [r4, #-4]!     @ (fixed stack effect)
      1c:   da03            bge.n   0x26
      1e:   c420            stmia   r4!, {r5}          @ my.0
      20:   9d00            ldr     r5, [sp, #0]
      22:   b002            add     sp, #8             @ ret
      24:   bd01            pop     {r0, pc}
      26:   c420            stmia   r4!, {r5}          @ my.1
      28:   9d01            ldr     r5, [sp, #4]
      2a:   b002            add     sp, #8             @ ret
      2c:   bd01            pop     {r0, pc}

Doing significantly better than that would take bigger changes, and
this is tolerable.  I mean it’s probably 4× slower than well-written
code for the platform but it’s surely good enough.  In fact, the
bytecode interpreter written in assembly was probably good enough, in
retrospect.

Grown/shrunk optimization
-------------------------

(Literally all of the code in this section is untested and some may
not even assemble.)

If we know the jump targets we could do a tiny bit of optimization
just by having *two* top-of-stack registers and ‘grown’ and ‘shrunk’
states to track whether the second one is full or empty.  Consider
this extended basic block, from [Jove
DFixMarks](./c-bytecode-notes.md):

    my.4 bz.9f my.4 field.3 setmy.4 my.4 field.0 my.5 bne.4f

This is my (possibly erroneous) translation of the beginning of this C loop:

            for (...; m != NULL; ...) {
                if (m->m_line == lp && ...

Suppose we start in ‘shrunk’.  Then `my.4`, instead of compiling to
the usual

        stmia r4!, {r5}
        ldr   r5, [sp, #16]

can transition to the ‘grown’ state and put the loaded value in r3:

        ldr r3, [sp, #16]

Then `bz.9f` in the ‘grown’ state transitions to the ‘shrunk’ state;
suppose the ‘shrunk’ state is the standard state for jump targets, in
which case it can just compile as

        cbz r3, 9f

The second `my.4` is again a single instruction (without further
optimization to remove the redundant load) and we are again ‘grown’:

        ldr r3, [sp, #16]

Then `field.3` invokes a load-word millicode routine specialized for
the ‘grown’ state, and stays in ‘grown’:

        add r3, #12
        bl.w paged_load_word_grown

Now `setmy.4` in the ‘grown’ state takes its argument from the second
TOS register and goes back to ‘shrunk’:

        str r3, [sp, #12]

Again `my.4` in ‘shrunk’ is

        ldr r3, [sp, #16]

which is slightly suboptimal in runtime, in that a better optimizer
would have avoided killing the previously loaded value of `my.4`, but
no different in space.  `field.0` then is just

        bl.w paged_load_word_grown

and now for the first time we have a `my` in ‘grown’ state, `my.5`,
and so for the first time we are doing more runtime work instead of
less:

        stmia r4!, {r5}
        mov r5, r3          @ this is the extra work
        ldr r3, [sp, #20]

Now our `bne.4f` is in ‘grown’, which is convenient for doing the
comparison before popping the stack back into ‘shrunk’ state:

        cmp r5, r3
        ldrdb r4!, {r5}
        bne 4f

So here’s our whole compilation of these 9 bytecodes (11 bytes):

        ldr r3, [sp, #16]            @ my.4
        cbz r3, 9f                   @ bz.9f
        ldr r3, [sp, #16]            @ my.4
        add r3, #12                  @ field.3
        bl.w paged_load_word_grown
        str r3, [sp, #12]            @ setmy.4
        ldr r3, [sp, #16]            @ my.4
        bl.w paged_load_word_grown   @ field.0
        stmia r4!, {r5}              @ my.5
        mov r5, r3
        ldr r3, [sp, #20]
        cmp r5, r3                   @ bne.4f
        ldmdb r4!, {r5}
        bne 4f

That’s 14 instructions and 7 memory references (all to the stacks,
since linear memory references are hidden in `paged_load_word_grown`)
for 21 clock cycles, 2.3 per bytecode, plus of course the 24 or so
more for virtual memory.

Contrast the only slightly dumber approach I’ve been thinking of so
far:

        stmia r4!, {r5}              @ my.4
        ldr r5, [sp, #16]
        tst r5, r5                   @ bz.9f
        ldmdb r4!, {r5}
        beq 9f
        stmia r4!, {r5}              @ my.4
        ldr r5, [sp, #16]
        add r5, #12                  @ field.3
        bl.w paged_load_word
        str r5, [sp, #12]            @ setmy.4
        ldmdb r4!, {r5}
        stmia r4!, {r5}              @ my.4
        ldr r5, [sp, #16]
        bl.w paged_load_word         @ field.0
        stmia r4!, {r5}              @ my.5 (note: shorter!)
        ldr r5, [sp, #20]
        ldmdb r4!, {r1, r3}          @ bne.4f
        cmp r3, r5
        mov r5, r1
        bne 4f

This is 20 instructions instead of 14, but also 13 memory references
instead of 7, for a total of 33 cycles, 3.7 cycles per bytecode.
That’s 61% longer execution time!  Probably more importantly, it’s 52
bytes assembled rather than 36, 44% more.  Fitting 44% more native
code into the native-code cache might be more important than it
running 61% faster.

It’s not really 61% longer execution time because both versions are
incurring about 24 cycles in `paged_load_word` or equivalent, so it’s
really 45 cycles versus 53, only 18% longer execution time.

Because this only adds 1 bit of state to the compiler, it can be
encoded in the compiler’s program counter, so it never has to be
explicitly tested; it just makes the compiler twice as big.  But my
estimate is that the compiler will be about 1.5K of native code, so
that’s okay.  This might actually make the compiler *faster*, because
it has to emit less output.  (Also, it will probably make any program
compiled with the compiler faster, including the compiler itself.)

What GCC generates for this C code is, unsurprisingly, enormously
better.  Largely because it allocates a register to the local `m`, it
gets 5 instructions including 1 memory reference for a total of 7
cycles.

    .L106:
    @ marks.c:243:                  if (m->m_line == lp
            ldr     r6, [r4]        @ m_48->m_line, m_48->m_line
            cmp     r6, r5  @ m_48->m_line, lp
            bne     .L102           @,
    @ ...
    .L102:
    @ ...
    @ marks.c:242:          for (m = curbuf->b_marks; m != NULL; m = m->m_next) {
            cmp     r4, #0  @ m
            bne     .L106   @

With this kind of optimization, the code given above for `my.0 my.1
bge.2f my.0 ret 2: my.1 ret` should look more like this, omitting the
prologue:

        ldr r3, [sp, #0]      @ my.0 -> grown
        stmia r4!, {r5}       @ my.1
        mov r5, r3
        ldr r3, [sp, #4]
        cmp r3, r5            @ bge -> shrunk
        ldmdb r4!, {r5}
        bge.n 1f
        ldr r3, [sp, #0]      @ my.0 -> grown
        stmia r4!, {r5}       @ back to shrunk for ret
        mov r5, r3
        pop {r0, r1, r2, pc}  @ ret requires shrunk
    1:  ldr r3, [sp, #4]      @ my.1
        stmia r4!, {r5}
        mov r5, r3
        pop {r0, r1, r2, pc}  @ ret

This isn’t actually better (still 15 instructions, 11 executed, and 12
memory references executed) but it isn’t worse either.

Local variable registers
------------------------

Putting a local variable on the return stack normally requires one
cycle on ARM to initialize it:

    push {r0, lr}                 @ 0-1 locals
    push {r0, r1, r2, lr}         @ 2-3 locals
    push {r0, r1, r2, r3, r6, lr} @ 4-5 locals

But then each access requires an extra cycle:

    ldr r5, [sp, #16]             @ read: 2 cycles
    str r5, [sp, #16]             @ write: 2 cycles
    
The way I’m doing it right now, it also requires an extra cycle to
remove it from the stack:

    pop  {r0, lr}                 @ 0-1 locals
    pop  {r0, r1, r2, lr}         @ 2-3 locals
    pop  {r0, r1, r2, r3, r6, lr} @ 4-5 locals: 7 cycles

That could be avoided:

    adds sp, #16                  @ discard 4 locals in 1 cycle
    pop {r6, lr}                  @ 3 cycles

If you were to put your local variable in r6 instead of [sp, #16],
three things would change:

- You would initialize r6 after pushing it instead of before, which
  wouldn’t change the runtime.
- Reads and writes would be one cycle instead of two.
- The possibility to not spend time restoring it before returning
  would not exist.

So if, for example, r6 and r7 were used as locals in this way, the
above grow/shrink code sketch would look like this:

        mov r3, r6                   @ my.4
        cbz r3, 9f                   @ bz.9f
        mov r3, r6                   @ my.4
        add r3, #12                  @ field.3
        bl.w paged_load_word_grown
        mov r6, r3                   @ setmy.4
        mov r3, r6                   @ my.4
        bl.w paged_load_word_grown   @ field.0
        stmia r4!, {r5}              @ my.5
        mov r5, r3
        mov r3, r7
        cmp r5, r3                   @ bne.4f
        ldmdb r4!, {r5}
        bne 4f

This is the same 14 instructions as before, but now only 2 memory
references instead of 7.

In this case, as I think in most cases, the locals that are frequently
accessed inside a loop are not arguments.  Loop counters are almost
never arguments.  Allocating registers to the first two arguments
would have sped the loop up a little, but allocating them to its
internal locals would help much more.

Doing this in a simple way requires a slight bytecode redesign:
instead of the N arguments being the *first* N of M locals, make them
the *last* N.  This way locals 0 and 1 always get assigned the same
registers independent of the rest of the function.  But for the time
being I’ll assume M-2 and M-1 get the registers.

A within-basic-block register allocator
---------------------------------------

This is getting dangerously close to a register allocator.  If instead
of a single bit of state to distinguish “grown” from “shrunk” we have
a mapping from stack positions to registers, we can implement most
local variable reads without any instructions:

        cbz r6, 9f                   @ my.4 bz.9f
        add r3, r6, #12              @ my.4 field.3
        bl.w paged_load_word_grown
        mov r6, r3                   @ setmy.4
        mov r3, r6                   @ my.4
        bl.w paged_load_word_grown   @ field.0
        cmp r3, r7                   @ my.5 bne.4f
        ldmdb r4!, {r5}
        bne 4f

This cuts those 14 instructions down to 9 and cuts the 2 memory
references down to 1.  I’m a little bit vague on exactly how this
handles the case where you have a local variable value deeply buried
on the (compile-time) stack and then you change the local variable.

This gives us potentially more stack levels in registers: r5, possibly
r3 on top of that, and then sometimes one or both of the local
variables.  The *compiler* maintains a stack of registers; it’s just
that there are never more than two temporaries in it, and never more
than four distinct values.  All operation outputs (fetches and ALU
ops) put their result in either r5 or r3.  At basic-block boundaries
we revert to the standard ‘shrunk’ state where top of stack is in r5
(costing us one `mov` at worst) and the rest of the stack is in RAM.

This may be overfitting to this particular basic block, so let’s look
at a different one.  Consider this loop from `png_build_8bit_table`
(see file `c-bytecode-notes.md`):

     1: lit.256 for.3
     4:     my.3 lit.255 alu.bitand  my.3 my.4 alu.add  storebyte
        next.4b

As it happens, my.3 (i) and my.4 (table) are in fact the only two
non-argument local variables.  With the dumbest approach we could
compile the loop block as something like this:

     4:  stmia r4!, {r5}              @ my.3
         ldr r5, [sp, #12]
         stmia r4!, {r5}              @ lit.255
         movs r5, #255
         ldmdb r4!, {r3}              @ alu.bitand
         and r5, r3
         stmia r4!, {r5}              @ my.3
         ldr r5, [sp, #12]
         stmia r4!, {r5}              @ my.4
         ldr r5, [sp, #16]
         ldmdb r4!, {r3}              @ alu.add
         add r5, r3
         bl.w paged_byte_store
         ldr r3, [sp, #12]            @ next.4b with loop variable my.3
         adds r3, #1
         cmp r3, r11                  @ compare to loop limit
         bne 4b

This is a basic block with 17 instructions and a whopping 10 memory
references, not counting what’s inside the millicode.  With the
grown/shrunk state it would probably look like this:

     4:  ldr r3, [sp, #12]            @ my.3 -> grown
         stmia r4!, {r5}              @ lit.255
         mov r5, r3
         movs r3, #255
         and r5, r3                   @ alu.bitand -> shrunk
         ldr r3, [sp, #12]            @ my.3 -> grown
         stmia r4!, {r5}              @ my.4
         mov r5, r3
         ldr r3, [sp, #16]
         add r5, r3                   @ alu.add -> shrunk
         bl.w paged_byte_store_shrunk
         ldr r3, [sp, #12]            @ next.4b with loop variable my.3
         adds r3, #1
         cmp r3, r11                  @ compare to loop limit
         bne 4b

Down to 15 instructions and 6 memory references, a small improvement.
If we add this ability to take operands directly from locals,
including a local not on top of the stack, this becomes:

    4:  movs r3, #255                 @ my.3 lit.255
        and  r3, r6                   @ alu.bitand, first arg from r6
        stmia r4!, {r5}               @ must free up r3
        mov  r5, r3
        add  r3, r6, r7               @ my.3 my.4 alu.add
        bl.w paged_byte_store_grown
        adds r6, #1
        cmp  r6, r11
        bne  4b

This is down to 9 instructions and 1 memory reference, which is just
over a third of the original, again discounting the cost of the
millicode.  In the middle it seemed like maybe it would be nice to use
r5 and r3 in any order for the top two stack items, but in this case
that would have been counterproductive: the arguments would have been
in the wrong registers for the millicode routine, unless we multiply
its versions further.

Calls
-----

Okay, the big missing pieces now are (a) calls and (b) paged memory.

### Expected compilation speed of 256 cycles per bytecode ###

I expect compilation to be fairly fast, so I can afford to compile a
lot, but I’d like to quantify that.  My prototype dumbjit.o at this
point is 2K of code and 600 bytes of data, so I don’t think the JIT
compiler itself will take up a lot of the system memory.  A lot of it
looks kind of like this:

    00000000 <assemble_halfword>:
       0:   6803            ldr     r3, [r0, #0]
       2:   1c5a            adds    r2, r3, #1
       4:   6002            str     r2, [r0, #0]
       6:   7019            strb    r1, [r3, #0]
       8:   6803            ldr     r3, [r0, #0]
       a:   0a09            lsrs    r1, r1, #8
       c:   1c5a            adds    r2, r3, #1
       e:   6002            str     r2, [r0, #0]
      10:   7019            strb    r1, [r3, #0]
      12:   4770            bx      lr

19 cycles.

    00000014 <assemble_pushn>:
      14:   f5a1 4198       sub.w   r1, r1, #19456  @ 0x4c00
      18:   b289            uxth    r1, r1
      1a:   f7ff bffe       b.w     0 <assemble_halfword>

    0000001e <assemble_popn>:
      1e:   f5a1 4188       sub.w   r1, r1, #17408  @ 0x4400
      22:   b289            uxth    r1, r1
      24:   f7ff bffe       b.w     0 <assemble_halfword>

    00000028 <assemble_movs8>:
      28:   f502 5200       add.w   r2, r2, #8192   @ 0x2000
      2c:   eb02 2101       add.w   r1, r2, r1, lsl #8
      30:   b289            uxth    r1, r1
      32:   f7ff bffe       b.w     0 <assemble_halfword>

7 cycles plus `assemble_halfword` for 26 in all.

...and down in the switch statement...

     220:   42b4            cmp     r4, r6                      @ end?
     222:   d316            bcc.n   252 <compile_bytecode+0x6e>

5 cycles...

     252:   68fa            ldr     r2, [r7, #12]         @ r7 is frame pointer?
     254:   f81b 9004       ldrb.w  r9, [fp, r4]          @ fetch bytecode
     258:   697b            ldr     r3, [r7, #20]
     25a:   f842 3024       str.w   r3, [r2, r4, lsl #2]  @ store native offset
     25e:   ea4f 1319       mov.w   r3, r9, lsr #4        @ drop high nibble
     262:   2b04            cmp     r3, #4
     264:   d86d            bhi.n   342 <compile_bytecode+0x15e>
     266:   e8df f003       tbb     [pc, r3]              @ switch on op type
     26a:   4003            .short  0x4003
     26c:   6c56            .short  0x6c56
     26e:   4b              .byte   0x4b
     26f:   00              .byte   0x00

15 more cycles...

     2c6:   3401            adds    r4, #1                      @ increment pc
     2c8:   e7aa            b.n     220 <compile_bytecode+0x3c>

5 more cycles... and let's look at a particular case.

     316:   f24c 4120       movw    r1, #50208      @ 0xc420
     31a:   4628            mov     r0, r5
     31c:   f7ff fffe       bl      0 <assemble_halfword>
     320:   f019 0f08       tst.w   r9, #8
     324:   d007            beq.n   336 <compile_bytecode+0x152>
     326:   ea6f 0209       mvn.w   r2, r9
     32a:   f002 020f       and.w   r2, r2, #15
     32e:   2105            movs    r1, #5
     330:   f7ff fffe       bl      11e <assemble_mvn8>
     334:   e7c7            b.n     2c6 <compile_bytecode+0xe2>

This is the negative-literal-pushing case, and it’s 13 cycles plus the
calls to `assemble_halfword` and `assemble_mvn8`.  It’s followed by
the positive-literal-pushing case:

     336:   f009 020f       and.w   r2, r9, #15
     33a:   2105            movs    r1, #5
     33c:   f7ff fffe       bl      28 <assemble_movs8>
     340:   e7c1            b.n     2c6 <compile_bytecode+0xe2>

The negative-literal-pushing case is 38 cycles in the switch loop plus
19 cycles in `assemble_halfword` and 26 in `assemble_mvn8`, for a
total of 83.  I don’t think this is too atypical.  Here’s the case for
the bytecode for setting a local variable:

     300:   f009 020f       and.w   r2, r9, #15
     304:   2105            movs    r1, #5
     306:   4628            mov     r0, r5
     308:   f7ff fffe       bl      dc <assemble_str_sp_rel>
     30c:   2220            movs    r2, #32
     30e:   2104            movs    r1, #4
     310:   f7ff fffe       bl      a6 <assemble_ldmdb>
     314:   e7d7            b.n     2c6 <compile_bytecode+0xe2>

That’s 17 cycles itself, and it invokes these:

    000000dc <assemble_str_sp_rel>:
      dc:   f5a2 42e0       sub.w   r2, r2, #28672  @ 0x7000
      e0:   eb02 2101       add.w   r1, r2, r1, lsl #8
      e4:   b289            uxth    r1, r1
      e6:   f7ff bffe       b.w     0 <assemble_halfword>

    000000a6 <assemble_ldmdb>:
      a6:   b538            push    {r3, r4, r5, lr}
      a8:   1e55            subs    r5, r2, #1               @ detect 1-register case
      aa:   4215            tst     r5, r2
      ac:   4614            mov     r4, r2
      ae:   d103            bne.n   b8 <assemble_ldmdb+0x12> @ skip tail-call
      b0:   e8bd 4038       ldmia.w sp!, {r3, r4, r5, lr}    @ tail call to alternative
      b4:   f7ff bffe       b.w     7e <assemble_ldmdb_1>
      b8:   f5a1 51b6       sub.w   r1, r1, #5824   @ 0x16c0
      bc:   3910            subs    r1, #16
      be:   b289            uxth    r1, r1
      c0:   f7ff fffe       bl      0 <assemble_halfword>
      c4:   b2a1            uxth    r1, r4
      c6:   e8bd 4038       ldmia.w sp!, {r3, r4, r5, lr}
      ca:   f7ff bffe       b.w     0 <assemble_halfword>

The 1-register case is 18 cycles and then invokes this:

    0000007e <assemble_ldmdb_1>:
      7e:   b538            push    {r3, r4, r5, lr}
      80:   4605            mov     r5, r0
      82:   4610            mov     r0, r2
      84:   f7ff fffe       bl      70 <count_trailing_zeroes>
      88:   f5a1 61f6       sub.w   r1, r1, #1968   @ 0x7b0
      8c:   4604            mov     r4, r0
      8e:   b289            uxth    r1, r1
      90:   4628            mov     r0, r5
      92:   f7ff fffe       bl      0 <assemble_halfword>
      96:   0321            lsls    r1, r4, #12
      98:   f601 5104       addw    r1, r1, #3332   @ 0xd04
      9c:   e8bd 4038       ldmia.w sp!, {r3, r4, r5, lr}
      a0:   b289            uxth    r1, r1
      a2:   f7ff bffe       b.w     0 <assemble_halfword>

Which is 22 cycles itself plus two calls to `assemble_halfword` (38
cycles) and one to `count_trailing_zeroes` which, ugh.  We’re at 95
cycles, plus the 25-cycle switch loop dispatch and
`count_trailing_zeroes`.

This could obviously be optimized a lot further, but I think even a
kind of badly optimized JIT compiler like this will take “only” on the
order of 128 cycles per bytecode.

### The native-code cache ###

So the idea is to have a fairly small, aggressively pruned cache of
compiled native code occupying some of the Zorzpad’s 64 KiB of TCM.  A
typical subroutine might be 16 lines of code, 64 bytes of bytecode,
and take 8192 cycles to compile to 128 machine instructions which take
256 bytes and 256 clock cycles to run, whereas interpreting them would
have taken 1024 cycles, 7168 cycles less.  So every execution we catch
up by 768 cycles; after 10 executions (2560 cycles) we’ve paid back
the time cost of the JIT compilation.  That might be 10 executions
inside a loop on a single call, or it might be 10 calls.

Note that at that point we’re still spending 76% of our time in the
JIT compiler instead of running application code, but it’s still
faster than the interpreter!  It uses up a lot of our TCM, though.

If the native-code cache is 32KiB then it can contain about 128
compiled native-code subroutines.  Intuitively this feels like enough
to run most of the code more than 10 times before it’s evicted.  In
absolute terms, recompiling the whole native-code cache should take
about 20 milliseconds at 48 MHz.

There’s an interesting effect of making the compiler produce better
code.  If we produce code that is 10% better, we can probably fit 10%
more of it in the cache, so we get a lower cache miss rate, so we have
to run the JIT compiler less often.  (Probably not 10% less often,
because the cache hit rate curve is nonlinear and
application-dependent, but maybe in the neighborhood of 10% less
often.)  Also, though, when we run the JIT compiler, which was
compiled with the same compiler, it takes 10% less time.  So we get
something like a 20% improvement in the JIT compiler itself, even
though the rest of the code only speeds up by 10%.

It’s plausible that we could get to more like 21 cycles per bytecode
processed.

### Calls through trampolines ###

Anyway, so the idea is to interpose a universal trampoline on every
call, so that the call looks something like

      e6:   f240 3054        movw r0, #(caller_id << 4 | callsite_number)
      ea:   f241 41e9        movw r1, callee_id
      ee:   f7ff ffec        bl trampoline

(or use `ldr` on Thumb-1) and the trampoline will look up the
subroutine in its subroutine table.

The callsite ID is necessary so that if this subroutine gets evicted
from the native-code cache before being returned to, the trampoline
can rewrite the return address to point at the right callsite when it
recompiles it.

Initially this is sort of a mockup.  We pass a callsite ID and a
callee ID to the trampoline:

A strawman `call_trampoline` looks like this:

    000000ca <call_trampoline>:
      ca:   b501        push    {r0, lr}                 @ preserve callerid
      cc:   f859 2021   ldr.w   r2, [r9, r1, lsl #2]     @ index native tbl
      d0:   b12a        cbz     r2, de <missing_callee>  @ check for null
      d2:   4790        blx     r2                       @ invoke non-null
      d4:   bc05        pop     {r0, r2}                 @ restore caller
      d6:   f3c0 110b   ubfx    r1, r0, #4, #12          @ separate callsite
      da:   b111        cbz     r1, e2 <missing_caller>  @ verify presence
      dc:   4710        bx      r2                       @ return if okay

Ultimately `missing_callee` and `missing_caller` will invoke the JIT
compiler, but for the time being they can just report an error.

#### A separate trampoline per callee would cost an order of magnitude more space ####

We could save some space by having a separate trampoline per callee,
like a traditional PLT:

      f2:   f241 41e9       movw    r1, #5353       ; 0x14e9
      f6:   e7e8            b.n     ca <trampoline>

This saves 4 bytes per callsite but adds 6 or 8 bytes per subroutine.
This might be a space win in some sense but probably costs an extra 4
cycles per call.  Also these trampolines can’t ever be evicted, so if
you have 8 calls per subroutine and 128 subroutines in the 32KiB
native-code cache, those 4 bytes per callsite take up 4KiB, but if the
entire system contains 32768 subroutines, 6 bytes of trampoline for
each one is 192KiB, which is larger by a factor of 12.

32768 is not an unreasonably large number and may even be unresaonably
small.  I’m typing this in an Emacs with 23109 subroutines callable
from Lisp.  Libjpeg in libjpeg-turbo8-dev has 280 subroutines.
/usr/bin on this machine has 4543 commands.  32768 subroutines with my
estimate of averaging 16 lines of code and 4 bytes of bytecode per
line gives us 2 mebibytes of bytecode, so it’s not an outrageously
large amount of code, either. (Especially if some of it goes into the
Apollo3 microcontroller’s 1MiB of NOR Flash.)

#### By the same token, we can’t use a simple array for callee addresses ###

The above strawman trampoline code presupposes it can just index into
an array to find the callee’s native-code address, if any.  But having
an array of 32640 zeroes and 128 pointers is silly and would use up
much of our RAM.  So instead of this single instruction, we probably
need a loop over a smallish hash table:

      cc:   f859 2021   ldr.w   r2, [r9, r1, lsl #2]

We have r0 and r3 available at that point.

Backwards arguments on stack
----------------------------

I was thinking  that having the stack grow upwards
rather than downwards makes it unnecessarily slow and bulky to copy
things because I can’t use `ldmia`... but I was confused.
I want the first-pushed
argument on the operand stack to be the one pointed to by sp (although
I could change that).  So what I’m doing now is wrong, although maybe
doubly wrong in a way that cancels out.  I’m putting them on the
return stack in the opposite order.

Let’s see in the debugger.

I’m invoking `jit_eabi_stub` with (code, 5353, 535353) the first time,
so 5353 is in r1 and 535353 is in r2.  This ends up with 5353 in r5
and 535353 on the operand stack in RAM.  Then the generated code puts
5353 from r5 into r0 and 535353 from the operand stack into r1, and
then pushes {r0, r1, r2, lr}, which leaves {5353, 535353, 535353,
70437} on the machine stack; 70437 is the return address inside
`jit_eabi_stub`.

So, it looks like I am in fact getting it backwards, but twice, so it
cancels out.  `jit_eabi_stub` is pushing the arguments in the wrong
order, and then my generated code is reversing them as it moves them
onto the return stack.  But if I do the right thing without changing
which way the stack grows, the subroutine prologue code to push
arguments onto the return stack is just 1-3 instructions, one of
these:

    push {r6, lr}                @ 0-argument case, 3 cycles

    push {r5, lr}                @ 1-argument case, 5 cycles
    ldr.w r5, [r4, #-4]!

    ldr.w r0, [r4, #-4]!         @ 2-argument case, 9 cycles
    push {r0, r5, r6, lr}
    ldr.w r5, [r4, #-4]!

    ldmdb r4!, {r0, r1}          @ 3-argument case, 10 cycles
    push {r0, r1, r5, lr}
    ldr.w r5, [r4, #-4]!

    ldmdb r4!, {r0, r1, r2}      @ 4-argument case, 13 cycles
    push {r0, r1, r2, r5, r6, lr}
    ldr.w r5, [r4, #-4]!

    ldmdb r4!, {r0, r1, r2, r3}  @ 5-argument case, 14 cycles
    push {r0, r1, r2, r3, r5, lr}
    ldr.w r5, [r4, #-4]!

For more than 5 arguments we need something more elaborate, but that
can be put off until later.  I feel like multiple ldmdb/push pairs are
probably OK.

In most of these cases we could have a single 16-bit instruction as
the epilogue, but in the last case we don’t have enough temporary
registers to clobber.

    pop {r6, pc}                 @ 0-argument case, 3 cycles

    pop {r0, pc}                 @ 1-argument case, 3 cycles

    pop {r0, r1, r2, pc}         @ 2-argument case, 5 cycles

But we can pop an arbitrary-sized stack frame in 4 cycles, which is
faster for 2 or more arguments, though more code:

    adds sp, #8                  @ 2-argument case, 4 cycles
    pop {r6, pc}                 @ hey, free local variable reg

    adds sp, #8                  @ 3-argument case, 4 cycles
    pop {r0, pc}                 @ no free local this time

    adds sp, #32                 @ 11-argument case, 4 cycles
    pop {r0, pc}

It’s probably useful to have a conditional in the `ldmdb` assembling
code to emit the `ldr`.  Similarly for `stmdb` if I end up having
that: there’s a similar prohibition on `stmdb` of a single register.
`stmia` and `ldmia` have Thumb-1 encodings with no such limitation.

I guess I’ll just declare that `jit_eabi_stub` has the right behavior
but that you must call it with the arguments backwards, because the
alternative would be to have a different version of it for every
arity, or to always call it with the same number of arguments, with
the correct arguments right-justified.  If this turns out to be too
error-prone, I can always define C wrappers that invoke it with 2, 3,
or 4 arguments in the reverse order.


Indexable memory
----------------

I’ve been wrestling with the memory problem for a while.  C programs
clearly need a linear memory array, which may be larger than physical
RAM, and so I need a virtual memory system.  There isn’t a lot of *a
priori* information available about how different pointers relate to
each other, so generally you need to traverse a page table for every
linear memory access.  See “Speed estimation” in file
`c-bytecode-notes.md` for details, but it’s on the order of 16 clock
cycles, and amounts to about a third of the estimated time for the
small scraps of C I’ve estimated the speed of.  It would be more like
two thirds with a more performant compiler like some of the sketches
above.

To be concrete, an example millicode routine I have in mind for
reading a word from paged memory is the following:

- r0 is a temporary register;
- r5 has the desired index into paged memory;
- r7 has a bitmask for the current paged memory space size;
- r8 has a pointer to the beginning of the array of pointers to
  1024-byte pages; and
- lr contains the address to continue execution upon success.

        and r5, r7               @ limit index range
        ubfx r0, r5, #10, #22    @ unsigned bitfield extract of 22 bits
        ldr r0, [r8, r0, lsl #2] @ load page pointer
        cbz r0, tramp_fall_pag_54
        ubfx r5, r5, #2, #8      @ implicitly discard problematic low bits
        ldr r5, [r0, r5, lsl #2] @ load desired word
        bx lr

The model is that the virtual machine is, at any given time, viewing
one particular paged-memory space, which has a virtual size that is a
power of 2.  This space is implicitly passed to subroutines, which can
change it; if they don’t want that change to affect the caller, they
must change it back, just like a CPU register.  To view a different
paged-memory space, we need to reload ARM registers r7 and r8, and
maybe r9.  Context switches between tasks of course save and restore
these registers.

The array of page pointers must be contiguous, and can only be swapped
out if it isn’t the one currently being used; the active page table
must be fully resident.  Reasonable memory sizes for the Zorzpad might
be up to 8 MiB or so; with a page size of 1024 bytes, this would be
8192 pointers and 16384 bytes, which is probably tolerable.  Likely
you want one page table for read accesses and another page table
(perhaps pointed to by a different reserved ARM register) for write
accesses.

### Three bank-select registers ###

It occurs to me that, for things that aren’t compiled C programs,
there’s a much simpler and faster approach available.  Let’s say that
the Hadix machine has three *bank select* registers: rd0, rd1, and
wr0, and the following new instructions:

- select-bank.$bsr ( pd -- ) loads the specified bank-select register
  $bsr (either rd0, rd1, or wr0) with a page descriptor from the
  stack.  Verifies that in fact the value from the stack is a valid
  page descriptor and, if $bsr is wr0, that it carries write
  permission as well as read.  Also, if $bsr is wr0, it marks the page
  as having been written to.  This may need to wait for the page to be
  zeroed, brought in from Flash, decompressed, etc., or for another
  page to be evicted to make room for it.

- read-word.$bsr ( addr -- val ) reads a 32-bit word from the page
  currently selected by $bsr (either rd0, rd1, or wr0) at the address
  addr, taken from the stack.  addr is truncated to the page size;
  higher-order bits are ignored.

- write-word.wr0 ( val addr -- ) writes a 32-bit word val, taken from
  the stack, to the page currently selected by wr0 at the address
  addr, also taken from the stack.  addr is truncated in the same way
  as for read-word.

These bank select registers are inherited by callees and callers in
the same way as the linear memory space.  If you want to support a
callee-save convention with them, you might want to add an instruction
to get a page descriptor from a given bank-select register, but I
don’t think that’s really necessary in the absence of interrupts; we
can just declare them caller-save.

The idea here is that read-word and write-word compile to nearly
trivial code.  Suppose we assign r10, r11, and r12 respectively to
rd0, rd1, and wr0.  (Note that in ATPCS r10 is optionally “stack
limit”, sl, which reminds me that the calling trampoline also needs to
check for stack overflow, and r9 is “static base”, sb, in “[RWPI][0]
[read-write position independent] variants”.)  Then read-word.rd0
might assemble to

        ubfx r5, r5, #0, #10
        ldr r5, [r10, r5]

[0]: https://lists.llvm.org/pipermail/llvm-dev/2015-November/092856.html "ROPI causes all read-only data to be addressed PC-relative, and RWPI causes all read-write data to be accessed R9-relative."

That’s probably 4 cycles instead of 16, three fourths of native speed.
Possible optimized cases where the index is a compile-time constant
would be just a single instruction.  Attempts to write via r10 or r11
simply won’t compile.

Even writes are not too bad at, probably, 8 cycles:

        ubfx r5, r5, #0, #10
        ldmdb r4!, {r0, r1}      @ new tos and val to store
        str r1, [r12, r5]
        mov r5, r0

The earlier-suggested grown/shrunk optimization might shorten that in
many cases to something like this:

        ubfx r3, r3, #0, #10
        str r5, [r12, r3]
        ldmdb r4!, {r5}

The currently running task keeps these three pages pinned in memory,
in the same way we must keep the page table pinned with the other
mechanism.  Switching to a task requires ensuring its three pages task
are resident before running it.

Code that uses this mechanism to access many different objects will
need to run select-bank a lot, but in a wide variety of cases it will
run select-bank an order of magnitude less often than read-word or
write-word.  Even things like dense matrix-vector or matrix-vector
multiplication should usually be able to do at least 16 memory
accesses per select-bank call, which is the motivation for having
three banks instead of just two.

Most types of records can fit entirely into a single page.  When this
is the case, allocating them so that they don’t extend across multiple
pages ensures that any number of accesses to the same record can
proceed without intermediate select-bank calls.  In particular, any
desired fields can be copied into local variables, where they can be
used even after another select-bank call, for example to copy into
another record in a different page.

#### How to implement select-bank? ####

The idea is that all the runtime validation and virtual-memory logic
happens in select-bank, when possible bank switches happen, so even if
it’s a little slow, the system can be adequately fast.  In fast-path
cases this might be ten instructions or less; if we assume that
somehow we can statically validate that a given stack item is in fact
a descriptor, the happy path of select-bank.rd1 might be as little as
this:

            ldr r5, [r8, r5, lsl #2] @ load page pointer
            cbz r0, tramp_fall_pag_54
            mov r11, r0
            bx lr

But that assumes r8 points at a resident array of all possible pages,
which is unreasonable when Flash is 1024 times the size of RAM, and
also ties up a precious register.  You might do something like start
with a probe into a two-way set-associative cache with 16 lines, which
keeps track of 32 pages that are still in physical memory:

            ldr r0, =tlb_base
            ubfx r2, r0, #2, #4
            ldr r1, [r0, r2, lsl #3]
            cmp r1, r5
            bne not_in_first_way
            adds r0, #4
            ldr r11, [r0, r2, lsl #3]
            bx lr
            .ltorg
    not_in_first_way:
            adds r0, #128
            ldr r1, [r0, r2, lsl #3]
            cmp r1, r5
            bne not_in_tlb
            adds r0, #4
            ldr r11, [r0, r2, lsl #3]
            bx lr
    not_in_tlb:

That’s 8 instructions and 3 memory accesses (probably to the TCM) in
the best case, 12 instructions and 4 memory accesses in the
second-best case.  If that’s not found, the next approach might be to
do a lookup in a hash table cataloging all resident pages, placed,
say, right after the TLB.  Here I’m making the hash table 512 entries
(2KiB) because we only have 384KiB of RAM and so it’s guaranteed to
not fill up, so we don’t need an extra termination test in the loop.

            eor r1, r5, r5, ror #17
            eor r1, r1, r1, ror #5
            adds r0, #128           @ 512-entry hash table is immediately after
    1:      ubfx r1, r1, #0, #9
            ldr r2, [r0, r1, lsl #3]
            cbz r2, not_in_hash
            cmp r2, r5
            bne 2f
            adds r0, #4
            ldr r11, [r0, r1, lsl #3]
            bx lr
    2:      adds r1, #1
            b 1b
    not_in_hash:

That’s 11 instructions and 2 more memory accesses in the case where
the page is found immediately, or 6 instructions and 1 more memory
access in the (perhaps more common) case where the page isn’t
resident.  This is probably fast enough that the previous TLB isn’t
beneficial and should be removed.  Even the two XOR instructions at
the top might be a waste; the bits of page descriptors might already
be random enough to use as a hash table key.  So you might end up with
something like this:

            eor r1, r5, r5, ror #17    @ just as fast as a mov, so...
            ldr r0, =resident_hash
    1:      ubfx r1, r1, #0, #9
            ldr r2, [r0, r1, lsl #3]
            cbz r2, 3f
            cmp r2, r5
            bne 2f
            adds r0, #4
            ldr r11, [r0, r1, lsl #3]
            bx lr
            .ltorg
    2:      adds r1, #1
            b 1b
    3:

5 instructions and 2 memory references on immediate failure, 10
instructions and 3 memory accesses on immediate success, 7
instructions and 1 memory access per unsuccessful loop iteration.

### Linear memory using bank-select registers ###

With these bank-select registers, maybe we wouldn’t have to do
page-table traversal in the runtime system at all.  When we want to
dereference a pointer value on the stack, we could include bytecode to
index into a page table with it, select the resulting page into a
bank, then index the resulting page.

If we do want to do that, we would probably want to have at least
2048-byte pages, assuming page descriptors can somehow be stored in 4
bytes.  (Presumably somehow the page is somehow tagged as containing
descriptors, i.e., capabilities, rather than bytes.)  That way, one
page of page descriptors holds 512 descriptors, which works out to a
mebibyte.  That’s a bit cramped, but probably programs that need much
more data than that are going to suffer rather badly on the Zorzpad.

Alternatively, pages full of descriptors could be bigger than pages
full of bytes, though this runs counter to the designs of KeyKOS and
EROS, or they could be variable-sized (requiring a dynamic bounds
check on each access).  I think it’s probably okay for operations on
descriptors to be more expensive than operations on integers.

### Are bank-select registers fatally flawed because they’re too visible? ###

A thing I’m struggling with here is that I feel like it might be hard
to hide bank-select registers behind an interface that’s simple, safe,
and efficient.  Smalltalk’s or Python’s interface is simple and safe:
you can write something like this:

    def lomuto(seq):
        "My simplified version of Lomuto’s partitioning algorithm."
        i = 0
        for j in range(len(seq)):
            if seq[j] <= seq[-1]:
                seq[j], seq[i] = seq[i], seq[j]
                i += 1

        return i - 1

This is totally abstract over sequence types as long as they have a
length and can be indexed for reading and writing, but the Python
implementation is not very efficient.  In particular it doesn’t care
at all whether the sequence fits in one page or not.  Very similar
code works with C++ STL iterators:

    template<typename Iter>
    Iter lomuto(Iter start, Iter end)
    {
      size_t i = 0;
      for (size_t j = 0; j != (size_t)(end - start); j++) {
        if (start[j] <= end[-1]) {
          swap(start[i], start[j]);
          i++;
        }
      }
      return start + i - 1;
    }

Although it’s more idiomatic in C++ to omit the integer indexing
entirely:

    template<typename Iter>
    Iter lomutoo(Iter start, Iter end)
    {
      Iter p = start, q = start;
      for (; q != end; ++q) {
        if (*q <= end[-1]) swap(*p++, *q);
      }
      return p - 1;
    }

This is, arguably, simple and efficient; it’s not safe, though, in the
sense that C++ promises to waterboard your sister if you ever make the
smallest mistake.  If you compiled it to use a linear paged memory
space as described above, it would be safe but probably not very
efficient.

What I’m nervous about is the possibility of scattering decisions all
over the code base about what’s small enough to fit into a page and
what isn’t, and then having to spend a lot of time changing those
decisions when it turns out that some of the things I thought were
small enough actually weren’t.  That isn’t my idea of fun.  I think
probably most of my code will want to use some kind of fairly generic
pointer that incurs an extra select-bank operation on every access,
but I’ll have a few inner loops that do things like copying a block of
bytes from one place to another, bitblting graphics, multiplying
matrices, or copying a struct into local variables, which take
advantage of the available higher efficiency.
