A standard blockchain (a Merkle chain) allows you to verify any past
block from the current tip hash or from any tip hash more recent than
that block, in linear time.  There are currently about 876000 blocks
in the Bitcoin blockchain; you could walk back along the blockchain
from the current tip with at most 876000 hashing operations.

If the inputs to the hashes along the spine of the chain are (car,
cdr) tuples, where the cdr is the hash of the previous spine node and
the car is the hash of the new node to add, each of those hashing
operations is relatively fast; it only has to hash the two hashes (if
you’re using SHA-256, they’re 256 bits each, 32 bytes each, 64 bytes
total) rather than hashing an entire block.  But that’s still up to
about 56 megs of data.

You can also package up a proof witness that can be checked offline,
consisting of the nodes along the spine back to the block you want to
verify, then the block.  So that’s up to 56 megs of data in a case
like this.  This is still pretty tiny compared to the whole
blockchain.

You can require logarithmic time rather than linear time, and make the
proof witness logarithmic-sized instead of linear-sized, by adding
additional hashes to the spinal nodes which allow you to leap over
large sections of the blockchain.  One additional hash should be
enough.

A simple O(log² N) approach
---------------------------

We assign each node a *temperature* (a skiplist “height”) in an
exponentially distributed way.  On the blockchain, each block has a
well-defined integer *distance* from the genesis block (the blockchain
“height”).  A straightforward way to handle this is to assign the
temperature as the number of zeroes at the end of the binary
representation of the distance, so, for example, block 8 (the block 8
blocks after the genesis block) has temperature 3, block 10 has
temperature 1, and block 12 has temperature 2, and all odd blocks have
temperature 0.

Now we can add the skiplist pointers.  The additional hash we add, in
addition to the previous hash, is a “hot hash” of the most recent node
of the same or higher temperature.  So, for example, node 27 (the node
for block 27) has temperature 0.  Its hot hash is the hash of node 26,
which is also its previous hash.  Node 26 has temperature 1, so its
hot hash is the hash of node 24, which has temperature 3.  Node 24’s
hot hash is the hash of node 16, which has temperature 4.  Node 16’s
hot hash is null.

This allows us to move backwards by exponentially increasing leaps
along the hot pointers until the next hot pointer would be too far.
Then we have to follow a previous pointer, which will generally take
us to a node of temperature 0, at which point we start following hot
pointers again.

Each hot chain takes a number of steps bounded by the log of the
distance to the genesis block and by reaching a lower temperature than
the previous hot chain.  So if there are 876000 blocks, the first hot
chain is at most 19 steps, the second is at most 18, the third is at
most 17, etc., so we need at most 190 steps along hot links (and 19
along previous links) to reach our destination.

We can package up those nodes into a proof witness.  209 64-byte nodes
would be 13 kilobytes, which is significantly smaller.

But can we do asymptotically better?  It would be nice if we could
find a way to use O(log N) hops instead of O(log² N).

An approach I thought would work with two hashes
------------------------------------------------

I thought that maybe the problem could be solved by 

Each node on the spine has
three hashes: the *previous* hash, the *hot* hash, and the *cold*
hash.  The previous hash is the hash of the previous node.  The hot
hash is the hash of the most recent node of the same or higher
temperature.  The cold hash is the hash of the most recent node whose
temperature is 1 unit lower.

So, for example, block 24 has temperature 3.  Its hot hash is the hash
of block 16, which has temperature 4, hotter than block 24.  And its
cold hash is the hash of block 20, which has temperature 2, colder
than block 24.

To traverse the skiplist backward to a given node, you just follow the
hot pointers a logarithmic number of times to get to 
