Watching [EEVblog’s Dave Jones describe his μCurrent][0] I was struck
by the description of the [TPS3809L30][1] low-battery detection chip:
“a tiny little 3-pin device”.  Two of its pins go to Vcc and ground;
the third provides a reset signal when Vcc is below 2.64 volts, which
he’s only using to power a battery-status LED through a
current-limiting resistor.

[0]: https://youtu.be/g7b5YZENvjY
[1]: https://www.ti.com/lit/ds/symlink/tps3809.pdf

It occurred to me that for the LED case, you could do this with a
*two*-pin chip, which you could use with a couple of external passives
even to provide the reset signal in the more conventional TPS3809 use,
where it holds other circuitry in the reset state until power has
stabilized.

Instead of connecting it to a *series* combination of an LED and a
resistor to ground, you’d connect it to a *parallel* combination.  The
chip would draw its usual 10μA most of the time, which is not enough
to light an LED, but a 330kΩ resistor in parallel with the LED would
be sufficient to supply it at 3.3V.  Periodically it would cut off
external power, dropping into the picoamps, to measure the full power
supply voltage while running from an internal capacitor.  If the
voltage was too low, it would go into a mode where it passes a
regulated 10mA or so, which will light the LED brightly.  It still has
to cut off the current periodically and allow the LED junction to
discharge in order to measure the power supply voltage and see if the
voltage has gone high enough.  The light interruption need not be long
enough to be visible to the naked eye.

To use this to supply a RESET signal to a microcontroller or
something, you dump the LED, use a smaller resistor (10kΩ, say), and
RC-filter the voltage across that resistor.

This seems like it would provide an active-*high* reset signal, while
the dadashit linked above says it provides an active-*low* reset
signal.  Jones’s design, though, turns the LED *off* when the voltage
is low, labeling it “BATT OK”.

Doing this in an active-low way seems like it would be much simpler:
you just make a voltage divider from a zener and a resistor, with the
resistor going from the zener to ground, the reverse order from when
you use it as a voltage reference.  As long as the power-supply
voltage is below the threshold, the resistor voltage is zero, but once
it goes above it, the resistor voltage follows it up.  The only
problem is that the resistor voltage doesn’t go very high; for the
particular circuit in question, a 2.64-volt zener will lower a
3.3-volt power supply to 0.66 volts, barely enough to turn on a BJT
and not even enough for TTL.  But you could use a smaller zener or
just one or more voltage drops.
