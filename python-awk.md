<https://news.ycombinator.com/item?id=40992191>

kernighan said in the interview

> *The main idea in Awk was associative arrays, which were newish at the time, but which now show up in most languages either as library functions (hashmaps in Java or C++) or directly in the language (dictionaries in Perl and Python). Associative arrays are a very powerful construct, and can be used to simulate lots of other data structures.*

awk was released in 01979, and the authors published this paper in sp&e that year: https://plan9.io/sources/contrib/steve/other-docs/awk.pdf but you see this report version dated september 01978.  but i don't think the report was widely circulated until the next year, when it was included in 7th edition unix as /usr/doc/awk (sudo apt install groff; groff -ms -Tutf8 v7/usr/doc/awk | less -r).  it explains: 

> *Array elements may be named by non-numeric
values, which gives awk a capability rather like the
associative memory of Snobol tables. (...) There is an alternate form of the for statement
which is suited for accessing the elements of an associative array:*

this pdf has evidently been retypeset from the troff sources from the open-source 7th edition release, but without the correct bibliographic database, so the references are missing.  a comment in the troff source says:

> *....It supersedes TM-77-1271-5, dated September 8, 1977.*

but possibly that reference is inaccurate

python goes beyond merely having dicts 'directly in the language'.  python's primary data structure is the dict; among other things, it uses dicts for modules, (most) class instances, associating methods with classes, the locals() user interface to the local-variable namespace, and passing keyword arguments to functions and methods.  that is, it uses associative arrays to simulate lots of other data structures, as you are obliged to do in awk, lua, and js.  so where did python get dicts?

python got dicts (and tuples) from abc, a teaching language which wikipedia claims was started in 01987, 8 years after awk's release, and added conventional arrays (lists) back in.  the five data types in abc https://homepages.cwi.nl/~steven/abc/language.html are numbers, strings, compounds (called tuples in ml), lists (really multisets because they're implicitly sorted), and tables (dictionaries), which are awk's 'associative arrays'—and, as in awk, js, lua, and tcl, they're used to provide the functionality of conventional arrays as well

however, lambert meertens credits the use of tables in abc to jack schwartz's setl https://inference-review.com/article/the-origins-of-python rather than to awk.  he says of the addition of tables to b (the early name for abc, not to be confused with the b that was an earlier version of c)

> *Having coded a few algorithms in SETL, I had experienced its power firsthand—a power that stemmed entirely from its high-level inbuilt data types. Particularly powerful were sets and maps, also known as “associative arrays,” containing data that can be indexed not only by consecutive integers but by arbitrary values. A programmer could introduce a simple database of quotations named whosaid, in which the value ”Descartes” could be stored in the location whosaid[”cogito ergo sum”]. These high-level types made it possible to express algorithms that required many steps in B1 using just a few steps in SETL. In a clear violation of the Fair-Expectation Rule, B1 allowed only integers as array indices. This design decision had been driven by fear: we had been concerned that aiming too high would make our language unimplementable on the small personal computers that were starting to appear on the market. But Dewar, in particular, convinced me that this meant we were designing for the past, not the future. This led us to redesign the system of data types for our beginners’ language. This time we used only the criteria of ease of learning and ease of use to select candidate systems. The winner turned out to be remarkably similar to the data type system of SETL. The set of possible data type systems to choose from was very large, and to make the process more manageable I had written a program to select the competitive (Pareto-optimal) candidate systems. Interestingly, but quite incidentally, that selection program itself was written in SETL. The winning type system became that of B2, and remained unchanged in the final iteration, released in 1985 under the name “ABC.”*

'associative arrays', of course, is the term used by awk

this story of adding associative arrays to abc only for b2 is somewhat complicated by the fact that the version of b (b1?) in meertens's 01981 'draft proposal for the b programming language' https://ir.cwi.nl/pub/16732 already includes tables, three years after the release of awk as part of 7th edition; p. 6 (9/91) says,

> *Tables are somewhat like dictionaries.  A short English-Dutch dictionary (not sufficient to maintain a conversation) might be (...) Table entries, like entries in a dictionary, consist of two parts.  The first part is called the* key *, and the second part is called the* associate*.  All keys must be the same type of value, and similarly for associates.  A table may be written thus:* {[’I’]: 1; [’V’]: 5; [’X’]: 10}.

> *If this table has been put in a target* roman*, then* roman[’X’] = 10.

note that this is also awk's *syntax* for indexing an associative array, though it doesn't have a syntax for writing one down.

a more recent set of slides on the relation between abc and python is https://www.cwi.nl/documents/195216/Meertens-20191121.pdf which describes again how abc was started in 01975.  this helpfully clarifies the timeline: b0 was 01975; b1 was 01978; b2 was 01979; and b∞ = abc was 01985.  so specifically the point at which setl inspired the replacement of conventional arrays in b1 with associative arrays in b2 was 01979, which was the year 7th edition unix was released and the aho, weinberger, and kernighan paper was published in sp&e

a question of some interest to me here is what platform they were developing abc on in 01979.  clearly it couldn't have been the ibm pc, which wouldn't come out until 01983 (and as far as i know abc on the ibm pc only runs under cygwin or 32-bit microsoft windows), or macos (which came out in 01984) or atari tos, which wouldn't come out until 01985.  and so far i haven't seen any mention in the history of abc of other operating systems of the time like cp/m, vm/cms, dg rdos, tenex, or tops-20.  the most likely platform would seem to have been unix, on which awk was one of the relatively few programming languages available.  perhaps at some point i'll run across an answer to that question in the abc papers

python adopted awk's syntax for putting 10 into roman['x'], which was `put 10 in roman['x']` in abc, but `roman['x'] = 10` in awk and python.  abc's syntax is uppercase, presumably case-insensitive, separates words with apostrophes, and departs widely from conventional infix syntax.  python's syntax is case-sensitive, mostly lowercase, and conventionally infix, features that have become common through the influence of unix.  python's control structures are for, while, and if/elif/else, as in algol and in abc, and indentation-sensitive as in abc, but uses a conventional ascii syntax rather than abc's scratch-like syntax-directed editor

abc was statically typed with a hindley-milner type system ('the type system is similar to that of lcf', p. 15 (18/91) of the draft proposal), while python is dynamically typed, like smalltalk, lisp, and awk

if meertens got his daring notion of storing everything in associative arrays from awk, he certainly doesn't mention it.  instead he mentions setl a lot!  the draft proposal doesn't cite awk but it also doesn't cite setl; it cites the algol-68 report, milner's lcf typing paper, a cleaveland and uzgalis paper about grammars, gehani, and three of his own papers, from 01976, 01978, and 01981.  unfortunately i can't find any of those earlier meertens papers online

the wikipedia page about setl says

> *SETL provides two basic aggregate data types: (unordered) sets, and tuples.[1][2][5] The elements of sets and tuples can be of any arbitrary type, including sets and tuples themselves, except the undefined value om[1] (sometimes capitalized: OM).[6] Maps are provided as sets of pairs (i.e., tuples of length 2) and can have arbitrary domain and range types.[1][5]*

but it's citing papers about setl from 01985 there, well after awk had supposedly popularized the notion of associative arrays

however, in meertens's essay on python's history, he cites a 01975 paper on setl!  https://www.softwarepreservation.org/projects/SETL/setl/doc/Schwartz_et_al-On_Programming_SETL_Interim_Report-1975.pdf/view

> *Jacob T. Schwartz. ON PROGRAMMING: An Interim Report on the SETL Project. Part I: Generalities; Part II: The SETL Language and Examples of Its Use. Computer Science Department, Courant Institute of Mathematical Sciences, New York University, revised June 1975.*

this discusses how setl represented data in memory starting on p. 57 (57/689).  it used hash tables to represent sets, including sets of tuples, rather than the ill-advised balanced-tree approach used by abc.  (python, like awk and setl, uses hash tables.)  on pp. 62–63 (62–63/689) it explains:

> *The hash code of a tuple is taken to be the hash code of
its first component, for reasons that will become clear in the
next section. The hash code of a set is the exclusive or of
the hash codes of all its members. (...)*

> *— Tuples in Sets —*

> *Though expressible in terms of the membership test, "with", and
"less" operations, functional evaluation plays so important a role
in SETL algorithms that we treat it as a primitive.*

> *SETL makes three types of set-related functional evaluation
operators available:*

> - *f(x)*

> - *f{x}*

> - *f[s]*

> *The most fundamental of these is f{x}, which invokes a search
over f for all n-tuples that begin with x (n ≥ 2), and which
yields as result the set of all tails of these n-tuples. More
precisely, in SETL:*

> *f{x} = if #y eq 2 then y(2) else tℓ y, y ∈ f | type y eq tupl and #y ge 2 and hd y eq x}*

> *The operation f(x) has a similar definition but includes a single valuedness check:*

> *f(x) = if #f{x} eq 1 then ∋f{x} else Ω*

> *The operation f[s] is adequately defined in terms of f{x}:*

> *f[s] = [+: x ∈ s] f{x}*

i am fairly confident that the f{x} definition translates into current vernacular python as the set-comprehension {y[2] if len(y) == 2 else y[1:] for y in f if type(y) == tuple and len(y) >= 2 and y[0] == x}.

(∋ is explained on p. 80 (80/689) as choosing an arbitrary element from a set, which is of course deterministic if the set contains only a single element, as in this case)

so, it becomes clear that already in 01975 setl treated sets of tuples as maps, which is to say associative arrays, but it didn't use the 'associative array' terminology used by meertens in 01981, or for that matter 'maps'.  to look up an element in the map, it didn't use the f[x] notation used by python, awk, and abc; instead it used f(x).  further explanation on pp. 64–65 (64–65/689) clarifies that really it is more accurate to think of 'sets of tuples' as trees; each item in the tuple entails following an additional level of pointers to a further hash table

(in a number of other notational details, python and presumably abc follows setl: start: or start:end for array index ranges, + for string concatenation, * for string repetition, boolean operators spelled out as and, or, and not. but indexing maps is far from the only difference)

abc (including b as described in the 01981 report) also seems to lack the f{x} operation and its possibility of associating an arbitrary-sized set of values with each key.  this is a nontrivial semantic divergence

so if abc got its idea of tables from setl, but used awk's terminology, notation, and semantics for them (and its own ill-conceived balanced-tree implementation, used by neither), and decided to adopt the table idea in the year when awk was released, probably on the platform that awk was released on, i think it's reasonable to assign some share of the credit for abc's tables to awk?  even if not all of it

but if that's so, then why didn't meertens credit aho, weinberger, and kernighan?  i don't know.  maybe awk's loosey-goosey nature was repugnant to him.  maybe weinberger is jewish and meertens is secretely anti-semitic.  maybe meertens thought that awk's loosey-goosey nature would be repugnant to the dijkstra-honoring dutch computer science establishment.  maybe aho insulted meertens's favorite beer one time when he visited the netherlands.  or maybe he thought it would be unfair for aho, weinberger, and kernighan to steal the thunder of schwartz, who did after all precede them in developing a general-purpose language whose memory was based entirely on hash tables.   from a certain point of view that would be like crediting carl sagan with the theory of relativity because he explained it on nova
