As I was walking around downtown doing some errands today, I was
thinking about Chuck Moore's posited duality of registers (store only
one value which can be read many times) and stacks (store many values,
each of which can be read only once), about autoincrement addressing
on the PDP-11 and MSP430, about systolic processor arrays, and about
Cray vector registers, and a connection occurred to me with Python
generators.  What if you have not just stacks in your processor but
also queues?

Python generators are little programs which incrementally produce
sequences of values.  They are suspended until asked for a value; then
they run until they have a value to yield, and are then suspended
again.  There are electronic interfaces like I²C which sort of work
this way, too; when the recipient receives a value, they can "stretch
the clock" to tell the sender to not send another value immediately.
I think Centronics parallel ports also work this way; each byte is
asserted until it's acknowledged.

Suppose in hardware you have two memory fetch units, A and B; a float
multiply unit, C; a float summing unit, D; and a memory store unit, E.
Each of these operates as a slightly eager generator.  Unlike Python
generators, it starts working as soon as it's set up, but whenever it
has produced a value, it stops working until that value is consumed.
You set up unit A to fetch 12 4-byte words from 0x302c00c with stride
4 and unit B to fetch 12 4-byte words from 0x830cd8c0 with stride 40.
You set up C to consume values from A and B.  You set up D to have an
initial sum of 0 and consume values from C.  Then you set up E to
store to 0x302e080 and consume a value from D.  The summing unit D
starts fetching products from C and continues until A and B produce an
end-of-file condition, which propagates to C, signaling D that its sum
is complete.

If we had set the vector length on D rather than A and B, they would
have fetched a total of 14 values, not 12, and this would both create
extra contention in the memory system and might cause side effects if
they stray into memory-mapped I/O or some kind of memory protection
system.

This seems like a way to get Cray-like pipelined vector performance
without the Cray-like large vector registers.  Moreover, the
possibility of consuming values "asynchronously" under program control
(I don't mean without a clock, just not necessarily on every clock
cycle) would seem to offer a great deal more flexibility than either
Cray vector registers or the currently popular GPU and
SIMD-instruction approaches.  And small programmable state machines
like the RP2040's PIO engine could perhaps do more interesting
processing at some nodes.

If you only have a few such functional units, you can connect them
with a crossbar switch, but it's easy to spend a lot of real estate on
that.  It might be better to connect them in a grid topology with a
small number of longer-distance buses and be able to program each unit
to do different things: at a minimum, either perform a processing step
or permit signals through unimpeded.

This seems like a more promising direction for the "field programmable
RTL array" thing I've been noodling on for a long time.  It also seems
related to the blocking grid communication in the GreenArrays chips.

Other processing nodes might include nodes that always return a
constant, potentially large FIFOs or ring buffers, counters, LUTs,
read ports for local memory, LFSRs, read and write ports for stacks,
and pointer-chain-following nodes which generate the nodes of a linked
list from RAM.

The way I've been thinking about it above, each port is always
single-producer single-consumer and is in one of four states:

1. Value ready, waiting on consumer.
2. No value ready, waiting on producer.
3. End of stream, waiting on consumer.
4. End of stream, ready for next task.

This permits, among other things, "wye" nodes which read from either
of two input ports and only have an unready output when *neither* of
their inputs are ready; "latest" nodes which always produce on their
output the most recent value that has been available on their input;
"cat" nodes which read from only their first input port until EOF,
then from their second input port; and "freewheel" nodes which consume
values from their input port as rapidly as possible even if their
output port is not being read.  These suggest that the ports ought to
be a bit wider than the data values typically processed, in order to
pass along an identifying meta tag along with the data.

How do you get configuration data to the nodes?  The traditional FPGA
and JTAG approach is basically a giant shift register snaking through
every node, thus dedicating a minimum of silicon to the task of
reconfiguring things quickly; but perhaps it might make sense to use a
bus that talks to all the nodes at once to permit more selective
reconfiguration, perhaps even a parallel bus to permit many bits per
cycle of configuration data to be sent.

A different approach to evaluating such a node graph is to time-share
ALUs between different node types, like in a regular CPU.  So 
