We have lots of Global Positioning Systems.  How about a Local
Positioning System?

UHF signals, in the band above VHF, 300 MHz to 3 GHz, are ubiquitous
in urban environments.  The lower end of the band penetrates buildings
easily.  Some of them are quite high power: UHF TV stations in the
470-700 MHz band can be tens of kilowatts.

Suppose you can observe a 500MHz signal being transmitted from a known
location with a known phase.  If you can observe its phase, you can
tell how far you are from the source, modulo the wavelength, which is
about 600mm.  That is, you can't tell if you're 1027.3m or 6027.3m
away, or in which direction, but you can sure tell if you're 1027.3m
away or 1027.6m away.  If you can measure the phase to within an
eighth of a cycle, you get about 3 bits of information about your
position.

If you can observe many such signals which are at different
frequencies from different sources, the 3 bits from each one will be
uncorrelated with the others, so you'll be able to use them to rule
out progressively more and more possibilities.  Supposing you have a
10km x 10km x 1km space, and the various strata of phases are on the
order of 100mm, you have about a hundred trillion possibilities to
choose among, so you need about 48 bits, so you need to establish the
phase of some 16 reference signals.

But if those signals are things like UHF TV stations, they aren't
being broadcast with a conveniently predictable phase or even dominant
frequency.  But suppose you have four observatory points at known
locations that *measure* the frequency and phase of the dominant 32 or
so signals in a predetermined band.  The frequency might require 24
bits of precision and the phase another 4, and the pre-measured
lat/lon/altitude values another 64.  These 960 or so bits can then be
transmitted by LoRa or other similar low-bandwidth radio protocol, for
example over the course of a second, along with a timing reference.

The Local Positioning System receiver receives these low-bandwidth
broadcasts and uses them to time its own capture for the band to be
the same 50ms interval the observatories are going to capture next.
Like the observatories, it logs the frequencies and phases of the
strongest signals in the band.

(Hmm, I guess we need a little more information: the estimated
location of each signal source.)

Anyway, when it manages to decode the next few thousand bits from the
observatories announcing their observations for the same time
interval, it can compare their phases to the ones it measured itself.
Thus it can, hopefully, compute its own location.
