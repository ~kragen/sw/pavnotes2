I was thinking that maybe you could do low-power low-bit-rate VHF
pulse radio with very simple transmitters and receivers.  A spark gap
would in some sense be ideal, but it’s burdened with historical
baggage, and also is somewhat difficult to trigger with low jitter.
Jitter is a huge limitation.  So I was thinking that maybe you could
just use a triac instead of a spark gap and get the jitter down into
the subnanosecond range.

Triac pulses
------------

The idea is basically that you have a copper wire which you charge up
to some ridiculous voltage, whatever your triac will withstand
reliably, and discharge it to ground through a triac when the time
comes.  Half a meter to ten meters of wire will give you a
half-wavelength whip antenna in the VHF (30–300 MHz) band, giving you
efficient coupling.  VHF gives you building penetration and a bit of
over-the-horizon propagation, like typically 150 km or so.

XXX you want a quarter-wave monopole, not half-wave

[Common home appliance repair triacs][1] cost US$2–5 and are rated for
600 or 800 volts (FT0107MN, FT0109MN, Z0107MN, Z0607MA, BTA41-600,
BT137-600E, BTA24-800, BTA16-600B, BTA41-600B) and can probably handle
1000, and probably you can put three or four of these in series to get
3000 or 4000 volts.  You put megohm resistors across them to balance
the voltages and trigger all their gates at once through a
capacitively coupled pulse.  (That might not be necessary if avalanche
discharge is sufficient to get one started, but that might damage the
triac, and might also have more jitter.)

[1]: https://listado.mercadolibre.com.ar/triac

[Digi-Key sells the BTA416Y-600C,127 for US$1.20][0].  I think this is
comparable.  They link to a [WeEn datasheet][2] which says it can
handle up to 176 amps for 16.7 milliseconds, even though it’s only
rated for 16 amps continuous, and has a 1.2–1.5 volt drop in the on
state, which works out to about 4.4 joules dissipated per pulse.
Since it’s rated for 600 volts, that would be about 1.8 kilojoules
transmitted.  It needs 2–35 mA to trigger it, and at Tⱼ=125° is
limited to 500V/μs (500MV/s) and 10 A/ms.  10A/ms seems pretty slow
but I think those are maybe when it’s turning off; its “absolute
maximum rating” for “rate of rise of on-state current dIₜ/dt” is
100A/μs, four orders of magnitude higher.  There’s a plot of
“non-repetitive peak on-state current as a function of pulse duration”
with a peak at 60 μs and 900 amps, limited linearly by the dIₜ/dt
below that point so that, for example, at 10 μs it’s only about 180
amps again.

[0]: https://www.digikey.com/en/products/detail/ween-semiconductors/BTA416Y-600C-127/1966177
[2]: https://www.ween-semi.com/sites/default/files/2018-10/bta416y-600c.pdf

It also has about a 10 pF capacitance from “main terminal 2” to its
heatsink, which I think suggests that maybe the main terminal 2 ought
to be the grounded one.

The datasheet has no information about jitter because I guess normally
it’s far too small to care about.

Of course we don’t want a 10μs pulse!  That would give us like a 50kHz
peak power, great for transmitting across the ocean, but which also
means you have to contend with noise from across the ocean.  We want a
pulse in the 3–33ns range, which would presumably limit us to 180mA,
though since dIₜ/dt is an “absolute maximum rating” I think maybe the
issue is damage to the triac, not whether it can generate fast pulse
edges.  The impedance of free space is about 376.7303137Ω, which I
think vaguely means that if your current is 180mA your voltage should
be in the tens of volts rather than the hundreds of volts.  (I think
this is sort of the surge impedance rather than the radiation
resistance.)

It turns out triac turn-on time is typically 100ns or more, and the
dIₜ/dt limit comes from needing to avoid hot spots on the junction.
(Possibly for a short enough pulse this doesn’t matter.)

All this makes me think that maybe it would be better to use an
avalanche transistor instead of a triac, since the potential benefit
of a triac is that it can handle higher voltages, but that’s useless
if it can’t handle high enough currents.  But maybe the triac dIₜ/dt
limitation isn’t a real thing in this case.

Antenna self-capacitance
------------------------

The formula for self-capacitance of a sphere (like van de Graaff
generator top electrode) is *C* = 4*πε*₀*R* where *R* is the radius.
So a 10-mm-radius copper sphere has capacitance to the universe of
about 1.1 pF, and if we approximate a 1000-mm-long 20-mm-diameter wire
(or rod, or pipe) as a stack of *n*=50 such spheres, its capacitance
to infinity should be about 55pF.  So charging it up to 1000 volts
would require about 55nC and 28μJ.  If I pretend that the impedance of
free space is an actual resistance through which the capacitance
discharges, it would start discharging at about 2.7 amps, which would
put it on a path to fully discharge in about 10 ns, which seems like
about the right thing — maybe this is equivalent to discovering that a
meter-long antenna efficiently radiates around 100 MHz, which we sort
of knew already.

If we increase *R* then we increase the capacitance of each sphere
proportionally but also decrease *n* proportionally, which leads to
the conclusion that a 1-meter-long whip antenna has a self-capacitance
of about 55pF regardless of how thin or thick you make it (until it
approaches being a sphere itself).  That also leads to the conclusion
that its self-inductance also remains fixed, since otherwise its
discharge time would change.  That seems intuitively wrong to me, but
I don’t know, maybe it’s right.

Using a pipe rather than a wire would reduce the antenna’s resistance
(since [the skin depth of copper at VHF is 4–12μm][4]) but that turns
out to only begin to matter at such low frequencies.  The pipe
diameter is important because we don’t want its skin resistance to be
large compared to the impedance of the radiative medium.  But it turns
out that at these frequencies that’s pretty easy; if the circumference
is 100 μm, the skin depth is 4 μm, the length is 1 m, and the
resistivity is 17.2nΩ·m, we get 43Ω of resistance, which is not large
compared to 376Ω.  So even hair-thin wires would be fine.

[4]: https://www.allaboutcircuits.com/tools/skin-depth-calculator/

You can get more capacitance by using a longer antenna, though I think
it’ll smooth off the top part of the VHF frequency range from your
pulses, and it makes the apparatus less convenient; you can get more
pulse energy by using a higher voltage; and presumably by using an
array antenna you can also get more pulse energy, along with
directionality.  You could imagine, for example, a 2-meter-wide
miniature endfire curtain array antenna made of 2-meter-long vertical
antenna wires, each 100 μm across and spaced 1 mm apart; this ought to
give you 2000 110pF antennas totaling 0.22 μF.  If you charge it up to
4000V you release 1.8J over about 40ns with, ideally, something close
to 33dBi of directionality, a pulse of 45 MW with an EIRP of 90 GW.

However, that’s not realistic; such a device would have to have less
self-capacitance than its circumscribed sphere, with 1414 mm radius,
but that is only 157pF, which would take less than 1.3mJ to charge to
4000V, and release under 32kW averaged across a 40ns pulse, with an
EIRP of under 63 MW, under probably absurdly overoptimistic
assumptions about the radiation pattern of an endfire curtain array.
And these are still upper bounds.

That would still be a ridiculously excessive EIRP, even if it’s
smeared across more than an octave of bandwidth, and might cause not
only interference but actual damage to the neighbors’ electronics, so
it would be a terrible idea.  But it does seem like something you
could do with a handful of triacs, at least once.  If you were doing
it at microwave frequencies instead of VHF you could probably
miniaturize it somewhat, and if you didn’t care about jitter you could
trigger it with a spark gap or pseudospark switch instead of
semiconductors.

Wikipedia tells me VHF analog TV signals are horizontally polarized to
reduce multipath fading from building reflections, but for pulse radio
that is much less of a problem.

BJT avalanches
--------------

If you instead use an BJT in avalanche mode, you’re limited to a lower
voltage, typically around 100 volts, and you can get pulse rise times
2–3 orders of magnitude faster: around a nanosecond, which is
sufficient and maybe even excessive for VHF.  As I understand it,
their jitter time is in the deep subnanoseconds.  You could maybe use
a pulse transformer or a series of pulse transformers to transform
this to higher voltage for an antenna.

I think you get about the same efficiency out of the BJT: once it’s
conducting, its voltage drop is 0.2 volts or so (compared to the
1.2–1.5 of the triac) and you’re dumping 100 volts or so compared to
the 600 of the triac.  So a single transistor can still trigger on the
order of a joule of pulse energy.

I think you can do the same trick with multiple avalanche transistors
in series, too, balancing the voltage across them with a
high-resistance divider.  1MΩ per 100V gives you 100μA of trickle
balancing current, which is way too much for battery-powered scenarios
(10 mW!) but doesn’t pose a thermal problem and would be fine for
line-powered systems.

Triggering the avalanche transistors simultaneously can be handled by
AC-coupling their bases.

Just regular transistors, sheesh
--------------------------------

But if we don’t need subnanosecond rise times, maybe we can just use a
regular transistor?  Consider a TO-92 2N7000G, rated at 60V, 200mA,
with pulses to 500mA.  It’s rated for 10ns maximum turn-on delay time
and 60 pF max input capacitance, with 3V max gate threshold voltage
and ±20V max Vgs.  Charging that gate to 10V in 5ns requires only
120mA of gate drive, which is totally a reasonable amount.  10V is the
largest Vgs shown in the V–I plot, topping out at 1500mA.  If you
wanted to discharge a 60V cap in a 10ns pulse through this FET at
1500mA, you’d need on the order of 250pF, which seems promising given
the calculations above giving 55pF as the self-capacitance of a
1-meter antenna.  (But that antenna would be on the other side of a
pulse transformer in this configuration.)

A 10ns pulse has most of its power around 200MHz, solidly in the VHF
band we want.  This gives us a pulse power of about a microjoule,
which is kind of lame.  You can get a few times more energy without
going outside VHF by just using a longer pulse, but less than an order
of magnitude.

Maybe we could do better with a regular, non-avalanche BJT, because
this FET’s Rds(on) is 5Ω, so 1500mA gives us 7.5V even in the ohmic
region.  And getting 1500mA out for 120mA in is equivalent to a β of
12.5, which you’d think most BJTs can beat.

A 2N3904 is only rated for 40V collector to emitter, but the same
200mA of collector current, and 0.2 V saturation, with min β of 30 at
100mA, up to max β of 300 at 10mA, and a transition frequency of
250–270 MHz.  That’s probably too slow; the low transition frequency
means that anywhere within the VHF band we’re getting at best β=9.
Similarly the delay time is given as 35ns and the storage time as
200ns.

So, maybe unsurprisingly, you’d probably have to use an RF BJT if
you’re not using avalanche mode.  But, maybe surprisingly,
garden-variety power MOSFETs seem like they’d be adequate.  And you
can parallel them, though maybe, given the low voltages per
transistor, it would be more useful to put them in series.

Timing precision
----------------

I think off-the-shelf quartz crystal oscillators have about 100ppm
timing error without any temperature compensation, so if we were
timing our pulses with 100ns precision we should expect to be able to
hold that over about 1ms.  This seems like an unappealingly short
integration time.

However, I think that a lot of that is long-term drift rather than i