The GreenArrays x18 core is impractical to use because you have to
program it in ArrayForth instead of Verilog or C.  As I recall it, it
has 64 18-bit words of ROM and 64 of RAM; when interpreted as
instructions, these contain up to 4 5-bit instructions.  The low-order
bits of the rightmost instruction are forced to zero, so only 8
possible instructions can fill that slot, one of which is fortunately
NOP.  Instructions with immediate operands, including jumps and calls,
use the remainder of the word as the operand.  Operands are kept on a
10-item operand stack, while a pointer register A indexes RAM and an
8-item return stack handles much control flow.  The ALU inputs are
hardwired to the top two operand-stack items, so no ALU input
multiplexing is needed; the non-control-flow instructions just
determine which direction to shift the stack, where to get the new
top-of-stack register from, and (due to immediate arguments) whether
to increment the program counter before getting to the end of the
word.

This approach frustrates pipelining but minimizes the amount of work
needed for each instruction.

Suppose that we design a processor sort of like this in which we treat
the ROM as being sort of “microcode”: it interprets a “bytecode”
loaded from a larger memory elsewhere, into which we can easily
compile programs of our choice from C or whatever.  But, as in the
x18, some of the “microcode” store is SRAM that can be written to
under the control of the bytecode, so by compiling an inner loop into
“microcode” we can run it much faster.  Among other things, we don’t
need to fetch it from von-Neumann memory for every iteration.

Even a few hundred bits of writable microcode would be sufficient to 

See also file `loop-registers.md` for another optimization for
efficiency of very simple hardware.
