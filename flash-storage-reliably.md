In erased NAND Flash memory, you can write a page at a time (typically
about 2kB or 4kB).  But you can’t overwrite existing data, and the
erase block size is much larger than a page; [`st_goliath` gives more
details][2]:

> You cannot do single byte writes on NAND. You have erase blocks
> (typically several 100k to M range) that are divided into pages
> (typically a few to several dozen k), *some* devices have sub-pages
> but that’s about it, that’s the smallest unit of data you can write.
>
> If you attempt to clear bits from 1 to 0 on an already written page,
> it will *generally* not work.
>
> I say *generally*, because I have actually tried this using raw NAND
> flash on an embedded Linux device. I found a chip, where it *did*
> work, but not very reliably.
>
> I also had an MLC chip, where it did *not* work, but instead caused
> random bit-flips on seemingly unrelated pages. I used this trick to
> systematically hammer down on the pages and figure out the pairings
> (only document in the NDA version of the datasheet), but failed to
> reproduce this on a different, more expensive MLC NAND.

[2]: https://news.ycombinator.com/item?id=41099250 (comment on 02024-07-29 by David Oberhollenzer of Innsbruck)

This means that, if you want to update existing data, it’s generally
best to write the update to a new location on the Flash rather than
trying to write it in the same place.  This is also important for data
integrity, because in order to write it in the same place, you’d have
to erase the old version of the data, resulting in a time window in
which a power loss will result in losing both the old version and the
new version.  This is in fact a commonly observed problem with SD/MMC
cards and other SSDs.

An erased page will, as I understand it, reliably read as all 1s (0xff
bytes) before ECC decoding, except possibly for bad bits.  In theory
bad bits can cause any data written on any page to read as any value
before ECC decoding, but you can drive the probability arbitrarily
low.  In practice I think this means that you can reliably distinguish
erased pages from correctly written pages, though I think the behavior
of partly written pages is more unpredictable, possibly being
correctly readable or not depending on conditions like voltage and
temperature.

Erasing is not only slow (milliseconds typically) but also costly in
another sense: erasing the same erase block many times will wear it
out and start to produce unacceptably high error rates.  This means
that it’s important to “level” this wear over the whole drive.

[Ma, Feng, and Li wrote an excellent survey of the space for ACM
Computing Surveys in 02014][4] that explains that MLC NAND typically
constrains writing even further:

> In SLC flash, pages in the same block can be programmed in any
> order.  This is of vital importance to some Flash Translation Layer
> (FTL) technologies introduced in this article, since they require
> that data should reside in offsets corresponding with their
> addresses.  Again, MLC flash disables this feature so that no page
> has to suffer from program disturbs caused by its adjacent pages
> twice [Dan and Singer 2003; Samsung 2009; Grupp et al. 2009].  It
> should be noted that the sequential page programming [restriction]
> requires that pages in a block can only be programmed sequentially,
> and there is no need to use every page.

A diagram clarifies that this means that you must write to pages in a
block in increasing order.

In general, this combination of wear leveling and large-block erasing
(even without the sequential page programming restriction) means that
you need to be able to locate any frequently updated data you store in
NAND Flash at flexible locations, to which a pointer is stored in a
table elsewhere.  Such tables ultimately form something like a tree or
DAG, with the root of the tree stored in RAM.

This poses the problem of how to find the tree root at mount time, the
so-called [“wandering tree problem”][19].

I was thinking about the kind of bitstuffing tricks discussed in file
`fast-serial-framing.md`, but those aren’t really useful in this
context, because you can’t usefully write less than a page at a time
anyway.  So all that’s really needed is to be able to reliably
distinguish an erased page from a non-erased page, at which point you
can just scan the NAND for the latest valid tree root.

The dumbest possible wear-leveling strategy is to treat the NAND as a
pure circular buffer: write pages in order from the beginning of the
NAND until the end, and when approaching the end, copy any
still-accessible data from the first erase block into the data being
written so that you can safely erase that erase block when you finally
reach the end, allowing you to wrap around to the beginning, and so
on.  This will always work as long as the amount of valid data on the
Flash is at least one erase block smaller than its total capacity, and
it erases every erase block exactly the same number of times.

If there’s a uniformly increasing timestamp or counter in every
non-erased erase block, finding the latest data added to such a
circular log is relatively straightforward.  You can treat erased
blocks as having timestamp -∞, and then you just have to search for
the unique place where block N has a *higher* timestamp t(N) than the
timestamp t(N+1) of block N+1.  Exactly those intervals [N, M]
spanning that discontinuity will have the property t(N) > t(M), so
it’s a simple matter of binary search.  A hypothetical 1-tebibyte NAND
Flash with 512KiB erase blocks (which Ma, Feng, and Li describe as
typical block and page sizes for large-block MLC Flash) would contain
2097152 = 2²¹ erase blocks and thus require up to 22 page reads to
localize the discontinuity to a single erase block.  This will take a
bit less than a millisecond, which is probably adequate for almost all
systems.

However, this baseline policy inflicts more wear on the Flash than is
necessary, because it has to copy all the surviving data on the Flash
once every lap.  But some of that data may survive for many
laps — often most of it.  It would be preferable to only copy such
long-lived data occasionally, instead choosing blocks to erase that
contain mostly or entirely non-surviving data.

However, scanning 2²¹ erase blocks to see which one has the latest
timestamp would take minutes.

The Sprite-LFS log-structured filesystem confronted a somewhat similar
problem 30 years ago, and its approach was to divide the disk into
“segments”.  There was always a “current segment” to which it was
appending data, while a “segment cleaner” task looked for promising
segments to clean out by moving their remaining live data into the
current segment.  The reasoning at the time was that disk traffic
would tend over time to become mostly writes rather than reads, I
think because RAM was growing faster than disks, and so it was
desirable to organize the disk to permit large sequential writes,
eliminating time wasted in seeking.

(In fact RAM did not grow faster than disks, so read performance
remained important.)

A significant difference is that nonsequential reads on spinning rust
are also expensive, while on Flash they’re not.  Flash is also happy
to do nonsequential writes; it just can’t do nonsequential erases.

(RAID4 and shingled magnetic recording (SMR) disks also impose
penalties on nonsequential writes but not reads, but for different
reasons.)

So a very simple and probably better variant of the baseline would be
to divide the disk into, say, 16 to 256 segments, and write the
segments one at a time, but in arbitrary order.  This probably allows
a more optimal choice of which segments to erase, and only adds an
additional 28–504 page reads at startup time to figure out what the
latest segment is before searching it for the latest updates.

However, we can probably do better than that by taking advantage of
Flash’s ability to efficiently have multiple segments open for writing
at the same time.  We can do this in two different ways.  First, we
can maintain a sort of append-only skip list of tree roots.  Second,
we can segregate data by expected longevity.

[4]: https://dbgroup.cs.tsinghua.edu.cn/ligl/papers/acm-csur-2014-flash.pdf
[19]: http://www.linux-mtd.infradead.org/doc/JFFS3design.pdf

A sort of append-only skip list of tree roots
---------------------------------------------

Suppose that we have a distinguished pair of erase blocks we use to
record new tree roots.  Every time we add a new tree root page to the
Flash, say once a second, we put it in this pair of erase blocks.
When we wrap from one of the erase block into the other, we erase the
first one.

But this is going to wear out our distinguished pair too fast; with
our hypothetical 512KiB erase blocks and 4KiB pages, we erase both of
these blocks every 256 new roots, which is to say, about 15 times
an hour.  This will reach 100’000 writes in under a year.

Very well, then.  Instead we shall have a roving tree root log erase
block.  Each new tree root page gets appended to it, and when it fills
up after 128 such writes, we allocate a new tree root log erase block
somewhere else, and append a page to our distinguished pair which
merely points to the new tree root log erase block.  This is a bit
wasteful (we’re using a 4KiB page to record 21 bits of actual
information) but the overhead is still only two erase blocks.  Now
we’ve reduced the wear on the distinguished pair by a factor of 128:
we erase it every 1048576 seconds, about 9 hours.  We won’t reach
100’000 erases for about 104 years, which is probably longer than the
service lifetime of the system.

But if we have a tebibyte Flash, it’s probably not rated for 100’000
writes of write endurance per page.  It’s probably a shitty MLC chip
with much lower numbers.  So we can add another level to our skip
list: the distinguished pair logs pointers to a second-level erase
block, which in turn logs pointers to the actual tree root log erase
block.  So every second a new page gets appended to the tree root log
erase block, every 128 seconds it fills up and a new page gets added
to the second-level erase block, every 16384 seconds it fills up and a
new page gets added to the distinguished pair, which gets entirely
erased and overwritten every 4194304 seconds, which is about a month
and a half.  This means that we will get to 10 overwrites of the
distinguished pair in a bit over a year, which may be acceptably long.

If not, we can add a third level: every second we add a new root page
to the leaf, every 128 seconds we add a new leaf block pointer at
level 1, every 16384 seconds (4½ hours) we add a new level-1 block
pointer at level 2, every 2’097’152 seconds (24¼ days) we add a new
level-2 block pointer at level 3, and every 268’435’456 seconds (8½
years) we add a new level-3 block pointer to our distinguished pair,
which means that it would take over a thousand of years to fill up
even the first block.  So it’s really just a distinguished erase
superblock.

The space overhead cost of this four-level mechanism is four erase
blocks, one for each level, and at mount time we must do a 7-read
binary search for the latest page in the latest erase block at each
level, requiring 28 page reads, which is still a bit less than a
millisecond.

This allows us to garbage-collect data at an erase-block granularity
without worrying about the impact on startup time.

Ma, Feng, and Li explain that some SLC flash chips supported “partial
page programming” in which you can program up to about 10 segments of
a page individually; this ability would increase the branching factor
of this design by an order of magnitude, probably allowing a reduction
of the number of levels.

Interestingly, this is effectively a WORM design: it works fine
without any ability to erase already-written data pages, although it
does require a limited ability to write pages out of sequence.  But it
can take advantage of the possibility offered by Flash of erasing
blocks for reuse.

All of this can be obviated with a small amount of non-volatile RAM to
store the root pointer, for example 21 bits.  NOR Flash could also be
used, but also suffers from wearout, and so it will be consumed over
time.

Segregating data by expected longevity
--------------------------------------

This is more snappily called “separating cold and hot”.

In order to clean an erase block, the erase blocks that please us most
are those containing either entirely dead data or entirely live data,
because they both cost us nothing.  Erase blocks with entirely dead
data can be erased without having to salvage any live data from them
by copying it elsewhere, while erase blocks with entirely live data
can be left where they are because there’s no point in copying them
(unless read disturb suggests it).  Blocks with a little dead data
aren’t wasting much space; blocks with a little live data can be
reclaimed cheaply.

The nasty, expensive blocks are the ones with lots of live data and
lots of dead data, because they’re wasting lots of space until we copy
lots of live data to somewhere else.

So what we really want is for data that is likely to die at the same
time to be in the same erase block.  Unfortunately, as an unknown
Danish parliamentarian observed, [it is difficult to make predictions,
especially about the future][3].  So we must fall back on heuristics.

[3]: https://quoteinvestigator.com/2013/10/20/no-predict/

There are a lot of heuristics we could use for this.  For example, we
could try to put files in the same directory, or belonging to the same
user, or having the same extension, into the same erase block.  We can
put data from the same file into the same erase block.  We can put
data created around the same time into the same erase block.  We can
segregate filesystem metadata from file contents, since it changes
more rarely, and freelist data, which changes more frequently.
Probably which of these heuristics performs best will depend on the
particular workload, and in most cases a combination will work better
than any one heuristic alone.

Whatever heuristic is used, it can probably perform better if it has
more erase blocks to choose from at any given time, and NAND is
equally happy to write a page to any currently erased block.  It’s not
especially happy to write data of less than a page, though, so we need
to lump small bits of data together to get up to the size of a page.

Filesystem byte insertion 
--------------------------

Traditional Unix and similar filesystems only support changing the
length of a file by appending to it or truncating it; bytes in the
middle of the file can be replaced, but only by the same number of
bytes.  I think this stems from the traditional implementation on
(non-SMR!) spinning-rust disks, where a file is mapped to some
sequence of fixed-size sectors plus possibly a sub-sector-sized tail,
and you can change the contents of the file efficiently by overwriting
some of the sectors.

As a consequence, applications like text editors which commonly insert
variable-length data into the middle of a file are not well served by
the filesystem, so they resort to one of the two following strategies:

1. Implement insertion and deletion of a small amount of data by
   rewriting the entire file from the beginning, possibly as a new
   file.  Typically, to implement this with acceptable efficiency
   (especially on spinning rust), they batch up many such updates in
   RAM until the user gives an explicit command to write them out.
   This is the approach taken by Emacs, ed, vi, nano, Kate, and indeed
   most text editors.  Unfortunately, if the editor or system crashes,
   a potentially large amount of work is lost.
   
2. Implement their own custom filesystem with a data model better
   suited for insertion within a file.  This is the approach taken by
   old versions of Microsoft Word and similar “OLE stream”
   applications; Vim also does this with its .swp files, while also
   supporting the traditional Unix vi :w command to rewrite the entire
   underlying file from the beginning.  Postgres also works this way.
   This approach suffers from interoperability problems, and
   application bugs can render the data in the file unrecoverable.

But, on Flash, the underlying reason for this limitation is gone; the
file’s data cannot be rewrittten in the same physical place, so
forcing it to be the same size is of little benefit.  For small files
(under 2KiB or 4KiB) and file tails, it has to be packed into a page
along with data from other files; this may be profitable for small
fixed-size updates as well, for example record updates in fixed-format
database files.

Efficient file insertion functionality is also potentially useful for,
for example, sorting; if the underlying filesystem can support
insertion with O(log *N*) performance, then simple binary-search
insertion sort becomes an O(*N* log *N*) algorithm that supports
incremental updates.  In effect the filesystem’s tree-balancing
algorithms provide the functionality of a balanced-tree index with a
simple Unix-like filesystem interface, although I don’t think it can
compete with an LSM-tree for incremental update performance.

Reflinks
--------

The traditional filesystem implementation makes it expensive to
provide a frozen-in-time snapshot of even a single file to a process
requiring a consistent view of the file, much less of multiple files
together.  If some process B overwrites a block, the operating system
would have to ensure somehow that no process A was depending on the
old contents of that block, copying the clean block elsewhere if
necessary.  This has several unfortunate results:

1. Text editors that wish to see a consistent view of a file possibly
   being updated by other processes must make a backup copy of the
   file when they open it, usually in RAM, although lazy loading would
   usually be much more efficient.  Although this works well enough to
   generally be a practical solution, it’s no guarantee of
   consistency, because the process of reading in the file at startup
   isn’t atomic with respect to writes; it can still read an
   inconsistent snapshot of the file, just with lower probability.
2. Similarly, you generally cannot back up a database tablespace at
   the filesystem level while the database is running, because it’s
   possible that the state of the parts of the file read early in the
   backup process will be inconsistent with the parts of the file read
   later.  On a large active database, this goes from “possible” to
   “almost certain”.  A few databases like SQLite offer write ordering
   guarantees that permit this to be done reliably, but most insist
   that you use the database’s internal mechanisms for backup and
   recovery.
3. Making a backup snapshot of, for example, your home directory,
   requires a large amount of space and time.

Some Linux filesystems, such as XFS, and btrfs, and OCFS2, support a
feature called “reflinks” which is similar to hardlinks, but which
makes a copy-on-write link to the current *contents* of a file.  GNU
`cp` has a `--reflink` option to make reflinks to, rather than copies
of or hardlinks to, the files it’s “copying”.  About 90% of the time
this is more useful than hardlinks.

As I understand it, Linux’s reflink facility does not support
copy-on-write links to subdirectory trees, just regular files.

In general, it’s fairly straightforward to offer reflink functionality
if data blocks are never overwritten, so this is functionality flash
filesystems can offer almost for free.  There’s just no way to expose
it through an IDE-compatible or SD-card-compatible FTL
interface — though see below about deduplication.

Reservations
------------

One of the few benefits of the traditional implementation using
spinning rust, aside from its simplicity, is that writes can never
fail — or, rather, they can only fail due to hardware failure, or when
a file is to be expanded, due to lack of space.  Overwriting data in
the middle of a file can only ever fail due to hardware failure; it
cannot fail due to a disk being full or a disk quota being exceeded.

We could imagine a situation where this becomes very serious.  A
database is participating in two-phase commit.  In the “voting” phase,
it approves the transaction and writes the tentative result into its
tablespace.  But in the “commit” phase, when it attempts to write a
commit record to its commit log, it gets an I/O error because the disk
is full.

I think a system call to reserve space for future writes should be
adequate to solve this problem.

Snapshots
---------

In 01996–7 I worked on a team of about 20 programmers at TCSI, working
on a network management system we were delivering to BellSouth.  We
stored all our project’s data on a huge rackmount 35-gigabyte NetApp
NFS fileserver, which every workstation had mounted, and contained
separate work areas for each programmer.  The NetApp WAFL filesystem
worked similarly to the Flash filesystems I’ve been outlining here:
except for the root of the DAG, data blocks were never overwritten in
place because the on-disk filesystem was stored as a DAG, with a new
snapshot written to disk every second.

Every hour, one of these snapshots was preserved for future use rather
than being replaced by a new snapshot in the following second; this
allowed you to see what a directory had looked like at the previous
hourly snapshot simply by typing

    $ cd .snapshot/hourly.0
    $ ls -l

Several of these hourly snapshots were kept (`hourly.1`, `hourly.2`,
etc.)  as well as daily and weekly snapshots.  This worked so well to
prevent accidental data loss that for months it escaped our notice
that our backup tapes were unreadable.  The expensive backup software
“Alexandria” that TCSI had licensed couldn’t handle the unreasonable
number of inodes we had created.  (We did this in order to give each
programmer a copy-on-write replica of the whole project tree,
permitting teamwide incremental compilation.)

The `.snapshot` subdirectory was accessible in every directory, but
was not enumerable with `readdir`, so as long as you didn’t name it
explicitly, the filesystem looked exactly like a conventional Unix
filesystem.

These immutable snapshots shared space copy-on-write with the active
filesystem, so sometimes we had to delete snapshots in order to free
up space.  If you were retaining two weekly snapshots, then deleting a
large file from your active filesystem wouldn’t free up any space for
two weeks.

Now that NetApp’s patents have expired, this same facility can be
efficiently provided by any filesystem that doesn’t overwrite data
blocks.  If the filesystem supports reflinks to directories, an
ordinary cron job can efficiently provide such snapshots, although
perhaps with a slightly less ergonomic user interface: if you’re in
/home/user/Downloads/hugi-1-nibbles-compofin/HOTBLACK it would be a
lot more convenient to `ls -l .snapshot/hourly.0` than to `ls -l
/home/.snapshot/hourly.0/user/Downloads/hugi-1-nibbles-compofin/HOTBLACK`.

Efficient change detection
--------------------------

Reliable, efficient incremental compilation depends on being able to
detect changes.  Some systems, like apenwarr’s implementation of redo,
use cryptographic hashes of file contents for this, but unfortunately
most filesystems do not intrinsically provide cryptographic hashes of
file contents; instead you must open the file and read it from
beginning to end in order to compute its hash.

Ideally the filesystem would provide you with some kind of “ETag”
which could be used in place of a cryptographic hash for such
purposes, something that was guaranteed to change if the file contents
changed and usually stay constant otherwise.  This would, among other
things, permit you to build a system for safely caching cryptographic
hashes of file contents so they don’t have to be recomputed.

If your filesystem is implemented in such a way that inodes and data
blocks are never overwritten, and the inode for a file contains direct
pointers to its data blocks (as opposed to, say, indices into a table
containing those pointers), you can use the physical location and
timestamp or generation number of the inode as such an ETag.  If
directories are implemented in a similarly immutable way with pointers
to particular versions of inodes (as opposed to, say, inode numbers)
then the lack of change in such an ETag for a directory guarantees
that the entire subtree under that directory is unchanged.

An alternative design that avoids copying directories up to the root
when file contents change is to store the inode contents in an “inode
file” into which the directories contain indices.  (IIRC this is how
WAFL works.)  This does not afford the same sort of efficient change
detection.

Shredding
---------

The shred(1) utility is used to request irrecoverable deletion of
sensitive data.  Ideally you wouldn’t ever write sensitive data to a
filesystem capable of providing snapshots arbitrarily far into the
past, but recovery from such an error is desirable.  Unfortunately,
shred(1) itself won’t help; it merely overwrites the data repeatedly,
which relies on the traditional implementation of the filesystem
outlined above for its irrecoverability.

XXX

Deduplication
-------------

XXX Jumprope to detect the same data in separate files to permit
reflink-like performance despite a traditional Unix interface

Literature
----------

Ma, Feng, and Li’s paper is pretty comprehensive, but would probably
be clearer if it were in Chinese.  And it’s ten years old, and it’s
restricted to the case of conventional FTLs, which attempt to provide
a simulacrum of block-based random-access spinning rust on a NAND
Flash substrate, incurring unavoidable costs of performance,
functionality, and usually reliability.

Despite those limitations, it explains not only the constraints
imposed by Flash (as of ten years ago) except for read disturb, but
also many ingenious solutions people have come up with.

I haven’t read *any* of these:

- [Forensic analysis of IoT file systems...][5]
- [Embedded Databases on Flash Memories: Performance and Lifetime
  Issues, the case of SQLite][6]
- [UBIFS][7]
- [Exploiting non-volatile RAM][8]
- [What systems researchers need to know about NAND Flash][9]
- [NANDFS for RAM-constrained systems][10]
- [Performance evaluation of flash file systems][11]
- [Design and implementation of cleaning][12], in Korean
- [Performance Analysis][13], in Korean
- [LogFS: finally a scalable flash file system][14]
- [LFTL: a multi-threaded FTL][15]
- [a concept and an open-source implementation of a digital forensics
  tool bridging this gap for the widespread UBI File System][16]
- [Flash File Systems and Deduplication during Garbage
  Collection][17], Bihani, 02010
- [F2FS][18]


[5]: https://mdpi-res.com/d_attachment/electronics/electronics-11-03219/article_deploy/electronics-11-03219-v2.pdf?version=1665472217
[6]: https://hal.science/hal-02272448/document
[7]: https://docs.kernel.org/filesystems/ubifs.html
[8]: https://websrv.cecs.uci.edu/~papers/esweek07/emsoft/p164.pdf
[9]: https://www.usenix.org/system/files/conference/hotstorage13/hotstorage13-desnoyers.pdf
[10]: https://dl.acm.org/doi/abs/10.1145/1629335.1629374
[11]: https://arxiv.org/pdf/1208.6390
[12]: https://koreascience.kr/article/JAKO200606141805682.pdf
[13]: https://koreascience.kr/article/JAKO201521056137838.pdf
[14]: https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=d5fc4ae1d0f5aba8cce023a38c90fcd3c254adf2
[15]: https://arxiv.org/pdf/1302.5502
[16]: https://www.sciencedirect.com/science/article/pii/S2666281723002081
[17]: https://d1wqtxts1xzle7.cloudfront.net/4224933/report-libre.pdf?1390836505=&response-content-disposition=inline%3B+filename%3DFlash_File_Systems_and_Deduplication_dur.pdf&Expires=1722356150&Signature=VcuZrWAZc5qzK8JV7HFQWk8mVT63PgeQosL9FCR1liFMbzbJcL2ljrKzEunYRxm~AU3m4q54fMIlOYFcW0LJfnCOvOZD6RxgAjCr0Uv38nODzRysynAg33eHoSONoKSnKzwSgIUTPFIeKNdJ50L8v4Kf6mtVdZ2puko6woZ-4FlIfT1-GVuaXXGPFo7smjh0EAjbm9hqrh3C3PvNrqqqgylX0WmEFZUmzlpuI7XckJJraBBUty17tiTwi0Mg~oUg8lTY6UOBsbLa4yt3K44mKWbUIgmq-XGc5KufC50SUActGld7fxVLsjQIvr3SYB0FlWUOh~CcgZbRXcGnoVW2iA__&Key-Pair-Id=APKAJLOHF5GGSLRBV4ZA
[18]: https://docs.kernel.org/filesystems/f2fs.html
