In a pubsub system, the message broker is often a centralized
bottleneck, like the linker in a build system.  The core functionality
of the Kafka message broker is designed to be very simple to minimize
the amount of work it has to do:

1. it validates the formatting of incoming message batches to be
   appended unchanged to an append-only log;
2. it sends the message batches out (prepended by some metadata but
   otherwise unchanged) to any connected consumers that have requested
   them;
3. it sends any requested message batches from the retained part of
   the log to the consumer that requested them by offset, which is
   used for consumers to retrieve messages they might have missed due
   to a lost connection;
4. it provides some minimal authentication, authorization, and
   multiplexing over multiple logs.

This feels like the kind of thing you could do in a few hundred lines
of assembly, executing on the order of 32 instructions per request.
The inner loop of validating a message batch might be something like
this:

    next_message:
        mov (%rax), %rcx            # Load message length (unaligned).
        lea 4(%rax,%rcx,1), %rax    # Compute address of message end.
        cmp %rdx, %rax              # Check message end against batch end;
        ja reject_batch             # if message is longer than batch, fail.
        dec %rdi                    # Decrement remaining message count.
        jnz next_message

That’s six instructions per message, probably three or four after op
fusion.  So you could plausibly handle a billion messages per second
per core on current CPUs, which is about four orders of magnitude
faster than [Kafka, which needed 20 brokers to hit 2 million messages
per second at Honeycomb][0], or [three six-core 2.5-GHz Xeons to hit 1
million messages per second][1], which works out to 56000 messages per
second per core.  A better message-batch format could plausibly get
higher ILP and thus better performance.

[0]: https://developer.confluent.io/podcast/handling-2-million-apache-kafka-messages-per-second-at-honeycomb/
[1]: https://engineering.linkedin.com/kafka/benchmarking-apache-kafka-2-million-writes-second-three-cheap-machines

(Incidentally, using strategies like this, ØMQ can get a couple
million messages per second on the laptops I’ve tried it on.  It keeps
three cores busy.  Like Kafka, ØMQ imposes minimal structure on the
messages it processes.)

The message-batch processing setup can be nearly as simple if we have
a 24-byte header containing size, producer ID, and message count as
little-endian 8-byte ints, pointed to by `%rax`:

        mov %rdx, %rax
        add (%rax), %rdx            # compute message batch end
        cmp %rbp, %rdx              # To see if we’ve received it all yet:
        ja defer_batch              # Would end be past the end of the buffer?
        cmp 8(%rax), %rsi           # Reject unless the producer ID is correct.
        jne reject_batch
        add $24, %rax               # Compute beginning of first message.
        mov -8(%rax), %rdi          # Load message count.

There are a few other checks you probably want to do, checking batch
sizes and message counts against responsiveness limits announced at
connection setup.

You don’t need to copy the message batch after validating it; your log
can consist of a linked list of message batches, or rather segments
containing successive message batches, precisely where you read them
into memory originally.  You may want to write them to disk before
acknowledging them to producers.  After a restart, you can recover
message-batch boundaries from the length fields.

Kafka assigns offsets to individual messages rather than message
batches, which is a significant difference.  This approach probably
still can’t handle more than 100 million messages per second per
broker core.

An operating system optimized for this kind of thing could do the
whole end-to-end delivery in a zero-copy fashion: the process posting
to the broker relinquishes its write access to the memory area it has
built the batch in, the broker receives a read-only mapping into its
memory space, and the broker then transmits this mapping to consumers
who should receive that message batch, appearing as a read-only
mapping in the consumers’ memory space as well.  This approach
generalizes well to zero-copy networking.
