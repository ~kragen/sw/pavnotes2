I’ve been [playing around with linear time-invariant sparse
time-domain filtering to decode data on more-or-less Kansas City
Standard cassette tapes][0], but it occurred to me that the
time-invariance of this approach is a real limit on it.  On the other
hand, the historically favored approach of counting times between
zero-crossings after some linear time-invariant analog time-domain
filtering is throwing away a lot of information that my approach takes
advantage of.

[0]: https://nbviewer.org/url/canonical.org/~kragen/sw/dev3/kansas-city-demodulation.ipynb "Demodulating a Sord 1750Hz/3500Hz variant of the Kansas City Standard"

I think a [particle-filtering approach, similar to Markov chain Monte
Carlo][1], could work a lot better.  We’re trying to do clock and data
recovery to get the bits that were originally recorded on the tape.
We could conceptualize this as attempting to determine the phase-space
trajectory of a system whose state variables of interest are ⓐ bit (0
or 1) and ⓑ clock, in the sense of “how far into the current bit are
we”, and maybe ⓒ whether data is being encoded at all or whether we’re
in a part of the tape with no data (for example, voice messages or
silence).  Moreover, each byte is bracketed by start and stop bits
with known polarity, except during break conditions
(a pilot tone of all ones); there’s a long
break condition before the program and a shorter one in between blocks
of data, so a fourth state variable of interest might be ⓓ position
within the current byte (or pilot tone).

[1]: https://ppasupat.github.io/a9online/wtf-is/mcmc.html "Gibbs Sampling, Metropolis-Hastings and Particle Filtering (Sequential Monte Carlo) are sampling-based methods for approximating distributions (joint or marginal) that are difficult to compute directly."

In order to fully predict the signal on the tape from these state
variables, you’d also need to know ⓔ tape speed, ⓕ signal amplitude, ⓖ
waveform clipping (possibly two or more scalar parameters), ⓗ
frequency response of the recording-tape-playback system (again
possibly two or more parameters), ⓘ phase delay of the
recording-tape-playback system (which varies with frequency, and thus
again possibly multiple parameters), ⓙ noise, and ⓚ dc offset.  Except
for noise, these parameters are usually constant for a whole
recording, or nearly so, though with a certain probability of
discontinuous changes.

So I propose to use a particle filter on the signal sample by sample.
After each sample, we compute the (distribution of) successor states
of each particle, then sample the new generation of particles from the
implicit distribution represented by these successor states.  Variable
ⓐ, the bit, only changes when variable ⓑ, the clock, says we’re
crossing a bit transition.  The amount ⓑ advances per sample is
determined by the tape speed ⓔ.  The most basic choice of next bit
when crossing a transition would just be to pick a bit at random, but
by using variable ⓓ we can use the start bits and stop bits to pick
20% of the bits correctly, once synchronization is achieved.

I suspect that the way to handle noise is as uncertainty in the
prediction of each sample, i.e., reducing the strength with which each
sample updates our posterior probabilities.  This implies a
phase-space variable for “noise amplitude” rather than an
instantaneous noise value.  The alternative of splattering lots of
particles around near high-probability sample values seems much less
efficient to me.

Some of these phase variables parameters are almost independent, such
as tape speed and signal amplitude, and it might be a good idea to
“crossbreed” the particles by transferring such parameters from one
particle to another from time to time, in effect improving the
necessarily fairly sparse coverage of the eleven-dimensional phase
space provided by any practical number of particles.

After having run the algorithm forward over the tape, we can use our
posterior probabilities to run it *backward* and thus recover bits we
were unable to recover the first time around, for example because of
discontinuities in amplitude or tape speed.  Because the algorithm
inherently provides a probability distribution over the possible bits
at each sample, it should be straightforward to use the
higher-confidence prediction between the forward and backward passes.

I think this representation of the problem captures its nonlinearly
time-varying dynamics in a way that an LTI filtering setup simply
cannot.  I predict that this will pay off in lower decoding error
rates.

Moreover, I think it could be extended to incorporate other
state variables that fit even more poorly into an LTI formulation,
such as unknown carrier frequencies, tape capstan speed flutter, ASCII
spaces and letters being much more probable in some stretches of tape
than control characters or characters with the 8th bit set, or even
character digraph frequencies.
