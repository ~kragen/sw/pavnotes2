Darius Bacon has suggested a “COMFY FORTH.”  What about a comfy script
shell?

    fac n =
        if n < 2 then
            1
        else
            fac (n - 1) + fac (n - 2)
        fi

In the comfy script shell, as in Icon, commands can succeed or fail,
and comparisons such as `x == y` or `x < 2` do not evaluate to a
Boolean value; they fail instead of succeeding if they fail to hold,
otherwise evaluating to their right argument.  More ordinary commands
such as `x := y - z` or `print x` (the application of the function
`print` to the value of the variable `x`) always succeed.
Pattern-matching destructuring assignment also can fail.

When a command succeeds, control simply passes to the command
following it, if any.  When it fails, control instead passes to the
current failure handler, which depends on the lexical context of the
command.  There are three compound commands that determine fail
handling: `if`/`then`/`else`/`fi`, `while`/`do`/`done`, and
`not`/`ton`.

The command `if <cond> then <cons> else <alt> fi` sets up a failure
handler for `cond` which jumps to `alt`.  Thus `if x > 0 then x := x -
1 else x := 80 fi` sets up a failure handler for `x > 0` so that, if
`x` is in fact not greater than zero, control passes to the command `x
:= 80`.  Also, `if` introduces an unconditional transfer of control
from `x := x - 1` to its end; control never passes from the `then`
clause to the `else` clause.  `else` is optional; if it is omitted,
the effect is as if there were an empty `else` clause.  The failure
handler in effect for `cond` is not in effect for `cons` or `alt`, so
if they fail, they jump to the failure handler in the context around
the `if` command.

Any of these clauses may be multiple commands.  In the case of the
`if` condition, any one of the commands failing will transfer control
to `alt`, so in effect command sequencing is short-circuit
conjunction.  So, for example, `if x >= 0; x < max then a[x] := 1 fi`
only sets the array element when `x` is in [0, `max`).

The `while <cond> do <body> done` command likewise sets up a failure
handler for `cond` which transfers control to the success after the
end of the loop, and an unconditional jump from the end of `body` back
to `cond`, while failures in `body` cause the `while` as a whole to
fail.  As with `else`, an omitted `do` is permitted.  Compound
conditions in `while` are useful for constructs like `while line :=
readline input; line != "" do process line done`.  However, in this
case, since `process line` cannot fail, being a simple subroutine
call, the `do` could simply be replaced with a semicolon.

The weird one is `not <cond> ton`.  This fails if execution reaches
the end of `cond` successfully, while succeeding if `cond` fails.  So
`not x < 0 ton` fails if `x` is greater than or equal to zero.

Commands can be separated by semicolons or line endings
interchangeably.  Parentheses provide grouping.  Line endings inside
parentheses do not separate commands.  Infix operators have lower
precedence than function invocation.  The operator precedence
hierarchy is not a total order; some combinations of operators require
explicit parenthesization.

The above rules permit a low-line-noise language which, if perhaps a
bit verbose, can be efficiently parsed with minimal lookahead.
