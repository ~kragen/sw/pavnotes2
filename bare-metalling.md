On #bootstrappable Googulator explained some of the relevant
differences between bringing up a new OS on bare metal and bringing it
up in QEMU; reformatted and with spelling corrections, they said:

> Most bare metal issues come from using a graphical console (vs a
> serial one), a commercial BIOS with DOS/9x backwards compatibility
> hacks (vs SeaBIOS without such hacks), a real IDE/SATA/SCSI/NVMe
> disk subsystem that reports some fake CHS geometry (vs. virtio
> that’s just a pure block device), a real network adapter spanning
> the whole OSI stack (vs. usermode-emulated networking with “nothing”
> below the IP layer), a real USB topology where not everything is
> directly connected to the root hub, or memory map differences.
>
> Some of these may be testable by using different QEMU options
> (e.g. not disabling graphical console, or using explicit IDE or SCSI
> disk emulation), but most of it won’t be caught if emulating via
> QEMU.  And, of course, no way to capture textual output from an
> emulated graphical terminal.
> 
> There’s actually one more key difference: QEMU doesn’t emulate real
> silicon, wires and electromagnetic fields between the various
> functional units, which are all present on bare metal.  Which means,
> if you clobber some MMIO registers in QEMU, it will likely render
> the relevant functional unit unusable until you reinitialize it
> properly; but not affect the _rest_ of the system.  Do it on bare
> metal, and you lock up, instantly reboot, get weird malfunctions in
> seemingly unrelated places, or even damage hardware; because
> clobbering MMIO  often causes components to malfunction in _analog_
> ways.
>
> Classic example is writing garbage to the PCIe root complex
> registers.  In QEMU, you lose PCIe until you reprogram the root
> complex correctly.  On bare metal, the misconfigured PCIe block
> sends a 3GHz sawtooth wave through pins that aren’t supposed to have
> high frequencies present, inducing eddy currents in nearby traces of
> the PCB, that will interfere with the CPU’s attempts to communicate
> with DRAM.  The only emulator that gets even remotely close to
> emulating such behavior is MAME, which is way too slow to bootstrap
> on, if it even supports x86-32.
>
> Even simpler — MUX the same UART to 2 sets of pins on a real
> Raspberry Pi, vs. one emulated in QEMU. In QEMU, IIRC it just works.
> On a real Pi, best case scenario is you get a weak signal on both
> pins that works with short cable runs, but not longer ones.
>
> And, of course, QEMU’s emulated Intel SATA controllers aren’t made
> up of transistors that wear out with use; unlike real ones in
> certain chipsets.

I asked, “Flash, or are they just driving regular transistors way too
hard?”, and they responded:

> No, it was an actual silicon bug that forced a recall of several
> chipsets back in the day.
