What I'd really like is a way to publish human-readable content hashes
for things like secure library imports, even in a way that requires
embedding some authenticator data in the library, and I'm wondering if
this series/parallel inversion trick can be somehow applied to it.
This presumably cannot be done in a postquantum-secure fashion.  The
length required for a human-readable content hash depends in part on
how many (classical) hash operations we need to defend against, which
in turn depends on our attacker's computational speed, computational
parallelism, and the cost of an individual hash operation.

One way to make a hash operation more expensive is to require you to
hash repeatedly with different nonces until you come up with a hash
with a required number of zeroes on the end, as in Hashcash and
Bitcoin.  If you require 10 zeroes on the end of the hash, you have
increased the cost of performing (but not verifying) the hash by a
factor of 1024, in a parallelizable way.

If instead you require feeding the hash output into its input 1024
times you have also increased the cost of performing the hash by a
factor of 1024, but not in a parallelizable way, and you've also
increased the cost of verifying it.  If you then chop off the last 10
bits of the hash, you've reduced the number of hash operations
required to brute-force a second preimage by a factor of 1024, getting
you back to where you started but with a shorter hash.

If the attacker can perform a billion hashes per second on each of a
million nodes for a decade, that's about 2<sup>50</sup> operations per
second, 2<sup>78</sup> in a decade.  So they can successfully attack a
79-bit hash but not an 80-bit one.  Current teraflops GPUs, much less
Bitcoin ASICs, suggest that this is about three orders of magnitude
too optimistic; the Bitcoin network is computing 8 × 10¹⁸
double-SHA256 hashes per second, which is 2<sup>63</sup> per second or
2<sup>91</sup> per decade.

A 92-bit random number represented as text might look like
beboha-samara-junaje-muroni-sadifo-guso,
than-bay-creed-mused-cloak-vice-poor-ropes, `EJBOu@71j2|jhu`, 专竈攊菵
頠挷嫠, or ADA-WAY-MISS-RIFT-REP-NESS-BOSS-BREW-BEAD, depending on
your chosen representation.  This is clearly somewhat unwieldy as a
library import.

Memory-hard hash functions like scrypt() are perhaps more promising
here.  Supposing a GiB of RAM costs US$1, then a billion-dollar-budget
attack can marshal, more or less, 2<sup>30</sup> GiB-seconds of scrypt
hashpower per second or 2<sup>58</sup> GiB-seconds per decade.  So, if
your library is signed with a GiB-second of scrypt(), then a 59-bit
hash will secure it for a decade against a billion-dollar attack.

A 59-bit random number represented as text might look like
bojamu-bamena-hohuhe-nune, iv-click-slim-scale-picks, `` Y?9Hp48`0 ``,
丁輵粳浢誅, or AIM-FUNK-GAIT-JACK-JUDO-BURL.  These are somewhat more
readable.

But suppose we use the zero-extending hash-stretching approach
described above and dedicate 9 hours of a 16-GiB laptop or cellphone
(US$0.10, though cheaper ways to get this much compute exist) to
signing the library, still only using 1-GiB-second scrypts.  This
allows us to compute 2<sup>19</sup> hashes, probably one of which will
have 19 leading zero bits, so we need only 40 bits of hash to secure
the library against the same attack.  And it still only requires 1
GiB-second of scrypt computation to verify the hash.

A 40-bit random number represented as text might look like
bedejo-gejuge-hafa, on-elton-eyed-crop, `";IX37W`, 嗧兘胎, or
CUT-SULK-LAB-SHUT.  Now we're into correct-horse-battery-staple
territory; these seem like very reasonable ways to specify a library
version to import or a repository version to import it from, or for
that matter an onionnet host to connect to or a GPG public key, and
this approach doesn't even require embedding an authentication nonce
into the library.

(In fact, even a 48-bit random number is correct-horse-battery-staple
territory: danagi-magobo-nimeba, clara-stew-width-min, `$OmUi^>O`, 丘
銙嶾歐, or ALP-SULK-GOLD-TALE-RUB.  Under the same assumptions this
defends against a US$256 billion classical attacker for a decade.)

If we're willing to require 16 times as much signing effort, 6 days on
that laptop, we can squeeze that down to 36 bits: birudo-mujebi-do,
cloak-uses-kpmg, `&d%N+]`, 也鵯掶, or AD-SOFA-SNAG-GUSH.  Another
factor of 8, 48 days, gets us down to 33 bits: hifesu-horofe,
nine-map-beer, `z"RTl`, 丏頚飙, or RICE-WRIT-GAWK.  These are more
memorable but the improvement is probably not worth the slowdown.

With public-key cryptography, we could pregenerate public/private key
pairs and precalculate these signatures, so this doesn't have to
involve 9 hours or 48 days of latency every time you want to publish a
new library version.  Public-key signatures have the advantage and
disadvantage of a sort of malleability: you can sign multiple library
versions with the same private key, which is an advantage to
publishers who want to publish a channel of updates but a disadvantage
to verifiers who would like to know that the library version they're
trying to pin hasn't been replaced with a different version signed by
the same key.

Single-use private keys like Merkle's original hash-based signature
system don't solve this problem: a malicious author can sign a
malicious version of a library with a key previously used to sign a
safe version, and the only punishment single-use private keys inflict
upon them is that malicious third parties can use the information thus
revealed to sign still more malicious versions.  This doesn't help the
verifier.

I don't know of a way to pregenerate hashes that avoids this problem,
not even through incentives, and on the face of it, it sounds
impossible.

For non-bleeding-edge cases, the library import mechanism could use a
catalog of libraries which carries the signature, so your program
header might look like this:

    with clara-stew-width-min
    use csv, collections, errno, os
    use requests 2.3
    use kquery 1.7

This retrieves a catalog with content-hash `clara-stew-width-min`,
imports the (immutable) latest versions of `csv`, `collections`,
`errno`, and `os` from it, and the non-latest versions 2.3 of
`requests` and 1.7 of `kquery`.  The catalog might be a text file that
looks like this:

    csv 1.0 43bb 1ceb 0b93 9796 acd4 5c71 9ae2 614c a338 a1cc 5f4b e584 6205 2fe5 fe33 50bb
    collections 3.8 458c 85aa cd88 3147 7788 ed44 0827 5b00 f1c9 31a6 2a7d 84d7 4c46 dc05 25c5 c8c4
    collections 3.7 9980 22f3 9315 a829 97bd b0d3 c605 87ff 3d78 b082 6296 af5b 4dfc 9585 7fd1 4bd2

