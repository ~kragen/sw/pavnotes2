Emacs ships with an asm-mode, which is mostly bad with a little bit of
good; I’ve used asm-mode to write a few thousand lines of assembly for
five architectures (RV64, 8086, i386, amd64, and my favorite, ARM32)
and I hate asm-mode.  I tried Charles Jackson’s arm-mode, but I like
it even less.  I tried finding ARM assembly code he’d written to see
what his desired style was, but I couldn’t find any on his GitHub
page.

So I thought I’d look at how I’d like my editor to help me format my
assembly and how I think the editor should behave to make that
maximally easy, and what else the editor could do to help me write
assembly.

Example assembly language
-------------------------

### ARM’s own example ###

[The ARM developer web site offers this example code][0]:

            AREA ThumbSub, CODE, READONLY   ; Name this block of code
            ENTRY                           ; Mark first instruction to execute
            CODE32                          ; Subsequent instructions are ARM
    header  ADR     r0, start + 1           ; Processor starts in ARM state,
            BX      r0                      ; so small ARM code header used
                                            ; to call Thumb main program
            CODE16                          ; Subsequent instructions are Thumb
    start
            MOV     r0, #10                 ; Set up parameters
            MOV     r1, #3
            BL      doadd                   ; Call subroutine
    stop
            MOV     r0, #0x18               ; angel_SWIreason_ReportException
            LDR     r1, =0x20026            ; ADP_Stopped_ApplicationExit
            SWI     0xAB                    ; Thumb semihosting SWI
    doadd
            ADD     r0, r0, r1              ; Subroutine code
            MOV     pc, lr                  ; Return from subroutine
            END                             ; Mark end of file

[0]: https://developer.arm.com/documentation/dui0068/b/Writing-ARM-and-Thumb-Assembly-Language/Structure-of-assembly-language-modules/An-example-Thumb-assembly-language-module

There are some things I don't like about how this code is formatted:

1. The labels don't have colons after them.  Colons make the code more
   readable and permit the use of free-form syntax.
2. The mnemonics are in capital letters.  Capital letters are
   **SHOUTY**.
3. Assembler directives like `CODE32` don't have gas’s leading `.`
   sigil to distinguish them from instructions.  (On ARM’s site they
   are in a different color.)
4. The `add` instruction has a redundant argument.
5. The comment character is `;`, but I like `@` better even though
   it’s noisier.

[ARM themselves have abandoned this syntax][2] in favor of GNU syntax
as of 02014.

[2]: https://developer.arm.com/documentation/dui0742/c/migrating-arm-syntax-assembly-code-to-gnu-syntax

There are some things I do like about it:

1. The labels, mnemonics, arguments, and comments are each aligned to
   particular columns, making them easy to pick out visually.  A
   reasonable alternative to this would be indenting the mnemonics to
   show structure (but I still don't like starting them on the left
   side.)
2. Only the labels are in the left column, which makes them easy to
   find.
3. When needed, there are lines without instructions to make space for
   the paragraphs of marginal comments.
4. There are spaces after the commas.
5. Constants do have sigils.
6. Neither mnemonics nor registers nor (references to) labels have
   sigils.  The `$rbx` and `%rbx` stuff in gdb and gas is irritating.

Maintaining a reasonably filled paragraph in a comment column like
that is a real pain in the ass without editor support.

### glibc setjmp ###

Here's part of the code for setjmp and sigsetjmp in glibc:

            mov     ip, r0

            /* setjmp probe expects sigsetjmp first argument (4@r0), second
               argument (-4@r1), and target address (4@r14), respectively.  */
            LIBC_PROBE (setjmp, 3, 4@r0, -4@r1, 4@r14)

            /* Save sp and lr */
    #ifdef PTR_MANGLE
            mov     a4, sp
            PTR_MANGLE2 (a4, a4, a3)
            str     a4, [ip], #4
            PTR_MANGLE2 (a4, lr, a3)
            str     a4, [ip], #4
    #else
            str     sp, [ip], #4
            str     lr, [ip], #4
    #endif
            /* Save registers */
            stmia   ip!, JMP_BUF_REGLIST

I mostly like how this is formatted, with lower-case registers and
mnemonics, and a well-defined operand column, but there are things I
don’t like.

This is not extremely assemblyish; it's leaning heavily on the C
preprocessor, and has correspondingly disagreeable macro names and
non-label things in the left column.  OTOH I’m not sure how you’re
supposed to define something like `JMP_BUF_REGLIST` with the gas macro
system.

I generally prefer comments-to-end-of-line over C-style multiline
comments; they’re less confusing.

I don't really like the use of the ABI register names a4, ip, etc.  I
don't mind lr, sp, and pc, but I have a hard time remembering whether
a4 is r3, r7, or what.  (It’s r3.)

For context, the `PTR_MANGLE2` stuff, which I think is intended to
harden against is defined in arm/sysdep.h, which is almost entirely
cccp stuff with a little bit of assembly mixed in, with definitions
like this:

    #if (IS_IN (rtld) \
         || (!defined SHARED && (IS_IN (libc) || IS_IN (libpthread))))
    # ifdef __ASSEMBLER__
    #  define PTR_MANGLE_LOAD(guard, tmp)                                   \
      LDR_HIDDEN (guard, tmp, C_SYMBOL_NAME(__pointer_chk_guard_local), 0)
    #  define PTR_MANGLE(dst, src, guard, tmp)                              \
      PTR_MANGLE_LOAD(guard, tmp);                                          \
      PTR_MANGLE2(dst, src, guard)
    /* Use PTR_MANGLE2 for efficiency if guard is already loaded.  */
    #  define PTR_MANGLE2(dst, src, guard)          \
      eor dst, src, guard

### old\_timer’s code ###

[Here is some code whose formatting I don't like at all:][1]

    add r0,r1,r2
    .thumb
    add r0,r1,r2
    .syntax unified
    add r0,r1,r2

Everything in the left margin, no spaces after commas, no blank lines.
At least it uses .sigils for directives, no sigils for registers and
mnemonics, and lowercase.

[1]: https://stackoverflow.com/a/76032185

### My own i386 code ###

Although ARM is much nicer, i386 is still the assembly language I’m
most comfortable with.  Here’s a subroutine from Qfitzah, which I
wrote in it.

            ## `assq` is our function for doing a variable lookup in an
            ## environment.  To avoid an extra unconditional jump, I’ve
            ## relocated the tail end of the loop to before the loop entry
            ## point, which has the bizarre effect of putting it before
            ## the *procedure* entry point.  It happens to set ZF when it
            ## succeeds and clear ZF when it fails, but `subst` ignores
            ## that at the moment.  It might make things simpler if it
            ## returned its key argument when it fails, like `walk` from
            ## μKanren?

    2:      cdr %ecx                # go to the next item before falling into assq
    proc assq                       # look up an item %eax in a dictionary %ecx
    1:      jnpair %cl, 1f          # nil or another atom terminates the dict
            car %ecx, %edx          # get the item
            cmp %eax, 0(%edx)       # is our dictionary key this item?  CISC 4ever!1
            jne 2b                  # if not, restart the loop, or
            mov %edx, %ecx          # on success we return the item, or on failure
    1:      xchg %ecx, %eax         # return the non-pair we were examining
            ret
            ## Possibly it would be better to inline `assq` as a macro
            ## inside `subst`, since that’s the only thing that uses it so
            ## far.

This is reasonably okay but I think there are some things that would
improve it:

1. Impossible in gas, but I wish I could put the `assq` label at the
   beginning of the line instead of preceding it with the macro name
   `proc`.  Similarly when defining a macro it would be very desirable
   for the macro name to be in the left margin.
2. It would be better if the numerical (local) labels were indented
   somewhat, leaving the left margin with only global labels.  Other
   parts of the code violate this more blatantly.
3. The operands do not form a nice column.  In this case the
   macro instruction `jnpair` demands a bit more space than usual.
   (`car` is also a macro instruction.)
4. gas’s choice of `#` as a comment character is very questionable
   given that people often use gas with cccp or some other cpp.
5. The register names and `%` sigils are noisy.
6. Again, I think this is impossible in gas, but `proc assq:` would be
   significantly more readable.

##### Trying for something better #####

Doing #2 and #3 would look like this:

        2:  cdr     %ecx            # go to the next item before falling into assq
    proc assq                       # look up an item %eax in a dictionary %ecx
        1:  jnpair  %cl, 1f         # nil or another atom terminates the dict
            car     %ecx, %edx      # get the item
            cmp     %eax, 0(%edx)   # is our dictionary key this item?  CISC 4ever!1
            jne     2b              # if not, restart the loop, or
            mov     %edx, %ecx      # on success we return the item, or on failure
        1:  xchg    %ecx, %eax      # return the non-pair we were examining
            ret

It might be nice to have a bit more space for the labels than the
traditional 8 spaces, particularly with the colon and space needed for
readability, which leave you only 6 letters for a name on the same
line with an instruction (though this code doesn’t use normal label
names anyway).  12, for example, cutting the operands column from 16
spaces to 15 and the comments column, assuming 80 columns total, from
48 spaces to 45 (including the 2 spaces for the comment character and
following space).

          2:    cdr     %ecx           # Go to the next item, then fall into assq.
    proc assq                          # Look up an item %eax in a dictionary %ecx.
          1:    jnpair  %cl, 1f        # nil or another atom terminates the dict.
                car     %ecx, %edx     # Get the item.
                cmp     %eax, 0(%edx)  # Is our dict key this item?  CISC 4ever!1
                jne     2b             # If not, restart the loop, or
                mov     %edx, %ecx     # on success we return the item, or on 
          1:    xchg    %ecx, %eax     # failure return the non-pair we were
                ret                    # examining.

If we want the editor to be able to refill comment paragraphs in the
comment column, which is pretty crucial, it's probably useful to
denote paragraph breaks somehow, ideally something that takes up a
little less space than an entire blank line, because the above code
block would then look like this:

          2:    cdr     %ecx           # Go to the next item, then fall into assq.

    proc assq                          # Look up an item %eax in a dictionary %ecx.

          1:    jnpair  %cl, 1f        # nil or another atom terminates the dict.

                car     %ecx, %edx     # Get the item.

                cmp     %eax, 0(%edx)  # Is our dict key this item?  CISC 4ever!1

                jne     2b             # If not, restart the loop, or

                mov     %edx, %ecx     # on success we return the item, or on 
          1:    xchg    %ecx, %eax     # failure return the non-pair we were
                ret                    # examining.

A hanging indent might be the best tradeoff, since the space cost is
only borne by the second and later lines of the paragraph:

          2:    cdr     %ecx           # Go to the next item, then fall into assq.
    proc assq                          # Look up an item %eax in a dictionary %ecx.
          1:    jnpair  %cl, 1f        # nil or another atom terminates the dict.
                car     %ecx, %edx     # Get the item.
                cmp     %eax, 0(%edx)  # Is our dict key this item?  CISC 4ever!1
                jne     2b             # If not, restart the loop, or
                mov     %edx, %ecx     # on success we return the item, or on 
          1:    xchg    %ecx, %eax     #    failure return the non-pair we were
                ret                    #    examining.

XXX
