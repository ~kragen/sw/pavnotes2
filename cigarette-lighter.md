I have a Bunsen-burner-type pocket cigarette lighter¹ (“torch lighter”
or “jet lighter”) which I just refilled, an operation which takes an
astonishingly short time compared to recharging a battery.  It
occurred to me that the energy content of the device is probably
pretty high; its external dimensions are 27 mm × 12 mm × 80 mm, for a
total volume of about 26 mℓ.

Unfortunately I didn’t have the presence of mind to measure its weight
before and after refilling it, and emptying it is somewhat
inconvenient because of course it has no lock to hold it open and is
not readily coaxed to eject liquid butane, so it takes many minutes to
empty, and I don’t have an appropriate clamp or vise.  But I guessed
that half of this volume, 13 mℓ, was liquid butane.  (As we will see
later, this turned out to be high by almost an order of magnitude.)

Wikipedia says that at 27° liquid butane’s density is 571.8±1 kg/m³ at
up to 2 MPa, which would give 7.4 g; it seems unlikely for the lighter
to exceed this pressure, particularly since its vapor pressure is
1 atm (101 kPa) around 0° and 170 kPa around 10°.  Its HHV is 49.1
MJ/kg and its LHV is 45.3 MJ/kg; taking the LHV, this lighter would
contain about 340 kJ, equivalent to a 25000 milliamp-hour 3.7-volt
lithium-ion battery or a 19000 milliamp-hour USB power bank, which are
much larger devices.

______  
¹ Incorrectly called “catalytic” in Argentina, because some of them
are catalytic and the vulgar don’t know what catalysis is; this one
lights with a piezo spark.

Lighter testing
---------------

After emptying it, the lighter weighs 19.66 g, 19.63–19.65 on a second
reading after power-cycling the shitty scale.  After filling it, it
weighs 20.98 g, 21.00 g in a second weighing after attempting further
filling, and 21.04 g after warming up the butane bottle in hot water
and attempting further filling.  This leads to the surprising
conclusion that this Magiclick-brand lighter only holds about 1400 mg
of butane (63 kJ), lower than I estimated above by more than a factor
of 5.  It heats up to the point of tending to burn fingers if run for
more than a minute at a time, but after a minute and a half of runtime
it weighs 20.88 g; after another 30 seconds (2′ total), 20.83 g; and
at this point I have to pause because it smells of burning plastic and
is giving off visible smoke.

This suggests a fuel consumption rate of about 1.7 mg/s, about 80
watts, which seems pretty realistic, and a time to empty of almost 14
minutes of continuous operation, which also sounds about right, though
it’s pretty poor operation toward the end of that time.

After another 30 seconds (2′30″ total) it still weighs in at 20.83 g,
demonstrating how shitty my scale is.

I ran it for another minute (3′30″ total) blowing out the flame, just
spewing out unburned butane into the room, and now it’s 20.67 g.
370 mg over 210 seconds is closer to 1.8 mg/s.

After an additional minute (4′30″ total) without the flame, it’s at
20.53 g; 510 mg over 270 seconds is closer to 1.9 mg/s, but the
difference is evidently within measurement error.

Upon refilling again it went back to 21.04 g.

### Later lighter testing ###

I got a damaged "Inova" lighter.  It weighs 12.99 g.  After running it
for 30 seconds it still weighs 12.99 g, which I guess is to say that
it's within measurement error.  I guess that makes sense if it's close
to 1.9 mg/s.  After emptying it for a few minutes it weighs 12.57 g,
so it probably also contained on the order of a gram of fuel and spat
it out at a few milligrams per second.

Current political considerations
--------------------------------

Are there practical obstacles to carrying around such a butane-powered
hand tool?  Perhaps on airlines — as well as disposable butane
lighters (but not “torch lighters”) FAA regulations (49 CFR §175.10)
permit airline passengers to carry up to two fuel cell fuel cartridges
of up to 200 mℓ of “liquid”; for butane and other “liquefied gases”
there is a slightly lower limit.  There is no specific limit on
disposable butane lighter capacity, but large ones are not allowed.
Regulations vary in other countries.

TEG tools
---------

You could imagine an *actually* catalytic device using butane and a
[thermoelectric generator, with a typical efficiency of 5–8%][0] which
would give you probably 2.9 kJ per g of fuel, assuming 6.5%
efficiency.  This is pretty poor compared to things like steam
turbines, but those might not scale down to pocket-sized devices very
well due to difficulties in insulation.

[0]: https://en.wikipedia.org/wiki/Thermoelectric_generator#Efficiency

6.5% of 80 watts is 5 watts.

Fuel-cell-powered tools
-----------------------

The
