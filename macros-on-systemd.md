I asked my friend macros about his experience with Linux’s switch to
systemd.  His answer was thought provoking (somewhat edited, hopefully
without introducing errors):

> I have been happy with the change and have written several tools
> making use of the D-BUS APIs.
> 
> `journald` has a poor implementation of a decent idea.  Not having
> multiple log systems is nice.
>
> The state tracking is nice, you can look at the status of any unit
> and get the last logs and current state.  There is a [deeper version
> of a ready state that is possible if you integrate with systemd.][0]
> 
> The override mechanisms are nice: you can drop a file in `/etc` to
> override or add to an existing unit.  There is a `systemctl edit`
> that wraps it too.  The tooling to show the resolved final config is
> good: `systemctl cat` gives you the resolved output.
> The override pattern is baked into our Chef tooling, useful for
> things like applying cgroup limits.
> 
> I’m happy to give up all the self-daemonization crap — just do
> reasonable things with stderr/stdout and run forever.
> 
> Timers are a nicer experience than cron.
> 
> Mounts has been nicer than automounters.
> 
> `systemd-boot` is nice for the less flexible cases.
> 
> `systemd-network` has been better than Debian net scripts; the
> resolver stuff I’m still a little meh on.
> 
> The user systemd stuff is nice; we ran `runit` on top of Ubuntu for
> services and replaced it with systemd only.  Being able to mark
> services into cgroups and kill them all is really nice, `runit`
> would leave us messes all the time to clean up from shit that didn’t
> daemonize the right way.
>
> I prefer writing systemd unit files to SysV init scripts, though
> honestly I don’t really do that since both Chef [and NixOS][1] have
> nicer abstractions.
>
> Some of the socket activation stuff is nice, haven’t done a ton with
> it.  systemd creates the socket and hands it off to the newly spun
> up daemon.
> 
> There are also “one-shot” jobs: “run this command and log the
> output,” essentially.  This can be used a pre and post conditions
> for other pieces — it turns that script into a service equivalent,
> that can be inserted into the boot process and/or run before another
> service, logging the output and last run in a consistent way.
> 
> Journald on the root device… — what a steaming pile of shit that
> should never touch disk!  Use it as a memory buffer and rsyslogd for
> writing to disk or other host.  You just have to specify in configs.
> 
> [Configuring it this way], you get the memory logs per service which
> are usually enough for status and a bit, but...you lose some of [the
> queryability journald normally gives you].  You can use Splunk or
> whatever downstream if you actually need to query, beyond the “tail
> what is going on now” case, which gets most things.  The query
> features are part of what makes it so expensive on disk — it isn't
> just “buffer up some strings and write them out,” it is a B-Tree
> database with a fair bit of write amplification.  And done via mmap.
> But yay for lots of features!
> 
> They really wanted a binary on-disk format and lots of features
> around attestation and remote shippping. I think it is a bad
> default. More of a special purpose tool.

[0]: https://vincent.bernat.ch/en/blog/2017-systemd-golang
[1]: https://nixos.wiki/wiki/NixOS:extend_NixOS
