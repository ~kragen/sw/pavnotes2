I was thinking about how slices in Golang and ranges in D and C++ get
used where we’d use pointers in C or assembly.

The motivating example with nul-terminated strings
--------------------------------------------------

I wrote this bit of C code today (in `dev3/masde4.c`) for parsing a
nul-terminated string with a set of user-specified delimiters:

    char *find_cset(char *s, char *delims)
    {
      for (; *s; s++) {
        for (char *t = delims; *t; t++) if (*s == *t) return s;
      }
      return s;
    }

The newly returned pointer splits the original string into a token up
to the first delimiter (if any) and a remaining part starting with the
delimiter.

To me this seems like a very nice and convenient way to express the
algorithm; it doesn’t even seem particularly obfuscated, though
chaining the `for`, `if`, and `return` all on the same line is
questionable.  On amd64, GCC 12.2.0 turns it into this with -Os: <!--
─┌┐└┘┶━►◄│╭╮╯╰ -->

    12cc:           mov rax, rdi          ; Initialize return value as s
    12cf:           mov cl, [rax] ◄─────╮ ; load byte from *s
    12d1:           test cl, cl         │ ; return s if nul byte
    12d3: ╭──────── je 12ef             │
    12d5: │         mov rdx, rsi        │ ; t = delims
    12d8: │         mov dil, [rdx] ◄──╮ │ ; load byte from *t
    12db: │         test dil, dil     │ │ ; check for end of delims
    12de: │ ╭────── je 12ea           │ │ ; terminate inner loop if so
    12e0: │ │       cmp dil, cl       │ │ ; *s == *t?
    12e3: │ │  ╭─── je 12ef           │ │ ; return s if equal
    12e5: │ │  │    inc rdx           │ │ ; move to next delimiter
    12e8: │ │  │    jmp 12d8 ─────────╯ │ ; repeat inner loop
    12ea: │ ╰──│──► inc rax             │ ; move to next input byte
    12ed: │    │    jmp 12cf ───────────╯ ; repeat outer loop
    12ef: ╰────┶━━► ret                   ; return current input position

These 15 instructions seem like a pretty reasonable way to implement
this algorithm, though rotating the loops to put the unconditional
jumps outside them would probably make it run faster, and of course
vectorizing it would probably make it run much faster.

Problems with the simple example
--------------------------------

The bigger issue, though, is that the semantics aren’t 8-bit clean; it
uses in-band signaling with nul bytes for end of string, giving rise
to an abundance of bugs.  Golang slices or D ranges (also in Boost and
C++20) have better semantics and seem like they should be just as
efficient, or even more so, if we presuppose that we have plenty of
registers, like 16 on amd64 or arm32, or 32 on arm64 or RISC-V.

(I’m using instruction count as a very crude estimate of the amount of
work required to carry out the algorithm.)

A couple of difficulties are:

1. Semantically this subroutine returns two ranges, not one, and
   languages like C don’t accommodate that well.
2. The syntaxes for slices and ranges is more verbose than the pointer
   syntax, to the point of obscuring the algorithm being expressed.

Implementation with ranges in assembly
--------------------------------------

But first let’s look at how we might implement the same algorithm with
ranges.  Let’s suppose our slices consist of two pointers and are
[passed in rdi, rsi, rdx, and rcx][0], and rbx, rsp, rbp, and r12–r15
are call-preserved, as per the standard amd64 ABI.  And let’s suppose
(contrary to that ABI) that our first returned slice (the parsed
token) is in rax and rdx, while the second one (the remaining input)
is in rdi and rsi.

One of the nice things about this ABI is that the default return value
is in rax, which isn’t even an argument register, much less the first
one, so your return value register can be live at the same time as
your first argument; this frequently saves you a mov.  So I’m sort of
cheating a little bit by making my *second* return value be the first
argument.

[0]: https://en.wikipedia.org/wiki/X86_calling_conventions#System_V_AMD64_ABI

Here’s an untested version done this way:

    find_cset:
            mov rax, rdi    # Parsed token starts at the beginning of input.
        1:  cmp rdi, rsi    # Are we at the end of input?
            je 1f           # If so, go to end of parsing.
            mov r9b, [rdi]  # Load input character into r9l (AMD calls it r9b)
            mov r8, rdx     # Load first delimiter pointer into r8
        2:  cmp r8, rcx     # Check for last delimiter
            je 2f           # If so, exit inner loop
            mov r10b, [r8]  # Load delimiter into r10l
            cmp r10b, r9b   # Is this delimiter found here?
            je 1f           # If so, we’re done.
            inc r8          # Otherwise, move to next delimiter
            jmp 2b          # and repeat inner loop.
        2:  inc rdi         # If no more delimiters, go to next input byte
            jmp 1b          # and repeat outer loop.
        1:  mov rdx, rdi    # End of token is beginning of remaining input.
            ret

This is 16 instructions, one instruction longer than the purely
pointer-based version, and it’s probably slightly more efficient,
because its 7-instruction inner loop exits one instruction sooner and
does #delims memory accesses instead of 1+#delims.  But it’s probably
splitting hairs, since if you’re looking for efficiency, you probably
should vectorize.  (Incidentally, vectorizing is a lot easier if you
don’t have to go looking for a null byte to figure out how far to
look.)

Implementation with ranges in C 
--------------------------------

We can express this in C by leaning rather heavily on by-value struct
passing (untested):

    typedef struct { char *b, *e; } range;
    typedef struct { range a, b; } two_ranges;

    two_ranges find_cset(range s, range delims)
    {
      char *b = s.b;
      for (; s.b != s.e; s.b++) {
        for (char *t = delims.b; t != delims.e; t++) {
          if (*t == *s.b) return (two_ranges) { { b, s.b }, s };
        }
      }
      return (two_ranges) { { b, s.b }, s };
    }

Not counting the struct declarations, this is about 50% longer than
the pointer version, and significantly uglier.  Because the AMD64 ABI
does struct returns with a hidden first parameter passed in rdi, the
compiled result is a little worse, but not outrageously so.  GCC also
loads the input character in the inner loop, though, which is a worse
sin.  However, it improves over the version I wrote above in that it
directly compares to a memory argument, saving an instruction in the
inner loop (though perhaps no micro-ops). <!-- ─┌┐└┘┶━►◄│╭╮╯╰┅┈┊ -->

       0:             mov rax, rdi          ; (uselessly move return pointer)
       3:             mov rdi, rsi          ; Save a copy of s.b in rsi.
       6:             cmp rdi, rdx ◄──────╮ ; s.b == s.e?  s.e is in rdx
       9:  ╭───────── je 25               │ ; Exit loop if so.
       b:  │          mov r9, rcx         │ ; Copy delims.b into r9 (t)
       e:  │          cmp r8, r9 ◄─────╮  │ ; delims.e is in r8; equal?
      11:  │  ╭────── je 20            │  │ ; If no more delims, break.
      13:  │  │       mov r10b, [rdi]  │  │ ; Load *s and
      16:  │  │       cmp [r9], r10b   │  │ ; compare to *t;
      19:  │  │  ╭─── je 25            │  │ ; return if we found a delimiter.
      1b:  │  │  │    inc r9           │  │ ; t++;
      1e:  │  │  │    jmp e ───────────╯  │ ; try the next delimiter.
      20:  │  ╰──│──► inc rdi             │ ; Increment to next input byte.
      23:  │     │    jmp 6 ──────────────╯ ; Next iteration of outer loop.
      25:  ╰─────┶━━► mov [rax], rsi        ; Return token begin,
      28:             mov [rax+0x8], rdi    ; token end,
      2c:             mov [rax+0x10], rdi   ; remaining input begin, and
      30:             mov [rax+0x18], rdx   ; remaining input end.
      34:             ret

That’s 19 instructions.  Admittedly, when I give it more reasonable
optimization flags, like -O5, GCC produces more reasonable code; I
won’t quote the whole thing here because it’s 28 instructions, but the
inner loop looks like this, eliminating both the unnecessary memory
access and the unnecessary unconditional jump:

      1a:           ╭─── jmp 29
      1c:           │    nop DWORD PTR [rax+0x0]
      20:           │    add rax,0x1 ◄──╮
      24:           │    cmp r8,rax     │
      27:        ╭──│─── je 48          │
      29:        │  ╰──► cmp [rax],dil  │
      2c:        │       jne 20 ────────╯

But of course -O5 can’t eliminate the extra 4 instructions at the end
which store the result into the caller’s stack frame, nor the pointer
argument needed to do that.

XXX include C++, D, and Golang expressions of the same algorithm with
ranges, ranges, and slices

Maybe nicer syntax
------------------

A more ideal syntax for this kind of thing would look something like
this:

    (bs, bs) find_cset(bs s, bs delims)
    {
      bs tok = s;
      for (; s; ++s) {
        for (bs t = delims; t; ++t) {
          if (s[0] == t[0]) return (bs(tok.b, s.b), s);
        }
      }
      return (tok, s);
    }

Here we’re expropriating the pointer preincrement operator to signify
removing the first item from such a range and the [0] syntax to
signify accessing that first item, and evaluating empty ranges as
false and other ranges as true.

A C++ attempt
-------------

I think we can almost achieve this in C++, and I’m curious whether the
more pleasant syntax would cost us any efficiency.  A strawman
implementation looks like this:

    using std::string, std::pair, std::make_pair;

    class bs {
    public:
      char *b, *e;
      char& operator[](int i) const { return b[i]; }
      bs& operator++() { b++; return *this; }
      explicit operator bool() const { return b != e; }
    };

    pair<bs, bs> find_cset(bs s, bs delims) {
      bs tok = s;
      for (; s; ++s) {
        for (bs t = delims; t; ++t) {
          if (s[0] == t[0]) return make_pair(bs(tok.b, s.b), s);
        }
      }
      return make_pair(tok, s);
    }

With -Os this compiles to the following 21-instruction subroutine,
almost identical to the C version, though with a few more optimization
blunders:

       b:             mov rax, rdi          ; (uselessly move return pointer)
       e:             mov rdi, rsi          ; Save a copy of s.b in rsi.
      11:             cmp rdx, rdi ◄────────╮ ; s.b == s.e?  s.e is in rdx
      14:  ╭───────── je 38                 │ ; Exit loop if so
      16:  │          mov r9, rcx           │ ; Copy delims.b into t.b
      19:  │          cmp r9, r8 ◄───────╮  │ ; delims.e is in r8; equal?
      1c:  │  ╭────── je 33              │  │ ; If no more delims, break.
      1e:  │  │       inc r9             │  │ ; Next delimiter.
      21:  │  │       mov r10b, [r9-0x1] │  │ ; Load delimiter from RAM.
      25:  │  │       cmp [rdi], r10b    │  │ ; Compare to input char at rdi.
      28:  │  │       jne 19 ────────────╯  │ ; Next loop unless equal.
      2a:  │  │       mov [rax], rsi        │ ; rsi:rdi is desired token
      2d:  │  │       mov [rax+0x8], rdi    │ ; since we’ve found a delimiter.
      31:  │  │  ╭─── jmp 3f                │ ; Exit loop.
      33:  │  ╰──│──► inc rdi               │ ; Increment input pointer.
      36:  │     │    jmp 11 ───────────────╯ ; Repeat outer loop.
      38:  ╰─────│──► mov [rax], rsi
      3b:        │    mov [rax+0x8], rdx  ; rdx, end of input, equals rdi!
      3f:        ╰──► mov [rax+0x10], rdi
      43:             mov [rax+0x18], rdx
      47:             ret


This enables the following code to work, so it’s not completely
untested:

    for (;;) {
      auto toknt = find_cset(t, delims);
      std::cout << "[" << toknt.first << "]";
      t = toknt.second;
      if (!t) break;
      char delim = t[0];
      std::cout << "{" << delim << "}";
      ++t;
    }
    std::cout << "\n";

Perhaps a slightly more convenient interface in the C++ context would
be one that modifies the byteslice in place, so it doesn’t have to
return pairs:

    for (;;) {
      bs tok = t.tok(delims);
      std::cout << "[" << tok << "]";
      if (!t) break;
      char delim = t[0];
      std::cout << "{" << delim << "}";
      ++t;
    }
    std::cout << "\n";

This can be implemented simply on top of the above, in this case as a
method:

    bs bs::tok(bs delims) {
      pair<bs, bs> result = find_cset(*this, delims);
      *this = result.second;
      return result.first;
    }

If I encourage the compiler to inline `find_cset`, with -Os it
compiles `bs::tok` to these 20 instructions:

    14e2:             mov r9, [rdi]             ; Load this->b into r9
    14e5:             mov r10, [rdi+0x8]        ; and this->e into r10.
    14e9:             mov rax, r9               ; Copy this->b into rax.
    14ec:             mov r8, rax ◄───────────╮ ; Overwrite r8 with rax.
    14ef:             cmp r10, rax            │ ; Compare rax to end of input.
    14f2:  ╭───────── je 150f                 │ ; If no more input, return.
    14f4:  │          mov rcx, rsi            │ ; Reinitialize rcx from delims.b.
    14f7:  │          cmp rcx, rdx ◄───────╮  │ ; Compare to delims.e.
    14fa:  │  ╭────── je 150a              │  │ ; If no more delimiters, break.
    14fc:  │  │       inc rcx              │  │ ; Increment delim pointer.
    14ff:  │  │       mov r11b, [rcx-0x1]  │  │ ; Fetch delimiter skipped.
    1503:  │  │       cmp [rax], r11b      │  │ ; Compare to input byte.
    1506:  │  │       jne 14f7 ────────────╯  │ ; Repeat loop if unequal.
    1508:  │  │  ╭─── jmp 150f                │ ; (useless unconditional jump)
    150a:  │  ╰──│──► inc rax                 │ ; Increment input pointer.
    150d:  │     │    jmp 14ec ───────────────╯
    150f:  ╰─────┶━━► mov [rdi], r8             ; update this->b from r8
    1512:             mov rax, r9               ; return value b in rax
    1515:             mov rdx, r8               ; return value e in rdx
    1518:             ret

Duplicating the input pointer between r8 and rax wastes space but
probably not time, and similarly for the useless unconditional jump
after the end of the inner loop.  Avoiding the wasteful ABI calling
convention for the pair of byte slices makes this routine actually
shorter than the one it called, allowing it to work with almost no
extra memory accesses — except that the input byte read should be
hoisted out of the inner loop.

(As before, -O5 does do that hoisting.)

A difficulty with this implementation of the idea in C++ is that it
can’t handle pointers to string literals, because its instance
variables are pointers to chars, not const chars.  If you don’t want
to cast away the constness, you can fix that as follows.

    template<typename T>
    class slice {
    public:
      T *b, *e;
      T& operator[](int i) const { return b[i]; }
      slice& operator++() { b++; return *this; }
      explicit operator bool() const { return b != e; }
    };

    using bs = slice<char>;
    using cbs = slice<const char>;

This requires you to make `find_cset` also a template.  Also in C++20
(maybe earlier?) you can whittle it down a bit by making the pair
construction implicit:

    template<typename T, typename U>
    pair<slice<T>, slice<T>> find_cset(slice<T> s, slice<U> delims) {
      auto tok = s;
      for (; s; ++s) {
        for (auto t = delims; t; ++t) {
          if (s[0] == t[0]) return { { tok.b, s.b }, s };
        }
      }
      return { tok, s };
    }

Also, though, the iteration over the delimiters is a simple ranged for
loop; by adding .begin() and .end() methods we can write it this way:

    (...)
      T *begin() const { return b; }
      T *end() const { return e; }
    (...)

    template<typename T, typename U>
    pair<slice<T>, slice<T>> find_cset(slice<T> s, slice<U> delims) {
      auto tok = s;
      for (; s; ++s) {
        for (auto c : delims) if (s[0] == c) return { { tok.b, s.b }, s };
      }
      return { tok, s };
    }
    
Comparing this to the C version we started with:

    char *find_cset(char *s, char *delims)
    {
      for (; *s; s++) {
        for (char *t = delims; *t; t++) if (*s == *t) return s;
      }
      return s;
    }

I think it’s easy to prefer either one.
