If you have a bunch of vertical stripes that you can rapidly turn on
and off independently, like really tall pixels, you can use them to
show an arbitrary 2-D (XY) moving image to a camera with a rolling
shutter, by using the subdivision of time provided by the rolling
shutter to get a vertical spatial subdivision.  Vsync may be a
problem.
