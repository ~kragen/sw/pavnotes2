The equation that makes water counterproductive for magnesium fires is:

> Mg + 2H₂O = Mg(OH)₂ + H₂

Or, rather, the large decrease in Gibbs free energy as you move to the
right-hand side.

This is interesting as a form of energy storage, for example as a
rocket engine fuel, because of the light weight of the ingredients.
Magnesium is only 24.3 daltons and water is only 18.0, or 36.0 for two
of them.  This 24:36 mass ratio would under normal conditions work out
to about a 14:36 volume ratio given magnesium's normal density of
1.738 g/cc.

That's much lighter than any other source of oxygen that's in a
condensed-matter phase at STP, except for the disagreeably reactive
hydrogen peroxide; normally the oxidizer in rocket engines is either
peroxide, cryogenic, or much heavier than whatever it oxidizes.
Magnesium stands almost alone in its ability to take advantage of this
unique oxidizer.  The resulting exhaust temperature is likely to be
inconveniently high, wasting much of the possible mechanical energy in
forms like optical radiation, but this can be remedied by injecting
additional inert gases such as hydrogen into the reaction to reduce
those losses.  The use of MgH₂ rather than metallic magnesium is a
potentially interesting measure there, though it would be more
difficult to store than metallic magnesium.

It would be especially interesting, despite the obvious risks, to make
a hydrogel of the magnesium, which is to say a foam with
nanometer-sized pores.  You cannot of course form a magnesium hydrogel
by the sol-gel process using water as the solvent; reducing the
magnesium to its metallic state in a water seems at best impractically
difficult.  Some researchers have reported success at similar tasks
using tetrahydrofuran as a solvent instead of water, and it might be
possible to form a magnesium gel by this route and then displace the
THF in its pores with water.

Glycerin is a potentially interesting alternative to water for this
purpose, despite its useless carbon content.
