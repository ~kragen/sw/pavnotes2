As I was writing file `micromonitor.md` I was thinking about VGA
modes.  Poor old crazy Terry Davis said 640×480 with 16 colors was the
video mode God decreed he had to use, but IIRC there were quite a
number of widely supported VESA video modes with less shitty color
rendering.

[QEMU emulates][0] a Cirrus CL-GD5446 by default before QEMU 2.2, and
since then “Standard VGA card with Bochs VBE extensions,” which I
guess uses the “VESA [2.0][1] VBE extensions”.  [VESA modes sound like
a bit of a pain][2].  There’s [a short summary][4] from MIT's
[Operating System Engineering][5] class which says:

> 
>     INT 10 - VESA SuperVGA BIOS - SET SuperVGA VIDEO MODE
>             AX = 4F02h
>             BX = mode
>                  bit 15 set means don't clear video memory
>                  bit 14 (VBE2) set to enable linear framebuffer mode
>     Return: AL = 4Fh function supported
>             AH = status
>                  00h successful
>                  01h failed

But then maybe you have to call a different function to find out where
the framebuffer is?

>     INT 10 - VESA SuperVGA BIOS - GET SuperVGA MODE INFORMATION
>             AX = 4F01h
>             CX = SuperVGA video mode
>             ES:DI -> 256-byte buffer mode information (see below)
>     Return: AL = 4Fh function supported
>             AH = status
>                  00h successful
>                  01h failed
> 
>     Format of mode information:
>     Offset Size     Description
>      00h   WORD     mode attributes
>                     bit 0: mode supported if set
>                         1: optional information available if set
>                         2: BIOS output supported if set
>                         3: set if color, clear if monochrome
>                         4: set if graphics mode, clear if text mode
>                         5: (VBE2) non-VGA mode
>                         6: (VBE2) No bank swiotching supported
>                         7: (VBE2) Linear framebuffer mode supported
>      02h   BYTE     window A attributes
>                     bit 0: exists if set
>                         1: readable if set
>                         2: writable if set
>                     bits 3-7 reserved
>      03h   BYTE     window B attributes (as for window A)
>      04h   WORD     window granularity in K
>      06h   WORD     window size in K
>      08h   WORD     start segment of window A
>      0Ah   WORD     start segment of window B
>      0Ch   DWORD -> FAR window positioning function (equivalent to AX=4F05h)
>      10h   WORD     bytes per scan line
>     ---remainder is optional for VESA modes, needed for OEM modes---

...

>     --- VBE v2.0 ---
>      28h   DWORD    Physical address of linear video buffer
>      2Ch   DWORD    Pointer to start of offscreen memory
>      30h   WORD     Offscreen memory in Kbytes

[4]: https://pdos.csail.mit.edu/6.828/2010/readings/hardware/vgadoc/VESA.TXT
[5]: https://pdos.csail.mit.edu/6.828/2004/

The [VESA 3.0 standard][3] lists an 80×60 text mode 108h and a number
of appealing standard modes:

    | 100h | 640×400   |   256 |
    | 101h | 640×480   |   256 |
    | 103h | 800×600   |   256 |
    | 105h | 1024×768  |   256 |
    | 107h | 1280×1024 |   256 |
    | 112h | 640×480   | 16.8M |
    | 115h | 800×600   | 16.8M |
    | 118h | 1024×768  | 16.8M |
    | 11Bh | 1280×1024 | 16.8M |

But it says:

> Starting with VBE version 2.0, VESA will no longer define new VESA
> mode numbers and it will no longer be mandatory to support these old
> mode numbers. OEM’s who wish to add new VBE mode numbers to their
> implementations, must make sure that all new modes are defined with
> mode numbers above 0x100, However, it is highly recommended that
> BIOS implementations continue to support these mode numbers for
> compatibility with older software and add new mode numbers after the
> last VESA defined mode number).

So it seems like you ought to be able to, in real mode, do something
like this to get a pointer to a linear 1280×1024 video buffer, if
that’s possible on your card:

    mov ax, 4f02h
    mov bx, 118h     ;XXX wrong see below
    int 10h
    mov ax, 1234h
    mov es, ax
    xor di, di
    mov ax, 4f01h
    mov cx, 118h    ;XXX prolly still wrong see below
    int 10h
    mov di, es:[28h]
    mov ax, es:[30h]
    mov es, ax

That’s only 12 instructions!  That’s not too terrible.  [Omarrx024 has
written a tutorial with code that actually works][8], though.

Hmm, later the standard says:

> The format of VBE mode numbers is as follows: ... D14 = Linear/Flat
> Frame Buffer Select. If D14 == 0, Use Banked/Windowed Frame
> Buffer. If D14 == 1, Use Linear/Flat Frame Buffer.

So I guess you want mode 4118h, not just 118h.

In terms of how to call it, the standard says:

> VBE functions are called from real mode using the INT 10h software
> interrupt vector ... All VBE functions are called with the AH
> register set to 4Fh to distinguish them from the standard VGA BIOS
> functions. The AL register is used to indicate which VBE function is
> to be performed.

And I guess you use AL to specify which VESA function to invoke, and
somehow you get a pointer to your framebuffer:

> Once you have initialized the graphics hardware into a mode that
> supports a hardware linear framebuffer, you need to create a pointer
> that your application can use to write to the linear framebuffer
> memory. The first thing you should realize is that the linear
> framebuffer location reported in the ModeInfoBlock for the mode you
> are using is a physical memory address, and cannot be used directly
> from protected mode applications.

Also there might be multiple buffers and you might want to schedule a
change between them:

> Hardware triple buffering is supported in the VBE/Core specification
> by allowing the application to schedule a display start address
> change, and then providing a function that can be called to
> determine the status of the last scheduled display start address
> change. VBE Function 4F07h is used for this, along with subfunctions
> 02h and 04h.

Oh, the “window A/window B” stuff above presupposes bank switching,
ew:

> One area that is of vital importance is the Windows Granularity, or
> the smallest increment that you can move the bank switching
> window. Many controllers simply provide a 64Kb granularity, which
> means that bank 0 starts at 0, bank 1 at 64Kb and bank 2 at 128Kb
> etc. However some controllers may provide either a 4Kb or a 16Kb
> granularity. For a 4Kb granularity controller, bank 0 starts at 0
> while bank 1 starts at 4Kb not 64Kb. On a 4Kb system to move the
> window to the next 64Kb location you would need to program bank
> number 8 rather than bank number 1.

I guess that’s what function 5 is for: repositioning your window onto
VRAM.

There’s also apparently a protected-mode interface:

> In addition to the INT 10h interface and protected mode entry point,
> a simple 32-bit Protected Mode Interface is available and is
> described below. In cases where there is both a standard interface
> and a 32-bit protected mode interface, the register parameters for
> both interfaces will be defined one after another for the function
> specification.

Good thing it’s *simple*.  Hmm, how does it work?

> The application or OS does not call the BIOS code directly from
> protected mode, but first makes a copy of the BIOS image in a
> writeable section of memory and then calls the code within this
> relocated memory block. The entry point is located within a special
> ‘Protected Mode Information Block’, which will be located somewhere
> within the first 32Kb of the BIOS image (if this information block
> is not found, then the BIOS does not support this new interface).

Zarathustra’s dandruff.

Apparently [some people run the BIOS in v86 mode][7] to avoid this.

UEFI machines use [GOP, the Graphics Output Protocol][6], instead.
But it has one huge limitation:

> It also provides an efficient BitBlitter function, which you can't
> use from your OS unfortunately. GOP is an EFI Boot Time Service,
> meaning you can’t access it after you call ExitBootServices().
> However, the framebuffer provided by GOP persists, so you can
> continue to use it for graphics output in your OS.

[0]: https://www.qemu.org/docs/master/system/qemu-manpage.html
[1]: https://en.wikipedia.org/wiki/VESA_BIOS_Extensions#VESA_BIOS_Extensions_(VBE_core)_2.0_(November_1994)
[2]: https://wiki.osdev.org/VESA_Video_Modes
[3]: https://pdos.csail.mit.edu/6.828/2004/readings/hardware/vbe3.pdf
[6]: https://wiki.osdev.org/GOP
[7]: https://wiki.osdev.org/How_do_I_set_a_graphics_mode
[8]: https://wiki.osdev.org/User:Omarrx024/VESA_Tutorial
