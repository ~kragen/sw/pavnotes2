Blackstrap is a sweet, thick, black programmable text editor and
virtual machine.  Its distinguishing feature is that it is very
minimal.  The Blackstrap executable for Linux is expected to be 6KiB,
most of which results from interfacing with Xlib.  You invoke the
executable with the path to an image, which is a filesystem directory;
if the image does not exist, it will be set up with the initial state.

In the initial state, it works as a primitive hexadecimal text
notepad, but you can extend it.  The first thing you will want to do
is probably add some more font glyphs, and then probably some mouse
commands, and then maybe an interpreter or assembler or something.

The Blackstrap virtual machine has a small big-endian 32-bit
instruction set, a keyboard interrupt, a mouse interrupt, a 1024x512
32-bit TrueColor screen, and transparently persistent memory without
address translation.  

Here is the virtual machine's memory map, in hexadecimal:

- 0x0 - 0xfff: nonexistent memory addresses, trap on access
- 0x1000: system reset handler address, also invoked on illegal
  instructions and accesses to nonexistent memory addresses
- 0x1004: keyboard interrupt address
- 0x1008: mouse interrupt address
- 0x100a: timer interrupt address
- 0x0000 1000 - 0x3fff ffff: ordinary RAM
- 0x8000 0000 - 0x8020 0000: framebuffer, row-major order, ABGR byte
  order within each pixel
- 0x8100 0000: last 32-bit keyboard scan code
- 0x8100 0004: last 32-bit mouse event

In the machine's initial state, the system reset handler address
points to 0x2000, where a monitor is loaded.  This monitor sets up the
keyboard interrupt to point to its keyboard handler and points the
mouse and timer interrupts at a nop handler.

The initial monitor implements a simple text editor with 32 lines of
128 characters, all of which are on the screen at once; a text cursor;
a mouse cursor; and an 8x16 fixed-width font containing the digits 0-9
and the letters a-f at their ASCII positions, in different colors,
while all other glyphs are blank.  Out of the box, it implements the
following commands:

- ^X: parse hexadecimal number preceding text cursor and invoke the
  subroutine at that address: *call*.
- ^J: parse hexadecimal number preceding text cursor, read the 32-bit
  word of memory at that location, and overwrite the 8 characters to
  the right of the cursor with 8 hexadecimal digits that say what word
  was stored there: *peek*.
- ^T: parse hexadecimal number preceding the text cursor as an
  address, the hexadecimal digits to the right of the text cursor as a
  32-bit word, and store that word at that address: *poke*.
- ^B: move text cursor left
- ^N: move text cursor down
- ^P: move text cursor up
- ^F: move text cursor right
- Enter: move text cursor to beginning of next line
- backspace: delete character to the left of the cursor, shifting the
  rest of the line left and inserting a space at the end
- all other keys: insert the extended ASCII byte corresponding to the
  key at the cursor position

By default, these key commands are buffered up if they are received
while Blackstrap is busy doing something, but the Esc key instead
aborts the execution of whatever was happening and inserts the
instruction address into the buffer at the text cursor, in
hexadecimal.

Initially the mouse doesn't do anything.

The initial monitor uses the following memory map:

- 0x2000-0x3000: text screen contents, one byte per cell
- 0x20000-0x40000: font, 512 bytes per glyph, same format as
  framebuffer except that each glyph is contiguous

The monitor has callable addresses for functions like 

I wanted to use octal rather than hexadecimal in order to save on
initial fonts, but decided otherwise for the following reasons:

- In hexadecimal, the peek-a-memory-word command dumps four bytes; in
  octal, either you need 6-bit or 9-bit bytes, or you need a separate
  peek-byte command.  Likewise for poke.
- In octal, natural instruction formats have 8 general-purpose
  registers, which is uncomfortably small, or 64, which is too many.
  In hex, you have 16, which is more comfortable.
- I was having a hard time writing down the memory map in octal
  because of the long strings of zeroes.

The instruction set is a simple RISC instruction set with 16
general-purpose registers, integer arithmetic, bitwise operations, bit
shifts, a RISC-V-style add-immediate instruction, byte and word load
and store instructions with immediate offsets, compare-and-branch
instructions rather than status flags, and a branch-and-link
instruction that stores a return address in a register.  To facilitate
writing machine code by hand in hexadecimal, instruction-format bit
fields are all 4-bit aligned, and even jump addresses are absolute
rather than PC-relative, though there is a RISC-V-like AUIPC
instruction and an indirect instruction, so that generating PIC isn't
impossible.

There's a WFI instruction which pauses execution until the next
interrupt, which as a side effect updates the screen and the
transparent-persistence system image in the disk file.  There's a
YIELD instruction that has the same effect but continues execution
immediately.

Rather than using memory-mapped I/O, there's a separate IO instruction
which takes parameters in registers.  This provides access to a blob
store within the image directory and to audio output.
