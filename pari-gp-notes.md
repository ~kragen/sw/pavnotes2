I’ve been playing a bit with the calculator program PARI/GP, though
Eeyore recommended GAP.  Things I’ve learned to do with PARI/GP, all
instantly, include arbitrary-precision arithmetic, solving systems of
linear equations, symbolic derivatives, LaTeX output, computing
inverses of finite field elements, include polynomial division,
symbolic integration, finding roots of polynomials (symbolically),
approximation with Taylor series, exact rational arithmetic,
continued-fraction approximation, binomial coefficients, composing and
computing properties of permutations, enumerating the partitions of a
number, factoring integers, plotting (in ASCII art, X-Windows,
PostScript, or SVG), *symbolic* linear algebra, *multivariate*
symbolic differentiation, computing prime numbers in a range,
arithmetic in non-prime finite fields, non-field arithmetic modulo a
base, loops, lambdas, higher-order functions, set comprehensions,
subroutines (with default parameters), random number generation, and
parallelizing work across all 8 of my cores.  And I’ve only started to
scratch the surface of what it can do.  [The user's guide][0]
explains:

> PARI/GP is a specialized computer algebra system, primarily aimed at
> number theorists, but has been put to good use in many other
> different fields, from topology or numerical analysis to physics.
>
> Although quite an amount of symbolic manipulation is possible, PARI
> does badly compared to systems like Axiom, Magma, Maple,
> Mathematica, Maxima, or Reduce on such tasks, e.g. multivariate
> polynomials, formal integration, etc. On the other hand, the three
> main advantages of the system are its speed, the possibility of
> using directly data types which are familiar to mathematicians, and
> its extensive algebraic number theory module (from the
> above-mentioned systems, only Magma provides similar features).

It has tab completion (including arguments), command-line editing,
persistent command-line history, and online help:

    ? ?contfrac
    contfrac(x,{b},{nmax}): continued fraction expansion of x (x rational,real or 
    rational function). b and nmax are both optional, where b is the vector of 
    numerators of the continued fraction, and nmax is a bound for the number of 
    terms in the continued fraction expansion.

In general everything I try in it on this laptop is close to
instantaneous.  Except when I tried to plot the sinc function with
computation precision set to 10000 digits, which errored out after a
few seconds.

It includes integration with Emacs through something called PariEmacs
and with TeXmacs.

The language is an interpreted Lisp with both static and dynamic
scoping (originally only the latter), without (mutable) aliasing, and
with a C-targeting compiler called `gp2c`, with conventional infix
syntax as syntactic sugar.  It spawns a traditional Lisp break-loop
debugger on errors.

How to do particular things
---------------------------

### Basic algebraic manipulation ###

Despite the disclaimer above, it’s pretty convenient for basic
algebraic manipulation like division of polynomials:

[0]: https://pari.math.u-bordeaux.fr/pub/pari/manuals/2.11.1/users.pdf

    ? [(b*a^2) \ (a+b)^2, (b*a^2) % (a+b)^2]
    %2 = [b, -2*b^2*a - b^3]

Better:

    ? divrem((b*a^2), (a+b)^2)
    %3 = [b, -2*b^2*a - b^3]~

Or their multiplication:

    ? (5*x^2 + 3*x + 3) * (x - 5)
    %14 = 5*x^3 - 22*x^2 - 12*x - 15

Or substitution:

    ? f = x^2 + 3*x - 1
    %3 = x^2 + 3*x - 1
    ? subst(f, x, y-1)
    %4 = y^2 + y - 3

To restore `f` as a free variable, you can use a quoted variable:

    ? f
    %5 = x^2 + 3*x - 1
    ? f = 'f
    %6 = f
    ? f
    %7 = f
    ? subst(f, x, y-1)
    %8 = f

But you can’t quote things that aren’t variable names.

You can find polynomial roots (note that I have \\p precision set low
here):

    ? polroots(y^6 + y^5 + 1)
    %105 = [-1.11 - 0.420*I, -1.11 + 0.420*I, 0.746 - 0.482*I, 0.746 + 0.482*I, -0.140 - 0.942*I, -0.140 + 0.942*I]~

It can approximate with a power series:

    ? f = 1/(x^2 + 3) + O(x^16)
    %31 = 1/3 - 1/9*x^2 + 1/27*x^4 - 1/81*x^6 + 1/243*x^8 - 1/729*x^10 + 1/2187*x^12 - 1/6561*x^14 + O(x^16)

Or do symbolic derivatives:

    ? deriv(5*x^2 + 3*x + 3)
    %1 = 10*x + 3
    ? f'
    %36 = -2/9*x + 4/27*x^3 - 2/27*x^5 + 8/243*x^7 - 10/729*x^9 + 4/729*x^11 - 14/6561*x^13 + O(x^15)
    ? f''
    %37 = -2/9 + 4/9*x^2 - 10/27*x^4 + 56/243*x^6 - 10/81*x^8 + 44/729*x^10 - 182/6561*x^12 + O(x^14)
    ? f'''
    %38 = 8/9*x - 40/27*x^3 + 112/81*x^5 - 80/81*x^7 + 440/729*x^9 - 728/2187*x^11 + O(x^13)

### Fractions, continued fractions, and GCD ###

It lets you calculate with rational numbers:

    ? (3/8)*(1+1/3)
    %1 = 1/2

And of course that means it can compute a GCD or greatest common
divisor:

    ? gcd(42, 24)
    %1 = 6
    ? gcd(2*x^2 + 6, x^3 + 3*x)
    %5 = x^2 + 3
    ? gcdext(42, 24)
    %63 = [-1, 2, 6]
    ? gcdext(2*x^2 + 6, x^3 + 3*x)
    %64 = [1/2, 0, x^2 + 3]

This extended version of the algorithm tells you what multiples of the
operands to add to get to the gcd:

    ? -1 * 42 + 2 * 24
    %65 = 6
    ? 1/2 * (2*x^2 + 6)
    %66 = x^2 + 3

It can do continued fractions:

    ? \p 10
       realprecision = 19 significant digits (10 digits displayed)
    ? contfrac(Pi)
    %27 = [3, 7, 15, 1, 292, 1, 1, 1, 2, 1, 3, 1, 14, 2, 1, 1, 2, 2]

And use them to compute approximants:

    ? [bestappr(Pi, 10^i) | i <- [0..10]]
    %31 = [3, 22/7, 22/7, 355/113, 355/113, 312689/99532, 1146408/364913, 5419351/1725033, 245850922/78256779, 2549491779/811528438, 23602834920/7513015697]
    ? %[#%-1] - Pi
    %32 = 5.738077256 E-19

(You can really see the Perl influence there.)

### Arbitrary precision ###

It can do arbitrary-precision arithmetic:

    ? \p 128
       realprecision = 134 significant digits (128 digits displayed)
    ? Pi
    %2 = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446

### Forcing a numerical result ###

You can approximate a result numerically:

    ? 1+1/(3 + 1/(1 + 5))
         25
    %6 = --
         19

    ? 0.+%
    %7 = 1.3157894736842105263157894736842105263

### Combinatorics: binomial coefficients, partitions, permutations ###

Factorials are built in, and arbitrary precision makes this easy:

    ? 1000!/998!/2!
    %10 = 499500

Or better:

    ? binomial(1000, 2)
    %13 = 499500

For enumerating the partitions of an integer I have to increase the
stack size limit:

    ? default(parisizemax, 32 000 000)
      ***   Warning: new maximum stack size = 32002048 (30.520 Mbytes).
    ? #partitions(50)
      *** partitions: Warning: increasing stack size to 16000000.
      *** partitions: Warning: increasing stack size to 32000000.
    %17 = 204226
    ? numbpart(50)
    %18 = 204226
    ? #divisors(60)
    %34 = 12

Exponentiation and multiplication on `Vecsmall`s treats them as permutations:

    ? p = Vecsmall([3,1,4,2,5])
    %19 = Vecsmall([3, 1, 4, 2, 5])
    ? p^2
    %20 = Vecsmall([4, 3, 2, 1, 5])
    ? p^3
    %21 = Vecsmall([2, 4, 1, 3, 5])
    ? p^4
    %22 = Vecsmall([1, 2, 3, 4, 5])
    ? permorder(p)
    %23 = 4
    ? p^3 * p
    %24 = Vecsmall([1, 2, 3, 4, 5])

### Meta-stuff: formats, performance ###

It can tell you how long any result took to compute:

    ? 100000!/99998!/2!
    %7 = 4999950000
    ? ##
      ***   last result: cpu time 48 ms, real time 48 ms.

It can output formulas in LaTeX:

    ? printtex((5*x^2 + 3*x + 3) * (x - 5)/y)
    \frac{5}{y}\*x^3
     - \frac{-22}{y}\*x^2
     - \frac{-12}{y}\*x
     - \frac{-15}{y}

Or ASCII art:

    ? default(output,3)
    ? (5*x^2 + 3*x + 3) * (x - 5)/y
         5   3   -22   2   -12      -15
    %3 = -\*x  - ---\*x  - ---\*x - ---
         y        y         y        y

It can plot in ASCII art, too:

    ? plot(x = -10, 10, sin(x)/x)

    0.9958061 |'''''''''''''''''''''''''''''_""_'''''''''''''''''''''''''''''|
              |                            _    _                            |
              |                                                              |
              |                           x      x                           |
              |                                                              |
              |                          x        x                          |
              |                                                              |
              |                         _          _                         |
              |                         :          :                         |
              |                        :            :                        |
              |                        x            x                        |
              |                                                              |
              |                       _              _                       |
              |                       :              :                       |
              |                      :                :                      |
              |     ___              "                "              ___     |
              |   x"   "x                                          x"   "x   |
              | _"       x          "                  "          x       "_ |
              _x``````````"````````_````````````````````_````````"``````````x_
              |            "                                    "            |
              |             "_    x                      x    _"             |
    -0.215947 |...............x__"........................"__x...............|
              -10                                                           10

Or in X-Windows:

    ? ploth(x = -10, 10, sin(x)/x)
    %30 = [-10.00000000, 10.00000000, -0.2172335004, 0.9999833000]

![(plot window screenshot)](pariplot.png)

Or PostScript or SVG (note the tab-completion):

    ? write("sinc.svg", ploth
    ploth           plothraw        plothsizes      
    plothexport     plothrawexport  
    ? write("sinc.svg", plothexport("svg", x = -10, 10, sin(x)/x))

![(SVG plot)](sinc.svg)

### Symbolic math and a little linear algebra ###

It can do symbolic linear algebra:

    ? printtex([a, b; c, d] * [e, f; g, h])
    \pmatrix{
     e\*a
     + g\*b&f\*a
     + h\*b\cr
     e\*c
     + g\*d&f\*c
     + h\*d\cr
     }

(XXX I already said that above)

Including symbolic differentiation and integration:

    ? [x, x^2, x^3, x^4]'
    %62 = [1, 2*x, 3*x^2, 4*x^3]
    ? intformal(%62)
    %64 = [x, x^2, x^3, x^4]

But there’s no Numpy-like broadcasting in most contexts; these vectors
behave like vectors:

    ? % + 1
      ***   at top-level: %+1
      ***                  ^--
      *** _+_: forbidden addition t_INT + t_VEC (4 elts).
      ***   Break loop: type 'break' to go back to GP prompt
    break>

Which of course means that you can multiply them by scalars:

    ? [x + 1, x - 2, x^2 - x + 3]
    %67 = [x + 1, x - 2, x^2 - x + 3]
    ? 2 * %
    %68 = [2*x + 2, 2*x - 4, 2*x^2 - 2*x + 6]
    ? y * %
    %69 = [2*y*x + 2*y, 2*y*x - 4*y, 2*y*x^2 - 2*y*x + 6*y]

You can take derivatives (including of vectors) with respect to
different variables:

    ? deriv(%, x)
    %70 = [2*y, 2*y, 4*y*x - 2*y]
    ? deriv(%, y)
    %71 = [2, 2, 4*x - 2]

### More linear algebra ###

I thought it could compute matrix exponentials, but it turns out this
is just broadcasting:

    ? \p 3
       realprecision = 19 significant digits (3 digits displayed)
    ? \ps 2
    ? exp([cos(x), sin(x); -sin(x), cos(x)])
    %76 = 
    [2.72 - 1.36*x^2 + O(x^4) 1 + x + 1/2*x^2 + O(x^3)]

    [1 - x + 1/2*x^2 + O(x^3) 2.72 - 1.36*x^2 + O(x^4)]

(Apparently transcendental functions do broadcast.)

You can get less verbose matrix output by coercing to a vector:

    ? [1,2,3;4,5,6]
    %107 = 
    [1 2 3]

    [4 5 6]

    ? Vec(%)
    %108 = [[1, 4]~, [2, 5]~, [3, 6]~]

Or by invoking the `print` function:

    ? print([1,2,3;4,5,6])
    [1, 2, 3; 4, 5, 6]

### Systems of linear equations ###

You can solve systems of linear equations with division by a
transposed matrix or with `matsolve`:

    ? default(output, 3)
    ? \p 5
       realprecision = 19 significant digits (5 digits displayed)
    ? M = [3.1, 4.4, -1; 2, 7, -5; -2, -1, 5.5]
         / 3.1000 4.4000   -1   \
    %8 = |    2      7     -5   |
         \   -2     -1   5.5000 /

    ? vx = [1, 2, 3] / M; print(vx)
    [0.77187, -0.11664, 0.57976]
    ? vx * M
    %12 = (1.0000 2.0000 3.0000)
    ? [1,2,3]/M~
    %13 = (-0.66209 0.79588 0.44940)
    ? M * %~
          / 1.0000 \
    %14 = | 2.0000 |
          \ 3.0000 /
    ? print(matsolve(M, [1,2,3]~))
    [-0.66209, 0.79588, 0.44940]~

### Number theory stuff: primes, factorization, finite fields, finite groups ###

You can find prime numbers:

    ? primes([250, 270])
    %37 = [251, 257, 263, 269]
    ? primes([1000000000000000000000000000000000000000, 1000000000000000000000000000000000000100])
    %35 = [1000000000000000000000000000000000000003, 1000000000000000000000000000000000000037, 1000000000000000000000000000000000000081, 1000000000000000000000000000000000000087]

Or compute in finite fields:

    ? ffgen(256)^10
    %15 = x^7 + x^4 + x^2 + x + 1
    ? ffgen(256)^100
    %16 = x^7 + x^5 + x^4 + x^3 + x^2
    ? %15*%16
    %17 = x^6 + x^5 + 1
    ? ffgen(251) + 300
    %13 = 49

You can take the multiplicative inverse in a finite field by taking it
to the -1 power:

    ? %^-1
    %14 = 41
    ? 41*49
    %15 = 2009
    ? 41*49 % 251
    %16 = 1

You can compute with integers modulo a base:

    ? Mod(7, 10)^-1
    %51 = Mod(3, 10)
    ? Mod(8, 10) * 2
    %48 = Mod(6, 10)
    ? %^-1
      ***   at top-level: %^-1
      ***                  ^---
      *** _^_: impossible inverse in Fp_inv: Mod(2, 10).
      ***   Break loop: type 'break' to go back to GP prompt
    break>

You can move a polynomial into one of those types by multiplying by an
identity element in them, so you can, for example, factor a polynomial
over a finite field:

    ? p = x^2 + 2
    %46 = x^2 + 2
    ? Vec(factor(p))
    %49 = [[x^2 + 2]~, [1]~]
    ? Vec(factor(p * Mod(1, 3)))
    %50 = [[Mod(1, 3)*x + Mod(1, 3), Mod(1, 3)*x + Mod(2, 3)]~, [1, 1]~]
    ? Vec(factor(p * ffgen(3)^0))
    %51 = [[x + 1, x + 2]~, [1, 1]~]
    ? (x+1)*(x+2)
    %52 = x^2 + 3*x + 2
    ? Vec(factor(p * ffgen(ffinit(3, 2, 't))^0))
    %53 = [[x + 1, x + 2]~, [1, 1]~]
    ? Vec(factor((x^2 + 1) * ffgen(ffinit(3, 2, 't))^0))
    %54 = [[x + (t + 2), x + (2*t + 1)]~, [1, 1]~]

You can find the factorization of integers, too:

    ? factor(3084368036082360823086084321)
    %39 = 
    [           3 1]

    [          11 1]

    [         467 1]

    [224439675721 1]

    [891734852291 1]

    ? ##
      ***   last result: cpu time 0 ms, real time 13 ms.

Or, for that matter, the factorization of polynomials over fields like
ℚ or ℂ:

    ? factor(a^2 + 2*a*b + b^2)
    %38 = 
    [a + b 2]

    ? Vec(factor(x^2 + 2 + 0.*I))
    %57 = [[x + (0.E-19 + 1.4142*I), x + (0.E-19 - 1.4142*I)]~, [1, 1]~]

I’m not sure how to get it to factor to *exact* complex numbers.

### Programming/scripting ###

GP is a surprisingly comfortable, relatively conventional imperative
programming language!

Functions can be anonymous, but named functions are as easy to define
as possible:

    ? f(x) = 3*x^2 - 2*x + 1
    %1 = (x)->3*x^2-2*x+1
    ? f(8)
    %2 = 177
    ? f(y+2)
    %3 = 3*y^2 + 10*y + 9
    ? apply(f, [0..10])
    %5 = [1, 2, 9, 22, 41, 66, 97, 134, 177, 226, 281]

You have `for` loops and `print` statements.

    ? x = 0; for (i = 0, 10, x += i); x
    %21 = 55
    ? for(i = 1, 3, print("BOOGA"))
    BOOGA
    BOOGA
    BOOGA

It’s pretty slow, slower than Python:

    ? n=0; for(i = 1, 1000, for(j = 1, 1000, n = n+1))
    ? ##
      ***   last result: cpu time 231 ms, real time 238 ms.
    ? n
    %10 = 1000000

Adapted from the [Wikipedia
page](https://en.wikipedia.org/wiki/PARI/GP) we have this example:

    ? for(z=25,30, print (z, " ", 2^z-1, " ", factor(2^z-1)))
    25 33554431 [31, 1; 601, 1; 1801, 1]
    26 67108863 [3, 1; 2731, 1; 8191, 1]
    27 134217727 [7, 1; 73, 1; 262657, 1]
    28 268435455 [3, 1; 5, 1; 29, 1; 43, 1; 113, 1; 127, 1]
    29 536870911 [233, 1; 1103, 1; 2089, 1]
    30 1073741823 [3, 2; 7, 1; 11, 1; 31, 1; 151, 1; 331, 1]

Though there are some much older control structures:

    ? sum(i=1, 10, 1/i^2)
    %31 = 1968329/1270080
    ? vector(10, i, (i - 5)^2)
    %8 = [16, 9, 4, 1, 0, 1, 4, 9, 16, 25]

And some unorthodox ones:

    ? forperm([1, 2, 3], p, print(p))
    Vecsmall([1, 2, 3])
    Vecsmall([1, 3, 2])
    Vecsmall([2, 1, 3])
    Vecsmall([2, 3, 1])
    Vecsmall([3, 1, 2])
    Vecsmall([3, 2, 1])
    ? forpart(x = 4, print(x))
    Vecsmall([4])
    Vecsmall([1, 3])
    Vecsmall([2, 2])
    Vecsmall([1, 1, 2])
    Vecsmall([1, 1, 1, 1])

And set comprehensions, which yield vectors:

    ? [x*34 | x <- [2, 3, 4, 5, 18], x % 2 == 0]
    %5 = [68, 136, 612]

Ranges are inclusive:

    ? [1..10]
    %106 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    ? [sum(i = 1, j, 1/i^2) + 0. | j <- [1..20]]
    %111 = [1.0000, 1.2500, 1.3611, 1.4236, 1.4636, 1.4914, 1.5118, 1.5274, 1.5398, 1.5498, 1.5580, 1.5650, 1.5709, 1.5760, 1.5804, 1.5843, 1.5878, 1.5909, 1.5937, 1.5962]

Subroutines can have dynamically-scoped local variables:

    ? s(n) = { local(m); m = n + 2; m * m }
    %22 = (n)->local(m);m=n+2;m*m
    ? s(y + 2)
    %24 = y^2 + 8*y + 16

Though lexically-scoped ones are better, and the kind used by most
scoping constructs:

    ? s(n) = { my(m = n + 2); m * m }
    %10 = (n)->my(m=n+2);m*m
    ? s(y + 2)
    %11 = y^2 + 8*y + 16

The braces only permit continuing the body across lines; they are not
necessary:

    ? s(n) = my(m = n + 2); m * m
    %12 = (n)->my(m=n+2);m*m

They don’t even provide grouping:

    ? s(n) = {my(m = n + 2); m * m}; 3
    %14 = (n)->my(m=n+2);m*m;3
    ? s(y+2)
    %15 = 3

Function definitions support default parameters:

    ? sq(n=53) = n^2
    %18 = (n=53)->n^2
    ? [sq(), sq(52), sq(53), sq(x)]
    %23 = [2809, 2704, 2809, x^2]

There’s a somewhat limited FFI sufficient for loading dynamic
libraries written as extensions for PARI/GP and a few other cases;
“isL” means “returning int, taking a string and a long”.

    ? install("printf", "is", "ourprintf", "/lib/x86_64-linux-gnu/libc.so.6")
    ? ourprintf("Hello, PARI\n")
    Hello, PARI
    %25 = 12
    ? install("printf", "isL", "printf1l", "/lib/x86_64-linux-gnu/libc.so.6")
    ? printf1l("I have a %ld.\n", 53)
    I have a 53.
    %27 = 13

You can take random numbers:

    ? random()
    %52 = 1546275796
    ? random(10)
    %53 = 6

Variable and function names can contain spaces, which are ignored.

    ? print tex((a small dog + 1)^2)
    asmalldog^2
     + 2\*asmalldog
     + 1

So can numbers:

    ? binomial(100 000, 2)
    %15 = 4999950000

There are many different things that look the same way but belong to
different types, like this polynomial and this finite field element:

    ? ffinit(2, 8)
    %96 = Mod(1, 2)*x^8 + Mod(1, 2)*x^6 + Mod(1, 2)*x^5 + Mod(1, 2)*x^4 + Mod(1, 2)*x^3 + Mod(1, 2)*x + Mod(1, 2)
    ? ffgen(%, 'y)
    %97 = y
    ? % ^ 0b1101110
    %98 = y^6 + y^5 + 1
    ? %.pol
    %99 = y^6 + y^5 + 1
    ? type(%)
    %100 = "t_POL"
    ? type(%98)
    %101 = "t_FFELT"

The implementation
------------------

The main PARI/GP package in Debian is only a download of 3.8 megabytes
built for amd64, though it uncompresses to be larger, more like 10
megabytes.  It recommends installing 160 megabytes of databases of
things like elliptic curves over the rationals and modular
polynomials.

I’m curious how hard it would be to run it on a new platform.

PARI/GP is mostly about a quarter million lines of C:

    $ cloc pari-2.15.2/
        2312 text files.
         353 unique files.                              
        1959 files ignored.

    github.com/AlDanial/cloc v 1.96  T=2.38 s (148.6 files/s, 166760.0 lines/s)
    -------------------------------------------------------------------------------
    Language                     files          blank        comment           code
    -------------------------------------------------------------------------------
    C                              230          19519          20285         259217
    TeX                             28          13948           3758          57042
    C/C++ Header                    65           1423           1472          17043
    Bourne Shell                    21            122            126           1027
    Perl                             6             59             38            571
    yacc                             1             27             10            214
    make                             1             17             14            165
    Fortran 90                       1              0             15             88
    -------------------------------------------------------------------------------
    SUM:                           353          35115          25718         335367
    -------------------------------------------------------------------------------

The first version was in 68020 assembly, and this version actually
includes assembly inner loops for i386, amd64, SPARC, Alpha, Itanic,
PowerPC, and HP-PA, and also (though the manual doesn't mention this)
ARM, 68000, MIPS and ARM64 but not RISC-V:

    $ ls -l pari-2.15.2/src/kernel/
    total 72
    drwxr-xr-x 2 4096 Dec 24  2022 aarch64
    drwxr-xr-x 2 4096 Dec 24  2022 alpha
    drwxr-xr-x 2 4096 Dec 24  2022 arm
    drwxr-xr-x 2 4096 Dec 24  2022 gmp
    drwxr-xr-x 2 4096 Dec 24  2022 hppa
    drwxr-xr-x 2 4096 Dec 24  2022 hppa64
    drwxr-xr-x 2 4096 Dec 24  2022 ia64
    drwxr-xr-x 2 4096 Dec 24  2022 ix86
    drwxr-xr-x 2 4096 Dec 24  2022 m68k
    drwxr-xr-x 2 4096 Dec 24  2022 mips
    drwxr-xr-x 2 4096 Dec 24  2022 mips64
    drwxr-xr-x 2 4096 Dec 24  2022 none
    drwxr-xr-x 2 4096 Dec 24  2022 ppc
    drwxr-xr-x 2 4096 Dec 24  2022 ppc64
    -rw-r--r-- 1 1470 Sep 22  2011 README
    drwxr-xr-x 2 4096 Dec 24  2022 sparcv8_micro
    drwxr-xr-x 2 4096 Dec 24  2022 sparcv8_super
    drwxr-xr-x 2 4096 Dec 24  2022 x86_64

These are pretty spare, though.  The m68k version only defines C
preprocessor macros for inline assembly for addll, addllx, subll,
subllx, mulll, addmul, bfffo (bit field find first one?), and divll,
most of which seem to be mere 64-bit arithmetic, while Bruno Haible’s
ix86 also defines addllx8 and subllx8.

Its build toolchain uses Perl and Bison.

Aside from the GP scripting language, it’s designed to be called as a
library from C, which is very simple indeed and extensively documented
in `/usr/share/pari/doc/libpari.pdf`:

    #include <stdio.h>
    #include <stdlib.h>
    #include <pari/pari.h>

    int main(int argc, char **argv)
    {
      long n = argv[1] ? atol(argv[1]) : 33;
      pari_init(1<<19, 0);          /* Without this it segfaults */
      printf("next prime after %ld is %ld\n", n, itos(nextprime(stoi(n))));
      return 0;
    }

It seems like it should be pretty easy to write bindings to call
arbitrary C libraries from GP, though the `install` prototype string
is not expressive enough for many; you have to write glue code in C.
It also can be compiled with MPI for easy parallel processing on a
cluster, though this copy is just using pthreads to run on many cores:

                  GP/PARI CALCULATOR Version 2.15.2 (released)
          amd64 running linux (x86-64/GMP-6.2.1 kernel) 64-bit version
          compiled: Dec 31 2022, gcc version 12.2.0 (Debian 12.2.0-11)
                           threading engine: pthread
    ...
    ? default(nbthreads)
    %41 = 16

It starts up very quickly, so piping input to it is a reasonable thing
to do:

    $ time gp -q <<<3+4
    7

    real    0m0.012s
    user    0m0.006s
    sys     0m0.007s

### A note on memory usage ###

The first argument to the `pari_init` call specifies how much memory
libpari can use.  If it’s only `1<<15` it dies with an error, but if
it’s `1<<16` or more it works.  The manual says it actually uses more
space than this for things like permanent objects, but I don't know
how much that is.  With `ulimit -d 1024`, it dies at startup, saying,

    ***   not enough memory  ***   Error in the PARI system. End of program.

With `ulimit -d 2048` it runs successfully.  I suspect that much of
this requirement is due to glibc, but I haven’t been able to
successfully compile it with klibc, which doesn’t seem to support
floating point, or dietlibc, which might be more plausible.  I haven’t
tried picolibc, musl, or newlib.

libpari.so is almost 13 megs:

    $ size /usr/lib/x86_64-linux-gnu/libpari.so
       text    data     bss     dec     hex filename
    12787518 204144    5112 12996774 c650a6 /usr/lib/x86_64-linux-gnu/libpari.so

That’s not in the pari-gp Debian package but in libpari-gmp-tls8.
/usr/bin/gp is 10.3 megabytes and dynamically linked with libgmp, so I
guess it’s statically linked with PARI, and the 3.8 megs I mentioned
earlier is just the compressed size.

The world around it
-------------------

Many OEIS sequences have PARI/GP scripts in their entries. [A000248,
for example][1], has two alternate implementations:

>     a(n)=sum(k=0, n, binomial(n, k)*(n-k)^k);
>     x='x+O('x^66); Vec(serlaplace(exp(x*exp(x))))

Both of these do seem to work instantly.

[1]: https://oeis.org/A000248

There doesn’t seem to be a CTAN-like, CRAN-like, or npm-like
repository of public PARI/GP scripts.  [GitHub has 12 repos][2]
mentioning it, of which a third are collections of implementations of
OEIS sequences.  I suspect that this is more because it’s for number
theorists, and there aren’t that many number theorists in the world,
than because it’s unpopular among its target audience.

[2]: https://github.com/topics/pari-gp

There’s a [message board on the Mersenne Forum][3].  It has a few
posts per year, mostly from Paul Underwood.  There are [263 questions
on MathOverflow that mention it][4] and [a tag on Stack Overflow with
114 questions][5].

[3]: https://mersenneforum.org/forumdisplay.php?f=155&daysprune=-1&order=desc&sort=lastpost&pp=25&page=1
[5]: https://stackoverflow.com/questions/tagged/pari-gp
[4]: https://mathoverflow.net/search?q=pari%2Fgp

SageMath and TeXmacs apparently know how to drive Pari/GP.  There’s an
Elisp package too apparently but I don’t have it.

The GAP system (“for computational discrete algebra”, `gap` in Debian)
has a package called Alnuth (“algebraic number theory”, `gap-alnuth`
in Debian) which includes an interface to PARI/GP.

There’s [an abandoned Python library][6], and [some quick-and-dirty
Python bindings][7].

[6]: https://code.google.com/archive/p/pari-python/
[7]: https://stackoverflow.com/questions/14943507/calling-pari-gp-from-python
