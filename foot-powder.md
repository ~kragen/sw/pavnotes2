My sneakers recently got wet in the rain and started to stink.
Washing them is, of course, the cure, but in the meantime, I dusted
them generously with borax to kill whatever was growing in them,
because borax was what I had around.

This led me to thinking about foot powders.  Sweating through my feet
is pretty unproductive when they’re inside shoes, so applying
antiperspirant there seems like it should be harmless.  [Potassium
alum][0] (fitkari) or [ammonium alum][7] is commonly sold to hippies
as a natural antiperspirant, and mass-produced antiperspirants are
pretty universally [soluble aluminum salts][1], typically
[sulfates][2], though aluminum also has a soluble [chloride][5],
[sulfacetate][4], and [diacetate][3] (the triacetate converts to the
diacetate in water).  Sometimes instead [aluminum zirconium
tetrachlohydrex gly][6] is used, a salt with hydroxide, chloride,
glycine, and zirconium.

[0]: https://en.wikipedia.org/wiki/Aluminium_sulfate
[1]: https://en.wikipedia.org/wiki/Deodorant#Product_formulations_and_formats
[2]: https://en.wikipedia.org/wiki/Aluminium_sulfate
[3]: https://en.wikipedia.org/wiki/Aluminium_diacetate
[4]: https://en.wikipedia.org/wiki/Aluminium_sulfacetate
[5]: https://en.wikipedia.org/wiki/Aluminium_chloride
[6]: https://en.wikipedia.org/wiki/Aluminium_zirconium_tetrachlorohydrex_gly
[7]: https://en.wikipedia.org/wiki/Ammonium_alum

All these soluble aluminum salts except for the diacetate are somewhat
acidic, and aluminum forms [extremely insoluble salts with borate][9]
sometimes called Alborite or Alborex, which are [used to reinforce
aluminum metal][10] and can be made by [reacting aluminum nitrate and
boric acid in glycerine][11], so it’s probably desirable not to
include borates or anything basic in the formulation.  But the
aluminum salts only have mild antibacterial action, so it might be
good to supplement them with something stronger.

[9]: https://en.wikipedia.org/wiki/Jeremejevite
[11]: https://media.suub.uni-bremen.de/bitstream/elib/4226/1/120_Post.pdf "Crystal chemical characterization of mullite-type aluminum borate compounds, Hoffmann et al."
[10]: http://sedici.unlp.edu.ar/bitstream/handle/10915/104762/Documento_completo.pdf-PDFA.pdf?sequence=1 "Formation, microstructure and properties of aluminum borate ceramics obtained from alumina and boric acid, by Hernández et al., DOI 10.1016/j.ceramint.2016.11.002"

Some [socks incorporate metallic silver][8] to reduce foot odor with
the [oligodynamic effect][12], which kills bacteria even at
0.01–0.1mg/ℓ.  [Zinc oxide][13] is commonly used in things like
sunscreen, baby powder, and “calamine” lotion (though calamine is
actually zinc carbonate or silicate) in part due to a similar effect.
But perhaps the most common oligodynamic metal is [copper][14], which
kills some bacteria at 0.02mg/ℓ and just about all bacteria at 10mg/ℓ,
being used at 0.2–0.8mg/ℓ together with copper to kill [Legionella
biofilms][17] in water supplies.  [Bordeaux mixture][15],
traditionally used in vineyards against fungal spores and thieves, is
1% (10000mg/ℓ) [blue vitriol][16], which is in turn 25.4% copper
(63.546g/mol out of 249.685g/mol) giving 2500mg/ℓ copper.  For
mammals, the lethal dose of blue vitriol is on the order of 100mg/kg,
or 25mg/kg of copper, and the EU limit for copper in drinking water is
2mg/ℓ.

[8]: https://en.wikipedia.org/wiki/Smelly_socks#Solutions
[17]: https://en.wikipedia.org/wiki/Copper-silver_ionization
[12]: https://en.wikipedia.org/wiki/Oligodynamic_effect
[13]: https://en.wikipedia.org/wiki/Zinc_oxide
[14]: https://en.wikipedia.org/wiki/Antimicrobial_properties_of_copper
[15]: https://en.wikipedia.org/wiki/Bordeaux_mixture
[16]: https://en.wikipedia.org/wiki/Copper(II)_sulfate

Presumably whatever copper compound is in contact with foot sweat will
produce [cupric chloride][18], whose solubility is 75.7g/100mℓ at 25°,
almost as high as the sulfate.  So, if you’re going to use copper, it
might be best to use an insoluble copper compound like [green
verditer][19] or [tenorite][20] or just metallic copper.

[18]: https://en.wikipedia.org/wiki/Copper(II)_chloride
[19]: https://en.wikipedia.org/wiki/Basic_copper_carbonate
[20]: https://en.wikipedia.org/wiki/Copper(II)_oxide

I weigh about 110 kg, so the lethal dose of 25 mg/kg of copper is on
the order of 3 grams of copper for me.  Perhaps more relevant for the
contemplated chronic exposure is that [the recommended dietary intake
is 0.9–10mg/day][21], so even tens of milligrams per day on my skin
are probably fine, since presumably you absorb less through your skin
than you would through your intestines.  And I think aluminum toxicity
from skin exposure is low enough to not worry about.

[21]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4339675/#S2title "Copper: Toxicological relevance and mechanisms, by Gaetke, Chow-Johnson, and Chow, Arch Toxicol. 2014 Nov; 88(11): 1929–1938, DOI 10.1007/s00204-014-1355-y, PMCID: PMC4339675, NIHMSID: NIHMS659791, PMID: 25199685"

Suppose you apply an unreasonably large amount of foot powder, like 10
grams, and have an even more unreasonably large 100 grams of sweat
(or, more likely, rainwater) in your shoes.  10mg/ℓ of copper, enough
to kill pretty much all bacteria (and five times the limit for
drinking water), would then be 1mg, which is maybe a factor of 30
below the safety limit.  You could increase the copper to 10mg for
those 10g of foot powder (0.1% copper) to ensure that your shoes are
thoroughly inhospitable to bacteria even if you only use a gram of the
powder.

There are other ingredients commonly used as foot powders.  [Talcum
powder][22] is recommended for treatment of trench foot, and [Gold
Bond Sterilizing Powder][23] is canonically talc, zinc oxide, gum,
methyl salicylate, salicylic acid, thymol, zinc stearate, and
[0.15%–0.8% menthol][26], which last [has some antibacterial
properties but mostly relieves itching][24], as do methyl salicylate
and thymol, the other essential oils listed as “inactive ingredients”.
The odor of menthol (and/or the other essential oils) may also mask
the odor of smelly socks, but sort of smells like a hospital, which
may not be an improvement.  [Camphor][25] is another high-melting
odor-masking antipruritic with reasonably low toxicity.

[22]: https://en.wikipedia.org/wiki/Trench_foot#Treatment
[23]: https://en.wikipedia.org/wiki/Gold_Bond
[24]: https://en.wikipedia.org/wiki/Menthol#Biological_properties
[25]: https://en.wikipedia.org/wiki/Camphor#Topical_medication
[26]: https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=dd74205d-4520-417e-b43f-6622c35c1971

[A different variant of Gold Bond][27] is “talc-free” with cornstarch,
baking soda, gum, silica, tricalcium phosphate, eucalyptus oil,
peppermint oil (which contains menthol), and [benzethonium
chloride][28], a water-soluble quaternary-ammonium cationic surfactant
that melts at 163° and is effective as a bactericide at 0.1–0.2%
(1000–2000mg/ℓ).

[27]: https://dailymed.nlm.nih.gov/dailymed/fda/fdaDrugXsl.cfm?setid=bb9cbd03-3254-4411-9b4f-e81908a41bd1&type=display
[28]: https://en.wikipedia.org/wiki/Benzethonium_chloride
