I’d like a more convenient way to interact with the things I’m writing
in Lua in Emacs.

Making the REPL print tables recursively
----------------------------------------

Lua has a passable REPL, though it becomes a lot more passable with a
function something like this:

    -- Recursively print out an object, using `prefix` for lines after the
    -- first.  For now handles only acyclic structures.
    function utra.dump(obj, output, prefix)
       if prefix == nil then prefix = '' end
       if output == nil then output = io.write end

       local t = type(obj)
       if t == 'table' then
          output('{')

          local nprefix = prefix..'  '
          for k, v in pairs(obj) do
             output('\n', nprefix, '[')
             utra.dump(k, output, nprefix..' ')
             output('] = ')
             utra.dump(v, output, nprefix)
             output(',')
          end

          output('\n', prefix, '}')

       elseif t == 'string' then
          output(string.format('%q', obj))
       else                         -- e.g., functions and numbers
          output(string.format('%s', obj))
       end
    end

Assigning this to `print` in the REPL does what you would hope, though
it doesn’t handle multiple values or append a newline:

    > ={}
    table: 0x405c00e0
    > print = utra.dump
    > ={}
    {
    }> 

Setting `compile-command` with a file-local variable
----------------------------------------------------

But basically I want a way to get faster feedback on the code in my
Lua buffer.

The simplest thing I can do is to set compile-command to something
like this:

    $ luajit -e 'print(loadfile("microtrans.lua")())'
    {
      ["val"] = {
        ["items"] = {
          [1] = "Whoever",
          [2] = "reads",
          [3] = "th1s",
          [4] = "is",
        },
      },
      ["offset"] = 24,
    }

This allows me to see results with a single keystroke.  Unfortunately
this doesn't give me compilation errors usefully:

    luajit: (command line):1: attempt to call a nil value
    stack traceback:
            (command line):1: in main chunk
            [C]: at 0x564fb88001d0

An improvement might be:

    luajit -e 'f, err = loadfile("microtrans.lua")
               if err then print(err) else print(f()) end'

This does indeed give me a useful compilation command with the ability
to jump to syntax errors with C-x \`.  And a local binding for
compile-command is [an example in the Emacs manual][0].

[0]: https://www.gnu.org/software/emacs/manual/html_node/emacs/Specifying-File-Variables.html

So I stuck this at the end of microtrans.lua, and now F5 in the file
(recompile) runs this compile-command:

    -- Local Variables:
    -- compile-command: "luajit -e 'f, err = loadfile(\"microtrans.lua\") if err then print(err) else print(f()) end'"
    -- End:

That’s already a big improvement.

Emacs parses Lua tracebacks incorrectly
---------------------------------------

In this traceback, Emacs is okay with the first error line, but screws
up on the later ones:

    luajit: microtrans.lua:27: attempt to index upvalue 'a' (a nil value)
    stack traceback:
            microtrans.lua:27: in function 'parse'
            microtrans.lua:41: in function 'parse'
            microtrans.lua:102: in function 'parse'
            microtrans.lua:215: in function 'f'
            (command line):1: in main chunk
            [C]: at 0x55b3a69e61d0

It thinks the indentation is part of the filename.
