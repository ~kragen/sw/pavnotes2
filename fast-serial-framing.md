I was thinking about the problem of framing
packets on high-speed serial links, like
[SLIP][1] and PPP framing and [COBS][0].
The scenario I was thinking about is where
you’re passing something like raw video
data over a megabits-speed serial link, but
the bit error rate on the link is nonzero,
and you’d like to be able to resynchronize
quickly after any bit or clock errors,
though resynchronizing immediately might be
too much to ask.  SPI links commonly go up
past ten megabits per second these days and
are commonly implemented on
microcontrollers with tens to thousands of
bytes of RAM but tens of MIPS.  UART
transmission can reliably do over a
megabit.

[0]: https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing (James Carlson, Stuart Cheshire, and Mary Baker)
[1]: https://www.rfc-editor.org/rfc/rfc1055.txt (RFC 1055, Romkey, 01988)

Although the SLIP approach typically
expands the data only a little bit, in the
worst case it can double the size of the
data; a data stream consisting entirely of
decimal 192 bytes (SLIP END) or entirely of
decimal 219 bytes (SLIP ESC) will be
doubled in size by the insertion of an ESC
before each byte.

COBS avoids this, with a worst case of one
extra byte inserted every 254, and often
can avoid inserting any extra bytes at all;
but you still must scan every byte for zero
bytes in order to frame it.  Worse, if you
do start inserting bytes, you need to copy
all the later bytes in the message to a new
location — which in general probably means
that you need to copy the whole message all
the time just in case you have to insert.
COBS also requires looking ahead in the
data stream (up to 254 bytes) in order to
frame it.  And I don’t think there’s any
guarantee of how long COBS will take to
resynchronize after a bit error.  The
COBS-encoding of the byte sequence ..., 1,
4, 2, 0, 1, 4, 2, 0, 1, 4, 2, 0, ... is
..., 4, 1, 4, 2, 4, 1, 4, 2, 4, 1, 4, 2,
... which a framing error (a 4 being turned
into a 2, for example) could evidently
decode as ..., 4, 1, 0, 2, 4, 1, 0, 2, 4,
1, 0, 2, ....  So the rest of the COBS
stream would decode incorrectly.

COBS is not itself a framing protocol, but
its motivation is to eliminate a particular
byte value from the data so that it can be
used for framing.  If it’s being used in
that way, the decoding error will
presumably terminate when the frame reaches
its terminating (physically present rather
than COBS-encoded) NUL, and indeed then the
error will be recognized.

Fixed-length framing protocols, in which
all frames are of the same size, in theory
avoid these problems: bit errors will never
lead to framing errors, there’s no
pathological case where the data size grows
significantly, you don’t have to copy the
data around in order to insert bytes into
the middle of it, you don’t have to scan
the data byte by byte when sending it and
receiving it, and you don’t have to look
ahead in the data stream to encode it.  But
they can’t deal with clocking errors at
all; once framing is lost, it can stay
lost.

A 6-byte trailer
----------------

So here’s a plausible compromise solution
to these problems, inspired by COBS.  We
append a six-byte trailer to each frame,
consisting of a 2-byte pointer field and a
magic number, say 'L7\xa5Z', which we
guarantee does not occur elsewhere in the
encoded frame.  The pointer points to the
*last* location where the magic number
occurred inside the unencoded packet data.
Usually this will be 0, which means that it
did not occur, but otherwise it is the
number of bytes to skip backward from the
beginning of the trailer to find the last
byte where the magic number should be,
where the encoding process has overwritten
it with another pointer encoded in the same
way.  The decoding process follows this
linked list of damaged fields backwards
through the packet data, overwriting each
one with a copy of the magic number to
restore the original data.

This scheme adds a fixed 6 bytes of framing
overhead per frame and can accommodate
frames of up to 65536 bytes.  If you get
lucky, it can accommodate bigger frames,
but it can reliably accommodate 64-KiB
jumbograms.  If the data being transmitted
is random, it only requires censoring a
magic number once every 4 gibibytes of
payload.  Any data transmission errors,
whether clocking or data, only corrupt the
frame in which they occur and occasionally
the previous frame to which it was
appended.  Unless the magic number is
chosen to contain repeated bytes,
Boyer–Moore string search can reliably
recover framing by examining one of every
3.94 bytes (assuming random data), and the
same computation is needed during encoding
to find the magic numbers to censor, a
process which requires only a single byte
of lookahead.  The data can be encoded and
decoded “in place”, without a
memory-to-memory copy.  Because there’s no
header, it doesn’t even lose alignment; if
it’s read into, say, an 8-byte-aligned
buffer, the payload remains 8-byte aligned
after decoding.

However, the receiver must be able to
buffer the entire frame in order to decode
it successfully, so, for communication
between small machines, it may be necessary
to limit the frame size to hundreds of
bytes.  So eliminating the need for
lookahead for transmission doesn’t really
solve the problem of communication between
machines with sub-kilobyte RAM.

For some applications, such as transmitting
audio or compressed video data, it may be
acceptable to just use the still-encoded
data as if it were the decoded data, which
eliminates the requirement for any
lookahead on the receiver.

Just a 4-byte terminator, no pointer
------------------------------------

Small machines might be better off using
something more like the SLIP scheme,
perhaps with a multiple-byte terminator
sequence.  If ‘ABCD’ is the terminator
sequence, we can transform all occurrences
of ‘ABC’ in the payload data into ‘ABC;’ to
encode, and then to decode transform all
occurrences of ‘ABCx’ back into ‘ABC’, for
all values of ‘x’ other than ‘D’.  ‘ABC;’
itself contains ‘ABC’ and therefore
transforms into ‘ABC;;’, etc.  This has a
worst-case expansion of 33% rather than
SLIP’s 100%, and it still permits
Boyer–Moore string search for the
terminator and for any strings requiring
escaping (though the latter must now
examine over a third of all bytes).

Here the escape byte ‘;’ was chosen to have
a relatively high Hamming distance of 7
from ‘D’ to minimize the chances of bit
errors turning into framing errors, but you
could do better if you aren’t trying to use
printable ASCII.  And you could choose to
decode other sequences such as ‘ABCd’,
‘ABCE’, ‘ABCF’, ‘ABC\`’, ‘ABCL’, ‘ABCT’,
etc., which are more similar to the
terminator, as terminators rather than
escapes.

This approach requires only three bytes of
lookahead on the transmitter and four on
the receiver.

You could imagine this scheme being used to
divide printable ASCII data files into
fields with a separator like ‘;@O+’.
