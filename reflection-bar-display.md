If you have a reflective round metal bar spinning around an axis
perpendicular to the bar, so that it sweeps out a disc, an infinitely
distant viewer looking at the disc along the axis of rotation will see
the reflection of an LED fixed along the outside of the disc as a
circle passing through the LED and through the center of rotation
whose diameter is the distance from the center of rotation to the LED.
This is because the spot of light they see on the bar at any given
moment is at the position along the bar where the line from the point
to the LED is perpendicular to the bar, so the segment of the bar from
the center of rotation to the spot of light forms a right angle with
the line from that point to the LED; one leg of this right angle
always intersects the LED, and is other leg always intersects the
center of rotation, so its locus is precisely that circle whose
diameter is the line segment between those two points.

If you blink the LED on and off while the bar is spinning, you can
choose which parts of the circle are lit up.  If you have several
LEDs, you have several circles, which can overlap, allowing you to
selectively light up a whole area.  That is, it provides a luminous,
transparent display.

This may be an improvement over the now-standard bicycle-wheel POV
approach of spinning one or more lines of LEDs around a center for a
variety of possible reasons:

1. There is no need to run power to the spinning element or to
   heatsink it.
2. Although the resolution is still uneven, it’s less uneven, because
   this system has high resolution at the center and at the edges, not
   just at the center.
