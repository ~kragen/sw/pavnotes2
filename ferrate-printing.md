Stereolithography conventionally uses ultraviolet to liberate free
radicals from photosensitive organic compounds to initiate
polymerization in a resin mix; free-radical scavenger antioxidants
prevent the reaction from running away, and ultraviolet blockers
prevent the light from penetrating below the surface of the resin
tank.  All of this is sort of expensive, in large part because organic
compounds are expensive, especially when they’re pure.

(Why are they expensive?  In part because they’re only metastable in
air at STP; the reaction equilibrium favors turning them into water,
carbon dioxide, and nitrogen.  So organic compounds represent embodied
energy.  But the purity is a bigger issue.)

[Two-photon polymerization][0] is an alternative to the sunscreen
which dramatically improves the spatial resolution of the process,
well into the submicron range.  By using a color of light that is too
red to initiate the reaction, you get a nonlinear (quadratic) increase
in the reaction rate with light intensity, so the reaction outside of
the desired focal spot is negligible despite the transparency of the
medium.

[0]: https://en.wikipedia.org/wiki/Multiphoton_lithography

Various reactions, including the precipitation of insoluble
phosphates, silicates, and carbonates, can be induced with the release
of polyvalent metallic cations.  The cyanotype photographic process
from 01843, [still in widespread commercial use until the 01940s][3],
uses the precipitation of ferric ferrocyanide by the reduction of
ferric ions (originally from ferric ammonium citrate, improved by Mike
ware to ferric ammonium oxalate) to trivalent ferrous ions, which
react with soluble and colorless potassium ferricyanide to produce the
ferric ferrocyanide.  But either the ferrous or ferric ion, if found
in solution, is sufficient to precipitate insoluble phosphates or
silicates.

[3]: https://en.wikipedia.org/wiki/Blueprint#The_blueprint_process

[Bichromate][1] [gum printing][2] is a process from about the same
time which instead uses the photosensitivity of [ammonium
dichromate][4] or similar dichromate salts to liberate, as I
understand it, trivalent chromium ions, hardening gums such as gelatin
and gum arabic by [cross-linking their proteins][5].  (Although
dichromate is a strong oxidizer, it evidently isn’t strong enough to
be destroyed by merely being in contact with gelatin or gum arabic.)

[1]: https://en.wikipedia.org/wiki/Gum_bichromate
[2]: https://en.wikipedia.org/wiki/Gum_printing
[4]: https://en.wikipedia.org/wiki/Ammonium_dichromate
[5]: https://en.wikipedia.org/wiki/Tanning_(leather)#Chrome_tanning

I was interested to learn today that the rare [ferrate(VI) ion][6]
FeO₄⁻⁻ is also photosensitive.  It’s made (with strong oxidizing
agents like hypochlorite) from iron, which is a [thousand times more
common than chromium][7], unlike dichromate, it is not known to be
carcinogenic.  It’s also a stronger oxidizing agent than permanganate,
capable of oxidizing ammonia in aqueous solution, so it’s probably
incompatible with things like gelatin and gum arabic.

[6]: https://en.wikipedia.org/wiki/Ferrate(VI)
[7]: https://en.wikipedia.org/wiki/Abundance_of_elements_in_Earth%27s_crust#/media/File:Elemental_abundances.svg

But it shouldn’t cause any problem with alkali phosphates or
silicates!  It should coexist with them just fine.

Ferrate papers
--------------

[Antolini et al. explain that ultraviolet oxidizes the hexavalent
iron][8] to unstable pentavalent or tetravalent states, which sounds
promising.

[Talaiekhozani et al. describe ferrate(VI) as “the most powerful
oxidant ever known”][9].  The iron ends up as trivalent ions after
oxidizing whatever it can get its grabby little oxygens on.  They also
explain that there is a “simple” electrochemical production process:

> Two rectangular iron [sic] with dimension [sic] of 60×24 mm and
> thickness of 0.63 mm were employed as anode and cathode electrodes,
> respectively [sic]. The DC voltage employed was within the range of
> 1–24 V. An electrolysis system was assembled to produce ferrate(VI)
> as demonstrated in Figure 1. In this study, 56 g of sodium hydroxide
> was dissolved in 100 mL of distilled water to prepare a 14M
> solution. Thereafter, the electrolysis container as illustrated
> [sic] Figure 1 was filled with 100 mL of 14M sodium
> hydroxide. Subsequently, electrodes were charged [sic] using DC
> current with voltage of 9 V and amperage of 1 A for 30
> minutes. Given that ferrate(VI) is converted to ferric(III) as time
> goes by, the prepared ferrate(VI) solution must be used as soon as
> possible.

I think I’m in love.  With ferrate, not with this paper, which is so
terrible I suspect it may be a satirical parody of academic
publishing.

They didn’t specify the electrode spacing, describe the electrode
purity, or explain whether this was controlled for constant voltage or
constant current (the chance that it was both 9 V for 30 minutes and 1
A for 30 minutes is zero; those were presumably the limits they set on
their lab power supply, and only one of them was reached at a time,
probably the current limit).  They do have a photo, and it looks like
the electrodes were about 20 mm apart.  It also looks like they were
about three-quarters out of the water, so the current density was
actually several times higher than the paragraph above implies.

[8]: https://pubs.acs.org/doi/full/10.1021/jacs.2c08048
[9]: http://ehemj.com/files/site1/user_files_cb3efc/amirtkh-A-10-93-6-be2dc4e.pdf "Hydrogen sulfide and organic compounds removal in municipal wastewater using ferrate (VI) and ultraviolet radiation, by Talaiekhozani, Eskandari, Bagheri, Talaei, and Salari, 02016, doi 10.15171/EHEM.2017.02"

As for the voltage, they have a figure 4(C) showing production of
ferrate(VI) as a function of voltage with five data points and a
regression line sloping down to no ferrate at 1.5 volts and linearly
increasing at 0.0386 mg/liter per volt after that, but they don’t say
at what temperature or whether in these runs they added FeCl₂ or not.
(The text says they added FeCl₂ in some runs as a source of “Fe³⁺”,
which it isn’t, and later they describe MgCl₂ and BaCl₂ as “ionic
compounds other than FeCl₃”, implying it was FeCl₃ they were adding.)

They never bother to say how they measured their ferrate(VI)
concentration, so I’m not sure how they know what they produced was
ferrate at all, rather than, say, FeCl₃ (on the runs where they used
chloride salts instead of just lye) or some kind of sodium vanadate
salt from vanadium impurities in their anode.  They say, “The amount
of ferrate(VI) was measured by the methods introduced by Talaiekhozani
et al. (15).”  Their graphs in figures 7(A) and 7(B) seem to show that
it had no effect on the degradation of the wastes, but the text says
the opposite; possibly figures 7(A) and 7(B) are mislabeled.

[Talaiekhozani et al. (15)][10] is another terrible paper, which says:

> A purple color appeared in the NaOH solution at early stage showing
> that ferrate(VI) was being produced. Electrolysis continued for min
> [sic] when a steady concentration of ferrate(VI) was produced based
> on periodic measurement of ferrate(VI) concentration during the
> electrolysis. (...)  In order to determine ferrate(VI)
> concentration, ferrate (VI) was converted to Fe(III) in an alkaline
> environment using arsenite as the primary reagent (Yongsiri et al.,
> 2004). In this method, a specific amount of arsenite which is higher
> than the stoichiometric amount required for complete conversion of
> ferrate(VI) to Fe(III) is added to the solution (Pomeroy and Bowlus,
> 1946). The extra arsenite can then be measured by the bromate and
> cerate standard solutions. The ferrate(VI) ion concentration in the
> solution was determined by mass balance knowing the reacted
> arsenite.

[10]: https://doi.org/10.1016/j.jenvman.2016.09.084 "Talaiekhozani, A., Salari, M., Talaei, M. R., Bagheri, M., & Eskandari, Z. (2016). Formaldehyde removal from wastewater and air by using UV, ferrate(VI) and UV/ferrate(VI). Journal of Environmental Management, 184, 204–209. doi:10.1016/j.jenvman.2016.09.084"

This is somewhat of an improvement, but they don’t say which arsenite
they used, what the non-primary reagents were, how they excluded other
oxidants that could oxidize the arsenite (which is used as a reducing
agent in organic chemistry, so it’s not very selective for ferrate),
and whether they used bromate or cerate, which are things you would
need to know to replicate their results.

Pomeroy and Bowlus turns out to be [“Pomeroy, R., Bowlus,
F.D., 1946. Progress report on sulfide control
research. Sew. Work. J. 18, 597–640][11],” from Sewage Works Journal,
which is on JSTOR.  It’s a 45-page-long paper discussing various ways
of oxidizing sulfide in sewer effluents, or preventing its formation
in the first place, including sulfuric acid, aeration, nitrate,
chlorination, manganese dioxide, chromate, ozone, phenol, cloroben,
trichlorophenol, ferrous sulfate, zinc chloride, copper sulfate, etc.
Contrary to Talaiekhozani *et al.*’s implication above, it never
mentions arsenite or ferrate, and unless I missed it, it doesn’t even
talk about measuring the quantity of one material by reacting it with
an excess of another and then weighing the residual reagent.

[11]: https://www.jstor.org/stable/25030283

[Yongsiri *et al.*, 2004][12] also doesn’t mention ferrate or
arsenite.  In fact, it’s a mathematical modeling paper that
*simulates* hydrogen sulfide emissions from sewers, so it doesn’t
mention any kind of measurement techniques at all.  It appears that
Talaiekhozani *et al.* were just citing random papers related to
wastewater hydrogen sulfide instead of, as they claimed, papers that
explained their technique for measuring ferrate(VI).

[12]: https://iwaponline.com/wst/article-abstract/50/4/161/11125/Hydrogen-sulfide-emission-in-sewer-networks-a-two?redirectedFrom=fulltext

[Qu *et al.* 02002][13] is a much, much better paper.  It mentions:

* That it's a powerful oxidant at all pH.
* “The standard half-cell oxidation potential has been estimated at
  2.20 and 0.72 V in acid and base, respectively (Audette et
  al. 1971).”
* Schreyer and Ockerman 1951 discusses the reasonably soluble
  potassium ferrate and how higher pH enhances its stability, though
  it “decomposes rapidly in acid solutions.”
* In acid solution, when it oxidizes things, the products are Fe³⁺ +
  4H₂O.
* In “weak acid, neutral, and alkalescent” solution, 

• In weak acid, neutral, and alkalescent solution, FeO 4
22
14H1
13e2→Fe~OH!31OH2; and

[13]: https://www.researchgate.net/publication/228479766_Reduction_of_Fulvic_Acid_in_Drinking_Water_by_Ferrate
