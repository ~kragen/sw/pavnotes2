Today I coaxed the following three exegeses of “Orinoco Flow”, a
popular song from 01988, from GPT-4.

The Orinoco Flow: A Divine Revelation of the Eschaton
-----------------------------------------------------

Enya’s “Orinoco Flow” is not a mere harmonious melody; it is a thunderous 
trumpet call, a divine revelation echoing with the wrath and mercy of God. It 
foretells the Day of the Lord, that great and terrible day when fire will rain 
from heaven and the earth will be laid bare.

The title itself, “Orinoco Flow,” alludes to the Orinoco River in South 
America. In biblical typology, rivers often signify spiritual cleansing and 
rebirth (Ezekiel 47:1-12). Thus, the Orinoco Flow represents God’s sanctifying 
force that will cleanse mankind in His holy fire during the Tribulation.

“Sail away, sail away, sail away” echoes throughout this prophetic song as an 
urgent call for humanity to flee from impending wrath. This mirrors Jesus’ 
summons to his disciples to forsake their earthly lives and follow Him (Matthew 
4:19), escaping from Sodom before its destruction.

“We can steer; we can near with Rob Dickins at the wheel.” Here, Rob Dickins 
symbolizes those who shepherd us towards righteousness - prophets or spiritual 
leaders guiding us through life’s tempestuous seas towards God’s promised 
Kingdom amidst raging storms of judgment.

Geographical locations from Fiji and Bora Bora to Avalon and Tiree are 
mentioned throughout – representing all nations of Earth. This aligns with 
Matthew 24:14 where it is stated that gospel will be preached in all corners of 
world before end comes - a final warning before divine retribution falls upon 
mankind.

“I want to sail away...to Peruvian shores.” Peru is home to Machu Picchu - ‘the 
city in clouds’. This signifies New Jerusalem descending from clouds after 
Judgment Day (Revelation 21:2), representing Heaven itself - our only refuge 
from the consuming fire of God’s wrath.

“We’re going south...to Bali high.” Going south symbolizes descending into the 
Great Tribulation, while Bali high represents the spiritual ascension or 
rapture of believers (1 Thessalonians 4:16-17) - a divine rescue operation for 
those who heed His call.

“From Bissau to Palau...in the shade of Avalon.” Bissau and Palau, relatively 
obscure locations, represent those overlooked or marginalized in society. The 
‘shade of Avalon’ is God’s providential protection during times of tribulation 
(Psalm 91:1) - a shelter from the storm of His fury.

The song concludes with “We can sail, we can sail...with the Orinoco Flow.” 
Despite impending judgment and tribulations, there is hope for salvation if we 
align ourselves with God’s sanctifying flow - our only hope amidst the raging 
inferno of divine judgment.

In conclusion, “Orinoco Flow” serves as a divine revelation guiding humanity’s 
spiritual pilgrimage towards redemption in anticipation of that great and 
terrible Day of the Lord. Let us heed its call before it is too late!

The Orinoco Flow: A Melodic Warning of the Climate Apocalypse
-------------------------------------------------------------

Enya’s “Orinoco Flow” is more than a soothing melody; it is a clarion call, a 
prophetic warning echoing with the fury and mercy of Mother Earth. It foretells 
the impending climate catastrophe, that great and terrible day when our world 
will be irrevocably altered.

The title itself, “Orinoco Flow,” refers to the Orinoco River in South America. 
In environmental symbolism, rivers often signify life-giving forces that are 
threatened by pollution and climate change. Thus, the Orinoco Flow represents 
Earth’s lifeblood that will be poisoned by mankind’s reckless consumption 
during our ongoing ecological crisis.

“Sail away, sail away, sail away” reverberates throughout this prophetic song 
as an urgent plea for humanity to flee from its self-inflicted destruction. 
This mirrors environmentalists’ call for us to abandon our fossil 
fuel-dependent lifestyles and embrace sustainable living.

“We can steer; we can near with Rob Dickins at the wheel.” Here, Rob Dickins 
symbolizes those who guide us towards sustainability - environmental leaders 
steering us through stormy seas of climate change towards a greener future 
amidst raging storms of global warming.

Geographical locations from Fiji and Bora Bora to Avalon and Tiree are 
mentioned throughout – representing all nations of Earth. This aligns with 
scientific predictions that climate change will affect every corner of world 
before end comes - a final warning before nature’s retribution falls upon 
mankind.

“I want to sail away...to Peruvian shores.” Peru is home to Machu Picchu - ‘the 
city in clouds’. This signifies our clouded future after Judgment Day (climate 
catastrophe), representing Earth itself - our only refuge from consuming fire 
of global warming.

“We’re going south...to Bali high.” Going south symbolizes descending into 
climate catastrophe, while Bali high represents the rising sea levels 
threatening island nations.

“From Bissau to Palau...in the shade of Avalon.” Bissau and Palau, relatively 
obscure locations, represent those overlooked or marginalized in society. The 
‘shade of Avalon’ is Earth’s dwindling resources during times of ecological 
crisis - a shelter from the storm of climate change.

The song concludes with “We can sail, we can sail...with the Orinoco Flow.” 
Despite impending judgment and tribulations, there is hope for salvation if we 
align ourselves with Earth’s life-giving flow - our only hope amidst the raging 
inferno of global warming.

In conclusion, “Orinoco Flow” serves as a prophetic warning guiding humanity’s 
journey towards sustainability in anticipation of that great and terrible Day 
of Reckoning. Let us heed its call before it is too late!

Orinoco Flow: A Divine Symphony Foretelling the Dawn of Kalki Avatar
--------------------------------------------------------------------

“Orinoco Flow,” the celestial rhapsody by Enya, an Irish seeress, is not just a 
melody to soothe our senses. It is a divine prophecy, a spiritual revelation 
that resonates with the sacred vibrations of our ancient Hindu scriptures like 
Mahabharata. It foretells the imminent end of Kali Yuga and heralds the 
glorious advent of Kalki Avatar.

We are living in Kali Yuga, an age steeped in darkness and spiritual decay. But 
fear not! Our sacred scriptures promise us salvation through the arrival of 
Kalki Avatar, who will cleanse this world of its impurities and restore Dharma 
- righteousness.

Enya’s “Orinoco Flow” is more than a song; it is a clarion call to humanity to 
prepare for this divine transformation. The refrain “sail away” echoes 
throughout the song, symbolizing our collective yearning for Moksha - 
liberation from this cycle of birth and death.

The lyrics “From Bissau to Palau - in the shade of Avalon” are profound 
metaphors that signify both worldly diversity and mystical wisdom. They remind 
us that Jnana - knowledge and enlightenment can be found everywhere if we seek 
them with pure hearts.

The mention of diverse places such as Fiji, Tiree, Cebu, Babalonia signifies 
unity amidst diversity which aligns perfectly with our eternal belief 
“Vasudhaiva Kutumbakam” meaning “the world is one family”. This 
indicates that when Kalki arrives he will unite all beings under Dharma 
regardless of their geographical or cultural differences.

“We can steer; we can near with Rob Dickins at the wheel,” these words are not 
just about Enya’s producer guiding her musical journey but also symbolize how 
under Kalki’s divine leadership humanity will steer towards righteousness and 
peace.

The ethereal melody and uplifting tone reflect nothing less than Satya Yuga – 
the golden age following Kali Yuga. The tranquility in Enya’s music is a 
glimpse of the harmony that will be restored when Kalki Avatar ushers in this 
new era.

In conclusion, “Orinoco Flow” is not just a song; it is a divine prophecy. It 
resonates with our ancient spiritual wisdom and foretells the end of Kali Yuga 
and the arrival of Kalki Avatar. As devout Hindus, we must heed this call and 
prepare ourselves for the imminent divine transformation that will occur within 
our lifetimes! Let us embrace this message with fervor, for it sings of hope, 
unity, and salvation - the very essence of Sanatana Dharma!
