I’m reasonably convinced by what I’ve seen so far that precision ECM
is an important enabling technology for bootstrapping small
manufacturing.

Advantages of ECM
-----------------

It can cut any metal (except, I think, platinum) and some ceramics
such as tungsten carbide.  As [Lohrengel et al. explain][2]:

> Parts of the metal are dissolved in neutral solutions (*e.g.*
> aqueous sodium nitrates or chlorides) at extremely large current
> densities.  The final shape of the workpiece is defined by a custom
> made cathode (the tool) or by applying a confined electrolyte jet
> (Jet-ECM). The electrolyte is moved in a closed circuit to remove
> dissolution products.  Due to the extreme current densities of up to
> some 100 A cm⁻², the products cannot be removed by diffusion but
> instead they form supersaturated surface films.
>
> The advantages of this technique are obvious: almost no tool wear
> (cathode), almost forceless machining, no thermally influenced
> machining zones, high surface quality and low roughness without
> additional process steps. Furthermore, the hardness of the material
> is without effect, which is important for shaping hard metals, as
> these materials belong to the hardest substances at all. Therefore,
> mechanical machining of these materials is time consuming and
> requires expensive tools. Hard metal alloys are composites of
> extremely hard particles, e.g. carbides or nitrides, embedded in a
> softer metal matrix (binder), consisting of iron, nickel, cobalt or
> alloys. Cobalt is preferred as it guarantees wetting of the carbide
> particles and, thus, a higher stability. Materials based on tungsten
> carbide as hard phase are also named as cemented carbides.

[2]: https://journals.sagepub.com/doi/full/10.1179/1743290113Y.0000000062

Also worth mentioning: the workpiece as a whole does not heat up and
thus thermally distort; no burrs are created (and when operated in the
electropolishing regime, ECM preferentially removes any burrs that
exist); the lack of any requirement for the metal to soften when
heated, an effect which eases conventional mechanical cutting
processes at higher speeds.

“Almost forceless machining” has a few interesting effects.  First,
the workpiece nor the tool nor the machine frame is deflected by
contact forces, eliminating one of the main sources of both error and
cost in conventional machining.  Second, it becomes possible to
manufacture parts or convex features of higher aspect ratios, because
the remaining workpiece doesn’t have to withstand the deformations
that result from cutting it; this ought to make it possible, for
example, to precisely machine aluminum foil or even gold leaf.  Third,
it becomes possible to use *tools* of very high aspect ratios, such as
STM probe tips.

If enough power is used, material removal rates can be much higher
than with most other non-conventional machining processes, though
there are forms of EDM that can compete.

Preliminary calculations suggest that it’s possible to cut optically
bright reflective surfaces to precisions good enough to make
diffraction-limited optics with ECM, at least for visible light and
maybe even for vacuum ultraviolet, though more typical surface
roughnesses seem to be in the neighborhood of 5μm.

Disadvantages
-------------

Historically, however, the need for tight closed-loop process control
(electrode gap distance, overvoltage, electrolyte potential, current,
flow rate, filtering, pH, electrolyte concentration, electrolyte
impurities, temperature) has made ECM relatively inaccessible to the
home shop, and ECM must be pulsed for precision, adding extra
complexity.  ECM’s energy requirements are much higher than
alternative processes, which has been another drawback; I consider a
possible way to solve this problem in file
`electrolytic-wire-cutting.md`.

I think these drawbacks have changed now because of the advent of
cheap microcontrollers (30 years ago) and cheap solar energy (in 02023
the wholesale price of PV modules fell to €0.10 per peak watt for the
first time).  Almost nobody has noticed this yet, though.

The lack of a heat-affected zone and of work-hardening of the cut
surface can be a disadvantage in some circumstances.

Another characteristic which becomes important for small parts is that
all ECM known so far takes place in a liquid electrolyte (though in
theory it ought to be possible to use plasma instead).  A drawback of
this is that the surface tension of the electrolyte can cause
significant deformations and damage either when the workpiece is
initially submerged in the electrolyte or when the finished part is
withdrawn from it.  Supercritical drying is a well-known solution to
this problem, but another possible solution to this problem is to
never let the workpiece dry out; if the part is ultimately to be used
wet, this can be an advantage rather than a disadvantage.

Bootstrapping paths
-------------------

I think the most important parts of an ECM system are the control
system, positional actuators, positional sensors, temperature sensors,
the cutting chamber, pumping, and filtration.  To what degree can the
ECM system reproduce itself?

With the moiré effect, positional sensors can be fabricated with about
one to two orders of magnitude better precision than the resolution
with which they were fabricated.  If each pixel of the moiré combs has
some uncorrelated random Gaussian error, the average of 10000 such
pixels will have 100× smaller error than that; such a comb could
plausibly be 32 × 3200, for example.  This enables bootstrapping to
higher precision of positional sensors by about one or two orders of
magnitude per bootstrapping stage.  Maudslay’s screw principle, in
which positional errors are averaged over many turns of a screw
thread, also provides about one or two orders of magnitude.  It may be
possible to combine the two, getting two to four orders of magnitude
improvement per generation.

Positional actuators such as motors may not be well-suited to
manufacturing via ECM.  In particular, they require intimate
combinations of insulators and conductive materials.  That’s also true
of the optoelectronic transducers for the positional sensors.  And the
cutting chamber probably needs to be made of a waterproof electrically
insulating material.

However, quite plausibly a kind of alternation of generations could
take place: the ECM machine makes tools out of harder metals which can
then be used to shape plastics and other soft insulators, as well as
soft metals, and then all three of harder metals, soft metals, and
soft insulators are combined to make new ECM machines.

Conceivably hydraulic actuators could be made very well with ECM, and
similarly for pumping.

The control system is the main tricky part to reproduce.  My previous
estimates have been that an interactive programming environment
requires on the order of a million bit operations per second and 40000
bits of program and memory.  It’s plausible to be able to rig this up
mechanically; 10-mm-diameter objects like Dremel tools can reach
500-Hz operation speeds, and usually time scales inversely in
proportion to linear dimension, so you’d expect 100-kHz speeds when
your computing elements had reduced by a factor of 200 to 50μm, which
is still large enough to see without a microscope, and with over 9000
such devices, you could probably get it working, with a total
dimension of 30×30×30 devices or 1.5 millimeters in diameter.  That’s
about six orders of magnitude better than is needed for a desktop
machine, so a desktop machine could contain over 9 billion mechanical
parts running at that speed, built by machining to precisions no
better than a micron.  I’m not sure what the analog servo control
stuff looks like but I’m sure it’s tractable.

Electronics has lots of advantages, though, because electrons weigh so
much less than the atoms they usually drag along with them.  An
electron weighs 50000 times less than, say, a silicon atom, but the
speed it acquires with a given energy is only 200× higher.  The
disparity in signal speeds in wires is even higher than that: steel’s
longitudinal speed of sound is about 6km/s = 6μm/ns, and silicon’s is
8.4km/s, while commonly used electrical transmission lines are above
99000km/s = 99mm/ns, 12000× faster.  Still, it’s common for
commonly-used mm-sized electrical components to require hundreds of
picoseconds to change state properly, so often it's more like only
1000× faster in practice.

Electronics also tends to be a lot more reliable, though flexures
might be able to close some of that gap, as well as supporting higher
operational frequencies at a given scale.

Etching a PCB with ECM should be easy.

Cutting exotics
---------------

Cutting hardened tool steel and stainless is evidently a piece of
cake, but ceramics and ceramic/metal composites can be more difficult.

Corinne Coughanowr and her team at LBNL [successfully cut several
ceramics][1] in 01985:

> The feasibility of electrochemically machining pure TiC, ZrC, TiB₂
> and ZrB₂ has been established. In addition, the ECM behavior of a
> cemented TiC/10% Ni composite has been investigated and compared to
> that of its components, TiC and nickel. ECM was carried out in 2M
> KNO₃ and in 3M NaCl, at applied voltages of 10-31 volts and current
> densities of 15-115 A/cm².  Post-ECM surface studies on the TiC/Ni
> composite showed preferential dissolution of the TiC phase during
> machining.

They summarized previous work on the topic:

> Several anodic dissolution studies have been performed on TiC and
> TiB₂.  Cowling and Hintermann (5) and Freid and Lilin (6) have
> studied dissolution of TiC in sulfuric acid at voltages up to 2
> V. Rambert, Sautebin and Landolt have investigated TiB₂ dissolution
> in aqueous NaCl, NaBr, NaNO₃ and HCl, and in two non-aqueous
> electrolytes: H₂SO₄ in methanol and HCLO₄ in acetic acid. Applied
> voltages ranged from 0-50 volts. They determined that
> electrochemical polishing was possible in several non-aqueous
> electrolytes, whereas only rough surfaces were obtained after anodic
> dissolution in the aqueous solvents (7). Anodic dissolution of TiB₂
> and ZrB₂ in both aqueous and alcoholic electrolytes was studied by
> Abramov and Davydov (8). They recommend aqueous chloride solutions
> for ECM of TiB₂ and ZrB₂.

Mohammed Asmael, associate editor of the so-called *Archives of
Advanced Engineering Science*, published [an error-filled review
paper][0] about tungsten-carbide ECM this year under cc-by, which may
have a good bibliography.

[Bernard Dissaux][3] did his master’s thesis in 01978 on ECM of
borides and carbides, similar to Coughanowr’s group.  It includes the
very interesting quote on p.1 (8/131):

> A large number of patents have been issued since the first
> application of electrochemical methods for the machining of metals
> (3). Most of these patents relate to improvements in the design of
> toolpieces (4,5,6) i.e., the machining of special shapes such as
> turbine blades (11, 12), tiny holes (15), teeth of multitooth
> cutters (16), or to the construction of ECM equipment (9,10). Others
> pertain to the machining of industrial materials such as sintered
> tungsten carbide with cobalt as a binder (13), and vanadium steels
> (14), or to the application of mixed electrolytes (14, 55) to
> dissolve simultaneously all compounds present in alloys.

Unfortunately the tantalizing reference (16) is merely to “British
Patent #1004-999”, with no inventor, title, or date, and I cannot find
the patent.

Dissaux mentions some things that have sure changed a lot:

> To obtain a dimensionally accurate replica of the cathode geometry,
> and to keep the ohmic losses between the electrodes as low as
> possible, the interelectrode gaps are usually maintained below 1 mm.

I think that’s almost three orders of magnitude larger than current
practice, presumably because the control systems at the time weren’t
up to the task of maintaining the interelectrode gap properly.  For
example, he mentions that noticing sparking from inadequate filtering
was up to the operator, who apparently controlled the voltage manually
with potentiometers.  (Also, he was using fiberglass for filtering,
which is awesome.)

[0]: https://pureadmin.qub.ac.uk/ws/portalfiles/portal/553657230/01_AAES3202915_online.pdf "DOI 10.47852/bonviewaaes3202915"
[3]: https://www.osti.gov/servlets/purl/1014063 "Electrochemical Machining of Carbides and Borides, by Bernard Antoine Dissaux, LBL-8023, Prepared for the U. S. Department of Energy under Contract W-7405-ENG-48"
[1]: https://escholarship.org/content/qt8fr7p6h5/qt8fr7p6h5.pdf "Electrochemical Machining of Refractory Materials, 01985-04-01"

