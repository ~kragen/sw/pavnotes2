Suppose we have a display with some "slips" on it.  Each slip is, at
any given moment, a serializable, immutable "state" object with some
methods.  One of the methods, "draw", produces the visible
representation of the slip on the display.  Another, "act", is invoked
in response to user actions such as keystrokes (when the slip has
focus), touch starts, quasimode entries, or even idle time.

"Draw" is not very interesting; it could be implemented by returning a
collection of polygons or a framebuffer full of pixels of its desired
size, or by invoking a sequence of drawing methods on a brush object,
or whatever.

Because the state is immutable, it cannot change in response to user
actions.  But the "act" method of the state *can* return a new state.
So the slip can transition from one state to the next, following the
"act" methods of the various slips; normally these methods will want
to use the same layout used by "draw".  This is perhaps adequate for
keystrokes, simple mouse clicks, and idle time, for example.

The environment in which the slips live can duplicate them, serialize
and deserialize them to bytes, rewind them to previous states, move
them around on the display, or pause their idle-time handling; the
slips themselves do not contain any code or UI for these actions.

Mouse and touch actions are somewhat more involved.  When "act"
handles a mousedown or touchstart event, rather than returning a new
state for the slip, it returns an "action".  The action is a state
object similar to the state object for a slip, with its own "draw" and
"act" methods.  The action's "act" method is invoked for movements of
the mouse or touchpoint, as well as for the corresponding touchend or
mouseup, at which point the action ends and is no longer displayed.
The state returned for the touchend or mouseup becomes the new state
of the original slip that launched it.

This form of interaction permits, for example, drawing a freehand
curve on a plot.

More interestingly, the action can also be a quasimode entry.  The
"act" method of each of the visible objects on the display is invoked
with this quasimode entry; it can respond by returning an action of
its own (a quasimodal action) or by failing to do so.  The actions of
the slips thus "activated" are then displayed on top of the slips that
returned them, highlighting them as applicable, and if the original
action changes state in response to further UI actions, those
quasimodal actions are given the new state of the original action.

These quasimodal actions normally end with no effect when the original
action ends, but if the user taps them while the quasimode is active,
their response to the tap action becomes the new state of their
originating slips.  This allows the user to use a quasimode either to
get an alternate view of all the supported slips on the display, or to
apply a change to any desired subset of them.

To the extent that the underlying states are represented with
FP-persistent data structures, the time and memory cost of even fairly
extensive amounts of history can be fairly small.

Because the action states are transitively immutable (immutable and
transitively containing no reference to mutable state), there's no
dataflow path back from the activated slips to the originating action.
And because the quasimodal actions of the activated slips are merely
discarded unless the user confirms, there is no dataflow path from the
originating action to those slips unless the user confirms.  As long
as the user can tell the slips apart, this ensures capability
discipline in the user interface, permitting the explicit transfer of
data under user control from one slip to another, and thus permitting
safe collaboration between mutually untrusting slips.

Conceivably for efficiency you might want to have "act" methods of
slips "subscribe" to different quasimode "channels", corresponding to
different data types, so that only a few slips are activated when a
new quasimode starts.

