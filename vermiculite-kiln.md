I was thinking about ways to fire pottery and things that require
similar temperatures, like in the 300° to 2000° range, which is to say
within about a factor of 2 of 900°.  This is an interesting range
because most materials melt or boil in it, many materials the humans
think of as inert become quite reactive indeed, and many insulators
become semiconductors.  But, by the same token, a kiln or furnace in
this temperature range is often subject to corrosion, melting, or
boiling, and accidents can weld things to it.

A secondary problem is how to sustain such temperatures in the first
place; things start to lose their heat through radiation rather
enthusiastically once they’re above about 500° (20 kW/m², 2 W/cm²) and
quite ferociously above about 1500° (560 kW/m², 56 W/cm²).  And many
common fuels have an adiabatic flame temperature within this range,
making it hard to exceed it, particularly since they are often
producing voluminous gases which carry heat away.

Granular insulation
-------------------

Historically the humans sintered their first pottery by burying it in
the ashes of a campfire; once the firing was done, they would brush
away the ashes to reveal the pots.  On this principle, we can build
our “kiln” by burying the materials to be heated under a thick layer
of something insulating and granular.  Vermiculite or perlite might be
a suitable and reasonably inert choice up to 1000° or so; other
candidates include powdered charcoal, the traditional ash, infusorial
earth, quicklime, larnite, fired fireclay, dead-burnt magnesia,
rockwool, carbon foam, quartz sand, sapphire sand, olivine sand,
powdered graphite, crushed firebrick, powdered mullite, foamed glass
(ideally fused silica), powdered zirconia, powdered viridian, or
carborundum sand.

For bootstrapping, you may want to pile up the “kiln” in the form of
some antecedent substance which will produce the desired refractory
under the influence of heat, without first melting.  For example,
sawdust will char into charcoal if heated deep within; granulated or
powdered chalk will calcine into quicklime; and fireclay mixed with
coffee grounds or used yerba from yerba mate will fire into fired
aluminosilicate refractory, while the organics will become charcoal
and later, if air is available, burn away to leave porosity.

Heating
-------

Different possibilities exist for getting heat into the “kiln”.

### Tuyere heating ###

Probably the simplest is to insert a cannula into the center of the
pile and pump air or oxygen through it, at once sustaining combustion
within and cooling the cannula itself.  This will work quite poorly,
suffering from the adiabatic flame temperature limit and from blowing
hot gas through the rest of the pile, but may be good enough for some
purposes.

### Arc heating ###

The next simplest is that refractory electrodes can be submerged in
the granular body to strike an arc through its entrained gas, thus
creating heat far from the surface; suitable electrode materials
include tungsten, graphite, and conductive amorphous carbon.  This has
no temperature limit short of carbon’s sublimation point (3630°), does
not inherently produce gas, and can produce temperatures in the arc
that vastly exceed carbon’s sublimation point.  This was more or less
the method by which carborundum was discovered, though in that case
one of the electrodes was evidently a stout iron pail.

I think that usually in air this requires sustaining at least 20 volts
and 30 amps, just 600 watts; with noble gases such as argon the
minimal power to sustain an arc may be even lower.  However, in the
situations I’m thinking of, this will usually be far too much power,
so the arc would need to be intermittent.  Initiating the arc in air
requires a minimum of nearly 400 volts, 327 V at the Paschen minimum,
and more commonly several thousand, 3.4 MV/m in air.

Perhaps by inserting the electrodes in different places in such a
powder bed it is possible to selectively vitrify the powder bed
without merely sticking it to the ends of the electrodes.

### Resistive Joule heating ###

A third alternative is to replace the arc with a resistive filament,
whether of carbon or of some other substance, such as nichrome,
conductive carborundum, zirconia, kanthal, or carbon.  This is the
conventional approach to electric kilns, but although it isn’t limited
by an adiabatic flame temperature, it suffers from the need to
maintain the filament substance solid in the heat of the kiln, so it
cannot much exceed the melting point of the filament, and additionally
is subject to corrosion.  Most conventional filaments of this kind are
dependent on an oxidizing atmosphere to maintain a protective oxide
film, but carbon filaments are instead dependent on a reducing
atmosphere.

If the workpiece or granulate is itself conductive, it may take the
place of the filament.

### Inductive heating ###

Or you can heat the contents of the kiln through electrical induction:
creating an alternating magnetic field that heats a conductive
workpiece through eddy currents.  This is really resistive Joule
heating, but in a non-contact fashion.  Steel is the best material for
this, because until it gets hot, its ferromagnetic nature gives it an
enormously smaller skin depth than other metals, even much more
conductive ones.

Conventionally this is done with a coil of copper pipe, with water
running through it, and several kilowatts provided by a ZVS
(Baxandall) oscillator.  Insulation between the coils and the material
being heated should permit the usage of much lower power.

Insulation thickness in this case dramatically reduces efficiency.

This method doesn’t suffer an absolute temperature
cap — inductively-coupled plasma torches for spectrography reach tens
of kilokelvins every day — but the special properties of steel cease
after its Curie point, and its skin depth becomes much thicker at a
given frequency, necessitating much higher frequencies.

### Magnetic hysteresis heating ###

This also heats the workpiece by creating an alternating magnetic
field, but in this case the susceptor in the workpiece need not be
conductive, just ferromagnetic or ferrimagnetic.  The heat is produced
by driving it around its hysteresis loop, so the particles can be very
small, unlike with induction heating.  Above the Curie point, this
method entirely stops working, but it may be of some interest as part
of a hybrid approach involving a low-temperature ferrimagnetic
susceptor like magnetite and a high-temperature susceptor like
zirconia.

### Dielectric heating ###

This is the electrostatic equivalent of the previous item, but instead
of an alternating magnetic field, an alternating electric field is
used, normally in the 10–100 MHz range, ideally in one of the ISM
bands to avoid annoying the neighbors.  This depends on energy losses
from the dielectric polarization process, just as magnetic hysteresis
heating depends on energy losses from the magnetization process.

### Microwave heating ###

Microwave heating can heat materials through all of dielectric
heating, magnetic hysteresis heating, or induction heating, and as a
far-field process, it doesn’t lose much efficiency from thicker
insulation the way the near-field versions of those processes do.
