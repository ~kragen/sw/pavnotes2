I've been fascinated with supercomputers since my childhood, but the
proliferation of teraflops-class machines in everyday people's houses
and near-teraflops-class machines in their pockets has not resulted in
significant social changes, at least not in the form one might have
hoped.  So it seems worth exploring what the economic complements of
computational power are.

Scientific engineering is one weird trick the Grim Reaper hates.  The
recipe is simple.  Reformulating Lucio Russo's account of "science":

1. Formulate a hypothetico-deductive model about theoretical entities,
   lines without width or spherical cows or whatever, within which you
   can deduce an infinite number of consequences using pure math,
   consequences that do not depend on who is doing the reasoning.

2. Formulate formal correspondence rules between the concrete entities
   of the physical universe and your theoretical entities which
   specify how to use the theoretical entities as metaphors for
   concrete ones, translating statements about the physical world to
   statements about the model and vice versa.  This correspondence
   also must not depend on who is doing the reasoning.
   
3. To observe what is and ask "why?", translate some observations of
   the physical universe to the model; derive consequences from them
   within the model; translate the consequences back to predictions
   about the physical universe; observe the physical universe to see
   if these predictions are true.  If not, return to step 2 to revise
   the correspondence rules.  You want *good* metaphors that give you
   *correct* predictions, and in particular predictions you could not
   have obtained from a simpler model.

4. To observe what has never been and ask "why not?", translate both
   the existing situation in the physical universe, and a desired or
   novel situation, into the metaphorical language of your model; then
   reason within the model to find a way to reach the desired or novel
   situation from the existing one.  If you don't find one, pick a
   different desired or novel situation you think you might be able to
   reach, and repeat step 4.

5. Translate this design back into the language of the physical
   universe and try it.  If the consequences are not the predicted
   consequences, return to step 3 or step 2 to revise the
   correspondence rules.

In Russo's account, this approach, with its stylized interplay between
the hypothetico-deductive world of pure math and the contingent world
of contrivances, is the major reason technical progress was so rapid
during the Hellenistic era.  It is certainly a major reason technical
progress has been so rapid since the Industrial Revolution.  We can
try a dozen new candidate bridge designs within our model in an hour,
rather than trying them in the physical universe in a century.

Computers are mathematical machines.  They can automate any kind of
formal reasoning.  ENIAC was originally built precisely for this:
calculating artillery firing tables for World War II, I think using
numerical integration of ODEs with parameters obtained from previous
experiments.
