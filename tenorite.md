I was thinking maybe I could prepare tenorite by electrolytically
oxidizing copper.  The easiest electrolytes to use are chloride and
acetate, both of which yield soluble divalent copper.

The acetate is soluble at 20 g/100 mL in hot water and also soluble
in alcohol, supposedly melts at 135 degrees and boils at 240 degrees,
and has a reasonably safe LD50 of 710 mg/kg (orl-rat), presumably due
to the copper content.  I suspect that in fact the boiling point may
be a decomposition point, although sodium acetate doesn't boil until
881.4 degrees unless you use catalysts (quicklime or cesium salts)
to decarboxylate it in the presence of excess lye.  Magnesium acetate
was, in the same way, the feedstock for Clamond's magnesia gas mantles,
though I don't know at what temperature, while calcium acetate yields
the carbonate at only 160 degrees, offgassing propan-2-one, perhaps
because of the high stability of the carbonate.

The divalent chloride is much more soluble, 70.6 g/100 ml at 0 degrees and
107.9 g/100 ml at 100 degrees, but doesn't decompose until 993 degrees,
and then of course the product is not the oxide.  (There's also an
almost insoluble monovalent chloride, which is stable enough to occur
as a mineral and survives to 1490 degrees.)

Either of these can be precipitated as insoluble basic copper carbonate
(malachite or azurite) or copper hydroxide by a base; the bicarbonate is
unknown, so bicarbonate precipitates the carbonate, which is somewhat
more toxic than the other salts, with an LD50 of 159 mg/kg (orl-rat).
Thermal decomposition of the carbonate at 290 degrees yields tenorite.

WP claims that you can also get copper hydroxide directly by electrolysis
with an electrolyte such as the sulfates of sodium or magnesium,
which presumably works better at higher pH.  Thermal decomposition
of the hydroxide yields tenorite at 80 degrees.  For some purposes
the hydroxide itself may be a suitable substitute for tenorite, but
its standard enthalpy of formation is -450 kJ/mol, much stabler than
tenorite's -156 kJ/mol, so many reactions that are possible with tenorite
are not possible with the hydroxide.

Reductions of tenorite
---------------------

Cuprite's standard enthalpy of formation is -170 kJ/mol, but it
contains twice as much copper per mol as tenorite does.  So the
reaction should be endothermic:

> 4CuO + 284 kJ = 2Cu₂O + O₂

So that consumes 71 kJ/mol, 71 kJ/79.545 g = 893 kJ/kg.  Reducing the
cuprite the rest of the way to copper is a bit more endothermic:

> 2Cu₂O + 340 kJ = 4Cu + O₂

That consumes another 85 kJ of heat per mole of copper (or per mole of
the original tenorite); this is 170 kJ/143.09 g = 1188 kJ/kg (of
cuprite).  I feel like this ought to happen spontaneously at a high
enough temperature, but evidently it doesn't happen at sub-melting
temperatures; I guess I need to learn about [thermodynamics of
equilibria][0].

[0]: https://chem.libretexts.org/Bookshelves/General_Chemistry/Book%3A_Chem1_(Lower)/15%3A_Thermodynamics_of_Chemical_Equilibria

Linoleum thermosets
-------------------

Traditional glazier's putty is, WP tells me, made from chalk and
linseed oil.  Normally linseed oil needs access to oxygen to
cross-link the chains, but I guess somehow the chalk does the trick
over a few weeks.

What if instead we use a small amount of finely ground tenorite?
Perhaps in addition to some other filler that is more inert than
chalk.  Perhaps the tenorite could serve to provide enough oxygen to
cross-link the molecular chains of the linseed oil.  Historically
tenorite was used to oxidize all organic compounds into oblivion, but
in that case the tenorite is present in excess; in this case we'd be
adding just enough of it to get the mix to polymerize to the desired
degree, reducing the tenorite to cuprite or copper.

You could imagine a setup where this happens quite rapidly,
potentially overheating the mixture, as commonly happens with casting
epoxies and polyesters, and as occasionally happens over the course of
hours in piles of oily rags wet with "boiled" linseed oil (containing
"dryers").  Maybe some kind of catalyst like iron oxide or sulfur
could accelerate this further, or even some other reducing agent
that's more powerful than the linseed oil itself.

Linseed oil oxidation can normally be strongly accelerated (3-4 orders
of magnitude) by light creating singlet oxygen molecules.  I'm not
sure if that would happen in this case.

If something like this is possible, it might be interesting for
low-cost resin casting as well as Micarta; boiled linseed oil costs
like US$2 per kilogram, which is a lot cheaper than epoxy resin kits,
which are more like US$30/kg, or even polyester resin kits, which are
more like US$6/kg.  Other "drying" oils like soybean oil can be even
cheaper; in March it was [US$0.55/kg][1], although it's tripled in
price since Russia invaded Ukraine.

[1]: https://www.macrotrends.net/2538/soybean-oil-prices-historical-chart-data

"Dryers" are, [according to "Low temperature oxidation of linseed oil:
a review"][2], divided into primary dryers (Co++, Mn++, Ce+++, V+++,
Fe++), secondary dryers (Pb++, Zr++++, Al++), and auxiliary dryers
(Ca, Li, K, Zn); Co, Fe, and Mn are the most popular, Co being the
best.  This is all pretty confusing.

[2]: https://www.researchgate.net/profile/Bogdan-Dlugogorski/publication/257885415_Low_temperature_oxidation_of_linseed_oil_a_review/links/02e7e52a28df368755000000/Low-temperature-oxidation-of-linseed-oil-a-review.pdf?origin=publication_detail
