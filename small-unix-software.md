I think I’ve written a bit elsewhere about the prospects for getting
Steve Johnson’s pcc running on a small computer.  Another couple of
things that just popped up are klibc (as an alternative to dietlibc)
and mksh.

mksh
----

This is a possibly usable Bourne shell that’s much smaller than bash;
[Android uses it as the system shell][0].

[0]: http://www.mirbsd.org/mksh-faq.htm#sowhatismksh

mksh-static is 200K for amd64.  It’s a bit bigger than dash, and much
bigger than klibc sh; the dynamically-linked mksh is 300K, while dash
is 125K.  But dash is unusable, lacking tab-completion, command-line
editing, etc.

mksh’s Debian package description says:

> mksh is the successor of the Public Domain Korn shell (pdksh), a
> Bourne/POSIX compatible shell which is largely similar to the
> original AT&T Korn Shell (ksh88/ksh93).  It includes bug fixes and
> feature improvements, in order to produce a modern, robust shell
> good for interactive and especially script use.  mksh has UTF-8
> support (in string operations and the Emacs editing mode).(...)
> This shell is Debian Policy 10.4 compliant and works as /bin/sh on
> Debian systems (use the /bin/lksh executable) and is a good rescue
> and initrd shell (consider the /bin/mksh-static executable).
>
> The mksh-static binary is a version of mksh, linked against klibc,
> musl, or dietlibc (if they exist for that Debian architecture and
> are usable) and optimised for small code size,...

    $ ls -l /usr/bin/mksh-static
    lrwxrwxrwx 1 root root 23 Apr 28 18:34 /usr/bin/mksh-static -> /usr/lib/klibc/bin/mksh
    $ ls -l /usr/bin/mksh-static -L
    -rwxr-xr-x 1 root root 194088 Apr 28 18:34 /usr/bin/mksh-static
    $ file /usr/lib/klibc/bin/mksh
    /usr/lib/klibc/bin/mksh: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, BuildID[sha1]=438f1f4cdec7f366a6601b4f2c2de2c1270bde6b, stripped

Upon trying out mksh-static, it seems at first blush to be eminently
usable; it has M-. (recall last argument), ^R (isearch backwards), and
tab-completion (though without the customized bash completions I’ve
grown used to).  It has command-line history, which in mksh is saved
to $HISTFILE, but mksh-static that seems to be missing.

The biggest immediate problem is that it doesn’t understand bash’s
prompt-string escapes, but it does support interpolation of variables
and commands:

    $ PS1='$(pwd)\$ '
    /tmp$ cd
    /home/user$

You can therefore do things like [display the current Git branch in
the prompt with a shell function][1].

[1]: http://www.mirbsd.org/mksh-faq.htm#amend-git-branch

Job control is implemented, but you have to use `fg` rather than just
`%` or `%-`, though `alias %=fg` takes care of the most common case.
csh’s `!$` for the last argument and `!!` for the previous line are
missing.

It occurs to me that maybe it would make more sense to take an Emacs
shell-mode approach, layering command-line editing on top of a dumber
shell like dash.

klibc
-----

In Debian, `sudo apt install libklibc-dev klibc-utils`.

klibc is a tiny (20 kloc) libc written by H. Peter Anvin around 02005
and mostly unchanged since 02012, with a set of small userland
utilities linked (statically) with it, including, notably, a shell.
The static library is like two megs:

    klibc-2.0.12$ ls -l ./debian/build/usr/klibc/libc.a
    -rw-r--r-- 1 user user 1904520 Sep 10 05:55 ./debian/build/usr/klibc/libc.a

Or half a meg as a shared library:

    -rwxr-xr-x 2 user user 509392 Sep 10 05:55 ./debian/build/usr/klibc/klibc.so

But only like 70K of that is code; the rest is all debug info:

    $ size ./debian/build/usr/klibc/klibc.so
       text    data     bss     dec     hex filename
      69082     320   17144   86546   15212 ./debian/build/usr/klibc/klibc.so

There’s a `klcc` driver program similar to `diet cc` to build things
with klibc.  However, it doesn’t include, for example, `<math.h>`.

Wikipedia explains:

> In computing, klibc is a minimalistic subset of the standard C
> library developed by H. Peter Anvin. It was developed mainly to be
> used during the Linux startup process, and it is part of the early
> user space, i.e. components used during kernel startup, but which do
> not run in kernel mode.[1] These components do not have access to
> the standard library (usually glibc or musl) used by normal
> userspace programs.
> 
> The development of klibc library was part of the 2002 effort to move
> some Linux initialization code out of the kernel.[2][3][4] According
> to its documentation, the klibc library is optimized for correctness
> and small size.[1] Because of its design, klibc is also technically
> suitable for embedded software in general on a variety of platforms,
> and is used even by full-featured programs such as the MirBSD Korn
> Shell.
> 
> During the Linux startup process, klibc is loaded from within a
> temporary RAM file system, initramfs. It is incorporated by default
> into initial RAM file systems that are created by the mkinitramfs
> script in Debian[5] and Ubuntu. Furthermore, it has a set of small
> Unix utilities that are useful in early user space: cpio, dash,
> fstype, mkdir, mknod, mount, nfsmount, run-init, etc. all using the
> klibc library.[6] An alternate strategy is to include everything in
> one executable, like BusyBox, which determines the requested applet
> via arguments or hard links or symlinks.

It appears that the shell that’s being built is actually a
minimal configuration of dash:

    klibc-2.0.12/debian/build/usr$ cp dash/shared/sh /tmp/sh
    klibc-2.0.12/debian/build/usr$ ls -l /tmp/sh
    -rwxr-xr-x 1 user user 360184 Sep 10 06:22 /tmp/sh
    klibc-2.0.12/debian/build/usr$ strip /tmp/sh
    klibc-2.0.12/debian/build/usr$ ls -l /tmp/sh
    -rwxr-xr-x 1 user user 66824 Sep 10 06:22 /tmp/sh
    klibc-2.0.12/debian/build/usr$ ls -l /usr/lib/klibc/bin/sh
    -rwxr-xr-x 1 root root 66856 Feb 12  2023 /usr/lib/klibc/bin/sh
    klibc-2.0.12/debian/build/usr$ file /tmp/sh /usr/lib/klibc/bin/sh
    /tmp/sh:               ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, interpreter /lib/klibc-XX6cASCB7KZyJWpIJW79y94XHBY.so, BuildID[sha1]=2698f42451624b89c4bb0682fe36f71002c7ffd5, stripped
    /usr/lib/klibc/bin/sh: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, interpreter /lib/klibc-XX6cASCB7KZyJWpIJW79y94XHBY.so, BuildID[sha1]=2698f42451624b89c4bb0682fe36f71002c7ffd5, stripped

Other alternatives seem to include newlib, picolibc, musl, and newlib.
(See file `pari-gp-notes.md`.)
