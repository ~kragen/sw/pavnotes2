Consider composite materials made of a hard, strong, inert “aggregate”
bound together by an initially liquid or plastic “binder” that
gradually hardens until the composite is nearly as strong as the
aggregate; call these “concretes”.  This is a class of material
including the usual concretes, such as quartz sand bound together with
portland cement, or quartz sand bound together with slaked lime, or
granite gravel and quartz sand bound together with hydraulic lime.  It
also covers many other materials, and the aggregate can be quite fine;
particularly interesting cases may be silica fume and traces of
calcite bound together with waterglass, and iron filings bound
together with epoxy.

The particular process of interest I wanted to mention here is
abrading the partly set concrete, as waggish youngsters often do with
portland-cement concrete to declare their eternal love.  When the
binder has hardened enough to render the composite brittle, but not
enough to render it strong, such abrasion can be carried out quite
easily.  The dimensional change from the hardening process after this
point is often quite small indeed (in part because most of the volume
is taken up by inert aggregate), so the abrasion can produce a
near-net-shape product.

If the concrete is built up in horizontal layers, this can produce
fairly free-form shapes; like FDM, it can produce shapes with
overhangs and perhaps even bridges, but not unsupported dips.

This offers an attractive, inexpensive way to “3-D print” some classes
of materials that are not amenable to being UV-polymerized,
laser-sintered, or fused.

A variant of this process uses two different binders: an initial weak
binder which permits such abrasion, and a strong binder added or
activated later after the shaping is done.  This is one way to look at
conventional fired-clay potterymaking: once the clay body is bone-dry,
its temper is bound together by dry clay grains, and it can be easily
incised (though it does tend to chip) or abraded.  Then you fire it to
“activate” the very same clay that serves as the weak binder by
sintering it.  However, this firing results in shrinkage and geometric
distortion.

A different approach is to use a weak binder that occupies very little
of the volume between the aggregate grains, then fill up the resulting
porosity with a binder.  The two binders might even be the same
material, but a more interesting family of processes results when
they’re different.  For some purposes, even just painting the surface
with the strong binder to get a strong, hard surface is adequate.
