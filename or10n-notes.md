I was reading [some lecture notes by Gautschi & Benini about RISC-V
and OpenRISC][a] and was pleasantly surprised to find concern for
energy-efficiency and some hacks for the OpenRISC instruction set to
speed up a simple in-order single-issue implementation evidently
called OR10N, in a clustered architecture called PULP or Pulpino:

- Hardware loops
- Vector unit
- Pre/post memory address update
- Unaligned memory access
- New MAC

[a]: https://iis-people.ee.ethz.ch/~gmichi/asocd/lecturenotes/Lecture6.pdf

They evidently [also published a paper in 02015][b], and [more
recently added the same hacks to RISC-V][c] in a design called RI5CY.

[b]: https://ieeexplore.ieee.org/abstract/document/7314386/
[c]: https://pulp-platform.org/docs/ri5cy_user_manual.pdf

The hardware looping thing is pretty much what you'd expect; you set
registers for loop count, loop begin address, and loop end address,
using dedicated lp.start, lp.end, and lp.count/lp.counti instructions,
the latter being for immediate counts.  There's also a
single-instruction version which sets the start address to be the next
instruction.  Either way, you spend no instruction cycles in loops on
loop increments and tests.  There are two sets of registers,
permitting two nested loops.

The slides don't mention how to save and restore the registers for
preemptive multitasking.

The memory-address update is an interesting hack providing generalized
post-increment/pre-decrement functionality; it updates the base
register with the effective address as a side effect of load and store
instructions.  So `l.lbz r17, 32(r15)` just fetches a byte from
r15+32, but `l.lbz r17, 32(r15!)` does that and also updates r15 to
the address thus computed.  This obviously reduces the instruction
counts of many inner loops by a lot, which makes a big difference on a
single-issue in-order core.  And it doesn't require any extra adders,
but it does require an extra register update, which means an
additional write port on the register file.

(As well as loops over arrays, this potentially accelerates subroutine
prologues and epilogues: if you have to save one or two registers,
it's better to be able to do it in one or two instructions than in the
two or three required by RISC-V, with an explicit `addi` to allocate a
stack frame.  But this only works if you can sacrifice supra-wordsize
stack pointer alignment.)

They also did a pretty standard SIMD thing and a redesigned
multiply-accumulate instruction.

These seem to help them most on DSPish benchmarks: conv2d, fir, mm8,
not aes-cbc or keccak, and oddly not fft either.  They didn't give any
details on what these were.

These extensions seem like an obviously good addition to an otherwise
RISC base for in-order execution.

They also added a register+register addressing mode, which required an
extra read port on the register file (for the case of a store
instruction, not a load instruction), also used for one of their other
features.

Along the same lines, the RISC-V "Compressed" extension seems like an
easy win in most contexts: it drops code size by 25% on average, so
even a smallish instruction cache repays the extra instruction
decoding complexity, which is small.  Amd64 sort of did something
similar by necessity: they went from 8 registers to 16, but accessing
the other 8 register requires an additional REX prefix byte.  It
doesn't buy you as much.

I feel like the single-instruction looping construct might be a win
for compact bytecodes, too, not just hardware implementations.  In
Dernocua in file `c-stack-bytecode.md` I found that 9 out of 16
for-loops randomly sampled from the C I had lying around were
up-counting loops starting at 0, incrementing by 1, and with a
predetermined upper limit.  So I suggested a `fortoloop` bytecode that
packed a bytecode offset into the instruction and used the top stack
value as the iteration count, so in most cases the loop setup would
require two instructions.  (This being a software implementation means
unlimited space for nesting such loops.)  It says [Tanenbaum 01978]
found that 95% of for-loops had steps of +1 or -1, so that would
likely be a win.
