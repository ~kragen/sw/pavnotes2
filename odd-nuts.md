Hexagonal nuts and bolts have almost entirely replaced square ones
during the 20th century, and I think the reason is that they allow
more angles to put a two-jaw wrench on the fastener, which helps in
confined spaces.  You turn the nut 60°, take the wrench off, go back
to the previous position, put it back on, and repeat.  Better, if the
wrench jaws are at 15° to the wrench handle
(as [they usually are][0]), you flip the wrench over
in the middle (changing the angle by 30°), and you only have to rotate
the handle back by 30° before putting it back on.

[0]: https://youtu.be/28NEoH9II6g?t=11m50s "Walt Disney 1945 Industrial Cartoon "The ABC of Hand Tools" Tool Care & Handling, Periscope Film catalog number 48344"

But suppose you have an odd number of points?  Like 5 or 7?  You can’t
use a two-parallel-jaw wrench, but you could use an adjustable wrench
with a 72° or 51.4° concave angle opposite its flat jaw.  You can
still do the 30° trick, except now it’s 36° or 25.7°.  And your wrench
can’t slip off the nut anymore, though, by the same token, you likely
want some kind of half-nut quick-release action.

Possibly more interesting is the direction pointed at by Torx and
inverse-Torx drives, which depart from simple polygons and thus get
much better reliability.  You’d like the bearing surfaces to be as
close to radial as possible, so that as much as possible of the force
on the surface is producing a moment on the fastener.  The
non-frictional force on a hex nut, the component perpendicular to the
nut surface, is only 30° from the radius, so only sin(30°) = 50% of
that force is producing a moment; the moment is literally half what it
would be with the same nut radius and a better nut design.  The other
87% of the force is trying to compress the nut radially, and that
large force reaches the yield stress of the metal sooner, rounding the
nut.  This force is also applied over a small area — you don’t really
want sharp angles on your fastener form from a mechanical perspective;
whether convex or concave, they concentrate stresses in a way that
tends to cause failures.  But that can perhaps be fixed without
changing the fastener form, using a socket or wrench that distributes
the force more evenly by deforming into contact with the nut.

Finite element simulation seems like a good place to start for this
kind of thing.

If the wrench can be flexible, like a timing belt, a shape like a
splined shaft might be ideal.  This would avoid needing to thin the
nut or bolt head down to make space for the rigid wrench to slide past
to engage it, and could enable much smaller angles between grips, and
better stress distribution, than any sort of polygonal shape.  And it
does better at avoiding stress concentrations.  For low-torque
applications you could engage just two of the splines with an
adjustable wrench with a single spline on the tip of each jaw, while
higher-torque applications could use a wrench with a belt or chain,
similar to a bicycle sprocket wrench.  The two-splined adjustable
wrench would even work (less efficiently) on fasteners with an odd
number of splines, as long as the splines were more or less circular
in outline.

You couldn’t use quite a single spline size and spacing for every
possible fastener size from 0.1mm up to 1000mm.  But you could
probably use 1mm-diameter splines with 2mm spacing for fastener
diameters from 2.55mm (4 splines occupying half the circumference) up
to 63.7mm (100 splines occupying half the circumference).  The
rarely-used M3 bolts have 2.5mm heads; M4 have 3mm; M5 have 4mm; sizes
up to M22 are commonly used.  This suggests that a 4:3 ratio between
adjacent sizes is tolerable, which gives about 8 steps per decimal
order of magnitude, in between the electronics E6 and E12 series; the
8th root of 10 is about 1.3335.  This suggests maybe standardizing
fasteners with 4, 5, 7, 9, 13, 17, 22, 30, 40, 53, 71, and 95 splines.
95 splines of 1mm diameter gives a circumference of 190mm and thus a
diameter of 60.5mm.  If you wanted to make the next size up (80.7mm
diameter, 253mm circumference) have four splines again, they’d be
about 31mm diameter, and these 31mm splines would reach up to 6000mm
circumference and 1910mm diameter.  Similarly, if you wanted to make
the next size down from 8mm circumference (6mm circumference) have 95
splines, they’d be 31.6μm in diameter, which is impractically small.

For a roller chain, what’s constant on different sized sprockets is
the chordal distance between adjacent roller centers, not the
circumferential distance.  If we apply this principle, we could put
*two* splines on each jaw of our hypothetical adjustable wrench,
spaced apart by that nominal distance, and fit perfectly onto any
even-numbered spline fastener with that spline spacing, from 4 up to
96 or whatever.

More generally, I’m still not a fan of screw fasteners.
