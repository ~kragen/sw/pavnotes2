I really want a language for doing reproducible calculations,
simulations, and plots in my notes, so I can write an unlimited
quantity of notes that include computations, and keep those
computations reproducible into the future.  For this I need a stable
interface layer; as long as the implementation implements the stable
interface correctly, it will be able to reproduce the computations in
my notes.

I'm aiming for a weak form of reproducibility, which is that you
should be able to recalculate the results and get bitwise-identical
results to what I got, unless the calculation fails.  I'm not trying
to tackle the question of failure-freeness.

Some kind of stack-based virtual machine seems like it would be ideal
to reduce incompatibilities due to syntax changes from version to
version; that is, the layer to nail down as a stable interface should
be a stack-based virtual machine bytecode, not a textual language.

But I think I probably want it to have arrays at the stable interface
layer, despite the complications that introduces.

Broader notetaking context
--------------------------

This is part of a broader desire for a better notetaking environment.
I posted [on the orange
website](https://news.ycombinator.com/item?id=31915882):

> For my own notes, I've mostly been using Markdown, which doesn't
  have fenced code blocks, but I've been thinking I might upgrade to a
  different language like CommonMark or GitLab-Flavored Markdown that
  does support them. One of the main reasons is the possibility of
  embedding non-text things in my notes; for example, the other day I
  was messing around with exact pole-zero cancellation in DSP in
  <https://nbviewer.org/url/canonical.org/~kragen/sw/dev3/pole-zero-cancellation.ipynb>
  which involved a lot of TeX equations and data plots of
  algorithmically generated output. Jupyter notebooks can do those
  things, but they have a number of extremely serious drawbacks for
  this sort of thing, such as irreproducibility, aggressively
  backwards-incompatible file formats, lack of interactivity (despite
  ipywidgets), unacceptably large "source code" file size, and
  spurious merge conflicts in Git.

> An interesting approach to solving this problem is Mermaid
  (<https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts>)
  which lets you include a ```mermaid fenced code block in your
  Markdown source and replaces it with a rendered image of a
  boxes-and-arrows diagram in the HTML output. This is sort of
  backwards-compatible with regular GLFM because things that don't
  support Mermaid just show the diagram's source code. Mermaid's
  "flowchart" diagram type is vaguely similar to Graphviz, but I think
  Graphviz has better syntax and nicer output, so I'd rather use
  Graphviz for this; Mermaid also has Gantt charts, UML diagrams,
  sequence diagrams, and entity-relationship diagrams.

> GitLab also supports language-tagged fenced code blocks for "math"
  (KaTeX's subset of LaTeX), "plantuml" (disabled by default, does UML
  diagrams), and Kroki (also disabled by default, supports Graphviz,
  Ditaa (warning, domain stolen by SEOs) for flowcharts, Wavedrom for
  digital timing diagrams, Vega-Lite for data plotting with
  interactive exploration from JSON, Svgbob for a fairly comprehensive
  set of ASCII-art line drawings, Burntsushi's Erd for
  entity-relationship diagrams, bytefield-svg, Pikchr for PIC-like
  things, several different ASCII-to-UML things, and another dozen or
  so other similar things).

> Pikchr, from the Fossil/SQLite folks, is also designed for embedding
  in GFML and similar Markdown-derived languages, even without Kroki.

> But most of these are just data formats, rather than rich data
  models or executable code like I'd need for the DSP algorithms I
  mentioned above. The closest thing I've found for that is Yihui
  Xie's R Notebooks:
  <https://bookdown.org/yihui/rmarkdown/notebook.html> where you can
  put fenced R code blocks into your Markdown and display the output
  inline both in the RStudio IDE and in the final rendered
  document. This still has Jupyter's problem with reproducibility, but
  solves the problems with massive source files, file-format backwards
  compatibility, and spurious merge conflicts.

> However, R Notebooks require you to write your code in R, which
  isn't ideal for all purposes, and as far as I know the rendered
  results don't have a way to do Vega-style or ObservableHQ-style
  interactive exploration. Also, pretty often I find it easier to
  sketch a diagram with a pencil than to write code to produce it.

> And, for things like analog circuit schematics or 3-D models, while
  a compact textual representation is achievable, it often isn't the
  ideal user interface for editing. Mermaid Live, OpenSCAD, and dot
  -Tx11 filename.gv are three approaches where the text-to-picture
  flow is one-way, and while they're pretty great, you still spend a
  fair bit of time hunting through source code for the thing you want
  to edit and trying to diagnose syntax errors.

> So I'd like to have a note-taking system that supports Git and
  Commonmark, reproducibility so that I don't need to check in giant
  binary files representing render outputs, plug-in visualizers for
  different kinds of quasi-textual content, and plug-in editors for
  editing things like 3-D models, circuit schematics, and 2-D
  sketches. And I'd like to be able to do things like run a SPICE
  simulation of a circuit schematic, a FEM simulation of a 3-D model,
  or a numerical integration of a Modelica model.

> Is there anything like that out there? What are the alternative
  approaches to this kind of algorithmic exploration such as
  ObservableHQ?

> The DSP notebook I mentioned above also includes an input audio
  sample and several processed versions of it, which you can't hear in
  the online Jupyter notebook viewer but you can hear if you run it
  locally. The input audio sample would pretty much have to be an
  external file, same as a photo, but ideally the processed versions
  could be recomputed reproducibly rather than stored in Git and
  copied around all over the net.

Arrays?  Why?
-------------

I think providing built-in array operations is a huge help to
efficient implementations of, at least, many of the things I do
routinely.  Consider, from file `m-sequence-transforms.md`:

    s = [0, 0, 0, 1]
    n = len(s)
    while len(s) < 15:
        s.append(s[-3] ^ s[-4])
    S = array([concatenate((s[i:], s[:i])) for i in range(n)])
    L = S.T @ inv(S[..., :n]) % 2
    Lp = sorted(range(len(L)), key=lambda i: tuple(L[i]))

Or, from file `circulant-fully-distributed-representation.md`:

    > xc <- function(n) (1 + pbinom(n/2, n, .5) - pbinom(n/2-1, n, .5))/2
    > xc(c(64, 256, 1024, 2048, 4096))
    [1] 0.5496734 0.5249096 0.5124639 0.5088144 0.5062331

Or, from some notes in Jupyter on exact pole-zero cancelation to
stabilize unstable recursive filters:

    x = numpy.zeros(32)  # impulse
    x[0] = 1
    y1 = x.copy()  # half-assed way to get the first few samples right
    for n in range(5, len(x)):
        y1[n] = x[n] + y1[n - 5]
    matplotlib.pyplot.subplot(132); stem(y1)  # note infinite impulse response

Or, from some notes in Jupyter on sparse multiplication-free
narrowband bandpass filtering, which provides a more efficient way to
do almost the exact same thing I'm doing inefficiently above:

    def osc_numpy(x, n):
        h = (len(x) + n-1)//n
        y = zeros(n*h, dtype=x.dtype)
        y[:len(x)] = x
        y.shape = (h, n)
        flips = (-1)**arange(h)
        flips.shape = (h, 1)
        y *= flips
        y.cumsum(axis=0, out=y)
        y *= flips
        y.shape = (n*h,)
        return y[:len(x)]

Or, from my implementation of Schelling's 01970s racism model:

     // each generation, swap isolated racists to random positions
     for (int ii = 0; ii < generations; ii++) {
          for (int jj = 0; jj < width; jj++) {
               for (int kk = 0; kk < width; kk++) {
                    int same_neighbors = 0, 
                        me = people[jj][kk];

                    for (int nn = 0; nn < LEN(dd); nn++) {
                         if (me ==
                             people[(jj + dd[nn].x + width) % width]
                                   [(kk + dd[nn].y + width) % width]) {
                              same_neighbors++;
                         }
                    }
    ...

Or, from my implementation of Exercise 2.3.10 from EOPL, de Bruijn
numbering:

    (define (set-union xs ys)
      (cond ((null? xs) ys)
            ((member (car xs) ys) (set-union (cdr xs) ys))
            (#t (cons (car xs) (set-union (cdr xs) ys)))))

Or, from my Unicode maze generation program:

    h := trues(width+1, height)
    v := trues(width, height+1)

    // Shave the outside of the maze.
    // i.e. convert
    // ╋╋╋╋      ┏┳┳┓
    // ╋╋╋╋      ┣╋╋┫
    // ╋╋╋╋ into ┣╋╋┫
    // ╋╋╋╋      ┗┻┻┛
    for r := 0; r < height; r++ {
            h[r][0] = false
            h[r][width] = false
    }

    for c := 0; c < width; c++ {
            v[0][c] = false
            v[height][c] = false
    }

Or, from [my demonstration of the Burrows&ndash;Wheeler
transform](http://canonical.org/~kragen/sw/netbook-misc-devel/bwt.html):

    bwt.rotate_string = function(string, n) {
      var rv = []
      for (var ii = 0; ii < string.length; ii++) {
        rv.push(string[(ii + n) % string.length])
      }
      return rv.join('')
    }

As for efficiency, plain Python normally reaches only about 2% of the
speed of compiled (single-threaded) C, but Numpy routinely reaches
20%.  Vector Crays were able to deliver world-beating performance
despite anemic clock speeds, as I understand it essentially using the
CPU instruction set as a scripting language for the vector engine.
And vector operations map reasonably well, if imperfectly, onto GPGPU
capabilities as well.

I think that in a lot of cases a simple JIT compiler that pipelines a
few vector operations together ought to be several times faster than
Numpy.

Aside from efficiency, array operations often make it easier to say
what I want to say.

Units
-----

I also want units.  Consider this quote from file `pumping-heat.md`:

> Picking a marginally acceptable output target of 24° and 55% RH, we
> find that the moisture capacity of the air is 19 g/kg, so 40% RH
> would be 7.6 g/kg, 55% RH would be 10.45 g/kg, the delta is 2.85
> g/kg, and we can get a 7.1° temperature drop, achievable from a
> starting point of 31.1° or below: either 31.1° desiccator output
> temperature with no intercooler, outdoor temperature with a passive
> intercooler, or outdoor dewpoint with an evaporatively cooled
> intercooler.  A 31° dewpoint might be 37° with 70% RH, 43° with 50%
> RH, or 47° with 40% RH.  At 43° you'd be getting 12° of evaporative
> cooling from the outdoor evaporative cooler plus 7° of cooling from
> the desiccant circuit.

I did those calculations in part with a psychrometric chart and in
part with GNU Units, with calculations like these:

    You have: 40% 19 g/kg
    You want: g/kg
            * 7.6
            / 0.13157895
    You have: 55% 19 g/kg
    You want: g/kg
            * 10.45
            / 0.09569378
    You have: (55-40)% 19 g/kg
    You want: g/kg
            * 2.85
            / 0.35087719
    You have: (55-40)% 19 g/kg * 2500 kJ/kg / (1 J/g/K) # water's heat of vaporization, air's specific heat
    You want: 
            Definition: 7.125 K
    You have: 24+7.1
    You want: 
            Definition: 31.1

This is obviously a case where arrays would help since I'm doing the
same calculation on three different percentages.  Also, though, the
unit tracking is super useful for ensuring that I'm not accidentally
calculating kelvin-kilograms instead of kelvins or something.  It
would be great if this calculation was embedded in my notes in a
checkable, potentially reusable way.

Or, from file `solar-cells.md`:

> also a Panasonic amorphous silicon solar cell, is US$6.90 and claims
> to produce 5.1 mA at 3.3 V in 25 mm × 20 mm, thus US$1.38/cm² and
> 3.4% efficient.

That calculation was something like this:

    You have: 5.1 mA 3.3 V / 25 mm 20 mm / (1000 W/m^2)
    You want: %
            * 3.366
            / 0.29708853
    You have: 6.90$ / 25 mm 20 mm
    You want: $/cm**2
            * 1.38
            / 0.72463768

XXX

Array concept
-------------

Lush separates its approach to multidimensional arrays into two
user-visible pieces, whose names I unfortunately forget; the same
mechanisms are present in Numpy and implementations of APL but hidden
from public view.  One is a linear bounded chunk of pointer-free
memory, which I'll call a "body", and another is a specification of
how a multidimensional array is mapped onto it, which I'll call a
"view".

The view has a reference to a body, a base address, a data type of
the scalars in the array (such as byte, uint32, or float64), and a
list of dimensions.  Each dimension has a size and a stride; the
stride is the number of bytes by which elements successive along that
dimension are separated.

In a simple case, a view might contain a reference to a 1920-byte
body, a base address of 0, a data type of byte, a first dimension of
size 24 with a stride of 80, and a second dimension of size 80 with a
stride of 1, which we could write as (s, Byte, 0), [24, 80; 80, 1] if
the body is called s.  This view might grant convenient access to an
emulated terminal screen by row and column.

We can compute the transpose of this view, granting convenient access
by x and y, as (s, Byte, 0), [80, 1; 24, 80].  A 10×40 window onto the
screen might be (s, Byte, 82), [10, 80; 40, 1].  A view that puts the
lines, but not the columns, in reverse order might be (s, Byte, 1840),
[80, 1; 24, -80].  A view that includes only even-numbered screen
lines might be (s, Byte, 0), [24, 160; 80, 1]; one that includes only
odd-numbered screen lines, (s, Byte, 80), [24, 160; 80, 1].

Considering a 3×4 matrix of single-precision floating-point numbers,
we might have a view (m, F32, 0), [3, 4; 4, 12] if they're in
column-major order; if they were in row-major order, it would be (m,
F32, 0), [3, 9; 4, 4].  The transposed matrix is (m, F32, 0), [4, 12;
3, 4] and its diagonal is (m, F32, 0), [3, 15].

So, indirecting our array access through views in this way allows us
to avoid copying the data for many kinds of "slicing", including,
generically, transpositions of axes, and the v[start:stop:step]
operation along any axis or simultaneous combination of axes,
including axis reversal with negative steps.  In many cases, it also
allows us to verify array indexing at the point where a new view is
being constructed, rather than at the point where we are looping over
that view.

But there are odder kinds of views that can be constructed.

A view that treats all the bytes on the screen as a single long line
might be (s, Byte, 0), [1920, 1].  A view that divides the screen into
two columns of 40 characters might be (s, Byte, 0) [2, 40; 24, 80; 40,
1]: the two columns start 40 bytes apart, lines in a 24-line column
start 80 bytes apart, and bytes on the line within a column are 1 byte
apart.  The first row of the matrix repeated 10 times is (m, F32, 0),
[3, 4; 10, 0].  A sliding window of 5 items over all 12 numbers in the
matrix is (m, F32, 0), [5, 4; 8, 4].  Numpy has some functions for
this kind of thing in its core but relegates others to
`numpy.lib.index_tricks`, and some like the sliding-window example
were impossible in previous versions of Numpy.

If you have two consecutive copies of a signed 32-bit integer sequence
of length n in a body b, you can represent the circulant matrix of the
sequence as (b, S32, 0), [n, 4; n, 4].

So I think it's best to expose the view-derivation operation at the  XXX

Because views are homogeneous, iterating over a view, for example due
to a broadcast operation, a reduction, or a scan (generalized prefix
sum), does not require elementwise type dispatch for generic
arithmetic.

In general the construction of a view from another view can be
validated dynamically by checking that the byte range covered by the
new view is a subset (though not necessarily a proper subset) of the
byte range covered by the old view.

Regardless of whether this is done at view construction time or at the
start of iteration, the elimination of generic arithmetic and bounds
checking from the inner loop allows even a naive interpreted
implementation to achieve Numpy's 20% of single-threaded C performance
for many cases; and in most cases this allows elementary arithmetic
operations to be polymorphic without unduly burdening arithmetic
performance.  Optimizations for pipelining, blocking, and SIMD
intrinsics can improve that further.

Indexing by arrays
------------------

For things like permutations, wavetable synthesis, texture mapping,
image resampling, sparse matrix handling, and function approximation
by table lookup, it's essential to be able to generate an array of
indices, or sometimes even two or more parallel arrays, and then index
another view using them.  The view-construction stuff above doesn't
allow us to safely elide per-iteration bounds checks here, though
there might be cleverer tricks that do.

Determinism
-----------

I want to not check the results of things like this in to source
control, but without losing reproducibility:

    assocs = random.random_sample((65, 16384)) > 0.5
    imshow(assocs[:256, :256])

This requires nailing down all the sources of nondeterminism: in this
case, obviously, the random seed and random number generator state
need to be the same if I recalculate that result.  Also, though, I
need to nail down the version of `imshow` I'm using so it doesn't
change its palette, and the semantics of the underlying code execution
engine.  Different ways of calculating `a + b*c`, for example, with or
without fused multiply-add (FMA), can result in differently rounded
results.  Historically, integer division has also had different
implementations with different rounding, and of course things like
transcendentals commonly produce results that vary from software
version to software version and, worse, from hardware version to
hardware version.

Mutability
----------

Mutability is a potential source of nondeterminism as well.  If the
results of an array-producing operation (such as a broadcast, a scan,
or just a count) are to be written into an array that is also being
read, it would be possible for optimizations to change the results of
the operation by delaying a write until after a read whose values it
might have affected.

This comes up in what mathematicians call "recursion".  For example,
consider this implementation of the 4-bit maximal LFSR for the *GF(2)*
polynomial *x*<sup>4</sup> + *x*³ + 1, quoted above:

    s = [0, 0, 0, 1]
    while len(s) < 15:
        s.append(s[-3] ^ s[-4])

This generates this M-sequence:

    [0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1]

We could instead try to replace the while-loop by taking the XOR of
two slices and writing it into a third slice, reasoning that if the
loop is executed from left to right in the array then each value will
be written before it is read:

    s = 1 * (arange(15) == 3)
    s[4:] = s[1:-3] ^ s[:-4]

But in the Numpy I have here, I instead get this result, suggesting
that either the iteration was done backwards or that some zeroes were
loaded from the array before being overwritten with ones:

    array([0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0])

Repeating the slice XOR operation twice more does produce the desired
result:

    array([0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1])

I think there is nothing in Numpy's documentation that tells you this
*won't* work.  Numpy could have produced the desired results on the
first run.  I want to avoid this kind of nondeterminism in the
specification of the virtual machine because it could give rise to
irreproducibility.

Mutability is valuable, though, for a variety of reasons:

- In cases like the LFSR example above, it's easy to see how to do
  them efficiently on a computer with mutability.  Efficient
  realizations without mutability are not impossible but are subtle
  and arcane; the `osc_numpy` function given earlier is an example,
  but I can't figure out how to adapt it to this case.  Other possible
  facilities that would work for this LFSR, but not in other cases,
  are a Haskell-style unfold using a one-integer state and a
  general-purpose linear recursion facility like those used for IIR
  filters.

- In cases like polygon rendering into a framebuffer, where the pixel
  indices to write are computed at runtime, mutability permits a
  simple and direct solution.

- In a lot of cases Numpy gets twice as fast when you avoid allocating
  extra intermediate results, so mutability can be a useful
  optimization.  But this is a weaker rationale because it's probably
  possible to do the equivalent transparently in many cases, for
  example for temporary results that don't escape an expression.

So, while I'm tempted to eliminate mutability in order to guarantee
determinism, I'm not sure I can do without it.

Fundamental operations
----------------------

What operations should be provided by the stable interface layer?

To help it be stable, I want to make it much narrower than
conventional machine languages.

If arithmetic inner loops will be generally provided by array
operations, then perhaps we don't need separate primitive arithmetic
instructions; we can regard arithmetic as invoking methods on objects,
which are free to handle it in varying ways.  But for operations I do
frequently, it's probably important to gateway them through to the
underlying hardware rather than emulating them in software.  You could
implement multiplication with repeated addition in an interpreted
loop, for example, but it's going to be a lot slower than 

Due to their origin as numerical calculating machines, the traditional
list of fundamental operations for computers was +, -, ×, ÷, &lt;, >,
==, and sometimes square root and/or exponentiation.

8-bit microprocessors commonly omitted most of these, though sometimes
they had BCD versions as well as binary, and usually had bitwise, bit
rotate, and bit shift operators.  Steve Wozniak's SWEET16 16-bit
virtual machine for the Apple ][ had SUB subtract, ADD add, DCR
decrement, INR increment, and CPR compare (with eight conditional
branch instructions depending on the result).  The 8086's built-in
multiply and divide instructions were touted as a selling point.

C adds %, &, |, &lt;&lt;, >>, ^, ~, and shortcut Boolean operators,
and Golang adds &^.  GNU bc adds natural logarithm, exponential, sin,
cos, arctangent, and the Bessel J functions.

APL has an alarmingly large number of "fundamental" operations, some
of which are especially valuable in reductions, such as pairwise min
and max.  GLSL includes these two as well as a bunch of vector stuff,
smoothstep, reciprocal square root, and noise.

ARB assembly, for OpenGL shaders, includes dot products, base-2
exponentials and logarithms, floor, min, max, powers, reciprocal
square root.

Numpy has a long list of two-argument "ufunc"s, or universal
functions.  These are the atomic operations for which it makes sense
to call .reduce() or .accumulate() (Numpy's name for scan or
generalized prefix sum), although only some of them are associative.
Many of them correspond to infix operators in Python.  Several can be
computed from others.

I think the list of associative ones, for which reduce and scan make
the most sense, is:

- arithmetic: add, matmul, multiply
- hypot, which is for the L<sup>2</sup> norm what add is for the
  L<sup>1</sup> norm; for some reason the corresponding operators for
  other norms like L<sup>3</sup> are not included
- maximum, minimum, fmax, fmin (the f- versions ignore NaNs rather
  than making them contagious)
- gcd and lcm, which are the meet and join of the lattice of
  divisibility in the same way as maximum and minimum for the total
  ordering of reals
- boolean: bitwise\_and, bitwise\ or, bitwise\_xor, logical\_and,
  logical\_or, logical\_xor; the ands and ors are also meets and joins
  of a lattice, and the ands and xors are also pointwise addition and
  multiplication on functions from bit indices to elements of $GF(2)$.
- copysign, which is sort of associative by accident and doesn't
  really fit in otherwise

The other binary ufuncs, which technically support .reduce and
.accumulate but where I can't figure out how that could be useful
since they aren't associative, are:

- arithmetic: subtract, divide, divmod, fmod, floor\_divide,
  true\_divide, mod, remainder, power, float\_power
- comparisons: equal greater less\_equal greater\_equal less
  not\_equal
- boolean: right\_shift left_shift
- arctan2 
- weird floating-point things: nextafter, which gives you the next
  floating-point value after a number in a particular direction;
  ldexp, load the base-2 exponent; logaddexp, log(exp(x1) + exp(x2));
  logaddexp2, log2(2<sup>x1</sup> + 2<sup>x2</sup>).
- heaviside, the Heaviside function (which takes a second parameter
  only to specify what value to use at the discontinuity)

The large number of varieties of division is somewhat alarming in the
sense that it suggests conflicting requirements for division for
different purposes.

It also has the following one-argument "ufunc"s.  As far as I know
they only differ from the corresponding functions in the Python `math`
module by virtue of implicitly broadcasting over arrays and handling a
wider variety of numeric types: abs, absolute, arccos, arccosh,
arcsin, arcsinh, arctan, arctanh, bitwise\_not, cbrt, ceil, conj,
conjugate, cos, cosh, deg2rad, degrees, exp, exp2, expm1, fabs, floor,
frexp, invert, isfinite, isinf, isnan, isnat, log, log10, log1p, log2,
logical\_not, modf, negative, positive, rad2deg, radians, reciprocal,
rint, sign, signbit, sin, sinh, spacing, sqrt, square, tan, tanh,
trunc.
