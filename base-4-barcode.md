Some notes on how to design handwritten barcodes.

IMb
---

I was looking at the Wikipedia page for the USPS’s new [Intelligent
Mail barcode][0], which uses four symbols rather than the two used by
their old POSTNET system.  POSTNET uses a 2-of-5 encoding for decimal
digits, while IMb uses a more complicated system.

2-of-5 codes have a “constant-weight” property (2 of the bits in each
group of 5 are set) which simplifies the scanning process; you always
have some threshold that’s set to distinguish black from white, and
there’s always some blurring and ink spread which changes the exact
sizes of printed features.  Using a constant-weight code makes it
possible to automatically adjust the thresholds to the right level.

The four symbols used by the Intelligent Mail barcode IMb are roughly
🬇🬉🬦▐, similar to Braille ⠐⠘⠰⠸, or, in more-compatible Unicode art
or ASCII art:

     ▐ ▐     | |
    ▐▐▐▐    ||||
      ▐▐      ||

[The specification][1] explains:

> The IMb is a height-modulated barcode that encodes up to 31 digits
> of mailpiece data into 65 vertical bars using a 4-state symbology.
> This symbology uses four different states of “bars” which allows
> more information to be encoded in a single barcode.

[0]: https://en.wikipedia.org/wiki/Intelligent_Mail_barcode
[1]: https://postalpro.usps.com/storages/2017-08/2190_USPSB3200IntelligentMailBarcode4State_0.pdf

So there’s a “central tracking region” that all the bars have, which
allows you to follow the line of bars accurately and keep track of how
many there are, and then two bits of useful data above and below it,
which it calls the “ascender” and “descender”; the three regions are
roughly the same size, which is roughly the spacing from the center of
one bar to the next, with spaces roughly the same size as the bars,
with specified tolerances I’d rather not lay out here.  The desired
data is encoded into these bits in a rather complicated way involving
5-of-13 encoding and 2-of-13 encoding; appendix E contains about 20
pages of relevant tables.

A barcode for handwriting
-------------------------

I wonder if something like this might be the easiest and fastest way
to hand-write barcodes that can be easily and reliably machine-read.
Each stroke of the pencil gives you close to two bits, and Baudot and
Baudot–Murray code used only five bits per character, so you should
need only 2½ strokes per character, plus some error correction
overhead.

To avoid difficult mental gymnastics, you could assign a 3-stroke or
4-stroke sequence per character, which also leaves a little space for
redundancy.  For human-readability, it would be useful to leave a
space between characters.

### A 6×6 3-stroke approach ###

If we consider the 3 ascender bits as one digit and the 3 descender
bits as another, it may be desirable to exclude the digits 111 and
000, thus ensuring at least 1 and at most 2 ascenders per character,
and respectively for descenders.  This leaves 6 valid digits in each
position, enough for 36 characters, which is adequate for writing
English or Spanish with some shift characters.  It’s not quite
constant-weight, but it’s probably close enough.

In addition to the 27 letters of the Spanish alphabet, which are also
enough for English, and space, you have 8 codepoints left over for
punctuation, shifts, or other control characters.

In ITA2 (Baudot–Murray code) the non-letter codepoints in the LETS
shift state were LETS (which was also the all-ones delete character),
FIGS, space, carriage return, line feed, and null (tape feed, the
all-zeroes character), because they only had space for six characters.
For my purposes of jotting barcodes down on paper I don’t need null,
delete, or separate carriage control and line feed, but space and at
least two shift characters like LETS and FIGS would be useful.  I’d
like to include letter case, which suggests at least a CAPS shift;
usually it should reset after a single character, but repeating it
twice could be used to continue to the end of the word.  For Spanish
you probably want an acute-accent shift or dead key which similarly
gives the next character an acute accent, at least if it’s a vowel.

#### Draft character repertoire for the LETS state ####

Here `_` represents a not-yet-assigned character:

                 lets
    001  010  011  100  101  110 ← ascenders
    SPC  a    b    c    d    e    001
    f    g    h    i    j    k    010
    l    m    n    ñ    o    p    011  descenders
    q    r    s    t    u    v    100
    w    x    y    z    CAPS FIGS 101
    LETS .    ,    ´    NL   _    110

Especially on non-letter positions, CAPS and acute-accent shift states
can provide additional characters that are often used in isolation;
this gives us four shift states of 33 characters (not counting the
shift codes) totaling 132 codepoints, 27+27+6+6+10 = 76 of which are
needed for lowercase and uppercase letters, accented vowels, and
digits.  Adding the four code points for space, ‘.’, ‘,’, and newline,
we have 80 code points assigned and 52 still unassigned.  In ASCII
there are 33 non-alphanumeric printable characters.

So “Hello, world.” is encoded, descenders first, as 101101 010011
001110 011001 011001 110011 001001 101001 011101 100010 011001 001101
110010, which looks like this:

    ▐ ▐  ▐▐ ▐▐    ▐   ▐  ▐▐   ▐   ▐ ▐ ▐  ▐    ▐ ▐ ▐  ▐
    ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐ ▐▐▐
    ▐ ▐  ▐    ▐  ▐▐  ▐▐ ▐▐    ▐ ▐ ▐  ▐▐ ▐    ▐▐   ▐ ▐▐

Copying this down onto a sheet of paper took me 91 seconds, which is
uninspiring for 13 codepoints (7 seconds per codepoint), but perhaps
practice would speed this up.  Also I left out the ‘l’.  Also I notice
I left out the “o” from the above, so what I actually wrote down in
barcode was “Hell, word.”  Unless there are more errors I haven’t
spotted.

Copying down the ‘e’ on the paper twenty times took me 49 seconds,
which is still not great but is only 2.5 seconds per code point and
per character.  This would work out to almost five words per minute,
less than a third of my normal handwriting speed and probably too slow
to be acceptable.  If we consider each code point to be 6 bits, that’s
about 2.4 bits per second; if we more accurately consider it to be
5.17 bits, it’s 2.1 bits per second.  I think probably this
demonstrates that this overall approach to hand-writable barcode
design is too slow.  Something better aligned with human motor
abilities is needed.

LETS is assigned a codepoint even in LETS state so that it can reset
the shift state to a known state.  FIGS FIGS could reasonably activate
an additional shift state.

#### Character frequencies in literature ####

One line of Python suggests some priorities.

    >>> collections.Counter(c for line in open('bible-pg10.txt')
                            for c in line).most_common(256)
    [(' ', 751112), ('e', 409522), ('t', 309981), ('h', 279470), ('a',
    257813), ('o', 234289), ('n', 223164), ('s', 185123), ('i', 180659),
    ('r', 162759), ('d', 149312), ('l', 120716), ('\n', 100222), ('u',
    83140), ('f', 81157), ('m', 76884), (',', 70717), ('w', 63079), ('y',
    58007), ('c', 53371), ('g', 49095), ('b', 44161), (':', 43827), ('p',
    41377), ('v', 30258), ('1', 27091), ('.', 26420), ('k', 21745), ('2',
    18978), ('A', 17914), ('I', 13300), ('3', 12259), (';', 10139), ('L',
    9222), ('4', 9150), ('O', 8896), ('D', 8782), ('T', 7763), ('R',
    7568), ('5', 7053), ('6', 6472), ('J', 6374), ('G', 6206), ('7',
    5917), ('8', 5779), ('9', 5643), ('0', 5320), ('S', 4905), ('B',
    4714), ('?', 3297), ('H', 3208), ('M', 3056), ('E', 2710), ('j',
    2515), ('W', 2408), ('F', 2386), ('z', 2068), ("'", 2024), ('N',
    1891), ('P', 1877), ('C', 1696), ('x', 1476), ('q', 958), ('Z', 904),
    ('Y', 569), ('K', 547), ('U', 333), ('!', 314), ('(', 241), (')',
    241), ('V', 107), ('-', 98), ('*', 31), ('/', 24), ('"', 22), ('Q',
    6), ('[', 2), (']', 2), ('X', 2), ('@', 2), ('$', 2), ('\ufeff', 1),
    ('#', 1), ('%', 1)]

The whole file has 4.35 million characters:

    >>> sum(len(line) for line in bible)
    4351845

Note that ‘e’ is 150 times as common as ‘E’, and even ‘a’ is 14 times
as common as ‘A’.  So it’s okay for capital letters to take an extra
codepoint to encode.

Mixed in with the letters we have newline, ‘,’, ‘:’, ‘.’, ‘;’, the
numbers, and finally the apostrophes.  But conceivably some of these
occur most commonly in contexts of numbers or punctuation or
something.  A little more fiddling around with Python suggests that
indeed only ‘,’ and ‘.’ belong there:

    >>> ''.join(c for c, _ in
                collections.Counter(c1 for line in open('bible-pg10.txt')
                                       for c0, c1 in itertools.pairwise(line)
                                       if c0.isalpha()
                                   ).most_common(256))
    ' ehnoatridsl,ufmy\ngcv.pwkb:;ODR?z\'xjq!E-INSTHA)BULGCFMYKV"PW/JX@ZQ'

Possibly the King James Bible isn’t the best possible text to use for
this, though.  Among [Project Gutenberg’s most popular books][2], the
top item in the 20th century is etext #2641, [_A Room with a View_][3]:

    >>> room = list(open('../library/forster-room-with-a-view-pg2641.txt'))
    >>> sum(len(line) for line in room)
    394371
    >>> ''.join(c for c, _ in
                collections.Counter(c1 for line in room
                                       for c0, c1 in itertools.pairwise(line)
                                       if c0.isalpha()).most_common(256))
    ' eontraihsdluy.c,gm\nfpvwkb’—?!;x-:zjIqETR_™”NAOSBHCULDG)VMFéYXP/JKWç]œæöäQ'

[2]: https://www.gutenberg.org/ebooks/search/?sort_order=downloads
[3]: https://www.gutenberg.org/ebooks/2641

How about characters that *precede* letters?

    >>> ''.join(c for c, _ in
                collections.Counter(c0 for line in room
                                       for c0, c1 in itertools.pairwise(line)
                                       if c1.isalpha()).most_common(256))
    ' eaotihnrslucwmdpbgfvy“kM’TBHLSIC—AWEGFOx-NPjqYDzRV‘U._J(XQé/Kç[\ufeffœæöä'

And characters that follow spaces?

    >>> ''.join(c for c, _ in
                collections.Counter(c1 for line in room
                                    for c0, c1 in itertools.pairwise(line)
                                    if c0.isspace()).most_common(256))
    'taswhoibmcdfpnlIrygeMuBL“HSTCvkAFEGW POjYqNVDR‘_UX(1J\n2Q34K•-5*’698[#$'

Maybe it would be a good idea to look for something in Spanish.  The
most popular Spanish book on PG is Don Quijote, with modernized
spelling but its original vocabulary.  This yields

    >>> sum(len(line) for line in quijote)
    2130021
    >>> ''.join(c for c, _ in
                collections.Counter(c1 for line in quijote
                                       for c0, c1 in itertools.pairwise(line)
                                       if c0.isalpha()).most_common(256))
    ' eaonsriultd,cm\nbgípjóh.ávzéq;ñyf:—ú?!IxEAO\'kNR-LXüTC"w)SVU™BDHGMJPFY”É»/ÓÍ’KïQZ]ÁùÑàW'

You can see that the spelling is modernized because ç does not occur.
The letterlike punctuation is basically just ‘.’ and ‘,’.

    >>> ''.join(c for c, _ in
                collections.Counter(c1 for line in quijote
                                       for c0, c1 in itertools.pairwise(line)
                                       if not c1.isalpha()).most_common(256))
    ' ,\n.;—:?¿!\'¡"-)(»™«10326“”9*4587/’•]$[#%‘'

So the other punctuation used is some of the ASCII punctuation
`([()],.:;?!'"-*$#%)`, plus `–¿¡«»™“”‘’•`.  A few of the ASCII
punctuation characters are also missing:

    >>> ''.join(c for qc in [set(c for line in quijote for c in line)]
                  for c in (chr(i) for i in range(32, 127)) if c not in qc)
    '&+<=>@\\^_`{|}~'

“Room” and the Bible omit respectively ``"&\'+<=>@Z\\^`{|}~`` and
``&+<=>\^_`{|}~``; it’s interesting that the ampersand, formerly such
an important character that it was often considered the 27th letter of
the English alphabet, has been so thoroughly marginalized.  The
backtick, caret, underscore, and tilde are really accent or overstrike
characters, only promoted to real characters by ASCII.  (The
apostrophe, quote, and comma were also used in this way.)  `+<=>` are
math characters, included in ASCII so you could write formulas (though
unfortunately `×` and `÷` were omitted).  It’s sort of surprising that
`*-%` occur.  ` \ ` was invented for ASCII by Bob Bemer so that you
could write the logical AND and OR operators in programming languages
as ` /\ ` and `\/`.  `[\]^{|}~` were considered inessential carefully
placed at the end of the alphabet so they could be replaced by other
letters in 7-bit European variants of ASCII for languages like
Finnish.  `@` was an ideograph used for commercial invoices and
commonly included on typewriters for that purpose.

Perhaps unsurprisingly, these characters not normally used in real
natural-language text, five of them brand-new computer innovations,
are among the favorite characters of programming and markup languages.

As a current sample, I tried the [English Wikipedia article
“Mathematics”][8], which is almost 170K in its source form.

    >>> math = list(open('../library/Mathematics.wikitext'))
    >>> sum(len(line) for line in math)
    166868
    >>> ''.join(c for c, _ in
                collections.Counter(c0 for line in math
                                       for c0 in line).most_common(64))
    " etairsonlchmud=|pf2b0y/1g.-[]wv\n,93{}74M8kA65S<>C:TP'jxEBFNDHGI"

This shows a lot of =| because those are used to pass parameters to
MediaWiki templates, which are invoked with {{}}; a lot of / because
URLs are full of /; and a lot of `[]` because WikiLinks are `[[]]`.  I
suspect the prominence of - is due to its use in MediaWiki template
parameter names like archive-url.

English Wikipedia uses a wider repertoire of alphabets than the other
texts we looked at above, due to citing authors with names like Paul
Erdős and Øystein Ore, chapters with names like “Ό Θεὸς Άριθμητίζει”,
and books with names like _Al-Kitāb al-muḫtaṣar fī ḥisāb al-ğabr
wa-l-muqābala_:

    >>> ''.join(c for c, _ in
                collections.Counter(c0 for line in math
                                       for c0 in line).most_common(1024)
                if c.isalpha())
    'etairsonlchmudpfbygwvMkASCTPjxEBFNDHGIRJLzOWqKUVQYZéXμαóητäθιçēáκöèüāīàőάςίØεζêήόḗὴέχνοὰòæσḫṣḥğṬūΣΌΘὸΆρΩôî'

Its non-alphabetic characters are almost just ASCII, though:

    >>> ''.join(c for c, _ in
               collections.Counter(c0 for line in math
                                      for c0 in line).most_common(1024)
               if not c.isalpha())
    ' =|20/1.-[]\n,93{}74865<>:\'"()–_*&;%~?#!+\\—³°̄×^«»’'

I also did the computations for characters that follow letters and
follow nonletters, but didn’t really turn up anything interesting.

[8]: https://en.wikipedia.org/wiki/Mathematics

I tried doing the same computation on an earlier version of the
present document in Markdown, but I’m not sure the result said
anything interesting:

    >>> ''.join(c for c, _ in
                collections.Counter(c0 for line in base4
                                       for c0 in line).most_common(73))
    " etoaisnrc'lh,d\npufm)(1b.gw20y_85->k▐I637vS49/`’Tj[]AC:x#EPLMq\\‘B“”;FGWzU"
    >>> ''.join(c for c, _ in
                collections.Counter(c1 for line in base4
                                       for c0, c1 in itertools.pairwise(line)
                                       if c0.isalpha() and c1.isprintable()
                                   ).most_common(73))
    " eotnarsihldcpumgfy.,('bk_wv-SI8’)x1/C:0TE]2GPMjqRzAL”;BFXON\\DW〉HUY4VQéæ?"
    >>> ''.join(c for c, _ in
                collections.Counter(c1 for line in base4
                                    for c0, c1 in itertools.pairwise(line)
                                    if not c0.isalpha() and c1.isprintable()
                                   ).most_common(73))
    " ',cita)12s0of(lw5bp▐>637.8rem4nu9d_`-hj[#I]/AT‘:CSkL\\P“gq’|{yMBE};=vU+”—"

#### Other character sets ####

The additional punctuation characters in Latin-1, some of them
regrettable choices, are:

    >>> ''.join(c for c in (chr(i) for i in range(128, 256))
                if c.isprintable() and not c.isalpha())
    '¡¢£¤¥¦§¨©«¬®¯°±²³´¶·¸¹»¼½¾¿×÷'

In ITA2, the punctuation characters (all of them in the FIGS shift
state) are `-'$!&#()"/:;?,.` and sometimes `£+=`, and there’s also a
character to ring the teletype bell, and sometimes a WRU control
character to request called station identification.  These are in
reasonable agreement with the characters used in the books.

[Python’s list of encodings][9] unfortunately doesn’t list TeX and the
PostScript StandardEncoding, which would be especially helpful for
this purpose, but it does have `palmos`, `mac_roman`, and `hp_roman8`,
the character set of the LaserJet IIP:

    >>> {name for _, name, ispkg in pkgutil.iter_modules(encodings.__path__)
              if not ispkg} - {'aliases'}
    {'cp037', 'cp1255', 'cp1140', 'quopri_codec', 'iso2022_jp_2',
    'zlib_codec', 'big5hkscs', 'cp862', 'euc_jisx0213', 'mac_latin2',
    'iso8859_3', 'utf_32', 'tis_620', 'cp950', 'palmos', 'mac_iceland',
    'iso8859_1', 'iso8859_6', 'gb18030', 'raw_unicode_escape', 'euc_jp',
    'cp500', 'iso2022_jp', 'gb2312', 'iso2022_jp_2004', 'mac_cyrillic',
    'bz2_codec', 'cp1253', 'cp1257', 'iso8859_10', 'unicode_escape',
    'cp863', 'cp869', 'shift_jis_2004', 'mac_roman', 'iso8859_8', 'cp737',
    'idna', 'cp855', 'ascii', 'hex_codec', 'cp1125', 'iso8859_9',
    'rot_13', 'cp1252', 'mbcs', 'cp856', 'cp1258', 'iso2022_jp_1',
    'iso2022_kr', 'iso8859_7', 'uu_codec', 'cp932', 'iso8859_14',
    'mac_turkish', 'cp775', 'koi8_u', 'cp860', 'utf_32_le', 'cp850',
    'shift_jis', 'cp424', 'euc_kr', 'cp874', 'latin_1', 'cp861',
    'iso8859_15', 'utf_32_be', 'ptcp154', 'cp949', 'utf_7', 'iso8859_13',
    'gbk', 'cp1251', 'cp437', 'hp_roman8', 'iso8859_4', 'iso8859_5',
    'punycode', 'johab', 'utf_16', 'mac_greek', 'cp1256', 'cp865',
    'iso2022_jp_ext', 'utf_16_le', 'big5', 'cp875', 'shift_jisx0213',
    'iso8859_11', 'base64_codec', 'euc_jis_2004', 'cp866', 'koi8_t',
    'oem', 'cp273', 'cp852', 'iso8859_16', 'utf_8_sig', 'cp1254', 'cp864',
    'cp858', 'hz', 'mac_romanian', 'iso2022_jp_3', 'cp720', 'koi8_r',
    'cp1006', 'mac_farsi', 'utf_16_be', 'iso8859_2', 'cp1026', 'kz1048',
    'undefined', 'cp857', 'cp1250', 'charmap', 'utf_8', 'mac_croatian',
    'mac_arabic'}

[9]: https://stackoverflow.com/a/1728414

The punctuation in the HP character set is mostly the same repertoire
as Latin-1 above:

    >>> ''.join(c for c in (bytes([i]).decode('hp_roman8') for i in range(128, 255))
                  if c.isprintable() and not c.isalpha())
    '´¨˜₤¯°¡¿¤£¥§¢·¶¾—¼½«■»±'
    >>> ''.join(c for c in (bytes([i]).decode('latin-1') for i in range(128, 256))
                  if c.isprintable() and not c.isalpha())
    '¡¢£¤¥¦§¨©«¬®¯°±²³´¶·¸¹»¼½¾¿×÷'

The additions in the LaserJet are {'■', '—', '˜', '₤'}.  Gucharmap
says that last one is an alternate symbol for the Italian Lira, which
is preferably represented by `£`, also in the list.  I seem to recall
that Windows-1252 adds the em dash `—` as well as smart quotes, which
are pretty egregious omissions from Latin-1 (they’re among the most
common characters in the PG ebooks above).

The MacRoman and PalmOS repertoires include a lot of fairly frivolous
characters, but also include the em dash and smart quotes, and a
number of other characters in Latin-1:

    >>> ''.join(c for c in (bytes([i]).decode('mac_roman')
                            for i in range(128, 256))
                  if c.isprintable() and not c.isalpha())
    '†°¢£§•¶®©™´¨≠∞±≤≥¥∂∑∏∫¿¡¬√≈∆«»…–—“”‘’÷◊⁄€‹›‡·‚„‰˜¯˘˙˚¸˝˛'
    >>> ''.join(c for c in (bytes([i]).decode('palmos')
                            for i in range(128, 256))
                  if c.isprintable() and not c.isalpha())
    '€‚„…†‡‰‹♦♣♥♠‘’“”•–—˜™¡¢£¤¥¦§¨©«¬®¯°±²³´¶·¸¹»¼½¾¿×÷'

The IBM PC used “code page 437”, which seems to have been influenced
more by the TRS-80 and VIC-20 than by the literary tradition, though
it also has reasonable coverage of Western European languages that
aren’t Greek, even including the Spanish peseta symbol:

    >>> ''.join(c for c in (bytes([i]).decode('cp437')
                            for i in range(128, 256))
                  if c.isprintable() and not c.isalpha())
    '¢£¥₧¿⌐¬½¼¡«»░▒▓│┤╡╢╖╕╣║╗╝╜╛┐└┴┬├─┼╞╟╚╔╩╦╠═╬╧╨╤╥╙╘╒╓╫╪┘┌█▄▌▐▀∞∩≡±≥≤⌠⌡÷≈°∙·√²■'
    >>> ''.join(c for c in (bytes([i]).decode('cp437')
                            for i in range(128, 256))
                  if c.isprintable() and c.isalpha())
    'ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜƒáíóúñÑªºαßΓπΣσµτΦΘΩδφεⁿ'

Windows-1252 adds to Latin-1 smart quotes, ellipsis, en and em dashes,
lowered smart quotes (used in German), daggers, trademark, bullet, per
mille, trademark, a raised tilde, and a trademark sign.

    >>> lc = {c for c in (bytes([i]).decode('latin-1')
                          for i in range(128, 256))
                if c.isprintable() and not c.isalpha()}
    >>> wc = {c for c in (bytes([i]).decode('windows-1252', 'replace')
                          for i in range(128, 256))
                if c.isprintable() and not c.isalpha()}
    >>> ''.join(wc - lc)
    '‚›‰�’‘“‹€˜„‡—™•”†…–'

[PostScript StandardEncoding][4] uses the ASCII apostrophe and
backtick character as smart quotes, as God and TeX intended.
According to Wikipedia, its non-ASCII printable characters are
`¡¢£∕¥ƒ§¤'“«‹›ﬁﬂ–†‡·¶•‚„”»…‰¿ˋ´ˆ˜ˉ˘˙¨˚¸˝˛ˇ—ÆªŁØŒºæıłøœß`, with a full
complement of overprintable accent characters, the en and em dashes,
smart quotes, and a couple of ligatures (though, oddly, none for ffi,
ff, and ffl).  Checking Appendix E.6 of the [PostScript Language
Reference Manual][5], it agrees with Wikipedia.

[4]: https://en.wikipedia.org/wiki/PostScript_Standard_Encoding
[5]: https://www.adobe.com/jp/print/postscript/pdfs/PLRM.pdf

In [Appendix C of the TeXBook][6], which begins on p. 362 (377/490),
the TeX 7-bit encoding is given.  In place of ASCII’s control
characters, it places the glyphs `·↓αβ∧¬∈πλγδ↑±⊕∞∂⊂⊃∩∪∀∃⊗⮀←→≠◊≤≥≡∨∫`,
which probably owes more to the [SAIL keyboard][7] and the character
encoding used on the terminals Knuth was using than his beliefs about
which characters were the most important to include.  This is very
nearly exactly the set of non-ASCII characters printed on that
keyboard, though there are some differences.  Below the table Knuth
explains:

> The extended code shown above was developed at MIT; it is similar
> to, but slightly better than, the code implemented at Stanford.
> Seven of the codes are conventionally assigned to the standard ASCII
> control functions NUL (〈null〉), HT (〈tab〉), LF (〈linefeed〉),
> FF (〈formfeed〉), CR (〈return〉), ESC (〈escape〉), and DEL
> (〈delete〉), and they appear in the standard ASCII positions; hence
> the corresponding seven characters ⋅ γ δ ± ⊕ ◊ ∫ do not actually
> appear on the keyboard. These seven “hidden” characters show up only
> on certain output devices.

[6]: https://visualmatheditor.equatheque.net/doc/texbook.pdf
[7]: http://www.xahlee.info/kbd/sail_keyboard.html

In 01990 in Cork, Ireland, [a new 8-bit TeX encoding was defined for
fonts][10], known as the Cork encoding.  TeX also uses 7- and 8-bit
encodings dubbed [OML][12] (7-bit, math variables), [OMS][13] (7-bit,
math symbols), [OT1][14] (7-bit, the culprit of the notorious “¿From”
showing up in TeX output), and [LY1][11] (8-bit).  The first three of
these stuff almost everything you need for mathematical and prose
typography glyphs into three 7-bit code pages (I think 192 glyphs),
though nowadays TeX most commonly uses Unicode.  OT1 is probably the
single best source for what characters are most important
typographically for conventional text, assuming you have overstrike.

[10]: https://en.wikipedia.org/wiki/Cork_encoding
[11]: https://en.wikipedia.org/wiki/LY1_encoding
[12]: https://en.wikipedia.org/wiki/OML_encoding
[13]: https://en.wikipedia.org/wiki/OMS_encoding
[14]: https://en.wikipedia.org/wiki/OT1_encoding

### A constant-weight 4-stroke approach ###

It occurred to me that in groups of four strokes, you could use a real
constant-weight code, using 8-bit words of weight 4, of which there
are (8×7×6×5)/(4×3×2×1) = 2×7×5 = 70:

    >>> [''.join(str(d) for d in m)
                 for m in [tuple((n >> (7-i)) & 1
                           for i in range(8))
                          for n in range(256)] if sum(m) == 4]
    ['00001111', '00010111', '00011011', '00011101', '00011110',
     '00100111', '00101011', '00101101', '00101110', '00110011',
     '00110101', '00110110', '00111001', '00111010', '00111100',
     '01000111', '01001011', '01001101', '01001110', '01010011',
     '01010101', '01010110', '01011001', '01011010', '01011100',
     '01100011', '01100101', '01100110', '01101001', '01101010',
     '01101100', '01110001', '01110010', '01110100', '01111000',
     '10000111', '10001011', '10001101', '10001110', '10010011',
     '10010101', '10010110', '10011001', '10011010', '10011100',
     '10100011', '10100101', '10100110', '10101001', '10101010',
     '10101100', '10110001', '10110010', '10110100', '10111000',
     '11000011', '11000101', '11000110', '11001001', '11001010',
     '11001100', '11010001', '11010010', '11010100', '11011000',
     '11100001', '11100010', '11100100', '11101000', '11110000']

Of these you might want to eliminate 11110000 and 00001111 so that
you’d never have a character missing all descenders or ascenders,
leaving 68 characters.

But after doing the experiments above with the three-stroke version, I
think this is probably going the wrong direction.  I either need bits
that are easier to write, an encoding that’s *more* variable-length
rather than less, or both.

### A 2-stroke approach to the problem is probably not enough ###

Pairs of strokes can encode 4 bits if there’s no weight or similar
constraint.  Hamming suggests that using a single 4-bit symbol would
be advantageous for space, ‘e’, ‘t’, and ‘h’:

    >>> for c, f in collections.Counter(
        c for line in bible for c in line
        ).most_common(8): print("%.3f %r" % (f/4351845, c))
    ... 
    0.173 ' '
    0.094 'e'
    0.071 't'
    0.064 'h'
    0.059 'a'
    0.054 'o'
    0.051 'n'
    0.043 's'

1/16 is 0.0625.  You could imagine a base-16 prefix code with
single-codon representations for those four characters, assigning the
other 6 codons as “zones” followed by another base-16 codon to
represent another 96 characters.

For 40% of characters in English, this will use only 2 strokes, but 4
strokes for the other 60%, for an average of 3.2 strokes.  This is
probably within a few percent in writing speed to the 6×6 system
outlined earlier, because about 6.5% of characters in the Bible file
require a shift character before them, the other 93.5% being lowercase
letters, spaces, periods, and commas.  So Huffman isn’t going to get
us very far.  What we need is a higher-bit-rate writing system.

### An approach with curved strokes and guidelines ###

Suppose that we draw two parallel horizontal guidelines, then position
our handwritten marks relative to them, moving left to right.  There
are five easily distinguishable positions in a given column: above the
top guideline, crossing the top guideline, between the two guidelines,
crossing the bottom guideline, and below the bottom guideline.

Suppose our marks are shaped like rounded right angles.  I think it
should be possible to quickly draw these in eight easily
distinguishable orientations; four of these are ╭, ╮, ╯, and ╰, with
the arms of the angle horizontal and vertical, and thus its opening
facing diagonally.  Versions with diagonal arms whose opening faces
vertically or horizontally don’t exist in Unicode, though 🮤, 🮥, 🮦, and
🮧 are close, and so are ⊃, ⊂, ∪, and ∩, or possibly ), (, ◡, and ◠,
and maybe 🤇, which mysteriously doesn’t have a reflected partner.

Combining these eight marks with five positions gives us 40 possible
characters to write with a single quick curved pen stroke, which seems
like it would be a good set to assign to space and the lowercase
letters, as before.

I folded a sheet of paper to get two parallel guidelines.  Drawing 20
∩ characters crossing my top guideline took me 18.46 seconds.  Drawing
another 20 ╭ characters in between the guidelines took me 18.69
seconds.  Assuming 5.32 bits per character, this is about 5.7 bits per
second, more than twice as fast as the previous test, not counting the
time to make the guidelines.  (The first seven strokes didn’t actually
come out properly because my ballpoint pen was “skipping”, a term
grossly inadequate in this case, but I counted them anyway.)

Straight strokes might be usable as quicker alternatives for extremely
common characters, such as space and lowercase ‘etaon’, but for
example a vertical stroke | would be easy to confuse with curved
vertical strokes ) or (, so we should consider them as taking up two
of these 40 possibilities.

I tried drawing 20 / characters below my bottom guideline.  This took
9.67 seconds; assuming 4.32 bits per character, this is 8.9 bits per
second.  To get a more realistic speed, I instead tried drawing them
in the five positions one after the other.  This took 18.42 seconds,
which is 4.7 bits per second.

To figure out which direction a curved stroke is open, I suspect that
the easiest approach is to figure out the direction from its mean ink
pixel location to the center of its bounding box (or its 5th to 95th
percentile bounding box, say).  A slightly better approach is probably
to figure out which of the octants of the bounding box have the least
ink pixels in them.  But I probably shouldn’t speculate on classifiers
like this without test data.

You could represent less common characters with columns containing two
strokes, but it seems like it would be just as easy to move your hand
one column to the right before writing the second stroke, which gives
you twice the repertoire as writing two strokes in the same column.
This also avoids ambiguity about whether two strokes are in the same
column or not: it’s easier to reliably decide whether one is to the
left or right of the other than to reliably decide whether one is to
the left, below, or right of the other.
