Muriate of lime makes a nicely plastic mass when you boil a solution
of it down; this is sometimes considered a nuisance, because it makes
it hard to obtain the dry material.  [Wikipedia][0] gives its melting
points as follows:

- 772–775 °C (1,422–1,427 °F; 1,045–1,048 K) anhydrous
- 260 °C (500 °F; 533 K) monohydrate, decomposes
- 175 °C (347 °F; 448 K) dihydrate, decomposes
- 45.5 °C (113.9 °F; 318.6 K) tetrahydrate, decomposes
- 30 °C (86 °F; 303 K) hexahydrate, decomposes

[0]: https://en.wikipedia.org/wiki/Calcium_chloride

What this sequence of decompositions looks like in practice is that
you get a liquid which never quite boils, unless you heat it to the
anhydrous form’s boiling point of 1935°, but gradually bubbles out
more and more water, getting gooier and gooier and occupying less and
less volume.  It also splatters little drops of the liquid around, and
those tend to rapidly dry into a white microcrystalline solid,
whatever the appropriately hydrated form is for the temperature you’re
at.  Sometimes you can also get white crusts over the top of the goo
with an air space underneath them.  But the main body of the goo
remains a viscous, honey-like liquid up to quite surprising
temperatures.  Unlike organic oils, this “oil bath” doesn’t decompose
or smoke, and it isn’t combustible.

Then, if you allow it to cool, it typically forms a rock-hard
semicrystalline solid of a mixture of hydrates, which adheres very
firmly to materials such as glazed ceramic and soft-drink can liners,
though not silicone.  It’s considerably sturdier than things like
near-freezing water ice, but tends to form sharp edges when broken,
like glass.

In this form of a hot, viscous liquid, it can easily be formed and
even molded.  Because it’s a chloride, it tends to be pretty corrosive
to metals, but I think it’s probably fairly inert to most plastics;
intuitively, at least, you’d expect it to be fairly poor at wetting
nonpolar materials.  The resulting molded article has the feature of
being deliquescent at anything above about 40% relative humidity.

I think that if you make a *thin* molded article from it, you may be
able to heat it gently to dehydrate it without remelting it.  I think
the “decomposition” temperatures given above are actually the points
at which the respective hydrates “melt”, which is to say, dissolve in
their own water of hydration.  But in the solid state they all have a
certain water vapor pressure which increases with temperature; the
hexahydrate’s vapor pressure is about 40% of pure water’s vapor
pressure, so in air under 30° that is kept under 40% relative
humidity, it will gradually dehydrate into the tetrahydrate.  This
suggests that you ought to be able to, for example, “melt” the
dihydrate at about 180°, mold it, and then bake the molded article at
170° to transform it into the monohydrate, which you can then bake at
250° to transform it into the anhydrous form, in both cases with a
certain loss of volume.

Its eutectic with water melts at about -50°, so you have, in effect, a
liquid that is fairly stable from -50° to 1935°, a truly astonishing
range.

(Actually, [maybe it solidifies above 178°][1] as the monohydrate and
anhydrous forms don’t quite act like this; I should probably carry out
the experiment carefully.  But maybe they take a long time to
solidify.)

[1]: https://www.science.smith.edu/~jbrady/petrology/igrocks-diagrams/binary/H2O-CaCl2.php

In effect, though, you can melt it at a low temperature, then mold it
into a solid that remains stable until a higher temperature — with the
baking suggested above, perhaps a *much* higher temperature.

It occurs to me that these features might make it useful as a material
for metal casting.  Not cast iron, obviously, which requires much
higher temperatures than 775°, or even brass.  But lead (328°),
aluminum (660°), lead-tin solder (183°), tin (232°), babbitt (249°),
zinc (420°), aluminum bronze (600–655°), magnesium (650°), and zamak
(381–387°) should all be castable in this way.

Such a molding compound would probably be improved by appropriate
fillers, which could increase its tensile strength, increase
resistance to crack propagation, and reduce the shrinkage on further
dehydration, as well as reducing the cost.  Promising candidates might
include chopped glass fiber, chopped carbon fiber, quartz sand, quartz
flour, calcium carbonate, bentonite or other clays, talcum powder,
and, for low-temperature applications, wood pulp.

Calcium carbonate or some similar buffer could be particularly useful
with repeated use to prevent the material from engendering spirits of
salt.  The benefits of calcium carbonate in particular are that it is
relatively inert itself (unlike, for example, calcium oxide); it could
add significant strength to the shape; it is very cheap; and its
reaction product with traces of spirits of salt would simply be more
muriate of lime, stabilizing the mixture’s composition rather than
causing it to drift.

For many applications, the tendency of the resulting mold to first
swell with water absorbed from the air and then deliquesce would be a
disadvantage.  You’d have to keep it in a super-dry atmosphere, in a
hermetically-sealed container, or in a vacuum during its lifetime, and
it would dissolve either rather quickly upon exposure to water or more
slowly upon exposure to moist air.  But for mold-making material, this
possibility of recycling the mold material easily could be beneficial.

Many other deliquescent or hygroscopic salts, such as muriate of
magnesia, tachyhydrite, carnallite, leonite/picromerite, or epsom
salt, might work as alternatives, but I don’t have much experience
with them.

An extremely desirable thing to do with the muriate would be to heat
it under pressure until the vapor pressure of its water was higher
than atmospheric, and then release the pressure to convert it into
foam, which should quickly solidify, since the bubble walls would have
lost much of their water into the bubbles.  Gradually heating the foam
further could drive more water out and increase its melting point
further.
