I was just reading Rett Berg's notes on their project "Civboot", as
well as Fabry's paper on capability machines from 01974, and a simple
reasonable compromise virtual machine design for interprocess
protection appeared to me.

The problem I'm trying to solve is how to use a virtual machine to
efficiently provide fault isolation between different processes
running on the same single-threaded CPU in the same address space,
because the CPU doesn't have paging hardware.  Most microcontrollers
don't have paging hardware, but paging can be super useful!  For
example, paging lets you do copy-on-write (lazy copying), easily write
code to manipulate objects much larger than your physical memory, and
provide transparent persistence.

Overview of the problem space and the solution space
----------------------------------------------------

Smalltalk-80 solves this problem, like the Burroughs B5000, by tagging
every word with type bits that distinguish binary numbers
("SmallIntegers") from pointers, and making most words be pointers
which point to "objects" ("segments" in Fabry's paper and on the
Burroughs).  Smalltalk additionally supports objects such as strings
whose contents are bytes rather than words; these are converted to
SmallIntegers when loaded into the registers of the virtual machine by
indexing operations.

Tag bits in every word, what Fabry calls "the tagged approach", is
expensive, because they require conditionals on every operation.  He
reviews a couple of designs that use a cheaper alternative he calls
"the partition approach", namely the Chicago Magic Number Machine and
the Plessey System 250, though he think that eventually the tagged
approach will supplant it:

> In the partition approach, each segment is designated at creation as
> containing either capabilities or data.  In addition, there is one
> set of processor registers for data and one for capabilities.  The
> processor instruction set satisfies rules analogous to those above:
> data can be copied only into data segments and registers;
> capabilities can be copied only into capability segments and
> registers; and so on.

From this point of view the Smalltalk approach is sort of a hybrid:
all the processor registers are tagged, but string objects and the
like are "designated at creation as containing data".  But mostly it's
the Burroughs tagged approach.

For efficiency we would like to compile our virtual machine
instruction set to native machine code, and, in the inner loops of
algorithms like memory copy, string search, and matrix-vector
multiplication, we would like that code to avoid tag checks and bounds
checks.  And we would like to do this without sacrificing the memory
safety that prevents a bug in one process from corrupting data
belonging to another process.

Efficiency is important for power usage: either battery life or heat
production is generally a limiting factor in what you can do with a
computer.

A basic virtual machine
-----------------------

So, let's see where we can go with the partition approach, with
paging.

To cover the basics, we endow our virtual machine, as usual, with a
program counter PC, a stack pointer SP, some small fixed number of
general-purpose data registers or a general-purpose data stack, some
ALU instructions which read and write those data registers, and some
control-flow instructions which give us conditionals and loops within
the current subroutine.  None of this requires any dynamic checking;
all of it can be straightforwardly compiled to native code.  A
register machine rather than a stack machine makes it a little more
trivial to generate efficient native code.

For additional local variables, let's add fetch-word and store-word
instructions that index data words off the stack pointer with
immediate (statically known) indices.  By inspecting the code for a
newly loaded subroutine we can compute the maximum index it uses and
thus how much stack space to allocate upon entry and deallocate upon
exit.  These instructions, too, require no dynamic checking and can be
fairly trivially compiled to native code.

This is enough to write subroutines that calculate GCD or square
roots, but so far we don't have any way to store data structures.

Indexing paged virtual memory
-----------------------------

For paged virtual memory, let's add some number of "page registers" to
our virtual CPU, similar to segment registers; at least 3 are needed,
but I think probably 8 would be a better number.  Each of these
registers conceptually holds a capability to a page of memory of some
fixed size, such as 1024 bytes.  Now we add memory-indexing
instructions; these have a bitfield to specify which of the pages we
are indexing and another bitfield to specify which data register to
get the low bits of the index from.  Only the appropriate number of
low bits of this register (say, 10 of them) are used for the index.

With each of these architecturally visible page registers we associate
a base-address register in the implementation; this register (a CPU
register if possible, but otherwise a fixed place in RAM) contains the
current base address of the page, which is guaranteed to be resident
in RAM whenever the active thread has it in one of its page registers.

Because only the low bits of the index register are used, and the page
is always resident, the indexing operation is statically known to be
safe.

So a load-byte instruction in the virtual machine might compile to
this amd64 code, if we're using 1024-byte pages, the base-address
register is %rbx, the index register is currently assigned to %rdi,
and the result goes into %rdx:

    mov %rdi, %rdx
    and $0x3ff, %rdx
    mov (%rbx,%rdx), %dl

Or, on RISC-V:

    andi a1, a2, 0x3ff
    add a3, a1, a1
    lb a1, 0(a1)

In the important and common special case where the index is a
constant, the AND can be moved to compile time, and for both
processors this becomes a single instruction:

    mov 5(%rbx), %dl  # amd64
    lb a1, 5(a3)      # RISC-V

Thus even a fairly naive JIT implementation of the virtual machine
(similar to the "code-generating loader" from Michael Franz's
dissertation) can get reasonable performance for such operations,
though you could imagine a less stupid compiler performing loop
hoisting and strength reduction to reduce the cost further.

For a load-32-bit operation, you might want to AND with 0x3fc instead
of 0x3ff, thus ignoring the low two bits of the pointer.  If you AND
with 0x3ff instead, the word loaded may protrude beyond the boundary
of the page.

Supposing the low bits of the page base address (in a3 or %rbx in the
above example code) are zero, it doesn't matter whether you use
addition, OR, or XOR to combine the base address with the index.  If
you use OR or XOR, you can set low-order bits in it to subdivide the
page into a power-of-2 number of power-of-2-sized objects.  This
requires an additional instruction on amd64.

Loading page registers
----------------------

You need some kind of way to load a page into a page register.  This
operation is the point at which you'd want to do whatever kind of
address translation is necessary.  Moreover, if some of the page
registers are for read-only pages, while others are not executable,
you can do access control at the time of loading these registers.

A thoroughly traditional, vaguely POSIX-like interface for loading
page registers maintains a per-process "memory map" that maps "virtual
addresses" to pages; the API might be something like this:

- `map(va, pr)` is a virtual machine bytecode that loads the page
  register `pr` to correspond to the virtual address `va`, which is
  looked up in the process's memory map.
- `va = allocate(size)` is a virtual machine bytecode that allocates
  `size` bytes of zeroed read-write pages, adds them to the process's
  memory space, and returns virtual address `va` at which the first
  has been mapped.
- `freeze(va)` is a virtual machine bytecode that makes the page
  mapped at `va` read-only.
- `free(va)` is a virtual machine bytecode that removes the page
  mapped at `va` from the process's memory space.
- `ifence(va)` is a virtual machine bytecode that makes the page
  mapped at `va` read-only and executable; this may trigger JIT
  compilation.
- `va = mmap(filedes, perms, offset, size)` is a system call that maps
  `size` bytes from file descriptor `filedes` into the process's
  memory space with permission bits `perms`, returning the virtual
  address `va` at the beginning of the resulting mapping.

### A more KeyKOS-like alternative ###

Other interfaces are possible.  The above requires pointer arithmetic
and complicates the sharing of memory between processes, which is what
Fabry's paper is about.  A different, KeyKOS-like possibility stores
"keys" in "nodes" of, say, 16 slots; some keys are capabilities to
nodes, while others are capabilities to pages.  The process is
associated with a node which holds its keys, these being the
"capability registers" of the virtual CPU.

- `loadkey(whence, where, dest)` is a virtual machine bytecode that
  provides access to keys in another node: `whence` is an index into
  the process's node at which to find a node key; `where` is an index
  into the node it designates; and `dest` is another index into the
  process's node at which to store the key found at that index.
- `storekey(whither, where, src)` does the opposite: it copies the key
  from slot `src` of the process's node to slot `where` of the node
  identified by the node key at index `whither` in the process's node.
- `blank(i)` allocates a blank page and stores the read-write page key
  at index `i` in the process's node.  (In KeyKOS the analogous
  operation requires a space bank to draw on.)
- `map(i, pr)` loads the page whose key is at index `i` of the
  process's node into page register `pr`.
- `node(i)` allocates a blank node and stores the read-write key for
  it at index `i` in the process's node.
- `weaken(i)` converts the page or node key at index `i` in the
  process's node into a read-only key.
- `compile(i)` converts the page key at index `i` of the process's
  node into an executable page key.
- `offset(i, n)` adds the intra-page offset `n` to the page key at
  index `i` of the process's node for use with XOR as described above.

Near, far, and huge pointers
----------------------------

These virtual-machine semantics are kind of error-prone, because, like
MS-DOS, they have near, far, and huge pointers.  A near pointer is
just an index into a page, and it's the kind being handledyu by the
example instruction sequences above.  A far pointer is a page
identifier (a virtual address or a page key) possibly paired with an
index, though the `offset` operation described above might allow us to
use just a page identifier for this.  Using a far pointer requires
loading it into a page register and possibly also indexing it.  A huge
pointer is a potentially large list of page identifiers, perhaps
obtained through virtual memory addressing.

When a compiler is generating code for the virtual machine, it must be
conscious of whether the object it is indexing fits within a single
page or may extend across pages.  If it may extend across pages, the
compiler must use a huge pointer, loading the correct page pointer
into a page register before each access.

Hmm, this is all sort of confused.  By using far pointers you can load
just the far pointer into a (page) register.
