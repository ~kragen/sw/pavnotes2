I just designed this goofy keyboard circuit in Paul Falstad's circuit.js:

    $ 1 0.000005 0.8031194996067259 50 5 43 5e-11
    s 16 192 64 240 0 1 true
    s 96 192 144 240 0 1 true
    s 256 192 304 240 0 0 true
    s 176 192 224 240 0 1 true
    s 416 192 464 240 0 1 true
    s 336 192 384 240 0 1 true
    s 336 256 384 304 0 1 true
    s 416 256 464 304 0 1 true
    s 176 256 224 304 0 1 true
    s 256 256 304 304 0 1 true
    s 96 256 144 304 0 1 true
    s 16 256 64 304 0 1 true
    s 336 128 384 176 0 1 true
    s 416 128 464 176 0 1 true
    s 176 128 224 176 0 1 true
    s 256 128 304 176 0 1 true
    s 96 128 144 176 0 1 true
    s 16 128 64 176 0 1 true
    w 16 128 96 128 0
    w 96 128 176 128 0
    w 176 128 256 128 0
    w 256 128 336 128 0
    w 336 128 416 128 0
    w 16 192 96 192 0
    w 96 192 176 192 0
    w 176 192 256 192 0
    w 256 192 336 192 0
    w 336 192 416 192 0
    w 416 256 336 256 0
    w 336 256 256 256 0
    w 256 256 176 256 0
    w 176 256 96 256 0
    w 96 256 16 256 0
    w 64 240 64 304 0
    w 144 304 144 240 0
    w 144 240 144 176 0
    w 224 176 224 240 0
    w 224 240 224 304 0
    w 304 304 304 240 0
    w 304 240 304 176 0
    w 384 176 384 240 0
    w 384 240 384 304 0
    w 464 176 464 240 0
    w 464 240 464 304 0
    w 64 176 64 240 0
    R 64 64 64 32 0 0 40 5 0 0 0.5
    r 64 64 64 112 0 10000
    w 64 112 64 176 0
    w 144 112 144 176 0
    r 144 64 144 112 0 10000
    R 144 64 144 32 0 0 40 5 0 0 0.5
    w 224 112 224 176 0
    r 224 64 224 112 0 10000
    R 224 64 224 32 0 0 40 5 0 0 0.5
    w 304 112 304 176 0
    r 304 64 304 112 0 10000
    R 304 64 304 32 0 0 40 5 0 0 0.5
    w 384 112 384 176 0
    r 384 64 384 112 0 10000
    R 384 64 384 32 0 0 40 5 0 0 0.5
    w 464 112 464 176 0
    r 464 64 464 112 0 10000
    R 464 64 464 32 0 0 40 5 0 0 0.5
    w -16 128 16 128 0
    w -16 192 16 192 0
    w -16 256 16 256 0
    d -16 128 -80 128 2 default
    d -16 192 -80 192 2 default
    d -16 256 -80 256 2 default
    193 -432 48 -384 48 4096 5
    w -336 112 -208 112 0
    w -304 -48 -304 144 0
    w -304 144 -208 144 0
    w -400 112 -400 208 0
    w -208 208 -400 208 0
    w -304 144 -304 160 0
    w -304 160 -304 176 0
    w -304 176 -208 176 0
    w -368 -48 -368 -16 0
    w -368 -16 -368 16 0
    w -368 -16 -256 -16 0
    w -256 -16 -256 240 0
    w -208 240 -256 240 0
    w -400 208 -400 272 0
    w -400 272 -208 272 0
    R -368 -160 -368 -224 1 2 500 2.5 2.5 0 0.5
    151 -208 128 -80 128 0 2 5 5
    151 -208 192 -80 192 0 2 0 5
    151 -208 256 -80 256 0 2 5 5
    x -400 335 -167 338 4 24 row\sscan\sgenerator
    b -32 304 -461 -235 0
    b 0 112 506 307 0
    x 520 138 638 141 4 24 keyswitch
    x 520 165 598 168 4 24 matrix
    x -266 -101 -98 -98 4 24 ripple\scounter
    x -269 -72 -94 -69 4 24 (a\sglitchy\sboi!)
    x -401 364 -53 367 4 24 (active\slow,\s"open\scollector")
    x 127 9 407 12 4 24 column\spullup\sresistors
    x -403 382 -129 385 4 12 in\s02023\sthis\ssi\sbullshit,\suse\sa\smicrocontroller
    R -400 16 -400 -48 0 0 40 5 0 0 0.5
    w -368 -160 -368 -48 0
    151 -304 -128 -304 -48 0 2 5 5
    w -368 -160 -320 -160 0
    w -320 -160 -320 -128 0
    w -320 -160 -288 -160 0
    w -288 -160 -288 -128 0
    M 464 304 432 336 0 2.5
    M 384 304 352 336 0 2.5
    M 224 304 192 336 0 2.5
    M 304 304 272 336 0 2.5
    M 688 304 656 336 0 2.5
    M 768 304 736 336 0 2.5
    M 64 304 32 336 0 2.5
    M 144 304 112 336 0 2.5

Then I noticed that he has a ring-counter element, so I used it to
make this simplified version:

    $ 1 0.000005 0.8031194996067259 50 5 43 5e-11
    s 16 192 64 240 0 1 true
    s 96 192 144 240 0 1 true
    s 256 192 304 240 0 0 true
    s 176 192 224 240 0 1 true
    s 416 192 464 240 0 1 true
    s 336 192 384 240 0 1 true
    s 336 256 384 304 0 1 true
    s 416 256 464 304 0 1 true
    s 176 256 224 304 0 1 true
    s 256 256 304 304 0 1 true
    s 96 256 144 304 0 1 true
    s 16 256 64 304 0 1 true
    s 336 128 384 176 0 1 true
    s 416 128 464 176 0 1 true
    s 176 128 224 176 0 1 true
    s 256 128 304 176 0 1 true
    s 96 128 144 176 0 1 true
    s 16 128 64 176 0 1 true
    w 16 128 96 128 0
    w 96 128 176 128 0
    w 176 128 256 128 0
    w 256 128 336 128 0
    w 336 128 416 128 0
    w 16 192 96 192 0
    w 96 192 176 192 0
    w 176 192 256 192 0
    w 256 192 336 192 0
    w 336 192 416 192 0
    w 416 256 336 256 0
    w 336 256 256 256 0
    w 256 256 176 256 0
    w 176 256 96 256 0
    w 96 256 16 256 0
    w 64 240 64 304 0
    w 144 304 144 240 0
    w 144 240 144 176 0
    w 224 176 224 240 0
    w 224 240 224 304 0
    w 304 304 304 240 0
    w 304 240 304 176 0
    w 384 176 384 240 0
    w 384 240 384 304 0
    w 464 176 464 240 0
    w 464 240 464 304 0
    w 64 176 64 240 0
    R 64 64 64 32 0 0 40 5 0 0 0.5
    r 64 64 64 112 0 10000
    w 64 112 64 176 0
    w 144 112 144 176 0
    r 144 64 144 112 0 10000
    R 144 64 144 32 0 0 40 5 0 0 0.5
    w 224 112 224 176 0
    r 224 64 224 112 0 10000
    R 224 64 224 32 0 0 40 5 0 0 0.5
    w 304 112 304 176 0
    r 304 64 304 112 0 10000
    R 304 64 304 32 0 0 40 5 0 0 0.5
    w 384 112 384 176 0
    r 384 64 384 112 0 10000
    R 384 64 384 32 0 0 40 5 0 0 0.5
    w 464 112 464 176 0
    r 464 64 464 112 0 10000
    R 464 64 464 32 0 0 40 5 0 0 0.5
    w -16 128 16 128 0
    w -16 192 16 192 0
    w -16 256 16 256 0
    x -400 335 -167 338 4 24 row\sscan\sgenerator
    b 15 114 489 309 0
    x 506 133 624 136 4 24 keyswitch
    x 506 160 584 163 4 24 matrix
    x -401 364 -75 367 4 24 (active\slow,\sopen\scollector)
    x 127 9 407 12 4 24 column\spullup\sresistors
    x -403 382 -129 385 4 12 in\s02023\sthis\sis\sbullshit,\suse\sa\smicrocontroller
    M 464 304 432 336 0 2.5
    M 384 304 352 336 0 2.5
    M 224 304 192 336 0 2.5
    M 304 304 272 336 0 2.5
    M 64 304 32 336 0 2.5
    M 144 304 112 336 0 2.5
    163 -416 176 -368 176 5126 3 0 5 0
    g -416 240 -416 288 0 0
    w -416 208 -416 240 0
    R -384 144 -384 96 1 2 500 2.5 2.5 0 0.5
    w -16 192 -112 192 0
    w -16 128 -208 128 0
    t -48 272 -16 272 0 1 5.956582920426318e-74 0 100 default
    g -16 288 -16 336 0 0
    r -48 272 -96 272 0 1000
    w -320 240 -320 272 0
    t -144 208 -112 208 0 1 0.6330334248133228 0.6524320019195652 100 default
    g -112 224 -112 240 0 0
    r -144 208 -176 208 0 1000
    w -176 208 -320 208 0
    w -96 272 -320 272 0
    w -272 144 -320 144 0
    r -240 144 -272 144 0 1000
    g -208 160 -208 176 0 0
    t -240 144 -208 144 0 1 2.1697971354955272e-33 0 100 default
    w -320 144 -320 176 0
    x -274 92 -106 95 4 12 open-collector\sRTL\sinverters

Charlieplexing a keyboard
-------------------------

Then I did [this Charlieplexed version with 15
keys](https://tinyurl.com/2adwnjer):

    $ 1 0.000005 13.097415321081861 50 5 43 5e-11
    163 -256 96 -144 96 5122 6 0 0 0 0 5 0
    w -96 96 -64 96 0
    s -64 272 0 304 0 1 true
    d -96 256 -64 272 2 default
    w -64 128 -96 128 0
    w -96 224 0 224 0
    w -96 256 16 256 0
    w 0 224 0 304 0
    w 16 320 128 320 0
    d 16 320 48 336 2 default
    s 48 336 112 368 0 1 true
    d 16 256 48 272 2 default
    s 48 272 112 304 0 1 true
    w 112 304 112 368 0
    w -96 192 112 192 0
    w 112 192 112 304 0
    w 16 256 128 256 0
    w -96 160 224 160 0
    w 224 160 224 304 0
    w 224 304 224 368 0
    w 0 304 16 320 0
    w 112 368 128 384 0
    w 224 368 224 432 0
    s 160 400 224 432 0 1 true
    d 128 384 160 400 2 default
    w 128 384 240 384 0
    w 224 432 240 448 0
    w 336 432 336 496 0
    s 272 464 336 496 0 1 true
    d 240 448 272 464 2 default
    w 240 448 352 448 0
    w 128 256 240 256 0
    s 160 272 224 304 0 1 true
    d 128 256 160 272 2 default
    w 240 256 352 256 0
    s 272 272 336 304 0 1 true
    d 240 256 272 272 2 default
    w 128 320 240 320 0
    s 160 336 224 368 0 1 true
    d 128 320 160 336 2 default
    w 240 320 352 320 0
    s 272 336 336 368 0 0 true
    d 240 320 272 336 2 default
    w 240 384 352 384 0
    s 272 400 336 432 0 1 true
    d 240 384 272 400 2 default
    w 336 432 336 368 0
    w 336 368 336 304 0
    w 336 304 336 128 0
    w -64 128 336 128 0
    w 352 256 464 256 0
    s 384 272 448 304 0 1 true
    d 352 256 384 272 2 default
    w 352 320 464 320 0
    s 384 336 448 368 0 1 true
    d 352 320 384 336 2 default
    w 352 384 464 384 0
    s 384 400 448 432 0 1 true
    d 352 384 384 400 2 default
    w 352 448 464 448 0
    s 384 464 448 496 0 1 true
    d 352 448 384 464 2 default
    w 448 432 448 368 0
    w 448 368 448 304 0
    w 448 304 448 96 0
    w 448 96 -64 96 0
    w 352 512 464 512 0
    d 352 512 384 528 2 default
    s 384 528 448 560 0 1 true
    w 336 496 352 512 0
    w 448 432 448 496 0
    w 448 560 448 496 0
    w 448 560 464 560 0
    M 464 256 496 224 0 2.5
    M 464 320 496 288 0 2.5
    M 464 448 496 416 0 2.5
    M 464 384 496 352 0 2.5
    M 464 560 496 528 0 2.5
    M 464 512 496 480 0 2.5
    R -256 256 -256 240 0 0 40 5 0 0 0.5
    g -256 128 -256 160 0 0
    R -224 64 -224 32 1 2 600 2.5 2.5 0 0.5
    d -160 96 -96 96 2 default
    d -160 128 -96 128 2 default
    d -160 192 -96 192 2 default
    d -160 160 -96 160 2 default
    d -160 224 -96 224 2 default
    d -160 256 -96 256 2 default
    r 16 320 16 400 0 1000000
    g 16 400 16 416 0 0
    g -96 336 -96 352 0 0
    r -96 256 -96 336 0 1000000
    g 128 464 128 480 0 0
    r 128 384 128 464 0 1000000
    g 240 528 240 544 0 0
    r 240 448 240 528 0 1000000
    g 352 592 352 608 0 0
    r 352 512 352 592 0 1000000
    g 448 640 448 656 0 0
    r 448 560 448 640 0 1000000
    x -344 415 -61 418 4 24 Charlieplexed\skeyboard
    o 78 64 0 4098 5 0.1 0 1

This version uses active-high outputs (because that's what Falstad's
ring counter produces) and gloms a diode on each one to simulate
tristating a microcontroller GPIO pin (setting it to input mode) when
it's not high.  This diode is reverse-biased precisely when that pin
would be sensing a keypress.

This strictly ordered form of charlieplexing allows any pin to pull
any *previous* pin high: Q4 can pull Q0, Q1, Q2, or Q3 high, but not
Q5, because that diode is missing.  Q0 can't pull anything high and
might as well be skipped in the scanning sequence.  This is due to me
mindlessly aping output-oriented charlieplexing.

This keyboard circuit already suffers from extensive key jamming or
ghosting: while the Q4-Q3 key is pressed, the only difference between
pressing the Q3-Q2 key and both the Q3-Q2 key and the Q4-Q2 key is a
diode drop, which a digital I/O pin can't detect.  But if we are
willing to accept such limitations, and stipulate that we are willing
to accept only one key at a time being reliably detected, we can go
further and install twice the number of keys, the other half having
the diodes in the opposite direction.  Then any line can pull any
other line high through some key, and we have N(N-1) keys instead of
half that.

With carefully chosen diode drops, we can do better, getting N(N-1)
keys with NKRO; if the diode drop of a single key is reliably small
enough for a diode drop below Vcc to register as logic 1, but Vcc
minus two diode drops in series is reliably logic 0, then this
condition is met.  Unfortunately standard CMOS thresholds are not
quite there; a diode drop of anything less than one third of Vcc will
ensure the former but not the latter, and anything more will ensure
the latter but not the former.  So you're potentially in metastability
land.  Most circuits exceed their specs by enough that it would
probably be fine.  A red LED at Vcc=5V would probably be okay.

Alternatively you could try to tweak things a bit by feeding the hot
line from a higher voltage than the one you're thresholding against,
or something, or maybe TTL levels would help.

An RC time constant keyboard
----------------------------

I also wrote [this design to show how to get many keys on two
pins](https://tinyurl.com/225tm7er) at the expense of several passives
per key:

    $ 1 0.000005 0.16487212707001284 50 5 43 5e-11
    R -464 256 -512 256 0 2 500 2.5 2.5 0 0.5
    r -416 256 -352 256 0 100
    s -352 256 -352 352 0 0 false
    c -352 384 -352 480 0 1e-8 1.3771221610808395 0.001
    g -352 480 -352 528 0 0
    r -336 384 -272 384 0 4700
    c -256 384 -256 480 0 1e-8 1.2305818199295784 0.001
    g -256 480 -256 528 0 0
    r -208 384 -160 384 0 100000
    g -160 384 -160 528 0 0
    w -352 352 -352 384 0
    r 320 352 320 480 0 1000000
    g 320 480 320 528 0 0
    368 320 352 320 304 0 0
    d -464 256 -416 256 2 default
    w -272 384 -256 384 0
    w -336 384 -352 384 0
    d -336 384 -336 352 2 default
    w 64 352 320 352 0
    w -256 384 -208 384 0
    162 -208 384 -208 480 2 default-led 1 0 0 0.01
    g -208 480 -208 528 0 0
    g 192 480 192 528 0 0
    162 192 384 192 480 2 default-led 1 0 0 0.01
    w 144 384 192 384 0
    d 64 384 64 352 2 default
    w 64 384 48 384 0
    w 128 384 144 384 0
    w 48 352 48 384 0
    g 240 384 240 528 0 0
    r 192 384 240 384 0 47000
    g 144 480 144 528 0 0
    c 144 384 144 480 0 1e-8 0.01954535730676947 0.001
    r 64 384 128 384 0 10000
    g 48 480 48 528 0 0
    c 48 384 48 480 0 1e-8 0.022546764494938216 0.001
    s 48 256 48 352 0 1 false
    w -336 352 64 352 0
    w 48 256 -352 256 0
    o 13 1 0 x81012 10 12.8 0 2 1 -88 13 3 5 0


This has a second-order RC circuit with an extra LED in it attached to
each keyswitch.  The clock signal charges up the capacitors through
the switch's relatively low but unknown resistance fairly quickly.
When the clock tristates (simulated here with a diode, again) the RC
network discharges until the next clock pulse.  Each key's RC network
has another diode attaching it to an analog input pin, so that pin
sees the voltage on whichever RC network is charged, minus a diode
drop.

The RC network contains two capacitors (0.01μF in this sim), one that
is charged up to Vcc (5V here) and discharges quickly, and another
that is charged up to a lower voltage regulated by the LED (about 1.3
V here) and takes longer to discharge.  On a log-linear plot, after
correcting for the diode drop, the discharge curve should look like
two straight lines, the first one being steeper, with the slope of
each being a factor of *e* every τ = RC time constant.

The two discharge resistors are different for every key, making each
discharge curve different in a consistent and measurable way which
does not depend on Vcc, the LED voltage, or the switch resistance.
One of my two example keys here uses 4.7kΩ and 100kΩ; the other uses
10kΩ and 47kΩ.  This should give τ of 47μs, 1ms, 100μs, and 470μs
respectively.

In this simulation the diode-max function is working far better than
it has any right to, I guess because I have a 1MΩ pulldown and so the
bias current through the diode is about a microamp.  This leads to the
error induced by this diode to be only about 170mV at worst, more like
80mV at the end of decay.

Suppose we're sampling this discharge curve for 1ms at 1Msps with 10
effective bits of precision.  The first slope from 5V down to 1.3V,
which is 1.3 time constants; with the resistances above, that's 63μs
(and 63 samples) or 130μs and 130 samples.  We could probably go down
to 20 samples or so and still be pretty okay, or up to 500.  Then the
second slope from 1.3V down to, say, 0.6V, should occupy the rest of
the millisecond, about a time constant or two.

If we had a bit faster decay, we could maybe get a more precise
estimaate of it.  Suppose we have an error on the order of 1/1000 in
the voltage; at 0.5 volts that's a 1% error.  Probably we can average
this over most of the samples on the relevant line, reducing the error
in our slope estimate over that by a factor of 4-32, the square root
of the number of samples.  So 0.25% to 0.03% error, about 9-12 bits of
precision for each slope.

That crudely suggests that you could measure each resistor to 0.3% or
better, which would mean that if they can range from 2.2kΩ to 100kΩ,
you should be able to distinguuish some 1300 different resistor
values.  If we reserve half this range for each resistor, we would be
able to reliably distinguish some 400'000 different keys, all hooked
up to the same two GPIO pins.  In practice the precision will be much
less than this, you can't get that many resistor values, and there are
considerations like choosing the two resistor values for a key far
enough apart that you can distinguish their slopes, but that
computation still suggests you can hook a lot of such keys up to two
pins, as many as you want.  Even E12 over this range has 24 values, so
you get 144 or more keys.

No rollover though.
