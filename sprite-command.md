I was thinking about how to draw an image with shell commands.  This
is of course too broad a problem, so let’s restrict our attention to
α-blending, which consists of a single aggregate operation:

    sprite(from=(x₀, y₀), to=(x₁, y₁), size=(Δx, Δy))

There is a source image *si* and a destination image *di*, and by
executing a sequence of these operations we alter the destination
image to our liking.  The fundamental operation is α-blending a pixel
in the destination image with a premultiplied pixel from the source
image:

    di[x, y] = di[x, y] * (1 - si[u, v].α) + si[u, v]

where

    u = x₀ + i;  x = x₁ + i  (i ∈ [0, Δx))
    v = y₀ + j;  y = y₁ + j  (j ∈ [0, Δy))

This operation is sufficient for things like conventional video games
and text rendering, it’s easy to program, and it can be executed
fairly fast with either GPUs or SIMD CPUs.

But how can we make it easy to invoke a sequence of such operations
from a shell script?  It takes a long time to start up a new process,
especially in bash, on the order of an entire millisecond.  So it
would be desirable to amortize that startup overhead over many
operations.

Shells are not very good at building up a long array of arguments, but
they can generate a long stream of text pretty easily.  So maybe the
way to do this is to start up a program with command-line arguments
specifying the source image and destination image, then feeding in a
bunch of alpha-blending commands on standard input:

    draw 4x6+8+12 at +100+25
    draw 4x6+16+20 at +104+25

This would be easiest to implement with an image file format that
supports alpha (unlike PPM or BMP) but is uncompressed (unlike
virtually anything else).  In file `filesystem-gui.md` I talked about
a design for such a format, using a 12-byte trailer to preserve
alignment and permit zero-copy data paths where possible.
