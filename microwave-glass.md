Watching [a video on “Making Glass in a Microwave” by “Degree of
Freedom”][0] I was surprised to see that the microwave oven he was
using was capable of not only melting glass (once it was already
heated enough to absorb radiation) but even heating up his
borosilicate powder mix to make glass without any additional
susceptors.  (His recipe was 2 parts by volume boric acid, 1½ parts
baking soda, 1 part quartz sand, and he got nice clear glass that
wasn’t full of bubbles.)

[0]: https://www.youtube.com/watch?v=h8DMIsqnRtk

He insulated the mix inside broken insulating firebricks to keep it
from cooling too much radiatively, but had various kinds of problems.

I suspect that a graphite crucible would be very useful here; it would
absorb microwaves efficiently and not react with the glass.  However,
the glass might still stick to it, which was a major problem he had
all along with his ceramic crucibles, each of which lasted only a
single melt.

Ideally for this kind of thing you’d like to use a “skull crucible”, a
powder bed around the thing being melted, so that it doesn’t exert
much stress on the thing you’re melting as it cools.

In the past I’ve used table salt for this, which is cheap and easily
available, but has certain downsides.  It melts at only 800.7° and
boils at 1465°, which is below the melting points of iron (1538°) and
of borosilicate glass (≈1650°), and it’s pretty reactive in its molten
form.

You could try using just quartz sand, but your nice clear glass piece
would come out with an opaque layer of quartz sand stuck to its
surface.  It might be possible to melt it into the surface, but
maybe challenging.

More promising skull-crucible material candidates might include oxides
of calcium (2613°) or magnesium (2852°), either of which can be washed
off at room temperature with a mild acid such as vinegar.  These will
still leave the glass surface enriched in calcium or magnesium, both
of which are common ingredients in soda-lime glass.

There’s still the problem of getting the melt started so it can absorb
microwaves efficiently.  You could heat the powder radiatively from a
conventional microwave kiln, whose inside surface is painted with a
susceptor such as graphite, graphene, magnetite, or carborundum, or by
adding a susceptor to the powder itself.

The most promising susceptor I can think of is an aluminum ring with a
gap in it; it will arc across the gap and locally produce very high
temperatures before melting and burning.  However, not all of the
aluminum will burn, and if you are to not have aluminum embedded in
your glass, you will have to oxidize it to aluminum oxide, which
borosilicate can easily dissolve.  This probably requires some kind of
oxygen source, and the air may not be sufficient.  Manganese dioxide
or lead dioxide should work; I believe that either can reduce to a
lower oxide that dissolves in the glass without discoloring it.
