I’ve been thinking a lot about algebraic-manipulation user interfaces,
where you interactively build up objects of some problem domain by
combining simpler domain objects, starting from primitive objects,
using some predefined set of operators.

This isn’t a fully general UI paradigm, but it does cover a lot of
interactions that traditional UIs are often not very good at.  But it
probably doesn’t cover, for example, sliders and text editing.

With a keyboard interface, something like RPN may be the best option,
but in a GUI it’s better to be able to just click on objects with a
mouse.  But I’m going to be focusing on a different level of the stack
here: not the particular UI operations used to designate objects and
operations, but the objects and operations that are being designated.

I’ve done some experiments with implementing such UIs for things like
polynomials, which is among their most obvious uses.  The thing I was
thinking was interesting in this case, though, is *programs*.  Or
program fragments.  Specifically, imperative control flow.

The Böhm and Jacopini structures: Π (`progn`), Ω (`while`), and Δ (`if`)
------------------------------------------------------------------------

The conventional operations for algebraically constructing imperative
control structures out of smaller ones are the [Böhm and Jacopini][0]
operations, which they call Π(a, b), Ω(α, a), and Δ(α, a, b).  Π puts
two “functional boxes” (statements) a and b in sequence to make a
larger functional box; Ω is a `while` loop, which repeatedly evaluates
a condition (“box of predicative type”) α until it fails, after each
successful evaluation running a functional box a before the next
evaluation; and Δ is an if-else statement combining a condition α
which determines which of two statements a and b to execute.  At some
point they replace Ω with Φ, the do-while loop.  These are all
single-input, single-output constructs.

We could write these out as netlists:

    Π(a, b):
        start = a.start
        a.end = b.start
        end = b.end

    Ω(α, a):
        start = α.start
        α.yes = a.start
        a.end = α.start
        α.no = end
        
    Δ(α, a, b):
        start = α.start
        α.yes = a.start
        α.no = b.start
        a.end = b.end = end

[0]: https://dl.acm.org/doi/pdf/10.1145/355592.365646 "Flow Diagrams, Turing Machines And Languages With Only Two Formation Rules, CACM vol. 9, no. 5, May 01966"

(Later in the paper, Jacopini comes up with a less practical rewriting
using an infinite stack of Booleans, in which the only conditional is
a while loop testing the top of the stack.)

There are two problems with this.  First, the division into
predicative and functional boxes is somewhat arbitrary; it’s common
to, for example, invoke a subroutine or cache a result as part of
evaluating a condition.  The predicative boxes α in the above netlists
seem like a bit of a *deus ex machina*.  Second, as Jacopini points
out in the paper, you have to duplicate code or introduce boolean
variables in order to reduce things to this form.  As Knuth pointed
out in “[Structured Programming with GO TO Statements][1]”, and
Kernighan pointed out in “[Why Pascal is Not My Favorite Programming
Language][2]” there are well-known and common situations where this
makes the code harder to understand and modify.

[1]: https://dl.acm.org/doi/10.1145/356635.356640
[2]: https://www.cs.virginia.edu/~evans/cs655/readings/bwk-on-pascal.html

COMFY: not/seq/if/while/nop
---------------------------

If we’re going to have single-entry dual-exit nodes in our
control-flow graph, it would be simpler and more regular to just build
everything out of them.  And that’s what [Henry Baker’s COMFY-65,
finally published in 01997][3], does.  Ordinary primitive operations
like assignment or arithmetic always pass control to their ‘yes’ exit.
A minimal set of COMFY control-flow operators:

[3]: https://dl.acm.org/doi/pdf/10.1145/270941.270947

    (not e):
        start = e.start
        yes = e.no
        no = e.yes
        
    (seq e1 e2):
        start = e1.start
        e1.yes = e2.start
        e2.yes = yes
        no = e1.no = e2.no
        
    (if e1 e2 e3):
        start = e1.start
        e1.yes = e2.start
        e1.no = e3.start
        yes = e2.yes = e3.yes
        no = e2.no = e3.no
        
    (while e1 e2):
        start = e1.start
        e1.yes = e2.start
        e2.yes = e1.start
        e2.no = no
        e1.no = yes

Baker also defines an `alt` operator, arbitrary arity for `seq` and
`alt`, a finite repetition operator, and a `loop`, which I don’t think
are so important here; they can be rewritten in terms of `if`, `seq`,
`while`, `not`, and a no-op operator `nop`:

    (seq e1 e2 . es) = (seq e1 (seq e2 . es))
    (alt e1 e2) = (not (seq (not e1) (not e2)))
    (alt e1 e2 . es) = (alt e1 (alt e2 . es))
    (loop e1) = (while nop e1)

### An alternative COMFY minimum set: alt/seq/not/loop ###

Baker’s [earlier COMFY paper][5] from 01976 instead has a set of four
operators: “|” (`alt`, infix), “;” (`seq`, infix), “¬” (`not`,
prefix), and “∞” (`loop`, postfix), plus primitive *succeed* and
*fail* leaves; but he points out that in the general case this is
insufficient for `if`, because you have to evaluate the condition
twice to avoid executing the alternate if the consequent fails.  It
also has the problem that the `if` should fail if the consequent
fails, and it doesn’t.  His “∞” also suffers from that problem; it
must always fail if it is to terminate.

### An alternative COMFY minimum set: if/nop/fail/while ###

I think that we can reduce `seq` to `if`, though, if we have a failing
version of `nop`, which we could call `fail`, because then we have

    (seq e1 e2) = (if e1 e2 fail)

This also allows us to desugar `not`:

    (not e) = (if e fail nop)

And although, as shown above, we can desugar `alt` to `not` and `seq`,
we can also desugar it more simply to an `if` with `nop`:

    (alt e1 e2) = (if e1 nop e2)

So, this gives us an alternative, smaller minimum set; instead of
`if`, `seq`, `while`, `not`, and `nop`, we have `if`, `nop`, `fail`,
and `while`.

[5]: https://dl.acm.org/doi/pdf/10.1145/261353.261356

### An alternative COMFY minimum set: alt/seq/if/while/do with not-elimination ###

Looking at the above, it occurred to me that this rewrite could maybe
yield acceptable code with pretty dumb compilation.

Baker’s scheme for compilation, as I recall it, was to build the
program in memory working down from the top address by having the
compiler recursively call itself.  Each call to the compiler had an
expression, a success-target, and a failure-target.  The targets were
addresses.  The return value is the entry point of the compiled
expression.

    (alt e1 e2) → (if e1 nop e2)

First you compile `e2`; its failure-target is the same as the `if` as
a whole, as it should be, and its success-target is too.  In the usual
case, where that target is the immediately following code, you don’t
want this to emit a jump, but that’s easily handled by special-casing
the jump-emitting routine to emit no instructions in the case of a
jump to the current location.  Then we compile whatever instructions
`e2` compiles to, and then it’s time to compile `nop`, which ought to
emit no instructions, except that its success-target is the
success-target of the `if`, which is *not* the immediately following
location if `e2` generated any instructions.  So we emit an
unconditional jump to the end of the `if`, or whatever its success
callback is.  So far our code looks like this:

    4:  jmp 1f
    2:  (compile e2 1f 3)
    1:

Now we have to compile `e1`.  For concreteness, let’s say `e1` is `r2
== r3`.  Its success-target is the nop code (the jump), and its
failure-target is 2f, the compilation of `e2`.  So we end up like
this:

        bne r2, r3, 2f
    4:  jmp 1f
    2:  (compile e2 1f 3)
    1:

This is not ideal, because it wastes an instruction on negating our
test instruction, but it’s not horrific.  Similar remarks apply to
compiling `seq e1 e2` as `if e1 e2 fail`, which ends up as:

        bne r2, r3, 2f    ; assuming e1 is still r2 == r3, heh
        (compile e2 1f 3)
        jmp 1f
     2: jmp 3
     1:

This is correct but pretty wasteful.  We’re unconditionally jumping
past our unnecessary unconditional jump instruction, inserted to
implement `fail`.

A direct implementation of `seq e1 e2` would instead start with the
same compilation of `e2` and then compile `e1` right before it, so
you’d end up with this, as you should:

        bne r2, r3, 3
        (compile e2 1f 3)
     1:

So I think that probably the right way to do this is to rewrite the
tree top-down to eliminate `not` by pushing it toward the leaves.  For
`alt` and `seq` this is just De Morgan’s laws.  (At this point I’m
suddenly switching to a different notation based on Qfitzah, Prolog,
and Jevko.)

    Not(Alt(e f)) → Seq(Not(e) Not(f))
    Not(Seq(e f)) → Alt(Not(e) Not(f))

For primitive commands, things that cannot fail such as `addi r3, 1`,
negation amounts to adding a jump to the failure handler after the
instruction.  And for jumps — instructions that steal the control
flow, such as a return instruction — it’s simpler still:

    Not(Cmd(x)) → Seq(Cmd(x) Fail)
    Not(Jump(x)) → Jump(x)

For branches, which are where the failure-target actually comes into
play, we need to negate the branch condition; this depends on what the
particular conditions available are, but it might look like this:

    Not(Beq(x y)) → Bne(x y)
    Not(Bne(x y)) → Beq(x y)
    Not(Blt(x y)) → Bge(x y)
    Not(Bge(x y)) → Blt(x y)
    Not(Bltu(x y)) → Bgeu(x y)
    Not(Bgeu(x y)) → Bltu(x y)

We could write `Beq(x y)` instead as `Branch(Eq(x y))` and `Bne(x y)`
as `Branch(Negated(Eq(x y)))`, and similarly for the other cases,
which would allow us to collapse the above to just this:

    Not(Branch(cond)) → Branch(Negated(cond))
    Negated(Negated(cond)) → cond

It’s important here that `cond` is not a program expression; it’s a
Boolean expression, which is not a thing that can appear in the
program except in a `Branch` or a `Negated`.

As Baker points out, you can’t quite reduce `if` to `alt` and `seq`,
so we should make it primitive too:

    Not(If(a b c)) → If(a Not(b) Not(c))

Then all that’s left is loops.  Baker’s `While(e f)` proceeds from `e`
to `f` on success, and then from `f` back to `e` on success,
indefinitely.  If `e` fails, the loop succeeds (a feature missing from
his original 01976 formulation with just the ∞ operator) and if `f`
fails, the loop fails.

So how do we negate that?  We want something which fails if `While(e
f)` succeeds, and succeeds if it fails.  We can almost do this with
`While(f e)`, but that has the problem that it starts by executing
`f`, not `e`.  What we want is something which starts out by executing
a thing which, if it fails, the whole loop fails, and only then
proceeds on to executing a “harmless” loop termination test.

This sounds like C’s `do { } while ()` construct.  If we add this
under the name `do`, we have a very simple way of eliminating
negativity around loops:

    Not(While(e f)) → Do(e f)
    Not(Do(e f)) → While(e f)

If we have primitive `Nop` and `Fail` instructions, as the
above rewriting of `Cmd` suggests, we also have to rewrite those:

    Not(Nop) → Fail
    Not(Fail) → Nop

Listing all these essential `Not` rewrite rules together:

    Not(Nop)          → Fail
    Not(Fail)         → Nop
    Not(While(e f))   → Do(e f)
    Not(Do(e f))      → While(e f)
    Not(Cmd(x))       → Seq(Cmd(x) Fail)
    Not(Jump(x))      → Jump(x)
    Not(Branch(cond)) → Branch(Negated(cond))
    Not(Alt(e f))     → Seq(Not(e) Not(f))
    Not(Seq(e f))     → Alt(Not(e) Not(f))

Applying these rules iteratively will eventually eliminate all
negation from a finite-depth program, except in the sense of negated
conditions.  The patterns should be exhaustive for all occurrences of
`Not` in a program, and the rewrite rules all decrease the depth of
the deepest `Not`-headed subtree in the tree being rewritten by at
least 1.

For example, if we have some tree `Not(While(e f))` in which `e` is of
depth 5 and contains a `Not` of depth 3, and `f` is of depth 6 and
contains a `Not` whose child is a leaf, so it is of depth 2, then the
depth of the `While` is 6, and the deepest `Not`-headed subtree is the
entire tree, of depth 7.  After we rewrite this to `Do(e f)`, the
deepest `Not`-headed subtree is the one down in `e` of depth 3, which
is less than 7.

The rewrite rules for `Alt` and `Seq` only reduce the depth by 1,
while the others may reduce it to -∞ by eliminating the only `Not` in
a subtree.

By rewriting the tree in this way, the compiler never has to emit code
for negation.

Finally, as an optimization, we can eliminate double negation:

    Not(Not(x)) → x

And, since negation of commands introduces extra unconditional jumps,
it might be better to flip if-conditions to avoid negation:

    If(Not(e) f g) → If(e g f)

Ported graph structures for alt/seq/if/while/do
-----------------------------------------------

Let’s consider our program as being made out of a graph which is
labeled not on nodes or on edges but on “ports”: node-edge pairs.
This seems like a natural formalism for things like logic circuits and
program control flow and dataflow graphs, but I don’t think I’ve seen
it described before.  (See file `ported-graphs.md` for more.)  In this
case, each of our nodes has three edges: “go”, where control flow
enters; “yes”, where control flow goes if the node succeeds; and “no”,
where control flow goes if the node fails.  We can define each of our
combining forms as a ported-graph rewrite rule as follows:

    Alt(e f) = {e[no=:x]; f[go=:x]}

This defines a local net `x` and says that we instantiate `e` with its
`no` port connected to `x` and `f` with its `go` port connected to
`x`.  All other ports are “inherited” from the surrounding context; so, if
the ports are `go`, `yes`, and `no`, this is equivalent to the following:

    Alt(e f) = {e[go=go, yes=yes, no=:x]; f[go=:x, yes=yes, no=no]}

The full list of fundamental combining forms is defined as follows:

    Alt(e f) = {e[no=:x]; f[go=:x]}
    Seq(e f) = {e[yes=:x]; f[go=:x]}
    If(e f g) = {e[yes=:x, no=:y]; f[go=:x], g[go=:y]}
    While(e f) = {e[yes=:x, no=yes]; f[go=:x, yes=go]}
    Do(e f) = {e[yes=:x]; f[go=:x, yes=go, no=yes]}

And we can define `Not` in the same way, as [“opposite day, when yes
means no and no means yes”][6]:

> PI [Philosophical Investigations] does away with this logical
> system. In it, Wittgenstein adopts a practice-first approach to
> language, concepts, meaning, grammar, and logic. He begins PI by
> having the reader consider a series of child-like games with
> language, whether it be builders who have only imperatives of
> increasing complexity, or children playing at opposite day, or a
> language consisting only of interrogatives and affirmatives or
> negations (PI §§1-19). Language, Wittgenstein tells us, works as it
> does in these games. How terms mean is a matter of convention, which
> develops in the midst of social practice. Just as children might
> decide that on a given day, “yes” means “no” and “no” means “yes,”
> we have decided that “desk” refers to the large flat thing I am
> currently resting my computer on, and “word” means the meaningful
> mouth-noises I utter and meaningful squiggles I am currently
> producing.

[6]: https://dalspace.library.dal.ca/bitstream/handle/10222/60351/Earl-Kaleb-MA-PHILO-August-2015.pdf?sequence=3&isAllowed=y "'Effing the Ineffable: The Mysticism of Simone Weil and Ludwig Wittgenstein', K G M Earl’s Dalhousie master’s thesis from 02015"

    Not(e) = {e[yes=no, no=yes]}

We can also write Böhm and Jacopini’s structures in this form, which
renders them considerably more compact:

    Π(a b)   = {a[end=:x]; b[start=:x]}
    Ω(α a)   = {α[yes=:x, no=end]; a[start=:x, end=start]}
    Δ(α a b) = {α[yes=:x, no=:y]; a[start=:x]; b[start=:y]}

(In this form they look exactly the same as three of the five COMFY
combining forms; the difference is only that in COMFY there are extra
control-flow edges that aren’t written out explicitly, and there’s
only one kind of term rather than both “functional boxes” and
“predicative boxes”, a distinction also left implicit here.)

In theory you should be able to use the above definitions to show that the
above-stated rewrite rules for these combining forms are correct; for
example:

    Not(Seq(e f)) → Alt(Not(e) Not(f))
    Not(Seq(e f)) = Not({e[yes=:x0]; f[go=:x0]})
                  = {e[yes=:x0]; f[go=:x0]}[yes=no, no=yes]
                  = {e[yes=:x0][yes=no, no=yes]; f[go=:x0][yes=no, no=yes]}
                  = {e[yes=:x0, no=yes]; f[go=:x0, yes=no, no=yes]}
    Alt(Not(e) Not(f)) = Alt({e[yes=no, no=yes]} {f[yes=no, no=yes]})
                       = {e[yes=:x0, no=yes]; f[go=:x0, yes=no, no=yes]}

So we can see that in this case we get the same graph structure in
both cases, so the transformation rule is semantics-preserving.

The analogous demonstrations for the other rewrite rules for combining
forms are as follows, in a somewhat less long-winded form.

    Not(While(e f)) → Do(e f)
    Not(While(e f)) = Not({e[yes=:x0, no=yes]; f[go=:x, yes=go]})
                    = {e[yes=:x0, no=no]; f[go=:x, yes=go, no=yes]}
    Do(e f) = {e[yes=:x]; f[go=:x, yes=go, no=yes]}

These two graphs are the same; `no=no` is redundant.

    Not(Do(e f)) → While(e f)
    Not(Do(e f)) = Not({e[yes=:x]; f[go=:x, yes=go, no=yes]})
                 = {e[yes=:x, no=yes]; f[go=:x, yes=go, no=no]}
    While(e f) = {e[yes=:x, no=yes]; f[go=:x, yes=go]}

Ditto.

    Not(Alt(e f)) → Seq(Not(e) Not(f))
    Not(Alt(e f)) = Not({e[no=:x]; f[go=:x]})
                  = {e[no=:x, yes=no]; f[go=:x, yes=no, no=yes]}
    Seq(Not(e) Not(f)) = Seq({e[yes=no, no=yes]} {f[yes=no, no=yes]})
                       = {e[yes=no, no=:x]; f[go=:x, yes=no, no=yes]}

In Baker’s approach, `yes` and `no` are the success-target and
failure-target passed to the compilation function, and `go` is its
return value, the entry point of the code.  But if you’re generating
relocations that have to get backpatched anyway, there isn't a very
strong reason for that sequence.

Not-elimination and compilation efficiency
------------------------------------------

Although I haven’t tried it, my intuition strongly suggests that this
approach will easily give close to optimal control-flow compilation
for most code.  There are some interesting degrees of freedom to play
with around loops; the simplest ways to compile them will result in an
extra unnecessary unconditional jump, though this can be moved outside
the innermost loop.

Like, you can handle `Compile(While(e f) yes no)` like this:

    1:  Compile(e 2f yes)
    2:  Compile(f 1b no)

This will usually result in an unnecessary extra jump in the inner
loop, because the success-target for f isn’t the immediately following
code, but previous code (which, in Baker’s setup, doesn’t yet have a
known address when you emit the jump, so you have to backpatch it).
To be more concrete, consider:

    int total = 0;
    for (int i = 0; i != n; i++) total += a[i];

The relevant `e` here is `i != n`, and `f` is `total += a[i]; i++`.
So we might end up with a loop something like this (untested RISC-V
asm):

    1:  beq a1, a2, 3f     ; leave loop if i == n
    2:  slli a3, a1, 2     ; convert i to byte offset
        add a3, a3, a4     ; add to base address of array a
        lw a3, 0(a3)       ; load a[i] from memory
        add a0, a0, a3     ; add to total
        addi a1, a1, 1     ; increment i
        j 1b
    3: 

Or you can compile it like this, which still has the jump, but it’s
moved outside the loop, which will often make it run faster:

        jmp 1f
    2:  Compile(f 1f no)
    1:  Compile(Not(e) yes 2b)

In the above example, this would look like this, so the inner loop is
6 instructions instead of 7:

        j 1f
    2:  slli a3, a1, 2     ; convert i to byte offset
        add a3, a3, a4     ; add to base address of array a
        lw a3, 0(a3)       ; load a[i] from memory
        add a0, a0, a3     ; add to total
        addi a1, a1, 1     ; increment i
    1:  bne a1, a2, 2b     ; repeat loop if i != n
    3: 

As an aside, I feel like in this case converting it to pointer
arithmetic would have been a more important improvement, and it’s sort
of embarrassing that GCC didn’t notice that with -Os:

        mv a2, a0          ; free up return value slot moving a to a2
        li a0, 0           ; initialize total to 0
        slli a1, a1, 2     ; convert n to byte offset
        add a1, a1, a2     ; replace n with &a[n]
        j 1f               ; or alternatively beq a2, a1, 3f
    2:  lw a3, 0(a2)       ; load a[i] from memory
        add a0, a0, a3     ; add to total
        addi a2, a2, 4     ; increment &a[i] by sizeof int
    1:  bne a2, a1, 2b     ; repeat loop if &a[i] != &a[n]
    3:  ret

Anyway, though, sometimes I’ve seen GCC do a sneaky thing where it
eliminates that unconditional jump entirely by clever reordering of
basic blocks, though this example doesn’t have enough degrees of
freedom for that.  You can rotate the loop as you please if you have
jump instructions both to enter and to exit; in this example, for
example, you can rotate the top two instructions to the bottom of the
loop if you feel like it:

        j 1f
    2:  lw a3, 0(a3)       ; load a[i] from memory
        add a0, a0, a3     ; add to total
        addi a1, 1         ; increment i
    1:  beq a1, a2, 3f     ; escape loop if i == n
        slli a3, a1, 2     ; convert i to byte offset
        add a3, a3, a4     ; add to base address of array a
        j 2b
    3:

If you have an if-else inside the loop, it generally requires a
conditional jump and an unconditional jump, which are both normally
forward but could instead be backward at no extra cost.  Similarly, if
the loop is itself inside of an if-else, the jump to the start of the
loop can be arranged to jump to anywhere inside of it.  So you may be
able to find an ordering of basic blocks, perhaps taking advantage of
inversion of if-conditions, for which both of the unconditional jumps
in this version of the loop can be elided (the one that enters the
loop and the one that exits it).

Direct manipulation of program fragments
----------------------------------------

The thing I think is interesting is the possibility of a UI where you
manipulate such programs by combining them with these operators to
compose larger programs out of them.  Maybe you’d have two program
fragments on your blackboard, then press the `while` hotkey to combine
them into a loop, or the `alt` hotkey to make them alternatives.

I suspect but do not know that you can represent arbitrary static
control-flow graphs in these terms, as long as no primitive node has
more than two edges leaving it.

Control domination in COMFY and variable lifetimes
--------------------------------------------------

Reading Olin Shivers’s [notes on loop macros for Scheme][4], I was
struck by this paragraph:

> The second property provided by classical lexical scoping, however,
> is a more generally useful one: definitions (or bindings)
> control-dominate uses (or references). That is, in a functional
> language based on the λ-calculus, we are assured that the only way
> program control can reach a variable reference is first to go
> through that variable’s binding site. We have a simple static
> guarantee that program execution will never reference an unbound
> variable. If this seems simple and obvious, note that it is not true
> of assembler or Fortran: one can declare a variable in a Fortran
> program, then branch around the initialising assignment to the
> variable, and proceed to a reference to the variable. Further, if we
> are casting about for a general principle on which to base a
> language design, the simpler and more obvious, the better.

[4]: https://www.ccs.neu.edu/home/shivers/papers/loop.pdf

It occurs to me that since every control-flow node in the COMFY AST is
a single-entry item, it has this same property: an assignment at the
entry to a COMFY AST node is guaranteed to have happened before
anything inside it.  So, if the scope of a variable is a single COMFY
AST node, if it is initialized at its entry point (and deleted at both
exits) we can guarantee that it is not used uninitialized (and freed
on exit).

