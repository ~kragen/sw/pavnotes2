Suppose you have some dirt with iron in it and some electricity, and
you want to do in-situ resource utilization with the iron.  Hematite,
[siderite][j], limonite, and goethite will not react with alkali, but can
be dissolved in strong acids.  So you should be able to get a solution
with iron ions just by heap leaching, first with alkali to get rid of
things that aren't iron, then with acid.  Then you should be able to
electrowin the iron from the acidic solution; see file
`diy-conductive-glue.md` for notes on electrolytically powdering
copper.

[j]: https://ntrs.nasa.gov/api/citations/20120001829/downloads/20120001829.pdf "Dissolution of Olivine, Siderite, and Basalt at 80°c in 0.1 M H₂SO₄ in a Flow Through Process: Insights into Acidic Weathering on Mars. Golden et al.  They got a pH of about 1.3."

Magnetite does dissolve in alkali at higher temperatures, like 300°,
but I don't think it is easily dissolved by alkali at room
temperature.  I do think you can probably dissolve it in acid.

You can even [separate iron from ilmenite][k] with sulfuric acid.

[k]: https://en.wikipedia.org/wiki/Titanium_tetrachloride

Most iron in Earth's crust is locked up in fayalite, which is
generally considered uneconomic to smelt iron from.  However, you can
easily dissolve it despite it being a silicate; it is possible to
oxidize fayalite to magnetite and quartz, and both nitric and sulfuric
acids have been used to leach iron from it.  [Jonckbloedt
reports][1+i]:

> The neutralization is preferably carried out under a nitrogen
> atmosphere, in order to prevent oxidation of the Fe(II).  The
> reaction goes to completion (pH 9-10) even at room temperature. For
> an industrial application, however, the reaction rate has to be
> increased, e.g. by increasing the temperature or decreasing the
> grain size of the olivine (acceptable reaction times, < 10 hours,
> are obtained in the temperature range 70-100°C with olivine grain
> sizes <200 μm). Although it is theoretically possible to neutralize
> the acid completely, the neutralization is stopped at pH 2, firstly
> in order to prevent contamination of the silica with metal
> precipitates, and secondly because the reaction rate falls
> progressively at decreasing H⁺ concentration. The neutralization
> yields a mixture of a magnesium/iron-salt solution, precipitated
> silica, the contaminants originally present in the waste acid and
> unreacted olivine and inert minerals.

[1+i]: https://dspace.library.uu.nl/bitstream/1874/295363/1/Jonckbloedt-RCL-The+dissolution+of+Olivine+in+acid-copy.pdf "The Dissolution of Olivine in Acid: A Cost Effective Process for the Elimination of Waste Acids, 01992 dissertation by R.C.L. Jonckbloedt, p. 16"

They later explain that the acid was 3 M (in chapter 7 they say that
it's not more concentrated to prevent the precipitation of epsom salt)
and that they have only done pilot-scale (20-liter) experiments and
smaller so far.  However, this wasn't a simple heap-leach setup; they
explain (p. 27) that the olivine had to be "stirred continuously" and
"vigorously" to prevent the acid from cementing it into a single solid
mass by precipitating silica.

To separate the iron from the magnesium, Jonckbloedt increases the pH
with MgO and oxidizes the iron further to precipitate it (by bubbling
air through the solution for six days), along with nickel and
manganese.  Other iron ores are also likely to have significant
magnesium substitution, so this is a crucial question.  It might be
possible to just separate them electrolytically; Mg's standard
electrode potential (at neutral pH) is -2.372V, while Fe(II) is just
-0.44V and Fe(III) is just -0.04V.  You might end up precipitating
brucite (Mg(OH)₂) along with the iron, but if so, I'd think mild acids
would have a strong tendency to attack the brucite preferentially.

On Earth felsic rocks like granite typically have very low iron
contents [of about 2-3%][-1] or [0.9-11.0%][-1-i], with the iron
usually in the form of amphibole.  I think amphibole is considerably
more resistant to acid than olivine, being an inosilicate rather than
a nesosilicate.

[Dithionite is used with ligands like citrate to reduce trivalent iron
to divalent][1] and selectively extract that iron from soil for
analysis; interestingly this is done at slightly basic pH.  The
Torstenfelt et al. paper linked above was only able to leach out a
small amount of the total iron in this way, about 0.1% of the weight
of the granite.  Most of the iron in their granite samples was only
available by a procedure that involved dissolving the samples in hot
HF.

[-1]: https://nature.berkeley.edu/classes/eps2/wisc/granite.html
[-1-i]: https://inis.iaea.org/collection/NCLCollectionStore/_Public/14/807/14807560.pdf "Iron content and reducing capacity of granites and bentonite, Torstenfelt, Allard, Johansson, and Ittner, Department of Nuclear Chemistry at Chalmers, 01983-04-15, TR 83-36 from SKBF/KBS (now skb.com, Svensk Kärnbränslehantering AB)"
[1]: https://en.wikipedia.org/wiki/Dithionite#Niche
