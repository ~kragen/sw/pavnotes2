Prismatic joints suck.  Revolute joints have many advantages I won't
get into here, but one big disadvantage is that, while a 3DoF motion
control system made of prismatic joints automatically guarantees nil
relative rotation between tool and workpiece, the degrees of freedom
in a 3DoF motion control system made of revolute joints include curved
paths.  (In a 2-D motion control system like a lathe or a laser
cutter, replace 3DoF with 2DoF.)  There are a few ways to solve this.

Rotation-invariant cutter geometry
----------------------------------

If the cutter is a single-point cutter or cuts a concave or convex
circle, rotation in the plane of that circle doesn't matter; for a
ball-nose endmill or similar spherical cutting tool, no rotation
matters.  Similarly for 2-D cutting processes like laser cutting,
waterjet cutting, and plasma cutting, and for ECM or EDM with a
spherical tool electrode.

Parallel kinematics for parallel motion
---------------------------------------

If your 3DoF (or 2-D 2DoF) system has more than three (respectively
two) revolute joints, some not actuated, it can maintain parallelism
anyway by using parallelogram linkages.  The motion path achieved by
actuating any single joint is still circular, but now the tool doesn't
rotate in the process.

Extra degrees of freedom
------------------------

Similarly, you can add additional *actuated* revolute joints to your
machine which permit you to choose any combination of tool orientation
and position.
