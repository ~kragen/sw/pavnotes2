Here are some things I'm thinking of carrying in my backpack for when
I go places.  I've found that, when traveling, the lighter your pack,
the better.  Mina and I recently spent five days at a nearby resort
town; for the trip I only packed my backpack, which weighed 3.5kg
full.

On the other hand, lacking the right object when you're traveling can
cause pretty bad problems sometimes.

Initial list
------------

So here's a list of somewhere around 64 objects I have or might have
in my backpack, along with their weights and, in some cases, costs.

Most of the costs below are on 02023-05-16, when the dollar was
AR$485.

### 0.5–1g ###

- 500mm paper tape measure: 690mg.  Probably not worth it, but 12
  times lighter than the other one.

Total 0.69g.

### 1–2g ###

- Sewing kit, 1.40g.  This consists of a cardboard business card with
  a large needle through it and a little bit of thread wound around
  it.  The safety pins are separate.
- Shoelace, 1.39g.  This is 800mm long and I actually got it to
  replace the shoelaces in my sneakers, which have rotten and broken.
  Unfortunately it's not long enough.  Rejected.

Total, including rejected, 2.79g.

### 2–4g ###

- Tweezers, 2.91g.  Remove splinters, precisely place electronic
  parts.  Part of a beauty kit bought for AR$800 some time ago; figure
  US$1.
- Sugarcane straw, 2.95g, I think about 80¢.  This is maybe useful
  thanks to the current fad for replacing plastic straws with paper
  ones that fall apart.
- Single-use plastic shopping bag, 230mm × 310mm, 3.26g, or 3.31g in
  another case, or 3.62g in a third.  Possibly protects things from
  water, keeps them organized.  These are the light plastic HDPE bags
  that have been outlawed in supermarkets but are still in use in many
  other businesses; they can hold several liters and two or three kg,
  but tend to get holes in them on first use and, for example, the
  handle broke off just now when I tried to tie one closed.  We can
  estimate that the plastic is about 20μm thick.  I'm not sure if
  these are worthwhile.
- Safety pins, 3.37g.  Connect pieces of cloth together, reopen
  superglue bottles, eject SIMs from cellphones, fix crushed paper
  domes in speaker drivers, etc.  This is ten safety pins connected
  together.  I got these as part of a much larger sewing kit on the
  subte, one including 25 needles, a threader, two large spools of
  thread, a tape measure, a pair of sharp-pointed scissors, and I
  think 48 straight pins for AR$500 (maybe US$1.25 at the time).
- Container of 12 60mm Staedtler 0.3mm 2H mechanical pencil leads,
  2.28g.  Write.  I think these cost about US$2.  The leads
  collectively weigh 140mg, 11.7mg each, the balance being the plastic
  bottle they're contained in.  These are more useful when I have an
  0.3mm mechanical pencil in good order.
- Reclosable heavy plastic bag, 2.4g, about 95mm × 105mm inside, maybe
  150mm outside.  This has a built-in sticky-tape strip that holds it
  shut, though probably not airtight.  We can probably guess that it's
  about 80μm thick.  Plastic bags are really important for keeping
  things organized.  I bought this at Farmacity with two cellulose
  sponges in it.
- Plastic dental flossers.  Remove food from between teeth.  25 units
  for AR$640 = US$1.32, 5.3¢ each.  Bag of 25 weighs 22.22g.  One
  weighs 0.79g.  Two weigh 1.59g.  Extrapolating, empty bag weighs
  2.34g.  Quite thin, about 1mm.  Probably best to carry 5 or so
  (3.95g), not the whole bag.
- Credit-card-sized Fresnel magnifier, 3.46g.  The one I have here has
  a focal length about 200mm, so it's about 5 diopter.  Magnifies
  things (better when it's not all scratched up; a cloth protective
  case would have been useful).  Focuses sunlight: at 200mm the
  9-milliradian image of the sun should be 1.8mm in diameter, and the
  active area of the magnifier is 79mm × 41mm, so in theory the
  concentration factor is almost 1300: 1.3MW/m², but only 3.2 watts.

Total assuming the above-listed quantities: 24.58g.

### 4–8g ###

- Nylon netting bag, 4.26g.  I think I bought this with some oranges
  or onions or something in it.  This is about 500mm circumference and
  200mm deep when fully open, so it can hold about 4 liters, a bit
  less than the plastic shopping bags.  But it's enormously sturdier
  than they are.  Its great virtue, though, is that it lets air pass
  through, so if you put stuff in it and hang it outside or in front
  of a fan, the stuff in the bag will dry out.  This is useful for
  food waste and things.  Unfortunately this bag is sort of
  unraveling; I should get another one.  Approximating as an open cylinder, XXX
- P-38 keychain can opener, 4.50g.  Opens cans, is occasionally
  otherwise useful for hardened steel point.
- Unnecessary sheet of A4-size printer paper, 4.85g.  Typical printer
  paper is 80g/m², which would be 5.00g.  So everything higher up on
  this list weighs less than that, which is good to keep in
  perspective.
- Foam nail files, bag of 5, AR$1060 = US$2.19, each 44¢.  Each 105mm
  × 15 mm × 3.6mm.  Bag weighs 15.69g.  One weighs 2.79g; two weigh
  5.48g; three weigh 8.25g.  Appears to be random manufacturing
  variation rather than scale nonlinearity.  The black abrasive can
  abrade glass but is worn smooth by the test.  Removes rough edges
  from fingernails or metal.  Soft foam can sand around curves.
  Probably best to carry a couple (5.48g) rather than the whole bag.
  Upon further experimentation, these are surfaced with paper (under
  the emery) which falls apart when wet and even when not wet wears
  out too quickly to be useful, so I should probably abandon these in
  favor of a more effective version.
- Pseudo-KN95 dust mask, 5.91g.  Prevents respiratory infection
  contagion; provides some protection from smoke and construction
  dust.
- SUBE card, 6.00g.  Permits the government to track all your public
  transport usage.  Required for public transport.
- Quarter-kg ice cream styrofoam container, 6.72g.  This can keep hot
  food hot or cold food cold for at least an hour or two, but it
  really needs some reinforcement around the outside, like duct tape
  or something, to protect it from puncture or crushing in transit.
  This makes it much heavier.
- Farmacity silicone earplugs, one pair, in case.  7.03g.  Protect
  from loud sounds.  AR$600 = US$1.20.
- Extra USB-C-to-A converter, 7.08g.  I paid US$5.40 for another,
  smaller one of these (see file `micropc-wishlist.md` and below).
- 5mm-diameter crochet hook, 7.10g.  Permits crocheting large objects
  from thick yarn quickly on long trips.  Possible objects include
  cellphone cases, coin purses, cute little animals, socks.  A 5mm
  round anodized aluminum rod of 75mm before flattening out could
  conceivably be useful as a lever or to push on a machine part.  I
  think this cost me about US$1.50.
- Reusable plastic shopping bag, AR$15 = 3.1¢.  270mm × 380mm, 7.15g.
  Protects things from water, keeps them organized.  These are the
  heavy plastic HDPE bags that have been imposed by legislative
  mandate in Buenos Aires supermarkets; they can hold several liters
  and two or three kg.  We can estimate that the plastic is about 30μm
  thick.  It's useful to have several of them; they can be used quite
  a number of times before they break, and you can tie them shut.
  [Similar bags are available in
  bulk](https://articulo.mercadolibre.com.ar/MLA-1124617264-bolsas-camiseta-reforza-verde-negra-reutilizable-45x55-x100u-_JM)
  with the description "Bolsas Camiseta Reforza Verde Negra
  Reutilizable 45x55 X100u" (perhaps these are slightly larger) for
  AR$879 = US$1.81, 1.8¢ each, a bit over half the retail price, from
  Lavadero Blancanieves (among other vendors) who sells the CamiCiudad
  brand.  [Standard sizes
  include](https://www.papelerarivadavia.com.ar/producto/bolsa-camiseta-ciudad-ba/)
  30×40, 40×50, 45×60, 50×50, 50×60, 50×70, and 60×80, which I suppose
  are centimeters.
- BIC Criterium 0.5mm mechanical pencil, 7.82g.  Writes and erases;
  eraser retracts by twisting, and lead support tube slides in
  automatically to protect against impacts.
- Long curved foam nail file, 7.87g.  This has a coarser abrasive than
  the smaller files mentioned above and is 180mm long and 18mm wide.
  AR$460 = 95¢.  May be best to carry this in addition to the small
  ones.  (Or, given the paper nature, discard both.)
- Farmacity q-tips, 5.72g.  This is probably 32 q-tips at 180mg each.
  They are in a cardboard box weighing 11.55g total, though a plastic
  bag would be better (less likely to spill and lighter).
- Sealed sterile envelope of 16 100mm × 100mm gauze squares, 7.77g,
  0.78g per gauze square.  Cover wounds too big for a bandaid, cushion
  feet.  This implies an areal density of 78g/m².

Total assuming the above-listed quantities of each: 95.26g.

### 8–16g ###

- 1500mm fiberglass vinyl tape measure: 8.28g.  This came as part of a
  sewing kit for about US$1.
- Double-ended black permanent marker (Rexon Twin Marker), 9.07g.
  Label objects or packages; fine point makes 0.5-mm-wide line, wide
  point makes 2.5-mm-wide bold black line.  Smells bad but not when
  the caps are on.  Can write on glass; mark is waterproof but can be
  removed with alcohol, or alcohol gel.  I think I paid US$1.50.
- Flexible cloth bandaids (Curitas brand) in individual sealed
  packages, 580mg each.  Cover blisters on feet to permit continued
  walking, cover small cuts to prevent reopening or infection.  Being
  the flexible cloth kind means that they can stay on even in areas of
  skin that experience a lot of flexing, such as hands, which is
  important when you're weight-limited.  These come 10 to a box that
  weighs 3.58g empty for about US$1, so they're about 10¢ each, and
  the whole box weighs about 9.4g.
- Blister pack of 15 200mg modafinil tablets, 9.94g, AR$11617/2 =
  US$11.98.  Helps concentration, memory, and executive function.
  663mg per tablet.
- Laundry soap, 158.55g per 100-mm-long bar.  For use with water in
  cleaning skin, clothes, hair, etc.  My thought is that I can cut off
  a small piece of this soap, around 10g, and take that with me.
- Short USB A-to-micro-B cable, 13.04g, 300mm long.  Permits charging
  portable devices.  This one is shitty and unreliable, but if it
  didn't suck it would be better than a long cable for things like
  charging a phone from my MicroPC or from a powerbank.
- Working TRS speaker cable, to connect the MicroPC to the
  rechargeable speaker, 13.72g.  (See file `micropc-wishlist.md`.)
- Blunt-nose scissors with 38mm blades, 13.75g.  Cut paper, thread,
  plastic bags, aluminum foil, and cloth to desired shape or length,
  without carrying sharp pointed objects.
- WAV brand kleenex, bag of 10, 14.26g.  Blow nose, clean dust from
  things, dry things, emergency substitute for toilet paper.  AR$39 =
  8¢.

Total assuming the above-listed quantities: 101.46g.

## 16–32g ##

- 6 Skyn condoms in wrappers, 16.08g, AR$2500 = US$5.15.  Safer sex.
  Latex-free condoms are less irritating.
- Candela butane cigarette lighter with piezo igniter, 16.63g.  Lights
  fires.  I think this was about 75¢, and it was an enormous comfort
  when we stayed in cabins heated by a wood stove last week.
- Non-wireless earbuds with microphone, 17.86g.  Permit hands-free
  phone conversations, private listening to music or text-to-speech,
  higher-quality audio than phone speaker.  See file
  `micropc-wishlist.md` for more.
- Bamboo Outdoor brand aerosol pepper spray, 19.37g.  Pepper spray is
  essential to de-escalating certain kinds of conflicts while
  minimizing risk of physical harm.  Getting the aerosol was a
  mistake; its effective range is less than a meter, and it cannot be
  used if there is wind.
- Toenail clippers, 19.51g.  Cut fingernails, toenails, thread,
  zipties, shoelaces, wires, cloth, etc., without carrying anything
  sharp.  These are from the same beauty kit as the tweezers, so
  figure US$0.75.  Probably I'll swap these out for lighter clippers,
  like, I have some here that are only 13.26g.
- Magnetic-disconnect USB-A-to-C cloth charging cable, 21.62g,
  US$6.50.  This cable is pretty cool because if you pull on it it
  just smoothly disconnects from the magnet instead of breaking your
  charging port or knocking your computer onto the floor.
  Unfortunately, it's generally too short to leave plugged in while
  you're using the device, only 1040mm.
- For Export Only travel power adaptor, 22.71g.  This is a relatively
  compact (35mm × 38mm × 38mm) 240VAC mains power adaptor that allows
  you to plug in virtually any kind of mains power plug, even one with
  a ground pin, into a standard angled-prong Argentine power outlet
  without a ground pin.  I've mostly used this for my laptop's US
  plug, but it's also useful for the terrible immersion heater.
- Diopter-4 reading glasses.  Magnify nearby objects, protect eyes,
  enable left eye distance vision.  23.51g, though a cloth carrying
  case is necessary and will add a little weight.  These were about
  US$3.  Lower-magnification glasses weigh less, 20 g, because the
  lens is thinner.
- Terrible immersion heater, 24.72g.  This has a nichrome filament
  coiled around a ceramic core, with a slotted plastic case around it
  to prevent electrical contact with nearby metal objects, and yields
  about 1100W when plugged into 240VAC; I just boiled 189g of water in
  60 seconds with it.  This enables you to cook in situations where
  all you have is 240VAC.  I had to re-entangle the nichrome with the
  wires from the flimsy power cord the first time I used it.
- Two nocturnal menstrual pads (Nosotras Nocturna Suave, Flujo
  Abundante); bag of 8 is 90.55g, 12.9g per pad, so two should be
  25.8g.  One of the pads out of its baggie weighs 11.12g, so
  evidently their manufacturing process isn't that consistent.  The
  other 7 (in their baggies) weigh 11.84, 10.86, 11.17, 11.12, 11.73,
  11.55, and 11.16g.  The idea here is not that I'm about to start
  menstruating, or even to have emergency supplies in case someone
  else does, but that these pads contain superabsorbent calcium
  polyacrylate gel, which may be useful in soaking up spills.  After a
  few minutes of soaking the 11.12g pad in water, I squeezed out what
  I could by hand and weighed it at 75.66g, so it has absorbed 65g of
  water, about 6× the weight of the original pad.  The surface is,
  surprisingly, almost dry.  Upon cutting the pad open with scissors,
  it's basically a paper bag of what seems to be calcium polyacrylate
  gel.  This seems worthwhile; I'll carry a couple.  For comparison, 5
  squares of cotton gauze weigh 1.71g; when soaked with water but no
  longer dripping, 10.04g (8.32g water, 4.9× dry weight); and when
  squeezed out, 5.06g (3.35g water, 2.0× dry weight).  So the
  advantage of the superabsorbent material is significant but not
  overwhelming.
- Good USB-A-to-micro-B cable: 23.91g.  This is only 1100mm long but
  the micro-B plug fits snugly even in damaged micro-B ports, allowing
  the device to charge anyway.  I've been leaving it at my house but
  maybe I should leave a different one.
- USB-A-to-micro-B cable, 26.36g.  This cable works okay and is 1300mm
  long.  It doesn't fit as well as the previous one.
- Yee On Wo Embrocation.  30.56g, AR$500 = US$1.20 (some time ago).
  Decongestant inhalant, topical analgesic, perfume, and (in flagrant
  violation of the labeling) soft-drink flavoring.  5mℓ bottle of
  essential oils: oil of wintergreen, menthol, oil of eucalyptus, oil
  of peppermint, oil of lavender, camphor.  5mℓ is roughly the lethal
  dose of several of these; 2–3 drops (0.12mℓ) is the recommended
  dose, so this is about 40 doses.  The bottle without the box and
  prospectus is only 26.30g.  Its flash point is well above room
  temperature, hot enough that visible mist rises and touching the
  surface is instantly painful.  I took this on a recent trip as a
  palliative for a cough, but should probably leave it at home now.

Total 288.58 grams.

### 32–64g ###

- 500mℓ plastic coke bottle, 22.97g.  This can carry 500g of water,
  but right now I'm just using it to protect the sugarcane straw.  (In
  the wrong section, but I don't want to recalculate other things.)
- Cloth speaker cable, US$2.60, 34.68g.  This is to connect the
  MicroPC to the rechargeable speaker without Bluetooth.  It doesn't
  work with the MicroPC.  The other speaker cable mentioned above
  does.  Rejected.
- Tecno Compra FoxBox USB-A charger: 35.80g.  This has two USB-A ports
  and claims to be able to supply 2.4A, which is 12W.  It seems to
  work okay, though one port doesn't work since I stepped on it.  It
  is able to operate the MicroPC.
- Farmacity alcohol gel squeeze bottle on rubber lanyard, gross weight
  38.53g.  Sterilize hands without water, remove permanent-marker mark
  from nonporous surfaces such as glass.  AR$550 = US$1.13.  Net
  wt. 25g.  Pineapple scent; no taste residue.
- US passport, 44.91g.  I try not to take this too many places so I
  can't lose it or get it stolen, but not having it during
  long-distance travel can be a major problem.
- Plastic and cloth hand fan, 49.78g.  210mm radius.  Produces airflow
  in environments that are too hot.  I should probably replace this
  with a bamboo-and-cloth fan, which would be lighter.
- Long red cloth-clad USB-C cable: 51.88g.  2m long.  A gift.  This
  seems to be a high-quality cable; it's what I use for charging both
  the USB-C cellphone and the MicroPC, along with a 3.71g USB-A-to-C
  adaptor.
- Polyurethane wallet, 56.52g.  Holds bills, credit cards, pocket
  Fresnel, etc.  A gift.
  
Total 335.07 g.

### 64–125g ###

- Farmacity baby wipes, resealable bag of 15, 78.45g (5.2g each).
  Clean body without water; contain phenoxyethanol as antiseptic.
  AR$330 = 68¢, 4.5¢ each.  Nauseating perfume.  Rejected; even though
  baby wipes are a huge boost to hygiene when there's inadequate or no
  toilet paper and no way to wash your hands, these aren't it.
- Half-empty resealable bag of 15 Bialcohol wet wipes, 25.66g.  These
  have both phenoxyethanol and benzalkonium chloride as antiseptics,
  and they don't have a nauseating stench like the Farmacity ones.  I
  think there might be 4–5 left and I've been trying to find a
  replacement before I run out.  Presumably a full bag is closer to
  79g.
- Ziploc bag of toilet paper, 88.60g.  This is a truly unreasonable
  amount of toilet paper; each sheet (210mm × 112mm) weighs 470mg, so
  this is presumably on the order of 170 sheets.  I think this was my
  traveling toilet paper supply when I was backpacking around South
  America in 02006.

Total 246.05 g, mostly rejected and the rest hypothetical.

### 125–250g ###

- Ayudín surface-disinfecting wipes, theoretically resealable bag,
  132.58g after removing a wipe, 127.82g after removing a second,
  122.68g after removing a third (5.0g each).  Sterilize body surface
  without water; contain benzalkonium chloride as antiseptic (n-alkyl
  dimethylbenzylammonium chloride, 0.15%, n-alkyl ethylbenzylammonium
  chloride 0.145%).  Bag fails to reseal.  Rejected.
- JBL portable rechargeable Bluetooth speaker, 140.86g (see also file
  `micropc-wishlist.md`).  This has much better sound quality and
  higher volume than either the cellphone or the MicroPC and can run
  for a few hours off its internal battery.  It has a headphone-sized
  jack to plug in wired sound, too, so it can be used without
  Bluetooth.  A gift, so I'm not sure how much it costs.
- Moleskine notebook, A6 size, 192 leaves.  150.13g.  For writing and
  drawing in.  About US$12, I think.
- LG Android cellphone, 173.13g.  Serves as alarm clock, camera, voice
  recorder, music player, connection to cellphone data network, web
  browser when an internet connection is available, GPS, map viewer,
  calculator, general-purpose computing, cryptocurrency wallet,
  shaving mirror, writing implement, flashlight, etc.  A gift.
- New USB-C Android Motorola cellphone, screen 140mm × 70mm, 182.68g.
  Also a gift.  Probably should leave the LG phone at home.  Has
  better sound quality than the LG phone or the MicroPC, especially
  microphone.
- 10050-mAh USB power bank, 204.73g.  Unfortunately this one only has
  about a quarter of its rated capacity; probably its cells need to be
  replaced, but maybe a smaller power bank in better condition would
  be more useful.
- Old Motorola Android cellphone, 230.64g.  Again, should probably
  leave this one at home.

Total 1214.75 grams, mostly rejected.

### 250g+ ###

This Genius LuxeMate 100 USB keyboard (US$10?) weighs 304g, and the
MicroPC weighs 443g, not including its foam carrying case (see file
`micropc-wishlist.md`).  I might be able to find a lighter keyboard.

The backpack itself is about 475g, which is too heavy for my
electronic scale.

I used a chopstick as a balance with my finger as the pivot, hanging
the backpack and the powerbank in two plastic bags.  The backpack hung
60mm±10mm from the balance point; the powerbank 140mm±10mm.  This
suggests that the backpack weighs between 15/5 and 13/7 of the
powerbank, which is to say, 380–620g.  This is not a very good
measurement.

Doing the measurement more carefully, using sewing thread for each of
the two suspension points and for the pivot point, gets a ratio of
142mm±1mm to 62mm±1mm.  Using 208g as the weight for the powerbank
plus the plastic bag it's hanging in, that gives us 466–488g, which is
I think a much more reasonable degree of measurement uncertainty.

The chopstick itself weighs 4.15g and is 240mm long, so there's an
extra gram or two on the long arm, but that doesn't increase the ±11g
measurement uncertainty greatly.

Total, not including the chopstick: 1222g.

Reweighing the backpack later with a more precise kitchen scale, I got
a reading of 490g, so I wasn't quite conservative enough with my
measurement uncertainty.  Also I found a paper bag inside that I
hadn't noticed earlier, which is another 20g of weight I can jettison.

### Overall ###

Everything listed above adds up to about 3530 grams, some of which
goes in my pockets, but there's clearly a substantial amount that can
be easily trimmed, and a few things that should be replicated, such as
shopping bags and ziploc bags.  At least one ziploc bag per cable
would be good.  3530 grams seems like a reasonable weight budget, even
if a smaller amount would be better.  1% is 35.30 grams, so the major
components are something like:

- Backpack itself: 13.5%.
- MicroPC: 12.6%.
- Keyboard: 8.6%.
- Old Motorola cellphone: 6.5%.
- Dilapidated power bank: 5.8%, tentatively rejected.
- New Motorola cellphone: 5.2%.  Note, this is where we cross 50%.
- LG cellphone: 4.9%, tentatively rejected.
- Moleskine: 4.3%.
- Bluetooth speaker: 4.0%.
- Ayudín wet wipes: 3.8%, rejected.
- Toilet paper: 2.5%, mostly rejected.
- Bialcohol wet wipes: 2.2% (if full).
- Farmacity wet wipes: 2.2%, rejected.
- Wallet: 1.6%.
- Good USB-C cable: 1.5%.
- Plastic hand fan: 1.4%.
- Passport: 1.3%.
- Alcohol squeeze bottle: 1.1%.
- USB charger: 1.0%.
- Nonworking speaker cable: 1.0%, rejected.
- Yee On Wo Embrocation: 0.9%, rejected.
- USB A-to-micro-B cable: 0.7%, tentatively rejected.
- Good USB A-to-micro-B cable: 0.7%.
- Two menstrual pads: 0.7%.
- Coke bottle (empty): 0.7%.
- Terrible immersion heater: 0.7%.
- Reading glasses: 0.7%.
- Travel power adaptor: 0.6%.
- Magnetic-disconnect cable: 0.6%.
- Toenail clippers: 0.6%.
- Pepper spray: 0.5%.
- Wired earbuds: 0.5%.
- Cigarette lighter: 0.5%.
- 6 condoms: 0.5%.
- Items under 16g: 6.4%.

(There's a 2.6% discrepancy from, I think, rounding.)

The rejections in the list above work out to something like 16% of the
total, about 560 grams.  That's enough space for something like 3000
q-tips, 1000 sheets of toilet paper, 1500 safety pins, 1000 bandaids,
800 modafinil tablets in blister packs, 700 gauze bandage sheets, 700
dental flossers, 400 kleenex, 200 condoms, 200 small foam nail files,
130 net bags, 120 can openers, 100 sheets of A4 paper, 100 wet wipes,
90 SUBE cards, 80 reusable shopping bags, 80 crochet hooks, 80 pairs
of earplugs, 80 styrofoam containers, 70 tape measures, 70 long nail
files, 70 mechanical pencils, 60 markers, 40 menstrual pads, 40 pairs
of scissors, 40 speaker cables, 40 short micro-B charging cables, 30
cigarette lighters, 30 sets of earbuds, 30 cans of pepper spray, 30
toenail clippers, 25 magnetic-disconnect cables, 25 coke bottles, 25
travel adaptors, 25 pairs of reading glasses, 25 terrible immersion
heaters, 20 micro-B cables, 20 bottles of essential oils, 15 USB
chargers, 14 alcohol gel bottles, 11 hand fans, 10 USB cables, 10
wallets, 4 Bluetooth speakers, 4 Moleskines, 3 cellphones, 3
powerbanks, or 3 bars of laundry soap.

Some of these light items are inherently unreliable or rapidly
consumable, so it might be a good idea to have more extras: USB
chargers, charging cables, immersion heaters, alcohol gel, menstrual
pads, bags, earplugs, mechanical pencils.  In other cases it would be
impossible or pointless.

A lighter-weight backpack, one with a larger number of compartments,
would go a long way.  This one spends a lot of weight on padding and a
metal logo.

Other things I might add
------------------------

- Clothes.  In actual fact I do own clothes and have some in the
  backpack; that's what most of the 3.5kg was on my last trip.

- The scale I've been weighing this stuff on.  That's probably not
  worth the risk nowadays, though.

- A beret, since that's a socially acceptable way to shade my eyes.

- A keychain LED flashlight.

- UHMWPE braided fishing line.  I have a spool of the stuff, but none
  with me.

- Gatorade-type bottle.  This is more versatile than the coke bottle:
  for example, in an emergency, you can pee in it, and you can store
  things in it in a watertight manner that won't fit through the 18mm
  neck of a coke bottle.

- Split rings, like for a keychain.  I do in fact have some on my
  keychain.

- Multicolor pencil, with a sequence of different pencil-lead
  cartridges of different colors.  This only weighs about as much as a
  regular pencil or pen but has multicolor capability.

- Calcium hypochlorite.  This is sold as chlorinating tablets for
  swimming pools and can be used to make water potable in an
  emergency, or to prepare antiseptic.  400ppm chlorine, what we used
  to use in the sanitizing bath in my restaurant days, is 400 mg/kg
  chlorine.  In theory this is 49.6% chlorine by weight, so ideally
  you should only need to add 810mg of tablet per liter of water to
  get a sanitizing solution, though WP says you only get 65–70% of the
  chlorine you'd expect; but you need to get the chlorine out of the
  water if you're going to drink it.  8mg/liter should bring water to
  its normal potable chlorine level, so 5g of tablets should be enough
  for like 500 liters.

- Alum.  To make water potable you need to get it clear first; alum is
  a flocculant that helps a lot with this.  It's also a natural
  antiperspirant and costs about US$5/kg.  10 grams is therefore about
  5¢.

- Salt.  Like alum, it's a water-soluble rock.  Pink Himalayan salt is
  currently a fad, and you can chip a chunk off for use in your food.
  Could ruin things if it gets wet.  I have a chunk here in my house;
  a friend foolishly tried to wash her salt lamp.

- La Gotita, superglue, both for repairs and to close cuts more
  thoroughly than a bandaid can. I have a 3-gram-net-weight tube here
  that weighs 9.42g in its box.  A problem is that if it leaks it can
  ruin a lot of stuff in your backpack in a hurry.

- Duct tape.  I have a roll here that's 46mm wide; 118mm of it weighs
  1.67g, and 157mm of it weighs 2.46g, so it's about 14–16mg/mm or
  310–340g/m².  The roll weighs about 100g, but rather than taking the
  roll, you could just take a flat piece of tape wrapped around itself
  many times.  A budget of 16g would give you almost a meter, 400-some
  cm², enough to solve some problems.

- A better sewing kit.  The thread fell off the business card that
  constituted my "sewing kit".

- Bluetooth earphones.  These are in very widespread use now, and many
  of them are the in-ear type which gives you good bass and some
  attenuation for outside noise.  They're expensive, though.

- Aluminum foil.  This is sometimes very useful for cooking, and it's
  quite cheap and surprisingly light.

- 30× magnifier, for looking at things.

- Sunglasses, for eyestrain.

- Zip-ties, for putting things together.  I have some here at the house.

- Wire saw, for cutting wood; there are keychain "ring saw" versions
  of this sold in camping stores for about US$2.50.

- Space blanket, for keeping people warm when it's cold.  These cost
  about US$4 and weigh 85g (2.4% of the weight budget).

- Masking tape, for labeling things, marking positions in a way that
  can be safely erased, and for fastening things that don't have to be
  strong.  A 167-mm-long piece of this 18-mm-wide roll here weighs
  300mg, so it's about 2mg/mm; 10 grams would be almost 5 meters.

- Pencil eraser, for erasing things.

- Chapstick, for protecting lips and lubricating inner thighs and
  mechanical things.
  
- Bug headnet to keep mosquitoes off my face.

- Tungsten carbide lathe turning insert, for scratching or cutting
  anything softer than steel.  These are normally sold in boxes of 10
  for about US$40 but for example Ditoma will sell you onesies for
  about US$4.

- Flexible plastic pipette, for measuring small volumes of liquids.

- Pocket multimeter.  Unfortunately the commercial ones cost about
  US$100.

- A bar of magnesium, for starting campfires.

- Steel wool, for that and as an abrasive.

- A short micro-USB-B cable that works reliably; 13g of charger cables
  per concurrently charging device wouldn't be too bad, much better
  than 24g or 52g.

- Plasma cigarette lighter, for creating high temperatures and
  providing high voltage.  These cost about US$15–30 and are popular
  among potheads; 3Rayos is a popular brand.

- Paper matches, which don't exist in Argentina.

- A pocket hammock (see file `balloon-chain-spring.md`), which doesn't
  exist anywhere.

- Disposable rubber gloves, for dumpster diving.

Immediate shopping list
-----------------------

Chinatown:

- Bamboo fan.  Also everything below.

Electronics:

- Keychain LED flashlight.
- Short micro-USB-B cables.
- Lighter-weight keyboard.
- Lighter-weight powerbank.

I think I'll see if I can try Todomicro: Villa Urquiza,
Av. Triunvirato 4135 piso 1 of 22, lun-vie 9 a 18hs y sábados 9 a
13hs; Microcentro, Parana 552 piso 4 oficina 42, lun-vie 8 a 18hs y
sábados 9 a 13hs; Chacarita, Leiva 4694, lun-vie 8 a 18hs; V. Gral
Mitre, San Blas 2646, Lun-vie 8 a 18hs y sab 9 a 13hs.

Guessing from the hours, I think the one in Microcentro will have the
largest selection.  It's between Córdoba and Corrientes, in Tribunales
really, a block and a half from Viamonte a bit west of the Obelisk, so
the 140 takes me right there.  But it's too late to go there today.

A different day I did go there, but I arrived late and they were super
closed.  It seems to be a residential apartment building, and office
42 is on floor 5, not floor 4, so I don't know.

Another different day, I went to the Leiva location, which is an
unmarked black door in an industrial district.  I entered and faced
two guys seated at desks.  No merchandise was visible, but they were
happy to fetch it from the storeroom and sell it to me, even
anonymously.

Pharmacy:

- Alcohol gel bottle.
- Bialcohol wet wipes.

Street vendors:

- Beret.
- Another terrible immersion heater.

Art supplies:

- Multicolor pencil.
- 30× magnifier.
- ziploc bags.
- Backup 0.3mm mechanical pencils.

Camping store:

- Wire saw.
- Space blanket.

Ugh, well, I got a fan, but it's sandalwood rather than bamboo and
cloth, and 172mm radius rather than 210, but it does weigh only 27.94
grams.  And then on 02023-05-18 at Universo Aventura I got a better
pepper spray (SABER brand, 32.49 grams, AR$5440 = US$11.20; foreign
exchange is unchanged since yesterday) and a space blanket (Coleman
brand, but made in China: 59.47g, 1346mm × 2083mm, mislabeled as
42.5g, AR$2105 = US$4.34 = US$1.55/m²).

Out of the packaging the space blanket weighs 49.23 g; the rest is a
4.20-gram cardboard advertising insert and a 2.58-gram ziploc bag.
Except now my scale is giving me unreliable readings, argh, and it
says the blanket alone is more like 52.69g, which is maybe correct.
If we assume the blanket is really the dimensions it says it is (I'm
not unfolding it right now to check) then it has a density of
1.88mg/cm² and thus (assuming the aluminum is negligible weight and
mylar is 1g/cc) about 19μm thick.  I don't have the calipers here to
check, but it pinches down to about 5mm thick, and folded up is about
112mm×87mm, which suggests a total volume of about 49mℓ, which would
work out to 17μm thick.

A later test finds that the space blanket does work adequately, but it
is not very opaque, and the aluminization has a lot of visible
striation in it.  It seems like a very low quality product.  Coleman
goes on my shit list.

I also managed to get some sanitizing wet wipes for AR$353 (73¢).
These are Espadol Dettol brand, 10 per bag, and more compact than the Bialcohol
brand.  I used one or more last night.  At this weighing, the bag
weighs 49.12g; upon removing another it weighs 44.16g, so the towel
weighed 4.96 g, and there are probably 8 of them left, plus 4.5g or so
of resealable plastic bag.

I've carved off a 10.66-gram piece of the laundry soap and am putting
it in its own little plastic bag so it doesn't smear all over other
things.

Aha, and here I found a bag of other things I intended to weigh last
night but lost:

- 0.5mg Clonazepam tablet in blister pack: 370mg.
- First aid tape for holding on gauze, unknown length: 1.65g.
- Round 7-day pill carrier: 23.72g.  I thought this was going to be a
  great way to carry around lots of small fragile things because it
  has 7 separate compartments with snap lids, but most things I want
  to put in it are too small, so I think this one gets rejected.  Even
  the 10.66g of soap above won't fit.
- 9 tablets ibuprofen 600mg in blister pack: 8.06g, 900mg each.
- Duct tape, unknown quantity: 14.36g.  This is the same duct tape as
  before, wrapped around itself.  At a rough 325g/m² or 15mg/mm this
  is 960mm or 440cm².  An ultralight backpacking video suggests
  wrapping it around a water bottle or trekking pole instead.
- Smaller fingernail clippers: 11.31g.
- Empty linear weekly pill bottle: 10.59g.  This has compartments that
  are smaller than the round one.

So that's another 79 grams of crap I'd forgotten I was carrying
around.

Oh, and the 500ml or so Powerade bottle replacing the coke bottle
weighs 34g.

I have a couple of quite sturdy Carrefour shopping bags made from
*friselina*.  They are some 450mm × 400mm and 36g, implying an area of
a bit over 0.36m² and an areal density of a bit under 100g/m².
Approximating them as a 900-mm-circumference cylinder with 400mm
depth, they should hold about 26ℓ, which is bigger than the 18ℓ of my
backpack.  Indeed, the loaded backpack stuffs nicely into one of them,
though a bit more tightly than that crude approximation suggests it
should.  [Color Sublime AR][i] sells a 100-pack of similar bags, blank
and ready for dye sublimation, for AR$9900 = US$20.40, 20¢ per bag or
57¢/m².

[i]: https://articulo.mercadolibre.com.ar/MLA-1106386535-100-bolsas-friselina-ecologica-tela-para-sublimar-45x40x10-_JM

I bought a disposable two-bladed razor at a *kiosco* for $220 (45¢)
which weighs 4.33g or, with the plastic blister covering the blades,
5.12g.  After cutting probably a bit too much of the handle (65mm,
leaving 50mm of razor), it weighs 2.61g, 3.38g with the blister.
Carrying two or three of these seems like it would be a good idea,
because they can make the difference between appearing well-groomed
and unhygienic.  This one I used dry, but wet wipes and soap would
probably make the blade last longer and give a better shave.

Further thoughts: packing tape, paper clips, pencil sharpener (2.13g,
AR$50 = 10¢), pencil, bamboo chopsticks (4.66g each).  A pencil
sharpener turns any dry stick into shavings for fire starting or into
a sharp tent peg, without the downsides of carrying a knife.  And a
chopstick is an especially high quality stick for purposes such as
these, as well as extending my grip into places my fingers can't or
shouldn't go.

It would be nice to have an HDPE candle too, but I've never seen one.
Wax candles are the same when they burn but tend to break and smear in
a backpack.

Additional Farmacity trip
-------------------------

The dollar is at AR$480.  I just went to the pharmacy.  This time I
*did* find Bialcohol wet wipes and bought two resealable bags of 15
for AR$427.50 (89¢, 3¢ per wet wipe), which probably total 160g.  I
guess Farmacity stocks them after all.  I almost erred and bought the
Farmacity brand instead.  Note that these are half the price of the
smaller Espadol-brand wipes.

I bought two 38-gram Cofler Block chocolate bars, too, for $320 (67¢,
33¢ each); one weighs 39.18g on the scale, wrapped.  These claim 556
kcal per 100g, 36 wt% fat, 13 wt% protein, 45 wt% carbohydrates, 42
wt% sugar.  This is pretty suboptimal nutritionally from any point of
view but it is easily available, nonperishable, non-spillable, and
much lighter-weight than many possible pack foods.  At 211 kcal for
39g and 33¢ you would need 12 bars totaling US$4 and 470g for the
calories in a day, so it's both practically portable and reasonably
economical.

Meal replacement bars from bodybuilder stores might be a less
nutrutionally horrific stopgap but surely cost more.

A much healthier and denser pack food would be jerky.  Meats cost
about US$2-5/kg at the butcher shop; unrendered fat trimmings are
US$2/kg.  Lean *bife de chorizo* is typically about 6 wt% fat and 30
wt% protein, the other 65% being water and trace elements.  I think
this is typical, so the protein costs US$6-15/kg and the fat US$2/kg.
Jerky that is by weight 45% protein, 45% fat, and the rest water and
salt, would thus cost US$4-9/kg, and 300g would be adequate nutrition
for a day if you could manage to digest it.  Replacing some of the fat
with carbohydrates might be an improvement nutritionally (this is
debated) but would increase the weight and cost.

Still, a week's worth of such pemmican-like jerky is over 2kg.  This
is a substantial amount to carry, but it makes it clear that it is
possible to pack for much longer trips than that.

I also got a short helical USB cable for AR$1400, which Mina likes
very much, and some vulva wet wipes with propylene glycol and herbal
extracts but no surfactants, for AR$1139.50 for 32 wet wipes.  These
may or may not be useful for their intended pupose but they should be
very gentle on skin, mucus membranes, etc.  Probably not good for
cleaning screens.  And another little tie-on bottle of scented alcohol
gel.

