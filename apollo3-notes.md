I got an Apollo3 RedBoard from SparkFun.  These are some notes about
it.

Building with Arduino (not 2! 1.8!)
-----------------------------------

I did get Arduino 2 to work IIRC but it was unbearable.

I did get Arduino 1.8.19 to work.  In File → Preferences I added
`https://raw.githubusercontent.com/sparkfun/Arduino_Apollo3/main/package_sparkfun_apollo3_index.json`
in "Additional Boards Manager URLs".

Compilation is unbearably slow, taking about a minute for tiny "hello
world" sketches, but it does seem to work, at least for the basics
I've tried.  The pins labeled 4 through 10 on the board are, as one
might expect, mapped as pins 4 through 10 by the Arduino libs; the
MOSI pin next to pin 10 is mapped as pin 11.  I haven't tried the
other pins yet.

The compilation says things like this (reformatted):

    /home/user/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-g++ \
        -include /home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/mbed_config.h \
        -include /home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/arduino/sdk/ArduinoSDK.h \
        -iprefix /home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/ \
        @/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/.cxx-flags \
        -MMD -DARDUINO=10819 -DARDUINO_APOLLO3_SFE_ARTEMIS -DARDUINO_ARCH_MBED -DARDUINO_ARCH_APOLLO3 \
        -DMBED_NO_GLOBAL_USING_DIRECTIVE -DCORDIO_ZERO_COPY_HCI \
        @/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/.cxx-symbols \
        -I/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/arduino \
        -I/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS \
        -I/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/arduino \
        -I/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/arduino/mbed-bridge \
        -I/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/arduino/mbed-bridge/core-api \
        -I/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/arduino/mbed-bridge/core-api/api/deprecated \
        @/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/.includes \
        /home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/config/pins.cpp \
        -o /tmp/arduino_build_968771/core/config/pins.cpp.o
    Using precompiled core: /tmp/arduino_cache_549090/core/core_adff3d20b0846c50fdc87e014e0fcdcb.a
    Linking everything together...
    /home/user/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-gcc \
        -T/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/tools/uploaders/svl/0x10000.ld \
        -Wl,-Map,/tmp/arduino_build_968771/keyboard_serial_apollo3.ino.map \
        -o /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.axf \
        /tmp/arduino_build_968771/sketch/keyboard_serial_apollo3.ino.cpp.o \
        /tmp/arduino_build_968771/core/config/pins.cpp.o \
        /tmp/arduino_build_968771/core/variant.cpp.o \
        -Wl,--whole-archive /tmp/arduino_cache_549090/core/core_adff3d20b0846c50fdc87e014e0fcdcb.a \
        -Wl,--no-whole-archive \
        -Wl,--whole-archive /home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/libmbed-os.a \
        -Wl,--no-whole-archive \
        -Wl,--whole-archive \
        /home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/cores/mbed-os/targets/TARGET_Ambiq_Micro/TARGET_Apollo3/sdk/CMSIS/ARM/Lib/ARM/libarm_cortexM4lf_math.a \
        -Wl,--no-whole-archive \
        @/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/.ld-flags \
        --specs=nano.specs \
        --specs=nosys.specs \
        -lsupc++ -lstdc++ -lm \
        -DARDUINO=10819 -DARDUINO_APOLLO3_SFE_ARTEMIS -DARDUINO_ARCH_MBED -DARDUINO_ARCH_APOLLO3 \
        -DMBED_NO_GLOBAL_USING_DIRECTIVE -DCORDIO_ZERO_COPY_HCI \
        @/home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/.ld-symbols
    /home/user/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-objcopy \
        -O binary \
        /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.axf \
        /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.bin
    /home/user/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-objcopy \
        -O ihex \
        /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.axf \
        /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.hex
    /home/user/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-size \
        -A /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.axf
    Sketch uses 116420 bytes (11%) of program storage space. Maximum is 983040 bytes.
    Global variables use 27560 bytes (7%) of dynamic memory, leaving 365656 bytes for local variables. Maximum is 393216 bytes.
    /home/user/.arduino15/packages/SparkFun/hardware/apollo3/2.2.0/tools/uploaders/svl/dist/linux/svl \
        /dev/ttyUSB0 \
        -f /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.bin -b 460800 -v 

It's rather appalling that it uses 116 kilobytes for 44 lines of code,
which I think should be closer to 116 *bytes*.  116 kilobytes wouldn't
even fit on an ATMega328.

The aforrementioned
``SparkFun/hardware/apollo3/2.2.0/variants/SFE_ARTEMIS/mbed/.cxx-flags`
says:

    -c -fno-rtti -std=gnu++14 -DMBED_MINIMAL_PRINTF
    -DMBED_TRAP_ERRORS_ENABLED=1 -Os -fdata-sections -ffunction-sections
    -fmessage-length=0 -fno-exceptions -fomit-frame-pointer
    -funsigned-char -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16
    -mthumb

### Disassembling ###

I can get a sensible-looking disassembly like this:

    arm-linux-gnueabi-objdump -Mforce-thumb -m cortex-m4 -b binary \
        -D /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.bin

This generates about fifty thousand lines of output, which is probably
an unreasonable amount to try to read.
`~/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-objdump`
also seems to work with the same options.

`/tmp/arduino_build_968771/keyboard_serial_apollo3.ino.axf` seems to
be an ELF image, and this produces a more usable disassembly:

    arm-linux-gnueabi-objdump -Mforce-thumb -m cortex-m4 \
        -D /tmp/arduino_build_968771/keyboard_serial_apollo3.ino.axf |
        c++filt

This includes a lot of assembly directives that I might plausibly need
for writing stuff in gas:

            .section        .text.muchacha_paso,"ax",%progbits
            .align  1
            .global muchacha_paso
            .syntax unified
            .thumb
            .thumb_func
            .fpu fpv4-sp-d16
            .type   muchacha_paso, %function

In particular this allows me to find my `setup` function at
line 702.  The original C++, more or less:

    void setup() {
      for (int i = 4; i < 12; i++) pinMode(i, INPUT_PULLUP);

      Serial.begin(9600);
      while (!Serial)
        ;

      Serial.println("Keyboard serial sketch");
    }

The disassembly:

    000106c8 <setup>:
       106c8:       b510            push    {r4, lr}
       106ca:       2404            movs    r4, #4
       106cc:       b2e0            uxtb    r0, r4
       106ce:       2102            movs    r1, #2
       106d0:       3401            adds    r4, #1
       106d2:       f000 f94d       bl      10970 <pinMode>
       106d6:       2c0c            cmp     r4, #12
       106d8:       d1f8            bne.n   106cc <setup+0x4>

Okay, that's simple enough: the loop counter `i` is in r4, and
`INPUT_PULLUP` is I guess 2.  Not sure why `uxtb` instead of just
`mov` to pass the first argument but whatever.

       106da:       f44f 5116       mov.w   r1, #9600       ; 0x2580
       106de:       4804            ldr     r0, [pc, #16]   ; (106f0 <setup+0x28>)
       106e0:       f000 fa13       bl      10b0a <UART::begin(unsigned long)>

I guess the PC-relative load here is to fetch the pointer 0x10001038
or something from address 106f0 below, which is probably the pointer
to the `Serial` object that becomes the this-pointer for UART::begin.

       106e4:       4903            ldr     r1, [pc, #12]   ; (106f4 <setup+0x2c>)
       106e6:       4802            ldr     r0, [pc, #8]    ; (106f0 <setup+0x28>)
       106e8:       e8bd 4010       ldmia.w sp!, {r4, lr}
       106ec:       f000 b8a0       b.w     10830 <arduino::Print::println(char const*)>

That looks like a tail-call to arduino::Print::println(char const\*),
with the addresses at 0x106f4 (0x00027fc7) and 0x106f0 (which I don't know
but maybe it's 0?)

I suspect the base address of the code may be something other than 0,
but if 0x00027fc7 is the address of the string constant, maybe it's
not.

And here we have the spurious disassembly of the constants:

       106f0:       1038            asrs    r0, r7, #32
       106f2:       1000            asrs    r0, r0, #32
       106f4:       7fc7            ldrb    r7, [r0, #31]
       106f6:       0002            movs    r2, r0

    000106f8 <initVariant>:
       106f8:       4770            bx      lr
        ...

    000106fc <_GLOBAL__sub_I_Serial1>:
       106fc:       b513            push    {r0, r1, r4, lr}

Hmm, that `bx lr` thing sure looks more like an instruction you'd
expect to see at the end of a routine than it looks like a constant,
though, and it has a label... maybe it's packing an actual
single-instruction subroutine into the constant pool?

The disassembly around 0x27fc7, where it seems to be passing the
string constant, looks like this:

       27fc6:       4b00            ldr     r3, [pc, #0]    ; (27fc8 <osRtxConfig+0x7c>)
       27fc8:       7965            ldrb    r5, [r4, #5]
       27fca:       6f62            ldr     r2, [r4, #116]  ; 0x74
       27fcc:       7261            strb    r1, [r4, #9]
       27fce:       2064            movs    r0, #100        ; 0x64
       27fd0:       6573            str     r3, [r6, #84]   ; 0x54
       27fd2:       6972            ldr     r2, [r6, #20]
       27fd4:       6c61            ldr     r1, [r4, #68]   ; 0x44
       27fd6:       7320            strb    r0, [r4, #12]
       27fd8:       656b            str     r3, [r5, #84]   ; 0x54
       27fda:       6374            str     r4, [r6, #52]   ; 0x34
       27fdc:       0068            lsls    r0, r5, #1

4b 65 79 62 6f 61 72 64 20 73 65 72 69 61 6c 20 73 6b 65 74 63 68 00.
Sure looks like an ASCII string.  And passing it through `perl -lane
'print map { chr(0 + "0x$_") } @F'` says `Keyboard serial sketch^@`,
so the guess was right, and indeed the code addresses are correct.

A thing I wasn't expecting to see:

    00010a64 <initTimer()>:
       10a64:       4801            ldr     r0, [pc, #4]    ; (10a6c <initTimer()+0x8>)
       10a66:       f000 bb97       b.w     11198 <mbed::TimerBase::start()>
       10a6a:       bf00            nop
       10a6c:       1020            asrs    r0, r4, #32
       10a6e:       1000            asrs    r0, r0, #32

    00010a70 <delay>:
       10a70:       f013 bb2a       b.w     240c8 <rtos::ThisThread::sleep_for(std::chrono::duration<unsigned long, std::ratio<1ll, 1000ll> >)>

Hmm, so the SparkFun kit is using Mbed, which I saw in the command
lines; but does that mean it's actually running under an RTOS?
[Yes,][0] that's what Mbed is.

[0]: https://os.mbed.com/docs/mbed-os/v5.15/apis/thisthread.html

Building with GCC and not Arduino with SparkFun's compiler
----------------------------------------------------------

I compiled a thing but I haven't loaded it on the device so I don't
know if it worked:

    ~/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-gcc \
    -Os -fdata-sections -ffunction-sections -fmessage-length=0 \
    -fomit-frame-pointer -funsigned-char -mcpu=cortex-m4 -mfloat-abi=hard \
    -mfpu=fpv4-sp-d16 -mthumb -c -Wall mpapel-core.c

Then I was able to disassemble it:

    arm-linux-gnueabi-objdump -Mforce-thumb -d mpapel-core.o

However, I am finding that maybe -S -fverbose-asm is more useful:

    ~/.arduino15/packages/SparkFun/tools/arm-none-eabi-gcc/8-2018-q4-major/bin/arm-none-eabi-gcc \
        -Os -fdata-sections -ffunction-sections -fmessage-length=0 \
        -fomit-frame-pointer -funsigned-char -mcpu=cortex-m4 -mfloat-abi=hard \
        -mfpu=fpv4-sp-d16 -mthumb -c -Wall -S -fverbose-asm mpapel-core.c

This produces an mpapel-core.s that is perhaps more readable than the
disassembly.

`ldm` and `stm`
---------------

These instructions offer in theory a doubling in speed of things ilke
memory copies.  But I'm a little bit unclear on the various
permutations of addressing modes they support; as I understand it, the
usual stack operations are equivalent to `ldmfd` and `stmfd`, load
multiple full-descending and store multiple full-descending, with a
pointer update (`!`).  In my disassembly of stuff built with the
SparkFun toolchain, I see:

       10284:       bf08            it      eq
       10286:       ecb0 8a10       vldmiaeq        r0!, {s16-s31}
       1028a:       e8b0 0ff0       ldmia.w r0!, {r4, r5, r6, r7, r8, r9, sl, fp}
       1028e:       f380 8809       msr     PSP, r0
       186c2:       e89b 0003       ldmia.w fp, {r0, r1}
       18700:       e8bd 8ff0       ldmia.w sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
       187e6:       e8bd 81f0       ldmia.w sp!, {r4, r5, r6, r7, r8, pc}
       18960:       e910 0007       ldmdb   r0, {r0, r1, r2}
       18a2e:       e895 000e       ldmia.w r5, {r1, r2, r3}
       18a38:       e8bd 81f0       ldmia.w sp!, {r4, r5, r6, r7, r8, pc}
       18a5c:       e910 0007       ldmdb   r0, {r0, r1, r2}
       1cd28:       e8bd 4008       ldmia.w sp!, {r3, lr}
       1cd46:       e8bd 4010       ldmia.w sp!, {r4, lr}
       1ce04:       cd55            ldmia   r5!, {r0, r2, r4, r6}
       10120:       c6c0            stmia   r6!, {r6, r7}
       1013c:       c6c0            stmia   r6!, {r6, r7}
       101dc:       c70f            stmia   r7!, {r0, r1, r2, r3}
       101e0:       c70f            stmia   r7!, {r0, r1, r2, r3}
       101ea:       c70f            stmia   r7!, {r0, r1, r2, r3}
       101fc:       c71f            stmia   r7!, {r0, r1, r2, r3, r4}
       10208:       c763            stmia   r7!, {r0, r1, r5, r6}
       1023a:       e88c 0003       stmia.w ip, {r0, r1}
       1025e:       e92c 0ff0       stmdb   ip!, {r4, r5, r6, r7, r8, r9, sl, fp}
       10268:       ed2c 8a10       vstmdbeq        ip!, {s16-s31}
       1030c:       e92d 47f0       stmdb   sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
       105f0:       e92d 4ff8       stmdb   sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}

So all of these are either -ia or -db, and both have pointer-updating
and non-pointer-updating versions.  (There's also vstmdbeq and
vldmiaeq which I think are used by the Mbed OS to save and restore
SIMD register context for context switches.)

My understanding is that push and pop are the same as `stmfd sp!` and
`ldmfd sp!`, and the `!` means to update the pointer; conceptually in
the `ldmfd` case the fetch happens before the increment and the
increment is +4, similar to the postincrement or "post-index"
addressing mode `ldr Rd, [sp], #4`, while the `stmfd` case is
precisely opposite: the increment is -4 and the fetch happens after
the increment, as in the preincrement or "auto-indexing" addressing
mode `str Rs, [sp, #-4]!`.  Thus the pointer register is always
pointing at a cell "full" of valid data, not an "empty" cell ready for
writing.  In theory I think the alternatives to `fd` full-descending
are `fa` full-ascending, `ed` empty-descending, and `ea`
empty-ascending.

[I think][11] `stmdb`, `stmia`, `ldmdb`, and `ldmia` [are
synonyms][13] for some of these alternative spellings, at least the
ones with exclamation marks, but I'm not sure which.  I think `db` is
"decrease before" and `ia` is "increase after".  I don't know if the
`da` and `ib` combinations are supported on the Cortex-M4, but I think
the synonyms are:

* `ldmfa`: `ldmda` (not observed)
* `stmfa`: `stmib` (not observed)
* `ldmfd`: `ldmia` (regular sp pop, incrementing after loading from
  full location)
* `stmfd`: `stmdb` (regular sp push, decrementing before storing)
* `ldmea`: `ldmdb` (observed; like a past-the-end array pointer in C
  or C++)
* `stmea`: `stmia` (observed)
* `ldmed`: `ldmib` (not observed)
* `stmed`: `stmda` (not observed)

[11]: https://www.csie.ntu.edu.tw/~cyy/courses/assembly/10fall/lectures/handouts/lec09_ARMisa_4up.pdf
[13]: https://upload.wikimedia.org/wikiversity/en/6/6a/ARM.2ISA.2A.AddrMode.20191108.pdf

Indeed, on attempting to compile a `stmed` instruction, gas says:

    hadixmv.S:119: Error: selected processor does not support `stmed r4!,{r5}' in Thumb mode

Despite the incrementing/decrementing distinction, [the
lowest-numbered registers always go into the lowest memory
locations][12], so the degree to which you can use these instructions
for efficient shuffling is limited.

[12]: https://epgp.inflibnet.ac.in/epgpdata/uploads/epgp_content/S000007CS/P001072/M023295/ET/1505969969Mod29Q1.pdf

On some implementations of ARM large ldm/stm instructions are
uninterruptible and so increase interrupt latency, but the Cortex-M4
doesn't have this problem.

For aligned memcpy() or memmove() you'd normally want to use
`ldmia`/`stmia` or `ldmdb`/`stmdb`, which may be why I see all four of
them.  I think the Cortex-M4 also supports unaligned accesses but at
some runtime penalty?  The [inflibnet course notes][12] linked above
give the following inner loop memcpying a multiple of 48 bytes
(reformatted):

    loop: ldmia r12!, {r0-r11}   @ load 48 bytes
          stmia r13!, {r0-r11}   @ and store them
          cmp r12, r14           @ check for the end
          bne loop               @ and loop until done

