I think the problem with utility-scale energy storage by lifting solid
weights in the day and lowering them at night is not so much the
weights (rocks are pretty cheap) as the capital cost of the
motor-generators used to convert between mechanical and electrical
energy. The rocks do consume a substantial land area.

Suppose we charge up during the day and discharge over four hours in
the evening. 100 MW for 4 hours is 1.44 TJ, and if you have a 10-meter
distance to lift it, you need 15 million tonnes, assuming perfect
efficiency. At 2.5 g/cc a 10-meter-thick disc would be 430 meters in
radius, 860 meters in diameter. Manageable, but awkward.

But the motor-generators need to pay off their capital cost from the
price difference between daytime energy and nighttime energy, despite
only yielding power 4 hours a day. This is surely doable — gas peakers
do it — but not trivial. And they're in competition with other future
forms of circadian energy storage such as thermochemical energy
storage, lithium batteries, sodium batteries, incandescent graphite
PV, vanadium flow batteries, liquid metal batteries, and pumped hydro,
which last can use the same hydroelectric generator that's powered by
snowmelt during the day and thus doesn't suffer from the capital-cost
problem.
