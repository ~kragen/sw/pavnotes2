I remember when I lived in a Volkswagen Vanagon being more satisfied
when I was parked than when I was traveling.  It occurred to me that a
van without a working engine would be a lot cheaper than one with a
working engine, and I wondered how little of an engine I could get
away with.

In particular, you could imagine batteries and an electric motor
that’s powerful enough to move the van down the road by, say, 15km
each day, enough to keep the neighbors from complaining, and enough to
cross the country in a few months.  You could build it to be
especially lightweight in order to be able to get by with wimpier
batteries and motors.

Solar possibilities, and a comparable-sized van
-----------------------------------------------

Is it practical to do that with solar panels on the roof?

Let’s suppose that the van is some 5926mm long, 1993mm wide, and
2820mm tall, like a Mercedes Sprinter 314CDI, which comes with a 107kW
engine and costs sixty thousand dollars new.  Its cargo compartment,
if I’m reading [the Mercedes page][7] right, is only 3272mm long, but
there’s a bit more roof than that.  So you could probably get about
6m² of solar panels on top of a van like that.

[7]: https://www.mercedes-benz.com.ar/vans/models/sprinter/panel-van/overview.html

How much power can you get out of 6m² of solar?  Well, when you have
full sun directly overhead, those are receiving 6kW of sunlight; if
21% efficient, that’s 1260 watts (and about US$130 wholesale of
panels, US$300 retail.)  Suppose your capacity factor (average power
output divided by theoretical peak output) is 10%, which is what
utility-scale solar farms get in Germany, the Netherlands, and China,
and probably pretty reasonable for solar panels that are horizontal
and located someplace random.  (In California they get 29%.)  So your
24-hour average power is 126 watts, 11 megajoules each day (3.0
“kilowatt hours”).

How far will that take you?  [Select Car claims][8]:

> The highest efficiency electric cars today achieve 4 miles per kWh
> or above. For example, both the Tesla Model 3 Standard Range Plus
> [4.56 miles per kWh] and the Fiat 500e [4.54 miles per kWh] enjoy
> excellent battery efficiencies.
>
> A very low efficiency figure would be 2 miles per kWh. Some of the
> early electric cars that came out 8-10 years ago weren’t great at
> converting stored battery energy into actual miles driven.
> 
> Nowadays, most electric cars have a real-world efficiency of at
> least 3 miles per kWh, though a few dip down to around 2.5 miles per
> kWh.

[Redditor Working\_Complaint203 reports][9] “400wh/mile” (2.5 miles
per kWh), while Zealousideal\_Tea9573 argues that 4 miles per kWh is
closer to the truth.  (These sound like spam account names generated
by the same spammer algorithm.)  lellololes argues for a figure of
“250-325 watt hours per mile”, with their lifetime average being 250
(4 miles per kWh).  [The US DOE gives estimates of 25–29kWh/100
miles][10] for different Tesla Model 3 variants, which is also in the
3.4 to 4 miles per kWh range.

[9]: https://old.reddit.com/r/TeslaModel3/comments/14bafsr/how_far_is_1_kwh/
[10]: https://www.fueleconomy.gov/feg/bymodel/2019_Tesla_Model_3.shtml

In SI units, 3 “miles per kWh” would be 1.3 meters per kilojoule, or
less transparently, 1.3 s²/kg/km.  So 11 megajoules works out to 14.8
kilometers, which is in the right ballpark.  Probably a large, heavy
van would get lower efficiencies than a regular car, which can be
lighter and more streamlined; on the other hand, it doesn’t need to
carry as many batteries, and you can drive it more slowly, getting
less wind resistance as a result.

Batteries
---------

If we assume [a specific energy of 0.5MJ/kg for lithium-ion
batteries][11], conservatively within the 0.36–0.875MJ/kg range in
Wikipedia’s table, 11 megajoules is 22 kilograms of battery, very
modest indeed.  Lead-acid batteries with their 0.17MJ/kg would bloat
that to 65kg, and they are still cheaper per energy capacity.
However, lithium-ion batteries have higher power density, and may be
cheaper per power density.

[11]: https://en.wikipedia.org/wiki/Energy_density#Other_release_mechanisms

Suppose we drive our daily 15 km at a leisurely 60 km/hour (17m/s);
then we spend 15 minutes driving, and our 11 megajoules get discharged
at 12 kilowatts, with the batteries discharging at a “4C” discharge
rate.  Almost all lithium-ion batteries can handle that discharge
rate, but not all lead-acid batteries can.  At times during the trip,
we’d want to accelerate and brake at higher powers, so it would be
valuable to have some batteries optimized for higher discharge rates.

At 12 volts, 11 megajoules is 250 amp hours, which probably costs on
the order of US$700, though here in Argentina right now the prices are
through the roof because of the economic crisis.  Generally sealed
lead-acid AGM prices are in the neighborhood of 15kJ/USD, according to
file `motors-and-tools.md`.  It's sort of surprising that the battery
price is so much higher than the panel price.

(In practice, we’d also want to have several times more energy storage
than that, for a variety of reasons: cloudy days, crossing regions
where parking is unsafe, operating refrigerators, etc.)

But is 12kW a reasonable power level?  Or will that struggle to move
the vehicle at all?  Let’s consider the canonical low-powered car, the
Volkswagen Beetle.

The Volkswagen as precedent
---------------------------

In 01961, Wikipedia tells me the Volkswagen Type 1 [Beetle][0]
supposedly had a 36-horsepower (27kW) air-cooled 1200cc gasoline
engine, though an earlier version was only 1100cc.  Elsewhere it tells
me that the 01965 “1200A” Beetle had a 22kW engine, while the 1300cc
Beetle introduced that year was 30kW, and the 1500cc Beetle introduced
in 01967 was 32kW, and in 01971 the 1300cc model was improved to 32kW.
[The article on the engine][1] says the 1200cc model is rated at 23kW
at 3000rpm and 30kW at 3900rpm.  The vehicle weighed 730–950kg, and
had a drag coefficient of 0.48.

[0]: https://en.wikipedia.org/wiki/Volkswagen_Beetle
[1]: https://en.wikipedia.org/wiki/Volkswagen_air-cooled_engine

Different sources give slightly different, but fairly consistent,
maximum speeds for a Beetle:

- [77mph (34m/s) for a 1200 or 1300][2], 80mph for a 1500 or 1600, or
  75mph for a 1300
- [72–74mph for a 30kW 1200][3] or 75mph for a 1300
- [80mph for a 1500][4]
- [85mph (38m/s) for a 1600][5]
- [“80 mph plus” for a 1600][6] producing 60 bhp (45kW), according to
  a Volkswagen brochure from around 01970
- [75mph for a 1300][13]
- [the 1200 was bumped to 72mph top speed in 01961][14], and 78–80 mph
  for the 1300 in 01966

[2]: https://old.reddit.com/r/beetle/comments/17wqykv/whats_a_safe_high_rpm_to_run_a_1300cc_beetle_at/
[3]: https://www.thesamba.com/vw/forum/viewtopic.php?t=731894
[4]: https://www.thesamba.com/vw/forum/viewtopic.php?t=94490
[5]: https://www.thesamba.com/vw/forum/viewtopic.php?t=294958
[6]: https://autocatalogarchive.com/wp-content/uploads/2019/06/VW-Beetle-1970-INT.pdf
[13]: https://www.vwvortex.com/threads/how-fast-is-your-bug.1818907/
[14]: https://www.curbsideclassic.com/curbside-classics-european/curbside-classic-1966-vw-1300-the-best-beetle-of-them-all/

So with 22–32 kW the Volkswagen could go twice our 17m/s target
speed — though, I’m pretty sure, not uphill.

Applying simulated power to our example van
-------------------------------------------

The Mercedes Sprinter 314CDI I was using as an example van above has
[2202kg of curb weight][12], close to three times the weight of the
Beetle.  And it has a higher drag coefficient and a higher cross
section.  You might be able to reduce the weight of the van
significantly by replacing body panels with fiberglass, but I don't
think that will make a huge difference.

So, for this much lower speed, a 12kW *average* power output while
driving is probably about right, maybe a bit low but not too much, but
the peak power needs to reach 40kW or more — both output and, ideally,
regenerative braking input.

40kW applied to 2202kg of weight could raise it at 1.9 meters per
second; on a 12% slope (almost too steep to drive up if it’s gravel)
that would be 15m/s horizontal speed, which is basically our target.
So 40kW is enough power and may even be excessive.

At 17m/s a 2202kg mass has 318kJ of kinetic energy; if the motors were
to output only 12kW all the way up to that max, and we neglect air
resistance during that acceleration, that’s 27 seconds of acceleration
time.  That seems totally reasonable, and cutting it by two thirds
with the higher power seems unnecessarily sporty.  [The Beetle fan
linked above said][14]:

> The 1966 1300 had by far the biggest one-time jump in power, a
> whopping 25% jump, from 40 hp to 50! (34 to 44 DIN/net) Wow; ten
> additional ponies; we couldn’t believe it. That was suddenly good
> for a solid 78-80 mph top speed. And acceleration was dramatically
> improved; 0 – 50 now came in only 14.5 seconds, and 0-60 in
> about 22. By today’s standards, that’s laughable. But it was quite
> close to what a 1960 Falcon did, with twice the engine (2.4 L) and
> almost twice the horsepower (90).

[12]: https://www.ultimatespecs.com/car-specs/Mercedes-Benz/115863/Mercedes-Benz-Sprinter-FWD-L2-314-CDI.html

Electric motors are ridiculously expensive though
-------------------------------------------------

In file `motors-and-tools.md` I found that quadcopter drone motors are
about 5 watts per gram and 7 watts per dollar, requiring an additional
25% cost for the required ESC (electronic speed controller).  This
suggests that 40 kilowatts peak (54 horsepower) would require about
US$5700 of motors and US$1400 of ESCs for a total of US$7100.
Unfortunately, this is vastly out of proportion to the rest of the
build, though it’s still nothing compared to the price of the working
vehicle.  It might be reasonable to shoot for an even lower power than
10kW in order to be able to cut the motor budget.

However, apparently you can get [a Wen angle grinder on Amazon for
US$23 that outputs 520 watts on a dynamometer][15], similar to a
40-amp car alternator.  That’s about 23 watts per dollar, with no ESC
needed because it’s a brushed universal motor, like all the other
angle grinders they tested it against.  At this price, 40kW (77 angle
grinders) would cost US$1700, still high but not ruinously so, and our
original 12kW budget (23 angle grinders) only US$530, which is pretty
reasonable.

[15]: https://www.youtube.com/watch?v=5z47EWoqvJ8 "Torque Test Channel review of Harbor Freight and other corded angle grinders"

At retail, in Canada and other countries, Golden Motor sells a
10-kilowatt brushless motor kit for your boat for [US$974 to
US$1174][16], including liquid cooling.  I think this does *not*
include the “electronic motor controller”, the VFD, which they don’t
call an ESC or VFD, which is US$690. So unfortunately my original
drone-motor estimate seems like about the right price point for
brushless.  A brushless motor should be considerably more reliable
than a brushed universal motor, much less a whole bunch of them.  The
motor is about the size of my head.

[16]: https://goldenmotor.bike/product/48-volt-10kw-bldc-motor-liquid-cooled/

The World Solar Challenge
-------------------------

The [Sunraycer][17] that won the first World Solar Challenge in 01987
was built by Paul MacCready; it weighed 265kg, had a drag coefficient
of 0.125, could go 109kph (30m/s), had 1500 watts of peak solar power,
used a 92%-efficient rare-earth motor.  It finished the 3005-km race
course in 5.2 days; counting only the 8 AM to 5 PM racing hours, this
was an average of 67kph (19m/s), but counting all 5.2 days, it's only
6.6m/s.

The winner of the latest World Solar Challenge, in 02023, was
[Sunswift 7][18], but it seems to be a thing that charges from the
power grid rather than from its own solar cells.  It weighs 680kg, has
a drag coefficient of 0.095, is 2050mm wide, 4990mm long, and 1200mm
tall, has a 38.2kWh battery and 4.4m² of solar cells, and can reach
130kph (36m/s).  Unfortunately I can’t figure out how many km it goes
per day on its solar cells.  On a battery charge it has gone 1000km in
11 hours and 52.08 minutes (23m/s).

[17]: https://en.wikipedia.org/wiki/Sunraycer
[18]: https://en.wikipedia.org/wiki/UNSW_Sunswift

Hybridizing with a portable electric generator
----------------------------------------------

You can buy a portable generator that runs on gas or diesel, for
example from Honda or Lüsqtoff.  At retail here you can get [2500W for
US$360][19], [1800W for US$1700][20], [3000W for US$700][21], or
[2500W for US$280][22].  If you stuck one of those in your camper van,
you could alternate charging the batteries with it for an hour and
driving for 15 minutes, all day long until you ran out of fuel.  Even
a smaller generator than that would be useful.

A diesel generator would be nicer due to using a safer fuel (and being
adaptable to more fuels), but those start at a slightly higher price
point.  And, in some countries, it might open you to suspicion of
using non-taxed farm diesel fuel for a road vehicle.

[19]: https://www.mercadolibre.com.ar/generador-lusqtoff-2500w-grupo-electrogeno-naftero-55hp/p/MLA7174413
[20]: https://articulo.mercadolibre.com.ar/MLA-1374554161-generador-honda-eu22i-monofasico-_JM
[21]: https://www.mercadolibre.com.ar/generador-portatil-honda-ez3000cx-3000w-monofasico-con-tecnologia-avr-220v/p/MLA15958687
[22]: https://www.mercadolibre.com.ar/generador-lusqtoff-2500w-grupo-electrogeno-naftero-55hp/p/MLA7174413
