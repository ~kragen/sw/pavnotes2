<p style="text-align: center; margin: 4em"><i><br/>
⁘⁛⁛⁘<br/>
Kragen Javier Sitaker<br />
Buenos Aires<br />
02022<br />
Public domain work<br />
⁘⁛⁛⁘</i><br /><br /></p>

Pavnotes2 is my notebook from 02022 CE, the third year of the covid
pandemic.  It’s the sequel to [Dercuano],
[Derctuo], and [Dernocua].  It
notes on various different topics: programming, machining,
electronics, digital fabrication, math, physics, economics, history,
and so on.  These are mostly notes I made while I was figuring things
out, so they have a lot of errors in them because of things I
understood incorrectly, and many of them are incomplete.

[Dercuano]: http://canonical.org/~kragen/dercuano
[Derctuo]: http://canonical.org/~kragen/derctuo
[Dernocua]: http://canonical.org/~kragen/dernocua

## Public-domain dedication ##

As far as I’m concerned, everyone is free to redistribute Pavnotes2, in
whole or in part, modified or unmodified, with or without credit; I
waive all rights associated with it to the maximum extent possible
under applicable law.  Where applicable, I abandon its copyright to
the public domain.  I wrote and published Dernocua in Argentina in
02022 (more conventionally called 2022, or 2022 AD).

The exception is [this gorgeous cc-by photo of Hawaii by Pedro
Szekely](51828921499_89bff3e7f9_o.jpg) and the various files derived
from it, which I cannot dedicate to the public domain because I am not
its author; it is licensed by Pedro Szekely under the Creative Commons
Attribution license.