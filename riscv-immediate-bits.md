I was thinking about a RISC-V RV32I bitsliced processor across a bunch
of identical bitslice boards (see file `pick-and-place.md` for notes
on how JLCPCB automated fabrication is cheaper if you make several
identical boards).  Maybe you’d have 16 identical boards each with a
2-bit bitslice of an RV32I processor, say, or 8 boards each with a
4-bit slice.  Each board would have the corresponding slice of the ALU
and of each of the 32 general-purpose registers, and a backplane
(maybe a stack of molex connectors) would tie them together into a
32-bit-wide processor.

The RV32I ALU instructions are +, <, &, |, ^, <<, >>, >>>, and -; in
the [spec][0] these are called respectively addi/add,
slt/slti/sltu/sltiu, and/andi, or/ori, xor/xori, sll/slli, sra/srai,
srl/srli, and sub.  The bitwise ops don’t require any inter-bit
communication, and +, -, and < only require a carry chain, which is
likely to be the critical timing path for the whole system.  Lookahead
carry for 16 bitslices requires two levels of 4-wide fan-in and
fanout, so it’s something like four bus wires between bitslices.

[0]: https://riscv.org/wp-content/uploads/2019/12/riscv-spec-20191213.pdf#page=36

The shifts are potentially more problematic, but you could maybe do
long shifts one bit or one bitslice at a time.  So maybe a 31-bit
shift would require 10 clock cycles or something.

You could think of maybe bitslicing the instruction word too, but
different bits of the instruction word are treated differently, and
they need to be shifted different distances.  And it probably isn’t
reasonable to wait multiple cycles to shift your immediates; you might
as well build a bit-serial processor like SeRV then, effectively
timesharing a single bitslice between all the bit positions.

There’s a table in figure 2.4 of the spec showing where immediate
operand bits come from:

- imm[31] is always inst[31], which is impressive.  (Only R-format
  instructions use the high bit of the instruction for something other
  than the immediate sign bit, and that’s because they have no
  immediates at all.)
- imm[30:20] is either inst[30:20] (U-format) or more copies of
  inst[31].
- imm[19:12] is either inst[19:12] (U-format and J-format) or still
  more copies of inst[31].
- imm[11] can be one of four possibilities: inst[31], constant 0
  (U-format), inst[7] (B-format), or inst[20] (J-format).
- imm[10:5] is either constant 0 (U-format) or inst[30:25].
- imm[4:1] is either constant 0 (U-format), inst[24:21] (I-format and
  J-format), or inst[11:8] (S-format and B-format).
- immm[0] is either constant 0 (U-format and B-format), inst[20]
  (I-format), or inst[7] (S-format).

So there are never more than four possible sources for a given
immediate bit.  But what’s the least number of wires you could use on
the backplane to do all this shifting?

Aside from broadcasting inst[31], the shifts are either 0 (no
backplane connections needed), +4 (imm[11] B), -9 (imm[11] J), -20
(imm[10:5] non-U, imm[4:1] I and J, and imm[0] I), or -7 (imm[4:0] S
and imm[4:1] B).  That’s four separate shifts, two of which are not
only not multiples of 4, they’re not even even.  All of them are
larger than the number of bits being shifted, though, so the simplest
solution is just to broadcast those bits onto the backplane
(inst[31:20] and inst[11:7], 17 bits of the instruction word in all;
the bits inst[19:12] are used as immediate bits in U-format but never
shifted).

You also need at least 6 bits on the backplane for broadcasting the
control signal of which instruction to execute to all the bitslices,
plus probably some way for each bitslice to find out which bitslice it
is so it can decode instructions; if there are only 8 bitslices a
simple approach to this requires 3 bitlines.

So each board has about 40 ALU gates (10 SSI chips), 128 register bits
(16 SSI chips), some line drivers, and some decoding logic; it's
something like 30 or 40 chips.  Each chip is about 15¢, so that's
about US$5 per board, or US$40 for all 8 slices.

I think it doesn’t really make sense to do the control in this
distributed fashion, though.  Instead I think you should do the
instruction decoding in a centralized unit and run bit lines out to
the individual bitslice boards, instead of trying to plumb 17
immediate bits from one to another.
