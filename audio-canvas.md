What would an audio equivalent of `<canvas>` look like?  The Web Audio
API sure isn’t it.

What aspects of `<canvas>` do I mean?
-------------------------------------

The browser `<canvas>` 2D interface is a reasonably easy way to get
interesting, flexible raster graphics on the screen at a fairly high
level.  You have to start with a small amount of worthless
boilerplate, typically something like this:

    const canvas = document.getElementsByTagName('canvas')[0]
        , ctx = canvas.getContext('2d')

Then, you can make calls that correspond fairly directly to things
that people can see and recognize; the antialiased alpha-blending
approach it uses by default makes it easy to get stuff people can
recognize.  Here’s a [simplified somewhat suboptimal example][0] I did
recently:

        ctx.fillStyle = 'black'
        ctx.fillRect(0, 0, ww, hh)

        ctx.globalAlpha = 0.05
        
        for (let i = 0; i < xx.length; i++) {
            ctx.fillStyle = color[i]
            ctx.fillRect(xx[i], yy[i], 2, 2)
        }

[0]: http://canonical.org/~kragen/sw/dev3/fireworks.html

Here’s another one for [drawing an analog clock][2]:

    // θ is measured clockwise from 12:00
    function radialLine(r0, r1, {θ, width, color}) {
        ctx.beginPath()
        ctx.moveTo(cx + Math.sin(θ) * rr * r0,
                   cy - Math.cos(θ) * rr * r0)
        ctx.lineTo(cx + Math.sin(θ) * rr * r1,
                   cy - Math.cos(θ) * rr * r1)
        ctx.lineWidth = width
        ctx.strokeStyle = color
        ctx.stroke()
    }

[2]: http://canonical.org/~kragen/sw/dev3/clock.html

Here’s another one, [drawing ellipses with a recently added
ellipse method][3]:

    ctx.beginPath()
    ctx.ellipse(x, y, minor_radius, major_radius, rotation_radians, 0, 2*Math.PI)
    ctx.fill()

[3]: http://canonical.org/~kragen/sw/dev3/ellipse-hatching.html

Here’s another one in [a fatbits visualization of a graphics
algorithm][4]:

    if (lastFontSize !== fontSize) {
        ctx.font = `${fontSize}px Times-Roman`
        lastFontSize = fontSize
    }

    ctx.fillText(t, px, py + lineCentering)


[4]: http://canonical.org/~kragen/sw/dev3/circlegrid.html

Here’s another one in [a visualization of an image filtering
algorithm][5]:

    const im = ctx.createImageData(canvas.width, canvas.height);
    // ...

    for (let i = 0; i < im.width * im.height * 4; i += 4) {
      const pix = thing3[i/4];
      im.data[i+2] = im.data[i+1] = im.data[i] = (pix % scale) * 256 / scale;
      im.data[i+3] = 255;
    }
    ctx.putImageData(im, 0, 0);

[5]: http://canonical.org/~kragen/sw/dev3/sumtableblur.html

Here’s another example from [a simple fixed-width pixel font text
renderer][6] copying rectangles from an existing image file:


    var fontIndex = cc - startCharCode
      , col       = fontIndex % charsPerLine
      , line      = Math.floor(fontIndex / charsPerLine)

    cx.drawImage( font
                , col * width, line * height, width, height
                , xx,          yy,            width, height
                )

    xx += width

[6]: http://canonical.org/~kragen/sw/dofonts.html

These six examples show six key aspects of `<canvas>` 2D:

1. It’s simple to fill the whole screen with a color, only taking a
   couple of (additional) lines of fairly readable code.
2. It’s simple to alpha-blend your drawing operations, being a
   universally available operation.
3. It’s simple to draw dots, lines, axis-aligned rectangles, ellipses,
   polygons (not shown), and other shapes in whatever color you want,
   mostly requiring only one to four lines of code.
4. It’s simple to draw text in a given font at a given position,
   requiring a couple of lines of code.
5. It’s simple to draw arbitrary pixel data computed in your code,
   requiring about four lines of code.
6. It’s simple to draw by copying pixel data from a stored raster
   image (sample), requiring about three lines of code.

We can generalize and say that `<canvas>` 2D provides a variety of
drawing operations that are fairly well aligned with the human visual
system and produce easily predictable visual effects, as well as
escapes from that paradigm when tighter control is needed.

There are some other important aspects that are not obvious from
looking at the code.  It’s reliably enough performant to use for
animations with thousands of moving objects.  It’s capable of good
visual quality — if you look at the rendered web pages, you’ll see
that some of them contain barely adequate graphics, while others
contain very nice graphics.  The gradual evolution from one to the
other is also a key aspect of what `<canvas>` 2D provides.

There are significant shortcomings in `<canvas>` 2D:

- Too many things in the API are specified with strings.
- It’s significantly more verbose than SVG or PostScript.
- The alpha-blending operation doesn’t use a perceptual color space,
  giving rise to artifacts.
- You can’t turn off the antialiasing, which is a problem in some
  situations, especially because you can only fill a single path at a
  time, leading to “cracks” when adjacent regions should have
  different colors.
- Your graphics go through two layers of resampling before hitting the
  screen, and they’re obligatorily bilinear, which is often a pretty
  poor compromise choice.
- There’s no way to apply filters such as sharpening or HDR
  compression to your drawing output.

And so on.  But what it gets wrong is less important than what it gets
right.

What would an audio equivalent of `<canvas>` 2D look like?
----------------------------------------------------------

Well, it almost certainly needs the ability to create filters, even
though `<canvas>` 2D doesn’t have them.  (It *does* have transforms,
which are sort of a limited geometric sort of filtering.)  Signal
filtering is important for images, but it’s a lot more important for
sounds.

It might be worthwhile to look at the facilities provided by music
systems that are popular for [livecoding][23], like [LSDJ][24], James
McCartney’s [SuperCollider][20], Miller Puckette’s [Pure Data][17],
viznut’s [IBNIZ][16], [ChucK][15], and maybe even Barry Vercoe’s
[CSound][18].  Several popular livecoding environments are just
syntactic sugar for SuperCollider, including [FoxDot][21] (Python),
[Sonic Pi][19] (Ruby) and [TidalCycles][22] (Haskell), so it might be
worthwhile to look especially hard at SC.

[15]: https://chuck.cs.princeton.edu/
[16]: http://viznut.fi/ibniz/
[17]: https://puredata.info/
[18]: https://csound.com/
[19]: https://sonic-pi.net/
[20]: https://supercollider.github.io/
[21]: https://foxdot681713046.wordpress.com/
[22]: https://tidalcycles.org/
[23]: https://livecodingbook.toplap.org/
[24]: https://www.littlesounddj.com/lsd/index.php

### Things an audio equivalent of `<canvas>` 2D is *not* ###

It’s *not* MIDI or MML.  MIDI and MML are the audio equivalent of
ASCII art or Hershey fonts; you don’t know what soundfont they will be
rendered in, and there are a lot of things they simply can’t express
at all: an explosion, varying the parameters of a state-variable
filter over time so it switches from low-pass to bandpass like deep
house music, speech, guitar pedal effects, etc.

It’s *not* just playing a stream of raw PCM audio samples generated by
your code.  That’s `<canvas>` 2D’s `.putImageData`, which is its
escape hatch to low-level pixel-by-pixel computations.  But you do
still need that escape hatch.

It’s *not* something like the [Nintendo APU][7], with white noise, a
variable-duty-cycle square wave, and a triangle wave, plus the ability
to feed it samples and mix multiple channels.  These individual
capabilities are important, but alone they are too limiting; Nintendo
music always sounds like Nintendo music.

[7]: https://www.nesdev.org/wiki/APU

It’s *not* just playing back pre-stored samples at different speeds,
though .MOD tracker files show that this is sufficient to get pretty
far.  That approach is simultaneously too low level, like playing a
stream of raw PCM samples, and not expressive enough, like modulating
noise and triangle waves.

It’s not just speech synthesis, either, because, like MIDI, that’s not
expressive enough.

It’s not the Web Audio API, which makes it impractically difficult and
inefficient to implement your own custom processing nodes or to
produce a stream of raw PCM samples, except through a deprecated API,
and which itself is painfully low-level for direct use, though in a
different way from producing raw PCM samples.

### A signal flow tree with `add_child`, `add_filter`, and `remove_child`? ###

But you need something that makes it pretty easy to add these
different audio elements to an “audio canvas”: synthesized speech,
prerecorded samples (at various sample speeds), synthesized musical
notes (with various canned instruments, or your own), melodies (made
out of the above), raw PCM audio samples you compute on the fly,
reverb applied to all or some part of the soundscape, and
so on.

My intuition is that you want a dynamically changing tree-structured
signal flow graph, similar to what you’d see on a Reactable.  The
tree’s internal nodes are “mixers” or “filters”, its root is the
actual sound output, and its leaves are mostly sources of audio data,
such as notes.  You can modify the tree in three ways:

1. Deleting a node; for example, a musical note deletes itself when it
   ends, and you can also end it prematurely.  If the node has
   children, they become children of its parent node.
2. Adding a child node; for example, adding a submixer to a mixer, or
   adding a musical note to a mixer, or adding a melody to a mixer.
   (The melody will then proceed to add further notes to its own
   parent.)
3. Adding a filter, which becomes the new parent node of the node
   where you add it; for example, adding a low-pass filter, a clipping
   saturation effect, a reverb effect, or a volume control, or a pan
   effect to shift a sound toward the left or right speaker.
   Alternatively you could make these just be attributes of a mixer
   node, but then the mixer node has to know about all possible
   filters.  On the other hand, the possibility that someone could
   delete a mixer-node child of the filter node means that the filter
   nodes have to include the mixer functionality of adding together
   multiple children.

The interface between parents and children might be something as
simple as this:

- parent periodically calls `child.render_audio(start_time, end_time,
  parent_proxy)` on each child;
- child responds by rendering its own children and/or calling
  `parent_proxy.play_samples(start_time, sample_buf, sample_rate,
  denominator)` zero or more times, or possibly
  `parent_proxy.add_child(new_child)` or
  `parent_proxy.remove_child(self)` or something.  Which
  `parent_proxy` it called the `play_samples` method on tells the
  parent which child the samples are coming from.

(With that interface, you can get Reactable-like visualizations by
interposing an additional layer proxy membrane.)

This is simpler than a traditional modular synthesizer because all the
signal paths between nodes are carrying actual audio data.  None of
them are carrying LFO-type control signals.  Simpler still might be to
fob off whatever sample rate conversion is needed to the leaf nodes by
requiring the whole signal graph to run at a single sample rate; this
makes the leaf nodes slightly more complex to implement, but if they
need to resample audio data to a different sample rate, they can just
call a library function to do it.  Many won’t.

The interface might not be sufficient for proper mixing, so you might
want to tweak it a bit.  And a more efficient implementation would
avoid having to amortize the numerous dynamically-dispatched method
calls by doing runtime code generation whenever the signal graph
changes.

### `.connect()` is a more parsimonious interface ###

The Web Audio API’s take on this same directed acyclic signal graph
approach is backwards from this: each (output of an) AudioNode is
`.connect`ed to either no destinations or one destination, which is
another AudioNode.  (It returns that node for convenient method
chaining.)  One difference in the single-output case is that you call
this method on the child node, passing the parent as an argument.  The
perhaps more important difference is that `.add_filter` and
`.add_child` are both implemented by the same `.connect` method; you
just have to call it twice for the filter case.  This seems like a
worthwhile improvement in parsimony.  It also eliminates some
aliasing/mutability problems with the semantics of `.add_filter`
(*e.g.*, if I have a node `x` and say `x.add_filter(y);
z.add_child(x);` is the filter `y` in the signal path or not?)

(The `.connect()` interface doesn’t allow you to add a new filter to
your actual physical output node!  To preserve that ability, you have
to make sure to instantiate a mixer node and connect everything to the
mixer, not to the real physical output.)

Then it’s just a matter of creating the right types of leaf nodes and
filters, like speech synthesizers, sine waves, noise sources, FM
synthesizers, melody sequencers, attenuators (“gain nodes”),
state-variable filters, biquad filters, and so on.

### A proposed example ###

This should make it possible to write code like this, using [Microsoft
BASIC MML][8]:

    song = play_mml("L8 CC  F4 FG AFAB-  >C4 >CB- A4 GF", piano);
    song.connect(tremolo({dB: -3, Hz: 15})).connect(output);
    beat = rock_beat(song.ticks, kick_drum, snare_drum, high_hat_closed);
    beat.connect(gain_dB(-6)).connect(output);

[8]: https://en.wikipedia.org/wiki/Music_Macro_Language#Modern_MML

This short example brings out a welter of problems:

- What if you want to add another track with the same beat but, say, a
  different instrument?  Maybe `play_mml` takes an optional `ticks`
  parameter?
- Relatedly, how does `play_mml` know when to start?
- What does the interface to instruments like `piano` and `kick_drum`
  look like?  Presumably at least `piano` can be asked for notes of
  many different pitches; can you then later mute one of those notes?
  If not, it isn’t going to be much good for the simplest application
  of all, playing notes interactively.  Above I suggested each note
  was a separate graph node; if so, you could call methods on
  particular notes to mute them (softly, without a click).
- If the instrument interface is well-defined so that you can plug
  different instruments into things like `play_mml`, what interesting
  functions can we define over that interface?  The most obvious one
  is a “composite instrument” that maps different pitches to different
  “base instruments”; two examples are the common synth mapping of one
  piano key to each percussion instrument, and the common
  sampling-synthesis technique of using a different high-quality
  sample not for each key but for each octave.
- Tremolo is easy, but how do you add vibrato?  I guess you could
  just resample the raw PCM signal to a wibbly-wobbly timescale?
- Are we just talking about a library of node types for the Web Audio
  API, or something potentially fundamentally different?

In a sense, `play_mml` is producing a “score” signal that flows to the
instrument object to produce one or more streams of samples that then
flow to the mixer.  The Reactable visualizes this and makes it
manipulable, but here that “signal” is a second-class entity.  A
really interesting effect you could apply to this “signal” is [Tristan
Jehan's][9] [“The Swinger” from 02010][14], [one of the code
samples][11] (or really [two of them][12]) for the Echo Nest
[Remix][10] code base, which provides [a lot of audio processing
horsepower][13].  As Paul Lamere explains:

> One of my favorite hacks at last weekend’s Music Hack Day is
> Tristan’s Swinger.  The Swinger is a bit of python code that takes
> any song and makes it swing.  It does this be taking each beat and
> time-stretching the first half of each beat while time-shrinking the
> second half.  It has quite a magical effect.

[9]: https://web.media.mit.edu/~tristan/
[14]: https://musicmachinery.com/2010/05/21/the-swinger/
[10]: https://web.archive.org/web/20100201151826/http://lindsay.at/work/remix/overview.html
[11]: https://github.com/echonest/remix/tree/master/src/echonest/remix
[12]: https://github.com/echonest/remix/blob/master/examples/swinger/swinger.py
[13]: https://web.archive.org/web/20100201151826/http://lindsay.at/work/remix/overview.html

Doing this on a raw PCM audio signal required deep magic to avoid
distorting the pitch, but doing it on a “score” should be trivial:
just adjust the note start times and durations slightly.  `play_mml`
must provide those to the “instrument” object somehow, and maybe it
provides them in turn to the mixer, so it should be trivial to do this
kind of transformation at this “score” level.

### Mixer companding ###

There are four somewhat incompatible desiderata for mixers:

1. A single sample or oscillator playing by itself should be able to
   use the full volume of the output sound device.
2. Multiple sound sources playing simultaneously should never clip.
3. There should not be clicks or pops when adding or removing sound
   sources.
4. Adding a new sound source containing silence shouldn’t attenuate
   ongoing sounds.

You can get all four of these by compressing the dynamic range of your
audio output (companding), but that’s often considered another
undesirable artifact.  I don’t know how existing systems handle this.

This becomes a much more urgent issue if each note in a score is, as I
propose above, a separate stream of samples flowing to the mixer.

### SuperCollider examples ###

I don’t know much about how SuperCollider works, [despite the
excellent CC-licensed tutorial][41], except that it has a somewhat
Smalltalky syntax and most of the values computed are "signals", sort
of functions of time; but this [set of 140-character audio
compositions on supercollider.github.io][25] licensed under CC
BY-NC-SA 3.0 ought to give some of its flavor:

    Nathaniel Virgo:
    {LocalOut.ar(a=CombN.ar(BPF.ar(LocalIn.ar(2)*7.5+Saw.ar([32,33],0.2),2**LFNoise0.kr(4/3,4)*300,0.1).distort,2,2,40));a}.play
    LFSaw:
    {Splay.ar(Ringz.ar(Impulse.ar([2, 1, 4], [0.1, 0.11, 0.12]), [0.1, 0.1, 0.5])) * EnvGen.kr(Env([1, 1, 0], [120, 10]), doneAction: 2)}.play
    Tim Walters:
    play{({|k|({|i|y=SinOsc;y.ar(i*k*k,y.ar(i*k**i/[4,5])*Decay.kr(Dust.kr(1/4**i),y.ar(0.1)+1*k+i,k*999))}!8).product}!16).sum}//#supercollider
    Nathaniel Virgo:
    b=Buffer.read(s,"sounds/a11wlk01.wav");play{t=Impulse.kr(5);PlayBuf.ar(1,b,1,t,Demand.kr(t,0,Dseq(1e3*[103,41,162,15,141,52,124,190],4)))!2}
    Batuhan Bozkurt:
    play{f=LocalIn.ar(2).tanh;k=Latch.kr(f[0].abs,Impulse.kr(1/4));LocalOut.ar(f+CombC.ar(Blip.ar([4,6],100*k+50,0.9),1,k*0.3,50*f));f}//44.1kHz
    Batuhan Bozkurt (refactored by Charles Celeste Hutchins):
    f={|t|Pbind(\note,Pseq([-1,1,6,8,9,1,-1,8,6,1,9,8]+5,319),\dur,t)};Ptpar([0,f.(1/6),12,f.(0.1672)],1).play//#supercollider reich RT @earslap
    Thor Magnusson:
    play{x=SinOsc;y=LFNoise0;a=y.ar(8);(x.ar(Pulse.ar(1)*24)+x.ar(90+(a*90))+MoogFF.ar(Saw.ar(y.ar(4,333,666)),a*XLine.ar(1,39,99,99,0,2)))!2/3}
    Charlie Hoistman:
    Ptpar(({|i|[i*8,Pbind(\scale,[0,2,4,7,9],\degree,Pseq(32.fib.fold(0,10),4)+(2*i+i)-10,\dur,1+2**i%2/6)]}!4).flat).play // #supercollider
    MCLD:
    {LocalOut.ar(a=DynKlank.ar(`[LocalIn.ar.clip2(LFPulse.kr([1,2,1/8]).sum/2)**100*100],Impulse.ar(10)));HPF.ar(a).clip2}.play//
    Julian Rohrhuber:
    /*eclecticity*/ Ndef(\x, { SinOsc.ar(BrownNoise.ar(30!2, 200), Ndef(\x).ar * LFNoise1.kr(1!2,1,1)) }).play;
    Micromoog:
    play{VarSaw.ar((Hasher.ar(Latch.ar(SinOsc.ar((1..4)!2),Impulse.ar([5/2,5])))*300+300).round(60),0,LFNoise2.ar(2,1/3,1/2))/5}//#supercollider
    Jose Padovani:
    play{x=165;b=SinOsc;p=Trig.ar(Saw.ar(x),1);y=b.ar(p*x);z=b.ar(p);(GVerb.ar(GrainIn.ar(2,y,y/2,z,p*z,-1),9))/9}//basso gettato #SuperCollider
    Batuhan Bozkurt:
    play{LeakDC.ar(BRF.ar(Saw.ar(8,Decay2.kr(x=Duty.kr(1/8,0,Drand([0,Drand((0.4,0.5..1))],inf)),0.01,0.3))**1.5,x*20+[45.1,45],0.1)).tanh}//#sc
    Nathaniel Virgo:
    Ndef('x',{x=Ndef('x').ar+0.01;a=BPF.ar(x,6**Latch.ar(x,Dust.ar(x))*200,0.1).sin;9.do{a=AllpassN.ar(a,0.2,{0.2.rand}!2,9)};a+a.mean}).play;
    Jason Dixon:
    {x=Array.fill(5,{[0.00001,0.03].asSpec.map(LFNoise2.kr(3))});Splay.ar(Friction.ar(LFTri.ar(50),friction:x,mass:x*30000))}.play
    Batuhan Bozkurt:
    play{AllpassC.ar(SinOsc.ar(55).tanh,0.4,TExpRand.ar(2e-4, 0.4,Impulse.ar(8)).round([2e-3,4e-3]),2)};// #supercollider with bass please...
    redFrik:
    {RHPF.ar(GbmanN.ar([2300,1150]),LFSaw.ar(Pulse.ar(4,[1,2]/8,1,LFPulse.ar(1/8)/5+1))+2)}.play //punk (loud!)
    Nathaniel Virgo:
    play{p=PinkNoise.ar(1!2);BRF.ar(p+Blip.ar(p+2,400),150,2,0.1)+LPF.ar(FreeVerb2.ar(*LPF.ar(p+0.2*Dust.ar(0.1),60)++[1,1,0.2,1e4]).tanh,2000)}
    MCLD:
    {a=[0.02,0.1,1,2,3,4]; k=LFPar.kr(a+0.5).sum; f=Latch.kr(k,Impulse.kr(a)); Splay.ar(SinOsc.ar(f*100+300)/5)}.play // #supercollider
    Sciss:
    play{2.collect{RecordBuf.ar(Limiter.ar(HPF.ar(Convolution2.ar(k=Crackle.ar(l=Line.kr(1,2,90)),b=LocalBuf(2048),Dust.kr(4)),8)+k)*(2-l),b)}}
    Andrea Valle:
    {13.do{|i|k="SuperCollider"[i].ascii;20.do{|u|{MoogFF.ar(Saw.ar((k/4).midicps)*EnvGen.ar(Env.perc),u+k*9,k/30)}.play;(k*0.001).wait}}}.fork
    MCLD:
    play{a=Duty.kr(0.1,0,Dseq(fib(32).wrap(20,55).midicps,inf));HPF.ar(LeakDC.ar(Splay.ar(LFCub.ar([-1,a,a/3,-2])))*9).clip2/9};//#supercollider

Some of the primitives used:

- [{}][28]: defines an anonymous Function; parameters go in `||` (or
  following the keyword `arg`)
- [`.play`][34]: “Play a Synth from UGens returned by the
  function. The function arguments become controls that can be set
  afterwards.”  `{foo}.play` is exactly equivalent to `play{foo}`.
- [[]][29]: defines an array
- [=][30]: assignment
- \\: introduces a Symbol
- [!][43]: generates an array of the size of its right argument,
  filling it with the left argument, which is called repeatedly if
  it’s a Function
- [.ar, .kr][27]: classmethods on UGens.  “This waveform loops,
  creating the output signal. 'ar' means make the instance audio
  rate. SuperCollider calculates audio in groups of samples, called
  blocks [default [64 samples][40]]. Audio rate means that the UGen
  will calculate a value for each sample in the block. There's another
  method, 'kr', which means control rate. This means calculate a
  single value for each block of samples. This can save a lot of
  computing power, and is fine for (you guessed it) signals which
  control other UGens, but it's not fine enough detail for
  synthesizing audio signals.”
- [SinOsc(freq, phase, mul, add)][31]: a class for a `freq` Hz sine
  wave with `phase` radians offset with amplitude `mul` and dc
  component `add`.  
- [HPF(in, freq, mul, add)][37]: a second-order high-pass filter with
  cutoff frequency `freq` applied to `in`, which is normally a UGen.
- [Decay(in, decayTime, mul, add)][38]: an exponential decay filter
  with 60dB decay time `decayTime`.
- [Dust(density, mul, add)][39]: “Generates random impulses [with
  average number of impulses per second `density`] from 0 to +1.”
- [Pbind][42]: “combine several value patterns to one event stream by
  binding keys to values”, like, to combine parallel streams of
  \\scale, \\note or \\degree or \\freq, and \\dur.  A “pattern” is
  what I was calling a “score” above.  This involves lazy streams
  potentially implemented with coroutines and whatnot, so it’s all
  very abstract.
- [Pseq.new(list, repeats, offset)][44]: Also a pattern.  “Cycles over
  a list of values. The repeats variable gives the number of times to
  repeat the entire list.”
- [Impulse][32]: same parameters as SinOsc, but generates (optionally repeated)
  “non-bandlimited single sample impulses.”
- [Saw][33]: “Band limited sawtooth wave generator,” same parameters
  except for phase.
- [DynKlank][26]: “A bank of frequency resonators which can be used to
  simulate the resonant modes of an object. Each mode is given a ring
  time, which is the time for the mode to decay by 60 dB.”
- [Splay][35]: “spreads an array of channels across the stereo
  field. Optional arguments are spread and center, and equal power
  levelCompensation”.
- [Latch][36]: “Trigger can be any signal. A trigger happens when the
  signal changes from non-positive to positive.  Holds input signal
  value when triggered. Latch will output 0 until it receives its
  first trigger.”

[25]: https://supercollider.github.io/sc-140
[26]: https://doc.sccode.org/Classes/DynKlank.html
[27]: https://depts.washington.edu/dxscdoc/Help/Tutorials/Getting-Started/05-Functions-and-Sound.html
[28]: https://depts.washington.edu/dxscdoc/Help/Tutorials/Getting-Started/04-Functions-and-Other-Functionality.html
[29]: https://depts.washington.edu/dxscdoc/Help/Tutorials/Getting-Started/06-Presented-in-Living-Stereo.html
[30]: https://depts.washington.edu/dxscdoc/Help/Tutorials/Getting-Started/04-Functions-and-Other-Functionality.html
[31]: https://doc.sccode.org/Classes/SinOsc.html
[32]: https://doc.sccode.org/Classes/Impulse.html
[33]: https://doc.sccode.org/Classes/Saw.html
[34]: https://doc.sccode.org/Classes/Function.html#-play
[35]: https://doc.sccode.org/Classes/Splay.html
[36]: https://doc.sccode.org/Classes/Latch.html
[37]: https://doc.sccode.org/Classes/HPF.html
[38]: https://doc.sccode.org/Classes/Decay.html
[39]: https://doc.sccode.org/Classes/Dust.html
[40]: https://doc.sccode.org/Reference/Server-Architecture.html
[41]: https://ccrma.stanford.edu/~ruviaro/texts/A_Gentle_Introduction_To_SuperCollider.pdf
[42]: https://doc.sccode.org/Classes/Pbind.html
[43]: https://depts.washington.edu/dxscdoc/Help/Overviews/SymbolicNotations.html
[44]: https://doc.sccode.org/Classes/Pseq.html

In general the arguments of UGens like SinOsc or Saw can be either
constants or other UGens.  This allows you to construct the signal
flow tree described in earlier sections using infix syntax.  It also
means that the output of a node flows not to another *node* but to an
*input* on the other node, and it avoids the need to name most of your
nodes.

Another interesting thing is that nearly all the UGens have `mul`
inputs, which gives them a built-in gain control, and `add` inputs,
which gives them a built-in mixer.  So in SC you can get by with a lot
less signal processing nodes in your tree than in the Web Audio API or
in my sketch above.

While SuperCollider by default knows about things like beats,
different scales, degrees of scales, note numbers, and DSP filtering,
it doesn’t seem to know much about instruments, though if you `.play`
a pattern without further ado it defaults to a simplified pianoish
“instrument”.
