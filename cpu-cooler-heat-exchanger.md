It occurred to me that two CPU coolers (heatsinks with fans,
occasionally called “fan-fin systems”) back-to-back should function
reasonably well as an air-to-air heat exchanger at small scales.
Typically they are bare aluminum, which melts at 660°, with thermal
paste and plastic fans that are only designed for temperatures up to
200° or less.
