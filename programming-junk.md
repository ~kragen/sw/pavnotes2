If, hypothetically, you're in a post-apocalyptic wasteland trying to
scrape together a computing environment, what should you try to get in
order to program?  Especially if electricity is unreliable and maybe
expensive?

### Computers ###

Ideally things that are powerful enough to run a compiler, have
peripherals that make them useful, have been sold in massive
quantities, and are feasible in practice to load new software onto.

#### [Nokia 1110](https://en.wikipedia.org/wiki/Nokia_1110) ####

Candybar Series 30 GSM phone with microphone, speaker, direction pad
plus 16-key keypad, 4 MB storage, vibrator, no J2ME, monochrome
[96x68](https://www.gsmarena.com/nokia_1110-1187.php) LCD screen with
green backlight, 700 mAh Li-ion battery, 80 g.  250 million units sold
since their launch in 02005.

Most of these are probably thrown away by now, but someone is trying
to sell an apparently working unit on MercadoLibre for US$75.  I don't
think they cost that much even new!

I haven't been able to find out even what CPU these used.  Apparently
Series 30 didn't run Symbian but some of them did use an ARM, while
others used the ST C166 microcontroller.

#### [Nokia 1100](https://en.wikipedia.org/wiki/Nokia_1100) ####

Another Series 30 phone with 250 million units shipped, launched
in 02003.  I see them used for US$10, for some reason lots more than
the 1110, but probably most are junked. Very similar specs but a
couple of buttons less, monophonic ringtones, and I think maybe less
memory.  No real connectivity options other than GSM or a proprietary
cable.

Series 40 phones might be more appealing because loading J2ME or maybe
MIDP applications on them was an officially supported thing to do,
[and people are doing it
today](https://hackaday.com/2022/09/20/finding-digital-solace-in-an-old-nokia-phone/).

#### [Nintendo Gameboy](https://en.wikipedia.org/wiki/Game_Boy) ####

Weak in the keyboard department (D-pad, A, B, Start, and Select), but
you can plug cartridges into it.  119 million shipped since 01989. 4.2
MHz 8080 extension with a handful of Z80 instructions.  16 KiB RAM, no
nonvolatile storage.  Uses 4 AA batteries, so you won't find units
unusable because of not having a battery.  160x144 60Hz black-on-green
screen with a decent tile and sprite engine.  Synth chip and speaker.
220 grams.

Plenty of these for sale used, but also for US$70-100.

I think there are maybe hobbyist cartridges that let you load your own
software on these without building your own hardware?

#### Other possibilities ####

SD cards.  Hard disk controllers.  Inkjet printers.  Laser printers.
Engine control units from old cars.  The HP HP-48GX calculator (4MHz
4-bit Saturn CPU, 512KiB ROM, 64KiB RAM, runs off AA batteries for
months, RS-232 and HP-IR infrared, 131x64 monochrome, several
available on MercadoLibre for US$70).  The [TI Z80 calculators
KnightOS runs on](https://knightos.org/): the TI-73, TI-83+, TI-83+
Silver Edition, TI-84+ (15 MHz, 24 KiB RAM, 1.5MB Flash, USB OTG,
96x64 screen, 9600 baud serial), TI-84+ Silver Edition, and TI-84+
Color Silver Edition. WebAssembly.  The HP 200LX.  The Nokia 6600 (150
million shipped since 02003; a 6620 from 02005 (SD card, Bluetooth,
camera, 150MHz ARM925T) is on MercadoLibre now for US$25).  The
original Nintendo (61 million).  OpenWRT-capable routers. Raspberry
Pi. Arduino Uno. CHDK-compatible Canon cameras.  The S1 MP3 player
(still a mass-market product, US$30). The Commodore 64 (12 million
plus shipped, available on MercadoLibre for US$100 right
now). Android, via F-Droid. Nokia 3210 (161 million shipped since
01999, TI ARM7TDMI, 84x48). Game Boy Color (US$80). Game Boy Advance
(US$150, 81 million, 16MHz ARM7TDMI, 384KiB RAM). Nintendo DS (155
million, Wi-Fi, 256KiB Flash, 2 256x192 LCDs, US$125 on ML). Nintendo
3DS (76 million, US$350). Nintendo Switch (125 million). Sony PSP (80
million).

#### The Blue Pill STM32 board ####

This is US$12 on Mercado Libre: 20KiB RAM, 128KiB Flash, 72MHz ARM
Cortex-M0, 1Msps ADC.  I like the STM32 line on paper, though I don't
have a lot of experience with it.

#### NodeMCU and similar [ESP32] ####

Wi-Fi, 2 Mbps Bluetooth LE, ZigBee, dual-core? 160MHz?
Harvard-architecture Tensilica XTensa, 520 KiB RAM, 448 KiB ROM?,
8-16MiB Flash, 100mW with radios off).  US$4.50 on MercadoLibre today.
This is maybe the most MIPS for the buck I've seen in a relatively
plug-and-play board, but of course it comes with basically no human
interface devices.

Two 12-bit ADCs, up to 2Msps theoretically, but [with poor
linearity](https://electronics.stackexchange.com/questions/530961/esp32-adc-not-good-enough-for-audio-music).

[Bitluni has gotten monochrome NTSC/PAL "composite" video
out](https://bitluni.net/esp32-composite-video).  Later work by Rossum
called `ESP_8_BIT` [extended this to
color](https://github.com/Roger-random/ESP_8_BIT_composite), as [he
explained on his repo](https://github.com/rossumur/esp_8_bit).  You
could hook this up to any monitor with a composite video input,
including lots of closed-circuit camera security monitors.

More recent revisions of the ESP32 [have broken this
code](https://learn.adafruit.com/video-nub-shank-esp32-qt-py-composite-video-injector/overview)
and use RISC-V (ESP32-C3) instead of Tensilica XTensa,
but I'm pretty sure you can still generate a working monochrome
signal.

#### Raspberry Pi Pico RP2040 ####

These cost
[US$7](https://articulo.mercadolibre.com.ar/MLA-927821525-placa-desarrollo-raspberry-pi-pico-rp2040-_JM)
and you get a 133MHz ARM Cortex-M0+ with 264 KiB of SRAM and 2 MiB of
Flash, 40 GPIO pins, and a programmable I/O coprocessor, on about 75
mW (though its deepest sleep mode still uses nearly a milliwatt).  Not
as good a deal as the ESP32 for most purposes.

Someone [did manage to squeeze color NTSC out of
it](https://hackaday.com/2023/01/24/generating-pal-video-with-a-heavily-overclocked-pi-pico/]
but only by overclocking it by almost 2.5x and using an external R-2R
DAC.

### Peripherals ###

#### NTSC/PAL ####

Suppose you have NTSC or PAL output working; how do you visualize it?

The [7-inch no-brand 800x400 M707 two-input LCD
monitor](https://articulo.mercadolibre.com.ar/MLA-1383527578-monitor-tft-de-7-pulgadas-2-entradas-de-video-camara-_JM)
runs on 12VDC, weighs 700g, and costs US$50 in MercadoLibre's
"monitores de vigilancia" category.  But you can also use old
discarded TVs if you can get them before the *cartoneros* break the
yokes off the CRTs for recycling.

#### Low-power LCDs ####

Old transflective Nokia LCDs use an SPI interface and run on about a
milliwatt; the 84x48 display of the 5110 in particular is popular and
goes for about US$5, already on a breakout board.  Broken graphing
calculators, dead cellphones, desk phones, and cordless phones may be
a viable source for reflective or transflective pixel matrix displays,
though many of these will be harder to interface.  Working new
non-graphing scientific calculators with 12 5x7 characters cost about
US$4; a graphing Casio fx-82LAX (63x192) is US$30.  LCD panels from
broken TVs and monitors may be viable.

There are also modules like the 320x240 color TFT-IL9341, which
TodoMicro offers for about US$12, but I think it needs to be backlit,
so it will probably use tens if not hundreds of milliwatts.

#### Audio ####

Just about anyone can scrounge up a speaker or pair of earphones (I
dumpster-dived 13 probably working speaker drivers from a bankrupt
Thonet & Vander outlet this evening), but using that as a
comprehensible output format may be harder, though surely feasible.
See file `frugal-speech-synthesis.md` for more.

If you can manage Bluetooth or AM or FM radio synthesis, that may also
be a useful way to do audio output.

#### Keyboards ####

Shitty computer keyboards are pretty abundant, but I also think it
probably wouldn't be hard to improvise a membrane keyboard out of
paper, plastic, pencil lead, and tape.  The only hard part is scanning
the matrix; if you're short on microcontrollers, the chip from a cheap
shitty keyboard that got accidentally smashed is likely good enough.
Some of these use a significant amount of power, up to hundreds of
milliwatts.

Earphones or piezo buzzers or speakers can detect finger taps.
