The other day I was trying to explain long division in terms of loop
invariants, but ended up explaining loop invariants in terms of long
division.  That was a terrible way to explain loop invariants.  Loop
invariants should be explained in terms of something that the audience
already understands perfectly, before attempting to apply them to
something they don’t.

Totaling a list
---------------

The simplest loop is probably something like this:

    for i in range(10):
        print(i)

But I don’t know how to explain it in terms of loop invariants,
because loop invariants are a technique for proving correctness, and
it’s hard to specify what would amount to correct behavior for that
loop.  Instead, I’ll explain this Python loop:

    total = 0
    for n in a:
        total += n

It computes the total of the numbers in list `a`.

Unfortunately, Python’s concision is kind of getting in our way here,
so I have to start by expanding this loop out a bit into a more
verbose form, still in Python:

    total, i = 0, 0     # 1
    while i < len(a):   # 2
        n = a[i]        # 3
        total += n      # 4
        i += 1          # 5
                        # 6

Hopefully it is clear that this does the same thing as the more
concise version above.

What we will show is that, after the loop, on line 6, `total` contains
the total of all the items in `a`, Σⱼa[j], which I will write out here
more explicitly as `total == sum(a[j] for j in 0:len(a))`, which is
not quite Python.  We can demonstrate this with a loop invariant as
follows.

Our loop invariant will be

    total == sum(a[j] for j in 0:i).

Saying that this is an “invariant” of our loop means that the loop
body *preserves it*: if it is true *before* an iteration of the loop,
at the beginning of line 3, it will also be true after it, at the end
of line 5; and that it is true before the loop begins, at the end of
line 1.

At the end of line 1, `i == 0` and so the sum reduces to `sum(a[j] for
j in 0:0)`.  But that’s an empty range; the sum of no items is 0.  And
indeed `total` does equal 0 there.  So the loop invariant is in fact
true before the loop.

Suppose that on a given iteration of the loop, the invariant is indeed
true at the beginning of line 3.  At that point `i` and `total` have
some value; let’s call them `i₃` and `total₃`, so we can write this
premise as

    total₃ = sum(a[j] for j in 0:i₃).  # 7

 On line 4 we change `total` to a new value, `total₄`, by adding
`a[i₃]` to it, and on line 5, we change `i` to a new value, `i₅`, by
adding 1 to it.  So we have:

    total₄ = total₃ + a[i₃]
    i₅ = i₃ + 1

So now the question we have to ask is whether we’ve maintained the
invariant or not.  Is it still true that

    total == sum(a[j] for j in 0:i)   # 8

now that

    total == total₄
    i == i₅
    
Well, 

    sum(a[j] for j in 0:i₅) == sum(a[j] for j in 0:(i₃+1)),
    
which is

    sum(a[j] for j in 0:i₃) + a[i₃].
    
And remember that our premise (# 7 above) was that

    total₃ = sum(a[j] for j in 0:i₃).
    
So if

    total₄ = total₃ + a[i₃]
    
then

    total₄ = sum(a[j] for j in 0:i₃) + a[i₃]
    
and therefore

    total₄ = sum(a[j] for j in 0:i₅),
    
which is what was to be demonstrated (# 8 above).

(And the loop condition has no side effects, so it doesn’t affect the
loop invariant.)

Therefore, if the invariant was true before a given iteration of the
loop, it is true at the end of that iteration.  And, since it was true
before the first iteration of the loop, we can conclude that it must
be true after every iteration of the loop.

And in particular it’s true when the loop *ends*, if it ever does.
(Which it does, something that can be demonstrated by a technique
called “loop variants”, which I won’t explain here.)

On line 6, `i == len(a)`, because that’s how we get out of the loop.
So our loop invariant that `total == sum(a[j] for j in 0:i)` now
demonstrates that `total == sum(a[j] for j in 0:len(a))`.  And so the
loop does correctly compute the total of the items in `a`.

It’s worth noting that, at the end of line 4, the invariant is
violated, because the invariant defines what value `total` is supposed
to have in terms of `i`, and on line 4 we change the value of `total`,
but we haven't yet changed `i` to match.  Then, it is restored in the
next line.  This is very typical.  So maybe “invariant” is misleading
terminology.

All of that might have been obvious beforehand, so it might seem like
a waste of time to make such a long-winded argument about it.  But
this is an absolutely rigorous technique which can be applied to much
more difficult loops to demonstrate that they do the right thing; the
above is intended to give a clear example of each of the moves in the
strategy used to checkmate all doubt:

1. Show that the invariant is true before the beginning of the loop.
2. Show that each iteration of the loop preserves the invariant.
3. Show that the loop eventually terminates, usually handwaved away
   because it’s too obvious.
4. Show that the invariant being true after the loop would mean that
   the loop computed what you wanted it to.

If you can fulfill each of these proof obligations, you have proven
that the loop does in fact compute what you wanted it to.

The tricky part is finding an invariant that fulfills both 1 and 4: it
has to be so trivial that it’s already true before the loop, and yet
so strong that it expresses your entire intent after the loop.  You do
this by making it depend on a variable that changes, which doesn’t
have to be a variable in your actual program; it can be something like
“let *k* be the number of iterations of the loop that have begun to
execute”.

Filtering a list
----------------

Here’s a slightly more complex example:

    results = []
    for o in a:
        if is_cool(o):
            results.append(o)

Let *k* be the number of iterations of the loop that have begun to
execute, and let a cool object be an object for which `is_cool`
returns a true value.  Here the loop invariant is that `results`
consists of all the cool objects `o` which reside at positions *i* in
list `a` such that *i* < *k*, and only those objects.  So for example
if *k* = 3, then `results` should contain all the cool objects from
the set `{a[0], a[1], a[2]}`.

This is true before the beginning of the loop because *k* is 0, so
there are no positions *i* into `a` such that *i* < *k*.

If `is_cool` doesn’t change the contents of `a` or `results`, the loop
maintains the truth of the invariant, because if a[*k*-1] is cool, it
gets added to `results`.  So, if `results` previously had all the cool
objects before a[*k*-1], now it has all the cool objects up to a[*k*].

The loop terminates if that `a` is a list because Python lists cannot
be infinite.

After the end of the loop, the invariant demonstrates that all the
cool objects in `a` are in `results`, which is hopefully what you
wanted.

This invariant would also work just as well if instead of
`results.append(o)` the code had said `results.insert(0, o)`, even
though it would produce the results in a different order.  You could
use a stronger invariant that puts conditions on the ordering of
`results` if you wanted to exclude this.

Selection sort
--------------

This is maybe the simplest “algorithmically interesting” example.

    for i in range(len(a)):
        for j in range(i+1, len(a)):
            if a[i] < a[j]:
                a[j], a[i] = a[i], a[j]

I think that the invariant for the outer loop is that the elements of
`a` are a permutation of the original elements of `a`; all the items
a[*k*] such that *k* < `i` are less than or equal to all the items
a[*n*] such that *n* ≥ `i`; and the items a[*k*] such that *k* < `i`
are sorted in ascending order.

I think that the invariant for the inner loop is that the elements of
`a` are a permutation of the original elements of `a`; the items
a[*k*] such that *k* < `i` are the same as they were when the inner
loop began executing; and `a[i]` is less than or equal to all of the
items a[*n*] such that `i` < *n* < `j`.
