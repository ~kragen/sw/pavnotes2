PVC pipe is easy to cut with saws or even just twine of cotton or
hemp.  But it's not very stiff or strong, and it's somewhat brittle;
you can chip it easily, and if you put it under a lot of stress it
tends to shatter.

Portland-cement concrete is heavy and hard to cut, but you can cast
it, and it's usually quite stiff and, at least in compression, pretty
strong.  (Even in tension it's pretty strong as long as it doesn't
have to support its own weight.)

It occurred to me that a kind of janky construction technique might be
able to marry some of the advantages of each material.  You can build
a frame out of PVC pipe, stuff a bit of rebar through it, and then
pump the pipe full of portland mortar (concrete without coarse
aggregate, just sand, water, and cement) and let it cure.  The
reinforced concrete frame thus formed will be fairly precisely in the
shape of the original PVC, but enormously stiffer, and even if the
rebar doesn't keep it from cracking, it will keep the pieces from
pulling apart.  This will prevent the PVC from being under any
structural stress transmitted from other parts of the structure,
unless it's so great as to break the concrete.

By making triangulated truss structures out of the PVC pipe in this
way, you can improvise a reinforced concrete truss.

This of course depends on the ability to make joints at weird angles,
not just 90°.  Off-the-shelf plumbing fittings may not be adequate.
Moreover, for a truss, you need to be able to bring together at least
six pipes at a single joint.

It may make more sense to prepare a large number of reinforced
concrete bars in this way ahead of time, with rebar sticking out of
their ends, and then tie the pre-cured bars together with baling wire
at joints at arbitrary angles.  Then you can build a small ferrocement
joint around the tied rebar to protect it from corrosion and provide
proper compressive strength.

By mixing some kind of gelator and some fiber reinforcement (basalt
fiber or whatever) into the mortar used at the joint, I think you can
reduce its tendency to flow and drip off the joint under the influence
of gravity; this may eliminate the need for reinforcing steel in the
joint.

Pretensioning these pre-cured bars may be a reasonable thing to do:
instead of sticking rebar through them, stick a hose through them, and
once the cement is in, stick some allthread through the middle of the
hose, which will make it straight and thus keep it away from the
sides.  Once the concrete is cured, you can torque down some nuts on
the end of the allthread.

In a truss structure, the bars are in pure tension or compression,
with no significant shear or bending moments, but the tension forces
can be quite significant.  Probably when the structure fails, it will
be from the rebar breaking under that tension.
