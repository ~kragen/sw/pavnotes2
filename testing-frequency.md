[I asked on Fecebutt what poly people's STI testing policy
was](https://www.facebook.com/groups/polygroup/posts/5894141660666075/).
All respondents seem to be from North America.  Summary of responses:

Reported testing frequency varies from "once or twice a month" to
"once a year".

I'm guessing there's probably a large population of people whose
answer would be "never" but who didn't want to respond to avoid being
publicly criticized, or who didn't respond because they aren't active
in the group. This same effect might bias people's responses toward
reporting more frequent testing than they practice consistently.

Everybody reports getting tested for HIV, syphilis, gonorrhea, and
chlamydia; some people also report getting tested for hepatitis B,
hepatitis C, HSV1, and HSV2.  One person reported getting tested for
trichomonas.

Some people, even in the US, are claiming somewhat implicitly they get
tested regularly for chancroid and donovanosis.  In one case, when
pressed, the claimant admitted that she wasn't sure what tests were
actually in her testing battery.

Nobody has *explicitly* reported getting regularly tested for
chancroid, donovanosis, Zika, molluscum, or HPV (though several people
have claimed so implicitly).

The median testing frequency was quarterly.

Empirical distribution of reported testing frequencies:

1. Every 6-8 weeks.
2. Once a month, sometimes twice a month.
3. Quarterly (when sexually active).
4. Quarterly.
5. Every three months (due to PrEP) but once a year for hep C.
6. At least twice a year, sometimes 3 times a year.
7. Six months ago ('When I last tested (July 2022)').
8. Once a year or whenever I give blood or give a blood test for
   something.
9. Annually, but if I or my partner(s) have a new partner, I test
   intermittently.
