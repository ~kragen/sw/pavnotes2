ECM machining ought to be able to cut a thinner kerf through plate
metal than conventional forms of machining, because the “blade”
doesn’t have to withstand cutting forces, so it can be thinner.  ECM’s
MRR can be comparable to conventional forms of machining; if that can
be realized in this setting, it would translate to cutting many more
millimeters per second, rather than fewer as wire EDM does.  Moreover,
being able to remove much less material should remove ECM’s
energy-intensity disadvantage relative to conventional forms of
machining.  And, of course, it would retain ECM’s ability to cut
hardened materials and even some ultrahard ceramics like tungsten
carbide.

To be concrete, let’s consider cutting 6mm mild steel plate with a
molybdenum or carbon-steel wire, which you can buy in different
thicknesses, down to 30 microns, though 60 microns and up is more
common.  Let’s suppose it has 300MPa of yield strength ([MatWeb says
pure molybdenum is 345, annealed][0], and music wire is commonly much
higher, so we can probably hope for three times that, and also
[tungsten-alloy steel wire is sold for this][1], and BS Stainless says
[their 30μm-diameter 316 stainless wires][13] have tensile strength of
“686–980” MPa); then a 30μm-diameter wire can handle 0.2N of stress.
You stretch this wire on a sort of very rigid bow saw or cheese
cutter, which you drag back and forth through the kerf as fast as you
can, while maintaining the wire at a negative potential relative to
the workpiece.  The wire drags electrolyte through the kerf,
encountering viscous drag, which could be the limiting factor on how
fast you can move it.  Let’s say we have a 30-μm-diameter wire in a
40-μm-thick kerf.  How fast can we pull it before the viscous drag of
the electrolyte breaks the wire?

(Incidentally, the Sunshine company that makes the 30-μm tungsten wire
also sells [“Superfine silver jump wire”][2] for cellphone electrical
repairs, which is 7μm thick.  On Ebay it's sold by user
“micro\_soldering”.)

[0]: https://www.matweb.com/search/datasheet.aspx?matguid=a6039ca64ad74598aadf05c9e2f6c1a4&ckck=1
[1]: https://articulo.mercadolibre.com.ar/MLA-812565835-hilo-separador-lcd-de-tungsteno-003mm-100m-ss-051-_JM
[2]: https://www.ebay.com/itm/313208656329
[13]: https://www.bsstainless.com/stainless-steel-ultra-fine-wire

Water’s viscosity decreases a lot with temperature, and I think higher
temperatures also favor ECM.  The [dynamic viscosity *μ* is
defined][3] by parallel-plates laminar shear *F* = *μAu*/*y*, where
*F* is the force, *A* is the area of the shear zone, *u* is the
relative velocity of the plates, and *y* is the separation.  We can
approximate this situation as two parallel plates of area 6mm × 110μm
= 0.66mm² separated by 5μm.  Suppose we take [the viscosity of water
at 70°][5] as 0.404 millipascal seconds.  We find we don’t reach 0.2N
until an astounding 4km/s:

    You have: .404 mPa s 6 mm 110μm 4000m/s / 5μm
    You want: N
            * 0.213312
            / 4.6879688

Saturating water with NaCl apparently increases its viscosity about
50%, but other solutes, even other chlorides, can have [other effects
including even decreasing viscosity][4].  But in fact it seems like
the viscosity is not, contrary to my expectations, going to be the
limiting factor.

[3]: https://en.wikipedia.org/wiki/Viscosity#Dynamic_viscosity
[4]: https://en.wikipedia.org/wiki/List_of_viscosities#Aqueous_solutions
[5]: https://en.wikipedia.org/wiki/Viscosity#Water

Probably in fact the limiting factor is how well we can control the
movement to prevent accidentally yanking on the wire and snapping it,
or conversely letting it go slack and flap against the side of the
kerf all the time.  (An occasional contact won’t be a problem if the
electrical side of the system is done properly; it just means we lose
the cutting voltage until the short is cleared, and shouldn't damage
the wire or workpiece, since the voltage is about a volt and the
capacitance can probably be kept under 100pF, so under 100pJ to
discharge.)

We could imagine a system with two reels of wire, rather than a
bow-saw-like setup, which actively controls the angular position of
each reel of wire tightly enough to keep the wire from snapping or
going slack.  To develop such a system (outside the kerf!) you can
place a light side force at the center of the supposedly tensioned
wire to deflect it, perhaps using a lightweight pulley; as you move
the wire from reel to reel, this deflection should remain constant at
a given speed, and its variation from one speed to another should be
only due to the centrifugal force on the wire.  In the kerf itself you
want to position the wire with high precision, which is most easily
done with bushings on the surface of the workpiece.

Suppose we can move the wire through the kerf at the
fantastic speed of 100 meters per second; the electrolyte is probably
moving through at about half that speed, 50 meters per second, which
works out to about 30 microliters of electrolyte per second.  Suppose
the hot water [dissolves 90g/100mℓ of FeCl₂][6] so those 30
microliters contain, I don’t know, 15 milligrams of FeCl₂, which is
44% iron, so about 7 milligrams of iron.  Per second.

[6]: https://en.wikipedia.org/wiki/Iron(II)_chloride

Well, what kind of cutting speed is that?  Our 6mm example has, per
millimeter of kerf, 6mm × 1mm × 40μm = 0.24mm³ of almost entirely
iron, which is about 1.9 milligrams.  So we’re cutting about four
millimeters per second with our 100 meters per second of wire, which
is 240 millimeters per minute: [slower than even a 500-watt fiber
laser][12], which can do 600–900mm/minute, but laser cutting has rough
tolerances; this is much faster than any other precision cutting
process.

[12]: https://www.machinemfg.com/laser-cutting-thickness-speed-chart/

If we could manage to use the 7-μm silver “jump wire” instead, maybe
we could make a 12-μm kerf, which would allow us to cut 3⅓ times as
fast, like 15 millimeters per second or so.  And if we could manage to
move the wire at Mach 1, we would get another factor of 3.4.

The fast-moving water will tend to lubricate the wire and hold it away
from the surface of the kerf.

Everything above is assuming laminar flow, because I figured that in a
5-micron-thick space, flow is just going to have to be laminar.  But I
haven’t actually calculated the Reynolds number, so maybe I’m wrong.

The [Reynolds number][7] is Re = *ρuL*/*μ*; roughly here we have
(1000kg/m³) · (100m/s) · 40μm / (0.4 mPa · s), which works out to 10000,
probably past the [laminar/turbulent transition][8], which usually
occurs between Re = 2000 and Re = 13000.  But if instead we take the
relevant length to be 5μm, we get only 1250.

We probably really don’t want turbulent electrolyte flow, because it
will yank on the wire and bash it into the walls of the kerf.  But the
good news is that we can probably ensure laminar flow at even higher
speeds by *making the kerf even thinner*, reducing the clearance
between the cutting wire and the kerf walls.

[7]: https://en.wikipedia.org/wiki/Reynolds_number#Definition
[8]: https://en.wikipedia.org/wiki/Laminar%E2%80%93turbulent_transition

Clearly there’s some point at which the basic assumptions break down;
water’s shear behavior won’t stay [Newtonian][9] and linear forever.
We’re already looking at shear rates of 20 million s⁻¹, which are way,
way outside our normal experience.  Pipe, Majmudar, and McKinley’s
[“High Shear Rate Viscometry”][11] say:

> Measurements using narrow gap Couette flows between concentric
> cylinders and between parallel-plate fixtures have been used to
> measure the viscosity of Newtonian and shear-thinning liquids up to
> ̇γ ∼ 106 s−1 (Merrill, 1954; Kulicke & Porter, 1981; Connelly &
> Greener, 1985; Dontula et al., 1999; Mriziq et al., 2004; Davies &
> Stokes, 2005) using gaps 0.5 μm ≤ H ≤ 50 μm. Suggestions that
> Newtonian fluids might show a decrease in viscosity at shear rates ̇γ
> ≈ 5 × 10⁴ s⁻¹ presented by Ram (1961) have been re-examined by
> Dontula et al. (1999), who argue that the fall in viscosity of a
> glycerin-water solution at high shear rates may well be due to
> non-viscometric flow phenomena such as viscous heating or
> hydrodynamic instabilities, rather than arising from a true
> rate-[dependent] material property.

[9]: https://en.wikipedia.org/wiki/Newtonian_fluid
[11]: https://web.mit.edu/nnf/publications/GHM116.pdf

[“A Novel Approach for Modeling the Non-Newtonian Behavior of Simple
Liquids: Application to Liquid Water Viscosity from Low to High Shear
Rates”][10] by Aitken and Volino (CC-BY) has a nice Figure 8 showing
experimental data of water’s Newtonian behavior with about 1 mPa · s
(because room temperature) from shear rates of about 50 s⁻¹ to about
600 s⁻¹, then increasing almost linearly to about 1.7mPa·s at 4000s⁻¹.
If this linear increase were to continue indefinitely, at a shear rate
of 20 million s⁻¹, its viscosity would be about 5000mPa·s rather than
1 or 2.  Unfortunately I didn’t actually read the paper so I don't
have any idea what they think will happen at orders of magnitude
higher shear rates.  If that’s what it did, my force estimates above
would be low by three orders of magnitude, but also the Reynolds
number would be high by three orders of magnitude.

[10]: https://mdpi-res.com/d_attachment/condensedmatter/condensedmatter-08-00022/article_deploy/condensedmatter-08-00022-v2.pdf?version=1676351969
