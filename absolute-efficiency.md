One of the things Stepanov cites as an advantage of C++ for generic
programming is its absolute efficiency (I think that’s the term he
uses); you can expect generic programs written in C++ to be roughly as
efficient as if you’d written them by hand in assembly language.
Often enough, C++ compilers have failed to deliver on this promise,
but today I found a shining example.  I was writing this code as an
example of linked-list traversal in Spanish:

    const Proveedor& Producto::mejor_proveedor() const
    {
      return *std::ranges::min_element(proveedores,
        [] (const Proveedor& a, const Proveedor& b) {
          return a.precio < b.precio;
        });
    }

`mejor_proveedor` means “best provider”.  This is supposed to walk the
doubly-linked list `Producto::proveedores` (product providers) in
search of the lowest-price provider of a product, then return a
reference to it.  `Proveedor` consists of a name and a price:

    class Proveedor {
    public:
      string nombre;
      Precio precio;
    };

`Precio` is an object containing an integer number of pennies; the
relevant members of the class are:

    class Precio {
      int centavos;
    public:
      strong_ordering operator<=>(const Precio& o) const {
        return centavos < o.centavos ? strong_ordering::less :
              centavos == o.centavos ? strong_ordering::equal :
                                       strong_ordering::greater;
      }
    };

(You might reasonably object that if I’m going to use `std::ranges` I
might as well use the compiler-generated comparator function as well,
and you’d probably be right about that.)

Here’s some commented disassembly of the code produced by G++ 10.5,
invoked rather confusingly as `g++-10 -Wall -Werror -Os -std=c++11 -g
-std=c++20`:

    00000000000028ba <Producto::mejor_proveedor() const>:
     28ba:             endbr64
     28be:             mov rax,QWORD PTR [rdi+0x20]  ; first list node pointer
     28c2:             lea rcx,[rdi+0x20]       ; load list header pointer
     28c6:             cmp rcx,rax              ; check if list empty
     28c9: ┌────────── je 28e3                  ; return if so
     28cb: │    ┌────► mov rdx,rax    ; point best-node pointer at current node
     28ce: │    │  ┌─► mov rax,QWORD PTR [rax]  ; follow link to next list item
     28d1: │    │  │   cmp rcx,rax              ; end of list?
     28d4: │ ┌──────── je 28e0                  ; return current node if so
     28d6: │ │  │  │   mov esi,DWORD PTR [rdx+0x30] ; load price of best node
     28d9: │ │  │  │   cmp DWORD PTR [rax+0x30],esi ; compare to current node
     28dc: │ │  │  └── jge 28ce       ; loop without updating best-pointer
     28de: │ │  └───── jmp 28cb       ; unless it was better
     28e0: │ └───────► mov rax,rdx    ; load result register from best-pointer
     28e3: └─────────► add rax,0x10   ; but increment past the pointer fields
     28e7:             ret

This is perfect except for some minor quibbles.  I think you might be
able to make the inner loop very slightly tighter here by rotating it
around so that the exit test was at the bottom (which might make it
bigger, and I asked for `-Os` on the theory that the disassembly would
be easier to read), and the STL’s choice of marking the list end with
a pointer to the list header (rather than a null pointer) requires an
extra `lea` instruction upon entry.  And in many cases the penultimate
`add` instruction would be sort of unnecessary, like if the pointer
fields actually belonged to the `Proveedor` object.  Otherwise,
though, I don’t see any way to optimize it.

You could cache the best price in a register, and I guess hitting L1
cache an extra time in the inner loop is probably a sin, but that
would probably make it bigger.  (However, the use of a linked list in
this case is probably a much bigger sin.)

Several levels of indirection have been collapsed at compile time
here.  The linked-list iterator, the `min_element` function, the
lambda object, the comparison-operator function, the multiple
comparisons within it, and the three levels of nesting (`Precio`
within `Proveedor` within the list node) have all been optimized away;
all that’s left of them is single registers and offsets.  The offset
0x30 (decimal 48) is the sum of the sizes of the linked-list pointer
fields and the 32 bytes of the `string` object in `Proveedor` that
precedes the price.

In 02018, [Silas Reinagel wrote a post][0] wrote, implicitly
referencing [Zed Shaw’s famous 02014 rant][1]:

> The concept of abstraction in software development is frequently
> misunderstood and confused with indirection. This is partially
> because of the keywords `abstract` and `interface` in
> statically-typed languages such as Java and C#. The confusion often
> leads to design changes that leave the code worse than before it was
> touched. Let’s look at how abstraction and indirection relate, and
> how to correctly connect components.

Contrary to Reinagel and Shaw’s thoughtful opinion, I think these
indirections make the `mejor_proveedor` method more abstract than a
version that iterates explicitly over a null-terminated linked list:

    typedef struct node {
      struct node *siguiente;
      string nombre;
      int precio_centavos;
    } node;

    node *
    concrete_mejor_proveedor(node *proveedores)
    {
      node *mejor = proveedores;
      for (; proveedores; proveedores = proveedores->siguiente) {
        if (proveedores->precio_centavos < mejor->precio_centavos) {
          mejor = proveedores;
        }
      }
      return mejor;
    }

I argue that this contains details about how prices are represented
and how the list is traversed that are absent from the STL version at
the top.  If, for example, I change the declaration of `proveedores`
from being a `list` to being a `deque`, which is probably a better
choice in practice, the compilation result changes to this new code:

    00000000000028fa <Producto::mejor_proveedor() const>:
     28fa:         endbr64
     28fe:         mov rcx,QWORD PTR [rdi+0x50] ; load end pointer into rcx
     2902:         mov r8,QWORD PTR [rdi+0x30]  ; first block pointer into r8
     2906:         cmp r8,rcx  ; skip loop if no blocks in deque
     2909:         je 2941 ──────────────────────┐
     290b:         mov rsi,QWORD PTR [rdi+0x40]  │ ; first block end pointer
     290f:         mov rdx,QWORD PTR [rdi+0x48]  │ ; array of block starts
     2913: ┌─────► mov rax,r8                    │
     2916: │ ┌───► add rax,0x28                  │ ; bump by Proveedor size
     291a: │ │     cmp rax,rsi                   │ ; compare to deque block end
     291d: │ │ ┌── jne 292e                      │ ; only if block end reached:
     291f: │ │ │   mov rax,QWORD PTR [rdx+0x8]   │ ; first pointer in next block
     2923: │ │ │   add rdx,0x8                   │ ; increment block pointer
     2927: │ │ │   lea rsi,[rax+0x1e0]           │ ; end of block is +480 bytes
     292e: │ │ └─► cmp rcx,rax                   │ ; end loop if no next block
     2931: │ │     je 2941  ─────────────────────┤
     2933: │ │     mov edi,DWORD PTR [r8+0x20]   │ ; load best price
     2937: │ │     cmp DWORD PTR [rax+0x20],edi  │ ; compare to current price
     293a: │ └──── jge 2916                      │ ; if current is less,
     293c: │       mov r8,rax                    │ ; update best pointer
     293f: └────── jmp 2913                      │
     2941:         mov rax,r8 ◄──────────────────┘ ; return best pointer
     2944:         ret
     2945:         nop

[0]: https://www.silasreinagel.com/blog/2018/10/30/indirection-is-not-abstraction/
[1]: http://web.archive.org/web/20160320004223/https://zedshaw.com/archive/indirection-is-not-abstraction/

This is a little more obviously suboptimal because of including the
useless `mov rax, r8` in the inner loop sometimes, but that’s probably
zero micro-operations, and this version is probably a lot faster in
practice because of chasing pointers less often by a factor of 12.
And it’s still a leaf subroutine, and the inner loop is only 8
instructions in the usual case, even if it does hit L1 cache an
unnecessary time.  So the efficiency is pretty good.

If, instead of changing the `list` into a `deque`, we change `Precio`
to store a float, instead we get this:

    00000000000028ca <Producto::mejor_proveedor() const>:
     28ca:             endbr64
     28ce:             mov rax,QWORD PTR [rdi+0x20]
     28d2:             lea rcx,[rdi+0x20]
     28d6:             cmp rcx,rax
     28d9:      ┌───── je 28f8
     28db:      │      mov rdx,rax
     28de:      │      mov rax,QWORD PTR [rax] ◄──────────┐
     28e1:      │      cmp rcx,rax                        │
     28e4:      │  ┌── je 28f5                            │
     28e6:      │  │   movss xmm0,DWORD PTR [rdx+0x30]    │
     28eb:      │  │   ucomiss xmm0,DWORD PTR [rax+0x30]  │
     28ef:      │  │   cmova rdx,rax                      │
     28f3:      │  │   jmp 28de ──────────────────────────┘
     28f5:      │  └─► mov rax,rdx
     28f8:      └────► add rax,0x10
     28fc:             ret

Again, this is pretty close to optimal, except that it doesn’t cache
the best price in a register, and you could probably rotate the loop
to avoid the unconditional jump inside it.

Here’s how Reinagel defines abstraction:

> Low abstraction (concrete) occurs when details are very specific and
> explicit. High abstraction occurs when all details are left out, and
> only general concepts are communicated. Low abstraction in software
> operates with full knowledge of details such as memory addresses,
> pointers, threads, registers, encodings, http status codes,
> etc. High abstraction in software operates in business concepts such
> as calendars, schedules, orders, customers, slides, pictures,
> etc. Things which are more abstract are more generally usable, and
> are easier to reason about.

I feel like the facts that the price is encoded as a floating-point
number (or an integer) and the providers are linked together in a
linked list (or packed together in arrays of 12 providers) are
“details such as memory addresses, pointers, threads, registers,
encodings, [HTTP] status codes, etc.”  Those details are omitted from
the `min_element` version of `mejor_proveedor`, and then added back
into it by the compiler, along with the registers, pointers, and
memory offsets.

But the *way* they’re omitted is just through indirection.  Those
details are still present somewhere; the structure and memory layout
of `std::list` is defined in the standard library headers (well, as
interpreted by the compiler), the choice of `std::list` for
`proveedores` is defined in the declaration of `Producto`, and the
memory layout of `Precio` and its comparison function are defined in
the `Precio` class.

By contrast, the choice of register `rcx` for the pointer that ends
the list is not really specified anywhere — the code I’m compiling
certainly doesn’t specify it, and although the compiler contains the
register assignment algorithm that settled on that assignment, that
algorithm doesn’t have a table entry in it specifically reserved for
this subroutine.  That’s a piece of detail created during the
compilation process.  You could argue that that’s a sort of
“difference in kind” from the fairly simple indirection involved here.

I don’t think it really is, though.  There are several other methods
in the program that traverse or modify the list of providers and
access the price field, whether to set it, to compare it, or to print
it.  For these purposes the compiler also incorporates into them this
same knowledge of how the list is structured, where the price field is
stored, and how it’s encoded.  So in the end the compiled program
contains these details from the source code in many places, not just
one, so in at least a crude sense it’s more detailed, but the details
are somewhat repetitive.

“Abstraction” often refers to the cognitive process of examining many
such repetitive details in the world to reconstruct their common
origin, also called an “abstraction”.

And, of course, it’s straightforward to write a subroutine which in
effect extends the compiler’s code generator by way of inlining,
constant propagation, and dead code elimination.  Here’s a stupid C
function that serves as an example of this, although in this case GCC
already knows how to do this optimization without the function:

    static void inline
    short_memcpy(char *dest, const char *src, size_t nbytes)
    {
      if (nbytes == 4) {
        /* Probably in real life you’d use inline assembly here */
        dest[0] = src[0]; dest[1] = src[1]; dest[2] = src[2]; dest[3] = src[3];
      } else if (nbytes < 4) {
        if (nbytes == 2) {
          dest[0] = src[0]; dest[1] = src[1];
          memcpy(dest, src, 2);
        } else if (nbytes < 2) {
          if (nbytes == 1) *dest = *src;
        } else {                    /* nbytes == 3 */
          dest[0] = src[0]; dest[1] = src[1]; dest[2] = src[2];
        }
      } else {
        memcpy(dest, src, nbytes);
      }
    }

A call to this function with a constant small number of bytes will
result in inlining one of its branches, the others having been
eliminated by constant propagation and dead code elimination.  You can
thus put inline assembly in the different branches which emits the
optimal sequence for the particular size being copied, probably an
unaligned load and store on amd64.  To me this seems extremely similar
to the instruction selection phase of a compiler’s code generator.

(I originally wrote the function slightly differently for a slightly
different purpose: with old versions of glibc, it gave a significant
speedup when nbytes was run-time variable but usually small.)

So the mere *indirection* in `mejor_proveedor` makes it *abstract*
over different implementations of `Precio` and different collection
types.  It’s *general* in a way that my `concrete_mejor_proveedor`
example isn’t.  That *abstraction* is precisely what makes it possible
to change the implementation of `Precio` or the collection type
without editing `mejor_proveedor` and all her sister functions.

In this case, all this indirection is resolved at compile time,
removing the abstraction in the compilation process, permitting the
efficiency Stepanov prizes so highly.  This makes it easy to see that
the compiled code does the same thing as the source code, but more
concretely.

But suppose you interpret the C++ with a C++ interpreter that walks
all those indirections at runtime, or compile the C without
optimization, so no constant-propagation is performed.  Does that make
the *source code* less abstract?  That would seem absurd; it still
uses the same amount of detail as before, and at least from the
perspective of the *behavior* of the running program, it still
describes the same computational process.

Consider translating the method into Lua:

    function mejor_proveedor(producto)
        return min_element(producto.proveedores,
            function(a, b) return a.precio < b.precio end)
    end

Lua doesn’t have equivalents of C++ object embedding, constructors,
destructors, and pointers-to-pointers, but you can definitely write
`min_element` in it.  If you run this code on LuaJIT, it will inline
the comparison in the lambda into the loop inside `min_element`, just
as G++ did, if it decides to JIT-compile it.  From my point of view,
it’s a little *more* abstract than the C++ version; while the C++
version is nailed down to specific concrete classes (`Proveedor` and
`Producto`) and to being called as a method, these details are omitted
from the Lua version, which is applicable to any object with a
`proveedores` property whose elements (as defined by `min_element`)
have `precio` properties that can be compared, and it can be called
either as a method or as a regular function.

If you run it in PUC Lua, which doesn’t do JIT compilation, it behaves
identically, just more slowly.  Is it less abstract when you run it in
PUC Lua?  I would argue not — it’s just that LuaJIT spends extra time
and complexity to reduce its hotspots to a *less* abstract form so
they run faster.  So ultimately the thing that the source code is an
abstraction *of* is not the bytecode but the computational process the
program carries out, or rather, the class of computational processes
the program *can* carry out given different inputs.

Now, let’s consider the introduction to Zed Shaw’s jeremiad, before
the part where he starts quoting dictionaries:

> I hate using badly designed APIs. I hate it even more when someone
> beats me over the head with words they were handed in some rhetoric
> class masquerading as a computer science course. Words like
> “abstract”, “pattern”, and “object oriented” are used like a shield
> to protect the implementer from critical words like “crap”,
> “complicated”, “obtuse”, and “annoying”. It’s even worse when the
> implementer realizes that if he implements the most complicated
> piece of shit possible then he can go rogue consultant and make tons
> of mad cash helping poor unsuspecting companies implement his
> steaming pile of bullshit. Harsh words? You bet. But I’m fed up with
> people imposing their faulty definitions and ideas on me without any
> way for me to easily fight back with a reasonable explanation as to
> why their crap is steaming. I’ve decided to start fighting back by
> coming up with a set of essays about programming that highlight
> common design misconceptions. This essay is about my top pet peeve:
> an abstract interface and an indirect interface are entirely
> different things.
>
> The point I’ll be trying to make throughout the essay is simple:
> Abstraction and indirection are very different yet cooperating
> concepts with two completely different purposes in software
> development.  Abstraction is used to reduce complexity.  Indirection
> is used to reduce coupling or dependence. The problem is that
> programmers frequently mix these up, using one for the other purpose
> and just generally screwing things up. By not knowing the difference
> between the two, and not knowing when to use one vs. the other or
> both, we end up with insanely convoluted systems with no real
> advantages.

While Shaw is mostly correct, abstraction and indirection are not as
separate as he claims.


> 4 · 5 = 5 · 4  
> 4 · *b* = *b* · 4  
> *a* · *b* = *b* · *a*  

The second claim can be applied to the case *b* = 5, but it is *more
abstract than* the first, because it is more general, and it achieves
that generality by making that reference to 5 *indirect*, which
enables you to also apply it to other things.  The third claim is more
abstract still, and can be applied to the case *a* = 4 to get the
second claim; by indirecting the reference to 4, we increase the
abstraction level of the claim.  We can consider what sorts of things
it might be true or false of; for example, it’s true of all (pairs of)
complex numbers and real vectors, but not most quaternions or
matrices.  If we want to add abstraction while ensuring that it’s
always true, we can resort to something like:

> ∀*a*, *b* ∈ ℂ: *a* · *b* = *b* · *a*  
> ∀*a*, *b* ∈ *A*: *a* · *b* = *b* · *a* where *A* is an abelian group

Note that this makes the statement *longer* and *harder to
understand*.  The second of these also adds another level of
indirection.  But to me it seems indisputably *more abstract*.  That’s
why the study of such claims is called “abstract algebra”, because
it’s more general than historical kinds of algebra that only concerned
themselves with real numbers.

That’s exactly the same thing that’s going on in our `mejor_proveedor`
function at the top; it’s applicable to any type of `proveedores` and
any type of `precio`, as long as they support the relevant operations
(here, iteration and comparison).

Let’s see what Shaw thinks about this:

> Abstraction and abstract definitions tend to look like this:
>
> - “a concept or idea not associated with any specific instance”
> - “the process of formulating general concepts by abstracting common
>   properties of instance[s]”
> - “the act of abstracting, the separating from the concrete. To
>   derive. To epitomize; to abridge, summarize, concentrate. A brief
>   statement of the chief points of a larger work”
> - [“]a condensed record or representation.”
>
> All of these definitions seem to discuss simplifying complexity and
> generalizing the concrete. They mention words I commonly associate
> with the reduction of some complex topic or idea into something more
> manageable. They also continually refer to generalizing something
> and combining repeated commonality which is also a means of reducing
> complexity.
>
> There’s one “definition” of abstraction that lays out the entire
> basis for my argument very clearly: “(v) The process of separating
> the interface to some functionality from the underlying
> implementation in such a way that the implementation can be changed
> without changing the way that piece of code is used. (n) The API
> (interface) for some piece of functionality that has been separated
> in this way.” Notice that this definition is entirely different from
> the others. It doesn’t mention words like “abridged, summarize,
> concentrate, concrete, general, or common”? Also notice that it is
> the *only* definition that is like this and it’s defined by[—]you
> guessed it[—]the programmers working on the Darwin project! Proof
> positive that programmers get this wrong right there from
> [G]oogle[.]
>
> Even more proof comes from the fact that Java uses the keyword
> “abstract” to create objects which actually support
> indirection. Think about it, the “abstract” keyword doesn’t reduce,
> summarize, or generalize a more concrete implementation, but rather
> creates an indirect path to the real implementation of that
> function. If this “abstract” class were to follow the previous
> definitions it would simply reduce the number of functions needed to
> use the actual concrete implementation. But, when you implement an
> “abstract” Java class you must implement the same number of
> functions including the abstract functions just to get it
> work. Ladies and gentlemen, this is indirection at its finest and
> abstraction left the building years ago.

I think I agree with Shaw that the Darwin definition doesn’t describe
abstraction *in general*; for example, it doesn’t describe the
hierarchy of abstractions I discussed above with respect to the
commutativity of multiplication.  It’s not even the only way to
achieve abstraction *in software*; C, for example, is more abstract
than VHDL, and SQL is more abstract than nested Python list
comprehensions, even though C is not an interface to VHDL, and SQL is
not an interface to nested Python list comprehensions.

Where we differ is on whether adding such a level of indirection
*necessarily* makes the code more abstract if it makes it possible to
change the implementation without changing the interface.  It does,
because, by virtue of that indirection, whatever code is written to
that interface is now generalized over all possible implementations of
the interface, in exactly the same way that 4 · *a* = *a* · 4 is
generalized over all numbers, and ∀*a*, *b* ∈ *A*: *a* · *b* = *b* ·
*a* is generalized over all abelian groups.
