Today I was reading "A Flexible Type System for Fearless Concurrency",
by Milano, Turcotti, and Myers, a PLDI '22 paper on a new language
where you can implement doubly-linked lists and stuff like that
easily but in a statically race-free fashion.

One of the things that I liked about their language (which they never
name) was its pattern-matching syntax:

    let some(x) = y.hd in {
        ...
    } else {
        ...
    }

I thought this was a really nice syntactic way to show the conditional
nature of the construct and delimit the scope of the binding for `x`
thus introduced, and also one that integrates smoothly with standard
`if` conditional syntax; it's easy and intuitive to see how you'd
alternate between `if` and pattern-matching.

There are still facilities you might desire that this syntax doesn't
make intuitive in a conventional imperative language — for example,
adding a Boolean condition to a match, matching against a pattern
containing a previously instantiated variable, and labeling a
subpattern of a match that is further destructured (OCaml's `as`).
But these are of secondary importance.

