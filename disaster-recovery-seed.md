Shae Erisson mentioned a wish for “a disaster recovery seed on my
drive, like, all of wikipedia and the source for my entire OS, and the
source for all the things I use.”

Well, Kiwix has Wikipedia, Project Gutenberg, Wiktionary, Stack
Overflow, Server Fault, the other Stack Exchange sites, Arch Wiki, Ask
Ubuntu, and WikiSource.  The Kiwix versions are somewhat suboptimal,
and so is the Kiwix software itself, and so is its file format, but
there doesn’t seem to be a better alternative for any of the three,
except in the sense that you can download the whole Wikipedia database
dump, and that is more complete.

The SubDivx database has the (Spanish-only) screenplay of basically
every mass-market movie, and it’s on archive.org.

OpenStreetMap has a map of the world down to the city block level, but
doesn’t include Blue Marble style data or elevation data (DEMs).

Debian’s source code archive should in theory be able to rebuild
everything in Debian, but it’s a constant struggle keeping it that
way.  I think it’s available via torrent and also jigdo.  Maybe it’s
worth it to include previous versions of Debian.

The bootstrappable.org project is working on getting that source code
to be really the source code, so that you really can rebuild the
entire distro from source code and an initial bootstrap seed of a few
hundred bytes of machine code.  That’s gotten a lot of help from
Debian’s work on reproducible builds, but it’s further advanced with
Guix.

I don’t know of a similar downloadable repository for electronic
design information, things like SeRV, PicoRV32, LibreSOC, or OpenC906,
not to mention all the other components that you need for a full
runnable system; there’s lots of info on opencores but I think a lot
of the new stuff is only on GitHub.

On the hardware side, the famous Gingery series of home-machine-shop
books is widely available, but I believe not legally in Erisson’s
country, and I don’t know of a legal alternative.

Copying the entire corpus of granted US patents is [tentatively legal
in the US for the time being][0], and German, Swiss, and UK patents
also.  Patents are not a very good source for technical background
information but in many cases they’re the best thing we’ve got.  US
patents up to 02015 can be [downloaded in bulk from Google][1], and
evidently post-02015 US patents are available in bulk [directly from
the USPTO][2].

[0]: https://en.wikipedia.org/wiki/Copyright_on_the_content_of_patents_and_in_the_context_of_patent_prosecution#United_States
[1]: https://www.google.com/googlebooks/uspto.html
[2]: https://www.uspto.gov/learning-and-resources/bulk-data-products

The Bureau of Naval Personnel has developed [a series of textbooks][4]
for training sailors on things like electronics, drafting, use of hand
tools, and aircraft engines.  These are in the public domain because
they were developed by the US government.  Most of these are outdated.

[4]: https://archive.org/details/navpers

There’s a torrent of the Creative Commons and GPL designs from
Thingiverse at
magnet:?xt=urn:btih:821c125364472b3d1cb29196aeeee90814d926be&dn=thingiverse-1-1&tr=https%3A%2F%2Ftracker.hama3.net%3A443%2Fannounce
and it doesn’t seem to be totally dead.  It’s 348.3 GB, and
magnet:?xt=urn:btih:ac040dc8f9d26bd0bad0592829ddede6355a4ccb&dn=thingiverse-1&tr=https%3A%2F%2Ftracker.hama3.net%3A443%2Fannounce
is a second torrent that brings it to 2088 gigabytes (that’s a torrent
in which 1.tar is broken), which
