I was just playing with [rxi’s MicroUI][0] because [tekknolagi and
akkartik wrote a Fenster backend for it last weekend][1].  Using a
reasonably convenient IMGUI API, in 1121 lines of C, it draws a
reasonably creditable-looking (if flat) WIMP GUI, without heap
allocation, using only four primitives:

    typedef struct { mu_BaseCommand base; mu_Rect rect; } mu_ClipCommand;
    typedef struct { mu_BaseCommand base; mu_Rect rect; mu_Color color; } mu_RectCommand;
    typedef struct { mu_BaseCommand base; mu_Font font; mu_Vec2 pos; mu_Color color; char str[1]; } mu_TextCommand;
    typedef struct { mu_BaseCommand base; mu_Rect rect; int id; mu_Color color; } mu_IconCommand;

It deposits a sequence of these into a command buffer that your
application is supposed to iterate over every frame.  ([The reason for
the queueing is that MicroUI supports multiple overlapping
windows][6], and the application can iterate over them in any order,
but MicroUI wants to do the drawing operations in the
painter’s-algorithm order, so internally it links the windows together
in a linked list which it can sort by z-order.)

[6]: https://rxi.github.io/microui_v2_an_implementation_overview.html#z_ordering_in_a_fixed_sized_buffer

The semantics of the clip command puzzled me for a while; it turns out
that it has the same semantics as `glScissor`, which [doesn’t nest or
stack, as StackOverflow explained][2]; so you can make the clipping
rect larger as well as smaller, unlike in SVG, PDF, or PostScript.  So
no “unclip” command is needed; instead we see clip commands with the
rect {0, 0, 16777216, 16777216}.  Internally, MicroUI maintains a
clipping stack, but reports changes to it through this flat command.

The `mu_Font` type is just a `void*`, the idea being I guess that
MicroUI itself has no idea what kind of font specification might be
applicable.  I suspect an integer `id` like the icon specifier would
be more useful.

[2]: https://stackoverflow.com/a/14645403

The widget-level API it provides is considerably richer than this
small set of primitives, supporting things like floating windows,
popup menus, tree controls, sliders, checkboxes, word-wrapped
paragraphs, etc.  For example:

      if (mu_begin_window(ctx, "Style Editor", mu_rect(350, 250, 300, 240))) {
        int sw = mu_get_current_container(ctx)->body.w * 0.14;
        mu_layout_row(ctx, 6, (int[]) { 80, sw, sw, sw, sw, -1 }, 0);
        for (int i = 0; colors[i].label; i++) {
          mu_label(ctx, colors[i].label);
          uint8_slider(ctx, &ctx->style->colors[i].r, 0, 255);

    ...

          if (mu_begin_treenode(ctx, "Test 2")) {
            mu_layout_row(ctx, 2, (int[]) { 54, 54 }, 0);
            if (mu_button(ctx, "Button 3")) { write_log("Pressed button 3"); }
            if (mu_button(ctx, "Button 4")) { write_log("Pressed button 4"); }
            if (mu_button(ctx, "Button 5")) { write_log("Pressed button 5"); }
            if (mu_button(ctx, "Button 6")) { write_log("Pressed button 6"); }
            mu_end_treenode(ctx);
          }
          if (mu_begin_treenode(ctx, "Test 3")) {
            static int checks[3] = { 1, 0, 1 };
            mu_checkbox(ctx, "Checkbox 1", &checks[0]);
            mu_checkbox(ctx, "Checkbox 2", &checks[1]);
            mu_checkbox(ctx, "Checkbox 3", &checks[2]);
            mu_end_treenode(ctx);
          }

I don’t want to overstate the case; the `mu_textbox` in the demo app
doesn’t implement so much as arrow keys or mouse click to reposition
the cursor, despite rxi having written [a very nice overview of the
basic behavior users expect from text editors nowadays][7].

[7]: https://rxi.github.io/textbox_behaviour.html

Record and replay
-----------------

So I hacked together a simple ASCII text format to represent these
four drawing operations, plus and their timing and division into
frames, in such a way that I could replay the UI afterwards.  The
format looks like this:

    # MicroUI metafile v0
    rect 350 250 300 240 #323232(255)
    rect 350 249 300 1 #191919(255)
    rect 350 490 300 1 #191919(255)
    rect 349 249 1 242 #191919(255)
    rect 650 249 1 242 #191919(255)
    rect 350 250 300 24 #191919(255)
    text 355 253 #f0f0f0(255)Style Editor
    icon 1 626 250 24 24 #f0f0f0(255)
    ...
    rect 159 414 1 22 #191919(255)
    text 154 416 #e6e6e6(255)100.00
    rect 250 367 73 74 #5a5f64(255)
    text 264 395 #e6e6e6(255)#5A5F64
    flush 1726121161.174904

    rect 350 250 300 240 #323232(255)
    ...

This took about 160 lines of C for the prototype replay code (using
the same OpenGL SDL renderer as the demo itself) and 80 lines of C for
the recording.  It took me about four hours, about one of which was
spent trying to figure out why my main loop never drew anything — I’d
managed to always set the alpha to 0, so all the colors I was drawing
were completely transparent.

I ran MicroUI’s 800×600 demo app to make some recordings; at 60fps it
generates about a megabyte per second of text, but that gzips to about
10 kilobytes per second, which seems to me to be fairly reasonable for
a 60fps half-megapixel WIMP GUI screen recording.  A single frame is
about 18 kilobytes, gzipping to 2.8K.  Despite the textual format,
playback is about 1000fps, 30× realtime.

[0]: https://github.com/rxi/microui
[1]: https://bernsteinbear.com/blog/fenster-microui/

Ben Sittler had suggested using a subset of the SVG format for the
textual representation, which is a reasonable thing to do, but I
didn’t do it in this case.  It would be significantly less pleasant to
read and write than this simple Unix-like format.

This is sort of similar to [a much simpler, unidirectional version of
Caleb Winston’s stdg][4], with only five commands rather than stdg’s
32 or so.

[4]: https://github.com/calebwin/stdg

This is exciting.  There are a lot of possibilities to explore and a
lot of obvious problems, especially in my protoype.

Expressivity
------------

This set of graphical primitives is much lower in expressivity than
conventional GUI libraries or HTML, closer to ANSI art.  But it’s
still plenty expressive for WIMP UIs, including proper proportional
fonts, kerning, margins, etc.

Generally, when we give up expressivity in a data format, we can get
tractability and manipulability in return.  So what kind of
manipulability can we hope for here?  The simplest answer is that we
can implement more backends.

I’ll revisit this question more below.

Fonts
-----

The demo application doesn’t use `mu_Font` at all, so I didn’t record
it in my metafile.  The demo renderer iterates over the text byte by
byte, appending a quad for each character to a bunch of OpenGL polygon
buffers, each quad referencing a texture atlas containing a bitmap
font:

    void r_draw_text(const char *text, mu_Vec2 pos, mu_Color color) {
      mu_Rect dst = { pos.x, pos.y, 0, 0 };
      for (const char *p = text; *p; p++) {
        if ((*p & 0xc0) == 0x80) { continue; }
        int chr = mu_min((unsigned char) *p, 127);
        mu_Rect src = atlas[ATLAS_FONT + chr];
        dst.w = src.w;
        dst.h = src.h;
        push_quad(dst, src, color);
        dst.x += dst.w;
      }
    }

So it always uses the same font and metrics.

I was puzzled as to how MicroUI accessed the font metrics so it could
do word wrap.  Turns out there are callback function pointers in the
`mu_Context` struct for this:

      int (*text_width)(mu_Font font, const char *str, int len);
      int (*text_height)(mu_Font font);
      void (*draw_frame)(mu_Context *ctx, mu_Rect rect, int colorid);

These are set up in `main()`, something I’d somehow failed to notice:

      mu_Context *ctx = malloc(sizeof(mu_Context));
      mu_init(ctx);
      ctx->text_width = text_width;
      ctx->text_height = text_height;

Why `malloc` is used here is anybody’s guess.  After a check for
whether len==-1, these functions invoke functions in the renderer
which likewise just add up the widths in the font atlas:

    int r_get_text_width(const char *text, int len) {
      int res = 0;
      for (const char *p = text; *p && len--; p++) {
        if ((*p & 0xc0) == 0x80) { continue; }
        int chr = mu_min((unsigned char) *p, 127);
        res += atlas[ATLAS_FONT + chr].w;
      }
      return res;
    }

    int r_get_text_height(void) {
      return 18;
    }

This code is [unchanged in tekknolagi and akkartik’s Fenster
backend][3] at the moment, even though it doesn’t use OpenGL,
alpha-blending the pixels into the framebuffer by hand instead.

[3]: https://github.com/tekknolagi/full-beans/blob/trunk/renderer.c#L150

This is of a lot of interest to me right now because font metrics are
a major Achilles heel for metafile rendering quality; it’s easy to get
wrong font metrics that result in text rendering errors that range
from ugly to indecipherable, because text that is less wide than
expected leaves an unintended space (possibly in the middle of a
word), while text that is wider than expected may overlap something
else.

The approach that the PDF Association has taken in recent years,
preceded by the PDF/A archival specification, is to require PDF files
to embed any fonts they use, other than the core fonts defined in the
PDF specification.

Icons and text rectangles
-------------------------

The font atlas in the demo renderer begins:

    static mu_Rect atlas[] = {
      [ MU_ICON_CLOSE ] = { 88, 68, 16, 16 },
      [ MU_ICON_CHECK ] = { 0, 0, 18, 18 },
      [ MU_ICON_EXPANDED ] = { 118, 68, 7, 5 },
      [ MU_ICON_COLLAPSED ] = { 113, 68, 5, 7 },
      [ ATLAS_WHITE ] = { 125, 68, 3, 3 },
      [ ATLAS_FONT+32 ] = { 84, 68, 2, 17 },
      [ ATLAS_FONT+33 ] = { 39, 68, 3, 17 },
      [ ATLAS_FONT+34 ] = { 114, 51, 5, 17 },

Because there are only four icons: the close button, the checkbox, and
the expanded and collapsed indicators.  I guess the difference between
the icon command and the text command is that the UI library has the
freedom to stretch an icon over whatever area it wants to, but
possibly including a width and height in the text command would be a
better way to do that.  Then you’d just have to assign four codepoints
to the desired icons.

That would considerably ease the font-metrics problem mentioned above;
two adjacent pieces of text would remain adjacent regardless of
font-metric variation, which would instead result in condensing or
expanding the width of the text.  This might reduce rendering quality,
but generally the resulting visual problems will be much smaller than
those resulting from screwing up the layout.

A benefit of this is that you can change the font of an
already-laid-out screen without making it unreadable.

Additionally, this would make the representation fully anisotropically
zoomable.

The disadvantage of this is that it *requires* the representation to
be fully anisotropically zoomable; the renderer can no longer render
text simply by blitting precomputed glyph bitmaps into a framebuffer.
For a renderer like the OpenGL one in the MicroUI demo, that’s no
problem, nor for a renderer using a PostScript, PDF, SVG, HTML canvas,
or HTML+CSS backend, but there are renderers that will get slower.

This reduces the command set from {clip, text, icon, rect} to {clip,
text, rect}.  You could imagine going an additional step by unifying
the text command with the rect command, leaving only {clip, text}, by
giving the text command a background color as well as a foreground
color.  The equivalent of the current text command would be using a
transparent background, and the equivalent of the current rect command
would be using an empty text string.  I think this is probably not a
good idea because the times when you want a background color change
exactly aligned with the text’s bounding box are almost
never — basically only for showing a text selection by highlighting.

Snapshots
---------

Above, I said a single frame of this 800×600 UI gzips to 2.8
kilobytes.  As far as I can tell so far, each frame is completely
independent; not only is the UI API immediate-mode, but so is the
rendering.  So you can store a precanned frame in a file and cat it.
In effect, such a sequence of drawing commands is a (relatively weak)
vector graphics format.

This is also a feature of Caleb Winston’s stdg, but stdg has more
graphics state; like SVG, PDF, PostScript, `<canvas>`, OpenGL, and
GDI+, but unlike MicroUI commands, it has a mapping between the
logical coordinates in the file and physical device coordinates that
you can change over time, allowing the same sequence of graphical
operations to appear at different places, different sizes, or
different orientations.  The only graphics state used by MicroUI
commands, by contrast, is the current clip rectangle, so if you cat a
precanned frame at different times, the only possible difference in
what you see will be that a different part of it will be visible.
MicroUI doesn’t even have a “current point”.

You can effect some of these transformations by relatively simple
transformations on the command sequence, though.  If you want to
translate the graphical operations to somewhere else on the screen,
you can just add constants to the X and Y coordinates in each drawing
command:

    rect 350 250 300 24 #191919(255)
    icon 1 626 250 24 24 #f0f0f0(255)
    clip 132 565 263 13
    text 137 566 #e6e6e6(255)Background Color

This gets both easier and more powerful with my proposed combination
of `icon` and `text` into one command, because you no longer need two
separate cases for the two separate commands, and because you can now
anisotropically *scale* a command sequence as well as merely
*translating* it.

Parseability
------------

You can *almost* parse my ad-hoc text format with `line.split()`, but
not quite, both because whitespace is significant within and following
text strings, and because the omnipresent alpha is always glommed onto
the color.  However, you can always identify each line by the first
word (indeed, the first byte).

I think I should switch to using tab-separated values and making alpha
a separate field, because tab is not a valid value in a text string,
and because I’d be quoted-printable-escaping it if it did occur.  It
would maybe be good to add some common character to the
quoted-printable escaping list, like space, so that you can easily see
that that field is quoted-printable encoded.  Right now there are zero
`=` characters in the entire metafile.

Right now I’m using `sscanf` to do the parsing, which has the
difficulty that it interprets a space delimiter as meaning “any number
of spaces”, which could eat leading spaces that were actually part of
the value of the field.  Quoting them would prevent that; I think
using a tab delimiter might also prevent it.

It would be kind of nice if the file were easily parseable as Tcl or
bash commands, but I think that conflicts with simple representation
of text strings.  It suggests not using parentheses, though, and not
using hash marks.

I’m also putting a redundant blank line between frames.

The stdg format instead puts text strings on a following line of their
own.  That would avoid the possibility of delimiters occurring inside
the text string, but it poses a risk of confusion if a text string is
also a valid command or a blank line.

A nice feature of the file in its current state — the result of the
lack of expressivity mentioned in the previous section — is that it’s
almost entirely stateless, so you can easily find, for example,
everything drawn in a particular color.  And you can snip out a
snapshot or a subsection very easily; there’s no dependency.

You *can’t* select out just the commands that affect a particular part
of the display, because you don’t know the whole area affected by
`text` commands.  This seems like another reason in favor of adding
width and height to them.

Miscellaneous deficiencies in the current sketch
------------------------------------------------

The window size and title are not being recorded in the metafile.  As
it happens, my replay program uses the same 800×600 size as the demo
program, but that’s the same sort of coincidence as using the same
font.

MicroUI relies on the surrounding environment to draw the mouse, so
there are no mouse-drawing commands included in the recorded metafile.
Consequently you can’t tell where the mouse is, though mouseover
effects and moving sliders sometimes give you some idea.

The replay program currently has buffer overflows in processing of
text-drawing operations.  The buffers are large enough that my
existing test files don’t overflow them.

Cheap tricks
------------

The very simple nature of this representation facilitates all sorts of
fun tricks at playback time.

### rot13 ###

I modified the replay program to display all the text in rot13 by
calling this function on each string before display:

    void rot13(char *s) {
      while (*s) {
        char c = *s & ~32;
        *s++ += 'A' < c && c < 'N' ? 13 : 'N' < c && c <= 'Z' ? -13 : 0;
      }
    }

This sometimes results in layout problems, because the new text is not
the same width as the original text; including the rectangle size in
the text command would facilitate this transformation too, but the
ease of implementing rot13 is hardly a central criterion for this sort
of thing.

A more interesting transformation that would also benefit from this
would be passing all the strings through a lookup table or an
automated machine translation system to translate a UI into a
different natural language.  Doing it after the layout level would
have some problems with word-wrapped text.

### Hollywood encryption text ###

To make all the text flutter, instead of calling `rot13`, I tried this:

    for (char *s = cmd->str; *s; s++) *s += rand() % 4;

This causes all the text to vary at random every frame, resulting in a
very flickery and disconcerting effect.  This often results in it
spilling out of its containers in a very “CSS is awesome” way, except
that of course this time I can’t blame CSS.

By putting

    srand(0);

in the per-frame loop, the randomized text becomes relatively stable,
only changing when previously-drawn text changes.

### Positional jitter ###

By adding a random positional error to each rectangle in each frame,
the replayer gets a nervous vibration effect:

    re->x += rand() % 4;  re->y += rand() % 4;

Because this is only applied to the rectangles and not the text, it
doesn’t impede readability much, though it is distracting.

As before, by reseeding the random number generator each frame, this
can be converted from a 60Hz jitter into something that’s fairly
consistent from one frame to the next; it gives the GUI a slightly
distressed or antiqued look.

### Color inversion ###

By inserting a call to this function in each of the three places that
decodes a color:

    void map_color(mu_Color *c) {
      c->r = 255 - c->r;  c->g = 255 - c->g;  c->b = 255 - c->b;
    }

the whole UI takes on the appearance of a photographic negative.  (Of
course, doing this *properly* would involve translation to a linear
color space and back.)

(Unifying `icon` and `text` would reduce that to two places.)

### Transparency ###

By changing `map_color` to instead reduce alpha, all the previously
opaque windows in the recording become translucent, and we can now see
things that were previously hidden:

    c->a /= 2;

### Flicker ###

If `map_color` instead adds a random offset to each color, the colors
flicker like an analog TV set receiving a weak signal:

    int clamp(int c) {
      return c > 255 ? 255 : c < 0 ? 0 : c;
    }

    void map_color(mu_Color *c) {
      c->r = clamp(c->r + rand() % 15 - 7);
      c->g = clamp(c->g + rand() % 15 - 7);
      c->b = clamp(c->b + rand() % 15 - 7);
    }

Other simple color filters include solarization, tinting, and
desaturation.

There are other possible ways for transformations to depend on time
other than being always constant and being randomly different every
frame, too.  Mapping space in a time-dependent way can provide camera
shake, and with the ability to do scaling, also things like
page-turning effects; globally mapping colors in a time-dependent way
can provide fades and lightning-storm effects.

### Slowdown ###

The replay code contains a `usleep` to ensure that it doesn’t run
faster than the original:

    double replay_delay = (now.tv_sec - r->replay_timestamp.tv_sec) +
      1e-6 * (now.tv_usec - r->replay_timestamp.tv_usec);
    double record_delay = when - r->record_timestamp;
    if (replay_delay < record_delay) {
      usleep(1e6 * (record_delay - replay_delay) + 0.5);
    }

It’s easy to make it play back slower than real time:

    record_delay *= 3;

(There’s some kind of bug here, actually, because just multiplying by
2 barely makes a difference; this makes it play back at about
two-thirds of the real-time speed instead of one-third.)

You ought to be able to speed up playback this way too.

Transports
----------

Could you use this as a remote bytestream protocol for remote access
to GUIs, similar to how VNC is used?  For example, over a serial port,
or over I²C, or over SSH, or talking to a process running inside a
Docker or Podman container.  You’d need some kind of bytestream going
in the other direction for input events like mouse and keyboard events
(stdg makes these polled, which is an interesting choice) but that’s
an easy problem to solve.

If each frame is 18 kilobytes, then a marginally usable
100-millisecond response time implies 180 kilobytes per second, about
1.4 megabits per second.  Nowadays it’s common for UARTs to be able to
handle that, but even relatively simple compression like ncompress
(LZW with up to 12-bit tokens) reduces it 10× even without any
protocol changes, so more like 140kbps.  gzip reduces my 33-megabyte
test demo file here by 100× (≈14kbps), and zstd -9c reduces it 780×
(≈1.8kbps).

This makes it seem eminently feasible.

Another very interesting potential transport is a shared file, written
at leisure by one program and read at leisure by one or more others,
possibly over a network, possibly prompted by asynchronous filesystem
change notifications.  (See file `filesystem-gui.md` for some notes on
this kind of thing in the raster context.)

For that kind of use, though, this protocol’s limitations would start
to chafe.  Alpha-blended translucency is cool, but you have no play
buttons, no custom fonts, no custom icons, no other raster images, no
white text outlined in black, no background gradients, no grainy
textures, no rotated text, no rounded corners, no bevels meeting at
diagonal boundaries, no fuzzy drop shadows, no 3-D polyhedra, no
schematics, no waveform views, no line plots, no bézier arrows, no
*diagonal lines*.  It’s so primitive that, not only does it omit
helpful UI legibility clues, it rules out large classes of
applications.

(You can do scatterplots, though, because you can position `.`
characters or one-pixel rectangles with whatever precision your
renderer uses.  Maybe you could scale and stretch slash and backslash
characters and overlap them to get arbitrary diagonal lines.)

Out of all these wishlist features, I think the ones with the highest
benefit-to-cost ratio would be linear gradients and polygons.  If you
can fill triangles or quads with linear gradients accurately and
reasonably efficiently, you can draw diagonal lines, arrowheads, line
plots, waveform views, schematics, 3-D polyhedra (Gouraud shaded),
bevels meeting at diagonal boundaries, approximately rounded corners,
background gradients, and play buttons, though not general custom
icons.  It doesn’t give you fuzzy drop shadows, really round shapes,
grainy textures, custom fonts, or bézier arrows.  But it adds very
little conceptual complexity and, for many uses at least, not too much
bandwidth or CPU.

Advantages of a user-agent design
---------------------------------

The above section is largely about how you could run a “terminal
emulator” running elsewhere than the actual application in order to
render such a stream of drawing commands — across a network, or a
serial line, or inside and outside a Docker container.  But, even when
running in the same place, it could have some advantages to run a
separate program that acts as a “terminal emulator” or “display
server” or “window manager” or “compositor” or “user-agent”; it can
provide certain kinds of common functionality in an orthogonal fashion
across many applications.  For example:

- Text copy and paste: the agent can see the text strings being
  displayed and the sequence in which they’re drawn.  If the user has
  a way to command the agent instead of the application, copying an
  individual text string is simple enough, and so too would sending it
  back to the application as a series of keystrokes, or as a specific
  paste message.  Another optional drawing operation would suffice to
  separate unrelated flows of text from one another.

- Zoom: adding width and height to the text command makes it possible
  for the application to zoom by changing the number of units in each
  width and height in its commands, but a user agent can zoom in and
  out by changing the mapping of those units to pixels.

- Visual filters: above I described a variety of filters that I have
  applied in my replay program; a user agent can similarly remap
  colors or coordinates in more elaborate ways than just zooming.
  
- Video recording and replay: a user agent can record past frames,
  allowing the user to rewind through them and replay them, perhaps
  zoomed in, and to perform full-text search on the text strings
  displayed.  The user-agent can display a draggable timeline when
  you’re looking at past history.
  
- Screenshot export: not only can the agent record past history and
  show it to you, it can export it, including in vector-graphics
  formats like SVG.  And this is a screenshot with searchable and
  copyable text.

- Graphical copy and paste: you can take a screenshot of only a part
  of the screen, using the same metafile format, and plausibly you can
  squirt it down the same bytestream channel to an application, though
  perhaps sending it as tens of thousands of keystroke events would be
  suboptimal.  You can even do this retrospectively and for historical
  time sequences.

- Documents, hypertext, and seeking: one word for a sequence of
  vector-graphics images is an “animation”, but another word is
  “ebook”.  Each frame is in effect a losslessly-compressed
  vector-graphics-format page, and by replaying a single frame
  (perhaps clipped or otherwise transformed) you can see the page.  By
  seeking to a different place in a file of such frames and replaying
  a frame from there, you can jump to a different page, not
  necessarily the next one.  (Seekability impairs compression
  somewhat, of course).  The metafile format is not a hypertext format
  by itself, but a header file that lists seek offsets and anchors for
  a metafile would make it one.  A frame-by-frame mode for the
  user-agent’s replay mode is already nearly an ebook reader.

- Editing input text before entry, perhaps augmented with language
  models; the AVNC Android VNC client has this feature, and it makes
  VNC dramatically more usable.  You type text into a text-entry field
  on the Android device, editing it with the Android keyboard and
  autocorrect configured to your preferences, and when you hit “send”,
  it sends presumably a bunch of keystroke events to the VNC server.
  (See file `yeso-vnc.md` and file `vncnotes.md` for more on this.)  A
  user agent for such a metafile stream protocol can do the same
  thing.

- Command-line history: as an extension to editing input text before
  entry, you can recall previous textual command lines.  This kind of
  facility really benefits from deeper integration with the
  application for things like tab-completion, syntax highlighting,
  etc., but `rlwrap` and Emacs shell-mode demonstrate that it can be
  very useful even without that.

- Keyboard and mouse macro recording and replay: the user-agent can
  provide this more general automation facility.  In its simplest
  form, still very useful for UI regression testing and delta
  debugging, it just replays literal keystrokes and mouse coordinates,
  but smarter facilities are easy to imagine.

- Text font changes: the user-agent can change the font of existing
  text without relayout, which may improve its readability.

- Multiplexing different applications: if the user-agent can be
  connected to multiple applications at once, it can provide a zooming
  windowing system which supports switching your interaction between
  them.

- Text translation: the user-agent can translate text to different
  human languages, either using LLMs or a simple lookup table for each
  application.

Possibility of greater statefulness: better compression, more expressiveness
----------------------------------------------------------------------------

When you invoke the renderer’s drawing function, it’s convenient to
have all the necessary data available.  And extra parameters are
simpler than extra commands.  The command dispatch in the demo app
looks like this:

    switch (cmd->type) {
      case MU_COMMAND_TEXT: r_draw_text(cmd->text.str, cmd->text.pos, cmd->text.color); break;
      case MU_COMMAND_RECT: r_draw_rect(cmd->rect.rect, cmd->rect.color); break;
      case MU_COMMAND_ICON: r_draw_icon(cmd->icon.id, cmd->icon.rect, cmd->icon.color); break;
      case MU_COMMAND_CLIP: r_set_clip_rect(cmd->clip.rect); break;
    }

And this is less bug-prone than the alternatives.  On the other hand,
if I’m adding another drawing primitive and also another kind of
background that isn’t a solid color, it might be better for them to be
orthogonal.  And this is an apparent waste of bandwidth, maybe even
after compression:

    rect 603 422 8 1 #4c1919(255)
    rect 603 443 8 1 #4c1919(255)
    rect 602 422 1 22 #4c1919(255)
    rect 611 422 1 22 #4c1919(255)
    text 582 424 #e6e6e6(255)255
    rect 615 423 18 20 #4b4b4b(255)
    text 360 448 #e6e6e6(255)buttonhover:
    rect 439 447 40 20 #1e1e1e(255)
    rect 439 446 40 1 #4c1919(255)
    rect 439 467 40 1 #4c1919(255)
    rect 438 446 1 22 #4c1919(255)
    rect 479 446 1 22 #4c1919(255)

If there were “current background” and “current foreground” colors,
you could say (changing only this aspect of the notation, without
changing the others I’ve said I should change):

    bg #4c1919(255)
    rect 603 422 8 1
    rect 603 443 8 1
    rect 602 422 1 22
    rect 611 422 1 22
    fg #e6e6e6(255)
    text 582 424 255
    bg #4b4b4b(255)
    rect 615 423 18 20
    text 360 448 buttonhover:
    bg #1e1e1e(255)
    rect 439 447 40 20
    bg #4c1919(255)
    rect 439 446 40 1
    rect 439 467 40 1
    rect 438 446 1 22
    rect 479 446 1 22

That’s 303 bytes instead of 377, a 20% reduction, though perhaps
compression would make that not matter.  But this would facilitate
setting a background gradient instead of a background color.

If we also added a currentpoint and made all the coordinates
currentpoint-relative, plus a “home” command to reset currentpoint, we
could reduce the size further, replacing all the coordinates with
deltas from the previous coordinate:

    bg #4c1919(255)
    rect 0 -1 8 1
    rect 0 21 8 1
    rect -1 -21 1 22
    rect -9 0 1 22
    fg #e6e6e6(255)
    text -29 2 255
    bg #4b4b4b(255)
    rect 33 1 18 20
    text -255 25 buttonhover:
    bg #1e1e1e(255)
    rect 79 -1 40 20
    bg #4c1919(255)
    rect 0 -1 40 1
    rect 0 21 40 1
    rect -1 -21 1 22
    rect 41 0 1 22

That’s 276 bytes, another reduction of 7% of the original 377.  This
time, though, I think it might *increase* the compressibility of the
command sequence rather than decreasing it; for example, here, both
`rect 438 446 1 22` and `rect 602 422 1 22` turned into `rect -1 -21 1
22`, exposing the redundancy as a simple byte sequence, and I think
that kind of thing is common.  Indeed, for any 40×20 rectangle in
those particular colors, the last six lines will be identical except
for the `79 -1` in the first `rect`, and there is a long series of
these at that point in the file.

Somewhat similarly, to do text with a non-fuzzy drop shadow, you might
say something like this:

    fg #303030(255)
    text -29 2 hello, world
    fg #cdcdcd(255)
    text -2 -2 hello, world

Presumably if you’re doing this frequently then both
“\nfg #303030(255)\ntext ” and “\nfg #cdcdcd(255)\ntext -2 -2 ” are
going to be frequent strings, and most LZ variants won’t have any
trouble replacing the second copy of the text with a reference back to
the first one.

Of course we can also reduce the commands to one letter:

    b #4c1919(255)
    r 0 -1 8 1
    r 0 21 8 1
    r -1 -21 1 22
    r -9 0 1 22
    f #e6e6e6(255)
    t -29 2 255
    b #4b4b4b(255)
    r 33 1 18 20
    t -255 25 buttonhover:
    b #1e1e1e(255)
    r 79 -1 40 20
    b #4c1919(255)
    r 0 -1 40 1
    r 0 21 40 1
    r -1 -21 1 22
    r 41 0 1 22

That’s 235 bytes, 62% of the original.  It’s 159 bytes after
ncompress, while the original was 216, so when compressed it’s 74% of
the original.  (Probably the alpha should come before the color for
better compressibility.)

Making the commands more stateful in this way, aside from making them
more compressible, makes them more expressive, which makes them a
little harder to analyze; you can no longer just `awk '$1 == "rect" &&
450 < $2 && $2 < 460 {print}'`, and by looking at a bit of it on the
screen you no longer have any idea what part you’re looking at.  But
because this version of the commands would be implicitly parameterized
by the initial state of the system when they start, the same command
sequence in different initial states can produce different effects:

- If it does not change the foreground color, it can draw text in many
  different colors.
- If it does not go `home`, it can draw the same shapes in many
  different places.
- If it does not change the background, it can draw the same shapes in
  many different colors, or in gradients.
  
This might be useful for putting canned things into text files, I
don’t know.  Maybe it only matters for compressibility, in which case
it might not be worth the extra complexity and bug-proneness.

Adding gradients and quads?
---------------------------

The gradient command might look something like this (35 bytes, spaces
represent tabs):

    g 255 4c1919 439 446 7f3131 17 -55

It defines the color at two points, and thus implicitly a linear
gradient everywhere that projects perpendicularly onto the line
segment between them.  Probably the most common case is for these
points to be relative to the currentpoint, though that isn’t how I
wrote it here.

The quad command might look something like this (26 bytes):

    q 0 -1 44 -2 3 -18 -47 -3

This is four coordinate pairs; the first one is relative to the
currentpoint, and each of the others is relative to the previous one
in the sequence.  The currentpoint is left at the first point where
the quad implicitly closed, and it is filled with the current
background gradient or color.

Part of what motivates me to include this is that I think it will be
relatively easy to implement this in implementations like the MicroUI
demo renderer, which is rendering everything in quads anyway
(implemented as pairs of triangles).

Command-line editing integration for user-agents
------------------------------------------------

In 02007, [Steve Yegge placed a command shell first in his list of
requisites for a “great system”][5]:

> First: **every great system has a command shell**. It is always an
> integral part of the system. It’s been there since the system was
> born. The designer of the system couldn’t imagine life without a
> command shell. The command shell is a full interface to the system:
> anything you can do with the system in some other way can also be
> done in the command shell. Great command shells are a big topic in
> their own right (most of the essential properties of living systems
> are, come to think of it.) A rough sketch: a great command shell
> always has a command language, an interactive help facility, a
> scripting language, an extension system, a command-history facility,
> and a rich command-line editor. (...) All existing command shells
> are crap, but they are an essential component of building the best
> software that can be built today.

Now, of course, you don’t need this to be built into your graphical
interface layer.  You can build it as an application.  But, as I said
above, `rlwrap` and Emacs shell-mode demonstrate that building it
orthogonally to applications can also be useful.  What should the
interface between the application and the user-agent look like in that
case?

[5]: http://steve-yegge.blogspot.com/2007/01/pinocchio-problem.html

REST is maybe the most extreme case of this.
