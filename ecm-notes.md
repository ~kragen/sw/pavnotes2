I was thinking about electrolytic machining.

Some possible objectives that occurred to me:

- If I can get into the sharpening rather than electropolishing
  regime, things like files and saws should be easy to make out of
  pre-hardened steel or cast iron.
  
- A circularly symmetric mirror can be made by physically sweeping a
  flat segment around its axis.  The segment itself (of, for example,
  parabolic or elliptical shape) can be cut electrolytically
  previously.

- Selective electrolytic dissolution of aluminum foil on a
  nonconductive backing can produce a shape that can later be
  "stamped" on other metals.

- For such stamping, though, it might be more reasonable to cut small
  holes in a sheet of insulator, either mechanically, with
  photolithography, or by burning with a laser.  Polyethylene, PET,
  teflon, and kapton seem like promising materials.

- Small hand tools like dental picks, screwdrivers, can openers,
  tweezers, pliers, wrenches, drill bits, rulers, deburring tools,
  thread gauges, feeler gauges, scalpels, balances, etc., should be
  straightforward to single-point cut under 3DoF control.

- More interestingly still, tapered roller bearings, screw threads,
  threading taps and dies, double-throated worm gears, roller screw
  threads, micrometers, snap gauges, pin vises, jaw vises, indicators,
  lockable swivel arms, automatic center punches, etc.  Coil springs
  might need a different approach in most cases than carving them out
  of a solid block, just because solid blocks of spring steel are hard
  to come by.

- Dot-matrix printing onto metal should be relatively straightforward
  with an electrode array.

- I ought to be able to deal with even very hard metals this way, but
  it might be useful to use other metals too, or graphite, especially
  if they're easy to shape to make electrodes.

- Lacking screw threads, a minimal screw thread can be made by
  wrapping a thick bare wire around a metal cylinder with a thin
  electrolyte-soaked cloth tube in between.

Volume electrochemical equivalents
----------------------------------

The key figure is the volume electrochemical equivalent.  One micron
decimeter squared per amp hour is 0.01 milliliters per amp-hour or
2.78 nanoliters per coulomb, as explained in file
`electrolytic-cyclic-fabrication.md`.  The most common metals in
Gamburg and Zambari's textbook are lead (34.09 μm dm²/A h), tin
(30.33), zinc (17.11), divalent copper (13.26), nickel (12.29), and
trivalent iron (8.83).  They don't list aluminum because you can't
electrodeposit it in water, so I'll have to calculate its VECE.  My
calculation for copper seems to come up with a value correct within
1%:

    You have: 63.546 dalton / (8.96 g/cc) / 2 e
    You want: micron dm^2 / amp hour
        * 13.230962
        / 0.075580295

And for zinc:

    You have: 65.38 dalton / (7.14 g/cc) / 2 e
    You want: micron dm^2 / amp hour
        * 17.082755
        / 0.058538567

So these two calculations produce the same results given in the
textbook.  The corresponding calculation for aluminum:

    You have: 26.982 dalton / (2.70 g/cc) / 3 e
    You want: micron dm^2 / amp hour
        * 12.428832
        / 0.080458082

Magnesium, titanium, cobalt, chromium, and zirconium are too exotic
for me at the moment.

Iron
----

Iron is especially interesting because it's stronger and more
refractory than other common metals, it's relatively nontoxic, and
it's very, very, very cheap.

According to the VECE above, to dissolve a cubic centimeter of iron
into its poorly soluble ferric (trivalent) form will take 40800
coulombs; at 3 volts that would be 122 kJ, about 20 minutes at 100
watts.  This works out to 122 nJ per cubic micron.  For a 600 dpi dot
(42 μm) etched 20 μm deep, it's 4.3 mJ; 100 watts would be 23000 such
dots per second.  All these values are of course divided by the
Faraday efficiency of the setup.

Assuming automatic control, 20 minutes is a trivial amount of time to
wait for a complex part.  And I think 100 watts is a reasonable amount
of thermal power to flush out of a tabletop setup with water.

As I understand it, the ferrous (divalent) form is not only easier to
dissolve anodically but also is soluble in water at any pH, while the
trivalent ferric ion requires a pH below 3.5 to remain soluble; but,
in water with dissolved oxygen, the ferrous form tends to oxidize to
the ferric and precipitate out, so sludge filtering is essential, and
acidity is helpful.  Suitable counterions include nitrate, fluoride,
chloride, bromide, iodide, acetate, thiocyanate, perchlorate, and of
course sulfate.  Iron's Pourbaix diagram (or stability-field diagram,
using the Eh redox potential instead of the E° standard oxidation
potential) says the dissolution is at about -0.6 volts at pH below
about 9, above which you instead get insoluble hydroxide.

I think the other relevant half-reaction is O₂(g) + 2H₂O + 4 e− ⇌
4OH−(aq), whose potential is +0.401 volts.  So, in theory, unless I'm
understanding this wrong, an iron-air battery should produce about a
volt, so you need over a volt to cathodically protect iron from
rusting.  But I bet you need an extra volt or two of overpotential to
get the reaction to proceed at a reasonable rate.

Copper
------

Copper is interesting for several reasons: it's the bulk of brass or
bronze, in its pure form it's very conductive, it's pretty corrosion
resistant, and it has a nice color.  Speculum metal was two-thirds
copper and one-third tin, so electrolytic copper etching is likely to
be useful for optics.  Copper is also easy to buy as scrap in a highly
pure form, unlike pretty much any other metal.

Brass and bronze have much lower friction on steel than steel-on-steel
contact does and will not gall.

Copper's softness makes it a better lap material than steel.

Nickel
------

Nickel is interesting for plating ferrous objects, either as a hard
and corrosion-resistant finish on its own or as a flash before
copper-plating.  Also, though, nickel is more corrosion-resistant than
copper and far more than iron, so information engraved on nickel can
be considered permanent for archival purposes in air.

It's also considerably brighter than copper or speculum metal :)

In addition to nickel welding electrodes or nickel from metal
suppliers, many currently circulating Argentine coins are a nickel
silver which is 25% nickel by weight, or have a part that is.  See
file `argentine-coins.md`.

Unbootstrapping paths
---------------------

I'm not sure exactly what the ideal cyclic-fabrication ECM machine
looks like (see file `electrolytic-cyclic-fabrication.md`).  In theory
it should be pretty easy to get an *electrical* megavoxel per second
per electrode, but turning this into actual precision seems
challenging, and getting rapid cutting speeds also seems challenging
(see file `ecm-array.md` and file `electrolytic-wire-cutting.md`).

A key point is that important parts of an ECM machine need to be
insulating, but ECM itself can only cut conductive things, indeed,
only metallic-bonded things.  So there's a certain alternation of
generations necessary: cut metal with ECM to make a machine that can
shape some kind of non-metal material, such as wax, other
thermoplastics, clay, wood, soluble-silicate-bonded ceramics,
aluminum-phosphate-based ceramics, glass, greensand, thermosetting
resins, salt, granite, protein, concrete, lime mortar, etc.

This also means that the ECM machine needs to be assembled from parts
made from different materials, some conductive and some not; it can't
be printed in a single material.

Optics are likely to be critical to feedback control, and ECM is
especially well-suited to optics, but it can't cut glass.  Cutting a
mirror pattern into steel with ECM can be followed by plating it with
nickel; alternatively anodizing a mirror pattern into aluminum may be
sufficient on its own.
