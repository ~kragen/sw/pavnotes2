[Watching YouTuber Ivan Miranda replace printed PLA parts of his CNC
router with aluminum parts][1101] he cut on it from 10mm aluminum
plate.  The objective is higher rigidity, I think, and better heat
resistance.  It’s an interesting bootstrapping exercise.

I feel like his extremely rectangular frame (made of square aluminum
structural tubing) is sort of pessimized for rigidity; parallelograms
are the worst possible shapes if you want rigidity.  Rectangles are
simple to measure and cut, but they get all their stiffness from the
stiffness of the joints.  Also, though, making it out of metal is
silly if you’re after rigidity rather than strength.  Concrete,
mineral-filled plastics, and similar things are much cheaper per unit
of rigidity.  I think even particle board or MDF beats steel or
aluminum on cost per unit of rigidity.

(Also, more generally, I feel like using a gantry design with finicky
prismatic joints and cable chains in order to simplify your kinematics
calculations is a dumb tradeoff when you have computers, an insight I
credit to Don Lancaster.)

He leaves the parts anchored to the surrounding plate at a few tabs,
which he then punches out with a chisel and mallet, requiring hand
filing to get smooth edges.  In some cases he does the same with
holes.  But he has to do an enormous amount of hand finishing
afterwards anyway (drilling, tapping, countersinking, counterboring,
sanding, which he says totaled about a week), and in any case a 3DoF
router will only ever give you 90° edges, so on metal, they’ll cut you
unless you hand-file or at least debur them, which is perhaps why in
places he’s using work gloves despite working on a drill press.

![(photo from Miranda's video of punching out tabs)](./punch-tabs.jpeg)

In some cases, he was drilling at right angles, but I don’t understand
why he drilled holes that were perpendicular to the plate surface
instead of routing them.

I was amused that he zip-tied bits of plywood to his vise as soft jaws.

I like how he’s used slots from the edge that get wider as holders for
hex-head bolts; the threaded shank goes into the narrow part of the
slot, and the head fits into the wide part, but tightly enough to keep
it from turning.  It’s an interesting sheet-cutting-specific variant
on the “insert nut here” PLA FDM approach to getting threaded holes.
But I think that in general clips and wedges are superior to threads
when detail is free.

He didn’t really get much rigidity improvement; he measured 3mm
deflection under 10kg (98N) load with the PLA and 1.75mm deflection
with the aluminum parts.

[1101]: https://youtu.be/RDnGvhdGFEY
