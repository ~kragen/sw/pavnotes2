I just learned about [LMDB][0] and [its astonishing performance][1],
typically about an order of magnitude faster than SQLite without
sacrificing ACID transactions, and was led to thinking about
FlatBuffers, ZeroMQ, and Kafka.  A thing all of these have in common
is being in some sense "deserialization-free".

[0]: https://en.wikipedia.org/wiki/Lightning_Memory-Mapped_Database
[1]: http://www.lmdb.tech/bench/microbench/

Kafka and ZeroMQ are both very fast message queuing systems; ZeroMQ is
based on experience with AMQP, and by using a different design where
the messages are just blobs of bytes that may be routed according to a
byte prefix and which are not necessarily durable, achieves
performance about an order of magnitude faster than AMQP
implementations like RabbitMQ can.  Kafka takes another approach and
is even faster than ZeroMQ: the subscribers themselves are responsible
for keeping track of how far along they are in the log of messages, so
the message broker doesn't have a per-subscriber queue at all.

The Kafka broker validates each incoming batch of messages and then
copies it verbatim to the message log; there is no deserialization
step which parses the message into an internal format or serialization
step which produces a fresh log message block.  This takes
deserialization-freeness even further than ZeroMQ.

LMDB, like the LevelDB and Berkeley DB it was designed to surpass,
gets a lot of its performance by storing dumb blobs of bytes, much
like ZeroMQ and Kafka.  But it also gets a lot of performance from a
read-only `mmap` of the database file for read transactions, [even
though many users of `mmap` have been disappointed in its
performance][2] and from a single-writer MVCC design where readers
don't block writers.  Writes are done via pwrite() rather than mmap().
Reading via mmap() allows an arbitrary number of readers to zero-copy
read the same block of memory.

[2]: https://db.cs.cmu.edu/papers/2022/cidr2022-p13-crotty.pdf

It occurred to me that it would be interesting to have a Kafkaesque
message queueing system which additionally supported mmap the way LMDB
does, so that many applications could peruse the event log at their
leisure, efficiently skipping past events they weren't interested in.
No central message-broker process would be needed; as with SQLite and
LMDB, each process could acquire a writelock on an event log when it
had events to publish to it, then release the writelock once it had
finished adding the events to the log.  On Linux, inotify might be
sufficient to unleash the thundering herd of subscribers.

In the limit, the thundering herd will be less efficient than a
message-broker process waking up only certain subscribers, but the
efficiency crossover might happen at quite a number of subscribers.
And the subscribers can do computations that go beyond ZeroMQ's byte
string prefix match.

FlatBuffers is synergistic with this kind of deserialization-free
stuff because avoiding copying a database entry or enqueued message
isn't that useful if the first thing the application does with it then
is to parse it byte by byte before, perhaps, discarding it.
FlatBuffers lets you parse only the parts of the blob that are
relevant to you, so it seems like a good fit for this kind of thing.
