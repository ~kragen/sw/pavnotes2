I’ve suspended my Firefox processes so that my CPU usage is between 3%
and 7% according to `dstat 5`:

    : tmp; dstat 5
    You did not select any stats, using -cdngy by default.
    --total-cpu-usage-- -dsk/total- -net/total- ---paging-- ---system--
    usr sys idl wai stl| read  writ| recv  send|  in   out | int   csw 
      6   2  92   0   0| 419G  273G|   0     0 | 305M 1653M|2264M 5229M
      3   3  94   0   0|  82k   14k| 209B   74B|   0     0 |1325  2434 
      1   2  97   0   0|   0    62k| 185B   57B|   0     0 | 732  1058 
      1   2  96   1   0|4232k   64k| 173B   28B|   0     0 | 934  1480 
      2   2  96   0   0| 334k  333k| 440B  234B|   0     0 |1047  1444 
      2   3  95   0   0|   0    14k| 326B  137B|   0     0 |1230  2423 
      4   3  93   0   0|   0  6554B| 175B   43B|   0     0 |1379  2540 
      1   1  98   0   0|4915B    0 | 204B   76B|   0     0 | 610   888 
      3   4  94   0   0| 491k   19k| 220B   72B|   0     0 |1535  2606

This results in this laptop using 6.91 watts:

    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    6910000

Launching four infinite loop processes in another window to use as
much power as possible:

    : tmp; while :;do :;done&while :;do :;done&while :;do :;done&while :;do :;done&
    [1] 2441372
    [2] 2441373
    [3] 2441374
    [4] 2441375

This boosts power usage to a reported 10.58 watts, gradually rising to
29.5 watts:

    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    10579000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    23448000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    27675000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    28943000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    29526000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    29462000

The CPUs are running at 3.1GHz:

    processor       : 7
    vendor_id       : AuthenticAMD
    cpu family      : 23
    model           : 24
    model name      : AMD Ryzen 5 3500U with Radeon Vega Mobile Gfx
    stepping        : 1
    microcode       : 0x8108102
    cpu MHz         : 3141.507

But some of them aren’t; I wonder if maybe there are 8 real cores,
rather than the four cores with 2 threads each I believe and [which
WikiChip asserts][0], so 4 processes isn’t enough?  I launch another
four processes:

    : tmp; while :;do :;done&while :;do :;done&while :;do :;done&while :;do :;done&
    [5] 2441504
    [6] 2441505
    [7] 2441506
    [8] 2441507

[0]: https://en.wikichip.org/wiki/amd/ryzen_5/3500u

Yes, that pegs all the clock speeds and boosts power usage to a
reported 34.0 watts:

    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    33514000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    33844000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    33878000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    33926000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    33989000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    34018000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    34009000
    : tmp; cat /sys/class/power_supply/BAT0/power_now 
    33987000

Killing the CPU-wasting processes

    : tmp; kill %{1..8}

reduces power usage back down to about 6.2 watts:

    : tmp; while : ; do echo -n "$(date) "; cat /sys/class/power_supply/BAT0/power_now; sleep 15; done
    Sun Jun 30 09:05:16 PM -03 2024 7143000
    Sun Jun 30 09:05:31 PM -03 2024 6522000
    Sun Jun 30 09:05:47 PM -03 2024 6148000
    Sun Jun 30 09:06:02 PM -03 2024 6088000
    Sun Jun 30 09:06:17 PM -03 2024 6212000

So we can estimate that the CPU uses about 28 watts, plus the 3%
original CPU load, so about 29 watts.

If we estimate that the CPU can run 3 instructions per clock on
average on each of 4 cores, that’s 37 billion instructions per second,
working out to about 800 picojoules per instruction, which is pretty
frugal.

As of this writing I've been running Emacs for a week and it's used
34'15" of CPU time, which works out to about 100 million instructions
per second and 90 milliwatts.  Or maybe 50 million if the CPU has been
at half the max clock speed the whole time.

According to `valgrind --tool=cachegrind emacs`, my usual startup of
Emacs, plus a shutdown, takes 10.7 billion instructions.  With `-q` to
use the default configuration instead, it’s 2.3 billion; with `-q -nw`
to not bring up a window system, it’s 950 million; and with `-q -nw
--no-site-file` to not load all the Emacs packages I have installed in
/usr/share/emacs, it’s 595 million.

As a more precise test, to test Emacs as an IDE rather than GTK and my
proliferation of Elisp packages, I ran `valgrind --tool=cachegrind
emacs -q -nw --no-site-file` and wrote this C program in it:

    #include <stdio.h>

    int main(int argc, char **argv)
    {
      char buf[256];
      printf("What's your name? ");
      fflush(stdout);
      fgets(buf, sizeof buf, stdin);
      for (char *p = buf; *p; p++) if (*p == '\n') *p = '\0';
      printf("Oh, hi, %s!  Lovely to meet you!\n", buf);
      return 0;
    }

Syntax highlighting was enabled, but `-nw` runs it in the terminal.  I
compiled it, fixed bugs in it (at first I didn’t have any but I wanted
to ensure I was doing a fair test), jumped to error message locations,
jumped to system header files, looked up manual pages in it, ran a
unix shell in it, and ran the program in the Unix shell and interacted
with it.

This took 8,834,093,972 instructions and about four minutes.  This is
about 30–40 million instructions per second, not counting the
instructions of the compiler, shell, terminal emulator, and X-Windows
server; so 100 million per second with a more elaborate configuration
and larger files seems like a plausible estimate.

