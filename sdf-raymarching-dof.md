It occurred to me that, when you’re doing sphere tracing, you get a
clue about when your ray passes close to other scene objects on its
way to the final destination.  You know at what distance from the
camera this happens, so you know if these objects are out of focus.
This should mean you can treat this near miss as a partly transparent
material, enabling you to produce a defocused image of the foreground
object composited on top of the focused objects.

You want to use a low-resolution approximation of the out-of-focus
object to figure out what color to draw, especially in cases where you
actually hit the object.  It’s obvious to me how to get a
low-resolution approximation of a texture, but not a low-resolution
approximation of a 3-D object.
b
