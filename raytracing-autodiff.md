I was thinking about approaches to solving the rendering equation, and
it occurred to me that when you’re estimating the illumination at a
point, whatever calculation you do (whether tracing rays toward it or
away from it) can be submitted to automatic differentiation, which can
give you some kind of estimate of how that illumination changes in
that region.  In the case of photon mapping for caustics, this is a
direct factor in your result (if the place a ray lands on an opaque
surface changes slowly with its direction of emission from the light
source, that makes that place brighter, infinitely brighter in the
case of a caustic) but in general it tells you what areas have rapid
rates of change and therefore might merit more samples, and what areas
can instead be well approximated by a linear gradient.

Speaking of approximating things by linear gradients, a lot of
raytracing algorithms seem to produce impulsive noise.  Mathematical
morphology operations such as dilation and erosion are generally
better at removing such salt-and-pepper noise than linear filters,
since they can preserve sharp edges much better, and they can be
carried out at fairly high speeds by pipelined processors like the
ERIM Cytocomputer.  This may be a good way to improve raytraced
images.
