Steam flowing through a hot pipe, if allowed to condense on the other
side, can carry an enormous heat flux with very little viscous losses;
but it requires the pipe to be hot, or it condenses on the pipe
instead of the destination.  *Intermittent* steam flow should be able
to carry a *small* heat flux a long distance through a small pipe, and
can be produced by a pulse boiler using a positive-feedback reaction
similar to those that drive volcanic geysers, in which boiling at the
heat source accelerates once it starts.  However, for most purposes,
relying on the pressure caused by the weight of the water is
undesirable; instead, it should rely on the movement of the water
boiling to reconfigure the system to increase heat flux.

(This is not related to the so-called “geyser pump” which pumps water
efficiently with compressed air without any moving parts, except that
in both cases an oscillation driven by positive feedback pumps a fluid
through a pipe without any moving parts.  That mechanism *does* rely
on the weight of the water.)

When the steam pulse begins, we can assume that our small pipe has
cooled down to ambient temperature, so the first part of the steam is
consumed by heating up the pipe itself; this is a loss of the
mechanism.  We minimize the thickness of pipe walls that needs to be
heated before the pulse finishes by making the steam pulse very short,
and by using pipe walls with high insulance, low density, and low
specific heat.

It’s probably desirable for the inner pipe walls to contain enough
crevices or porosity to hold the condensation from the steam so that
it doesn’t block the steam that comes behind it; that way, we minimize
the time that the pipe is full.  However, viscous losses in the pipe
(whether from liquid or gas) convert mechanical work (originally
derived from the expansion of the steam in the pulse boiler) into
heat, which is either delivered to the destination, or used to heat up
the pipe walls.  But delivering heat to the destination is the
objective!  So the system’s energy losses are entirely from the cost
of heating up the walls to the necessary thickness, so it follows
that, for a given pipe diameter, we should make the steam pulse
through the pipe as rapid as we possibly can to minimize those losses.

Between pulses, the pipe is filled with a partial vacuum containing
only water vapor at its ambient vapor pressure, as in a heat pipe.
You could describe this system as an oscillating heat pipe; wall
porosity is a possible return route for the condensed water, but it
may be more desirable to have a separate pipe for the water return, so
that it can return more quickly than the wall porosity would permit.

But how rapid is “as rapid as we possibly can”?

The penetration thickness of the heat in the pipe walls is
proportional to the [thermal diffusivity][0] of their material and to
the square root of the pulse length.  So a 1-millisecond pulse will
diffuse heat 3.2 times less deeply into the walls than a
10-millisecond pulse, and lose 3.2 times less heat, assuming the
penetration depth is insignificant compared to the pipe diameter.  If
the pipe walls are effectively made of water, thermal diffusivity
0.143mm²/s, then in a millisecond we expect the characteristic length
to be about 12 μm.  If instead they’re made of AISI 1010 stainless,
18.8mm²/s, 140 μm.  If copper, 111mm²/s, 330 μm.  If we go back to the
water and lengthen the pulse to 10ms, it’s 39 μm.

[0]: https://en.wikipedia.org/wiki/Thermal_diffusivity

As I understand it, the heat deposited has an exponential decay with
depth.  If we just crudely pretend that all the material up to that
skin depth heats up to the boiling temperature, while all the material
deeper stays at room temperature, which is probably not wrong by more
than a factor of 3, 12 μm of a 1-mm-diameter tube gives us 38 mg of
condensed water per meter that we have to heat up (say from 20° to
120°) to get the steam through the tube, which works out to 16 joules
per meter.  So each 1-millisecond pulse loses 16 joules per meter.

A 100-millisecond pulse would lose 160 joules per meter, heating up
the walls to a thickness of 120 μm (a thick sheet of paper), but if
that has the same flow rate of steam at the same temperature, that’s a
loss proportionally ten times smaller.  So not only should the steam
flow as fast as possible in the pulse; it should flow *as long and
infrequently as possible*, so it’s flowing through an already-heated
pipe instead of a mostly cold one.

16 joules is enough to boil 7mg of water at 100° and atmospheric
pressure; 160 joules is enough to boil 70mg of water.  70mg of steam
at 100° should be 120 mℓ:

    You have: (70mg/((oxygen + 2 hydrogen)g/mol)) R tempC(100) / atm
    You want: ml
            * 118.97564
            / 0.0084050821

Sending that “one-meter-overhead steam” alone through a 1-mm-diameter
pipe in 100ms would require accelerating it to almost Mach 5:

    You have: 120ml/circlearea(.5mm)/100ms
    You want: 
            Definition: 1527.8875 m / s

So that probably isn’t going to happen.  If we increase the pipe
diameter to 4mm, we have 4 times as much wall area and therefore 280mg
per meter of overhead steam, occupying 480mℓ, reducing the necessary
speed to a regular rifle-bullet speed, just over Mach 1:

    You have: 480ml/circlearea(2mm)/100ms
    You want: 
            Definition: 381.97186 m / s

But if we need to transmit the heat over, say, 4 meters, as well as
transmit some larger amount of “payload steam” in addition to the
overhead, we probably end up closer to 6mm diameter and a second, or a
few seconds, for a practical system.  A 10-second pulse gives us a
heat penetration depth of 1.2mm, and thus 9500J per meter of overhead
steam:

    You have: pi 6 mm 1.2mm 1g/cc * 1cal/g/K * 100 K
    You want: J/m
            * 9463.985
            / 0.00010566373

Which is 4.2g/m of overhead steam:

    You have: 9500J/m / water_vaporization_heat
    You want: g/m
            * 4.2102464
            / 0.23751579

Which is 7.3ℓ/m under standard conditions:

    You have: (4.28g/m/((oxygen + 2 hydrogen)g/mol)) R tempC(100) / atm
    You want: ℓ/m
            * 7.2745104
            / 0.1374663

This requires 26m/s per meter:

    You have: 7.3ℓ/m/circlearea(3mm)/10s
    You want: m/s/m
            * 25.818469
            / 0.038731964

If we suppose we want to transmit five times that amount of steam
through the pipe, so that 20% is lost to heating the pipe but the
other 80% is delivered to the destination, we need 125 meters per
second per meter.  So for four meters we’re 500 meters per second,
just over the speed of sound again.

So, we boil 84 grams of water, producing 143 liters of steam, which we
then cram through this 6-mm-diameter pipe over 10 seconds at 500
meters per second.  20% of it condenses on the walls, heating them up
to a depth of a millimeter or two, but the other 67 grams arrives at
the destination and condenses there, depositing 150 kJ, for a power
delivery rate of 15 kW during those 10 seconds and 80% efficiency.

    You have: water_vaporization_heat * 67 g
    You want: kJ
            * 151.1788
            / 0.0066146841

This is assuming atmospheric pressure and boiling temperature, which
are clearly incorrect assumptions; you’ll need a lot more than an
atmosphere of pressure to drive the steam through the tube that fast
(overcoming the [pressure due to friction loss][5], which depends on
the [viscosity of steam][2], the [Moody chart][3], and the
[Darcy-Weisbach equation][4]), which means the boiling temperature is
higher, and so are the thermal losses to the walls of the tube.  And
15 kW isn’t that spectacular for such a big, strong pipe; 6 mm would
be ASTM B-258 AWG 2.8 wire.  [3-gauge wire is rated for 75-amp power
transmission][1], which would be 6 kilowatts at 240 volts, but it can
go a lot farther than 4 meters.  And the whole idea here is that this
is *intermittent* transmission, so maybe the average power is 1.5 kW
or 0.5 kW at a 10% or 3% duty cycle.

[1]: https://www.powerstream.com/Wire_Size.htm
[2]: https://physics.stackexchange.com/questions/174381/viscosity-for-steam-in-the-reynolds-number
[3]: https://en.wikipedia.org/wiki/Moody_chart#/media/File:Moody_EN.svg
[4]: https://en.wikipedia.org/wiki/Darcy%E2%80%93Weisbach_equation
[5]: https://en.wikipedia.org/wiki/Friction_loss#Calculating_friction_loss

The kinetic energy of 84 grams at 500m/s is 10.5 kJ, and less than 1%
of the water is in transit through the pipe at any given moment, so
clearly we have enough energy available.

Even if the flow inside the pipe isn’t actually supersonic in the
steam because it’s [lower in molecular weight and higher in
temperature and thus has a higher sound speed][6], and even if the
frictional losses aren’t really losses in this context, I think you’ll
have to keep your steam velocities far, far below atmospheric Mach 1
to avoid turbulent vortices ripping the porous lining off your pipe
walls.

[6]: https://en.wikipedia.org/wiki/Speed_of_sound#Dependence_on_the_properties_of_the_medium

Although the above sketch is missing probably a factor of 2 or 3 in
scaling up to become practical, I think these calculations validate
the basic idea: you can get transmit many kilowatts of power over
household distances through piping you can easily handle by hand, and
get reasonable efficiency, as long as you do it intermittently and
over only household distances rather than, say, municipal distances.

An advantage of having smallish amounts of water present, like 84
grams rather than a kilogram or ten kilograms, is that if a steam
explosion escapes the piping, it’s significantly less likely to kill
you.  A disadvantage is that you have to do pulses more often, which
means (for a given steam flow speed) that you lose more heat to the
tube walls.
