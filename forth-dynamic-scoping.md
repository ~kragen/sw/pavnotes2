I just noticed that it’s possible to implement Lisp-style dynamic
scoping in standard Forth in one line of code.  It turns out the
implementation works perfectly (within its limits) in all four Forth
implementations I could test it in, including F83 from 01984.

`co`, or `rswap`
----------------

[Van der Horst shows][0] a word that simply swaps the top two items on
the return stack, so that its caller’s caller will return to the point
in its caller that called `co`, assuming they aren’t using the return
stack for anything else:

    : co  2r> >r >r ;

His use case is stackless coroutines.  My first thought on seeing
this, though, was that you could use it to get a weak equivalent of
Golang’s `defer`.

[0]: https://home.hccnet.nl/a.w.m.van.der.horst/forthlecture6.html

Forth variables
---------------

In standard Forth, `variable`s and `value`s are sort of global:
although they are visible only in a lexical scope extending from their
declaration to the next declaration of another word by the same name,
and only within a given wordlist or vocabulary, their lifetime is
unlimited.  Consequently, if you want to use a `variable` or `value`
to store data for a recursive subroutine, you must explicitly save its
old value on entry and restore it on exit.

It’s useful sometimes to set a system state variable temporarily to
some new value and then restore it upon exit.  The `variable` `base`
used to determine the base in which numbers are printed is one
example, and for example GForth provides a `dec.` word to print a
number in decimal:

    hex 35 dec. 53  ok
    see dec. 
    : dec.  
      useraddr <70>  @ decimal swap . useraddr <70>  ! ; ok

(If your Forth doesn’t have a `dec.` word, this is confusing, because
`dec.` is a valid hexadecimal number, so you don’t get an error!)

It turns out that `useraddr <70>` here is `base`, as we can see by
compiling a word that sets it and then decompiling that word:

    : setbase base ! ; see setbase 
    : setbase  
      useraddr <70>  ! ; ok

Its old value is simply stored on the operand stack until `.` finishes
printing it.

(I think “useraddr” means “thread-local storage”: each thread, known
in Forth as a task or user, has its own `base` to read and print
numbers in.)

The evolution of variable scoping in Lisp
-----------------------------------------

Lisp traditionally used “dynamic scoping”, in which
a new binding for a variable, for example as a
parameter, is visible to its transitive callees.  For example, this
prints 5, not 3, in traditional Lisps:

    (setq x 3)
    (defun px () (print x))
    (defun q (x) (px))
    (q 5)

While this mechanism does support recursion, this sort of thing is
kind of bug-prone, so Common Lisp
switched the default to lexical (static) scoping, in which the
variable `x` in `q` is a different `x` which has nothing to do with
the global `x`, so for example in SBCL
the above example prints 3.  Even in
Emacs 28.2, though, it prints 5, though, as I understand it,
in other contexts Emacs Lisp has also switched to lexical scoping by default.

You can use the `let` construct to provide such a temporary value
within a single block rather than a whole function:

    (let ((x 7)) (px))

Common Lisp retained dynamically-scoped “special variables” because
they’re useful for things like [`with-output-to-string`][1]:

    * (with-output-to-string (*standard-output*) (prin1 5) (prin1 3))
    "53"

[1]: http://clhs.lisp.se/Body/v_debug_.htm

This macro invocation binds `*standard-output*` to a temporary stream
that writes to a string, evaluates the provided code (which may
implicitly use `*standard-output*`, as `prin1` does), and returns the
string.

Other parameters that it’s commonly useful to set temporarily in such
a way include graphics parameters (pen location, foreground color,
line width, current window, transformation matrix, current font, etc.)
and locale parameters (date format, string collating order).  Things
like this are the rationale for preserving the dynamic-scoping
facility in Common Lisp as “special variables”.  In my
.emacs.d/init.el, for example, I have this code (simplified) to invoke
`untabify` with the global default value for `tab-width` instead of
whatever it currently is:

    (let ((tab-width (default-value 'tab-width)))
      (untabify start end))

And this code to do a case-sensitive search even if case-insensitive
searching is the default:

    (let ((case-fold-search nil))
      (search-backward-regexp "[^\n \t][A-Z]" 0 t))

Another use for this facility is to limit the scope of *other* code's
modifications to such global variables; for example, Emacs has a
`deactivate-mark` variable which you can similarly locally bind to a
value to prevent commands that would normaly deactivate the mark from
doing so (by restoring its value upon return).

But, in older Lisps, including older versions of Emacs Lisp, the main
use of this mechanism was just normal local variables in possibly
recursive subroutines.  PostScript also uses dynamic scoping for this
kind of thing.

Deep binding and shallow binding
--------------------------------

The obvious way to implement dynamic scoping is to keep the entire
environment in an alist and do variable lookups with `assoc` or `assq`
or their moral equivalents.  On exit from a function, you just restore
the environment pointer to its previous value.  This is known as “deep
binding” and is, unfortunately, very slow, because every variable
reference requires a linear search through at least dozens and
potentially myriads of variable bindings.

So dynamic scoping is usually implemented with “shallow binding”, in
which the current value of each variable is stored at a fixed
location, and when a function wants to give it a local value, it saves
its old value on a stack on entry and restores it on exit.

Hacking the return stack to implement shallow binding in Forth
--------------------------------------------------------------

It occurred to me that you can use the approach demonstrated in
`co` to provide this kind of dynamic scoping in standard Forth,
allowing you to write `dec.` as follows:

    decimal : dec. 10 base let! . ;

This requires that `let!` somehow store the previous value of `base`
to be restored when `dec.` returns, as well as arrange for `dec.` to
invoke the restoration code when it returns.  `Co` can arrange for
`dec.` to invoke part of `let!` when it returns, so it can restore the
old value to `base`:

    0 value old  0 value where  : co 2r> >r >r ;
    : let! dup to where  where @ to old  !  co  old where ! ;
    decimal : dec. 10 base let! . ;

This does work in this example, but it only allows you to save one
variable at a time.  What you really need is to save the variable
address and its old value on a stack, ideally the return stack so you
don’t have to worry about how many inputs and outputs callers such as
`dec.` have.  Then instead of `old where !` you can say `2r> !`.  But
this requires a different return-stack effect; given `val a-addr` on
the operand stack and `dec.+n let!+m` on the return stack, you want to
switch to having `a-addr val let!+m dec.+n` on the return stack.  This
requires a combined stack manipulation something like `swap 2r> rot >r
rot >r >r >r`.  So our generalized `let!` looks like this:

    : (let!) dup @ over swap 2r> rot >r rot >r >r >r ! ; : let! (let!) 2r> ! ;
    decimal : dec.  10 base let!  . ;  hex 53 dup dec. .

This does seem to work when I test it in Gforth, PForth and PFE.  In
F83 it requires a definition of `2r>`.  After some flubs, including
crashing DOSBox, this worked:

    : 2r> r> r> r> rot >r swap ;

So, in one line of standard Forth, we have implemented the dynamic
scoping facility that defined local variables in old Lisps.

The dynamically-scoped Towers of Hanoi
--------------------------------------

Perhaps we can use it for Towers of Hanoi:

    variable src  variable dest  variable stor  variable n

    defer move-disc

    : text-disc
      cr ." Move disc " n @ . ." from " src @ emit ."  to " dest @ emit ;

    ' text-disc is move-disc

    : hanoi ( src stor dest n -- )
      n let!  dest let!  stor let!  src let!
      n @ 0= if exit then
      src @  dest @  stor @  n @ 1-  recurse
      move-disc
      stor @  src @  dest @  n @ 1-  recurse ;

    char A char B char C 4 hanoi

(I used `n @ .` because for some reason PForth doesn’t implement `?`.)

That seems to work just as you would hope
in Gforth, PForth, PFE,
and even F83 (except it requires `ascii` for `char`).
I’m told it even works in Christopher Leonard’s [zenv][3], a ZX Spectrum Forth!
(But only up to 3 discs, because 4 would require 65 items on the return stack)
And it demonstrates that
this form of “local variables” doesn’t impede you from factoring out
individual lines of code that use the variables.
Block-scoped lexical local
variables would.

[3]: https://github.com/veltas/zenv

Reflections on Forth
--------------------

This is the kind of thing that gets people excited about Forth: it's
such a malleable language that you can add fundamental facilities like
dynamically-scoped local variables to it in a single line of code.

Such things can be an attractive nuisance.  This implementation isn’t
very efficient; in an interpreted Forth (without superinstructions) it
requires, I think, 16 subroutine invocations per local variable.  A
native-code compiled Forth will require something like that number of
instructions instead, but your return-address branch predictor will
explode, so your performance will still go to hell.  And such things
are pretty confusing to debug when they go wrong — `(let!)` has a
sequence of nine stack manipulations in a row, most of them
return-stack manipulations.

And Forth tends to break easily.  In Gforth you *can* invoke `let!`
from the text interpreter usefully:

    3 x let! x ? 3  ok
    x ? 0  ok

but this behavior is not guaranteed.  Running `3 x (let!)` crashes
Gforth.  `Let!` doesn’t work inside a `do loop`, either:

    : (let!) dup @ over swap 2r> rot >r rot >r >r >r ! ; : let! (let!) 2r> ! ;  ok 
    variable x : xsum 0 do x @ i + x let! loop x @ ;  ok
    5 xsum . 
    :3: Return stack overflow
    5 >>>xsum<<< .
    Backtrace:

If you wanted to implement dynamic scoping in Forth in an efficient
way, you’d want to statically allocate space in a stack frame for all
the variables a colon definition created local bindings for, copy the
old values of those variables into that stack frame on entry, and copy
them out on exit.  This would add about two instructions to a call and
return sequence for a definition using this facility, plus two
instructions per variable.  And it would break return-stack
manipulations like this one.

A related problem is that Forth tends to disproportionately attract
people who like to spend their time hacking on the language
implementation rather than using it to write code to do something
else — both because you *can* hack on the implementation so easily,
and because you have to understand the implementation in order to
debug your failures.  This can sink projects in Forth.
