As I was shaving my face in the shower today with one of those
disposable razor heads with multiple blades mounted on springs, it
occurred to me that there are two main reasons it’s so easy to cut
yourself while shaving, and why a close shave is possible at all:

1. The direction in which the razor blade cuts and the resulting force
   on the razor blade are almost perpendicular to the forces that
   direct the razor blade, such as the spring force from within the
   razor and the skin force from without, so a small side force can
   direct a deeper or shallower cut that is performed with greater
   force.  In this case I think the cutting force is of the order 0.1
   N and the perpendicular force that sets the cut depth is of the
   order 0.01 N.
2. Because the area of the razor blade edge is smaller (≈1nm ×
   20mm/blade × 3 blades) than the area over which the propulsive
   force is applied (≈100mm²) a small pressure can produce a large
   pressure.  In this case, I think the razor is being driven by a
   contact pressure difference with my hand of the order 1kPa, but the
   cutting pressure at the razor’s edge is potentially closer to 2GPa,
   higher in places such as hairs where it encounters more resistance.
   However, the actual cutting pressure is probably at least an order
   of magnitude down from this because most of the face force on the
   razor is cutting drag, not the cutting-edge force itself.

Item #2 happened to be the one that struck my attention most.  It
results in the razor imposing the geometry of my soft face skin onto
my hard face hairs, to the point that what was once stubble now feels
smooth, at least in the direction the razor went.  Which means a
precision of ten microns or better; as [Natalie Angier writes][0]:

> Imagine you’re in a dark room, running your fingers over a smooth
> surface in search of a single dot the size of this period. How high
> do you think the dot must be for your finger pads to feel it? A
> hundredth of an inch above background? A thousandth?
> 
> Well, take a tip from the economy and keep downsizing. Scientists
> have determined that the human finger is so sensitive it can detect
> a surface bump just one micron high. All our punctuation point need
> do, then, is poke above its glassy backdrop by 1/400,000th of an
> inch — the diameter of a bacterial cell — and our fastidious fingers
> can find it.

Unfortunately, a 400,000th of an inch is 64 nanometers, 32 times
smaller than a micron.  Bacterial cells are indeed about a micron in
diameter.  Sadly, in typical New York Times fashion, Angier is more
concerned with making jokes about the economy than with the truth, a
concern which would involve citing sources so we can find out which
way she got her calculation wrong.

[0]: https://www.nytimes.com/2008/12/09/health/09iht-09angi.18522553.html "Primal, acute and easily duped: Our sense of touch"

[Sid Hazra, who seems to be a researcher in the field, says on
Quora][2a]:

> In haptics engineering, we consider the casual usable dynamic
> tactioception discrimination limit to be around 9 microns. So if you
> have a really smooth surface with a 10 x 10 x 10 micron cubical
> bump, you will feel it when you slide your (untreated/unprepared)
> fingers over it if you are mildly attentive. (...) The usable static
> feature discrimination limit, feature that you can feel when your
> skin is statically pressed against the feature (different
> mechanoreceptors are involved in detecting such features) is around
> 100 microns, human hair.

[2a]: https://www.quora.com/What-is-the-smallest-thing-you-can-feel-with-your-hands

[Johansson and Lamotte 01983][1] did the experiment and found that
people could feel edges as small as 0.85μm, but that tiny
40-μm-diameter raised dots had to be 6μm tall for people to feel them.
This is close to the size of the hairs on my face.  So I’m comfortable
with the assertion that the razor is cutting the hairs to a precision
of at least ten microns, but possibly not as good as a single micron.
Given that the whiskers are themselves over 100μm in diameter and not
rigidly clamped in place, a single micron of alignment is somewhat
implausible.

[1]: https://www.researchgate.net/publication/16516243_Tactile_Detection_Thresholds_for_a_Single_Asperity_on_an_Otherwise_Smooth_Surface/link/5a9ecfc7a6fdcc22e2cb451b/download "Tactile Detection Thresholds for a Single Asperity on an Otherwise Smooth Surface, doi 10.3109/07367228309144538"

This struck me as having interesting implications for mechanically
cutting fine surface features on metal or glass.

Consider how a cam-driven screw machine works: a steel cam presses on
a cam follower to set the position of a single-point cutting tool,
which exerts higher pressure on a workpiece than the cam exerts on the
follower, thus cutting the workpiece even if it is harder than the cam
is. This is possible thanks to the two dynamics I described above with
the razors, and also because the screw machine can incorporate other
mechanisms; for example, leverage or other mechanical advantage can
enable the cam to exert not just higher pressure but more *force* on
the cutting tool than the cutting tool exerts on the cam, at the
expense of requiring a larger displacement, and negative feedback
mechanisms can enable an arbitrarily small power to control an
arbitrarily large power, thus entirely severing the link between the
power at the cam and the power at the workpiece.

Consider the ultimate development of cam technology, the long-play
phonograph or gramophone record.  Spinning at 33⅓rpm, the
25-micron-diameter “microgroove” needle takes 39 minutes to follow the
groove to almost the border of the 100-mm-diameter label, reproducing
signals up to 25 kHz with [60—70dB signal to noise ratio][2] while
pressing into the groove with 20mN of force, the weight of 2 grams.
The innermost groove is [actually 56mm in radius][5], the record is
moving at 195.5mm/s, so a full oscillation of a 25kHz wave is 8μm
long, a 4-μm pit followed by a 4-μm land; [122kHz has been
demonstrated possible][3], which makes me wonder why [16⅔-rpm
records][4] failed so totally in the marketplace; I always thought it
was a question of sound quality.  The groove is a [minimum of 25
microns wide at the top][5], with a 6-micron radius at the bottom, and
125 microns from one turn of the groove to the next on the record, but
I don’t know what the maximum modulation depth is; suppose it’s 25
microns peak to peak.  Then that 60 dB below the maximum is 25nm peak
to peak or 8nm rms.

Well, [Geoff Martin says that the depth of a groove can vary from 25μm
to 127μm deep][12], with the width varying from 50μm to 250μm; this
implies the “depth” he’s measuring is perpendicular to the surface of
the record, so the sidewalls can each move up and down by 280μm peak
to peak, so I guess my numbers above are an order of magnitude too
small: 60 dB below that 280μm maximum would be 280nm peak to peak or
99nm rms.

[2]: https://en.wikipedia.org/wiki/Comparison_of_analog_and_digital_recording#Dynamic_range
[3]: https://en.wikipedia.org/wiki/Comparison_of_analog_and_digital_recording#Frequency_response
[4]: https://bloggerhythms.blogspot.com/2011/05/slower-than-slow-16-rpm-records.html
[5]: http://pspatialaudio.com/stylus_grooves.htm
[12]: https://www.tonmeister.ca/wordpress/2020/12/13/turntables-and-vinyl-part-3/

So, suppose you have a cam-driven cutting tool similar to a miniature
[guilloché rose engine lathe][6] or straight-line engine, rastering a
1-micron-sized single-point cutting tool over a surface, with rasters
that are 1 micron apart, while moving it into and out of the surface
by up to ten microns at up to 25 kHz, thus permitting 50 kilosamples
per second of arbitrary cutting depths.  This seems eminently feasible
in light of the above.

[6]: https://en.wikipedia.org/wiki/Guilloch%C3%A9

If you move it at 50000 microns per second, you get the same
resolution along the scan line as between scan lines; cutting a square
centimeter will cost you 2000 seconds.  If we decide to credit
ourselves not just for all hundred megapixels of surface but for a
billion voxels, we’re getting half a million voxels per second.
Sounds like pretty good voxel power, but it’s probably deceptive.

If the tip needs to be able to force a square micron of steel to not
just yield but break, that might typically require [600MPa][7], 0.6mN,
the weight of 60mg.  This is more than an order of magnitude lower
than the force of a phonograph needle, so you could use phonograph
vinyl to push a stylus through steel in this way just by virtue of
making the cutting end sharper and moving the phonograph faster than
the workpiece.

[7]: https://en.wikipedia.org/wiki/Yield_(engineering)#Definition

The cutting tip needs to be harder than steel for this to work,
ideally much harder, but that hard tip can be fairly small.  A conical
tip with a tip radius of one micron (which isn’t so much cutting steel
as bulldozing it), a base area of 100 square microns, and a
phonograph-like 90° included angle, has a base radius of 5.6μm,
consequently a height of 5.6μm to its imaginary tip, and an actual
height of slightly less than this.  Whatever it’s mounted on can have
a yield stress of as little as 6MPa, because the force is spread over
an area that’s (roughly) 100× larger than the tip.  ∫π*r*²d*r* = ⅓πr³,
so this works out to 184μm³, 184 picograms per g/cc.  So with
[tungsten carbide at 15.6g/cc][8] you’d need 2.9 nanograms per cutting
tip.  One gram of tungsten carbide could produce up to 340 million
such cutting tips.

Such tips could be shaped with [ECM with a mixture of sulfuric and
nitric acid with 100-ns 9V pulses interspersed with current
reversal][9] or in [aqueous ammonia with sodium nitrate][10],
2.0–2.9mol/ℓ, conductivity 151 mS/cm, pH 12.  A [recent CC-BY review
by Asmael and Memarzadeh][11] gives an overview, but it is of
extremely poor quality, for example stating, “Factually, WC is known
as Wolfram.”  Still, it might be a good place to start.

[8]: https://en.wikipedia.org/wiki/Tungsten_carbide
[9]: https://koreascience.kr/article/JAKO200606140743645.pdf
[10]: https://www.sciencedirect.com/science/article/abs/pii/S026343681400119X
[11]: https://ojs.bonviewpress.com/index.php/AAES/article/view/915
