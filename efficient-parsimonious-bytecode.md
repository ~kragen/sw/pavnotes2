For the Zorzpad I want a compact bytecode virtual machine that permits
memory safety, lock-free transactional memory on a uniprocessor,
virtual memory, and higher-order programming.  It would be great if it
also didn't take a lot of CPU, if it wasn't too complicated, and if it
was good at immutable data structures.

I want to use a stack-machine design because I think stack bytecode
tends to be more compact than the alternatives, and it's not harder to
generate (easier, in fact).  And I'm okay with its lower execution
efficiency (because higher number of instructions).

For transactions I want to distinguish read-only access from
read-write access at fairly small granularity, and I want to privilege
immutability, because each mutable variable that gets read needs to be
logged in the transaction log and later validated.  But I would like
to not have to do that on every bytecode instruction.

Micro-efficiency sketch
-----------------------

I'd like the majority of bytecode instructions executed to require
minimal type checks and bounds checks.  Here's a couple of
instructions from a sketch of a stack bytecode interpreter for amd64,
which hasn't been tested because it definitely doesn't work; not
enough of it has been written.

* %rdx, the frame pointer, points at an activation record containing
  at least 8 local variable slots.
* %rax is a scratch register initially containing the
  bytecode being run.
* %rcx is the top of the data (operand) stack.
* %rsi points 8 bytes below the next item on the data stack.
* %rbp is the bytecode program counter.

Here's the commented disassembly:

    0000000000401089 <add_bytecode>:
      401089:   48 83 c6 08             add    $0x8,%rsi    # pop data stack
      40108d:   03 0e                   add    (%rsi),%ecx  # addition
      40108f:   31 c0                   xor    %eax,%eax   # same get-next-insn
      401091:   8a 45 00                mov    0x0(%rbp),%al
      401094:   48 ff c5                inc    %rbp
      401097:   ff 24 c5 00 20 40 00    jmpq   *0x402000(,%rax,8)

At 0x402000 there is a table of ops; it contains 8 8-byte pointers to
`localfetch_bytecode`, one pointer to `add_bytecode`, etc.  So
dispatching a bytecode requires:

- clearing a register;
- indexing memory with the program counter to fetch the bytecode;
- incrementing the bytecode program counter;
- adding the (shifted) bytecode to a constant to get an effective address;
- indexing memory again to fetch the code pointer for that bytecode;
- setting the CPU's program counter to the code pointer.

This is six operations.  For even minimal ALU operations like
addition, we add a couple more: incrementing the data stack pointer
and indexing memory with it.  So the bytecode interpreter is doing 9
basic operations for the interpreted program to do one: a 9x slowdown,
naively.  But even in well-optimized C programs it is common to need a
register shuffle or two for this kind of thing, so the slowdown might
be more like 5x.

You could hide some of the work in opcode dispatch by things like
using %rsi for the program counter so you can use `lodsb`, and that
would reduce the code size, but probably wouldn't speed it up on a
superscalar OoO CPU, and wouldn't translate to RISC.  (Maybe it would,
though.)

Adding a type-tag check on top of that doesn't really seem
overwhelmingly bad:

    0000000000401098 <dynamic_add_bytecode>:
      401098:   48 83 c6 08             add    $0x8,%rsi
      40109c:   4c 8b 06                mov    (%rsi),%r8
      40109f:   4c 89 c7                mov    %r8,%rdi
      4010a2:   48 09 cf                or     %rcx,%rdi
      4010a5:   40 f6 c7 07             test   $0x7,%dil
      4010a9:   75 3d                   jne    4010e8 <slow_arithmetic_path>
      4010ab:   4c 01 c1                add    %r8,%rcx
      4010ae:   31 c0                   xor    %eax,%eax
      4010b0:   8a 45 00                mov    0x0(%rbp),%al
      4010b3:   48 ff c5                inc    %rbp
      4010b6:   ff 24 c5 00 20 40 00    jmpq   *0x402000(,%rax,8)

That adds an extra `mov`, `or`, `test`, and `jnz`, four more
operations.

Even if that isn't overwhelming, it would be desirable to avoid that
additional check.  Intuitively it feels like the type-checking ought
to be doable less often than the arithmetic: perhaps we have a loop
that is fetching numbers from an array of binary data, so we know that
the only thing we can fetch out of it is integers, not handles.  Also,
it's costing us 3 bits in evey integer!  See below for more thoughts
about handles.

Here's a sketch of implementation of another bytecode, which stores
into the stack frame:

    000000000040106e <localfetch_bytecode>:
      40106e:   48 89 0e                mov    %rcx,(%rsi)  # push data stack
      401071:   48 83 ee 08             sub    $0x8,%rsi    # data stack ptr
      401072:   83 e0 07                and    $0x7,%eax    # local var offset
      # fetch local variable from activation record at %rdx
      401075:   48 8b 4c c2 20          mov    0x20(%rdx,%rax,8),%rcx
      40107a:   31 c0                   xor    %eax,%eax      # get next insn
      40107c:   8a 45 00                mov    0x0(%rbp),%al  # pc in %rbp
      40107f:   48 ff c5                inc    %rbp           # next insn
      401082:   ff 24 c5 00 20 40 00    jmpq   *0x402000(,%rax,8) # index ops

This doesn't have to check the bounds of the local variable offset, I
claim, because every activation record contains at least 8 local
variables.  But another approach would be to say that we bounds-check
at load time that all the local variable references are within the
activation record size declared in the subroutine header.  That would
allow us to avoid zeroing eight local variables for subroutines that
have less.

In the instruction dispatch path, a possible alternative to the second
fetch would be just multiplying the bytecode by, say, 16 bytes, and
adding a base address, to get the code address to jump to.
Implementations that didn't fit in 16 bytes would need an extra jump
to their bottom half.  But the 8 copies of `localfetch_bytecode` would
occupy 128 bytes.  A middle ground would be to index a table of 16-bit
offsets.

The subroutine-call operation is a bit heavier-weight.  It has to
construct the frame for the new subroutine, zero all its local
variables and set the return address and saved frame pointer, and
probably initialize the appropriate number of local variables from the
stack.  And if the new subroutine has constants, it's probably
desirable to initialize some register to the base address of the
constants.

Handles/keys sketch
-------------------

Above I talked about how it would be nice to not have to do type
checks all the time for things like arithmetic.

Suppose that almost all our values on the stack, in local variables in
the frame, in memory, and in arguments and return values, are binary
data rather than capabilities: integers, floats, and so on.  To access
memory we need a capability to it, with which we can access it; we
could call this capability a "handle" or "key".  It's analogous to a
Unix file descriptor, sometimes called a filehandle.

If a program contains no mutually untrusting agents, it's fine to just
put all its keys in an array, sometimes called a c-list, and index
into that c-list with small integers, exactly as Unix programs do with
file descriptors.  If a key identifies, for example, an immutable
string, it can use the key's index to invoke an operation to read part
of that string, or to find its length.  If it identifies an external
collection of keys, it can invoke an operation with its index and an
index into the collection to copy a key into its own c-list.

Let's call a piece of software with no mutually untrusting agents a
"domain".  Each domain needs its own c-list, but if the domains are
largish, then almost all operations within a domain can be done in
terms of binary data rather than keys.  Meanwhile, to invoke
operations on another domain, the domain needs the ability to pass its
keys to the other domain as part of the operation.

Especially in a bytecode interpreter, the costs we're trying to avoid
here are mostly fairly small, like the extra five instructions in the
`add` bytecode above.  It's probably okay to incur them in every
function call, just not in every instruction.  We really want to get
them out of the inner loops.

### Indexing memory ###

Indexing memory is kind of a special case.  I said above that we need
a capability to memory in order to access it; but many inner loops do
access memory.  Let's call our memory blocks "flors".  I think the
right solution is to be able to *map* a flor of memory once in order
to access it any number of times thereafter.  Each of the indexing
operations still has to be bounds-checked individually, as in this
sketch of a bytecode to index one of two currently mapped flors:

    0000000000401106 <slicefetch_bytecode>:
      # extract low bit of bytecode to distinguish which flor
      401106:	83 e0 01             	and    $0x1,%eax
      # multiply by 2 because the base+bounds is 16 bytes
      401109:	d1 e0                	shl    %eax
      # multiply by 8 more, and put the pointer to base+bounds in %rax
      40110b:	49 8d 04 c1          	lea    (%r9,%rax,8),%rax
      # to check the bound, load the bound into %r8
      40110f:	4c 8b 40 08          	mov    0x8(%rax),%r8
      # unsigned comparison of index in %rcx
      401113:	49 39 c8             	cmp    %rcx,%r8
      401116:	76 df                	jbe    4010f7 <bounds_error>
      # bounds check passed; now we can load the base pointer
      401118:	48 8b 00             	mov    (%rax),%rax
      # and index it (now assuming no tag bits!)
      40111b:	48 8b 0c c8          	mov    (%rax,%rcx,8),%rcx
      # and finally fetch the next bytecode
      40111f:	31 c0                	xor    %eax,%eax
      401121:	8a 45 00             	mov    0x0(%rbp),%al
      401124:	48 ff c5             	inc    %rbp
      401127:	ff 24 c5 00 20 40 00 	jmpq   *0x402000(,%rax,8)

The bounds check proper is really just the mov/cmp/jbe sequence: an
offset memory fetch and a conditional jump on unsigned comparison.  We
also have 5 other instructions before the next-bytecode dispatch,
though.  If we were just YOLOing it Forth-style, it would look like
this:

    000000000040112e <forthcells_bytecode>:
      40112e:   c1 e1 03                shl    $0x3,%ecx
      401131:   31 c0                   xor    %eax,%eax
      401133:   8a 45 00                mov    0x0(%rbp),%al
      401136:   48 ff c5                inc    %rbp
      401139:   ff 24 c5 00 20 40 00    jmpq   *0x402000(,%rax,8)

    0000000000401140 <forthfetch_bytecode>:
      401140:   48 8b 09                mov    (%rcx),%rcx
      401143:   31 c0                   xor    %eax,%eax
      401145:   8a 45 00                mov    0x0(%rbp),%al
      401148:   48 ff c5                inc    %rbp
      40114b:   ff 24 c5 00 20 40 00    jmpq   *0x402000(,%rax,8)

That is, instead of running 12 amd64 instructions to index an array of
8-byte cells, we'd be running 16 of them in three bytecodes: `CELLS +
@`.  Plus maybe another one because the we'd have to put the array
base pointer on the stack.  And we still wouldn't get memory safety.

The idea here is that we invoke some kind of operation on a key in
order to map a flor, and that operation can afford to do a little bit
of extra type checking, like validating that we're not trying to do a
read-write mapping of a read-only flor, or a flor accessed through a
read-only key.  That way we don't have to do that checking on every
access.  And the compiler generating the code to do this can
statically assign flors to mapping slots for particular sections of
code; maybe mapping slots can even be non-call-preserved, so you may
have to map the flor again if you call another subroutine.

Part of the appeal here is for transactional memory: if one subroutine
has a flor mapped read-only ("oler") and another one in the same
transaction maps it read-write ("besar") then it's important to ensure
that future reads from the first subroutine see changes made by the
second one.  If the whole transaction only has two or four slots for
mapping flors, the amount of work involved is limited.  And hopefully
the amount of work involved in mapping a flor a second time is small,
on the order of 16–32 hardware instructions rather than 128 or 256, so
it maybe isn't worth bending over backwards to avoid except in the
kind of inner loops that can't have function calls in them anyway.

#### I tried Ranges but I think they are a dead end ####

[Alexandrescu's "range" abstraction][0] seems like an appealing way to
handle such memory mappings.  Suppose that you always have two Ranges
open, and they are always nonempty, and represented as [begin, end)
pointer pairs instead of (as above) base and length.  Then you can
always fetch the first item of either of them:

[0]: https://www.informit.com/articles/printerfriendly/1407357 "On Iteration, 02009-11-09"

    000000000040112e <current_bytecode>:
      40112e:   83 e0 01                and    $0x1,%eax
      401131:   d1 e0                   shl    %eax
      401133:   49 8d 04 c1             lea    (%r9,%rax,8),%rax
      # fetch from begin pointer
      401137:   48 8b 08                mov    (%rax),%rcx
      40113a:   31 c0                   xor    %eax,%eax
      40113c:   8a 45 00                mov    0x0(%rbp),%al
      40113f:   48 ff c5                inc    %rbp
      401142:   ff 24 c5 00 20 40 00    jmpq   *0x402000(,%rax,8)

That makes indexing the slice comparatively cheap.  And the
possibility of slicing a range further, or adding a stride to it
(including the possibility of reversing it), is appealing in its own
way.

However, we still have to be able to advance the pointer, and I think
that's more expensive than the indexing operation.  Also, the
inability to handle empty ranges is disturbing.  So I think Ranges
don't really help us out here.
