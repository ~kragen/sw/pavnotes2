I was discussing with a friend of mine how to build a micropower
computer like the Zorzpad.  My current plan is sort of to boil the
ocean, rewriting all the software I need for a new operating system
called Hadix that can run on an MMUless ultra-low-power Ambiq
microcontroller.  And I still think that's probably a good idea, but I
might want to supplement it with a Raspberry Pi.

He reports that the Pi Zero W uses 650mW when idle and that someone
has gotten it to boot in 2 seconds (?) using something called
Buildroot, and that the Pi 3 and Pi 4 (which use more power) can not
only do that but also go into a sleep mode where they use about 50 mW,
with the sleep/wakeup cycle taking 100 ms.

50 mW is still far too much for the Zorzpad at night, and 100 ms is
far too long for a responsive user interface, but it occurred to me
that you could get a dramatic reduction in power usage with a sort of
webserver/browser system, where the server runs on a Raspberry Pi Zero
and the "browser" runs on an FPGA or a tiny low-power microcontroller.
2 seconds is not an unusably long period of time to wait for a
web-server-like transaction such as a search-engine result or a
Wikipedia page.

The FEP could thus boot up the Pi to handle a single request or a
series of requests, then turn it back off again.  The smallest thing
it could do would be to emulate a VT100 or something at a Unix shell
prompt.

A "tiny low-power microcontroller" nowadays might be something like
the Ambiq Apollo3, which is a 48 MHz ARM Cortex-M4F (IIRC) that can
burst to 96 MHz, using about 30 picojoules per cycle, with 384 KiB of
RAM.  That's almost 3 milliwatts at the 96 MHz speed; under a
milliwatt you still have about 30 million 32-bit instructions per
second.  This is about the speed of an early-90s Sun4 workstation and
30 times the speed of a Macintosh 512.  And compression algorithms
have advanced since then, so you could plausibly fit the equivalent of
a megabyte of text into its RAM.

Moreover, the FEP itself can have large amounts of storage available;
it doesn't need to boot up the Pi just to load an ebook from a MicroSD
card or something like that.  You can happily browse hypertext from
your MicroSD card all day if it's in a format the FEP is powerful
enough to interpret.  HTML5 is out of the question, but HTML3.2 is
probably fine, for example.

([Real-time MP3 decoding might test its limits][0], though, so it
might be worthwhile to fire up the big computer every 15 seconds or so
to decode some MP3 into, say, ADPCM.  32 ksps mono 4-bit ADPCM would
be 16 kilobytes per second, so RAM would hold a 24-second buffer.  Or
you could use a separate low-power DSP chip.)

[0]: https://retrocomputing.stackexchange.com/questions/2672/why-does-mp3-audio-decoding-overwhelm-retro-cpus "[an] IEEE article quotes a minimum performance of 24 MIPS, and that is based on platform optimizations of the decoder."

For computations that would either take too long on a Sun4 or need too
much memory, you can send a request to the Pi (powering it on if
necessary) and wait for the response.

You can get a pretty reasonable facsimile of a regular Unix shell
session this way if your command-line history, scrollback, environment
variables, current directory, and other similar shell state is
maintained in the FEP rather than the actual host, allowing the host
to be powered off with impunity.

But emulating a VT100 emulating an ASR-33 is setting our sights way
too low.  At 30 MIPS and 60 fps you have half a million 32-bit
instructions per screen frame.  You can recompile programs as you edit
them and run automated tests on them at that speed, including
generative property-based tests.  The Pi is 30 times faster than that
and, though it uses 600 times as much power, is worth spinning up for
many purposes.
