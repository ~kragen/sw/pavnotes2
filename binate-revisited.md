Several years back I came up with [a proposal for a
binary-relation-based database query language called Binate][0].
[This was sometime between 02006 and 02010][1].  Tonight I tried some
syntactic alternatives out on ChatGPT, asking it to guess what they
meant.

[0]: http://canonical.org/~kragen/binary-relations
[1]: http://web.archive.org/web/20100323012736/http://canonical.org/~kragen/binary-relations.html

In most of the page I used:

- `;` for the relational product;
- concatenation for relational composition;
- `~` for relational converse; and
- `,` for intersection.

Also, I used:

- `()` for grouping;
- `=` for naming new relations;
- `.` for separating such assignments;
- constants such as 53 and `"Bob"` as relations from literally
  everything to that constant.

Nikita Borisov had suggested previously that `&` would be better than
`,` for intersection, which is clearly correct.  GPT-4 had a hard time
guessing the meanings of `;` and `~` too.  I asked it questions like

> Can you please explain how the Binate database query `squarekey =
> ~(square_page; square_column; square_row). x = squarekey square_x. y
> = squarekey square_y. route = ~(visits_page; visits_column;
> visits_row) visits_route.` defines the binary relations `x`, `y`,
> and `route` in terms of the binary relations `visits_page`,
> `visits_column`, `visits_row`, `visits_route`, `square_page`,
> `square_column`,

and

> How does the Binate query `parents = father; mother. sibling =
> parents ~parents.` define the binary relation `sibling` in terms of
> the binary relations `father` and `mother`?

It confabulated things like this:

> In Binate, the tilde (~) operator is used to define a composite
> key.  The semicolon (;) operator is used to concatenate keys.
>
> (...)
>
> 1. The first part of the query, `parents = father; mother`, combines
>    the relations `father` and `mother` into a new relation called
>    `parents`. This means that for any given individual, their
>    parents are defined as both their father and mother.
>
> 2. The second part of the query, `sibling = parents ~parents`,
>    defines the relation 'sibling'. It does this by taking all pairs
>    of individuals who share at least one parent (i.e., they have at
>    least one parent in common), but then removes from this set all
>    pairs where both parents are identical (i.e., they are actually
>    the same individual).

The explanation of `parents` here is hopelessly fuzzy, but it fits set
union better than relational product.

For converse, I tried `-` instead of `~` but that was even worse:

> Then, it defines the relation `sibling`. The `-` operator in Binate
> is used to denote relational difference. So, when we say
> `parents.-parents`, it means that we are looking for pairs of tuples
> in the 'parents' relation that have the same parent (i.e., they are
> siblings), but are not identical (i.e., they are not the same
> person).

Of course the relational difference of `parents` with itself would be
the empty relation; what it’s describing here would be the relational
difference of `sibling` with the identity relation.

The best solution I could find was postfix `'`.  For relational
composition, infix `.` seemed to work well (used for functional
composition in Haskell but also properties of properties of properties
in most mainstream languages, such as C, Pascal, JS, Golang, Python,
Ruby, etc.).  This required a different statement terminator, so I
used `;`.

I tried representing relational composition as `|`, like Unix pipes,
but that was even more misleading:

> \2. The second part of the query, `sibling = parents|parents';`,
> then defines the binary relation called `sibling`. This is done by
> taking the union (represented by '|') of two sets:

Relational product was the hardest part.  I never did find a syntax
that GPT-4 didn’t consistently confuse with set union.  I tried to get
it to make one up:

> > Given the Binate queries `child = parent'; grandparent =
> > parent.parent; grandchild = grandparent'; sibling = parent.child;`
> > how would you define a `cousin` binary relation?
>
>  ​A `cousin` binary relation can be defined as follows: 
>
> `cousin = parent.sibling.child;`
>
> This means that a cousin is the child of the sibling of one’s
> parent.
>
> > Good.  Now, given `father` and `mother` binary relations, how
> > would we redefine `sibling` to exclude half-siblings using the
> > relational product operation?
>
> A `sibling` binary relation excluding half-siblings can be redefined
> as follows:
>
> `sibling = father.child ∩ mother.child;`
> 
> This means that a sibling is someone who shares both the same father
> and mother.
>
> > Yes, while that is correct (except that in Binate ∩ is written as
> > `&`) it doesn’t use the relational product operation, instead
> > using relational intersection.
>
> I apologize for misunderstanding your question earlier.  If we want
> to use the relational product operation, we can redefine `sibling`
> as follows:
>
> `sibling = (father & mother).child;`
>
> This means that a sibling is someone who shares both the same father
> and mother.
>
> > Yes, while that is correct, it doesn’t use the relational product
> > operation, instead using relational intersection.  How would you
> > write it instead using the relational product operator to create a
> > relation with father-mother pairs?

My mistake there, since in fact that was *not* correct; possibly that
cued it to make more logic errors.

> I apologize for misunderstanding your question earlier. If we want
> to use the relational product operation, we can redefine `sibling`
> as follows:
>
> `sibling = (father × mother).child;`
>
> This means that a sibling is someone who shares both the same father
> and mother.
>
> > That’s almost right, but it has two flaws: first, in Binate the
> > relational product is not written with `×` because that is not in
> > ASCII, and second, the domain of the `child` relation is
> > individual parents, not pairs of them.  Can you correct the
> > expression?  You may need to use the relational converse
> > operation.

At this point I had told it `child = parent';` at the beginning of the
conversation.

> I apologize for the confusion. In Binate, the relational product is
> written as `*`. Also, we need to use the converse operation (`'`)
> to correctly define `sibling` using father-mother pairs. Here's
> how you can do it:
>
> `sibling = (father' * mother').child;` 
>
> This means that a sibling is someone who has both the same father
> and mother.
>
> > No, that is not correct.
>
> I apologize for the confusion. The correct way to define `sibling`
> using the relational product operation and excluding half-siblings
> would be:
>
> `sibling = (father & mother).child.child;`
>
> This means that a sibling is someone who shares both the same father
> and mother, as defined by their children’s children.

That’s getting worse and worse.  I decided to feed the entropy monster
its syntactic proposal, logically corrected, in a fresh chat to see
what it guessed the meaning was.

> > Can you explain the parts of the Binate query `parents = (father *
> > mother); sibling = parents.parents';` and how it excludes
> > half-siblings from the definition of the binary relation
> > `sibling`?
> 
> The Binate query `parents = (father * mother); sibling =
> parents.parents';` is defining two binary relations: `parents` and
> `sibling`.
>
> 1. `parents = (father * mother);`: This part of the query is
> defining a binary relation called `parents`. The `(father * mother)`
> part of the equation is using the Cartesian product to create pairs
> of fathers and mothers. In other words, it’s creating a set of all
> possible combinations of fathers and mothers.
> 
> 2. `sibling = parents.parents';`: This part of the query is defining
> another binary relation called `sibling`. The `.parents'` operation
> is taking the transpose of the previously defined `parents`
> relation. In other words, it's flipping the pairs around so that
> they go from being `(father, mother)` to `(mother, father)`.
> 
> The resulting 'sibling' relation will only include those individuals
> who share both a father and a mother - i.e., full
> siblings. Half-siblings, who share only one parent, are excluded
> because they do not meet this criteria.
> 
> In summary, this Binate query defines 'siblings' as those
> individuals who share both parents in common, thereby excluding
> half-siblings from its definition.

This is not correct, but it is probably representative of the kinds of
confusion that people would have trying to learn Binate.

One difficulty with the relational-product operation is that it makes
tuples, and then how do you split them apart again?  I had built-in
relations called `first` and `second` for this.  But what is the
domain of these relations?  Like, what are the values of `53 ~first`?
Logically they should be all the pairs whose first element is 53.  But
you can always make more of those.  (Of course, `~53` has the same
problem.)  And it makes a binary infix relational product operator not
associative.

The solution I favor, which I mentioned on the original Binate page,
is *named* relational product, in which each new relational-product
expression introduces several new relations, whose domain is
(initially!) precisely the range of the relational product in
question.  So, rather than `parents = father; mother`, you would also
define new relations called, for example, `pa` and `ma`, saying
something like

    parents = {pa: father, ma: mother}
    parents = <pa: father * ma: mother>
    parents = <pa: father x ma: mother>
    parents = [pa -> father, ma -> mother]
    parents = {pa=father, ma=mother}

Then `parents.pa` is not exactly `father`; rather, it’s `father`
restricted to the domain of `mother`.  A motherless child might have a
father (be in the domain of the `father` relation) but not have
`parents` and thus not be in the domain of `parents.pa` either.

This way of defining “fields” like `pa` is confusingly similar to, but
treacherously different from, the mere-alias semantics of things like
`parents`.  The crucial difference is in the domain: the domain of
such a “field” is the set of records produced by the
relational-product operation, mating pairs in this case.  By contrast,
the domain of `dad` in

    dad = father

is the set of children who have fathers, not the set of mating pairs.

Accordingly I don’t think they should use the same operator as in my
last example.  Of the alternatives I've tried, this one seems to be
the best:

    parents = {pa: father, ma: mother};

But I think it makes sense to define fields in this way as the union
of fields from several different relational products.  This allows new
records to “implement an interface” required by some other relation.
I don’t think this poses the same kind of problem of infinities as
`first` and `second`.

Here’s GPT-4 with confusion about this syntax:

> > Can you explain the parts of the Binate query
> > `parents = {pa: father, ma: mother}; sibling = parents.parents';`
> > and how it excludes half-siblings from the definition of the
> > binary relation `sibling`?
> 
> (...)
> 
> 1. `parents = {pa: father, ma: mother};`: This part of the query is
> defining a set called ‘parents’ where each element in the set has
> two properties - ‘pa’ (representing the father) and ‘ma’
> (representing the mother).
> 
> 2. `sibling = parents.parents';`: This part of the query is defining
> a binary relation called ‘sibling’. The ‘.parents'’ notation means
> that it’s looking at the transpose of the ‘parents’ set. In other
> words, it’s flipping the rows and columns of this set.
> 
> This definition excludes half-siblings because both parents (’pa’
> and ‘ma’) must be identical for two individuals to be considered
> siblings under this definition. If only one parent was shared (as
> would be the case with half-siblings), then they wouldn’t meet these
> criteria and thus wouldn’t be considered siblings according to this
> binary relation.
> 
> In simpler terms, this query says “siblings are those who share both
> their father (‘pa’) and their mother (‘ma’)”.

I prompted it to hallucinate some syntax for aggregate operations, and
it came up with

    parents = {pa: father, ma: mother};
    numChildren = count(parents);

I think probably the function-like syntax is good, although the sense
in this case is probably backwards; I think you'd want `numChildren =
count(child);` instead.

My motivating example in the original page was something like this,
though it’s a bit unclear:

    squarekey = ~(square_page; square_column; square_row).
    x = squarekey square_x.
    y = squarekey square_y.
    route = ~(visits_page; visits_column; visits_row) visits_route.
    succ = (i; 1) sum.
    near = succ ~=<, ~succ =<.
    neighbor = x near ~x, y near ~y.
    connected_by = first route, second route.
    connected_to_neighbor_by = (first; second neighbor) connected_by.
    
    (("9", "b", "4"), ("16", "c", "4")).connected_to_neighbor_by

As best I can make out, the things in the three-tuples are values of
`square_page`, `square_column`, and `square_row`, taken from the “Guia
T” bus system guide in Buenos Aires.

With the suggested syntactic changes, I think this looks like this:

    squarekey = {pg: square_page, c: square_column, r: square_row}';
    x = squarekey.square_x;
    y = squarekey.square_y;
    route = {pg: visits_page, c: visits_column, r: visits_row}'.visits_route;
    succ = {addend1: i, addend2: 1}.sum;
    near = succ.=<' & succ'.=<;
    neighbor = x.near.x' & y.near.y';
    connected_by = from.route & to.route;
    connected_to_neighbor_by = {from: from; to: to.neighbor).connected_by;

    {from: {pg: "9",  c: "b", r: "4"},
     to:   {pg: "16", c: "c", r: "4"}}.connected_to_neighbor_by

This obviously has some risky infinities, but no more than before.
