Some [goofy YouTube video][a] showed the Nemo Shield Tanto tent/bivvy bag,
which has the interesting feature of being held up by an inflatable
"rib" done with an air pump.

[a]: https://youtu.be/vhzjO05MR0g

It occurred to me that if you put a balloon-animal balloon (for
globoflexia; *globologia* in Spanish) into a strong, non-stretchy
cloth tube, you ought to be able to inflate it to a reasonably high
pressure, and the balloons are totally ultralight, seal well enough to
remain inflated for several days, and can be easily replaced if they
break, eliminating the dread that usually accompanies inflatable
camping equipment.  A 50-pack of balloons costs about US$2 and weighs
maybe 50g.

If you're going to inflate the balloons in the traditional way, with
your breath, you probably can't exceed 0.1 atmospheres, and even 0.05
would be unusual.  Compressing them after they're inside the tube may
be a better idea, enabling 2-3 atmospheres.  Rolling up the cloth tube
seems like the easiest way to do this.  (See file
`personal-aircraft.md` for calculations on the strength of such
compressed-air structural members.)  The Shield tent mentioned above
comes with an ultralightweight hand pump consisting of a flexible
plastic bag with two check valves; you blow through one check valve to
inflate the bag, then squeeze it to inflate the tent rib.

*Friselina*, nonwoven plastic cloth, is probably the most
tear-resistant cheap lightweight material for the cloth tubes (see
file `modular-backpack.md`); ripstop nylon might have a better
strength-to-weight ratio, but I wouldn't count on it.  It would slide
more smoothly over the balloons, though.
