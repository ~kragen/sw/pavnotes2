I was thinking a bit about inotify-driven user interfaces (see file
XXX) and it occurred to me that an interesting thing to do might be to
make a filesystem directory with a document to lay out in one file,
user interface state such as a scroll position in a second file, and a
rendered pixel window in a third file.  By rerunning the layout
program when the document or UI state change, the rendered pixel
window can be updated.

This can only scale so far, of course, but it might be possible to
scale far enough to be interesting.  I wrote a [simple
pseudo-table-layout program in OCaml][0] which takes 137 instructions
per output characters, almost an order of magnitude less than printf;
a single-page layout of 5000 characters would thus take about 0.7
million instructions, and at 6000 MIPS (like a single core of a 1.5
IPC 4GHz CPU) would theoretically take about 120 microseconds.  So you
could lay out 140 pages (0.7 megabytes) in the time of a 60Hz screen
frame, or 42 megabytes a second.  [Another similar test program in
C][1], rendering text to actual pixels in a proportional font with
word wrap, gets 70 megabytes per second on one core.  While these
numbers fall far short of any fundamental limits, they are still fast
enough that you could relayout a book-sized document for every screen
frame, if your layout model is pretty simple.

[0]: http://canonical.org/~kragen/sw/dev3/alglayout.ml
[1]: http://canonical.org/~kragen/sw/dev3/propfont.c

An interesting thing about this approach is that you have your whole
model and view state in the filesystem, so the code doesn’t need to
keep any persistent state in memory, so you can upgrade the code at
will.  Also you can reboot without losing state.

If the “rendering” code runs under some kind of system call
interposition to detect which files it opens (perhaps with strace or a
custom filesystem), can be monitored noninutrusively through the
kernel (perhaps with a custom kernel, SystemTap, dtrace, or the
fanotify interface used by fatrace), or is linked with a library that
logs that information (perhaps with `LD_PRELOAD` and `dlsym(RTLD_NEXT,
...)`), it can be rerun automatically if one of its inputs chages.
erlehmann and others have used this approach for build systems.

If you want to try to apply this same approach to things like
processing an event log, you probably need to allow computational
steps that mutate internal state they also read, such as an offset in
the log.  A simple approach is to run such computational steps to a
fixed point, when they no longer update the state variables they read,
so they will no longer give rise to a new iteration.  In the
log-offset case, the log reader can seek to the given offset in the
log, attempt to read, and hit end of file immediately (or at any rate
before reading a full event), and therefore not update the offset.

I feel like this kind of approach can work in concert with software
transactional memory, which needs to track reads and writes in the
same way; it also needs to undo conflicting writes and retry the
offending computation.
