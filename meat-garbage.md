Wednesday I bought 3kg of chicken wings (US$1.50), but by Friday night
Mina reported they smelled suspicious.  Last night (Saturday) I took
them out to the garbage basket a meter above the sidewalk, but it
turns out we don’t have garbage pickup service on Saturdays; a
neighborhood dog found the bag, pulled it out of the basket, ripped it
open a bit, and ate some of the wings, leaving the others to rot.
This is suboptimal; it was 25° when I found the bag, full of flies,
and it will be 32° for several hours this afternoon before the next
garbage pickup.  I’ve rebagged the rotting dinosaur corpses and put
them back in the basket, but I’ll have to check at night to see if the
dog has returned for a mischievous carrion refill.

How could this be improved?

The traditional approach, of course, is either to leave the meat as a
sacrifice in the temple of Ba‘al-Zevuv, or, more commonly, to burn or
bury waste meat under two meters of dirt to keep wild animals from
digging it up to eat.  But it occurs to me that much milder conditions
should be adequate for dehydration without putrefaction; all that is
needed is control of temperature and humidity, plus some kind of
treatment of the waste air.

Consider a dehydration receptacle in which the waste meat is
dehydrated, rendering it inhospitable to the unwanted bacteria once
the dehydration is complete.  Air can be recirculated around it and
around heating elements to bring it up to the desired temperature,
while keeping the temperature as low as possible to reduce the
thermolysis of the proteins.  Temperatures above 60° are out of the
“temperature danger zone”, and in restaurants, foods kept above 74°
are considered safe indefinitely, though their quality may degrade.

Once the meat has been heated to this temperature, dehydration can
proceed, in which water will gradually diffuse out of the depth of the
meat until it is dried.  It is probably necessary to include some
mechanical disturbance as well to prevent fats in the meat from oozing
into a solid mass that impedes the escape of water vapor.  The kind of
tumble dryer used for clothes may not be the best sort of motion to
use.

Each kilogram of meat contains about 700 grams of water, the
vaporization of which will require about 1.6 megajoules.  As with
recondensing clothes dryers, most of this energy can plausibly be
recovered with a condensing heat exchanger which transfers most of the
heat to new incoming air.  Water’s vapor pressure roughly doubles
every 10°: 4 g/kg of dry air at 0°, 8 g/kg at 10°, 15 g/kg at 20°, and
27 g/kg at 30°, so at 80° it should amount to roughly half the air
volume, so you only need a couple kilograms of dry air to dry out the
kilogram of meat.

Probably you want to pass that dry air through something very hot
chamber to burn up the meat smells, though.  The [lower explosive
limit of propane][0] is 2.37%, or I guess about 3% by weight, so it
would probably be adequate to use your couple of kilograms of dry air
as the air supply to burn 80 grams or so of propane.  Alternatively,
you could pass the air through a packed bed of heated ceramic beads.

[0]: https://en.wikipedia.org/wiki/Propane

Though millimeter-thick laundry usually dries in an hour or two, we
might be talking about a process that takes days or weaks for meat,
since it might be 20 mm thick or more, and, as I understand it, such
diffusion processes take time roughly proportional to the square of
the material thickness.  So a front-end shredding or grinding stage,
like a sausage grinder, might be worthwhile to ensure that the meat is
in small enough pieces to dehydrate in a reasonable period of time.
But the grinding stage probably needs to be immediately washed; meat
drying onto it will be hard to remove later.

Even if you could do it in six hours, the airflow that must be
deodorized is fairly small: 2 kg of air is about 1.7m², and 1.7m² in
six hours is 80 milliliters per second or 0.2 “cfm”.

Here on the surface of the Earth, cremating the chicken or whatever
may be a better idea.  Would it require more air?  If we approximate
the chicken as one third long-chain hydrocarbons and two thirds
carbohydrates, well, 2CH₂ + 3O₂ → 2CO₂ + 2H₂O burns 28 grams of
hydrocarbons with 96 grams of oxygen, and CH₂O + O₂ → CO₂ + H₂O burns
30 grams of carbohydrates with 32 grams of oxygen, so roughly 100
grams of chicken requires 96 + 32 + 32 ≈ 130 grams of oxygen and 650
grams of air, and burning 300 dry grams of chicken would require about
the same 2 kg of air as merely removing its water at 80°.  And, once
you get it started, it provides its own fuel.

Treating the smoke may be more difficult.
