Thinking tonight about my previous frustrating experiences with
powder-bed printing of ceramic with powdered flux, some ideas occurred
to me:

- Dried sodium silicate is pretty solid, but still has a lot of water
  in it, and it melts at a very low temperature, like, below the
  so-called [Draper point][0] of 525°, because it’s being heated by an iron
  plate that isn’t glowing red.  This causes it to foam up, allowing
  the water vapor to escape.  These properties would make it a much
  better choice for a powdered flux than the things I was attempting
  to use; the foam expanding between the grains of sand (or other
  siliceous compound, such as quartz flour) would tend to increase the
  connectivity of the cemented quartz network, giving a better chance
  of forming a solid piece.
- Other sources of sodium, potassium, or borate might work, though I’m
  less sure about this; borax is especially interesting as a source of
  sodium and water as well.  And some source of calcium or magnesium
  might help stabilize the resulting glass.  Borosilicate glass prints
  would be much less likely to break as they cooled than soda-lime
  glass.
- Another interesting binder that could be reliably activated with
  relatively low heat might be diammonium phosphate, the fertilizer
  (or a purified version of the fertilizer), which is fairly
  hygroscopic.  Roughly speaking, at 155°, [this decomposes to
  monoammonium phosphate][1], which [decomposes to liquid phosphoric
  acid at 200°][2], which will avidly combine with any metal oxides in
  the environment that are even slightly alkaline; iron and aluminum
  seem like the most promising metals.
  
[0]: https://en.wikipedia.org/wiki/Draper_point
[2]: https://en.wikipedia.org/wiki/Ammonium_dihydrogen_phosphate
[1]: https://en.wikipedia.org/wiki/Diammonium_phosphate
