I was thinking about how [SDS 930 QED][a] completed each command
character to the full word as soon as it was typed, then executed it
if a "." was typed; how the Symbolics Genera command prompt prompted
for arguments and expanded out command names; and about immediate-mode
GUIs.

[a]: https://dl.acm.org/doi/10.1145/363848.363863

It occurred to me that it would be interesting to write out a grammar
of commands in a form similar to an immediate-mode GUI menu tree:

    if (submenu("&Edit")) {
        if (!get_selection()) disable_item();
        if (readonly()) disable_item();
        if (command("Cut", ctrl('c'))) do_copy();

        if (!get_selection()) disable_item();
        if (command("Copy", ctrl('c'))) do_copy();

        if (readonly()) disable_item();
        if (command("Paste", ctrl('v'))) do_paste();

        separator();
        if (submenu("Foobar")) { ... }
    }
    if (submenu("&Buffers")) {
        int n = n_buffers();
        for (int i = 0; i < n; i++) {
            if (command(get_buffer(i).name)) activate_buffer(i);
        }
    }

The idea here is that every time we want to paint the screen or handle
a UI event, we run through all this code, and don't have to retain in
RAM a possibly-out-of-sync tree of in-memory objects representing UI
widgets; the code's execution flow materializes UI things on demand,
while some framework is keeping track of the current navigational
state, what widget is selected, etc.

But in the case of the command language grammar, you could have things
like filename completion or numerical parameters in the flow of the
parse, along with help messages and a list of candidate next words.
And in cases where a command or parameter name might become ambiguous
later, you could type as much or as little of a parameter name as you
think is necessary before hitting space to accept a completion, and
moreover an incomplete command can still provide you with a live GUI
preview of what the command is going to do if you do invoke it.

You could imagine, for example, typing `o ` to invoke the `open`
command, then tab-completing a filename, and then seeing which program
will be invoked if you hit Enter, as well as the alternative options
you can override it with.

I don't think this is necessarily a good way to structure a
programming language or a programming-language editor, but it seems
like it might help significantly with easing the tradeoffs between
readability and writability for things like debugger command
languages.  You really do want to be able to type `ls -latr` and `cp
-a` but maybe it would be helpful to expand them out to phrases for
the benefit of an audience or later readers.
