In file `algebraic-comfy-manipulation.md`, I described a simple
formalism as follows:

> Let’s consider our program as being made out of a graph which is
> labeled not on nodes or on edges but on “ports”: node-edge
> pairs. This seems like a natural formalism for things like logic
> circuits and program control flow and dataflow graphs, but I don’t
> think I’ve seen it described before. (...) In this case, each of our
> nodes has three edges: “go”, where control flow enters; “yes”, where
> control flow goes if the node succeeds; and “no”, where control flow
> goes if the node fails.  We can define each of our combining forms
> as a ported-graph rewrite rule as follows:
>
>     Alt(e f) = {e[no=:x]; f[go=:x]}
>
> This defines a local net `x` and says that we instantiate `e` with
> its `no` port connected to `x` and `f` with its `go` port connected
> to `x`.  All other ports are “inherited” from the surrounding
> context; so, if the ports are `go`, `yes`, and `no`, this is
> equivalent to the following:
>
>     Alt(e f) = {e[go=go, yes=yes, no=:x]; f[go=:x, yes=yes, no=no]}

I put “inherit” in quotes because usually that implies object-oriented
derivation from a prototype object or base class, and that’s not what
we’re talking about here.  To the extent that an object-derivation
thing is happening here, it’s sort of in a dimension different from
the one I’m talking about here; you could say that `e[no=:x]` is
“inherited from” `e` while “overriding” `no`.

Aside from the notation for the graph rewrite rules, this seems pretty
similar to Prolog in some ways.  The non-directionality and the
dynamic-scoping thing seems well suited to things like electric
circuits.

Circuits with ported-graph rewrite rules
----------------------------------------

Let’s say that two-terminal components have an anode `a` and a cathode
`k`.  Then we can define a series combination and reversal trivially:

    ser(c d) = {c[k=:x]; d[a=:x]}
    rev(c) = {c[k=a, a=k]}

Parallel combination is too trivial to be worth writing:

    par(c d) = {c; d}

We can define the [Darlington pair][0] of a bipolar transistor type
just as trivially, using `b`, `e`, and `c` for base, emitter, and
collector; this definition works just as well for npn and pnp, which
would not be the case if we assigned emitter and collector to `a` and
`k`:

    darlington(q) = {q[e=:x]; q[b=:x]}

This looks an awful lot like `ser` above, though.  If we regard a
transistor as a diode with an extra collector terminal, thus giving us
`a`, `k`, and `c`, we can actually use `ser` for `darlington`, for
both npn and pnp.  Or we can define an adaptor:

    npnd(q) = {q[b=a, e=k]}
    pnpd(q) = {q[b=k, e=a]}
    npndarlington(q) = ser(npnd(q) npnd(q))
    pnpdarlington(q) = ser(pnpd(q) pnpd(q))

Though usually you’d want a resistor across the power transistor’s b-e
junction, and I think that works better with `b`, `e`, and `c`:

    darlington(q r) = {q[e=:x]; q[b=:x]; r[k=:x, a=e]}

If we are treating the transistor as a diode with an extra terminal,
the resistor is “in parallel with” the power transistor:

    npndarlington(q r) = {ser(npnd(q) {r; npnd(q)})}

This is starting to be a little confusing, though!

[0]: https://circuitcellar.com/resources/darlington-vs-sziklai-pair/

It's also easy to write Sziklai, though we have to choose which
transistor to lie about:

    sziklai(q1 q2 r) = {q1[c=:x]; q2[b=:x, e=c, c=e]; r[k=:x, a=e]}

“Inheriting” terminals like `vcc` and `gnd` would allow you to write
things like this:

    clamp(d) = {d[a=gnd, k=pin]; d[a=pin, k=vcc]}

For things like logic gates and op-amps, this kind of “inheritance” of
power-supply rails seems like it would be pretty useful.  Clocks and
buses seem like other nets you’d often, but not always, want to
“inherit”.

We could imagine “inheriting” transistors and resistor values and
whatnot in the same dynamically-scoped way we inherit terminals,
largely eliminating the distinction between `()` and `[]` which
complicates the above, though perhaps impairing readability by
confusing the reader between which arguments are component types and
which are nets.  Then we might write something like this:

    ser = {x1(k=:t); x2(a=:t)}
    rev = {x(k=a, a=k)}
    par = {x1; x2}
    npnd = {q(b=a, e=k)}
    pnpd = {q(b=k, e=a)}
    npndarlington = {ser(x1=npnd, x2=npnd)}
    pnpdarlington = {ser(x1=pnpd, x2=pnpd)}
    npndarlington = {ser(x1=npnd, x2={r; npnd})}
    darlington = {q(e=:t); q(b=:t); r(k=:t, a=e)}
    sziklai = {q1(c=:t); q(b=:t, e=c, c=e); r(k=:t, a=e)}
    clamp = {d(a=gnd, k=pin); d(a=pin, k=vcc)}

I don’t know, this seems like it might be a promising approach,
especially coupled with visualization.  The implicit parameterization
of things like this for the transistor type and resistor values seems
like a plus:

    diffampnpn = {npn(e=:t, b=in1, c=:u); npn(e=:t, b=in2, c=out);
        sink(a=:t, k=gnd); r(a=vcc, k=:u); r(a=vcc, k=out)}
    sink = {r}   # could use a constant-current sink for higher gain

How do you know what the arguments of something like `ser = {x1(k=:t);
x2(a=:t)}` are?  Maybe it isn’t actually static!  We know `x1` and
`x2` are arguments, because they occur free, but instantiating them
there might create other terminals — in the `npndarlington =
{ser(x1=npnd, x2=npnd)}` case, for example, we want a `c` terminal.
But I think it might be reasonable to say that it's the occurrences of
`npnd` in `npndarlington` that give `npndarlington` its `c` terminal,
not `ser` itself; then `ser` can be known to statically depend only on
`x1` and `x2`.

I’m dubious about this, though, and I think I might be getting
confused.

Relational programming
----------------------

Suppose that, instead of electrical components, we consider our nodes
to represent N-ary relations, such as “x + y = z”, "x > y", “x’s first
name is y and last name is z”, or “z = ∫x dt”.  Then we can consider
the graph thus built up to represent a set of constraints, where each
net is a value (though, in my last example, the values are arbitrary
functions of or distributions over time, rather than anything
finite-dimensional).  One interesting thing you can do with this graph
is to try to compute the values on some or all of its nets.

This embraces higher-order programming somewhat more comfortably than
the electric-circuit-modeling approach above.
