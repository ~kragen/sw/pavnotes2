A discussion on HN today focused on Tcl; [I tackled the question of
whether it was small or not][0], arguing that at 8½ times the size of
Lua 5.2 and 107 times the size of the 7th Edition Bourne shell (17
kilobytes), Tcl could not be considered a small embeddable language.
Even if CPython *is* four times as big.

[0]: https://news.ycombinator.com/item?id=41020823

However, that led me to investigating [CP/M XLISP][1] and [8kLisp][2],
which are relatively powerful interpreted languages that ran, I
believe, in a 64K memory space.  8kLisp is an 8192-byte interpreter
for the Z80, which only *had* a 64K memory space.

Lisps have some big disadvantages, though.  Pointers take up a lot of
memory space compared to data (even in a 16-bit address space), and
garbage collection both takes up more memory space and incurs a
runtime cost for write barriers.

Lua 5.2 shares these problems, and even though it’s small compared to
Tcl, still cannot really be run on a 16-bit machine.  Just the
stripped Lua runtime shared library is 217kB on amd64.

Syntax optimized for interaction rather than programming
--------------------------------------------------------

But the bigger issue for using Lisp or Lua *as a scripting language*
is [the merely syntactic issue identified by Yossi Kreinin][3]:

> * **Tcl likes literals, not variables.** Tcl: `string, $var`.  Pop
>   infix: `"string", var`.
>
> * **Tcl likes commands, not expressions.** Tcl: `doit this that`,
>   `[expr $this+$that]`.  Pop infix: `doit("this","that")`,
>   `this+that`.
>
> (...)
>
> So basically, pop infix languages (and I use the term in the most
> non-judgmental, factual way), pop infix languages are optimized for
> programming (duh, they are programming languages).  Programming is
> definitions.  Define a variable and it will be easy to use it, and
> computing hairy expressions from variables is also easy.  Tcl is
> optimized for usage.  Most of the time, users give simple commands.
> Command names and literal parameters are easy.  If you are a
> sophisticated user, and you want to do `pmem 0 bkpt [expr [pmem 0
> pc] + 1]`, go ahead and do it.  A bit ugly, but on the other hand,
> simple commands are really, really simple.
>
> And eventually, simple commands become all that matters for the
> user, because the sophisticated user grows personal shortcuts, which
> abstract away variables and expressions, so you end up with `pmem 0
> bkpt nextpc` or something.  Apparently, flat function calls with
> literal arguments is what interactive program usage is all about.
>
> (...)
>
> I have a bug, I’m frigging anxious, I gotta GO GO GO as fast as I
> can to find out what it is already, and you think now is the time to
> type parens, commas and quotation marks?! Fuck you!

[1]: http://www.cpm.z80.de/download/xlisp.zip
[2]: https://www.softwarepreservation.org/projects/LISP/picolisp/8kLisp.tgz/view
[3]: https://yosefk.com/blog/i-cant-believe-im-praising-tcl.html

He has a point.  (See further discussion of syntaxes addressing this point in
file `tclish-basic.md`.)  I don’t really like Tcl’s particular punctuation
choices, much less its semantic model, but they’re certainly optimized
for that kind of thing.  I think `()` is preferable to `[]` or `{}`
for expression nesting, even though common keyboard layouts have these
in inconvenient places, just because they’re much more common in
normal prose.  If I were to make a vapid tier list of punctuation, I’d
rank it as follows:

- **S**: `()',./;` reachable on my keyboard without moving hands off
  the home row or using shift
- **A**: `?-=:"` reachable with just shift or moving hands, and not
  too uncommon
- **B**: ``!{}`\<>*&`` uncommon in prose, and either require shift or
  moving hands, or common in prose and require both shift and moving
  hands
- **C**: ``~@#$%^[]_+|``: uncommon in prose and require both shift
  and moving hands
- **D**: `¡¿÷×≠≈†‡§∴∵⁂√→←↑↓↕↔⇒⇐⇔⇑⇓⇕≤≥•–—¢£€¥«»¬°±¶·`: not in ASCII and
  therefore missing from many keyboards.‡
  
The punctuation in common shell commands like `ffmpeg -i input.wav
-c:a libmp3lame -b:a 128k -V 5 output.mp3` consists entirely of S-tier
and A-tier punctuation with the occasional exceptions of `<`, `>`,
`&`, and `|`, which I think could probably be productively replaced
with A-tier digraphs if you’re aiming at typability.  (The original
designers didn’t know how to touch-type, so their criteria for
typability were different.)

That `ffmpeg` command calls out another obvious feature of shell and
Tk commands which Yosef didn’t mention: name-value pairs are king, and
you want to abbreviate them.  Conventional programming languages use
named arguments sparingly, perhaps because passing many parameters
takes extra run time, but both WIMP GUIs and Unix command lines have
converged on having a large number of parameters for each operation,
only a few of which have non-default values on any particular
invocation.  Tcl doesn’t directly support named parameters.  Lua, on
the other hand, is deeply designed for them: like JSON and YAML, had
named parameters before it even had executable code.

The obvious sigils for indicating variable interpolation would seem to
be `,`, `.`, `/`, `?`, or `:`, rather than `$`, and `()` seem
preferable for nested commands.  Or possibly `:()`or `,()` or
something.  So maybe you’d write:

    doit this that
    expr ,this + ,that
    pmem 0 bkpt ,(expr ,(pmem 0 pc) + 1)   # pmem(0, 'bkpt', pmem(0, 'pc') + 1)
    pmem 0 bkpt nextpc

The conventional use of `.` in programming languages for separating
components of a traversal of a tree or graph of associative arrays or
records suggests using `.` rather than `,` to identify variables, as
in `.this` or `.points(3).x`; Tk uses them to great effect to identify
widgets in the widget hierarchy.

Tcl chose the nesting delimiters `{}` for quoting literal strings,
which makes it practical to have literal strings including entire
blocks of Tcl code with their own embedded literal strings, without
backslashes growing exponentially the way they do when you try to do
this in the Bourne shell.  However, the particular semantics chosen
for this quoting have their own problems; the backslashes are not
actually removed on input, but on indexing the string as a list:

    % set x {a \{b { c} d}
    a \{b { c} d
    % puts $x
    a \{b { c} d
    % lindex $x 1
    {b
    % lindex $x 2
     c
    % set x "{a {b { c} d}"
    {a {b { c} d}
    % lindex $x 2
    unmatched open brace in list
    % set x {a {b {e \{ f} c} d}
    a {b {e \{ f} c} d
    % lindex $x 1
    b {e \{ f} c
    % lindex [lindex $x 1] 1
    e \{ f
    % lindex [lindex [lindex $x 1] 1] 1
    {

PostScript also uses nesting delimiters for strings (parentheses in
this case), but strips the backslashes on input:

    GS>(a\(b) length == (()) length ==
    3
    2

I need to think more about quoting.

----

‡ The Latin American keyboard layout uses AltGr for a second shift
state, so you can type `¡¿ªº·¬` and accented letters as well as all of
ASCII.  Historically it didn’t have `@` until it was added to AltGr-2,
and from their position on AltGr-number keys I’m guessing `|#¬` are
also added at that time.

Memory frugality
----------------

An interesting question to me is how small and easily embeddable you
could make an interactive scripting language like Tcl.  Smalltalk,
Forth, and tokenized BASICs like BASIC-80 demonstrate that there are
more compact representations for program code that can be deparsed
into readable code and also interpreted efficiently; indirect-threaded
code (as in SPITBOL and Forth) requires about 2 bytes of memory per
source code token and can be executed with an interpretive overhead of
about 5x.  If designed with deparsing in mind, it can avoid the need
to keep textual source code in memory, and the deparsing code itself
can be encoded as threaded code.  Smalltalk and Emacs Lisp instead use
stack bytecode with typically about one byte per input token, which is
slower to interpret than indirect-threaded code but more compact.

The next question is how to represent data in memory.  Ideally, a list
of 10 16-bit numbers should take 20 bytes.  In Tcl, `{8 2020 -3806 102
7 3 11 5 36 86}` is 33 bytes.  In Lisp, you need 10 cons cells,
typically 4 bytes each on a 16-bit machine, so 40 bytes, assuming the
integers can be unboxed.  On a 32-bit machine, 80 bytes.  As a UTF-8
string, it would be 12 bytes; to represent negative numbes in UTF-8
you’d need to do some kind of transformation.  For example, you could
use sign-magnitude, shifting in the sign bit as the least significant
bit.

Of course, it’s tricky to change or even index an element in the
middle of a UTF-8 string, but appending to the string and iterating
over it are easy and fast.  And it’s certainly a lot faster than
indexing into a Tcl list.

Being able to switch between representations on the fly without
altering the calling code, as Smalltalk dynamic method dispatch does,
is a pretty useful way to save memory.  Although 16-bit Smalltalk
didn’t attempt this kind of UTF-8 trick, it could represent 15-bit
integers in unboxed tagged pointers, transparently overflowing to
boxed bignums, and thanks to Smalltalk’s dynamic dispatch, you can
supply TypedArray instances to (almost all) code that expects normal
arrays.

One of the things that came out of the STL work is that algorithms
that require random or even bidirectional access to containers are
actually relatively uncommon.  So it’s unfortunate that the sequence
protocol of Smalltalk (and old versions of Python) requires
implementing #at:, with an integer index.  A D-like range protocol
would be a better approach.

Often, the best way to do a computation in a small amount of memory is
to avoid storing large intermediate results, consuming them
incrementally instead, and recomputing them if necessary.  This is
possible when the algorithms involved don’t need to jump backward in
the data sequence and producing the sequence is a pure function.  But,
while tuning such time/memory tradeoffs is often necessary, it would
be useful to reserve the tuning for cases where it *is* necessary, by
default using a policy designed to minimize the chances of
pathological results by opportunistically caching small and expensive
intermediate results.

When you do want to store intermediate results that are produced
incrementally, you need to allocate storage for them, and you may not
know at the outset how big they are.  These days, the standard way to
build up an array of unknown size in Python or Golang or Common Lisp
or the C++ STL is to allocate it on the heap and reallocate it
periodically with exponentially growing amounts of slack space.  This
takes amortized linear time and is a very general approach that
supports interleaving the growth of various arrays.  However, it
generally wastes memory on a sort of internal fragmentation: when the
array is fully built, it still has some slack space at the end that is
never reclaimed; under certain reasonable assumptions†, this waste is
about 36–44.2% of the size of the actual useful data.

PostScript has a more efficient way to do it.  Consider this line of
PostScript code, generated by the Acrobat 4.0 Import Plug-in:

    <</PageSize [611.52 792] /Policies << /PageSize 1 >> >> setpagedevice

To clarify the syntax, in Python this might be written as follows:

    setpagedevice({'PageSize': [611.52, 792], 'Policies': {'PageSize': 1}})

`PageSize` and `Policies` are “name” objects, which is what PostScript
calls Lisp symbols, but here I’m representing them as Python strings.
So this constructs a dictionary containing an array of two numbers and
a one-item dictionary.  PostScript works by executing the operators on
a stack in the order in which they are written, which is why
`setpagedevice` is last.  The PostScript operator `]` constructs an
array from the items on the stack down to the top stack mark, which
was placed there by the operator `[`.  At this point it knows the
number of items that will be in the array, so it can heap-allocate an
array of the right size.  Similarly, `>>` constructs a dictionary of
precisely the right size from the items on the stack down to the stack
mark placed by `<<`.  Thus we don’t have to allocate the heap memory
until we know how much we need.  So we can avoid this 44% slack waste.
And, in theory, if we have multiple candidate memory representations
for the same data, we can choose the best one at that point.

(PostScript went all-in on this mechanism; there’s no way to grow the
size of an array once you’ve created it — no Common-Lisp-like fill
pointer — and in early versions you couldn’t even dynamically grow a
dictionary, though you could at least create it with some slack
space.)

The PostScript mechanism still involves copying all the items to
someplace else at the end of the process, though, and that means we
need memory space for two copies of the set of items, and we need to
waste a block of CPU time doing the copy.  This can be avoided with
[obstacks][4], which also permit some limited interleaving of the
growth of different objects by putting them on different obstacks, and
which also prevent allocator fragmentation between objects.  However,
compiling code for the above nested dictionary for a single obstack
would require completely building all the child objects, and
presumably storing pointers to them in temporary registers, before
beginning to build the parent.  This could be pretty difficult if
you’re calling subroutines to compute the individual items in the
dictionary.

If you’re compiling code for what Python calls an “array display”,
like `[x, 4]`, you might know the size of the array at compile time,
but the PostScript stack approach generalizes to list/dict/set
comprehensions, where the number of items is not known until runtime.

You can implement PostScript stack marks by linear search through the
stack (since you’re about to do a linear amount of work to copy all
those things off the stack), but you don’t need to; you can store the
position of the topmost mark in a variable and save its old value in
the mark you push onto the stack.

With respect to time/memory tradeoffs, if you’re building a large
“array” to be consumed sequentially by some other algorithm, it makes
some sense to initially start building it on a stack in this way,
doing a context switch if the data on the stack gets too big.  This
suggests using something more like Python’s generator mechanism, in
which a generator explicitly `yield`s each new value produced, so the
system can tell which stack values are destined to belong to the
output sequence and which are just temporaries.

[4]: https://www.gnu.org/software/libc/manual/html_node/Obstacks.html

----

† I just did some simulations of doubling on each allocation with
exponentially-distributed useful-data sizes, with the exponential
parameter varying between 8 and 1045876.

### Compressed OOPs ###

Consider the possibility of representing object references (ordinary
object pointers, OOPs) in 16 bits each.  The Java Virtual Machine does
something similar, representing its OOPs in 32 bits even on 64-bit
platforms.  [Java’s approach][7], enabled by not needing pointer
tagging, is to shift the pointer left by 3 bits and add it to a base
address, thus permitting up to 32 gibibytes of oop-addressed objects.
This imposes a surprisingly small amount of overhead; on amd64:

        ## Index compressed oop in r8 using heap base pointer in r12
	## to load a compressed oop from memory into r10
   	## 47 8b 54 c4 10  mov 0x10(%r12,%r8,8),%r10d
	mov r10d, dword ptr [r12 + r8*8 + 16]

        ## Contrast with uncompressed oops
	## 4d 8b 50 20     mov 0x20(%r8),%r10
        movq r10, [r8 + 32]

There’s an additional fixed bit shift and add, but those are likely to
run concurrently on superscalar hardware and so cost no extra latency,
just a little more power usage.  Even on simple in-order processors
it’s probably not much more.  16-bit compressed OOPs are similar:

        ## 16-bit compressed OOPs on 32-bit-aligned objects
        ## 66 47 8b 54 84 08   mov 0x8(%r12,%r8,4),%r10w
        mov r10w, word ptr [r12 + r8*4 + 8]

These 16-bit compressed OOPs can usefully access up to 256KiB of RAM.

However, if we use pointer tagging to represent small integers without
changing the alignment, we sacrifice half of our address space; OOPs
can only address 128KiB of RAM and 32768 small integers.  If we have
more than 128KiB of RAM, we could use some of the rest of it for
strings or other large arrays that are “owned” by objects inside the
128KiB space.  Also, threaded code and interpreter machine code don’t
have to be within the OOP space.

On ARM (Thumb-2 or non-Thumb, but not Thumb-1) you can load the first
16-bit or 32-bit field in a single instruction.  Suppose r4 has the
heap base and the compressed OOP, low bit 0, is in r0:

        @@ f834 0010  ldrh.w r0, [r4, r0, lsl #1]
        ldrh r0, [r4, r0, lsl #1]

        @@ f854 0010  ldr.w r0, [r4, r0, lsl #1]
        ldr r0, [r4, r0, lsl #1]

But loading other object fields requires a temporary register and an
additional instruction:

        @@ eb04 0140  add.w r1, r4, r0, lsl #1
        @@ 8888       ldrh  r0, [r1, #4]
	add r1, r4, r0, lsl #1
        ldrh r0, [r1, #4]

In some cases you can reasonably load multiple fields from the same
object in a single `ldm` instruction once you use such an `add` to
compute the object’s absolute base pointer.  Here we load 6 fields in
nominally 5 clock cycles:

        add r0, r4, r0, lsl #1
        ldmia r0!, {r1-r3}

16-bit register halves are reasonably accessible through the ARM’s
barrel shifter, though unfortunately not for memory addressing
operations:

        add r0, r2, r1, ror #16

[7]: https://wiki.openjdk.org/display/HotSpot/CompressedOops

Some UX reflections
-------------------

The transformation of the list of integers is actually kind of a
paradigmatic case of one kind of thing I’d actually like to be able to
script.  Right now I do it with Python and command-line editing:

    >>> [((n < 0) + 0, abs(n) << 1)
         for n in [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]]
    [(0, 16), (0, 4040), (1, 7612), (0, 204), (0, 14),
     (0, 6), (0, 22), (0, 10), (0, 72), (0, 172)]
    >>> [((n < 0) | (abs(n) << 1))
         for n in [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]]
    [16, 4040, 7613, 204, 14, 6, 22, 10, 72, 172]
    >>> [chr((n < 0) | (abs(n) << 1))
         for n in [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]]
    ['\x10', '࿈', 'ᶽ', 'Ì', '\x0e', '\x06', '\x16', '\n', 'H', '¬']
    >>> [chr((n < 0) | (abs(n) << 1)).encode('utf-8')
         for n in [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]]
    [b'\x10', b'\xe0\xbf\x88', b'\xe1\xb6\xbd', b'\xc3\x8c',
     b'\x0e', b'\x06', b'\x16', b'\n', b'H', b'\xc2\xac']
    >>> b''.join([chr((n < 0) | (abs(n) << 1)).encode('utf-8')
                  for n in [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]])
    b'\x10\xe0\xbf\x88\xe1\xb6\xbd\xc3\x8c\x0e\x06\x16\nH\xc2\xac'
    >>> len(b''.join([chr((n < 0) | (abs(n) << 1)).encode('utf-8')
                      for n in [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]]))
    16
    >>> [-(n >> 1) if n & 1 else n >> 1
         for c in (b''.join([chr((n < 0) | (abs(n) << 1)).encode('utf-8') 
                             for n in [8, 2020, -3806, 102, 7,
                                       3, 11, 5, 36, 86]])
                  ).decode('utf-8') for n in [ord(c)]]
    [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]

So that particular list of 10 numbers takes 16 bytes as a string,
because two of them hit the worst-case three-bytes-per-number case.
But this is still 20% smaller than the raw binary 16-bit case, 52%
smaller than Tcl’s textual representation, 60% smaller than the 16-bit
Lisp representation (or an array of 32-bit integers), and 80% smaller
than the 32-bit Lisp representation.

Here’s another example.  Earlier I said you could productively replace
B-tier and C-tier punctuation in shell syntax with digraphs of S-tier
and A-tier punctuation.  What are all the S/A digraphs?

    >>> a = '?-=:'
    >>> s = "()',./;"
    >>> [c + d for c in a + s for d in a + s]
    ['??', '?-', '?=', '?:', '?(', '?)', "?'", '?,', '?.', '?/', '?;',
     '-?', '--', '-=', '-:', '-(', '-)', "-'", '-,', '-.', '-/', '-;',
     '=?', '=-', '==', '=:', '=(', '=)', "='", '=,', '=.', '=/', '=;',
     ':?', ':-', ':=', '::', ':(', ':)', ":'", ':,', ':.', ':/', ':;',
     '(?', '(-', '(=', '(:', '((', '()', "('", '(,', '(.', '(/', '(;',
     ')?', ')-', ')=', '):', ')(', '))', ")'", '),', ').', ')/', ');',
     "'?", "'-", "'=", "':", "'(", "')", "''", "',", "'.", "'/", "';",
     ',?', ',-', ',=', ',:', ',(', ',)', ",'", ',,', ',.', ',/', ',;',
     '.?', '.-', '.=', '.:', '.(', '.)', ".'", '.,', '..', './', '.;',
     '/?', '/-', '/=', '/:', '/(', '/)', "/'", '/,', '/.', '//', '/;',
     ';?', ';-', ';=', ';:', ';(', ';)', ";'", ';,', ';.', ';/', ';;']
    >>> len(_)
    121

Here are some deficiencies in the Python experience:

1. I had to do all the pretty-printing by hand.  The standard CPython
   REPL just runs everything together on one line and punishes you for
   trying to split your code across multiple lines; for normal
   interactive use, the Interlisp approach of dynamically doing a
   reasonable layout from an internal representation seems better than
   requiring the user to edit in newlines.  I’d like to be writing
   different pipeline stages on successive lines, as with Haskell
   do-notation.  IPython has a sort of pretty-printer, but it
   represents the first result in an uncomfortably vertical fashion:

        [(0, 16),
         (0, 4040),
         (1, 7612),
         (0, 204),
         (0, 14),
         (0, 6),
         (0, 22),
         (0, 10),
         (0, 72),
         (0, 172)]

    Worse, it presents the 121 digraphs on 123 lines, because they
    don’t fit on one, apparently.  If I ask for the first 13 ([:13]),
    they are presented on two lines (one blank); 14 ([:14]) are
    presented on 16 lines.

2. At each step, I had to recompute all the previous computational
   results, rather than having them cached as I would in a
   spreadsheet.  I was manipulating the previous command lines as
   voodoo dolls for the results they computed.  I could have manually
   stored each of them in a variable:

        >>> bs = b''.join([chr((n < 0) | (abs(n) << 1)).encode('utf-8')
                           for n in [8, 2020, -3806, 102, 7, 3, 11, 5, 36, 86]])
        >>> bs
        b'\x10\xe0\xbf\x88\xe1\xb6\xbd\xc3\x8c\x0e\x06\x16\nH\xc2\xac'
        >>> len(bs)
        16

    But when you do that, CPython punishes you twice: first it
    declines to show you even abbreviated results until you ask, as if
    you were using a teletype, and then, if you want to change the
    definition of that variable, you have to manually step back
    through the following computational results.

3. It’s optimized for readability rather than writability.  All those
   apostrophes and parentheses and square brackets are things I had to
   *type* when I just wanted to GO GO GO.
   
4. Generally there’s no tab-completion or other intermediate feedback
   until you submit the command.  Python list comprehensions in
   particular have syntax that frustrates tab-completion: at the point
   you’ve typed `[c + d for c in a + s` there’s no way for the
   interpreter to know that `d` is actually in scope, much less what
   its type is, because it isn’t declared yet.

Here’s another paradigmatic example of an application of scripting, in
this case from my .emacs.d/init.el:

    (defun red-on-black ()
      (interactive)
      (custom-set-faces
       '(default ((t (:stipple nil :background "black"
                      :foreground "red" :inverse-video nil :box nil
                      :strike-through nil :overline nil :underline nil 
                      :slant normal :weight normal :height 120
                      :width normal :foundry "unknown"
                      :family "DejaVu Sans Mono"))))
       '(mode-line ((((class color) (min-colors 88))
                     (:background "black" :foreground "#990000"
                     :box (:line-width -1 :style released-button)))))))

This command sets my Emacs colors to a low-brightness setup that I
find readable when my eyes are tired.  I “wrote” it by interacting
with the Emacs customize-face GUI to select valid values for the
different name-value pairs while getting live feedback on the
resulting appearance, then cutting and pasting the result into a
custom command.  Ultimately it’s just passing a chunk of structured
data to the `custom-set-faces` Lisp function.

Having multiple different, concurrently shown representations of the
same computation automatically kept in sync seems important.
Frugality aside, you’d like to have at least the following four items
always displayed:

1. A display of computational *results*.
2. Context for what the results *mean*.  (The result might be an
    integer, but that integer might be the number of digraphs, for
    example.)
3. An editable representation of the *procedure* that produced the
    results — the command or script proper.
4. Options for what possible changes you can make to the
    procedure/results — if not all of it, at least the currently
    focused part of it.  Tab-completion is one form of this; dialog
    boxes full of widgets are another.

In cases with side effects, you also want a “do it” button which
invokes the side effects, causing the display of results to update.
In Yossi’s example, the command `word 0x1f8cc000` would read a word
from the debug target’s memory, but depending on the address, that
might have side effects (“pop an item from a LIFO, that sort of
thing”), so if you want to invoke `word` speculatively or peremptorily
to get partial feedback, you need to wrap it in a procedure that’s
marked safe for such invocation.

(“Undo” is pretty useful when you can get it, but sometimes your
program is interacting with things outside the computer.)

Both in conventional programming languages and in conventional
command-line languages, #2 and #3 are the same: they are the text of
the command.  This has the advantage of reducing the gulf of
execution: if you know what the command looks like, you know how to
type it, unless it includes D-tier punctuation or foreign characters.
But it increases the gulf of evaluation; you don’t know if your
command means what you meant it to mean.

Separating #2 and #3 potentially ameliorates the syntactic tradeoffs
Yossi was talking about.  In some contexts, they might be individual
keystrokes.  In Emacs, I use command keys to erase characters or
words, cut and paste, search, move the cursor around the buffer, save
the file, indent and outdent lines, etc.  The sequence of keystrokes
is relatively hard to read, but nowadays `view-lossage` includes
command-name annotations that help clarify:

     C-a [move-beginning-of-line]
     c [self-insert-command]
     o [self-insert-command]
     m [self-insert-command]
     m [self-insert-command]
    (...)
     m [self-insert-command]
     e [self-insert-command]
     SPC [self-insert-command]
     C-e [move-end-of-line]
     C-x C-s [save-buffer]
     C-h l [view-lossage]

When I first got access to the internet on VAX/VMS, I bound keystroke
commands like PF1 and PF2 to internet-access commands like FTP and
TELNET, which then entered an INTERNET-SITE state in which these keys
were instead bound to the names of internet sites I frequently used,
such as WUARCHIVE.WUSTL.EDU and NIC.DDN.MIL.  In this way I could
connect to my favorite sites with just a couple of keystrokes.

Menu hierarchies work similarly, but provide a constant display of
available options (#4).  You could imagine Yossi binding
context-dependent control keys to his `word` and `pmem` commands,
giving him a faster way to GO GO GO, without giving up scriptability
or compromising the readability of the resulting script.  Maybe he'd
rename `pmem` to `program-memory` or something, for example.

Runtime inspection facilities
-----------------------------

It turns out to be important to be able to introspect on your scripts
to debug them.  Often this is easier than debugging your C programs,
but it can be harder if your scripting language doesn’t provide
facilities for it.  This involves:

1. Inspecting the runtime state of the program, such as its call
   stack, local variables, and data structures.  This often involves
   pausing execution to keep the state stable during the inspection.
   Taking a snapshot of the relevant parts of the program state is an
   alternative.
2. *Changing* that runtime state in arbitrary ways.  The most common
   change is to step forward by an instruction, statement, or
   subroutine call, but altering the program counter, call stack, or
   local variables is also common.
3. Attaching traces to certain events, such as a certain subroutine
   being called, or a certain variable changing, which trigger some
   action, such as incrementing a counter, logging the state of some
   data structure, or changing the value of a parameter.

Facilities like these are often intended for debugging, but they can
be used for other things, such as profiling, advice, live code
upgrade, or algorithm visualization.

Sofware engineering facilities
------------------------------

We have some newly popular techniques — some of them actually
new — that can really help with debugging software, including even
simple scripts.  Some of these are enormously easier to implement if
support for them is built in at a basic level, although this can
conflict with other virtues like not using much memory.

Logging has been crucial to debugging at least back to the 01950s, but
*structured logging* records full data structures that can be more
effectively queried afterwards.

Static typing with type inference, pioneered in Milner’s LCF but now
popular in languages like Golang and TypeScript, catches many bugs at
compile time.

Automated testing allows us to detect many bugs automatically as soon
as they are introduced — and, perhaps even more importantly, gives us
a large body of established facts about our software’s behavior that
we don’t have to re-establish when debugging.  DejaGnu is the first
example I came across, in the early 90s.  Fuzzing and “generative” or
“property-based” testing frameworks like Hypothesis provide enormously
better cost-benefit ratios for automated testing.

Instruction coverage and branch coverage can be used to guide manual
testing, manual automated tests, and generative tests; a defect in an
instruction or a branch that is never executed during tests cannot be
detected by those tests.

Version-tracking systems like Git, originating in PWB/UNIX’s SCCS
(with a paper published in 01978), make it easy to reproduce past
software configurations so we can determine when bugs were introduced,
and to see differences between them to help us understand what the bug
is.

Copy-on-write data structures make Git acceptably efficient, and they
can also make it fast to take a snapshot of program state for
inspection.

Automated history bisection debugging, such as `git bisect`, uses
automated testing and version-tracking software to *automatically*
determine in which code change a bug was introduced.  A new variant of
bisection debugging was recently discovered by [the Cinder team][6]
and [the Golang team][5], which performs binary search over the
execution history of a program running an automated test rather than
over the change history of the program, which is specific to the case
where a change to a working component of the program broke a different
part of the program whose behavior it shouldn’t have affected.

[5]: https://research.swtch.com/bisect
[6]: https://bernsteinbear.com/blog/cinder-jit-bisect/

Formal proofs of correctness have finally advanced to the point that
entire operating systems like seL4 can be proven correct.  SMT solvers
are routinely used for verifying some software.

“Mocking” an interface used by a piece of code facilitates testing it
in a variety of ways; the mock implementation can simulate situations
that are difficult to reproduce in reality, and it can do checks on
how it’s being invoked that are difficult to check in reality.

A particularly interesting application of mocking is record-and-replay
systems like `mec-replay` and `rr`, sometimes called “omniscient
debuggers”.  This makes it theoretically possible to query the
execution history of a program to answer questions like “How did this
pointer get set to NULL?”  This kind of re-execution is the same kind
of re-execution described earlier to avoid caching a large
intermediate result in limited memory.

There’s also a whole field of observability and telemetry stuff I
don’t know much about, where failures or anomalies in the field are
logged, aggregated, and available for later queries.

Namespace management is also easy to overlook, but necessary when you
have many scripts you don’t want to break each other.
