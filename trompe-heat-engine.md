Recently I heard about [Todd Thompson’s new air compression startup
Carnot Compression, based on the trompe][0].  Unfortunately [it seems
to have folded in 02021 after eight years of development][1] begun in
02013 [with a provisional patent granted in 02017][2] to Mark
A. Cherry, Robert A. Alderman, and D. Hans Shillinger, originally at
Rotational Trompe Compressors LLC.

[0]: https://news.ycombinator.com/item?id=27066295
[2]: https://patents.google.com/patent/US20150023807A1/en (patent US9618013B2)
[1]: https://carnotcompression.com/news

The trompe is an air compressor with no moving parts, powered by
water.  It would seem to belong to the Early Modern Era of buildings
built around machinery rather than machinery built to install in
buildings; it works by mixing bubbles into water that is dumped into a
well going deep underground, where the bubbling water flows around a
bend into a separation chamber, where the air bubbles out of the
water.  The water, without the air, then flows up a second well back
up to ground level, but necessarily a somewhat lower ground level:
without the bubbles, its density is higher, so the undiluted water
column reaches the same pressure with a smaller change in elevation.
The energy it has lost has been stored in the compression of the air
bubbles as they are drawn down the chute; this can provide dry air for
pneumatic power.

Because the compression is isothermal, the efficiency can be
surprisingly high despite the turbulent losses in the water.  But the
physical size of the apparatus would seem to be necessarily enormous:
to get a single bar of compression requires a head difference of 10
meters, and if the bubbly water is 10% bubbles (at which point the
diameter of the bubbles is comparable to the separation between them)
the separation chamber must be 100 meters underground.

The startup, whose name I have forgotten, had reduced their prototype
trompe to a portable size by putting it inside a centrifuge rather
than relying on gravity for the compression.  This practically
necessitates a source of electricity to drive it, rather than natural
hydraulic power.  I don’t know exactly how it works, but presumably
either the water being centrifuged is pumped “uphill” toward the
center of the rotor, opposing the centrifugal force, to provide the
energy used to compress the air; or the rotor slows down as the water
is on net displaced “downhill” toward the rim of the rotor and away
from its center, thus losing kinetic energy.

Can you run this approach in reverse as an isothermal heat engine?

Trompes are reputedly not very efficient because the water falling
down the chute acquires kinetic energy that is then dissipated in the
separator.
