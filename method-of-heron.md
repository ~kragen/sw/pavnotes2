I was thinking about Heron’s method of extracting square roots, of
which Newton’s method of root finding is a generalization.  In Heron’s
method to find the square root of some number *a*² you iterate guesses
*cᵢ* with the recurrence relation

> *cᵢ* = ½(*cᵢ*₋₁ + *a*²/*cᵢ*₋₁)

starting from some initial nonzero guess *c*₀.  So, for instance, if
*a*² = 100 and *c*₀ = 1, the sequence goes roughly 1, 50.5, 26.24,
15.03, 10.84, 10.0326, 10.000'0529, 10.000'000'000'14, etc.

In common cases, we expect the error to vanish quadratically like this
in Newton’s method based on an approximation of the error in terms of
the second derivative.  But that analysis is approximate, subtle, and
tricky, and it has surprising exceptions.  In the case of Heron’s
method in particular, we can do better with an exact algebraic analysis.

Let *bᵢ* be *cᵢ* - *a*, the unknown error in the square root
approximation.  How does it change with the iteration?  We have

> *a*²/*cᵢ* = *a*²/(*a* + *bᵢ*)

and, as I was trying to take a nap this afternoon to recover from
covid, I realized we can rewrite

> *a*² = (*a* + *bᵢ*)(*a* - *bᵢ*) + *bᵢ*²

so we can cancel out the *a* + *bᵢ* factor and get

> *a*²/*cᵢ* = *a* - *bᵢ* + *bᵢ*²/(*a* + *bᵢ*)

so our next iteration value is

> *cᵢ* = ½(*a* + *bᵢ*₋₁ + *a* - *bᵢ*₋₁ + *bᵢ*₋₁²/(*a* + *bᵢ*₋₁))
> = *a* + ½*bᵢ*₋₁²/(*a* + *bᵢ*₋₁)

and the error in that iteration is

> *bᵢ* = *cᵢ* - *a* = ½*bᵢ*₋₁²/(*a* + *bᵢ*₋₁).

There are basically four regimes of behavior here, three of which are
on display above.  When *bᵢ*₋₁ is large compared to *a*, it
approximately halves, like from 40.5 to 16.24:

> *bᵢ* = ½*bᵢ*₋₁²/(*a* + *bᵢ*₋₁) ≈ ½*bᵢ*₋₁²/*bᵢ*₋₁ = ½*bᵢ*₋₁

When it’s small compared to *a*, the new error is roughly half the
square of the old error divided by *a*:

> *bᵢ* = ½*bᵢ*₋₁²/(*a* + *bᵢ*₋₁) ≈ ½*bᵢ*₋₁²/*a*

So, for example, in the above example, when the error was .0326, its
square was .001'06, half of it was .000'531, divided by *a* = 10 you
get .000'0531, and the actual error in the next step was .000'0529.

A third regime is when neither does *cᵢ* ≈ *a* nor does *cᵢ* ≈ *bᵢ*
because *bᵢ* ≈ -*a*, which was the case in the first iteration above,
when we went from *b*₀ = -9 to *b*₁ = +40.

How about when *bᵢ* ≈ -2*a*?  For example, if *c*₀ = -10, we have a
fixed point, which we can understand in terms of Newton’s method as
finding the other zero of *x*² - 100:

> *c*₁ = ½(*c*₀ + *a*²/*c*₀) = ½(-10 + 100/-10) = ½(-10 + -10) = -10.

Here if we apply our iteration formula for *bᵢ* from before,

> *bᵢ* = ½*bᵢ*₋₁²/(*a* + *bᵢ*₋₁),

we get

> *b*₁ = ½*b*₀²/(10 + *b*₀) = ½100/(10 + -20) = ½100/-10 = 50/-10 = -5,

which is wrong.
