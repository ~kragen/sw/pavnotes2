In file `near-natural-adhesives.md` I talked a bit about resins as
possible near-natural adhesives, and I mentioned that you need
thinners or solvents for them.

The near-natural room-temperature solvents that occur to me as
relatively easy to obtain are water (of course), turpentine,
d-limonene, methanol, acetic acid, acetone, ethanol, ethyl acetate,
and carbon dioxide.

[Turpentine][0] can be distilled from pine resin and some other
resins, or even just from the wood.  This requires a distillation
apparatus, but that’s all.  Adequate distillation apparatus can be
made with glazed pottery and castable refractory.  For some reason
Wikipedia says you need to use steam distillation and a copper still,
but I don’t see why simple distillation wouldn’t be enough; would you
destroy the delicate terpenes?  Would that be a bad thing in this
case?  [McCranie][14] apparently used fire distillation before
switching to steam, so I think it’s just an efficiency thing, not a
feasibility one.

[0]: https://en.wikipedia.org/wiki/Turpentine
[14]: https://en.wikipedia.org/wiki/McCranie%27s_Turpentine_Still

[D-limonene][1] is an essential oil like turpentine but less toxic to
the humans.  It’s a major constituent of citrus peels; if you only
need droplets of it, you can just squeeze it out with your hands, but
if you want to use it to paint things with resin, you need to use
steam distillation.  But that’s all, and you get pretty much pure
limonene out of the distillation.

[1]: https://en.wikipedia.org/wiki/Limonene

Methanol can apparently be produced by destructive distillation of
hardwoods, [and has been since ancient Egypt][2], which is why it’s
called “spirit of wood”.  But I think that in general you’ll get a lot
of different products out of that, the so-called [pyroligneous acid or
liquid smoke][3], which WP says is mostly acetic acid, acetone, and
methanol (but I think water is a bigger component than any of these),
so you’ll need to do further distillation if you want to separate
those, and in particular you’ll need to break the acetone–methanol
azeotrope (55.7°, 88% acetone, according to [US patent 4,501,645][4]).
But maybe that azeotrope is already a good enough solvent for your
purposes.

The acetic acid can be easily separated, I think, because it doesn’t
boil until 118°.  Pyroligneous acid was the world’s main source of
acetic acid by 01910, but at the time it was separated in a different
way, one using lime and sulfuric acid.

[4]: https://patents.google.com/patent/US4501645A/en
[3]: https://en.wikipedia.org/wiki/Pyroligneous_acid
[2]: https://en.wikipedia.org/wiki/Methanol#History

[Acetic acid][8] is very useful as a food preservative, because it can
easily lower the pH of food to a point that will preserve it for
years.  It’s useful as a reagent because it forms soluble salts with
almost all metals, even some difficult cases like lead and silver, but
its usefulness as a solvent for reactions is largely confined to
organics.

[8]: https://en.wikipedia.org/wiki/Acetic_acid

If you can get acetic acid, though, you can probably neutralize some
lime with it to form [acetate of lime][5], which decomposes at 160° to
anhydrous acetone and carbonate of lime, [a process which commercially
produced a 75% yield in 01919][6] apparently using higher temperatures
of 620° to 880°, or [at least 430°][7] in order to get it to happen
reasonably fast.

Acetone is interesting because it can dissolve most natural resins,
including amber, and also cellulose acetate and celluloid.

(Other acetates such as those of barium and lead have also been
employed.)

[5]: https://en.wikipedia.org/wiki/Calcium_acetate
[6]: https://zenodo.org/records/1428748 "Losses Incurred in the Preparation of Acetone by the Distillation of Acetate of Lime, by Leo Frank Goodwin and Edward Tyghe Sterne, Journal of Industrial and Engineering Chemistry, Vol. 12, No. 3, 01920-03-01, pp. 240–243"
[7]: https://pubs.acs.org/doi/abs/10.1021/ie50179a013 "Distillation of Acetate of Lime, by Ardagh, Barbour, McClellan, and McBride, Industrial and Engineering Chemistry, November, 01924, pp. 1133–1139"

About 10% of the world’s acetic acid today is made by fermenting
aqueous ethanol into vinegar, which is still probably the laziest way.
If you mix up fruit and water and let it sit for a few months, you’ll
get vinegar.  Distilling the ethanol in between will probably reduce
the amount of crap you have to remove from your acetic acid.  Normally
the vinegarification process takes months, but [by bubbling air
through a continuously stirred tank, it can take only 24 hours.][9]
[Even just having a large surface area wet with your vinegar exposed
to air can get it down to a few weeks][11].  A [starter culture or
“mother” will also speed the process up a lot][10].

[9]: https://en.wikipedia.org/wiki/Acetic_acid#Oxidative_fermentation
[11]: https://supremevinegar.com/quickly-make-vinegar-semi-quick-process-making-vinegar-boerhaave-process/
[10]: https://www.vinegarshed.com/pages/how-to-make-a-vinegar-mother

To get the ethanol, of course, you ferment sugars, either from mash or
from fruit.

[Ethyl acetate][13], the acetone-free nail polish remover, is made by
reacting ethanol with acetic acid, potentially just at STP.  It can
also dissolve celluloid, polystyrene, and decaffeinate coffee.  I
think it can even dissolve cellulose acetate if mixed with 20%
ethanol.

[13]: https://en.wikipedia.org/wiki/Ethyl_acetate

Carbon dioxide is the most interesting nonpolar solvent, and very easy
to get, but liquefying it requires [exceeding its critical pressure of
7.3773 MPa][12].  This seems potentially difficult to bootstrap in the
wilderness, and it won’t help with the desire to paint glue onto
things.  (The critical temperature is only 30.9780°, but that’s cold
comfort.)

[12]: https://en.wikipedia.org/wiki/Supercritical_carbon_dioxide
