I was thinking that common Argentine coins might be a reasonable
source of raw metals to purify by electrowinning, particularly given
their relative worthlessness after the collapse of our currency over
the last few years.  [The current series][1] with the tree flowers is
absolute shite, being all steel with various platings except for the
$10, which is nickel silver of some uncontrolled composition.

[1]: http://www.bcra.gob.ar/MediosPago/Nueva_familia_monedas_i.asp

But there are lots of older coins still in circulation, and it looks
like some of them might be useful sources of nickel.

The [1 centavo coin][2] in circulation since 01992 was pretty much out
of circulation before I ever came to Argentina, though old people
sometimes have them.  There were three variants; the octagonal one and
one of the circular ones were 92% Cu, 8% Al, while the other circular
one is 97% Cu, 2.5% Zn, 0.5% Sn.  I think these are all 16.2 mm
diameter but the aluminum-bronze coins are 1.77 g while the brass
ones, which I think may have been introduced later (the example on the
BCRA's site is stamped "1998" rather than "1992"), are 2.00 g.  The
circular coins still have an octagonal stamping on their face.

[2]: http://www.bcra.gob.ar/MediosPago/Moneda_1_centavo.asp

The [5 centavo coin][3], also in circulation since 01992, has an
aluminum-bronze variant (92% Cu, 8% Al, 2.02 g) with milled edges, and
two steel-on-brass variants with smooth edges, one white and one
yellow.  The yellow steel is 18.2 mm in diameter and 2.20 g, while the
aluminum-bronze and white steel versions are 17.2 mm.

[3]: http://www.bcra.gob.ar/MediosPago/Moneda_5_centavos.asp

The [10-centavo coin][4], also since 01992, also has a milled-edge Cu
92/Al 8 version weighing 2.25 g and a smooth-edge steel-on-brass
version weighing 2.20 g.  These were common when I arrived in 02006
but have not been in real circulation for years.

[4]: http://www.bcra.gob.ar/MediosPago/Moneda_10_centavos.asp

The [25-centavo coin][5], also since 01992, has an aluminum-bronze
version (92% Cu, 8% Al, 5.40 g) and a nickel silver version (75% Cu,
25% Ni, 6.10 g).  Otherwise these are identical.  They were common
until just a few years ago, maybe 02017.

[5]: http://www.bcra.gob.ar/MediosPago/Moneda_25_centavos.asp

The [50-centavo coin][6] also since 01992, only exists in a 92% Cu, 8%
Al, 5.80 g version.

[6]: http://www.bcra.gob.ar/MediosPago/Moneda_50_centavos.asp

The [$1 coin][7], introduced in 01994, is aluminum bronze in the
center (92% Cu, 6% Al, 2% Ni) with a nickel silver ring around it (75%
Cu, 25% Ni).  It weighs 6.35 g, though I have observed significant
deviations from this.  This is unchanged in [the bicentennial
version][8] and the [May Revolution bicentennial version][9].

[7]: http://www.bcra.gob.ar/MediosPago/Moneda_1_peso.asp
[8]: http://www.bcra.gob.ar/MediosPago/Moneda_1_peso_bicentenario.asp
[9]: http://www.bcra.gob.ar/MediosPago/Moneda_1_peso_bicentenario_revolucion_mayo.asp

And the [common 7.2 gram bimetallic $2 coin][0], in circulation since
02011, has an outer ring that's 92% Cu, 6% Al, 2% Ni, and a nickel
silver center: 75% Cu, 25% Ni.  It weighs 7.20 g.  I got two of these
and a $1 coin in change today.

[0]: http://www.bcra.gob.ar/MediosPago/Moneda_2_pesos.asp

[Older circulating coins][10] are no longer valid and their face value
is worthless due to inflation before 01992 anyway.  The Austral (A),
the currency from 01985 to 01992, is still in the possession of many
people, with denominations of ½ centavo, 1 centavo, 5 centavos, 10
centavos, 50 centavos, 1 ₳, 5 ₳, 10 ₳, 100 ₳, 500 ₳, and 1000 ₳.  By
the end there were 500000-₳ bills, and the new peso was 10000₳.  [The
centavo coins were all the same 92% Cu, 8% Al aluminum bronze][11],
while the austral coins were all pure aluminum.  From my point of view
this is perfectly worthless, except perhaps as a novelty, because
scrap copper and aluminum are more easily available in other forms.

[10]: http://www.bcra.gob.ar/MediosPago/Emisiones_anteriores.asp
[11]: https://en.wikipedia.org/wiki/Argentine_austral

From 01983 to 01985 we had the "peso argentino" (aluminum centavos and
1$a coin, brass 5$a and 10$a, aluminum bronze 50$a) and before that
the "peso ley" from 01970 to 01983.
