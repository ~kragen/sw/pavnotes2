I was thinking more about tone detection with comb filters, and I
found a couple of very appealing ideas, including one that seems
capable of doing tone detection in an order of magnitude less
multiplies per sample than the standard Goertzel algorithm, with a
close approximation of a Gaussian window.

Basic comb filter tone detection
--------------------------------

It occurred to me (and I think I’ve said this before) that for optimal
efficiency for detecting a single tone, you want to downsample to 2–4×
times the frequency of interest, so that each oscillation in your comb
filter is just 2–4 samples.  For example, if you’re trying to detect a
1046.5 Hz tone (C₆, soprano C, high C) in 48000-sample-per-second
audio data, you might resample the data to 4186 samples per second
before using a comb filter to filter down to that particular tone.

However, much of my interest is in doing this at very low
computational cost, and the cheap ways I know to downsample a signal
like this can only downsample by integer amounts.  There must be some
way to hack a Hogenauer filter to downsample by some variable amount,
but the by-the-book third-order version is that you run your digitized
signal through three integrators, decimate it by some integer ratio
(downsampling 48000 Hz by 11, for example, gives you 4363.6 samples
per second), run three feedforward comb filters with a single-sample
lag on the decimated result.  In the absence of rounding error, the
composition of an integrator and a feedforward comb filter gives you a
box filter (convolution with a pulse) and so the impulse response of
this Hogenauer downsampling filter is the convolution of three box
filters, giving you a piecewise-quadratic approximation of a Gaussian
low-pass filter.  This doesn’t have a very flat passband, so you throw
the now properly downsampled signal at a FIR filter to flatten it.

The FIR part is unnecessary if you’re just trying to detect a single
tone or a few tones.  But if you now run a feedback (recursive) comb
filter on this signal, with a lag of 2 or 4 samples, your filter peak
will be at 1090.9 Hz instead of the desired 1046.9.  That’s 72 cents
sharp, almost a whole half-step off.  If your filter is sharp enough
(has high enough Q) to separate tones a half-step apart, it’ll heavily
attenuate the tone you actually wanted.

Still, this filter is pretty appealing from a computational cost
standpoint.  Each new sample increments the sample counter, does three
wraparound integer additions to the accumulators in the integrators,
and one out of 11 times, runs the feedforward combs (three
subtractions) and your feedback comb filter (an addition or
subtraction if it’s unity-gain).  So this is 4.36 RTL operations per
sample, all of which can run in parallel, and one of which is just an
increment, while the others are all additions and subtractions with no
multiplication or other expensive stuff.

As I’ve written elsewhere, you can consider the recursive comb filter
to be a sort of integrator that operates at your target frequency
instead of at DC, and you can do the same Hogenauer trick to get an
impulse response of a nicely-approximated Gaussian wave packet at the desired
frequency: cascade three or more unity-gain feedback combs (instead of
integrators), optionally decimate, and then run the same number of
feedforward combs to precisely cancel the poles of the feedback combs,
of course assuming there is no rounding error.  How big your
wavepacket is (how long the lags in the last stage are) determines the
Q of your filter.

Supposing that the decimation is a factor of 40 (ten full cycles) and
we again use a third-order filter here, then we’re doing:

- an increment and three additions every sample;
- every 11 samples:
    - three subtractions for the feedforward combs;
    - three additions or subtractions for the feedback combs;
    - every 40 times:
        - three additions or subtractions for the feedforward combs.

This works out to 4.55 RTL operations (integer additions and
subtractions in this case) per sample and a fixed memory footprint of
13 registers, and gives you an I/Q signal with a new I/Q value pair
every 880 samples.  On an FPGA or something, you don’t need any
further calculations, but on a CPU you will need array-indexing
operations as well.  On an FPGA you might prefer to do the
less-frequent operations bit-serially, using shift registers, because
probably implementing a state machine to multiplex a parallel adder
among different registers would be a cure worse than the disease.

If you’re downsampling the signal to several different sample rates,
for example to detect several different tones, you can share the same
sample counter and front-end integrators between all the different
frequencies, so adding a new similar frequency only requires another
0.55 or so operations per sample.  Similarly, if you want several
different bandpass filters with the same center frequency but
different Q, you can share the feedback combs, so each additional Q
only costs about 0.007 additions or subtractions per sample.

The antialiasing Hogenauer filter in the downsampling step removes
harmonics that the comb would otherwise be responsive to.  But what if
you want to detect those harmonics?

Harmonic detection
------------------

Certain signals, like a person whistling or the tones in DTMF, are
characterized by the absence of any significant harmonics; they’re
almost pure sine waves.  So it’s potentially of interest to analyze
both a fundamental and one or more of its harmonics, enabling you to
avoid spurious tone detections.  Let’s call this “note detection”,
since a musical note typically contains a fundamental tone and some of
its harmonics.

Comb filters get their name from the fact that their frequency
response has an infinite sequence of evenly spaced peaks or poles (in
the case of recursive, feedback combs) or nulls or zeroes (in the case
of nonrecursive, feedforward combs), which are harmonics of a
fundamental — either all the harmonics or just the odd harmonics,
depending on whether the two unit impulses in the filter are of the
same sign or opposite signs.

So if you have an unity-gain feedback comb filter with a pole at
1046.9 Hz, it will also have poles at 3140.7 Hz, 5234.5 Hz, 7328.3 Hz,
etc., and possibly also at 0 Hz, 2093.8 Hz, 4187.6 Hz, 6281.4 Hz,
8375.2 Hz, etc.  This aspect of their impulse response is exploited in
the Karplus–Strong Digitar algorithm, for example, by stimulating it
with a noise pulse, and it’s why you get such a nice guitar note out
of it.

The delay line in a recursive comb filter is sort of like a buffer
that holds a sum of all the periods seen so far, or all the
half-periods with alternating signs if you’re using negative unity
gain.  (Or an exponential moving average, if the feedback coefficient
is less than unity, but as I said above I think that’s often better
done as a later windowing stage so you can get a better window shape).

Suppose you’d like to detect second and third harmonics.  You need to
use a positive-feedback comb filter, and let’s suppose you use unity
gain.  The fundamental will oscillate once from one end of the buffer
to the other, the second harmonic twice, and the third harmonic three
times.  If we want to be sure we catch the third harmonic whatever its
phase is, we need 12 samples in the buffer.  The six basis functions
we’re looking for in those 12 samples are:

- cos *θ*: [1, 0.866, 0.5, 0, -0.5, -0.866, -1, -0.866, -0.5, 0, 0.5, 0.866]
- sin *θ*: [0, 0.5, 0.866, 1, 0.866, 0.5, 0, -0.5, -0.866, -1, -0.866, -0.5]
- cos 2*θ*: [1, 0.5, -0.5, -1, -0.5, 0.5, 1, 0.5, -0.5, -1, -0.5, 0.5]
- sin 2*θ*: [0, 0.866, 0.866, 0, -0.866, -0.866, 0, 0.866, 0.866, 0, -0.866, -0.866]
- cos 3*θ*: [1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0]
- sin 3*θ*: [0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1]

That is, first we estimate our waveform by looking at our 12-sample
buffer, or a difference between two snapshots of it, or some other
windowed view.  Then we take its dot product with each of those
12-element vectors, telling us the amount of each harmonic that is
present.

Unlike everything up to this point, this does require some
multiplications.  However, it actually requires very few of them
because of the paucity of distinct factors.  The third-harmonic
signals don’t require any multiplications at all, just five additions
and subtractions.  The sin 2*θ* signal requires a single
multiplication by 0.866 at the end, following seven additions and
subtractions.  The cos 2*θ* signal requires 11 additions and
subtractions followed by a multiply-accumulate with 0.5.  The
fundamental signals are the priciest, requiring 9 additions and
subtractions each, followed by *two* multiply-accumulates each, both
with 0.866 and 0.5.  So in total we need 46 additions and
subtractions, 6 multiplies (though half of them are by 0.5), and
possibly 5 more additions.  And for this we get three I/Q channels for
these three harmonics.

Actually when I said you need 12 samples in the buffer to be sure of
catching the third harmonic, I lied.  You only need 7, but then the
basis functions you’re looking for look like this:

- cos *θ*: [1, 0.623, -0.223, -0.901, -0.901, -0.223, 0.623]
- sin *θ*: [0, 0.782, 0.975, 0.434, -0.434, -0.975, -0.782]
- cos 2*θ*: [1, -0.223, -0.901, 0.623, 0.623, -0.901, -0.223]
- sin 2*θ*: [0, 0.975, -0.434, -0.782, 0.782, 0.434, -0.975]
- cos 3*θ*: [1, -0.901, 0.623, -0.223, -0.223, 0.623, -0.901]
- sin 3*θ*: [0, 0.434, -0.782, 0.975, -0.975, 0.782, -0.434]

This requires three honest-to-God multiplications per detected basis
function instead of half a multiplication and half a bit shift right.

However, all these multiplications are happening something like once
every 256–2048 samples.  The comb-filtering operations that go into
accumulating the harmonics are going to happen typically at least an
order of magnitude more often; for high-Q filters, multiple orders of
magnitude more often.  (And of course the front-end integrators run
even faster than that, but they can be shared between multiple notes.)
So it’s probably okay to do these multiplications, because even
multiplication that is enormously slower than the addition the
integrators are doing will still be fast enough.

From a certain point of view, what we’re doing here is paying the cost
to downsample to a frequency that is still high enough to detect the
third harmonic, but then by accumulating the waveform using a comb
filter with a longer lag, we get the fundamental and second harmonic
almost for free.

Avoiding uncomfortable decimation ratios with gear ratios
---------------------------------------------------------

There’s something else appealing about this harmonic-detection
approach: in the 7-samples-per-buffer example above, we’re first
downsampling to some sample frequency *f* (perhaps the original sample
rate divided by an integer), but then detecting a tone at the
frequency 7*f*/3.  We can evidently do this for *f* divided by any
rational fraction *n*/*m* that is less than ½, at a cost that is
extremely modest until the denominator *m*, the size of the required
delay line and consequent number of coefficients, becomes very large
indeed.  For example, *m* of 200 would require, usually, 398
multiply-accumulates per window.  If the window includes 2000 samples,
that’s still less than 0.2 multiply-accumulates per sample.

To take our original example of wanting to detect a 1046.9-Hz signal
in 48-ksps audio, we could start by downsampling by a factor of 19,
down to 2526.3 samples per second.  We could set our delay line size
to 111, so it accumulates tones that are harmonics of 22.7596 Hz.  And
then by doing 220 multiply-accumulates per window, we can measure its
46th harmonic, which is 1046.94 Hz.  Probably even a much worse
approximation would do, such as a 41-sample window (a 61.617 Hz
fundamental) and its 17th harmonic, 1047.5 Hz, doing 80
multiply-accumulates.  That would be adequate even with a Q of 500, a
value which would imply we were doing this dot-product operation on
the order of twice a second.

After picking 19 as being close to the highest plausible decimation
(24 or higher would make the task impossible, since our signal would
be below Nyquist) I derived these values (111/41 and 41/17) from a
[continued-fraction approximation][0] of 1046.9/2526.3 = 2.4131395:

    5/2 = 2.5
    12/5 = 2.4
    29/12 = 2.41666666667
    41/17 = 2.41176470588
    70/29 = 2.41379310345
    111/46 = 2.41304347826
    514/213 = 2.41314553991

[0]: http://canonical.org/~kragen/sw/netbook-misc-devel/contfrac.py

These are the truncated approximants of 2 + 1/(2 + 1/(2 + 1/(2 +
1/(1 + 1/(1 + 1/(1 + 1/(4))))))) = 514/213.

This method is recommended in the Machinery’s Handbook for choosing
the number of teeth on gears to approximate a desired gear ratio.  It
seems like a potentially much cheaper and more precise way to do tone
detection than the approaches I’d seen so far.  With the same
decimation of 19, 133/52 approximates 987.7666 Hz (B₅) as 987.73 Hz,
84/31 approximates 932.3275 Hz (A♯₅) as 932.331 Hz, and 89/31
approximates 880 Hz (A₅) as 879.95 Hz.  Cruder approximations are 23/9
(989 Hz), 19/7 (931 Hz), and 23/8 (879 Hz) respectively; note that two
of these could share a 23-sample buffer.

I suspect that this method will cost half as many multiplies if we use
a negative-unity-feedback comb for the accumulation instead, which
also eliminates DC.

### The efficiency in one example ###

Let’s consider the cost of detecting A₅ (880 Hz) in a 100-millisecond
window, using a piecewise-quadratic approximation to a Gaussian
window, with the 48000/19/(23/8) approximation.  We pass 4800 samples
through three integrators, requiring 14400 additions.  We decimate
this by a factor of 19, leaving probably 253 decimated samples.  We
run these through three differencers (lag-1 feedforward unity-gain
comb filters) at a cost of 759 subtractions.  We have a cascade of
three 23-sample delay lines implementing recursive feedforward
positive-unity-gain comb filters, and so we add the 253 samples to the
first one to get 253 samples to add to the second one, and so on, at a
cost of 759 additions, not counting index arithmetic.  (By luck, 253
is a multiple of 23, so each sample is going through the same number
of updates.) Then we feed our final 23 samples through a cascade of
three lag-23 differencers to compute the average 109.84-Hz wave across
our Gaussian window; these differencers are initialized with state
from the previous window.  This costs 69 more subtractions, not
counting index arithmetic.  Finally, we take the dot product of the
23-sample signal with 23 samples of frequency-8 sine and cosine waves,
which are really 878.719 Hz, at the cost of 44 more
multiply-accumulates.

Whew.  The total is 15159 additions, 828 subtractions, and 44
multiply-accumulates, 3.34 operations per sample (not counting
incrementing the sample counter or index arithmetic) and our Q is
somewhere around 90, so we’re detecting tones within about a
10-Hz-wide window.  89.8% of this work is the initial downsampling
integrators.

If we want to also detect A₄, A₃, or B₅, we don’t need any more delay
lines; we can run additional dot products with sinusoids of frequency
4, 2, and 9 respectively, which will cost 44 multiply-accumulates
each, 0.01 multiply-accumulates per input sample.  If we do the full
Fourier transform on our 23-sample window, the frequencies are:

- DC
- 109.84 Hz (A₂, 110 Hz)
- 219.68 Hz (A₃, 220 Hz)
- 329.52 Hz (E₄, 329.6 Hz)
- 439.36 Hz (A₄, 440 Hz)
- 549.20 Hz (a relatively poor C♯₅, 554 Hz)
- 659.04 Hz (E₅, 659.3 Hz)
- 768.88 Hz (in between G♭₅ and G₅)
- 878.72 Hz (A₅, 880 Hz, as mentioned above)
- 988.56 Hz (B₅, 987.8 Hz, as mentioned above)

So with this one delay line we detect not one note but seven or eight.
What we’re doing here is pretty close to resonating the A₂ string on a
piano or a guitar with the input signal, though that would give us an
exponential-decay window rather than the Gaussian.

Each of these frequencies costs us another 22 multiply-accumulates in
the brute-force Fourier approach.  I think Rader’s algorithm is a fast
version of the Fourier transform for prime sizes like 23, but I don’t
know whether it would actually be a savings in this case.

(Actually, because the sine and cosine vectors have respectively odd
and even symmetry between their first 11 samples and their second 11
samples, you could get by with half that number of multiplies by
previously summing or differencing the samples that need to be
multiplied by the same absolute weight.)

The 10-Hz-wide passband of these filters is probably too wide for the
lower octaves, so it might be advisable to use a second set of
differentiators on the same 110-Hz resonator to get a narrower
passband.

If we want to add the detection of, say, A♯₅ (932.3275 Hz), we need to
feed our downsampled signal into another delay line or cascade of
them, this one of 19 samples, costing us another 759 additions (0.16
per sample) and 57 subtractions, and 36 multiply-accumulates with a
7-cycle signal then gives us I and Q signals at 930.75 Hz.  The rest
of the Fourier transform on this new resonator gives us:

- DC
- 132.96 Hz (C₃, 131 Hz)
- 265.93 Hz (a rather poor C₄, 262 Hz)
- 398.89 Hz (in between G₄ and G♯₄)
- 531.86 Hz (in between C₅ and C♯₅)
- 664.82 Hz (in between E₅ and F₅)
- 797.78 Hz (in between G₅ and G♯₅)
- 930.75 Hz (our desired A♯₅)
- 1063.71 Hz (in between C₆ and C♯₆)
- 1196.68 Hz (in between D₆ and D♯₆)

These mostly don’t hit musical notes because 7, the numerator of our
approximation 7/19, isn’t a product of 2s and 3s.

### A slight reformulation ###

We can look at the above as downsampling the original signal by a
factor of 19 with a third-order Hogenauer filter, then converting it
into a 23-dimensional vector signal which is then downsampled by a
factor of 11 with another third-order Hogenauer filter, before finally
taking the dot products with the sin 8*θ* and cos 8*θ* vectors.

Or we can think of it as "commutating" the original 19× downsampled
signal among 23 different Hogenauer filters (operating on scalars
instead of 23-dimensional vectors), each running at 1/23 the speed,
like a polyphase filter.

### Negative-feedback combs can apparently give an additional boost ###

I’ve had more affection in the past for negative-feedback comb
filters, which is to say, *y*[*n*] = *x*[*n*] - *y*[*n* - *m*], for
three reasons: they require half as many samples of memory, they have
a null in their response at DC (precisely zero response), and they
have a null in their response at the second harmonic.  If *m* = 5, for
example, the filter has poles at periods 10, 10/3, 10/5, and so on,
and zeroes at periods 0 (DC), 10/2, 10/4, and so on.

In this context, if we can manage to use such a negative-feedback
filter as our resonator, not only would we no longer have to worry
about DC bias, we might be able to cut the number of
multiply-accumulates per sample in half again.

Essentially instead of having a natural number of oscillations within
the window (0, 1, 2, etc.) we have an *odd* natural number of
*half*-oscillations (1, 3, 5, etc.).  Waves that have an *even* number
of half-oscillations would need to be followed by another copy of the
window waveform to continue, but what the negative-feedback comb gives
us is a *negated* copy in continuation.

So, suppose we have an 11-sample window and 5 half-oscillations; the
period of the tone detected is then 22/5 = 4.4 samples.  If we were
using the same 2526.3-Hz intermediate sample rate, that would be a
574.16 Hz tone.  The sample weights to detect this tone would then be:

- cos: [1, 0.142, -0.959, -0.415, 0.841, 0.655, -0.655, -0.841, 0.415, 0.959, -0.142]
- sin: [0, 0.990, 0.282, -0.910, -0.541, 0.756, 0.756, -0.541, -0.910, 0.282, 0.990]

So at least in this case we haven’t lost the symmetry of the weight
vector that gives us our five half-oscillations; we can compute these
dot products with only five multiply-accumulates each rather than 10.
And that still holds for 9 half-oscillations (9/22 of 2526.3 Hz:
1033.5 Hz):

- cos: [1, -0.841, 0.415, 0.142, -0.655, 0.959, -0.959, 0.655, -0.142, -0.415, 0.841]
- sin: [0, 0.541, -0.910, 0.990, -0.756, 0.282, 0.282, -0.756, 0.990, -0.910, 0.541]

So I think we can get this 2× bonus with a negative-feedback comb for
any ratio *n*/*m* such that *n* is odd, *m* is even, and *n* is less
than half of *m*.  This does restrict us to half of the search space
of relatively prime numerators and denominators, which may force
additional compromises.  Consider again the problem of detecting
880 Hz in a 2526.3-Hz sample stream.  Some quick random search finds
7/20 gives us 884.21 Hz and 23/66 gives us 880.68.  If 884.21 Hz is
acceptable, we could use a 10-sample delay line and these
coefficients:

- cos: [ 1.    -0.588 -0.309  0.951 -0.809 -0.     0.809 -0.951  0.309  0.588]
- sin: [ 0.     0.809 -0.951  0.309  0.588 -1.     0.588  0.309 -0.951  0.809]

Thus, instead of the 11 multiply-accumulates we’d need for each of
these components with the 8/23 approximation (or 22
multiply-accumulates without taking advantage of the repeated
coefficients), we can get by with only 4 each using this 7/20
approximation, a negative-feedback comb, and the coefficient symmetry.

So far I haven’t tried also tweaking the decimation rate, which seems
likely to help with this kind of thing.

Precise frequency measurement with phase unwrapping
---------------------------------------------------

Supposing we detect a tone with one of the techniques above, we now
have I and Q channels for it.  We know it’s somewhere within our
filter passband, which may be fairly wide depending on the Q of the
filter.  If we’re in a reasonably quiet place relative to that Q, we
might be able to assume that that single tone is most of the energy in
the passband.  In that case, we can do something like a phase vocoder:
use atan2(I, Q) to track the *phase* of that tone in successive
windows, relative to the center frequency of our filter; this requires
that the phase not drift by more than τ/2 radians between successive
windows so we can tell which way it’s going, and probably τ/3.

If you are using 33-ms windows, 30 of them per second, this means that
the frequency difference can be at most ±10 Hz, or in theory up to
±15 Hz.  If you were tracking a tone around 1 kHz these windows might
have a Q of 30 or so and therefore be able to detect tones around
±15 Hz.  By shortening the windows we drop Q and also make it
meaningful to run proportionally more of them per second, which would
enable you to track proportionally faster phase shifting and thus
proportionally greater difference in frequency.  So things kind of
work out as you’d hope.

Prefix sum algorithms for parallelism
-------------------------------------

Consider again our example of first downsampling 19× with a
third-order Hogenauer filter, accumulating in a 41-sample resonator
via (from one point of view) 41 third-order Hogenauer filters running
on 41 decimated versions which downsample a bit more, and then
computing the 17th harmonic of this resulting signal, and suppose that
we are doing this in hardware rather than software, but at a sample
rate which is challenging for the hardware, even at the 19×-decimated
rate.  How the heck can we run the initial pipeline of three
integrators at the full data rate?

An integrator computes the prefix sum of its input signal, and there
is a well-known general parallel algorithm for parallel prefix sum
which applies to any monoid.  In fact, carry-lookahead adders are
already an application of this algorithm!

Suppose you want to do this, and you have an unlimited amount of
addition hardware, but that addition hardware takes 20× as long as a
sample time to do an addition.  Aside from speeding up your adders
(for example, using carry-save addition if you aren't already), you
can use parallel prefix sum in the following way.

You begin by feeding 16 front-end adders a superchunk of 32 samples;
each one sums two samples and passes them, plus the sum, to the next
stage in the pipeline.  There, while the front-end adders are
processing the next superchunk, 8 second-layer adders sum pairs of
sums to compute sums of non-overlapping exhaustive groups of 4
samples, which are then passed along to a third layer, each
accompanied with the original 4 samples and the 2 sums of 2 samples.
In the third layer, 4 third-layer adders compute sums of 8 samples; in
the fourth layer, we get 2 sums of 16 samples, and in a fifth level we
get the sum of all 32 samples.

As the seventh stage of the pipeline, this sum is added to a prefix
sum P(i-32), stored in a register, of all the samples up to the
previous superchunk, which value P(i) is used to update that register.
But we aren't done yet!

Now we know the prefix sum before the beginning of our superchunk, and
at its end, but not yet anywhere between.  So our eighth stage adds
the previous prefix sum P(i-32) to the sum of the leftmost 16 samples
to get the prefix sum P(i-16) in the middle of the superchunk.  And
the ninth stage adds P(i-32) and P(i-16) to two sums of 8 samples to
derive P(i-24) and P(i-8).  The tenth stage adds four sums of 4
samples, the 11th stage adds 8 sums of 2 samples, and the 12th stage
adds the 16 odd-numbered samples, and now we have computed the prefix
sum for the whole 32-sample superblock.

This took 12 clock cycles of latency and 63 adders of whatever the
right width is (say 25 bits if the input data is 10 bits, so 3150
half-adders), but it produces 32 samples of prefix-sum signal from 32
samples of input signal every clock cycle.  It can be scaled to
arbitrarily wide degrees of parallelism at only a linearithmic area
cost O(N lg N); 12 is 2 + 2 lg(32).  It can process 1.6Gsps on a 50MHz
FPGA if you can somehow get the samples onto the chip.  And if that
32× throughput speedup isn't enough, you can make it wider.

Increasing the number of integrators increases the latency and area
linearly but doesn't decrease the throughput.  And at the output of
this you can demux one or two downsampled samples.

Without more changes, more integrator/comb filters will compute
results with more bits, but I think it's harmless to just discard
those low-order noise bits at some point.

Multistage downsampling probably isn’t useful
---------------------------------------------

In many situations, the initial step of running three levels of
integrators at the full input sample rate accounts for almost all of
the computational effort of this algorithm.  The reason for the
integrators is to filter out high-frequency signals that would
otherwise alias down into low frequencies after decimation.  I was
thinking it might be reasonable to use two or more intermediate sample
rates in order to reduce this effort; the third-order filter described
above attenuates at 18 dB per octave after a cutoff frequency just a
bit below the Nyquist frequency, while a second-order filter would
only attenuate at 12 dB per octave and have a cutoff frequency a bit
higher, and maybe that would be enough to reduce the data
significantly.

But in practice I don’t think it’ll work out to be a good tradeoff.
For example, you could start with two integrators, a decimation by 2
or 3, and two differencers to reduce the sample rate by a factor of 2
or 3, and then do a second decimation by some other factor like 8,
using for example a third-order Hogenauer filter.  So you save, say, a
third of the initial per-sample additions: 96000 additions per second
instead of 144000.  But because you’re not decimating by very much,
your cutoff frequency is higher (24 kHz or 16 kHz) and so the
interference that could alias is closer to it.  And once that
interference is aliased down into the baseband, you can’t remove it in
a later downsampling operation.  And those later downsampling
operations can’t skimp on the order; they still have to be whatever
order they would have had to be, they’re just operating on half or a
third as much data.  So maybe you’re doing a third-order downsampling
filter on a third as much data, and that eats all your possible gains
from this approach.

So I think multistage downsampling makes sense with FIR downsampling
filters but probably not with Hogenauer filters.

On the other hand, you could hook up a cascade of multibit carry-save
adders to the output of your ADC or whatever is generating the data,
and those carry-save adders can run just about as fast as you like;
their path length is quite short.  (Maybe if you don’t need the high
frequencies, you could run your ADC slower, but maybe you don't have
that luxury.)
