I was thinking about regenerators, catalytic converters, and air
purification.  The ISS uses a platinum catalytic converter to burn up
methane; cars use them to burn up unburned hydrocarbons and also to
reduce nitrogen from nitrogen oxides.

What if you just use ordinary steel wool?  It will burn to iron oxide;
will the iron oxide catalyze the combustion of hydrocarbons?

This also seems like a simple, inexpensive way to get a fast
regenerator that is good up to about 1500° ([Fe₂O₃ melts at
1539°][0]).  Steel wool is relatively inexpensive and widely available
with millimeter-scale porosity, and [the thermal diffusivity of air is
about 19mm²/s][1].  By making your regenerator from steel wool instead
of something like sapphire, you avoid the difficulty of shaping the
regenerator into a high-porosity form with small pores but low air
resistance.  By fluffing it up before you oxidize it, you can increase
the pore size further.

[0]: https://en.wikipedia.org/wiki/Iron(III)_oxide
[1]: https://en.wikipedia.org/wiki/Thermal_diffusivity

On these criteria, something like excelsior might make an equally good
and cheaper regenerator when high-temperature performance and
catalysis is not required.
