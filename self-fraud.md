A popular Everly Brothers song from 01958 said:

> When I feel blue in the night  
> And I need you to hold me tight  
> Whenever I want you, all I have to do is  
> Dream
>
> I can make you mine, taste your lips of wine  
> Anytime night or day  
> Only trouble is, gee whiz  
> I'm dreamin' my life away

The narrator is living in a fantasy world: rather than enjoying the
embrace of their limerent object, who is apparently unattainable for
whatever reason, they pass their time fantasizing about doing so.

theater

D&D

football

porn

religion

corporations

