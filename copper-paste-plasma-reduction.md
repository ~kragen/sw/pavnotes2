Suppose you make a paste of copper particles of under 100μm in an
aqueous polyvinyl alcohol gel (maybe 5% is enough to get a good gel?)
made with a near-saturated solution of copper chloride.  Perhaps the
filler loading could be around 60%.  This paste should be quite
plastic, and a bit of water evaporation should solidify it by
precipitating copper chloride crystals between the copper grains.
This green body is a fairly weak and water-soluble concrete still, but
I think you can reduce it to almost entirely metallic copper with a
cold hydrogen plasma; the excited and dissociated atoms or ions of
atomic hydrogen will easily overcome the activation barrier to react
with the chloride ions, carrying them away (at room temperature or
above) as HCl gas.  This can be bubbled through water to remove it
from the gas and drive the reaction forward, as well as providing
freshly cooled gas to cool the plasma with.

Until solidified by evaporation, the organic binder should enable it
to hold its shape.

In this way, an entirely room-temperature process enables you to mold
almost pure copper articles from a plastic paste, as long as their
walls are thin enough for gas diffusion.  Being entirely room
temperature is important for carrying out the process at submillimeter
scales on Earth.

The net reaction is very simple:

> H₂(g) + CuCl₂(s) → Cu(s) + 2HCl(g)

I don’t have the enthalpy of formation of CuCl₂ handy here to see
whether this is endothermic or exothermic, but I suspect it’s
exothermic.  Although absorbing the HCl in water will help to keep it
from going backwards, if the reaction is endothermic, the equilibrium
might be very unfavorable at STP.  Heating it up (impossible at the
submillimeter scale except transiently) and/or reducing the pressure
(easy at the submillimeter scale) will solve this problem.

Other candidate absorbers for HCl gas might include solid oxides and
hydroxides of metals, especially of alkali metals, of alkaline earth
metals, and of aluminum; and a solution of nitrate of either silver,
lead, or mercury, though that’s probably too expensive to be practical
at scale.  Or just a cold trap, since it boils at -85°.

The same process should be applicable to most metals, including gold
(with chloroauric acid), iron, magnesium, nickel, chromium, lead,
cobalt, and, most excitingly, aluminum.  (The chlorides of titanium
and zirconium are unfortunately not suitable.)  By using a mixture of
salts of different metals, it should be able to produce alloys that
could not be formed from a melt, and it won’t have the dendritic
artifacts that arise from the gradual crystallization of the melt,
because, as with bloomery iron, the metal is never molten.

The metal filler particles are included in the recipe to reduce the
amount of time and hydrogen needed and to ameliorate the dimensional
shrinkage and consequent stresses induced by the removal of the
chlorine; and polyvinyl alcohol is used as the ionomer thickener
because I don’t think polyvalent cations like copper will precipitate
it; it’s an ionomer, but its functional groups are all alcohol
hydroxyls, so they attract anions like borate.  Eventually the
hydrogen bombardment will probably convert it to polyethylene, which
will remain in the metal as an impurity.

If hydrogen and time are abundant, and the dimensional shrinkage is
manageable, it might be reasonable to use an insoluble copper salt
instead of metallic copper.  For example, chalcopyrite, chalcocite,
covellite, bornite, carbonates of copper, copper iodide, tenorite,
cuprite, either of the hydroxides of copper; the non-copper products
of reacting any of these with hydrogen are relatively stable and
fairly volatile, and some of them are less reactive than HCl.  I don’t
think copper phosphates and silicates such as turquoise,
pseudomalachite, and dioptase, or copper phosphide, are likely to work
with hydrogen, because I don’t think the silane and phosphine products
are sufficiently stable to escape the workpiece.  SF₆ in place of
hydrogen might work to remove those anions (leaving behind CuF₂, which
is almost water-insoluble and doesn’t melt until 836°) followed by a
second plasma treatment to remove the fluorine.

Of course, the same remark about alternative insoluble solid fillers
that can be reduced to metal applies to the other metals, too.

Soluble copper salts that might work as alternatives to CuCl₂
(solubility 73g/100mℓ) include CuNO₃ (125g/100mℓ), CuBr₂ (126g/100mℓ),
Cu(ClO₃)₂ (242g/100mℓ), Cu(ClO₄)₂ (146g/100mℓ at 30°) and maybe even
CuSO₄ (32g/100mℓ) or CuSiF₆ (81.6g/100mℓ).  However, all of these are
much bulkier.  Most metal powders should generally not be mixed with
chlorates, perchlorates, or nitrates, especially around plasma, but I
think copper might be less of a problem that way.

Of course, alternative soluble salts of other metals can also be used.

An alternative way to produce excited and dissociated hydrogen is with
intense ultraviolet light, but I think the usual corona-discharge and
avalanche-discharge approaches are a lot more practical.

Exploring the aluminum system in particular, starting with hydrated
aluminum chloride, this is the reaction we want:

> 3H₂(g) + 2Al(H₂O)₆Cl₃(s) → 2Al(s) + 6HCl(g) + 6H₂O(l)

There’s an uncomfortable question of whether you’ll end up with
aluminum oxide or hydroxide instead, as you do if you just heat the
chloride.  The other aluminum salts with substantial aqueous
solubilities are the sulfate, the acetate, the perchlorate, and the
nitrate; all of these contain oxygen, and it seems imprudent to mix
aluminum powder with any of them except the sulfate or acetate.

Unfortunately, this reaction looks unlikely:

> Al2(SO4)3 + 15H2 -> 2Al + 3H2S + 12H2O

The heat of formation of the sulfate on the left is -3440kJ/mol.  That
of H2S is -21kJ/mol, and water’s is -285.83kJ/mol.  This works out to
-3492kJ/mol, so this is very slightly exothermic.  Consider this
competing reaction:

> Al2(SO4)3 + 12H2 -> Al2O3 + 3H2S + 9H2O

We’ve lost three waters, +857 kJ/mol, but gained an alumina,
-1675.7kJ/mol, taking the reaction from slightly exothermic to
enthusiastically exothermic.

So maybe hydrogen plasma reduction of soluble salts isn’t actually a
way to get aluminum metal.  Too bad.  How about iron?

> 2FeCl₃ + 3H₂ -> 2Fe + 3HCl

The standard heat of formation of the HCl is -92.31kJ/mol, but, as
with copper, unfortunately I don’t have that of FeCl₃ handy.  I feel
like they’re both pretty thoroughly ionic, so they ought to be
computable pretty closely from a component from each ion.

Hmm, [the enthalpy of formation of FeCl₃ is -399.5kJ/mol][0], so this
is an extremely endothermic reaction: from -799kJ to -277kJ.  That’s
just not going to happen at any significant speed.

[0]: https://chem.libretexts.org/Bookshelves/General_Chemistry/Chemistry_-_Atoms_First_1e_(OpenSTAX)/09%3A_Thermochemistry/9.3%3A_Enthalpy#Hess.E2.80.99s_Law

How about copper chloride?  [-205.85kJ/mol][1], though several other
sources say -220.1.  This is a more plausible case: from -205.85kJ to
-184.62kJ, still endothermic, but only by 21kJ (or 36kJ if the other
sources ar right).  I need to learn to calculate the equilibrium but I
think this’ll work.

[1]: https://webbook.nist.gov/cgi/cbook.cgi?ID=C7447394&Units=SI&Mask=2#Thermo-Condensed

What happens if we use methane or ammonia instead of hydrogen as our
gas?  Ionized ammonia ought to react with the CuCl₂ or FeCl₃ to give
us hydrazine, normally considered a strong reducing agent, and solid
ammonium chloride.  Methane ought to give us ethane and HCl gas.  In
both cases, the number of gas molecules increases rather than staying
the same as it does with hydrogen, which I think entropically favors
the desired reaction, especially if we drop the pressure.

With these alternative gases, you’re likely to get carbides or
nitrides of the metal rather than just pure metal.  This might be
better or worse.
