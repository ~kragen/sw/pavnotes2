Tim Bray says, “When the going gets tough, the tough use printf.”  But
while debug logging is great, printf does have some real disadvantages.

Suppose you have a custom log viewer?  It occurred to me that for
debug printfs, it would be useful to include the filename and line
number, and be able to filter your printfs that way.  In some
circumstances it would also be useful to record some part of the call
chain.  Efficiency is always somewhat of a constraint here; [as Yosef
Kreinin points out][0], you can improve that situation dramatically by
delaying things like decimal formatting until visualization of the log
messages, so printf itself only has to append a record of its
arguments to a log buffer.  You can even compile the format strings
out of your firmware.

[0]: https://yosefk.com/blog/delayed-printf-for-real-time-logging.html

This approach potentially also permits the log viewer to do things
like display the log messages from a particular callsite (or perhaps
format string) as a table and do table-like things with them, such as
filter the rows by field comparisons, plot a column, average a column,
count the total number of distinct values of a column, etc.

Call chain recording
--------------------

The C-macro approach to figuring out where your debug-print function
is called from is to [use `__FILE__` and `__LINE__` in a macro][1],
something like this:

    #define dprint(...) dprint_func(__FILE__, __LINE__, __VA_ARGS__)

[1]: https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html

This works, but it’s unnecessarily inefficient (passing two additional
arguments and possibly adding a new string constant to the string
constant pool), and it’s limited to recording a single level of the
call chain.  This is very useful, but we can do much better.

For recording a compile-time-chosen amount of the call chain, I feel
like you generally only need a couple of instructions per level — less
than it takes to pass these extra arguments.  If your stack frames use
a standard format, this could be baked into the debug-printing routine
itself.  With GCC -fno-omit-frame-pointer on amd64, this is %rbp, with
this standard prologue and epilogue:

    push   %rbp
    mov    %rsp,%rbp
    ...
    pop    %rbp
    ret

So %rbp points to the caller’s saved %rbp, and the word above that is
the return address.  You can presumably get your own calling PC/%rip,
which can be mapped back to a file and line, with a single
instruction:

    mov 8(%rbp), %rdi     ; get return address

If you don’t make the debug print routine *itself* save its caller’s
frame pointer with the above prologue, this will give you its caller’s
caller instead, and getting its own caller will instead look something
like this:

    mov 24(%rsp), %rdi     ; offset depends on stack frame size

So you get two levels of stack trace into registers with two
instructions, but each additional level costs two more instructions:

    mov (%rbp), %rsi       ; get caller’s frame pointer
    mov 8(%rsi), %rdi      ; and corresponding return address
    ;; (do something with it here)
    mov (%rsi), %rsi       ; follow the chain up one more level
    mov 8(%rsi), %rdi      ; get corresponding return address
    ;; (do something with it here)

These are still mostly two instructions even on RISC platforms, but if
you have a suitable double-word load instruction like ARM’s LDRD or
the two-register case of LDMIA, you may be able to reduce it to a
single instruction.  RISC platforms also generally eliminate the need
for an instruction to get the return address into a register; it’s
already there.  So amd64 is sort of the worst case, actually.

With slight changes, this works in interrupt handlers or Unix signal
handlers, too.

However, all of this depends on having a guaranteed frame pointer
(costing you a temporary register, not too bad on ARM or amd64, but
rather painful on i386), as well as a sufficiently long call chain.
(The last can be guaranteed by fiddling with the program startup code
to add a self-referential dummy call frame.)  If you encounter someone
using %rbp for something else as you walk the call chain, you will
crash, which is a problem if you have to support callbacks from code
that uses a different ABI.

The debug-print subroutine itself can count on knowing its callsite,
regardless of how that callsite is compiled.  If only code you compile
can call your debug-print subroutine, you can compile that code with a
guaranteed frame pointer.  This is sufficient to provide two levels of
stack, without putting any constraints on the ABI of other code in the
call stack, which is already quite useful.

Forgetful traces for eager discard
----------------------------------

Generally, when debug logs get long, we throw away older log messages.
This has the unfortunate result that a single overly-voluminous debug
print can flush far more useful information.  If the debug-print
routine maintains a separate FIFO buffer for each callsite, this
problem greatly diminishes.  This requires only a few instructions to
do a hash table lookup, but probably reduces the available logging
space a lot, since most of the logging space will be allocated to FIFO
buffers that are almost empty.  You could have a separate FIFO for
each hash bucket instead of each callsite, which is even faster and
would reduce that problem.  A slight variant of this approach is
probably better: hash each callsite to *two* hash buckets, and if the
first one’s FIFO buffer is full, write it to the second one.
