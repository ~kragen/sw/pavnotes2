It occurred to me that electroplating nickel onto aluminum foil has
some interesting potential:

1. If you ignite the composite, it exothermically (SHS) converts to
    refractory but brittle [nickel aluminides][0] (Ni₃Al melts at
    1395°, NiAl at 1722°).  The heat released makes it incandescent,
    usually temporarily.
2. Because aluminum is amphoteric, it can be removed as a sacrificial
   layer with ordinary lye, or probably through anodic dissolution in
   a solution of sulfate, nitrate, chloride, or acetate, either with
   voltage carefully controlled to avoid dissolving the nickel, or
   relying on the nickel itself to be the cathode.  This permits
   forming a small part out of ordinary 10-μm-thick aluminum foil,
   then converting it into ½-μm-thick nickel foil, though if you want
   to get a free-standing structure in air you’ll probably have to use
   supercritical drying to remove the solvent that carried the lye.
3. Origami makes it possible to form interesting shapes out of
   aluminum foil.  Stamping and various single-point sheet-cutting
   processes are another promising approach.

[0]: https://en.wikipedia.org/wiki/Nickel_aluminide

If you use titanium foil instead of aluminum, you can selectively etch
away the titanium with chlorine gas, ionized or not, thus avoiding the
annoying supercritical drying step.

Similar dissolving-scaffolding approaches are possible for many other
metal combinations:

- Anything plated with gold, because just about nothing dissolves
  gold.
- Aluminum plated with any metal stable in alkali, such as copper or
  iron, using the alkali to dissolve the aluminum.
- Iron plated with any metal stable in alum, such as copper, brass,
  [aluminum][1], [silver, and stainless steel][2], and thus probably
  nickel as well.
- Copper plated with any metal that’s stable in ammonia.

[1]: https://www.snaderflyby.com/dissolving-steel-out-of-aluminum-with-alum-powder/
[2]: https://en.wikipedia.org/wiki/Alum#Uses
