If you pull a strip of paper at right angles to a metal bar to which
it is clamped by seven small spring-loaded electrodes arranged along
the paper’s width, high voltages applied to the electrodes can print
holes in the paper by provoking electric avalanche breakdown in the
paper dielectric, burning it.  Seven pixels is enough height to print
legible letters in a variety of typefaces, as the Hellschreiber did.

If the paper is wrapped around the metal bar a bit, it is possible to
use an additional electrode a bit ahead or behind the column of seven
to get timing information; if it is behind, the timing electrode can
write timing marks that one of the column of seven can sense, and if
it is ahead, the timing electrode can sense timing marks placed by one
of the column of seven.  This would enable the entire apparatus to
work without any motors, solenoids, or other mechanical actuators.
However, optical sensing would be more reliable.

The [dielectric strength of paper is about 16 MV/m][0], according to
the work of Vashti Prasad, and typical paper is about 100 μm thick,
which is thus about 1600 V.  But you also need enough energy to burn
it.  Suppose the spark is about 100 μm wide; that’s on the order of
10⁻¹² m³, about a microgram if it’s about the density of water, and so
about 10 μJ/K if it’s about 1 J/g/K.  I’m not sure how hot you have to
heat the paper to mark it visibly; probably between 200° and 1200°.
Supposing it’s 700°, that’s 700 μJ.  You can release this over a short
enough period of time that heat transfer to the environment is
insignificant compared to the heating rate.  [The thermal diffusivity
of wood][2] is about 0.1 mm²/s, which to me suggests that the relevant
time scale for diffusion over 100 μm is on the order of 100
milliseconds, so anything under about 10 milliseconds would be fine.

[0]: https://hypertextbook.com/facts/2007/VashtiPrasad.shtml
[2]: https://en.wikipedia.org/wiki/Thermal_diffusivity

You might want to yank the paper out at 1 m/s, though, for which
purpose you’d want to print 5000 pixels per second, so something like
10 μs would be better.  Typically electric sparks like this ramp up
over the course of a few nanoseconds, so that’s probably reasonably
easy.

A millijoule over 20 μs is 50 watts (per pixel!), which is a somewhat
menacing power level; it might be better to shoot for gently pulling
the paper out at 10 mm/s, increasing the time interval to 2 ms and
reducing the maximum power to 0.5 watts, 3.5 watts across all 7
pixels.

I think that to build the driver circuit you want to build up the
spark energy in a storage medium over time and then release it all at
once, because a millijoule over a nanosecond is a megawatt.  You can
use an inductor or a capacitor.  Suppose you use a capacitor; you want
it to hold a few millijoules, like 10 or 20, at 2000 V, which suggests
about 0.005–0.01 μF of capacitance.

[Digi-Key’s #4 most in-stock ceramic capacitor is Murata’s
GJM1555C1H120FB01D][1], which is rated for 25 volts.  But, uh, that’s
a C0G/NP0 precision type, with only 12 pF.  [#5 is Yageo’s 0402-sized
CC0402KRX7R7BB104][3], which is 16 volts, 1 mm × 0.5 mm, 0.6¢ in
quantity 2500, and 0.1 μF (none of this nanofarads rubbish here!).  A
series string of 125 of these would be rated for 2000 volts, but also
only 800 pF, so you’d actually want something like ten such strings in
parallel to get the desired energy storage.  1250 of them would run
you US$8.

[1]: https://www.digikey.com/en/products/detail/murata-electronics/GJM1555C1H120FB01D/2592850
[3]: https://www.digikey.com/en/products/detail/yageo/CC0402KRX7R7BB104/2103076

Electrolytics don’t simplify this, because they’re all too big, most
over 1 μF, but you can get higher-voltage ceramics.  For example, the
[Samsung CL31B104KEHSFNE][4], which is 23¢ in quantity 1, 0.1 μF,
250V, X7R, and size 1206.  Ten of these in series get you 2500V and
0.01 μF for US$1.55.  (In quantity 100 they’re 7.84¢ each.)

[4]: https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL31B104KEHSFNE/3888741

(It occurs to me that for most electronics prototyping purposes, it
might be sensible to stick to high-voltage ceramics; I think they
don’t have much higher ESL and ESR than lower-voltage versions with
the same capacitance, they just take up more space and cost more
money.  But, at a given voltage, I think they’ll have less acoustic
noise, less leakage, and less chance of failure.)

I’m not sure what the best way to switch 2000 volts under
microcontroller control would be; I suspect it’s a power MOSFET,
though maybe a power triac or BJT would work.  I understand triacs
have a hard time with rapid current rises, though.

The *average* current involved is only 250 μA (per pixel!), but if
it’s all concentrated in a nanosecond every 2 ms, that’s a 500 A
pulse, or 3500 A if all seven pixels fire at once.  It might be best
to do most of the switching with the spark itself, letting the
capacitors discharge through the spark channel once it’s been
triggered by the semiconductors.  Then the semiconductors don’t have
to handle hundreds of amps.  (You might be able to get away with
pretty high pulses at the nanosecond level, though.)

In that context, maybe the best approach is to use seven tiny
inductors each grounded through a beefy BJT in parallel with the
paper, which BJT is then suddenly slammed off to initiate the
high-voltage pulse.  I don’t know, how do external-trigger flashtube
circuits work?  I guess they use a trigger transformer to step up a
trigger pulse to a high voltage.  [Digi-Key only has ridiculously
expensive trigger transformers][5] but I think cheap ones are commonly
found in consumer products like ion generators and disposable cameras.
[AliExpress has a lot of 5 for US$8][6], stepping up 200–600 volts to
8000–10000 (something is clearly wrong here); [YouTubers like
bigclivedotcom have dissected them][7], finding turns ratios like
13:495 and 20.5:493.

[5]: https://www.digikey.com/en/products/detail/excelitas-technologies/202-0180B/5885808
[7]: https://www.youtube.com/watch?v=OdLO4HhcDXY
[6]: https://es.aliexpress.com/item/1005003910915021.html

With this design, only the trigger-transformer output need leap into
the kilovolts; the capacitor bank that provides the energy to burn the
paper can be running at a more convenient 50–300 volts.  Normally in a
flashtube circuit you either have to dump the main current pulse
through the trigger transformer (requiring a huge trigger transformer)
or not have a wiring path between the two circuits, but I suspect that
in this case a little bit of inductance between the capacitor bank and
the spark gap would solve the problem (of how to keep the capacitor
bank from eating the trigger pulse), because it would be fine for the
capacitor discharge to happen over a much longer period of time than
the trigger pulse.

The trigger pulse might happen in a nanosecond, for example, while the
capacitor discharge can take as long as 10 μs.  If we want the LC
resonant frequency to be something much slower than a nanosecond, say
10 MHz, it’s convenient to use a higher capacitance (and lower
voltage).  Supposing our cap bank consists of ten CL31B104KEHSFNE caps
in *parallel* charged up to 250V, the total capacitance is 1 μF.  To
get 10 MHz we need 250 picohenries, which is [the self-inductance of
500 μm of 100-μm-diameter wire, or 1 mm of 1-mm-diameter wire][8], and
therefore probably unachievably low; 100 kHz would be 2.5
microhenries.  So probably inductance anywhere in that range would
work.  But, uh, we probably need a lot more inductance in the path to
the capacitors than in the path through the trigger transformer and
through the paper.

[8]: https://www.eeweb.com/tools/wire-self-inductance-calculator/

What do you make the electrodes out of?  I was thinking copper, Dan
Moniz suggests diamond record-player styluses.  Greg Sittler suggests
tungsten.
