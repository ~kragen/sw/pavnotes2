Kip Ingram explained his idea for high-performance compact interpreted
code.  Though I haven’t tested it, I think it might provide a
significant compactness advantage (≈15%) over the best such approaches
known.  This is particularly interesting to me for my Zorzpad project,
in which I’m using an Ambiq Apollo3 microcontroller, which has 96 MIPS
but only 384KiB of RAM.  On such a platform,
saving RAM is crucial, and it might be
worth sacrificing a lot of speed for it.

Ingram’s bytecode approach
--------------------------

Ingram explained to me that he is working on a bytecode Forth
interpreter in which *next* (the code fragment to fetch and execute
the next bytecode, basically the bytecode interpreter) is this 10-byte
sequence:

    mov bl, al
    sar rax, 8
    jmp [<table_reg>+8*rbx]

That is, it shifts 8 bits off the low end of the 64-bit `rax` register
(this being AMD64) and uses it to index into a 256-entry jump table.
So it’s “fetching” the next bytecode from the bits already stored in
`rax`.  But obviously you can’t store your entire program in
`rax` — but you have a kind of built-in underflow exception, as Ingram
explained to me:

> A cell† full of opcodes will have either 0 or 1 as its MSB, so
> eventually you will shift 0x00 or 0xFF into the low byte.  Those two
> opcodes will simply run `lodsq` to load up the next cell (so `rsi`
> is the instruction pointer) and then do *next*.  (...) [W]e only
> have to `lodsq` every eight ops.  And I don't have to code that
> in — it just gets “auto-inserted” naturally.

(If you used `shr` instead of `sar` it would always be 0x00.)

Ingram’s original expression of the idea said `jmp
[<table_reg>+4*rbx]`, using a 1024-byte table of 32-bit pointers, but
for this to work properly, you need to say `dword ptr`, which adds an
0x66 prefix byte, making the sequence 11 bytes rather than 10.

Using `lodsq` probably makes no real difference to efficiency, though
it’s 2 bytes rather than the 7 bytes of something like `mov rax,
[rcx]; add rcx, 8`.  But that’s just a one-time 5-byte saving in the
instruction-cell-underflow-handling subroutine, not a repeated 5-byte
saving.

Anyway, you only need to do a memory access to fetch the next 64-bit cell
full of bytecode every 8 bytecode instructions (although you do need
to do a memory access to the jump table every bytecode instruction).
If you required call instructions to fill up the remainder of the
cell, you could implement a standards-compliant Forth this way, but
Ingram’s plan is instead to allow you to put more bytecode after them:

> I am going to have the ability to return back into the middle of a
> cell.  So a call might be just one byte somewhere mid-cell; it [will
> still] work.  That does involve storing not only the IP [Forth
> instruction pointer register, `rsi` in this case] but also the
> instruction word (partially shifted — `rax` here) on the return
> stack on calls.  But I figure since they will likely go in the same
> cache line that will be very fast.
>
> It will make the return stack larger, though.

I believe that storing two cells rather than one on the return stack
makes this incompatible with Forth standards, but relatively little
Forth code depends on the number of items pushed onto the return stack
by a call, so porting standard Forth code to such an implementation
would generally require minimal changes.  (See file
`forth-dynamic-scoping.md` for an example of Forth code that does
depend on it.)

This idea of storing a number of small instructions in a register and
shifting them one by one has been used in computers such as the CDC
6600, the MuP21, and its descendant the F18A, and Ingram said:

> Yes, the F18A was the origin of the idea.

Forth systems generally have fairly low interpretive overhead,
typically on the order of a factor of 5 or 10.  This is despite their
virtual machine being fairly low level and a stack machine, which, as
I’ve explained in some of the other notes here, needs to execute about
twice as many virtual-machine instructions as an otherwise comparable
register virtual machine to do any particular job.  This is achieved
in large part by cutting the work done by the virtual-machine’s
interpretation loop to an absolute minimum; the version from the
popular Forth implementation F83 is this three-instruction sequence:

    LABEL >NEXT   AX LODS   AX W MOV   0 [W] JMP

This is in Forth-assembler syntax.  W is an alias for the BX register.
In conventional assembly syntax, that would be:

    tonext:
        lodsw ax
        mov bx, ax
        jmp [bx]

In many systems, this sequence is copied onto the end of the
implementation for each primitive Forth word (subroutine) rather than
having a central interpreter that they all jump to, which is done
because it saves a jump and thus provides a non-negligible speedup.
(On systems with branch target buffers for indirect branch prediction,
this also helps the branch predictor.)

So, in general, one would expect a significant slowdown from a
“bytecode” system, in which an extra level of indirection is added to
this inner loop.  Typically in Forths it’s called “token threading”
and user-level subroutines are added to the same jump table; it’s an
approach used on especially memory-constrained systems.  Ingram said
to me:

> I used to think token threading would be slower than I cared for,
> but when I hit upon this particular thing we’ve talked about above,
> with only the occasional need to fetch instruction cells, I started
> to think that maybe it could be in the same performance ballpark as
> my previous systems.

I think he’s probably right.

Of course it isn’t necessary for the bytecode instruction size to
divide the cell size evenly; leftover bits at the end of the cell are
only a slight waste but do not cause any incorrect behavior.

-----

† Forth uses the term “cell” for a register-sized unit of RAM, what is
normally called a “word”, a term that has a conflicting definition in
Forth, where it is a subroutine.  It also has a conflicting definition
on AMD64, where a “word” is conventionally 16 bits rather than 64 for
hysterical raisins.

Variable-bit-length bytecodes
-----------------------------

It occurs to me that some Forth instructions are used often enough to
justify assigning them more than 1/256 of the code space.  For
example, in [my minimal Forth
roguelike](http://canonical.org/~kragen/sw/dev3/wmaze.fs), in 51
non-comment nonblank lines of Forth, I used *;* 47 times, *dup* 32
times, *+* 13 times, *drop* 11 times, *to* 11 times, *if* 11 times
(each accompanied by a *then*), and *loop* 9 times (each accompanied
by a *?do*.)  Taking a rough estimate of 12 “instructions” per line
(counting a call to a subroutine as one instruction), this is about
600 instructions, so, roughly:

- one out of every 13 is a *;*;
- one out of every 19 is a *dup*;
- one out of every 47 is a *+*;
- one out of every 56 is a *drop*;
- the same for *to* and *if*.
- one out of every 68 is a *loop*, and the same for *?do*.

So it would be profitable to allocate 1/16 of the opcode space to
*dup* and 1/64 of it to each of *+*, *drop*, *to*, *if*, *?do*, and
*loop*, if we could in return enable *dup* to only consume 4 bits of
our opcode cell and the others to only consume 6 rather than the usual
8.

Suppose our standard *next* is instead this three-instruction
sequence:

    shr rax, 8
    mov bl, al
    jmp [rbp + rbx*8]

Then we could reasonably implement *dup* with a modified version that
only shifts off 4 bits:

    shr rax, 4
    mov bl, al
    jmp [rbp + rbx*8]

Then we just need to put 16 pointers to *dup* into the jump table
instead of just one, for example at indices 0x06, 0x16, 0x26, 0x36,
... 0xe6, 0xf6.

Words like *+* that we want to give 6-bit opcodes would work
similarly, but they would `shr rax, 6` and get 4 table entries rather
than 16, for example at indices 0x07, 0x47, 0x87, and 0xc7.

The `shr` instruction can be moved earlier in the code, which may help
with instruction scheduling on superscalar processors.

With this approach, we can use variable-bit-length instructions in our
bytecode without any extra runtime cost except for some infinitesimal
increase in the L1D cache miss rate.

Although *;*, returning from a colon definition, was by far the most
common operation in my test code, it’s kind of a special case, because
it is almost always the last bytecode in a cell — when you return, you
pop the register of instructions to decode from the return stack.
(There were also three calls to *exit* in my example code.)  So if we
use `shr` rather than `sar`, guaranteeing that the bits being shifted
in are 0 bits, we could represent *;* or *exit* at the end of a cell
with as little as a single bit if we assign it the opcode 0x01, or for
example two bits with the opcode 0x02 or 0x03.  It can occupy very few
bits of the instruction cell without taking up multiple table entries.

[The Common Case in Forth Programs][2] found that typically half of
the static instruction mix in Forth programs consisted of the four
instructions call, literal (constant), memory fetch, and return;
*dup*, *?branch*, *+*, *!*, and *c!* were also common, each typically
comprising about 1/32 of the static instructions, roughly followed by,
in no particular order, *execute*, *swap*, *>r*, *r>*, *branch*,
*drop*, *i*, *over*, *useraddr* (?), *0=*, *2dup*, *2@*, *-*, *+!*,
*=*, *and*, *cells*, and *or*, where the last ones were typically
about 1/127.  More interestingly, the top 20 instructions comprised
87.4%, 85.3%, 85.2%, 84.9%, 87.8%, and 92.2% of the total instructions
in their six benchmarks, respectively “prims2x”, “gray”, “brew”,
“brainless”, “benchgc”, and “pentomino”.

Here’s some code I quickly hacked together to analyze this in Emacs:

    ;;; Percentage of static instructions attributable to each of the
    ;;; top 20 instructions in each of six benchmarks.  The .01 is
    ;;; presumably an error.
    (setq forth-instruction-mixes
          (list
           (list 17.44 16.10 9.67 9.43 4.80 3.75 3.36 2.14 2.12 2.00
                 1.83 1.80 1.80 1.77 1.36 1.19 1.12 1.09 1.09 1.08)
           (list 23.61 17.61 15.32 7.47 3.50 2.96 2.93 2.76 1.37 1.27
                 1.16 1.09 1.06 1.05 1.03 .01 1.00 .91 .86 .85)
           (list 19 18.28 11.17 8.35 4.33 3.95 2.71 2.04 1.76 1.69 
                 1.58 1.53 1.35 1.28 1.17 1.17 1.14 1.12 .79 .79)
           (list 30.81 9.94 9.58 8.39 6.45 4.63 3.74 3.41 3.37 2.23
                 1.62 1.51 1.28 .97 .89 .74 .74 .74 .65 .49)
           (list 22.43 19.22 10.22 9.01 4.01 2.80 2.63 2.39 1.67 1.63
                 1.57 1.57 1.44 1.29 1.25 1.19 .86 .79 .73 .73)
           (list 21.73 18.21 9.44 9.15 3.64 2.77 2.77 2.39 2.33 1.35
                 1.46 1.43 1.35 1.27 1.19 1.17 1.11 .94 .84 .73)))

    ;;; all give 20:
    (mapcar #'length forth-instruction-mixes)

    (defun lg (x)
      (/ (log x) (log 2)))

    (defun percent (x)
      (/ x 100.0))

    (defun bits (x)
      "Given a symbol probability, returns how many bits per symbol it contributes."
      (* x (- (lg x))))

    (defun sum (xs)
      (if (null xs) 0
        (+ (car xs) (sum (cdr xs)))))

    ;;; How many bits per instruction are accounted for by the top 20?
    ;;; This should give us a lower bound on the number of bits per
    ;;; instruction, but it’s implicitly counting all the instructions
    ;;; not listed as zero bits.
    (mapcar (lambda (mix)
              (sum (mapcar #'bits (mapcar #'percent mix))))
      forth-instruction-mixes)
    ;;; (3.28 3.00 3.15 3.21 3.11 3.10)

    ;;; What percentage of all static instructions are accounted for?
    (mapcar (lambda (mix) (sum mix)) forth-instruction-mixes)
    ;;; (84.94 87.82 85.2 92.18 87.43 85.27)

    ;;; Number of bits to guess each not-listed instruction accounts
    ;;; for.
    (setq misc-instruction-bits 10)

    (defun analyze-instruction-mix (mix)
      "How many bits per instruction are needed for this mix?"
      (+ (* misc-instruction-bits (percent (- 100 (sum mix))))
         (sum (mapcar #'bits (mapcar #'percent mix)))))

    (mapcar #'analyze-instruction-mix forth-instruction-mixes)
    ;;; (4.78 4.22 4.63 3.99 4.37 4.57)

    ;;; But that’s sort of assuming arithmetic encoding.  What if we
    ;;; have to use integer numbers of bits?
    (defun huffbits (x)
      (* x (ceiling (- (lg x)))))

    (huffbits .25) ; 0.5, because 2 bits * 25% chance
    (huffbits .24) ; 0.72, because 3 bits * 24% chance

    (defun analyze-huff-mix (mix)
      (+ (* misc-instruction-bits (percent (- 100 (sum mix))))
         (sum (mapcar #'huffbits (mapcar #'percent mix)))))
    (mapcar #'analyze-huff-mix forth-instruction-mixes)
    ;;; (5.16 4.72 5.10 4.36 4.93 5.12)

This doesn’t provide a lot of actionable guidance in instruction set
design (unless what you want is to run existing Forth programs) but
does make it plausible that you could hit an average of 6 bits per
instruction or less with this approach.  A big caveat is that it
doesn’t count the operand bits, and call, literal, *?branch*, and
*branch* all have operand bits.

[2]: https://www.complang.tuwien.ac.at/anton/euroforth/ef01/gregg01.pdf

Variable-bit-length immediate operands
--------------------------------------

With this possibility of variable bit length in mind, we can very
plausibly implement weird-sized bitfield immediate operands for other
instructions.  For example, *if* does a conditional jump forward; one
version of this would just jump forward some number of bits in the
instruction cell, while another would jump forward some number of
instruction cells.  The former needs only a 6-bit immediate bit count,
telling how many bits to shift, while the latter probably needs
something like a 2–4 bit field telling how many cells to jump ahead.
(A conditional return instruction would be more compact and is
adequate in general.)

The [BASIC Stamp used such a scheme][0] ([archive][1]) for its BASIC
interpreter.  For example, its variable indices were 6 bits.

[0]: http://www.mcmanis.com/chuck/robotics/stamp-decode.html
[1]: https://web.archive.org/web/20240308121146/http://www.mcmanis.com/chuck/robotics/stamp-decode.html

Another use of such variable-length fields is calls to nearby
subroutines.  Ingram told me:

> My “style” in Forth is to write a lot of very short definitions, so
> if I really want to keep things compact I need compact calls.  Most
> of the calls that style creates are calls to places “nearby,” so
> I’ve oriented the design to making *that* compact.  Usually I’m
> calling some factored “helper word” that is just [one] or two or
> three definitions back.

So a 3- to 4-bit immediate operand field might be adequate for such a
local-call instruction.

Many bytecode instruction sets use a 2-to-4-bit index in the
instruction byte for operations like “store local variable” or “load
from constant table”.  Forth doesn’t conventionally have those things,
but this approach is not limited to Forth.  With this design, the
index could be larger than would fit into a byte along with the
opcode.  For example, “load local variable” might reasonably have a
5-bit opcode and a 4-bit immediate operand which indexes the
local-variable vector.

Rarely called subroutines could be put in an auxiliary jump table
instead of blowing a whole 1/256 of the instruction space on them.
One or a few auxiliary-jump-table instructions could fetch an
immediate index in the same way from the instruction register in order
to index into that table, in effect giving you additional opcodes of
9, 10, or more bits, without slowing down the main *next* loop.

One jump table per module
-------------------------

But, in the common case, where you have some subroutines that are
frequently called in one module and rarely or never called outside
them, you could have more than one main jump table.  In my example
code above, `rbp` pointed at the jump table, and we could just point
it at a different jump table when we make an inter-module call, and
restore it afterwards.  Each jump table would need to duplicate the
pointers to the commonly-used Forth words.

This requires call-gate stub subroutines whose job is just to save,
load, and restore the jump-table base register.

Smaller registers
-----------------

On a smaller machine, like one with registers of 32 bits or less, it
would probably be slow to shift a 64-bit quantity by a variable number
of bits.  But, for example, on the AVR, multiple-bit shifts
of quantities over 8 bits require a loop that shifts one bit per
iteration.  Still, with a 32-bit cell size and an average of 7 bits
per instruction, you ought to be able to get an average of 4.5
instructions per cell, about 10% better than the conventional bytecode
approach.

Possibly this is why this approach wasn’t used by Smalltalk on 16-bit
machines in the 01970s and early 01980s.

On ARM Thumb-2, bitfield extraction and update instructions can make
this less onerous; shifting each of two 32-bit registers by six bits
and copying the low six bits of one into the high six bits of the
other is only, I think, three instructions.  Whether this is a
worthwhile tradeoff for less wasted bits I don’t know.

Alignment wastage
-----------------

Long subroutines will on average run out of instructions near the
middle of an instruction cell.  This will waste about 32 bits of space
on a 64-bit machine unless we can start a bytecode subroutine in the
middle of a cell.  Short subroutines will probably average less than
64 bits each and might waste significantly less.

When we do the indirect jump, the table index used for the jump is
left in the W register (`rbx` in the example code above) and, for
colon definitions, can be used to index an auxiliary table of
subroutine start addresses.  This avoids the need to duplicate a
colon-definition header for each bytecode subroutine, as
direct-threaded Forths normally do.

We can interpret these table entries as we wish; in particular we
could start bytecode subroutines in the middle of cells at the expense
of 6 bits of bytecode address space and an additional runtime-variable
bit shift instruction.

On AMD64 and similar architectures where the penalty for unaligned
fetches is small, you could start each subroutine on any byte boundary
rather than a cell-size-aligned cell, reducing the average wasted
space to about 4 bits.

Either of these approaches eliminates the advantage described earlier
for *;* return instructions; if we allocate them a full 8-bit opcode,
they’ll occupy a full 8 bits of space.  So in these cases it might be
a good idea to allocate them a 4-bit opcode, though still an opcode
whose high bits are zeroes for the occasional case where we get lucky
and have it at the end of the cell.
