Normal CPU register files are three- or even four-ported, which makes
them larger.  For example, in [OpenRAM][0] in Skywater SKY130, a
single-port 6T cell is 1.2μm × 1.58μm; dual-port is 2.40μm × 1.58μm,
twice as big.  An interesting alternative might be to divide the
register file into four single-ported banks, so that you have more
on-chip memory in the same area.

[0]: http://ef.content.s3.amazonaws.com/OpenRAM_%20FOSSI%20Dial-Up%202020.pdf 

One way this might look, particularly if you're not concerned with
instruction set backward compatibility, is that each
register-to-register instruction has three 4-bit register operand
fields, two sources and a destination.  Two bits of the register
number select the bank the register is in, and the other two bits
select which of 4 registers in the bank is desired.  The two source
banks must be different from one another and from the destination
field of the previous instruction in the pipeline, or perhaps the
instruction two cycles earlier.

The theory is that with this approach you get 16 single-ported
general-purpose registers into the same silicon area as about 7
three-ported registers.  Occasionally you have to insert a nop to be
able to read a given value, or a mov in order to get a value into a
different bank than another value you want to combine it with, but in
the vast majority of cases these problems can be avoided.

[Torben Ægidius Mogensen said in 02011][1] that "the 2800" (maybe the
Intel iXP 2800 network processor?) did something like this:

> The saving in instruction bits and ports are really separate: You
> can imply register bits even if there is only one physical register
> file and you can split the physical register file even if there is
> only one logical register file. But you get most benefit if you do
> both: You can tie each register file to its own data paths and ALUs
> (which is typical for an integer/FP split) or you can force
> instructions to use one argument from each file (like the 2800). The
> latter allows separate data paths to the ALU (left argument from one
> file, right argument from the other) but shares the ALU.

[1]: https://groups.google.com/g/comp.arch/c/xf7eQ0e8TZQ/m/cLFC_uYiWkcJ "Subject: split register sets; message-id unknown"

Another participant in the thread, whose name Google has erased,
comments in 02010:

> If the memory cells are true multiported, their area scales roughly
> with the square of the number of ports. Cells with more ports are
> slower too, and tend to use rather more power.
