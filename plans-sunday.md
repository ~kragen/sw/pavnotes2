Probably I'll wake up around 10:00 Sunday morning.  I'd like to get
into the habit of doing several hours of planned work each day, and
this seems like a good time to start.

If we divide the day recursively into thirds, we have one period of 24
hours, three shifts of 8 hours, 9 slots of 2h40m, 27 cubies of 53'20",
81 pomodoros of 17'46⅔", and 243 moments of 5'55.5̅".  The standard
Chinese "996" schedule is 9 AM to 9 PM, 6 days per week, but that's
probably higher than is optimal for creative work, though it might be
best for washing the dishes or riveting sheet metal.  It might be
reasonable to shoot for filling 4 of the 9 slots with planned work on
workdays: 10 hours and 40 minutes.

But let's start with *one* slot tomorrow.  The slot consists of 9
pomodoros; maybe the best approach is to do three pomodoros
individually, interspersed with one-pomodoro breaks and two full
cubies, intended for deeper work.

That is:

* Pomodoro N+0: task A
* Pomodoro N+1: break
* Pomodoro N+2: task B
* Pomodoro N+3: task B
* Pomodoro N+4: task B
* Pomodoro N+5: break
* Pomodoro N+6: task C
* Pomodoro N+7: break
* Pomodoro N+8: task D
* Pomodoro N+9: task D
* Pomodoro N+10: task D
* Pomodoro N+11: break
* Pomodoro N+12: task E

Or in schematic form, A - B B B - C - D D D - E.

If I were to get up 4 slots after midnight (12 cubies or 36 pomodoros:
10:40 AM) and begin this schedule one cubie later at 11:33:20, I would
finish it at 15:24:26⅔.  This is maybe suboptimal because it's a
little bit late for lunch.  If instead I were to get up 3 slots after
midnight (one shift, 9 cubies, 27 pomodoros, 8:00 AM) instead I would
start at 8:53:20 and finish at 12:44:26⅔, which seems like a
reasonable time to break for lunch.

That's probably too much of a shift for tomorrow morning because I got
up about 12:00 today and there will probably be loud nightclub music
playing tonight; I can already hear the asshole DJ beginning to shout
at the crowd.

But supposing that were the goal, I could maybe add a one-cubie lunch
break, maybe including time for a nap, and then another slot of work
hours: A - B B B - C - D D D - E - - - F - G G G - H - I I I - J, a
timespan of 29 pomodoros in all, 8h35m33⅓s.  If that starts at 8:53:20
(10 cubies) it finishes at 17:28:53⅓.  That seems like a pretty
reasonable way to organize a workday for creative work.  But that's
still only 2 slots' worth of actual work hours (18 pomodoros).  If I'm
going to spend 4 slots working each day I need to crunch them together
a bit more, with less break time.

Still, let's try the initial schedule above tomorrow.  Candidate tasks
include:

* washing the dishes
* writing a simple compiler for some Zorzpad experiments
* writing a simple ARM assembly program for some Zorzpad experiments
  (though I don't have the ARM dev tools handy)
* prototyping a cardboard box folding design system
* prototyping a text editor (also a Zorzpad experiment)
* trying out the M-sequence convolution approach on a real signal,
  either for fully distributed representation (see file
  `circulant-fully-distributed-representation.md`) or for
  communication (see file `m-sequence-transforms.md`)
* prototyping Meckle (see file `simple-graphics-thing.md`)
* prototyping text layout with nested tables (see file `nestable.md`)
* prototyping file `tclish-basic.md`
* prototyping wavegrower-style ellipse hatching (see file `ellipse-hatching.md`)
* prototyping the Blackstrap editor (see file `blackstrap.md`)
* attempting to analyze hypothetical optics with raytracing
* prototyping the VoxelSpace landscape renderer (see file `pseudo3d.md`)

Plan for Sunday
---------------

So, after a combination of random number generation and deliberate
choice, I'll try this:

* 10:40:00: wake up, take shower, drink mate, etc.
* 11:33:20: task A: wash dishes
* 11:51:06⅔: break
* 12:08:53⅓: task B: prototype wavegrower-style ellipse hatching
* 13:02:13⅓: break
* 13:20:00: task C: prototype Meckle
* 13:37:46⅔: break
* 13:55:33⅓: task D: prototype a small compiler with infix syntax
* 14:48:53⅓: break
* 15:06:40: task E: write retrospective on tasks A, B, C, D
* 15:24:26⅔: quit

The next day is Monday, and I'll have a pomodoro that day to bill my
hours worked that I'm late billing for, as well as following up on at
least one of these tasks, and doing some actual billable work.

The objective here is to get back into the habit of doing several
hours of billable work per day, or, failing that, work that improves
my immediate environment, enhances my skills, or advances my other
objectives (for example, by research).  I need to figure out what kind
of habits along these lines I can sustain.  I can get a lot done in
3–6 hours per day if I'm focused and not floundering.

More generally, I want to make myself into the kind of person who can
autonomously meet their most basic needs, again, in the context of
this society, by means of making plans and carrying them out.
Moreover, I want to be able to make and carry out plans for other
purposes as well, going beyond my own most basic needs.

Actual experience
-----------------

I woke up around 9:00, thanks in part to sunlight; I took a shower,
reviewed these plans, went to the bathroom, gathered up and took out a
couple bags of garbage, did concentration, and drank some mate on the
roof.  I went to the grocery store, bought an açai energy drink,
greeted the newspaper stand guy, petted a dachshund outside the
grocery store, and petted a cat sticking out somebody's window.

I realized that at 15:00 I have a chat with a friend of mine planned,
so I'm revising the tail end of the schedule as follows:

* 14:48:53⅓: break
* 15:00:00: chat
* 16:00:00: task E: write retrospective on tasks A, B, C, D
* 16:17:46⅔: quit

I feel remarkably calm in the moment despite my life teetering on the
brink of total collapse.  Perhaps it's a result of feeling relieved
from the pressure to do everything by having definite plans to do some
things.  Or maybe it's just the sun.  And the yerba mate.

### First break ###

Then I washed the dishes, finishing them almost precisely at one
pomodoro.  Now I have a break.  And I have all the dishes clean.

It occurred to me that it would be useful to organize my
learning-oriented planned tasks with spaced repetition.  For example,
the wg-style rendering thing involves 3-D geometry, probably
JavaScript (since it's a quick prototype), probably `<canvas>`, visual
rendering, and the Zorzpad.  It would be desirable to keep track of
these things so I don't go too long without practicing, say, Rust,
React, asyncio, linear optimization, or automatic differentiation.

At this moment I feel like an entire pomodoro of break time is too
long, which is sort of reassuring, because I was thinking it would be
hard to get 4 slots of planned tasks into one day with so much break
time in between.

It occurs to me that a schedule-clock program would be useful for
this.  Something that shows the current planned task (or other block),
the amount of time remaining until it ends, and the next planned task,
and that plays an alarm when it expires.  A nice bonus would be an
editable schedule display (maybe as a Sierpiński triangle, if I'm
using this base-3 breakdown of the day) and an easy facility for
adding notes on the current block.

Also it would be nice to have a soundtrack setup that, for example,
plays only music without lyrics during planned tasks, and no music
during planned meetings.

So now I'm waiting impatiently for 12:08:53⅓ so I can get started on
ellipse hatching!

### Second break ###

Well, that was pretty great; I made real progress on ellipse hatching,
though unfortunately it was almost all a matter of trying to figure
out how to draw non-antialiased ellipses in `<canvas>`.  I didn't want
to stop, so maybe I'll spend more time hacking on this later, after
the planned tasks are over.

I learned that, although I have the W3C recommendations installed
locally (package w3-recs, 183 megabytes) they don't include
documentation for `<canvas>`!  I wonder if there's an offline-usable
copy of MDN.  And it occurs to me that, at least on Linux, Kiwix might
work better as a type of compressed filesystem rather than an unstable
application.

I'm not super optimistic about what I'll be able to get done on Meckle
in a single pomodoro.  The three-pomodoro (one-cubie) time for ellipse
hatching was enough to get my head into the space and get a result
that's at least fun to play with, even if it doesn't actually look
like anything in 3-D.

I got some evidence that drinking mate provokes diarrhea for me, in
this case with 1½ hours of delay.  Useful information.  Consequently
this break wasn't too short.

### Third break ###

Okay, so I got a really barebones Meckle prototype kind of running in
a single pomodoro using Python3 and tkinter.  It does run, present a
GUI, and do I/O, but it isn't quite good enough to actually use for
anything, not even bringing up a dialog box from a shell script.  But
it seems like a cubie's worth of time ought to produce something
useful.

I'm happy to find that I do have both tkinter (package python3-tk) and
the Python3 documentation installed locally (package python3.8-doc, in
/usr/share/doc/python3.8).  However, tasks squeezed into a single
pomodoro probably are too small for something that involves reading a
manual.

Not having a reasonable parsing toolkit ready to hand was a handicap
for the Meckle prototype, as was being rusty with tkinter.  The next
task is an infix compiler task, which is also pretty parsing-heavy.
Unfortunately I also want to do it in Python, and I don't have Parson
installed.  But I do have python3-pyparsing installed, though not its
docs.

Hmm, I guess I didn't do very well at taking a break!  Another
indication that my breaks are too long.

### Fourth break ###

I really need a clock denominated in pomodoros and cubies and things.

I learned enough about pyparsing from its pydoc to get it to compile
this:

    n = 0;
    for (i = 0; i < 10; i++) {
        n += i;
    }

To this:

    push 0
    dupstore n
    pop
    for
    (
    push 0
    dupstore i
    ;
    load i
    push 10
    lessthan
    ;
    incr i
    )
    load i
    addstore n
    pop

Which is, you know, not quite runnable code, but definitely getting
close.  Maybe constructing an AST would have been a good idea.  I
think probably another cubie might be enough to compile it to
working assembly language.

Now I'm making some noodle soup and waiting for my friend to show up.
During the day I realized I need some more bottled water, and I think
some earphones would help with listening to music; maybe I'll head to
Lacroze or Barrio Chino to pick up some cheap shit headphones and
bottled water.

### Retrospective on the planned task time ###

Well, that was pretty great.  I took some tangible steps toward making
my apartment habitable, inventing a new form of non-photorealistic
rendering, getting 3-D on the Zorzpad, trying out Meckle, and writing
a compiler for the Zorzpad, and in the process I learned more about
`<canvas>`, tkinter, and pyparsing, as well as PEG parsing engine API
design.  Maybe more importantly, I was able to spend a couple of hours
with my attention directed to what I had chosen and planned to direct
it to ahead of time: a real step towards making a habit of carrying
out plans.

I'm a little bit tired, which I suspect is partly because I just ate a
big bowl of noodles and partly because I have had entirely too much
caffeine today, not just because I spent a couple of hours doing
concentrated mental work.

I am left with lots of questions:

- How can I run a Python loop reading from stdin while running a
  Tkinter GUI?  The Python REPL does it!  Though I have a vague,
  possibly incorrect, memory that it didn't always.  The Tkinter docs
  are no help!
- Is it possible to store a sequence of instructions under a name in a
  pyparsing parse result?  How exactly do token sequences interact
  with named results in pyparsing?
- How do I disable antialiasing in `<canvas>`?  Is it even possible?
  If not, is it possible in SVG?
- If I render a `<canvas>` at three times the screen resolution
  without antialiasing, does the browser antialias by rescaling it for
  display?  Is the antialiasing reasonably good?  Is an optimized PNG
  file of the full-resolution result smaller or larger than the same
  thing antialiased in the usual way?

2½ hours of awake time before getting started on pomodoros was too
long, and possibly an entire pomodoro between planned tasks was also
too long.  A pomodoro clock would be helpful.

Leftover things today: possible headphones, bottled water, charging
SUBE, charging my cellphone account, taking an afternoon nap, and
planning tomorrow.  I'll see if I can manage the nap now.

Well, no, I couldn't.  Too much caffeine?

Went to Chinatown and got a pair of earphones (AR$550, US$2.10, the
only earphones I've ever seen that have poor bass, and both of the
earbuds are evidently connected to the right channel, but whatever,
they're still better than the laptop speakers), three gel candies to
share with Mina when she comes (US$1.10), a 1.5-liter bottle of
carbonated water because Mina's concerned that the water in my
apartment is green-brown (US$0.75), a box of tooth flossers (US$1.50)
and a diet Red Bull which I probably shouldn't have drunk (US$1.10).

It occurs to me that one of the main risks to establishing this
planning habit is that I might get bored with the process of planning
itself, as well as the retrospectives.

I feel like automating some of the planning process is going to be an
important aspect of its sustainability.  I've written 7 pages on this
so far, 3500 words: about 2 pages last night and about 5 pages today.
I probably won't want to keep spending that much time on the planning
process, so I need to make sure it can experience lower levels of
attention without totally collapsing.

It's interesting that I've
made more progress on priority things today by *not prioritizing* than
I have in weeks.  Picking up *something* and working on it beats the
living shit out of dithering in indecision about what to invest time
in.  On the other hand, if I work entirely on random things and never
invest *enough* time in any one thing, I might still not get anywhere;
but that's still a different problem from not spending any time doing
anything productive.

There's the risk of too much focus on one thing; Dietrich Dörner's
*The Logic of Failure* found that a common failure mode for people in
complex problem solving situations was to focus entirely on the parts
of the problem they knew how to solve, even when those parts of the
problem were already doing well enough.  Hyperfocus can result in
ruining the kettle by letting it boil dry, and worse things along the
same line.  In a sense today I was mostly focused on programming stuff
rather than things like cleaning my apartment, though I did do a
little apartment-cleaning stuff.

So some degree of prioritization might be worthwhile: looking over the
things that might be getting too close to disaster (like, not having
sent an invoice, or not having paid the rent) and making sure to do
those.  I'm already doing abominably on those, though, so I don't
really have to worry about doing worse!

Also, suppose I have 32 pending tasks that I'm picking from, and I do
five each day, one of which is the same as a task from a previous day.
I'll get through all 32 in 8 days.  That's a much shorter time than
I've been failing to make progress on many of these.  So it's probably
okay if most of my effort goes to low-priority things if it speeds up
getting to the high-priority ones.

Plan for Monday
---------------

I'll be here in the morning and early afternoon and head back to
Mina's house in the later afternoon.

Things to consider doing tomorrow before that: one of:

- work more on Meckle
- work more on simple compilers
- work more on ellipse hatching

Plus one or more of these researchy thing:

* writing a simple ARM assembly program for some Zorzpad experiments
  (though I don't have the ARM dev tools handy)
* prototyping a cardboard box folding design system
* prototyping a text editor (also a Zorzpad experiment)
* trying out the M-sequence convolution approach on a real signal,
  either for fully distributed representation (see file
  `circulant-fully-distributed-representation.md`) or for
  communication (see file `m-sequence-transforms.md`)
* prototyping text layout with nested tables (see file `nestable.md`)
* prototyping file `tclish-basic.md`
* prototyping the Blackstrap editor (see file `blackstrap.md`)
* attempting to analyze hypothetical optics with raytracing
* prototyping the VoxelSpace landscape renderer (see file `pseudo3d.md`)
* analyze the flexion of a cantilever (see file `pipe-dome`)
* prototype some kind of notecard-granularity hypertext browser (see
  file `grain-editor`)
* prototype something or other with gradient descent, maybe inverse kinematics
* get the linear-time dilation and erosion stuff working

Plus one or more things for improving my immediate environment:

- set up keybindings and Compose bindings properly on this Dell
- sweep my apartment
- sweep Mina's apartment
- set up my Emacs setup on this Dell with smartquotes and stuff
- write a clock program to reduce the effort of following a plan
- find out if I can mirror MDN for offline reading.  Or at least HTML5!
- also mirror StackOverflow for offline reading
- replace this Dell's hard disk to see if a different one works better

Things to consider doing at Mina's:

- fill out invoice and send it
- talk with parents
- talk with Banasidhe
- write draft merge request for stack overflow fix in Hammer
- answer the pre-COVID SRI email
- find out the answer about the nonantialiased `<canvas>` and/or SVG
- find out the answer about Tkinter and I/O loops
- find out how token sequences interact with named results in pyparsing
- try out the `<canvas>` superresolution thing
- cut out a cardboard drawer prototype at larger than 3:10 scale, from actual cardboard
- call my landlord to pay the rent
- get something running on an Ambiq chip
- get something running on the FPGA devboard
- control a motor with a microcontroller
- put all my electronics stuff in a box or bucket or collection of boxes and buckets
- get a bucket
- electro-etch steel

I think I'll probably get up tomorrow at about 9:30, given that today
I got up at 9:00 and couldn't get back to sleep, but I've been in the
habit of getting up at 12:00–15:00.  Let's round that to 11 cubies:
9:46:40 AM.  2½ hours after getting up was far too long to wait, but
perhaps the one cubie I had planned would be fine, so I'll start at 12
cubies, 4 slots, 10:40.

So I can start like this:

* 09:46:40: get up, take a shower, drink mate, do concentration
* 10:40:00: task A: try to get ellipse hatching to show a rotating cube
* 10:57:46⅔: break (for now I'll keep with the one-pomodoro breaks)
* 11:15:33⅓: task B: prototype a text editor
* 12:08:53⅓: break

But at that point Mina will probably be here, and we will travel to
her house, which might take until 15 cubies past midnight, 13:20.

* 13:20:00: task C: send invoice(s)
* 13:37:46⅔: break
* 13:55:33⅓: task D: write draft merge request for stack overflow fix in Hammer
* 14:48:53⅓: break
* 15:00:00: talk with family
* 16:00:00: talk with Banasidhe
* 16:17:46⅔: break
* 16:35:33⅓: task E: retrospective on tasks A, B, C, D
* 16:53:20: break
* 17:11:06⅔: plan Tuesday
* 17:28:53⅓: quit

This seems probably achievable, though I might be pretty tired when we
get to Mina's from the fucking bus ordeal, which is not going to be
the easiest time to motivate myself to face invoices.  Whatever.

Things to watch for:

- Am I able to actually follow this schedule, despite the disruption
  of travel and the availability of high-bandwidth internet access?
  It has 10 pomodoros.
- Are the breaks a reasonable length?  Or do I still feel they're too
  long?  Or do I start feeling they're too short?
- Do I feel like I could have taken a nap when we arrived at Mina's
  house?  Naps provide an enormous cognitive boost.
- What kind of affordances would make it easier to make and follow
  these plans?
- How does my cognitive performance before and after a pomodoro or
  cubie differ?

### Actual experience ###

Got up at the appointed time but got distracted by chatting with
people (and signed up for a responsibility for tomorrow).  I think
I'll postpone the first pomodoro for 15 minutes.  Revised:

* 10:55:00: task A: try to get ellipse hatching to show a rotating cube
* 11:12:46⅔: break
* 11:25:00⅔: task B: prototype a text editor
* 12:18:20⅓: break

#### First break ####

Ugh.  I didn't manage to make any changes to the ellipse hatching code
at all; trying to get the idea clear in my mind, I wrote like 80 words
of text, and some new code, but no calls to it.  That's maybe a lesson
for breaking up changes into smaller parts.  Also, I probably spent
the first two or three minutes of the pomodoro coming down from the
roof, which shortened the available time further.

I sure was focused, though!  That intentionally directed focus is the
main thing I'm trying to cultivate.  Even if on some occasions it
doesn't bear fruit, it's far more fertile ground than arguing with
random people on the orange website.

#### Second break ####

So, I almost have a working text editor, 108 lines of Python, but it
can't quite save.  Maybe I should have implemented that first.  It can
edit itself though.  It doesn't do anything weird yet.

I got distracted partway through because Mina arrived and then was
stressed out from travel.  We spent a little time together and now I'm
replanning the rest of the day.  It's 13:00.

* 13:10 to 14:40: travel to Mina's house
* 14:40: task C: write and, if possible, send invoice
* 14:57:46⅔: break
* 15:00: talk to family
* 16:00: talk to Banasidhe
* 16:17:46⅔: break
* 16:35:33⅓: task D: write draft merge request for stack overflow fix in Hammer
* 17:28:53⅓: break
* 17:46:40: task E: retrospective on tasks A, B, C, D
* 18:04:26⅔: break
* 18:22:13⅓: plan Tuesday
* 18:40:00: quit

#### After traveling ####

We arrived 14:35 but I got distracted with a discussion on etymology
until talking to family and Banasidhe.  So it's 16:08 and I still need
to send the invoice:

* 16:17:46⅔: break
* 16:35:33⅓: task C: write and, if possible, send invoice
* 16:53:20: break
* 17:11:06⅔: task D: write draft merge request for stack overflow fix in Hammer
* 18:04:26⅔: break
* 18:22:13⅓: task E: retrospective on tasks A, B, C, D
* 18:40:00: break
* 18:57:46⅔: plan Tuesday
* 19:15:33⅓: quit

#### Fourth break ####

Okay, I actually wrote an invoice.  It's incomplete, but I haven't
sent it yet.  I'm pretty sure I can finish it and send it off in
another pomodoro.  But the next task is a draft MR.

#### Finally ####

Well, I ended up writing some notes about the draft MR, and then not
actually doing them, instead getting sidetracked by arguing with
people on the orange website.

I fiddled around a little more with the editor ("edumb") and the
ellipse hatching stuff.  Apparently there's no way to turn off
antialiasing on the JS canvas!  But I did get edumb to the point of
being able to self-host its own further development.  Which, really,
is probably finished.

So I was able to focus for the first three tasks, pretty much, but not
the fourth, perhaps the most aversive one.

I suspect that the first task I do in the morning benefits from being
something relatively mindless.

So today's tasks were:

- write invoice (mostly done)
- write stack overflow MR (not really started)
- write text editor (finished)
- do non-photorealistic 3-D rendering (no real progress)
- review progress (doing now)

So I did probably 5 of the 10 pomodoros, really.

I don't have strong feedback on break length.  I think that was
dominated by travel.

I did try to take a nap in the afternoon but couldn't sleep; heart
pounding, etc.  Probably less caffeine would be good.  No diarrhea,
though, despite drinking mate.

I also signed up to try to get Sam Atman's Espalier to compile to
pyparsing tomorrow, so I'd better devote at least one pomodoro to
that.

Plan for Tuesday
----------------

So, Tuesday, 02022-09-12.  I'll get up at 9:46:40 AM and start my
first pomodoro at 10:40:00.  But what should I do?  The urgent tasks
are:

- finish and send invoice
- write stack overflow MR
- try to make Espalier compile to pyparsing

If I'm planning five tasks, one of which is the retrospective, that
leaves space for one other.  A random number generator selecting from
some of the stuff above suggests:

- get something running on an Ambiq chip

I guess that of the four tasks, "finish and send invoice" is the one
that requires the least creativity, so I'll do that first.  Of the
other three, the one to assign a single pomodoro to is probably "try
to make Espalier compile to pyparsing"; I surely won't finish it but
hopefully I'll learn enough to have useful questions for Sam.

So, here's the draft plan:

- 09:46:40: get up, take shower, do concentration, prepare and drink mate
- 10:40:00: task A: finish and send invoice
- 10:57:46⅔: break
- 11:15:33⅓: task B: write stack overflow MR (because that way it'll
  be more likely to get done before talking to Banasidhe)
- 12:08:53⅓: break
- 12:26:40: task C: try to make Espalier compile to pyparsing
- 12:44:26⅔: break
- 13:02:13⅓: task D: get something running on an Ambiq chip
- 13:55:33⅓: break
- 14:13:20: task E: retrospective on tasks A, B, C, D
- 14:31:06⅔: break
- 15:00:00: talk to family
- 16:00:00: talk to Banasidhe
- 16:10:00: break
- 16:30:00: plan Wednesday
- 16:47:46⅔: quit

So, I guess that's where I'll leave it for now.

### Actual experience ###

I accidentally woke up at 4 AM, so I got about 5 hours of sleep.  Took
a shower, but the hot water cut out partway through, which was
unpleasant.  Did concentration and walked around a couple of blocks
and heated up water for mate.

#### Invoice ####

I was able to finish and send the invoice, despite being interrupted
by pooping.  I ran into the break a little bit to get it sent.

#### Stack overflow MR ####

I wasn't able to reproduce the error!  I ran the current version of
the PDF parser with the input files that used to crash it, but they
mostly don't anymore.  I ended up running a little bit over the time
slot, into the break, again.

Lots of good interaction on the chat.

In the process I got bccpi.py and some test PDFs onto the new laptop.

Then I missed the *end* of the break, thus shortchanging the Espalier
task a bit.

#### Espalier compiling ####

I kind of thrashed around for half an hour trying to remember how to
get bridge-tools going again.

I keep failing to notice end points of time intervals because I set a
bunch of alarms at the beginning of the day, but not all of them, and
so once I ran out of the preset alarms I kept forgetting to add new
ones.  Like when you wear pants with no zipper for a while, you're
more likely to forget to zip your zipper on the rare occasion that you
wear pants with a zipper.  So I keep forgetting to take breaks.

So, given that, and that I've done the SC tasks, and that I need more
sleep, I think I'll postpone the Ambiq thing until after talking with
people and try to take a nap.

...but sleeping failed.  Heart pounding, mind racing.  Back up to
write about stuff.

#### Ambiq stuff ####

I didn't actually do a cubie making stuff run on the Ambiq board, or
even get a board out.  What I did do was google around and find the
Ambiq Suite SDK and download it, and read some tutorials on how to get
stuff up and running with it, and peruse its source code a little.  It
looks very promising; it seems like a few shell commands are all I
should need to get some demos up and running on at least one of the
Ambiq boards I got.

#### Retrospective ####

So, I spent five pomodoros focused on the stuff I planned to focus on,
pretty much all billable work.  And I acquired some new to-do items.
But I only actually did three of the five tasks I'd planned, at least
in their time slots.  Though I did spend some time on each of them at
some point.

Mate again failed to provoke diarrhea.

Setting several, but not all, alarms in advance, that was a terrible
idea.

I'm probably significantly sleep-deprived and thus functioning well
under capacity.  I'm going to try to sleep early tonight.

Getting that invoice sent was a huge relief.

Plan for Wednesday
------------------

Here are my leftover to-do items from Tuesday:

- build pylon so I can try to build Espalier compiler stuff
- write xref fixing script for PDFs
- make an issue to track the stack overflow problem in the PDF parser
  (or bump the existing one)

I also need to:

- keep digging into the stack overflow problem

I think I'll pick three of these four billable tasks and postpone the
other one for later.

- Task A: write xref fixing script for PDFs
- Task B: make an issue to track the stack overflow problem in the PDF parser
  (or bump the existing one)
- Task C: keep digging into the stack overflow problem in the PDF parser
- Task D: get something running on an Ambiq chip
- Task E: retrospective on tasks A, B, C, D

Postponed until at least Thursday:

- build pylon so I can try to build Espalier compiler stuff for pyparsing

So here's the plan:

- 09:46:40: get up, take shower, do concentration, prepare and drink mate
- 10:40:00: task A: write xref fixing script for PDFs
- 10:57:46⅔: break
- 11:15:33⅓: task B: make a Gitlab issue to track the stack overflow
  problem in the PDF parser (or bump the existing one)
- 12:08:53⅓: break
- 12:26:40: task C: keep digging into the stack overflow problem in
  the PDF parser (and create MR if possible)
- 12:44:26⅔: break
- 13:02:13⅓: task D: get something running on an Ambiq chip
- 13:55:33⅓: break
- 14:13:20: task E: retrospective on tasks A, B, C, D
- 14:31:06⅔: break
- 15:00:00: talk to family
- 16:00:00: talk to Banasidhe
- 16:10:00: break
- 16:30:00: plan Thursday
- 16:47:46⅔: quit

### Actual experience ###

Got up at 07:45, got distracted by chats.  Didn't take a shower, but
did do concentration, prepare and drink mate, go out and buy a
Monster, walk around a little, write some Hadamard–Walsh transform
code, that kind of thing.

#### Xref fixing script ####

Once I managed to disconnect from the chat, I did manage to spend a
pretty focused 15 minutes getting this script started, and got it to
successfully compute xrefs for one input file.  Next up will be
replacing the existing xrefs and formatting the computed xrefs.

Now that I think about it, this is kind of ... the same as the first
part of the next task actually, because what I need to do to dig into
the stack overflow problem is probably to generate PDFs that overflow
the stack.

Or, I guess, alternatively I could run the *one* stack-overflowing PDF
that still segfaults under GDB, and verify that that's still what it's
doing.

#### PDF parser stack overflow ####

Well, so the h-hashtable-get-comp-2 bug is definitely a stack
overflow, 10312 stack levels deep; it's parsing a large array (at byte
8022 into the file), but oddly enough it's not a deeply nested array,
just a long one with both integers and arrays of integers in it.

The other cases really were deeply syntactically nested structures.
But, maybe I mentioned above, my attempts to provoke stack overflows
yesterday with deeply nested structures failed, and I think I may need
to fix the xrefs to make them work.  The fact that this one surviving
segfault doesn't fit that pattern makes me wonder if fixing the xrefs
will be enough.

And, dismayingly, upon getting the xrefs to build properly, I still
can't reproduce the issue with one example of deeply nested syntax.  I
just get:

    helloverflowfixed.pdf: no parse
    helloverflowfixed.pdf: error after position 9 (0x9)

I also realize I got the "stack overflow investigation" task
interchanged with the "make a Gitlab issue" task.

I overflowed a bit into the break because I was so eager to see the
results.  The famous disease of "one last bug fix".  I think probably
the sudden interruption of the alarm is actually pretty helpful for
getting started again later, because there's an obviously unfinished
thing to work on.

#### Afterwards ####

I got distracted with IRC (partly due to having inadvertently offended
someone in the morning before starting work) and then talked to family
and Banasidhe.  Now I'm super tired for no reason (except, I guess,
sleeping only 8 hours, and I guess having a dismaying debugging result
just as I ran out of time earlier).

I guess it's good that I was able to do 1.25 hours of billable work
today, even if that's less than the 2 hours yesterday.  But I should
figure out how I can do more.

Mina has taken care of getting a bucket!

Here's an updated list of planned tasks to do again tomorrow, or if I
feel up to it, later tonight:

- make a Gitlab issue for the PDF stack overflow
- dig into PDF stack overflow further, including xref fixing script
- try to make Espalier compile to pyparsing

Also I think I'd better do this:

- call my landlord to pay the rent

But that won't take an entire pomodoro.

And here is a bag of things to pick from at random:

- write a quantitative constraint modeling calculator
- write some kind of email client
- download my mailbox to a portable disk
- write a clock for this pomodoro scheduling stuff
- finish doing Walsh-Hadamard transforms in finite groups
- work more on Meckle
- work more on simple compilers
- work more on ellipse hatching
- writing a simple ARM assembly program for some Zorzpad experiments
  (though I don't have the ARM dev tools handy)
- installing the ARM dev tools on the Dell
- prototyping a cardboard box folding design system
- prototyping a text editor (also a Zorzpad experiment)
- trying out the M-sequence convolution approach on a real signal,
  either for fully distributed representation (see file
  `circulant-fully-distributed-representation.md`) or for
  communication (see file `m-sequence-transforms.md`)
- prototyping text layout with nested tables (see file `nestable.md`)
- prototyping file `tclish-basic.md`
- prototyping the Blackstrap editor (see file `blackstrap.md`)
- attempting to analyze hypothetical optics with raytracing
- prototyping the VoxelSpace landscape renderer (see file `pseudo3d.md`)
- analyze the flexion of a cantilever (see file `pipe-dome`)
- prototype some kind of notecard-granularity hypertext browser (see
  file `grain-editor`)
- prototype something or other with gradient descent, maybe inverse kinematics
- get the linear-time dilation and erosion stuff working
- set up keybindings and Compose bindings properly on this Dell
- sweep my apartment
- sweep Mina's apartment
- set up my Emacs setup on this Dell with smartquotes and stuff
- write a clock program to reduce the effort of following a plan
- find out if I can mirror MDN for offline reading.  Or at least HTML5!
- also mirror StackOverflow for offline reading
- replace this Dell's hard disk to see if a different one works better
- answer the pre-COVID SRI email
- find out how token sequences interact with named results in pyparsing
- try out the `<canvas>` superresolution thing
- cut out a cardboard drawer prototype at larger than 3:10 scale, from actual cardboard
- get something running on an Ambiq chip
- get something running on the FPGA devboard
- control a motor with a microcontroller
- put all my electronics stuff in a box or bucket or collection of boxes and buckets
- electro-etch steel

So my planned tasks for tomorrow (or later tonight if I feel up to it)
are:

- make a Gitlab issue for the PDF stack overflow
- dig into PDF stack overflow further, including xref fixing script
- try to make Espalier compile to pyparsing
- call my landlord to pay the rent
- sweep Mina's apartment

The following Monday
--------------------

So, Thursday, I fell off the wagon pretty badly, in terms of planning.
I didn't make plans for Thursday, and then I didn't carry out any
planned tasks, because there weren't any.  For six days; today I got
up at 11:00.  So today I am taking this approach back up.

It's already Monday night, so I'm going to make plans for tomorrow
instead of for tonight.

Minimal plans for Tuesday:

- 09:46:40: get up, take shower, do concentration, prepare and drink
  mate, and plan day
- 10:40:00: task A
- 10:57:46⅔: break
- 11:15:33⅓: task B
- 12:08:53⅓: break
- 12:26:40: task C
- 12:44:26⅔: break
- 13:02:13⅓: task D
- 13:55:33⅓: break
- 14:13:20: task E: retrospective on tasks A, B, C, D
- 14:31:06⅔: break
- 15:00:00: talk to family
- 16:00:00: talk to Banasidhe
- 16:10:00: break
- 16:30:00: plan Wednesday
- 16:47:46⅔: quit

Tuesday, Sep 20
---------------

Okay, well, I got up about 8:00 because I couldn't sleep, which I
guess is some sort of improvement over waking up at 11:00 yesterday.
I've put some black-eyed peas in water to soak so I'll have something
to eat tonight and made some mate; I had an unopened 1-kg bag of "BCP"
"bajo contenido de polvo" yerba, Taragui brand, did concentration,
read, and wrote a little.

- 10:40:00: task A
- 10:57:46⅔: break
- 11:15:33⅓: task B
- 12:08:53⅓: break
- 12:26:40: task C
- 12:44:26⅔: break
- 13:02:13⅓: task D
- 13:55:33⅓: break
- 14:13:20: task E: retrospective on tasks A, B, C, D
- 14:31:06⅔: break
- 15:00:00: talk to family
- 16:00:00: talk to Banasidhe
- 16:10:00: break
- 16:30:00: plan Wednesday
- 16:47:46⅔: quit

...well, what I actually did, that day and Wednesday, was write 6300
words about particle physics (see file `small-power.md`) and 600 words
about pseudospark generators (see file `micro-pseudospark.md`).

In summary, while I enjoyed the relaxation of being at my apartment, I
didn't really get anything done.
