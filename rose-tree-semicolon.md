I was thinking about OGDL again today because of Jevko, whose data
model seems maybe actually more interesting.

A nice thing about OGDL is that it represents nesting with just
concatenation, like a filesystem path:

    network interfaces eth0 ip 192.168.0.1

It's almost a rose-tree notation like Prolog or Mathematica, but OGDL
labels nodes rather than arcs, and it supports multiple references to
a node, so it's not entirely limited to trees.  It uses indentation to
eliminate most punctuation.

The sort of canonical way to represent the above structure in Prolog
or Mathematica doesn't use indentation; it looks more like this:

    network(interfaces(eth0(ip(192.168.0.1))))

It occurred to me that the open parenthesis is unnecessary if you
don't permit abbreviating `foo()` as `foo`; this occurs once in the
above example, with `192.168.0.1`.  If you eliminate this possibility,
you can just use a close parenthesis:

    network interfaces eth0 ip 192.168.0.1)))))

But that looks wrong, because we expect parentheses to balance, so it
would be better to use a different terminator.  Like:

    network interfaces eth0 ip 192.168.0.1;;;;;

Or, consider OGDL's canonical example:

    network
      eth0
        ip   192.168.0.10
        mask 255.255.255.0
        gw   192.168.0.1

    hostname crispin

This becomes:

    network
      eth0
        ip   192.168.0.10;;
        mask 255.255.255.0;;
        gw   192.168.0.1;;
    ;;

    hostname crispin;;
