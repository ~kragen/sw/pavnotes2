I wrote [a really simple didactic recursive-descent infix
compiler](http://canonical.org/~kragen/sw/dev3/didcomp.c) today.  The
main function is this:

    static void translate(int dep)
    {
      token op = {0};                  // additive operation between terms
      for (;; next()) {                // translate one or more terms
        int td = dep + (op.s ? 1 : 0); // term depth
        token top = {0};               // term op (multiplicative)
        for (;; next()) {              // translate one or more factors
          translate_factor(td + (top.s ? 1 : 0));
          if (top.s) {
            printf("\t%s r%d, r%d, r%d\n",
                   eq(top, "*") ? "mul" : "div", td, td, td+1);
          }

          if (!eq(top = cur, "*") && !eq(top, "/")) break;
        }

        if (op.s) {
          printf("\t%s r%d, r%d, r%d\n",
                 eq(op, "+") ? "add" : "sub", dep, dep, dep+1);
        }

        if (!eq(op = cur, "+") && !eq(op, "-")) break;
      }
    }

I was curious about where it spent most of its time.  The answer
according to callgrind is that, despite all the obviously stupid
string comparisons, it spends about 90% of its 1055 instructions per
token in `printf` and `fwrite` (you need `fwrite` to output tokens,
which is done in `translate_factor`).  Most tokens correspond to a
single output instruction like

            add r3, r4, r4

1000 instructions per token is about 6 million tokens a second on one
core of this laptop, but on something like a 48MHz ARM it might be
more like 40000 tokens a second, slow enough to detect even when
recompiling a large method.  All it’s doing is appending about 3–5
potentially precanned strings like “\n\tadd ” and “r3, ” to the output
buffer, so it’s appending something like 30 million such strings per
second.  The strings are small enough that copying their bytes to the
buffer is going to be faster than any kind of writev-like piece-table
setup.

In [kmregion.h](http://canonical.org/~kragen/sw/dev3/kmregion.h) I got
about 520 million region allocations (and initializations) per second
on one core with this fast path, which allocates downward from the end
of the region:

    static inline void *
    km_new(kmregion *r, size_t n)
    {
      n = (n + alignof(void*) - 1) & ~(alignof(void*) - 1);
      size_t p = r->n - n;
      if (p <= r->n) {
        r->n = p;
        return r->bp + p;
      }

      return km_slow_path_allocate(r, n);
    }

I think it’s probably possible to get similar speeds with a similar,
but simpler, approach for appending strings to an I/O buffer, inlining
the fast path.

Directly emitting binary code is almost guaranteed to be faster than
appending textual assembly code, though.

The obviously stupid string comparisons actually get inlined to a
significant extent; the check to see if the current token is either \*
or /, followed by the inner-loop `next()` call, ends up looking
something like this:

        150d:       83 fd 01                cmp    $0x1,%ebp
        1510:       74 5e                   je     1570 <translate+0x100>
    ...
        1570:       0f b6 03                movzbl (%rbx),%eax
        1573:       3c 2a                   cmp    $0x2a,%al    ; '*
        1575:       74 11                   je     1588 <translate+0x118>
        1577:       3c 2f                   cmp    $0x2f,%al    ; '/
        1579:       74 0d                   je     1588 <translate+0x118>
        157b:       4d 85 ff                test   %r15,%r15
        157e:       75 97                   jne    1517 <translate+0xa7>
    ...
        1588:       31 c0                   xor    %eax,%eax
        158a:       e8 61 fc ff ff          call   11f0 <next>
        158f:       e9 2c ff ff ff          jmp    14c0 <translate+0x50>

So detecting that the token is a multiply only requires five
instructions.  Still, probably some kind of token-type switch using a
token-type enum set by the `next()` function would almost certainly be
faster.

The conventional DFA | LALR approach seems like it would reduce the
choice of what to do next to a few instructions.  It might have poor
branch-prediction features, but branch prediction seems like a bit of
a lost cause here anyway.
