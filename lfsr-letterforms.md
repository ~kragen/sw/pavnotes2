I was discussing vector CRTs with qu1jot3 (indexphp) today, and he pointed out
some Fourier-series-driven analog character generators.  They multiply
ten terms from a Fourier series by carefully chosen coefficients to
get oscilloscope deflection voltages to trace out the characters.  The
original paper on this is from 01958, using carefully trimmed
secondary windings on transformers to get the necessary 20 analog
multiplications (voltages which are summed by putting the secondaries
in series) but Glen K has done a current version.

Sine and cosine harmonics do provide a particularly nice basis for
this kind of thing, because they’re orthogonal.  But it occurred to me
that there were other basis functions that would probably be easier to
generate, and would probably work fine.  For example, different taps
on an LFSR.  The pure digital square-wave noise output from the LFSR
wouldn’t be ideal because it’s almost discontinuous, but a little bit
of passive filtering ought to clean that up.  [Here’s an example
circuit][1]:

![(schematic plotting different filtered LFSR taps)](noise-plotting.png)

In the text format of Falstad’s circuit.js:

    $ 1 7.8125e-10 0.2954511527092107 50 5 43 5e-11
    189 32 208 176 208 5120 10 404
    154 208 320 288 320 0 2 5 5
    w 336 128 96 128 0
    w 96 128 96 176 0
    w 160 304 208 304 0
    w 160 336 208 336 0
    I 288 320 336 320 0 0.5 5
    w 336 320 336 128 0
    R 64 176 64 96 0 2 40000000 5 0 0 0.5
    w 160 528 224 528 0
    403 96 560 240 720 0 9_1_0_4098_5_0.003125_-1_2_9_3
    r 224 528 368 528 0 100
    c 368 528 368 656 0 4.7e-10 3.138073034412179 0.001
    g 368 656 368 688 0 0
    403 432 560 592 688 0 12_1_0_4098_5_0.0015625_-1_2_12_3
    r 160 496 688 496 0 100
    c 688 496 688 656 0 4.7e-10 2.979075842267241 0.001
    g 688 656 688 688 0 0
    403 736 544 896 688 0 16_1_0_4098_5_0.1_-1_2_16_3
    r 160 240 624 240 0 100
    g 624 400 624 432 0 0
    c 624 240 624 400 0 4.7e-10 1.389955684446081 0.001
    403 448 272 608 432 0 21_1_0_4098_5_0.1_-1_2_21_3
    o 21 1 0 x810d6 5 0.1 0 2 0.5 -45 12 0 0.5 -110
    o 12 1 0 4290 5 0.1 1 2 16 0

[1]: http://tinyurl.com/yqsu27rp

You can build a period-15 LFSR from four bits of a SIPO shift register
and an XOR gate, and different delays on its output will get you up to
15 almost precisely orthogonal signals.  I took a look at my notes on
JLCPCB’s 689 “basic” parts from a couple of years ago (see file
`pick-and-place.md`) and see that relevant shift registers include the
74HC164 (9.04¢) and the latched-output 74HC595 (10.84¢), both 8 bits,
so you'd need two chips.

They don’t include any XOR gates, just a hex inverter and a quad AND,
but they do include 8-channel analog mux/demuxers CD4051/2/3, the 4051
for 15.01¢.  By wiring the CD4051’s 8 multiplexed channels to power or
ground, you have a 3-LUT, which can simulate any three-input
combinational logic on its ABC select pins.  The CD4052 used in the
same way only has two selector inputs (AB) but two separate outputs (X
and Y), so you could, for example, simultaneously compute the AND and
XOR of the two inputs, giving you a half-adder.  At 5V, typical
propagation delay is rated at 30ns, 60ns max; at 3V they’d be even
slower, probably 50ns typ, 100ns max.  So you can only use them to XOR
at about 10 MHz, though they can pass signals up to 20 MHz on their
pass transistors.

These muxes also have an inhibit input, so you can gang them together
to make bigger LUTs, if you can negate the input that goes to inhibit,
and tolerate the microsecond delays on the inhibit propagation.
They’re rated for up to 10mA, which is a lot, and only 125Ω.

The 74HC164 has an asynchronous reset but no asynchronous preset, so
it might be a good idea to use an XNOR-type LFSR, so that all zeroes
is a valid state instead of a hang.  The CD4051/2 can do XNOR as
easily as XOR.  At 4.5V and 25° the ’164 is rated for 30MHz, so it
wouldn’t be the speed bottleneck in the LFSR.

For filtering the outputs, I think you want two levels of RC
filtering, probably with C0G/NP0 capacitors.  A single level of RC
filtering still leaves your waveforms with sharp corners on them
rather than parabolas, and those will result in kinks in your
letterforms; such kinks are visible above.  Also, if the RC constants
are the same, a single level of filtering means that in between the
kinks you get straight lines.

How much do RC filters cost?  One of JLCPCB’s “basic” parts is the
4D02WGJ0102TCE pack of four 5% 1kΩ resistors for 0.34¢ in an 0804 (?)
package; an 0402 capacitor of any type is 0.1¢.

So here’s a draft BOM for the ten-dimensional 5MHz noise generator:

    | part               | qty | price  | total  |
    | C0G/NP0 filter cap |  20 | 0.1¢   | 2¢     |
    | 4-resistor pack    |   5 | 0.34¢  | 1.7¢   |
    | 74HC164            |   2 | 9.04¢  | 18.08¢ |
    | CD4051             |   1 | 15.01¢ | 15.01¢ |
    | solder joints      | 110 | 0.15¢  | 16.5¢  |
    |--------------------+-----+--------+--------|
    | total              |     |        | 53.29¢ |

This doesn’t account for the clock oscillator or the reset circuit.

Discrete 0402 resistors are only 0.05¢, cheaper than the resistor pack
(just bigger), and ±1% instead of ±5%.  This is plausibly a case where
you’d want the ±1%.

Incidentally, you could use the third selector input on the CD4051 as
a delayed disable: if, instead of outputting an XNOR output, it just
outputs zeroes, then within a few clock cycles all the noise outputs
become quiescent.  You could probably use the inhibit input for this,
too, if you need three inputs for your XNOR.  (There are two-tap
maximal LFSRs for 2–7 bits, as well as many longer registers such as
15 bits, period 32767.  But 8 bits requires 4 taps.)

But maybe the asynchronous master reset on the shift register is a
better way to silence it when desired.

The LFSR will work down to DC, but the RC filtering won’t; you need to
pick capacitors for the frequency you expect to run at.  You probably
want a time constant of about two cycles of your clock; at 5MHz and
1kΩ this would be 400ns and 400pF, so it might be wise to use a lower
resistance in order to get a more precise capacitance, though maybe
the 74HC164 will have a hard time sourcing much more current.  Or you
could back off on the frequency thing a bit and go for 500kHz!

5MHz divided by a period of 15 is 333kHz, which is how many times per
second the system would redraw a complete glyph if you weren’t
fiddling with its biases.  If your screen refresh rate is 15Hz
(oscilloscopes have long persistence!) you get 22000 characters per
frame that way.  So another order of magnitude lower should be fine.

I haven’t tried projecting letterforms onto the space thus defined
yet.

In terms of summing a bunch of voltages together with analog
multiplications, the thing that occurred to me was that you could make
programmable current sinks by grounding a couple transistors’ emitters
through resistors, applying the input voltages on the bases, tying the
collectors together at a summing junction, and pulling up the
collectors through a common resistor.  As long as the base voltage is
high enough, the emitter voltage will always be 0.6V below whatever
the momentary base voltage is, which will create a proportional
current through the emitter resistor, mostly drawn from the collector.
The collector currents at the summing junction are all drawn through
the common resistor, which converts the current back to a voltage.
The emitter resistor sets the per-input gain.  So each such summing
junction has a (biased, negated) weighted sum of the basis signal
voltages applied to the bases.

This unfortunately requires one npn and one precision resistor with a
weird custom value per term, so a sparse approximation is highly
desirable.  But an MMBT3904 npn is 0.87¢ as part of “basic” parts, so
you can use quite a few.  Its β at 10mA (collector current) is
supposed to be 100 minimum, 300 typical, and 40 at 100μA, and it can
reach past 5 MHz no problem; supposedly its “current gain-bandwidth
product” is 300 MHz.

In simulation [that does seem to work][0], at least when I add an
extra collector resistor on each transistor to stay out of the
nonlinear zone:

![(schematic of summing amplifier)](programmable-sum.png)

[0]: http://tinyurl.com/yt3g8q7o

    $ 1 0.000005 21.593987231061412 50 5 50 5e-11
    R 560 352 512 352 0 1 40 0.5 2.5 0 0.5
    R 560 160 512 160 0 1 400 0.5 2.5 0 0.5
    t 560 160 688 160 0 1 -0.3902097154364519 0.5728099347486197 100 default
    174 752 144 736 192 1 10000 0.42080000000000006 Resistance
    w 688 176 736 176 0
    g 752 208 752 240 0 0
    g 752 400 752 432 0 0
    w 688 368 736 368 0
    174 752 336 736 384 1 10000 0.7673000000000001 Resistance
    t 560 352 688 352 0 1 -0.6026466932854126 0.5891379566166153 100 default
    w 688 336 688 288 0
    w 688 144 688 96 0
    w 880 96 880 288 0
    r 880 96 880 0 0 1000
    R 880 0 880 -48 0 0 40 5 0 0 0.5
    403 928 0 1200 176 0 13_32_0_x81096_5_0.1_-1_2_0.2_80_13_3_0.025_0
    403 208 0 464 192 0 1_32_0_x81096_5_0.1_-1_2_1.25_0_1_3_0.025_0
    403 208 256 464 448 0 0_32_0_x81096_5_0.1_-1_2_1.25_0_0_3_0.025_0
    r 880 96 688 96 0 1000
    r 880 288 688 288 0 1000

Maybe a better way to solve this problem would be to sum
programmable-gain amplifiers whose gain can be programmed not by
changing resistors but by changing input voltages.

For character repertoires of more than a dozen or so characters, it
might be desirable to overstrike several strokes one after the other
at the same character position, somewhat as with 14-segment displays;
in this way we can greatly reduce the number of distinct shapes that
must be approximated with (sparse) combinations of basis vectors.
