Compressed data can usually only be decompressed sequentially, because
the decompression of each new element (backreference or whatever)
relies on the preceding context.  The need for random-access reads
sometimes militates against compressing data in stable storage such as
disks and Flash, even when the CPU time to do the compression and
decompression wouldn’t be a problem.  (On conventional filesystems,
random-access writes are even less compatible with compressed data,
since the size of the new compressed data is unpredictable.)  This, in
turn, tempts us to design our data formats to be compact even at the
cost of other merits such as simplicity, generality, compatibility,
efficiency, and clarity.

If you split the data to be compressed into chunks that are compressed
independently and therefore decompressible independently, you can get
random access, at some cost to compression.  Typically around a
megabyte is a good chunk size; with gzip, a megabyte takes about 150ms
to compress and 10ms to decompress on current cellphone CPUs and only
costs you a few percent in compression.  zstd can be much faster,
usually exceeding Flash SSD bandwidth on a single core.

Many compression file formats, including gzip and zstd, have a sort of
homomorphism† for concatenation: gunzip(gzip(a) || gzip(b)) == a || b.
This means that you can concatenate these independently compressed
chunks and still get a decompressible file, so as long as you can
figure out where a chunk begins, you can seek to it and start reading.
So if you put a chunk dictionary somewhere you have seekable
compressed data.

The dictzip(1) man page explains the dictd implementation of this
approach, with parameters optimized for mid-01990s computers:

> `dictzip` compresses files using the `gzip`(1) algorithm (LZ77) in a
> manner which is completely compatible with the `gzip` file format.
> An extension to the `gzip` file format (Extra Field, described in
> 2.3.1.1 of RFC 1952) allows extra data to be stored in the header of
> a compressed file.  Programs like `gzip` and `zcat` will ignore this
> extra data.  However, `dictd`(8), the DICT protocol dictionary
> server[,] will make use of this data to perform pseudo-random access
> on the file.
>
> (...)
>
> The dictzip program uses ‘R’ for [the first type byte] SI1, and ‘A’
> for SI2 (i.e., “Random Access”).  After the LEN field, the data is
> arranged as follows:
>
>       +---+---+---+---+---+---+===============================+
>       |  VER  | CHLEN | CHCNT |  ... CHCNT words of data ...  |
>       +---+---+---+---+---+---+===============================+
>
> As per RFC 1952, all data is stored least-significant byte first.
> For VER 1 of the data, all values are 16-bits long (2 bytes), and
> are unsigned integers.
>
> XLEN (which is specified earlier in the header) is a two byte
> integer, so the extra field can be 0xffff bytes long, 2 bytes of
> which are used for the subfield ID (SI1 and SI1), and 2 bytes of
> which are used for the subfield length (LEN).  This leaves 0xfffb
> bytes (0x7ffd 2-byte entries or 0x3ffe 4-byte entries).  Given that
> the zip output buffer must be 10% + 12 bytes larger than the input
> buffer, we can store 58969 bytes per entry, or about 1.8GB if the
> 2-byte entries are used.  If this becomes a limiting factor, another
> format version can be selected and defined for 4-byte entries.
>
> For compression, the file is divided up into “chunks” of data, each
> chunk is less than 64kB, and can be compressed into an area that is
> also less than 64kB long (taking incompressible data into
> account — usually the data is compressed into a block that is much
> smaller than the original).  The CHLEN field specifies the length of
> a “chunk” of data.  The CHCNT field specifies how many chunks are
> [present], and the CHCNT words of data specifies how long each chunk
> is after compression (*i.e.*, in the current compressed file).

Aside from the rather small limitation on chunk size, because the
chunk data is in the gzip file header, it must be known at file
creation time; you cannot append more chunks to it later without
moving all the rest of the data in the file, which is an operation
conventional filesystems cannot do efficiently.

There is a similar extension for the zstd file format.

The common PKZIP file format, however, can contain many independently
compressed data “files” (each with a name, permissions, etc.) in a
single zipfile.  A “central directory” at the end of the file lists
all of the “files” to permit random access to them; it is possible to
append more data “files” later by appending a new central directory.
If I’m reading APPNOTE.TXT correctly, each directory entry is 46 byte
plus the filename, any file comments, etc.  ZIP supports gzip
compression since pkz204g (“deflate”) and also recently added support
for zstd.

So, I think zipfiles make a good format for a compressed data stream
in which each chunk is randomly readable and new chunks can be
appended.  Each compressed chunk is just represented as a separate
“file” in the zipfile.  This permits access to the data using standard
tools, including efficient random-access reads, without giving up much
compression.  The chunk filenames may be useful as index keys or may
not be.

----

† Not homeomorphism; that’s the topological deformation thing where
one space is another space wearing a trenchcoat.  I always have to
look this up.
