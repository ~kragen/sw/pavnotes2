I was updating [my résumé][0] and thought it would be useful to list
recent personal projects.  But what are they?  Maybe I should put some
of them on GitLab.

[0]: http://canonical.org/~kragen/resume

Overview
--------

I’ve been working on a project tentatively called the “book in
itself”.

Thanks to the revolutionary work of people like Doug Engelbart and
Alan Kay, computing systems are pervasive in modern society.  But
existing computing systems embody complexity beyond the comprehension
of any single person.  As a result, we often accept them as givens
beyond our control, like the sun and the seas, bending over backwards
to adapt ourselves to them instead of adapting *them* to *our* needs.
Thus we can only make *evolutionary* change to these inscrutable
monstrosities rather than the *revolutionary* changes that are needed.
Consequently, Engelbart’s revolution still remains unfinished.

I’ve been searching for ways to reduce that complexity to a
comprehensible level without sacrificing functionality.  With Alan
Kay, I believe that doing so is necessary for computing systems to be
media for individual creative expression.  This freedom enables
experimentation with new ways of structuring the systems and new forms
of human–machine interaction.

Engelbart’s vision was somewhat different: he envisioned a coevolution
between tools and users, in which both adapted themselves to *the
system* designed to increase the *collective* capacity of a community
following a collective plan.  My own orientation is toward increasing
*individual* capability with which each individual can realize their
unique potential, enabling voluntary contributions which benefit the
entire community.

### January 02024

- [Smolhershey][1], a tiny C library for rendering scalable vector
  fonts on microcontrollers (img!)
- [Straightpin][2], a tiny Python3 library for PEG parsing
- [Reverse-engineering Douglas Hofstadter’s gridfonts file format][3]
  (img!)
- [Translating disassembled IMLAC PDS-1D fonts into Hershey fonts][4]
  (img!)
- [r3cts][5], a linear-time table-layout constraint solver for
  microcontrollers as a tiny header-only C library (could have img but
  doesn’t yet)
- [A fireworks simulator][6] in JS. (img!)

[1]: http://canonical.org/~kragen/dev3/smolhershey.md.html
[2]: http://canonical.org/~kragen/dev3/straightpin.py
[3]: http://canonical.org/~kragen/dev3/gridfontparse.py
[4]: http://canonical.org/~kragen/dev3/imlacparse.py
[5]: http://canonical.org/~kragen/dev3/r3cts.h
[6]: http://canonical.org/~kragen/dev3/fireworks.html

### December 02023

- [A watchlighting clock][8] in JS. (img!)

[8]: http://canonical.org/~kragen/dev3/clock.html

### November 02023

- [An IFS fractal editor][11] in DHTML (img!)

[9]: http://canonical.org/~kragen/dev3/gyap
[10]: http://canonical.org/~kragen/dev3/rgyap
[11]: http://canonical.org/~kragen/dev3/ifs.html

### October 02023

- [gyap][9] and [rgyap][10], an ultra-simple text chat system as Unix
  shell scripts
- [hcurve][13], a morphic word that generates Niedermeier, Reinhardt,
  and Sander’s H-indexing, which is an alternative to the Hilbert
  curve with better locality. (img!)
- [Wing IFS][14] in ASCII art, an iterated function system fractal
  animation using affine maps on complex numbers instead of vectors,
  with variants in Lua, [obfuscated Perl][15], and [obfuscated][16]
  [Python][17]. (img!)
- [utra][18], a combinator parser library for PEG parsing in Lua
- [ARM Unicode tetris][19], an implementation of the game Tetris for a
  Unicode terminal emulator in under 1 kibibyte of ARM code (img!)
- [testsort][20], a tutorial example of how to write a property-based
  unit test for a sort function in Python with Hypothesis
- [Convolution with pseudorandom time-domain sequences for
  spread-spectrum communications][21], a DSP notebook about a
  prototype alternative to time-hopping for ultra-low-power
  time-domain radio (img!)

[13]: http://canonical.org/~kragen/dev3/hcurvestream.py
[14]: http://canonical.org/~kragen/dev3/wing.lua
[15]: http://canonical.org/~kragen/dev3/wing.pl
[16]: http://canonical.org/~kragen/dev3/cifs.py
[17]: http://canonical.org/~kragen/dev3/cifsd.py
[18]: http://canonical.org/~kragen/dev3/microtrans.lua
[19]: http://canonical.org/~kragen/dev3/tetris.S
[20]: http://canonical.org/~kragen/dev3/testsort.py

### September 02023 ###

- [Demodulating a Sord M5 1750Hz/3500Hz variant of the Kansas City
  Standard][22], a notebook of DSP successfully recovering historical
  video game data from digitized 40-year-old cassette tapes for an
  obsolete Japanese home computer

[21]: https://nbviewer.org/url/canonical.org/~kragen/sw/dev3/random-truncated-correlation.ipynb
[22]: https://nbviewer.org/url/canonical.org/~kragen/sw/dev3/kansas-city-demodulation.ipynb

### August 02023 ###

- [Dumbjit][12], a prototype JIT compiler from a stack bytecode to ARM
  Thumb machine code, targeting microcontrollers
- [Mapfield][23], an implementation of ARM-assembler-style “map
  fields” (basically structs) in macros for the GNU assembler

[12]: http://canonical.org/~kragen/dev3/dumbjit.c
[23]: http://canonical.org/~kragen/dev3/mapfield.S

### July 02023 ###

- [Einkornix][24] and [Monokokko][25], two minimalist green threads
  packages in ARM assembly, sometimes called “real-time operating
  systems”.
- [Finding a counterexample to the assertion that selection sort is
  stable][26] using DRMacIver's Hypothesis.

[24]: http://canonical.org/~kragen/dev3/einkornix.h
[25]: http://canonical.org/~kragen/dev3/monokokko.S
[26]: http://canonical.org/~kragen/dev3/hypostable.py

### June 02023 ###

- [emjshi][27], a test program for drawing on an HTML `<canvas>` from Wasm
  in C
- [Text editor for a one-line buffer][28] in 838 bytes of AVR code.

[27]: http://canonical.org/~kragen/dev3/emjshi.c
[28]: http://canonical.org/~kragen/dev3/oleditress.c

### April 02023 ###

- [A prototype of a new, cheap way to detect frequencies
  efficiently][29] on microcontrollers without on-chip multipliers by
  approximating the frequencies as general rational fractions of the
  sampling rate

[29]: https://nbviewer.org/url/canonical.org/~kragen/sw/dev3/rational-comb-filter-test.ipynb

### September 02022 ###

- An implementation of the fast Walsh–Hadamard transform [over the
  finite field GF(251)][30]
- [Edumb][33], an extremely minimal terminal text editor in Python
- [An LFSR in RISC-V assembly language][34].

[30]: http://canonical.org/~kragen/dev3/fwht.c
[34]: http://canonical.org/~kragen/dev3/lfsr-riscv.s
[33]: http://canonical.org/~kragen/dev3/edumb.py

### August 02022 ###

- [zhegalkin.py][31], a SAT solver and logic minimizer as an EDSL in
  Python using Zhegalkin polynomials
  
[31]: http://canonical.org/~kragen/dev3/zhegalkin.py

### July 02022 ###

- A [Doom-style raycasting demo][32] in JS with `<canvas>`

[32]: http://canonical.org/~kragen/dev3/raycast.html

### April 02022 ###

- [trama.html][35], a compiler in JavaScript targeting JavaScript from
  a language for livecoding bitmap animations with RPN bitwise
  operations

[35]: http://canonical.org/~kragen/dev3/raycast.html

### February 02022 ###

- [devtt.py][36], a tool for converting VTT subtitle files from
  YouTube into plain text suitable for reading

[36]: http://canonical.org/~kragen/dev3/devtt.py

### October 02021 ###

- [syllabize.py][37], a prototype-quality text-to-IPA-phonemes
  translator for Spanish statistical computational phonotactics or
  speech-to-text

[37]: http://canonical.org/~kragen/dev3/syllabize.py

### September 02021 ###

- [Qfitzah][38], a prototype interpreter for an object-oriented
  language with ad-hoc polymorphism with multiple dispatch and
  Prolog-like pattern-matching in a 1-kilobyte executable, using the
  term-rewriting paradigm

[38]: http://canonical.org/~kragen/dev3/qfitzah.s

### April 02021 ###

- [Skitch][39], an interactive grammar-based fractal generator in HTML
  and JavaScript
- [alglayout.ml][40] and [alglayout.py][41], an embedded
  domain-specific language for text layout in OCaml and Python
- [Meta5ix][42], a minimal parser generator based on META-II, which
  can bootstrap itself from 18 lines of code

[39]: http://canonical.org/~kragen/dev3/skitch.html
[40]: http://canonical.org/~kragen/dev3/alglayout.ml
[41]: http://canonical.org/~kragen/dev3/alglayout.py
[42]: http://canonical.org/~kragen/dev3/meta5ixrun.py

### March 02021 ###

- [qj2d][43], a microframework/micro-game engine in JavaScript used to
  write a [Space Invaders game called qvaders][44], an [amusing
  ecological simulation called Qabbits][45], and a [graphics
  demo][46].
- [μkanren.ml][47], a port of the relational programming language
  microKanren to an OCaml embedded domain-specific language
- [kmregion][48], an open-source header-only C library for
  pointer-bumping “region” memory allocation; it is about 10 times
  faster than `malloc` and `free`
- [sqlitescene][49], a (sketch of a) 3-D software rasterizer in
  SQLite, just because no reasonable person would do this and SQLite's
  dialect of SQL isn’t even Turing-complete
- [yahoogold][50], an open-source screen scraper to get historical (?)
  commodities prices from Yahoo Finance

[43]: http://canonical.org/~kragen/dev3/qj2d.js
[50]: http://canonical.org/~kragen/dev3/yahoogold.py
[49]: http://canonical.org/~kragen/dev3/sqlitescene.py
[48]: http://canonical.org/~kragen/dev3/kmregion.h
[47]: http://canonical.org/~kragen/dev3/mukanren.ml
[46]: http://canonical.org/~kragen/dev3/offscreen.html
[45]: http://canonical.org/~kragen/dev3/qabbits.html
[44]: http://canonical.org/~kragen/dev3/qvaders.html

### 
