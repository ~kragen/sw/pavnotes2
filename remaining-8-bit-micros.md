I was looking at compiling
[Smolhershey](http://canonical.org/~kragen/sw/dev3/smolhershey.md.html)
for tiny microcontrollers, and it occurred to me to wonder what a
“tiny microcontroller architecture” even is these days.  The answer
seems to be that it’s mostly AVR, PIC, and ARM.  PIC support in free
software is not good.

Families with most varied models: one crude measure of popularity
-----------------------------------------------------------------

I thought I’d check some popular 8-bit CPU families at Digi-Key to see
what was popular these days.  I selected “in stock” and “exclude
marketplace” on their [microcontrollers faceted browser][0], then
picked particular items from the “core processor” menu.  I was
surprised at the results; I thought that ARM would clearly dominate,
but there are actually more PICs than anything else.  The AVR remains
popular, but also the RL78 is surprisingly strong.  The 8051
apparently has more staying power than others of its ilk.

- overall: 18998

Popular families:

- PIC: 4956
- dsPIC: 991, bringing the PIC total to 5947, nearly a third of all
  microcontroller models
- ARM7: 106
- Cortex-M0: 478
- Cortex-M0+, including variants with M7, M7F, or M4F to provide
  gronk: 985
- Cortex-M23: 226
- Cortex-M3: 790
- Cortex-M33: 241
- Cortex-M4, M4F, and variants with things like M0+ or M0: 1576
- Cortex-M7: 303
- Cortex-R4 and R4F: 37; with the non-exhaustive previous ARM
  enumeration, this adds up to 4742.
- RL78: 1799
- AVR: 1406
- RX, RXv2, RXv3: 1202

Lesser-used families:

- all MSP430 together: 946
- 80C51: 59
- 8051: 402
- 8052: 45
- CIP-51 8051: 112
- CIP-51: 6
- MCS 51: 17; with the previous five this works out to 639
- all Coldfire variants together: 55
- MIPS32 M4K: 309
- STM8, STM8A: 164

Moribund families:

- HC08: 20
- HC11: 1
- HCS12: 27
- HCS12X: 29
- M16C/60: 18
- M8C: 80
- MAXQ20/MAXQ20S: 4
- SH-4: 2
- TMS320: 5
- eZ80: 9

[0]: https://www.digikey.com/en/products/filter/microcontrollers/685

This is of course somewhat imperfect (it’s biased towards cores
available from more manufacturers and made in more minor variants, and
I’ve clearly overlooked important things) but it’s still highly
suggestive of consolidation.

The thing that really surprised me was how dead the 68HC08, 68HC11,
and 68HC12 are.  The 8051 is still kind of holding steady.  Also, it
surprised me to see so many RL78s, which I think of as NEC’s redesign
of the 8086 with the benefit of hindsight (originally “78K”,
introduced in 01986, of which Digi-Key has 5 models in stock), and so
many PICs, which I think of as the design error that AVR rescued us
from.

You can get RL78s (now from Renesas) for like 60¢ in quantity or less,
comparable to an AVR in price and I/Os and power consumption, but
16-bit instead of 8.  Given their apparent popularity, it’s surprising
to not see *any* compatible 8086 chips.

Given TI’s historical hostility to tinkering, it’s sort of nice to see
their fortunes, in some sense, waning, as the TMS320 and MSP430 are
not as popular as they used to be.  At least 100 of the ARMs are
theirs, though.

I’m not familiar with this RX thing.

As for the STM8, ST has made the STM8 “not recommended for new
designs,” which may in part account for their declining popularity.

Most popular individual models: an opposite crude measure of popularity
-----------------------------------------------------------------------

A way to correct the bias of the above search toward more minor
variants is to look at the particular microcontrollers which Digi-Key
has the largest stock of, which hopefully represents some kind of
guess about their popularity.  The prices here are in quantity 1.

- ATTINY13A-SSU: 317,499 in stock, US$0.75, AVR
- ATSAMD20E16A-MNT: 284,718, US$2.88, ARM® Cortex®-M0+
- STM32F030R8T6: 190,711, US$2.86, ARM® Cortex®-M0
- PIC18F46K22T-I/PT: 162,445, US$3.94, PIC
- PIC12F1822-I/SN: 138,785, US$1.29, PIC
- ATMEGA328P-AUR: 133,428, US$2.74, AVR
- Z8F6421AN020SG: 124,153, US$9.11, eZ8 (from Zilog)
- ATTINY84A-SSFR: 121,726, US$1.08, AVR
- PIC18F46K22-I/P: 118,505, US$4.34, PIC
- PIC18F13K22T-I/SS: 105,537, US$2.14, PIC
- PIC16F506-I/SL: 104,076, US$0.92, PIC
- MSP430F249TPMR: 103,939, US$9.47, MSP430 CPU16
- ATTINY84A-SSUR: 103,636, US$1.08, AVR
- STM32G030K8T6: 103,489, US$1.96, ARM® Cortex®-M0+
- PIC10F200T-I/OT: 101,360, US$0.58, PIC
- ATTINY24A-SSU: 101,210, US$0.90, AVR
- ATMEGA8L-8AU: 99,104, US$3.09, AVR
- ATMEGA328P-PU: 97,405, US$2.89, AVR
- ATTINY4-TSHR: 97,362, US$0.44, AVR
- STM32G030F6P6: 93,862, US$1.61, ARM® Cortex®-M0+
- C8051F860-C-GSR: 93,548, US$2.81, 8051 (from Silicon Labs)
- PIC10F222T-I/OT: 91,822, US$0.69, PIC
- PIC12F629-I/P: 89,876, US$1.41, PIC
- ATTINY1616-MFR: 85,992, US$0.92, AVR
- ATTINY416-MFR: 83,059, US$0.77, AVR

So the top 25 individual chips by stock count are 10 AVRs, 8 PICs
(mostly PIC18s), 4 ARMs, an MSP430, an eZ8, and an 8051.  There’s a
tendency for the ARMs to be toward the higher end; the cheapest ARM is
US$1.61, and 12 of these “most popular” 25 micros are cheaper than
that.

Perhaps the RL78 and RX showed up so strongly in the earlier list
because of greater specialization in Renesas’s product lines rather
than great popularity?

Another bias is that Digi-Key’s coverage of the most important Chinese
brands like GigaDevice and WCH is poor.  They have literally zero pure
RISC-V microcontrollers.

Renesas RX
----------

RX turns out to be a 32-bit thing, also from Renesas, starting at
about US$1.50.  A representative might be the US$3
[R5F51115ADFL#1A][1], which Digi-Key describe as “RX RX111
Microcontroller IC 32-Bit Single-Core 32MHz 128KB (128K x 8) FLASH
48-LQFP (7x7)”.  The datasheet for the RX111 family describes them as,
“32 MHz 32-bit RX MCUs, 50 DMIPS, up to 512 Kbytes of flash memory,
USB 2.0 full-speed host/function/OTG, up to 6 comms channels, 12-bit
A/D, 8-bit D/A, RTC.”  In more detail it describes the CPU, which
sounds RISCier than it really is at this level of detail:

> - Maximum operating frequency: 32 MHz
> - 32-bit RX CPU
> - Minimum instruction execution time: One instruction per clock cycle
> - Address space: 4-Gbyte linear
> - Register set
>     - General purpose: Sixteen 32-bit registers
>     - Control: Eight 32-bit registers
>     - Accumulator: One 64-bit register
> - Basic instructions: 73
> - DSP instructions: 9
> - Addressing modes: 10
> - Data arrangement
>     - Instructions: Little endian
>     - Data: Selectable as little endian or big endian
> - On-chip 32-bit multiplier: 32-bit × 32-bit → 64-bit
> - On-chip divider: 32-bit ÷ 32-bit → 32 bits
> - Barrel shifter: 32 bits

R0 is the stack pointer, and interrupts swap stacks.  The
“accumulator” is for 64-bit multiply-accumulate operations.  I/O is
memory-mapped.  Like the 68k and 88k, operand sizes are written in
assembly language with suffixes: mov.b, mov.ub, mov.w, mov.uw, mov.l.
(Perhaps in the instruction encoding too there are bits for this,
rather than unrelated opcodes.)  There’s a separate [instruction set
manual][6].  The instruction encoding is variable-length (1–8 bytes,
with plenty of 3-byte instructions).  It puts arithmetic flags in the
PSW: carry, zero, sign, overflow.  PSW writes in user mode ignore some
bits, complicating virtualization.

Unusual instructions include ABS (absolute value), SAT (saturate),
MAX/MIN (maximum/minimum of two signed integers), BCLR/BNOT/BSET/BTEST
(clear/invert/set/test a bit), BMCnd (conditional bit transfer), SSTR
(string store), SUNTIL (string search until equal), STZ (store on
zero), SMOVB (string move backwards), SMOVF (string move forwards),
PUSHM/POPM (push/pop multiple registers with stack), RMPA (repeat
multiply-accumulate).

There are ten “addressing modes”, though I’d only call 6 of them
addressing modes:

- Immediate
- Register direct
- Register indirect
- Register relative
- Post-increment register indirect
- Pre-decrement register indirect
- Indexed register indirect
- Control register direct
- PSW direct
- Program counter relative

“Register direct” operates on the contents of a register; only seven
of these modes access memory.  “Program counter relative” is only used
for jumps.

Some of the instructions, like NEG and ABS, are only
register-to-register, but many are not.  Things like ADD and AND can
directly load from memory, but can only write to a register.  The bit
manipulation instructions read and write memory in a single
instruction; so do some variants of MOV.  The BSR subroutine call
instruction pushes onto the stack.

So it’s not very RISCy.  It looks like Renesas thought, “What if we
did the 80386 right?”  Wikipedia says it’s from 02009.

PUSHM/POPM are less useful than the ARM versions because they can only
handle a contiguous register range and cannot pop PC.

RMPA is an integer vector dot product instruction; you set the vector
length of 65536 or less in R3, pointers in R1 and R2, and the initial
80-bit (!) bias value in R6:R5:R4, and it goes to town with the number
of multiply-accumulates specified in R3, incrementing R1 and R2.

The string instructions (SMOV, etc.) also take a count in R3.  SMOVU
is like SMOV but will stop after the first NUL, like REPNZ MOVSB.
SUNTIL searches for a given byte, short, or long in an array.

The SAT operation looks at the overflow and sign flags, and if a
signed overflow has occurred in either direction, sets the destination
register to the minimum or maximum 32-bit signed integer.

[1]: https://www.digikey.com/en/products/detail/renesas-electronics-corporation/R5F51115ADFL-1A/12750565
[6]: https://www.renesas.com/us/en/document/mas/rx-family-rxv1-instruction-set-architecture-users-manual-software-rev130

Free-software compiler support for various families
---------------------------------------------------

Free-software AVR support is of course first-class.

sdcc was originally written for the 8051, has some support for the
PIC14 and PIC16, and has apparently good support for the STM8.
[Overall, the PIC situation is abysmal][4].

There’s a [comparison of STM8 compilers][17], comparing SDCC with
three proprietary compilers.

There’s an [mspgcc/gcc-msp430][2] package in Debian, with a [guide to
how to use it][3] and apparently GDB support; TI says, "[The MSP430
GCC compiler is owned by TI and maintained by Mitto Systems since 2018
(it was previously maintained by Somnium and Red Hat).][5]".

RX is [supported in mainline GCC][7], but there’s also [a separate
fork of GCC 8.3.0][8].  And they also have [LLVM support for RL78][9].
[FreeRTOS has a build for RX][10].  Renesas also has their own
[officially supported proprietary compiler][11] I think, plus
[something for Eclipse][12].

[2]: https://packages.debian.org/sid/gcc-msp430
[3]: https://github.com/msp430club/msp430-linux-guide
[4]: http://www.picprojects.net/piccompiler/
[5]: https://www.ti.com/tool/MSP430-GCC-OPENSOURCE
[7]: https://gcc.gnu.org/onlinedocs/gcc/RX-Options.html
[8]: https://llvm-gcc-renesas.com/rx/rx-latest-source-code/
[9]: https://llvm-gcc-renesas.com/llvm-for-renesas-rl78-10-0-0-202312-has-been-released/
[10]: https://github.com/GerryFerdinandus/Renesas-RX-GCC
[11]: https://www.renesas.com/us/en/software-tool/cc-compiler-package-rx-family
[12]: https://www.renesas.com/us/en/software-tool/e2studio-information-rx-family
[17]: http://colecovision.eu/stm8/compilers.shtml

[Hiramatsu Kunihito explains][13]:

> The former KPIT support for the GNU toolchain has been replaced by
> Open Source Tools for Renesas has added a new GNU tools. (...)
>
> The tool can be downloaded by anyone who registers, and there is no
> binary limit. It also seems to have optimizations in RX
> microcomputers and support for the latest cores. Since gcc is based
> on 8.3.0, it supports C++17. It has deeper optimizations than normal
> gcc and supports the latest CPU cores. All projects using the C++
> framework published here are compileable.
> 
> They are also providing support.（CyberTHOR Studios Limited） (...)
>
> Features in Renesas GNU-RX 8.3.0：
>
> - RXv3 Core Support
> - Generation of double-precision floating-point instructions
> - RX72N Built-in trigonometric function unit (TFU) support

[I'm assuming that’s in contrast to mainline GCC?]

> The difference between CC-RX: In terms of compiler optimization,
> CC-RX seems to be superior to GNU-RX in the CoreMark benchmark.
> It's hard to imagine why such a big difference would occur, but if
> you know the internal structure of the CPU, you might be able to
> achieve it...

[13]: https://github.com/hirakuni45/RX

Zilog Z8
--------

Though the Z8, introduced in 01979 (with [the eZ8 released in
02002][15]) is more expensive than the other popular microcontrollers,
I thought it would be worthwhile to see what support was like.
There’s a [dead “dbgutils” project for it][14] and, that’s about it?
Supposedly Zilog had [their own gratis compiler][16] for “Windows
95/98/NT” (on the DOS command line) but I can’t find it now and don’t
know if it would work under WINE.  And it probably only supports C89.

[14]: https://github.com/jessexm/dbgutils
[16]: http://www.zilog.com/docs/devtools/um0028.pdf
[15]: https://www.eetimes.com/microcontrollers-microprocessors-out-in-force-at-embedded-systems/

Best prices at different ROM sizes
----------------------------------

LCSC has [the Padauk microcontrollers][24], which Digi-Key still
doesn’t.  The cheapest one they have in stock is the 8-pin OTP
[PMS150G-S08][25] at 5.53¢ in quantity 100, going down to 3.75¢ in
quantity 10000.  The [datasheet they link][26] is in Chinese, and it
says it runs at 2MHz, has “1KW” (presumably 1KiB) of OTP PROM, and 64
bytes of RAM.  If you go up to the 13¢ (qty 100) OTP [PMS134-S16A][27]
you get “4KB” of OTP PROM and 256 bytes of RAM and, [the Chinese
datasheet][28] touts, a 12-bit ADC.  The datasheet describes this as
“4KW”, 256 bytes, 22 GPIOs (impossible in a 16-SOIC), 13 ADC lines.
[Padauk’s own homepage for the microcontroller][29] links an [English
datasheet][30], which says it can run at 8MHz off its internal 16MHz
oscillator, and also shows an analog comparator, and 3 11-bit PWM
channels, and a hardware 8×8 multiplier.  And for basically the same
price, 14.61¢, [LCSC have the Flash PFC161-EY10][31], which has [2 KW
of Flash and 128 bytes of RAM][32] and isn’t OTP.  The [English
datasheet][33] gives more detail.

[24]: https://www.lcsc.com/products/Microcontroller-Units-MCUs-MPUs-SOCs_11329.html?brand=1246
[32]: http://www.padauk.com.tw/en/product/show.aspx?num=117&kind=42
[25]: https://www.lcsc.com/product-detail/Microcontroller-Units-MCUs-MPUs-SOCs_PADAUK-Tech-PMS150G-S08_C2909819.html
[26]: https://datasheet.lcsc.com/lcsc/2207211830_PADAUK-Tech-PMS150G-S08_C2909819.pdf
[27]: https://www.lcsc.com/product-detail/Microcontroller-Units-MCUs-MPUs-SOCs_PADAUK-Tech-PMS134-S16A_C317584.html
[28]: https://datasheet.lcsc.com/lcsc/1810251527_PADAUK-Tech-PMS134-S16A_C317584.pdf
[29]: http://www.padauk.com.tw/en/product/show.aspx?num=45
[30]: http://www.padauk.com.tw/upload/doc/PMS133,%20PMS134%20datasheet_EN_V106_20230616.pdf
[31]: https://www.lcsc.com/product-detail/Microcontroller-Units-MCUs-MPUs-SOCs_PADAUK-Tech-PFC161-EY10_C2857158.html
[33]: http://www.padauk.com.tw/upload/doc/PFC161%20datasheet_EN_V003_20230209...pdf

I rigged up a Digi-Key query for [the microcontrollers with apparently
good free-software support][18], selecting 11052 of the 18998 they
have in stock, and did some searches for different ROM sizes.  I’m
trying to get a sense of what you can get at different price points.

- 512 bytes: [6-pin ATTiny4][19], US$0.35 in quantity 100, US$0.44 in
  quantity 1
- 1024+ bytes:
    - Cypress(?) [16MHz 8KiB flash ARM Cortex-M0,
      CY8CMBR3145-LQXI][20], 33¢ in quantity 1, in a 16-QFN, with 1K
      RAM.  Discontinued, datasheet no longer available.
    - 6-pin [ATTiny10-TSHR][21], 35¢ in quantity 100, 12 MHz, 32 bytes
      RAM, 1KiB flash.  Too small for C, probably.
    - 8-pin [ATTiny202-SSNR][22] SOIC, 43¢ in quantity 100, 20MHz, 2
      KiB Flash, 128 bytes RAM, probably still too small for C.
    - 15-ball [ATTiny24A-CCU][23] UFBGA, US$1.38 in quantity 100,
      20MHz, 2 KiB Flash, 128 bytes RAM, likewise.  But this one is
      3mm×3mm.
    - 20-TSSOP Renesas RL78 [R5F11Y67ASM#50][34], 66¢ in quantity
      100, the first one I’ve found that’s not an AVR.  4KiB Flash,
      20MHz, 512 bytes of RAM, 16-bit operation (but probably still
      slower than an AVR).
- 8192+ bytes:
     - 14-SOIC [ATTiny804-SSF][35], 62¢ in quantity 100, 8KiB
       Flash, 128 bytes RAM, 16MHz.  [They’ve sped up the 10-bit ADC to
       115ksps and there is a hardware multiplier][36].
- 16384+ bytes:
    - 14-SOIC [ATTINY1604-SSNR][37], 64¢ (qty 100), 16KiB Flash, 1024
      bytes RAM, 20MHz.
    - 20-TSSOP Nuvoton NuMicro [M031FB0AE ARM Cortex-M0][38], 66.9¢
      (qty 100, but they don't have that much stock), 48MHz, 16KiB
      Flash, 2048 bytes RAM.  In quantity 5000+ this 3.3V chip is
      38.687¢.  It has a 2Msps 12-bit ADC which supports “16
      single-ended input channels or 8 differential input pairs”.  It
      says it uses 8.5mA = 28mW at 48MHz running CoreMark from Flash,
      which is a reasonable 580pJ/instruction, assuming 1 IPC.
- 32768+ bytes, like an ATMega328:
    - 14-SOIC [ATTiny3224-SSU][39], 81¢ (qty 100), 20MHz, 32KiB Flash,
      3KiB RAM.  In quantity 1 it’s 97¢, still far from the US$2.74
      they want for an otherwise similar ATMega328P.  They've beefed
      up the ADC further to 12 bits, differential, and 375 ksps.
- 65536+ bytes, like a CP/M machine:
    - 48QFN Atmel [ATSAM3N4AA-MU][40], 102¢ (qty 1–70), a discontinued
      48MHz ARM Cortex-M3 with 256KiB of Flash and 24KiB of RAM
    - 14-SOIC [AVR64DD14][41], 107¢ (qty 100), a 24MHz AVR with 64KiB
      of Flash and 8KiB of RAM
    - 16-SOT-23 (!) [MSPM0L1306SDYYR][42] from TI, 108¢ (qty 100), a
      32MHz Cortex-M0+ with 64KiB of Flash and 4KiB of RAM
- 131072+ bytes, like a Commodore 128:
    - [MSPM0G1106TRHBR][43], also from TI, 116¢, an 80MHz Cortex-M0+
      with 128KiB of Flash and 32KiB of RAM
    - 64-UFBGA [ATSAMD21J18A-CUT][44], a 48MHz Cortex-M0+, US$3.89,
      256KiB Flash and 32KiB of RAM

More than that, I don’t think it’s really that tiny anymore.

An interesting thing about this list is that it’s almost all AVR, with
a few RL78s and ARMs as you get toward the top.

[18]: https://www.digikey.com/en/products/filter/microcontrollers/685?s=N4IgjCBcoKwAwDYqgMZQGYEMA2BnApgDQgD2UA2iAMxgDsYATDCMTQCwCcDVL1YnbWr3Yc2EVvw4xmEzlQAcvBBypsGDYf3kM4S%2BR3m0Oeg-N2z58nhfkyQgqh1p2Ethgg3F6tOGpOGhYldTcXs2bRhQ9ktjCw5zPnDRTSS7aKcU-UVZeIT02MT40ODLT0K4MvopOGty2qMqKhg2FPiWnLg7H1p9JA6%2BkFcYBE49YfgxhEjhWh6wULY4SzgExeXaxxV5dvsYKmc0jlF1YScGOl4GeWUdS%2BuubOoGDmVAp5eDYT24Iy%2BaBjeTX%2BBX2bCaO1UMHkYB4AF1iAAHAAuUBAAGUkQAnACWADsAOYgAC%2BxAAtMZoCA0JAsQBXIikCjgECwkkgUkaSnUukMsiQSjMVlEolAA
[19]: https://www.digikey.com/en/products/detail/microchip-technology/ATTINY4-TSHR/2238292
[20]: https://www.digikey.com/en/products/detail/infineon-technologies/CY8CMBR3145-LQXI/7717069
[21]: https://www.digikey.com/en/products/detail/microchip-technology/ATTINY10-TSHR/2051008
[22]: https://www.digikey.com/en/products/detail/microchip-technology/ATTINY202-SSNR/9554944
[23]: https://www.digikey.com/en/products/detail/microchip-technology/ATTINY24A-CCU/2522793
[34]: https://www.digikey.com/en/products/detail/renesas-electronics-corporation/R5F11Y67ASM-50/18164521
[35]: https://www.digikey.com/en/products/detail/microchip-technology/ATTINY804-SSF/10270321
[36]: https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny804-06-07-1604-06-07-DataSheet-DS40002312A.pdf
[37]: https://www.digikey.com/en/products/detail/microchip-technology/ATTINY1604-SSNR/10270297
[38]: https://www.digikey.com/en/products/detail/nuvoton-technology-corporation/M031FB0AE/16399561
[39]: https://www.digikey.com/en/products/detail/microchip-technology/ATTINY3224-SSU/15520488
[40]: https://www.digikey.com/en/products/detail/microchip-technology/ATSAM3N4AA-MU/2522782
[41]: https://www.digikey.com/en/products/detail/microchip-technology/AVR64DD14-I-SL/16915537
[42]: https://www.digikey.com/en/products/detail/texas-instruments/MSPM0L1306SDYYR/20415364
[43]: https://www.digikey.com/en/products/detail/texas-instruments/MSPM0G1106TRHBR/21819952
[44]: https://www.digikey.com/en/products/detail/microchip-technology/ATSAMD21J18A-CUT/5057259

The astounding CH32V003
-----------------------

Digi-Key, as I said, unfortunately doesn’t carry WCH chips, but LCSC
offers [some CH32V003 models for 18.23¢ in quantity 50–149][47] in a
SOP-8, dropping to 12.34¢ in quantity 5000+, so cutting into Padauk's
pricing territory rather sharply — but with RISC-V!  These are 48MHz
RV32EC microcontrollers with a clone of ST’s STM8S003 peripherals.
This has naturally resulted in a lot of excitement [on the EEVBlog
forums][49], [Reddit][50], [Hackster][51], [CNX Software][52], [the
orange website][53].

[47]: https://www.lcsc.com/product-detail/Microcontroller-Units-MCUs-MPUs-SOCs_WCH-Jiangsu-Qin-Heng-CH32V003J4M6_C5346354.html
[49]: https://www.eevblog.com/forum/microcontrollers/stm8s-versus-ch32v003-price-war/
[50]: https://old.reddit.com/r/nicechips/comments/yjxxzj/wch_ch32v003_dirt_cheap_48mhz_riscv/
[51]: https://www.hackster.io/news/wch-launches-a-sub-10-risc-v-microcontroller-while-a-6-90-dev-board-gets-you-started-90b1ffd7490a
[52]: https://www.cnx-software.com/2022/10/22/10-cents-ch32v003-risc-v-mcu-offers-2kb-sram-16kb-flash-in-sop8-to-qfn20-packages/
[53]: https://news.ycombinator.com/item?id=33302035

[OpenOCD evidently supports WCH’s custom “wchlink” on-chip debug
protocol][54] after [somebody reverse-engineered it][55], and WCH also
added support for it to [their fork of OpenOCD][56].

[54]: https://github.com/fxsheep/openocd_wchlink-rv
[55]: https://github.com/fxsheep/openocd_wchlink-rv/wiki/WCH-Link-RV-USB-protocol
[56]: https://github.com/fxsheep/openocd_wchlink-rv/wiki/WCH-Link-RV-USB-protocol

[The English datasheet is short][46], though [the Chinese one is even
shorter][48].  Apparently it can handle 3.3 or 5 volts or even down to
2.7.  Low standby current of 9μA?  16KiB of Flash and 2KiB of SRAM.  A
10-bit single-ended ADC (how fast?  internal bandgap?).  An on-chip
op-amp.  On-chip 24MHz RC oscillator (which can get doubled as
“PLLSRC” for 48MHz) and external crystal support.  USART, SPI, I²C,
PWM, two watchdogs, single-wire serial debug.  Packages with 8, 16,
and 20 pins.  Von Neumann.

8 pins is not a lot for power, ground, reset, serial wire debug, and
the actual application, even without an external clock.  The [SOP-16
is 23.11¢ in quantity 1, 16.72¢ in quantity 50–149, and 12.62¢ in
quantity 5000+][57], making it actually *cheaper* than the SOP-8 in
quantity 100.  On the 16-pin package, SWIO is on pin 7 and NRST is on
pin 11; they can’t share one pin.  On the 8-pin package, SWIO is on
pin 7 and there seems to be no reset pin at all, which I guess is
another reason not to use the SOP-8.

They’ve added a two-level hardware interrupt stack rather than using
the slow standard RISC-V interrupt handling mechanism.

Power management has “sleep” (peripheral clocks powered normally, 30μs
wakeup time) and “standby” (only low-speed clocks enabled, for the
independent watchdog and the real-time clock, 200μs wakeup time).  At
48MHz with peripherals off it’s supposed to use 4.6mA with an external
clock or 3.9mA on internal clock.  3.9mA works out to 12.9mW at 3.3V,
or 270pJ per clock and presumably roughly per instruction, which is
quite low for conventional CMOS.  But that’s running from Flash;
running from SRAM (table 3-7-1) it’s only 1.8mA ∴ 5.9mW ∴ 124pJ/insn,
which is fantastic.  Sleep mode is the same as full speed operation
(1.8mA), which can’t be right, and standby is 10.5μA at 3.3V = 35μW
with the low-speed oscillator peripherals on.

The longest useful standby time is presumably on the order of
200μs·5.9mW/35μW ≈ 34ms, in the sense that if it wakes up every 34ms
for 200μs and uses 5.9mW, it’s using an additional 35μW on top of the
unavoidable standby power of 35μW.  So you can increase sleep times
toward infinity to reduce power consumption by another factor of 2,
from 70μW to 35μW.  A reasonable low-power-background mode is then to
wake up for 400μs to do 200μs (9600 cycles) of computation every 70ms,
for a total of 137k instructions per second, almost equivalent to a
Commodore 64, but using 70μW.  (And, of course, with only 16KiB of
Flash and 2KiB of SRAM.)

Minimum external clock speed is 4MHz; you can divide it down for
slower operation.  Internal clock speed “after calibration” is ±1.6%,
comparable to the ADC bandgap reference precision.

The timers support 6-channel three-phase PWM generation (like for a
BLDC H-bridge I guess), DMA, and counting quadrature encoder signals.

GPIOs have pullups and pulldowns (45kΩ) and both push-pull and
open-drain options.  Three of the pins on the 16-pin package are
5V-tolerant, and two on the 8-pin package.  It’s rated for up to 20mA
per GPIO, 100mA on Vdd, and 80mA on Vss.  150mV of hysteresis, 1μA
input bias, 3μA input bias on 5V-tolerant pins, 5pF.  There’s a
MODEx[1:0] to, I think, limit slew rates.  Fastest I/O supported is
30MHz, 10ns transition times.

The rail-to-rail opamp is one of the coolest things, even though it’s
not a great opamp.  Input offset voltage is 3–10mV, so it’s not high
precision, and it’s only 12MHz GBW.  It can drive 1.5mA of output.
Noise is 28–83nv/√Hz.  It’s unclear what its offset current and bias
current are, but probably similar to the 1μA for GPIOs.

The ADC has an internal bandgap reference rated for 1.17–1.23V, which
is a tighter rating than most such chips.  It requires 370μA of power
to operate, which is large compared to standby usage, but you can’t
use it in standby mode anyway, just sleep and run mode.  To run its
clock at 24MHz instead of 12MHz, you need a 5V power supply (>4.5V,
anyway); at >3.2V you get 12MHz, and at >2.8V you only get 6MHz.
Conversion time is 14 clocks, so you can get 1.7Msps at full speed,
0.86Msps at half speed, and 0.43Msps at low speed.  Again, no bias
current numbers are given.

[57]: https://www.lcsc.com/product-detail/Microcontroller-Units-MCUs-MPUs-SOCs_WCH-Jiangsu-Qin-Heng-CH32V003A4M6_C5346357.html

There’s an [SDK from WCH on GitHub][45] in English and Chinese.

[45]: https://github.com/openwch/ch32v003
[46]: https://www.wch-ic.com/downloads/file/359.html?time=2024-01-18%2014:53:10&code=o145oQ8A3XIVI7c8H7f6tWgnQZQ15uVDsK5xffmK
[48]: https://datasheet.lcsc.com/lcsc/2303301000_WCH-Jiangsu-Qin-Heng-CH32V003J4M6_C5346354.pdf
