I was thinking about the problem of using near-natural materials to
make a housing for a smelting blower, as discussed in file
`plant-blower.md`.  The obvious thing to do is to take natural
materials that come in the form of lightweight sheets, such as leaves
or bark, and patch them together to form the blower housing.  But how
do you patch them together in a nearly airtight fashion?  The obvious
approach is to seal them with adhesives, but what adhesives?

The adhesives I remember seeing YouTuber John Plant use in his series
“Primitive Technology” are quicklime (from snail shells or from wood
ash), iron bacteria, and clay.  But there are actually quite a number
of other natural or near-natural adhesives available, some of them
even in the rain forest near Cairns in Far North Queensland, where he
films his videos.  (He lives in a Cairns suburb with his parents, Lex
and Lesley Plant.)

Basic adhesives list
--------------------

- plaster of Paris (gypsum calcined to its hemihydrate state, as
  commonly used for setting broken bones)
- raw egg (the yolk is the adhesive in tempera)
- gluten (from wheat, barley, rye, or sometimes oats)
- mucilage (see below)
- asphalt, in a few places
- [birch tar][0] (*brea de abedul*), “used widely as an adhesive as
  early as the Middle Paleolithic to early Mesolithic
  era. Neanderthals produced tar through the dry distillation of birch
  bark as early as 200,000 years ago,” including in Ötzi’s arrows and
  copper axe.  However, I do not think birch trees are native to
  Australia, or for that matter Argentina.
- [wood tar][19] made by destructive distillation of pine wood in a
  tar kiln, in the same way as birch tar.  Wikipedia explains the
  structure: “They were built close to the forest, from limestone or
  from more primitive holes in the ground. The bottom is sloped into
  an outlet hole to allow the tar to pour out. The wood is split into
  dimensions of a finger, stacked densely, and finally covered tight
  with earth and moss. If oxygen can enter, the wood might catch fire,
  and the production would be ruined. On top of this, a fire is
  stacked and lit. After a few hours, the tar starts to pour out and
  continues to do so for a few days.”  Hardwoods will not work: “When
  deciduous tree woods are subjected to destructive distillation, the
  products are methanol (wood alcohol) and charcoal.”
- pine resin and other tree resins; see the section below
- [camphor][30], which is often grouped with the balsams but is a
  specific molecule which can be distilled from many different plants,
  including basil, rosemary, [camphorweed][31], and the [camphor
  laurel][32], which is a "noxious weed" throughout Queensland
- latex from the dandelion (*Taraxacum*; the [Kazakh dandelion][2] is
  cultivated for this following its traditional use as chewing gum)
- latex from the rubber tree (*Hevea brasilensis*)
- latex from the [Panama rubber tree][35] (*Castilla elastica*)
- latex from the [guayule][36]
- latex from [other laticifers, which apparently constitute 10% of all
  flowering plants][1]; in Australia and in particular in northern
  Queensland apparently “the worst weed” is the [rubber vine][3]
  *Cryptostegia grandiflora* which “can strangle and kill” native
  trees.
- [plastilin][4] (clay-filled oil; [plasticine][5] is instead
  typically gypsum and some kind of lime bound together with vaseline,
  stearic acid, and lanolin)
- fats containing significant amounts of saturated fat; this can be
  increased by dry-fractionating them into so-called stearin and olein
  fractions.  Sheep fat is especially high in saturated fat, and beef
  tallow is especially abundant.  [Fractionation benefits from some
  agitation to speed the partial crystallization][7], and separation
  is speeded by centrifugation and [membrane press filtration][8].
  (Solvent fractionation with acetone is also used in some cases.)
- lanolin, which is [technically not a fat][9]
- gelatin
- peanut butter
- honey
- sugar syrup
- beeswax
- hydrolysate of [keratin][11], for example from hair, [made with
  lye][10] or possibly just with slaked lime, though enzymatic
  hydrolysis is much more efficient; historically the lime-sulfide
  unhairing process produces a hydrolysate which might work
- creosote
- konjac
- hagfish slime
- quicklime/slaked lime
- clay
- sodium silicate
- linseed oil or other “drying” oils (see below)
- semen
- blood
- mucus
- sea salt (forming solid crystals penetrating through porous
  materials when it dries)
- citric acid, in the same way
- *nigari* (magnesium chloride, the remaining liquid after making sea
  salt)
- alginate
- smegma
- pulverized insects
- inky-cap mushrooms
- banana flesh
- carrageenan
- uric acid from bird droppings
- soap
- [grease][12] consisting of soap (potentially just sodium stearate)
  and oil; [quicklime can do this in situ in olive oil][13]
- slime from snails or especially slugs
- shellac
- cooked zucchini
- starch (wheat paste)
- carnauba wax and similar vegetable waxes; possibly palm trees other
  than the carnaúba also yield usable amounts of wax, but
  [bayberries][14], [the candelilla bush][15], [sugar cane][16], [rice
  bran][17], [a couple of Japanese sumac species][18], and other
  plants also yield profitable amounts of waxes, usually by boiling
  them in water.
- ear wax
- pectin, which is contained in every cell of every terrestrial plant,
  and is [“abundant in the green parts”][39], but citrus peels are
  about 30% pectin while rose hips are about 15%.

To these basic adhesives you can add a number of fillers and other
additives.  [Harbutt’s wool-filled plasticine patent][6] from 01915,
now GB191419211A but originally “Nº 19,211, A.D. 1914” for its date of
application, added 1% lamb’s wool or cotton wool to Plasticine to
increase its strength as a discontinuous reinforcing filler, and
cotton gauze is often used as a continuous reinforcement for plaster,
as in [ancient Egyptian cartonnage][33] and bone-setting casts.  Inert
granular fillers such as clay, gypsum, charcoal, sawdust, and talc are
likely to greatly extend these adhesives.

Many of the materials listed are biodegradable, and probably some kind
of biocide would help to prevent them from biodegrading.  The most
easily improvised choices are smoke, as in smoked meat, and salt, as
in Play-Doh.  Sodium chloride affects the rheology of the mix
somewhat, but other salts such as magnesium chloride will in some
cases have much larger effects, but all of these require fairly high
additive loadings to preserve effectively.  Alternative biocides such
as camphor, sulfur, oil of cloves, or blue vitriol may be effective at
much lower levels.

[0]: https://en.wikipedia.org/wiki/Birch_tar
[1]: https://en.wikipedia.org/wiki/Latex
[2]: https://en.wikipedia.org/wiki/Taraxacum_kok-saghyz#Rubber
[3]: https://en.wikipedia.org/wiki/Cryptostegia_grandiflora
[4]: https://en.wikipedia.org/wiki/Modelling_clay#Oil-based_clay
[5]: https://en.wikipedia.org/wiki/Plasticine
[6]: https://worldwide.espacenet.com/patent/search/family/032374642/publication/GB191419211A?q=pn%3DGB191419211A
[7]: https://lipidlibrary.aocs.org/edible-oil-processing/dry-fractionation
[8]: https://www.soci.org/-/media/files/lecture-series/pb166b.ashx
[9]: https://en.wikipedia.org/wiki/Lanolin
[10]: https://revistapielarieincaltaminte.ro/revistapielarieincaltaminteresurse/en/fisiere/full/vol20-nr3/article5_vol20_issue3.pdf
[11]: https://mostwiedzy.pl/pl/publication/download/1/solubilization-of-keratins-and-functional-properties-of-their-isolates-and-hydrolysates_24796.pdf
[12]: https://en.wikipedia.org/wiki/Grease_(lubricant)#Thickeners
[13]: https://en.wikipedia.org/wiki/Grease_(lubricant)#History
[14]: https://en.wikipedia.org/wiki/Bayberry_wax
[15]: https://en.wikipedia.org/wiki/Candelilla_wax
[16]: https://en.wikipedia.org/wiki/Sugarcane_wax
[17]: https://en.wikipedia.org/wiki/Rice_bran_wax
[18]: https://en.wikipedia.org/wiki/Japan_wax
[19]: https://en.wikipedia.org/wiki/Tar#Wood_tar
[30]: https://en.wikipedia.org/wiki/Camphor
[31]: https://en.wikipedia.org/wiki/Heterotheca
[32]: https://en.wikipedia.org/wiki/Camphora_officinarum#In_Australia
[33]: https://en.wikipedia.org/wiki/Cartonnage
[35]: https://en.wikipedia.org/wiki/Castilla_elastica
[36]: https://en.wikipedia.org/wiki/Parthenium_argentatum
[39]: https://en.wikipedia.org/wiki/Pectin

Mucilage
--------

Mucilage in particular is available from [“nearly all plants and some
microorganisms”][34], but of course some produce more of it than
others.  Wikipedia mentions flaxseeds, cacti and other succulents
(aloe vera being especially easy and rapid to grow, though WP also
specifically calls out the *nopal* or prickly pear), slippery elm
inner bark, natto, marshmallow, kelp, chia, taro, mullein
(*Verbascum*), okra, licorice root, and the husks of psyllium; I would
add elm seeds to the list.  Plant has cultivated taro in the past.

WP also mentions “Irish moss”, which is “carrageenan moss”, a red alga
whose particular mucilage is the “carrageenan” widely used in
industrial food production and constitutes an astounding 55% of the
alga’s dry mass.  It might be worthwhile trying to boil whatever
freshwater algae you can find to see if they share this amazing trait.

WP mentions that [filé powder][37] of the leaves of the sassafras tree
is used as an alternative in gumbo to okra as a thickener, which
presumably means that they also contain a massive amount of mucilage;
WP also says that [young sassafras leaves and twigs are “quite
mucilaginous”][38].

Most terrestrial plant roots contain massive amounts of mucilage for
unclear reasons.

Like functionalized industrial cellulose derivatives, mucilages are
functionalized polysaccharides, and, as I understand it, their anionic
moieties are what make them water-soluble — they’re anionic
polyelectrolytes with a negative ζ potential.  This solvation is
sometimes pH-dependent, and can often be disrupted by the presence of
polyvalent cations such as calcium, magnesium, or especially aluminum,
which can produce a gel that is no longer water-soluble, so-called
“ionotropic gelation”.  Pectins also often have this property.

*Aloe vera*’s gel is the so-called [acemannan][40], and [it forms a
gel inside the leaf at water concentrations of 98.5% to 99.5%][41],
which is to say that one gram of acemannan can gelate between 65 and
200 grams of water.  Actually, the 0.5% to 1.5% that isn’t water is
only partly acemannan.

I’m not sure if [gum arabic][43], the traditional watercolor-paint
binder and a major motivator of colonial-era warfare, is a mucilage or
not; but normally it’s the hardened polysaccharide and glycoprotein
sap of [*Vachellia nilotica*][44], Linnaeus’s type species for
*Acacia*, also known as babbula or babul.  “It is (...) considered a
‘weed of national significance’ and an invasive species of concern in
Australia.”  It’s possibly the most widely used industrial
polysaccharide gum despite being more expensive than most of the
alternatives.  The harvesting process is similar to that for resins
and latexes.

[34]: https://en.wikipedia.org/wiki/Mucilage
[44]: https://en.wikipedia.org/wiki/Vachellia_nilotica
[43]: https://en.wikipedia.org/wiki/Gum_arabic
[37]: https://en.wikipedia.org/wiki/Fil%C3%A9_powder
[38]: https://en.wikipedia.org/wiki/Sassafras
[40]: https://en.wikipedia.org/wiki/Acemannan
[41]: https://doi.org/10.1002/9781119711414.ch1 "Natural Polysaccharides From Aloe vera L. Gel (Aloe barbadensis Miller): Processing Techniques and Analytical Methods, by Lacerda Jales, de Melo Barbosa, da Silva, Severino, and Accioly de Lima Moura"


Resins
------

In addition to pine resin, there are a wide variety of other plant
resins which can work as non-water-soluble adhesives, such as
[dammar][20], [copal][21], [dragon’s blood][22], [mastic][23],
[amber][24], [kauri kapia][25], [sandarac][26], [storax][29], etc.
Nearly all of these [balsams][28] originate from trees (except for
[asafetida][27] and its *Ferula* relatives), but those trees include
both gymnosperms and angiosperms (rosids, asterids, and saxifragales)!
So resin adhesives are not quite as widespread as mucilage, but they
are still very widespread.

Resins are generally liquid when they come out of the plant, but tend
to harden as their more volatile components evaporate.  For commercial
use, this process is often accelerated artificially, and sometimes the
resins are harvested long after the plant has secreted them — in the
case of amber, sometimes hundreds of millions of years later.
Generally these resins are not water-soluble, but can be thinned with,
for example, acetone or methanol, which provides a relatively easy way
to get a thin layer.  See file `near-natural-solvents.md`.  (In some
cases, you can also melt them, but sometimes the heat is undesirable.)

[Wikipedia says of the aloe-related genus *Xanthorrhoea*, one of the
plants known as “grasstree”][42]:

> The resin from *Xanthorrhoea* plants is used in spear-making and is
> an invaluable adhesive for Aboriginal people, often used to patch up
> leaky coolamons (water containers) and even yidaki (didgeridoos).
> (...)
>
> Resin collected from the plant was used in Australia until the
> mid-twentieth century for the following purposes:
>
> * Burnt as an incense in churches.
> * A base component for a varnish used on furniture and in dwellings.
> * A polish and a coating used on metal surfaces including stoves,
>   tin cans used for storing meat and “brass instruments.”
> * A component used in industrial processes associated with “sizing
>   paper, in soap making, perfumery and in manufacturing early
>   gramophone records.”

It shows *Xanthorrhoea* as being endemic to all of Queensland and says
they “grow in coastal heaths, and wet and dry forests of Australia”,
though with their slow growth rate I have a hard time imagining them
being very successful in the rain forest.  In the [scarred tree
article][45], it again mentions the resin’s use:

> Bark canoes were made from massive single piece of bark. They were
> softened over fire, tied at both ends to make a canoe shape and used
> sticks or spars to keep it open. The resin from *Xanthorrhoea*
> (grass tree) was used to waterproof the base and were also used to
> repair any leaks or small holes.

It also mentions the resin of [*Triodia*/spinifex/tjanpi][46]:

> Spinifex resin is a gum [sic] coating of some species of spinifex
> grasses. This sticky resin was traditionally used as an adhesive in
> tool making by Aboriginal Australians. Many species of spinifex are
> extremely resinous, to the extent that resin may drip down the stems
> and leaves on hot days, and large residual lumps of resin often may
> be seen at the bases of hummocks which have burned.
> 
> (...)
>
> The spinifex is threshed until the resin particles fall free. These
> particles are heated until they fuse together to form a moldable
> black tar which is worked while warm. When set, this gum is quite
> strong.
>
> The preparation of spinifex for hafting use is similar to that of
> *Xanthorrhoea*.  It is thought to have been preferable to
> *Xanthorrhoea* for hafting, due to its ability to be re-heated and
> remodelled several times without going brittle.  The resin can be
> re-softened using fire and some moisture.

However, spinifex seems even less likely to be useful in the rain
forest.

[20]: https://en.wikipedia.org/wiki/Dammar_gum
[21]: https://en.wikipedia.org/wiki/Copal
[22]: https://en.wikipedia.org/wiki/Dragon%27s_blood
[23]: https://en.wikipedia.org/wiki/Mastic_(plant_resin)
[24]: https://en.wikipedia.org/wiki/Amber
[25]: https://en.wikipedia.org/wiki/Kauri_gum
[26]: https://en.wikipedia.org/wiki/Sandarac
[27]: https://en.wikipedia.org/wiki/Asafoetida
[28]: https://en.wikipedia.org/wiki/Balsam
[29]: https://en.wikipedia.org/wiki/Storax_balsam
[42]: https://en.wikipedia.org/wiki/Xanthorrhoea#Uses
[45]: https://en.wikipedia.org/wiki/Scarred_tree
[46]: https://en.wikipedia.org/wiki/Spinifex_resin

Drying oils
-----------

Linseed oil is a so-called “drying” oil, which polymerizes by
free-radical chemical crosslinking by oxidation, which requires a
[siccative][47] to take place in a reasonable time.  This is the
binder used in oil paint, for example.  They used to boil linseed oil
with lead oxide, but nowadays they use less toxic salts.  Wikipedia
says:

> Typical oil drying agents are derived from ions of cobalt,
> manganese, and iron, prepared as “salts” of lipophilic carboxylic
> acids such as naphthenic acids, in order to give them a soap-like
> chemical formula and make them oil-soluble.
>
> In technical literature, oil drying agents, such as naphthenates,
> are described as salts, but they are probably also non-ionic
> coordination complexes with structures similar to basic zinc
> acetate. These catalysts were traditionally hydrocarbon carboxylate
> chelates of lead, but due to lead’s toxicity, cobalt and other
> elements, such as zirconium, zinc, calcium, and iron, have replaced
> the lead in more popular products. Most driers are colorless but
> cobalt driers are a deep blue purple color and iron driers are
> reddish orange. These colored driers are therefore compatible only
> with certain darker pigmented paints where their color will be
> unseen.

I feel like you could probably boil your linseed oil with quicklime
without too much trouble, and maybe that would give you an adhesive
that would make a permanent bond between two porous surfaces.  Soybean
oil, tung oil, poppy seed oil, and walnut oil are possible
alternatives to linseed oil here.

Making up naphthenates of zinc, zirconium, iron, and cobalt in the
rain forest seems like it could be significantly more challenging.

[47]: https://en.wikipedia.org/wiki/Oil_drying_agent
