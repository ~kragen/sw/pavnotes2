Reading [Devine Lu Linvega's talk about seeking "a way of doing
computers that nobody could take away from
me"](https://100r.co/site/weathering_software_winter.html), I came
across this entertaining passage:

> Forth had a different problem, everyone I'd talk to would say Forth
> was the best, and that I should really use it, but then I'd ask them
> questions like, "how do I calculate the distance between two
> points?" I walked away from that interaction thinking, well, it
> seems like a lot of people like Forth, but don't write it. It was
> hard to find any code at all.

So I thought it would be interesting to try that problem.  I used
Gforth because it's easily available and free software:

    $ gforth
    Gforth 0.7.3, Copyright (C) 1995-2008 Free Software Foundation, Inc.
    Gforth comes with ABSOLUTELY NO WARRANTY; for details type `license'
    Type `bye' to exit

My first move was to define what a "point" was.  I decided that maybe
it would be a struct with two fields, so I defined that:

    : point create , , ;  ok
    3 4 point x  ok

So let's see if it stored the (3, 4) as I intended:

    x ? 4  ok
    x cell+ ? 3  ok

Well, kind of.  It stored (4, 3), which I guess is good enough.  But
`x` is a bad name for a point.  How about `p`?

    4 5 point p  ok

Okay, now to find the distance, we're going to need a square root.
Forth isn't that strong with numerics, so I thought I'd check to see
if there was a built-in square root function, which I figured would be
called `sqrt` if it existed:

    sqrt 
    :6: Undefined word
    >>>sqrt<<<
    Backtrace:
    $7F785F145A68 throw 
    $7F785F15BDD0 no.extensions 
    $7F785F145D28 interpreter-notfound1 

Hmm, well, this is going to be more work than I thought.  How about
`square`, everybody's standard example of Forth?

    square 
    :7: Undefined word
    >>>square<<<
    Backtrace:
    $7F785F145A68 throw 
    $7F785F15BDD0 no.extensions 
    $7F785F145D28 interpreter-notfound1 

Nope.  Well, let's start by computing the *square* of the Pythagorean
sum rather than the Pythagorean sum itself:

    : square dup * ;  : pythsumsq square swap square + ;  ok
    3 4 pythsumsq . 25  ok

Seems okay.  But now I realize I kind of want my `point`s to push both
coordinates on the stack when they're called, at the expense of making
them immutable.  So I redefined `point` to have a `does>`:

    : point create , , does> dup @ swap cell+ @ ; redefined point   ok
    0 5 point q  ok
    q .s <2> 5 0  ok

Seems okay.  Except that it's backwards, which is now really bugging
me, so I'll put the `cell+` on the other field fetch.  Note that this
is really facilitated by the fact that I'm running Gforth in Emacs
shell-mode, so I can recall the earlier line with M-p M-p and then
edit it before resubmitting:

    : point create , , does> dup cell+ @ swap @ ; redefined point   ok
    0 5 point q redefined q   ok
    q .s <4> 5 0 0 5  ok

That's better. Let's clear the stack, which I'm sure there's a word to
do but don't remember:

    . . . .  5 0 0 5  ok
    q . . 5 0  ok

Okay, that's fine. It's printing backwards just as if I'd typed
`0 5 . .` so now let's define a function which calculates the
*displacement* between two points on the stack.  You could certainly
do this with stack manipulations, but I thought it would be less
confusing with temporary variables called `x` and `y`, which initially
we store the second point in.

    variable x variable y : disp y ! x ! y @ - y ! x @ - y @ ; redefined x   ok
    4 0 point p redefined p   ok
    p . . 0 4  ok
    p q disp . . -5 4  ok

Okay, that worked, so now we should be able to calculate the squared
distance between them, right?

    p q disp pythsumsq . 41  ok

Uh, I guess I was thinking (4, 5, something) was a Pythagorean triple,
but actually it's (3, 4, 5), so let's give `q` a different value:

    0 3 point q redefined q   ok
    p q disp pythsumsq . 25  ok

Okay, good.  Now all we need is a square root routine.  The iterative
procedure of taking the average of *x* and *y*/*x* as the new *x* is
now recognized as an instance of Newton's method, but it dates back to
I think ancient Babylonia.  Normally you iterate until it's close, but
I figure 20 times is probably enough. The body of the loop here says,
in pop infix notation, "x = (xsq/x + x)/2".  It would be shorter with
stack manipulations instead of variables but I think even less
readable, and readability is never Forth's forte.

    variable xsq  variable x redefined x   ok
    : sqrt dup xsq ! x ! 20 0 do xsq @ x @ / x @ + 2/ x ! loop x @ ;  ok
    25 sqrt . 5  ok

Okay, so that works for that example at least.

    : pythsum pythsumsq sqrt ;  ok
    : distance disp pythsum ;  ok

And here I can finally calculate the distance between two points:

    p q distance . 5  ok

Just in case, let's see if it can calculate some other triple:

    6 8 pythsum . 10  ok
    \ seems to work  ok

Just in case, one more, a little more difficult:

    1000 1000 pythsum . 1414  ok

All right, I guess that worked.  That took about 12 minutes.  Here's
the final 10 lines of code I ended up with:

    variable xsq  variable x
    : sqrt dup xsq ! x ! 20 0 do xsq @ x @ / x @ + 2/ x ! loop x @ ;
    : square dup * ;  : pythsumsq square swap square + ;
    : pythsum pythsumsq sqrt ;
    variable x variable y : disp y ! x ! y @ - y ! x @ - y @ ;
    : distance disp pythsum ;
    : point create , , does> dup cell+ @ swap @ ;
    4 0 point p 
    0 3 point q
    p q distance .

Although this does work, it could clearly be improved, but the biggest
oversight is that in fact Gforth does have not only floating-point
support but also a square-root function in it.  I just guessed the
name wrong.  So you can just do this:

    3.e0 fdup f* 4.e0 fdup f* f+ fsqrt f. 5.  ok

And if your points are single-precision integers, you can just convert
them to floating point, so you only need one line of code instead of
four for `pythsum`.  Here I'm adding a 0 to the stack to convert
single-precision integers to double-precision so I can convert them to
floats, and dropping what I hope is a 0 at the end to reverse the
process.

    : pythsum 0 d>f 0 d>f fdup f* fswap fdup f* f+ fsqrt f>d drop ;  ok
    3 4 pythsum . 5  ok

I think I've never used the dpANS96 FLOATING wordset before, which I
guess reinforces Devine's point that it seems like a lot of people
like Forth, but don't write it.

Also see file `interactive-forth.md` for notes on the Forth UI.
