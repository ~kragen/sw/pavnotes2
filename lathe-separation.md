The standard way to make a lathe or similar machine tool is to make it
very rigid and precise using thick steel sections.  The resulting
machine needs most of its steel for rigidity and is enormously
stronger than would be needed to resist the loads it will experience
in use.

This has some disadvantages.  Steel is heavy, expensive to cut, and
even expensive in an absolute sense compared to materials like brick;
large assemblies made from it must be fastened together with fasteners
if they are to be precise, because welding distorts the steel and
ruins your precision, and cutting a large assembly out of a block of
steel is even more expensive.

A possible solution is to separate out four requirements of the
machine tool frame, all normally supplied by the steel frame, into
separate materials: the rough geometry is provided by a mold made from
plastic or sheet metal (perhaps backed up with plaster), rigidity is
provided by a cheap mineral ceramic (portland-cement concrete,
lime-cement concrete, Sorel-cement concrete, brick masonry,
sodium-silicate concrete, castable refractory, etc.), strength is
provided by steel plates embedded within the ceramic, and precise
geometry is provided by steel plates set into the surface of the
machine.  Once the ceramic has set, these plates are ground to precise
shape (or otherwise cut, for example with ECM).  Tensioned steel
cables strung through the mineral part might increase rigidity
further.

In this way, the machine can achieve even greater rigidity than is
conventionally built into a steel machine, and far greater vibration
damping, while costing much less.  I think it will weigh more for the
same rigidity, though, because these super cheap mineral composites
are softer than steel.

Some of these mineral materials even have smaller thermal coefficients
of expansion than steel, enabling lower thermally induced dimensional
errors, though more precise thermal compensation with composite
structures like gridiron pendulums can provide much better figures.
