The Chaos Glyph is generally considered to represent the advent of the
anarchy after the collapse of Hollingham imperial rule.  It first
appears in inscriptions in the interregnum in a somewhat more
elaborate form, associated with the date of the death of Lars
Hollingham IV.  In ensuing centuries, it became associated with not
only anti-imperialism but more generally with the movement to freedom
of religion.

The similarity to the mathematical infinity symbol ∞ used during the
Hollingham empire (and even earlier) is generally presumed to be
coincidental, since the early forms of the Chaos Glyph don’t look
anything like the infinity symbol.  However, it’s certainly possible
that there was an influence in later centuries, and certainly many
tyrannical demagogues and other writers have used the similarity to
support their own agendas.

In the earliest forms, the circling arrows are enveloped in what
appears to be a dust cloud.
