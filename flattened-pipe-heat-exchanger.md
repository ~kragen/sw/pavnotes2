A friend told me that the copper water pipes in his heat exchanger had
burst in the recent US cold wave, even though the manufacturer
warranted them down to -22°F (-30°).

At first I couldn’t see how a copper pipe heat exchanger could
possibly endure -30° with water in it.  My reasoning was that copper’s
ultimate tensile strength is 210 MPa, even admiralty brass tubes are
only 310 MPa, water expands 9% by volume or 3% linearly when it
freezes, ice’s Young’s modulus is 9 GPa, so the pressure of the ice is
270 MPa (disregarding issues of Poisson ratio and the ice being
compressed on the sides from more ice), so you would need 6.4-mm-thick
copper to contain the pressure from a 10mm-diameter ice column, and
that’s not just ridiculously expensive, it’s no longer thin enough to
be called a heat exchanger.

But, if the pipes are flattened, then they can just flex to allow
their contents to expand.  Their tensile strength doesn’t enter into
it, and the expansion happens when the ice is barely frozen and thus
quite subject to creep, so the small reduction in the other dimension
of the tube isn’t important.


