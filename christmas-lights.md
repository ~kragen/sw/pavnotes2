We have some LEDs and some resistors and stuff and we want to make
some Christmas lights.

I designed [this circuit](http://tinyurl.com/yndh6mnp) to show why you
don’t want to put a lot of LEDs in parallel:

![(schematic of 5 LEDs in parallel)](5leds.png)

    $ 1 0.000005 10.20027730826997 50 5 43 5e-11
    v 48 320 48 112 0 0 40 5 0 0 0.5
    w 48 112 192 112 0
    r 192 112 192 208 0 270
    w 112 208 144 208 0
    w 144 208 176 208 0
    w 208 208 240 208 0
    w 192 208 208 208 0
    34 fwdrop\q2.1 1 9.32e-11 0 3.51532028522468 0 1
    162 240 208 240 320 2 fwdrop\q2.1 1 0 0 0.01
    w 240 320 208 320 0
    w 208 320 176 320 0
    162 208 208 208 320 2 default-led 1 0 0 0.01
    w 176 320 144 320 0
    34 fwdrop\q2.4 1 9.32e-11 0 4.017508897399634 0 1
    162 176 208 176 320 2 fwdrop\q2.4 1 0 0 0.01
    w 144 320 112 320 0
    162 144 208 144 320 2 default-led 1 0 0 0.01
    w 112 320 48 320 0
    162 112 208 112 320 2 fwdrop\q2.4 1 0 0 0.01
    w 192 208 176 208 0

Because the different LEDs are using models with slightly different
forward drops, some of them barely light up, and one of them is
hogging the majority of the current.  A phenomenon circuit.js doesn’t
model is that when more current runs through one of the parallel LEDs,
it dissipates proportionally more power, so it heats up more.
Depending on how fast it is able to lose that heat to the environment,
this can result in one LED hogging *all* the current instead of just
most of it.

If you put multiple LEDs in series, they all have the same current,
and their voltages add.  The lower leftover voltage means you can use
a lower-value resistor, but this also makes the brightness less
predictable.  Suppose [you put three 1.5-volt LEDs in
series](http://tinyurl.com/ywtfvb47) across a 5-volt supply, to get
4.5 volts, and you use a 22Ω resistor to get 23mA:

![(schematic of 3 LEDs in series)](ledseries.png)

    $ 1 0.000005 10.20027730826997 50 5 43 5e-11
    v 48 384 48 112 0 0 40 5 0 0 0.5
    w 112 112 304 112 0
    34 fwdrop\q1.5 1 9.32e-11 0 2.8851674717606546 0 0.05
    162 304 112 304 144 2 fwdrop\q1.5 1 0 0 0.01
    r 304 208 304 384 0 22
    w 112 112 48 112 0
    w 48 384 112 384 0
    w 304 384 112 384 0
    162 304 144 304 176 2 fwdrop\q1.5 1 0 0 0.01
    162 304 176 304 208 2 fwdrop\q1.5 1 0 0 0.01

In this simulation I’m getting 28.5mA because I’ve set the LEDs to
have 1.5V forward voltage at 50mA, so the forward voltage of the LEDs
is only about 1.45 volts at the lower current, so the resistor sees
0.67V instead of the intended 0.5V, resulting in 34% more current.  If
the LEDs heat up enough to drop their 50mA forward voltage to 1.4V, in
the simulation, the voltage across them drops to 1.38V each, and the
current leaps to 39mA.  If they heat up more to drop the 50mA voltage
to 1.3V, then each one carries 50mA, more than twice the design
current, and probably twice the brightness.

This kind of thing can result in thermal runaway burning out LEDs, but
even if it doesn’t, it may be desirable to have more predictable
brightness.  So most LED lightbulbs use some variant of [the following
current-sink circuit](http://tinyurl.com/yqzh5p9t) instead:

![(schematic of a current-regulated 4 LEDs in
series)](ledcurrentreg.png)

    $ 1 0.000005 10.20027730826997 50 5 43 5e-11
    v 48 384 48 112 0 0 40 12 0 0 0.5
    w 112 112 304 112 0
    34 fwdrop\q2.1 1 9.32e-11 0 3.51532028522468 0 1
    162 304 176 304 208 2 fwdrop\q2.1 1 0 0 0.01
    34 fwdrop\q2.4 1 9.32e-11 0 4.017508897399634 0 1
    162 304 208 304 240 2 fwdrop\q2.4 1 0 0 0.01
    162 304 144 304 176 2 default-led 1 0 0 0.01
    162 304 112 304 144 2 fwdrop\q2.4 1 0 0 0.01
    t 272 272 304 272 0 1 -2.7276194139219476 0.7000881601197889 100 default
    r 304 320 304 384 0 10
    r 112 256 112 112 0 1000
    w 112 112 48 112 0
    w 48 384 112 384 0
    w 304 256 304 240 0
    w 304 384 112 384 0
    a 160 272 240 272 9 11 0.1 1000000 0.5746105025319618 0.574573438731456 100000
    r 240 272 272 272 0 1000
    w 112 256 160 256 0
    w 304 320 304 288 0
    w 304 320 160 320 0
    w 160 288 160 320 0
    d 112 256 112 384 2 default

This uses a 12-volt power supply.  Here all four LEDs are in series,
so they necessarily have the same current.  A forward-biased silicon
diode carrying 11mA serves as a crude voltage reference at about 570mV
(I think the ICs normally used for this have an internal bandgap
reference instead); the current-sensing 10Ω collector resistor reaches
the same voltage at a current of 57mA, probably safe for most
indicator-type LEDs, though illumination LEDs can commonly withstand
an order of magnitude more.  The opamp compares the current-sense
voltage to the voltage reference and turns on the npn transistor to
the necessary level to get the desired current.

In this simulation, the voltage at the collector is 4.0 volts, 3.4
volts of which is dropped across the transistor, which results in it
dissipating 200 milliwatts, enough to require some attention.  Another
33mW is lost in the current-sense resistor.  The different simulated
LEDs drop from 1.84V to 2.10V each, 8.0 volts in total, so this is
reasonably efficient; two thirds of the power goes to the LEDs and
only one third gets burned in the linear current source.  You could
maybe make it a bit better by dividing down the shitty diode voltage
reference closer to the negative rail so you can use a lower-value
current sense resistor, so it won’t waste as much power and won’t heat
up as much.

Typically, in lightbulbs, this circuit runs directly off the rectified
powerline voltage, so the power supply is more like 340 volts instead
of 12.

A more efficient approach used by some lightbulbs is to build a
*switching* current sink, in which the transistor is operated as a
switch instead of in linear mode, so the current sink burns up very
little power.  I think that, in the particular case of LEDs, you can
even do this without an inductor, because what you’re trying to avoid
is enough *average* current to fry the LEDs.  You can run an order of
magnitude more current through LEDs for short times; the LED chip’s
thermal mass soaks up the excess power.  In fact, pulsed currents are
easier on LEDs than continuous current; as I understand it, this is
because the same current-hogging thermal runaway phenomenon that
happens with multiple LEDs in parallel also happens with points across
the junction of a single LED, where if part of the junction gets hot
it can start to hog the current and heat up more.  Delivering the
current in short pulses ameliorates this phenomenon.

[Here’s one way to build such a switching current source.][0]

![(schematic of switching current sink)](ledswitcher.png)

    $ 1 0.000005 1.1685319768402522 50 5 43 5e-11
    v 48 384 48 112 0 0 40 12 0 0 0.5
    w 112 112 400 112 0
    34 fwdrop\q2.1 1 9.32e-11 0 3.51532028522468 0 1
    162 400 176 400 208 2 fwdrop\q2.1 1 0 0 0.01
    34 fwdrop\q1.5 1 9.32e-11 0 4.017508897399634 0 1
    162 400 208 400 240 2 fwdrop\q1.5 1 0 0 0.01
    162 400 144 400 176 2 default-led 1 0 0 0.01
    162 400 112 400 144 2 fwdrop\q1.5 1 0 0 0.01
    t 368 272 400 272 0 1 0.630378940556219 0.7448436307297666 100 default
    r 400 320 400 384 0 10
    r 112 256 112 112 0 1000
    w 112 112 48 112 0
    w 48 384 112 384 0
    w 400 256 400 240 0
    w 240 384 112 384 0
    a 208 272 288 272 9 11 0.1 1000000 1.4418005820914312 1.5260708358048691 100000
    r 288 272 368 272 0 1000
    w 400 320 400 288 0
    w 240 320 208 320 0
    w 208 288 208 320 0
    d 112 256 112 384 2 default
    r 400 320 320 320 0 10000
    c 320 320 320 384 0 1e-8 2.0946973833487776 0.001
    r 320 320 240 320 0 10000
    c 240 320 240 384 0 1e-8 1.4418005820914312 0.001
    w 240 384 320 384 0
    w 320 384 400 384 0
    r 208 208 288 208 0 10000
    w 208 256 208 208 0
    r 112 256 208 256 0 1000
    w 288 208 288 272 0
    403 192 400 288 496 0 22_4_0_4362_2.5_0.00009765625_-1_2_22_3

[0]: http://tinyurl.com/yvd3f8yq

Here a positive-feedback 10kΩ resistor gives Schmitt-trigger action to
minimize switching losses, and the negative-feedback path has a couple
of RC circuits in it which provide some averaging and also some phase
shift.  I think I fucked that part up because the phase shift is only
about 90°, to the extent that you can talk about a single “phase
shift” between a square wave and a slightly softened triangle wave.  I
feel like this is probably excessive complexity, but whatever, it runs
the simulated LEDs at 314mA and a 33% duty cycle at 1852Hz, so each
pulse is about 150μs long.  The transistor alternates between
saturation (Vce = 0.114V in the simulation, so power of 40mW) and
cutoff (Ic = 28pA in the simulation, so 300pW) so it burns a lot less
power than the one in the linear current sink earlier.

Muntzing the circuit a bit, I get [a simpler
version](http://tinyurl.com/yl5n5kqt) which still works in simulation
without the Schmitt trigger, just as a pure phase-shift oscillator.
Unfortunately, I don’t understand how this circuit works, and in
particular how to get it to run slower than 2.7kHz, or how fast the
switching transitions on the transistor are.  It has a lower duty
cycle of 18%, which seems better, because 18% of 314mA is 57mA, which
is the current we wanted.  And this switching regulated current sink
is 10 components instead of 12.

At least in simulation it is possible for the oscillations to die out
and for it to find a stable dc operating point where the op-amp's
output voltage is a constant 1.844V (if you twiddle with the resistor
and capacitor values), turning it into a more complicated version of
the earlier linear sink, but I don’t think that can actually happen in
practice; I think it depends on the complete absence of noise in the
simulation, because I think any noise would get amplified into
rail-to-rail oscillation by the phase shift.

![(Muntzed schematic)](muntzed.png)

    $ 1 0.000005 1.1685319768402522 50 5 43 5e-11
    v 112 384 112 112 0 0 40 12 0 0 0.5
    w 176 112 400 112 0
    34 fwdrop\q2.1 1 9.32e-11 0 3.51532028522468 0 1
    162 400 176 400 208 2 fwdrop\q2.1 1 0 0 0.01
    34 fwdrop\q1.5 1 9.32e-11 0 4.017508897399634 0 1
    162 400 208 400 240 2 fwdrop\q1.5 1 0 0 0.01
    162 400 144 400 176 2 default-led 1 0 0 0.01
    162 400 112 400 144 2 fwdrop\q1.5 1 0 0 0.01
    t 368 272 400 272 0 1 0.6303830771577537 0.744837805826589 100 default
    r 400 320 400 384 0 10
    r 176 256 176 112 0 1000
    w 176 112 112 112 0
    w 112 384 176 384 0
    w 400 256 400 240 0
    w 240 384 176 384 0
    a 208 272 288 272 9 11 0.1 1000000 0.5740573636701803 0.574573438731456 100000
    r 288 272 368 272 0 1000
    w 400 320 400 288 0
    w 240 320 208 320 0
    w 208 288 208 320 0
    d 176 256 176 384 2 default
    r 400 320 320 320 0 100000
    c 320 320 320 384 0 1e-8 0.5423993515549715 0.001
    r 320 320 240 320 0 220000.00000000003
    c 240 320 240 384 0 1e-8 0.5740573636701803 0.001
    w 240 384 320 384 0
    w 320 384 400 384 0
    403 192 400 288 496 0 22_4_0_4362_1.25_0.00009765625_-1_2_22_3
    403 240 144 368 208 0 2_2_0_36873_5_0.4_-1_2_2_3
    w 208 256 176 256 0
    o 7 4 3 4098 5 0.1 0 2 22 3

<style>
html { background-color: black; color: #ccc }
body { max-width: 35em; margin: 0 auto }
</style>
