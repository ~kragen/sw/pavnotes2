A figure of merit for a cyclic fabrication system for geometry is
voxel power: "voxels per second".  This is necessarily only a crude
approximation for fabrication technologies that are not really
voxel-based, like lathe turning, surface grinding, and wire bending,
but I think it may still be precise enough to be useful.  And for
fabrication systems for things other than geometry, such as refractive
index or semiconductor doping, it may also fail to capture the
relevant aspects of the process.  Still, I think it could be very
useful, since it gives us a figure of merit to try to improve,
comparable between widely different processes at least to within an
order of magnitude or so.

Often such manufacturing processes are instead compared by their MRR
("material removal rate").  But in general there is a tendency for
processes with a greater MRR to have lower precision, which means that
you need to make parts larger to get the same mechanical
functionality.  The objective of "voxel power" is to come up with a
metric to estimate how fast a process can make a given mechanism to
the necessary precision at *any* scale.  Of course, no mechanism
design will function at all scales; at a small enough scale,
sliding-contact mechanisms will be overwhelmed by friction, while heat
engines will be overwhelmed by heat conduction, and at a large enough
scale, thin links will yield or break from the weight or accelerations
of the mechanism.  Generally making a mechanism smaller makes it
faster, stronger in relative terms, but weaker in absolute terms: a
1-mm bearing can spin at higher RPM than the same bearing fabricated
at 10 mm, can bear more times its own weight in load without damage,
but can bear fewer kilograms of load.

My interest in voxel power is justified by my interest in optimizing
the speed to produce a machine with a given *complexity*, regardless
of how big it is, rather than a machine with a given *load capacity*.

In many cases, we can determine the voxel power of a process step by
dividing an MRR by a "voxel size".  We take the "voxel size" to be the
product of *twice* the local geometric precision in each dimension.
To justify this, a photolithographic process step with 2-D resolution
of 10 μm being used to control an acid etch that etches away 2 μm of a
substrate would have a voxel size of 10 μm × 10 μm × 2 μm, or 200 μm³,
2e-16 m³, because either side of the pit could be ±5 μm and the
choices of depth of cut are 0 and 2 μm, so you have an error of 1 μm
with respect to some arbitrary depth you might wish to cut within the
range -1 to +3 μm.  (If you need a larger range of depths of cut, you
can repeat the process step.)  If this process step takes 20 minutes
to pattern and etch a 200 mm wafer, onto which it can selectively etch
some 314 million voxels, that's about a quarter of a megavoxel per
second.

Applying this goofy procedure to vertical milling with a 4-flute end
mill with 10 mm diameter taking 100-μm-thick chips at 3000 rpm as it
feeds along the X axis, with a depth of cut precise to 25 μm in X, Y,
and Z, our "voxels" are 50 μm in Y and Z but about 10 mm in X, because
that's the smallest diameter we can cut.  Moreover, if we're trying to
cut some kind of curvy surface in Z, as the 25-μm number suggests,
only about the last 50 μm of the tooth will be cutting.  We can take
deeper cuts, but at the penalty of much less resolution for the Z
surface we want.  So our MRR is 50 μm in Z, by maybe 3 mm of depth of
engagement in Y, by 100 microns per tooth, 200 teeth per second, for a
total of 3 mm³/s, and 120 "voxels per second".

Common RepRap FDM 3-D printers usually have about 100 μm error in all
three dimensions, though typically they can be adjusted to have
smaller error in Z at the cost of more print time.  These might run at
40 mm per second with a 200-μm layer height.  We could try to
calculate their voxel power from their material deposition rate, but
that seems wrong, because the geometry of the part is very nearly the
same regardless of whether the perimeter is 300 μm or 3 mm, as long as
the error is still 100 μm.  A simple hack is to just say the surface
is 200 μm thick, so our voxel rate is 1600 voxels per second.

If the surface of a 3-D printed vase pits in or juts out by 1 mm, at
40 mm/s that will take an extra 25 ms or so, or an extra 18 ms if it's
at 45°.  Dividing down to twice the error, if there's a bump of 200 μm
(the smallest you can reliably produce) that's about 5 ms.  This
method of calculating suggests only about 200 voxels per second.

The inference here is that an FDM 3-D printer can print working gears
or four-bar linkages about as fast as a CNC milling machine can cut
them, or maybe an order of magnitude faster, but that they will be
much larger (four times linear expansion, so 64 times volume
expansion) for the same smoothness of action.  In crude terms this
seems plausible.

ECM apparati might be able to deliver much higher voxel powers; see
file `ecm-array.md`.  Even low-risk approaches seem likely to be able
to deliver a quarter million voxels per second or so.

Even low-powered laser cutters can routinely deliver much higher voxel
power, with roughly 100-micron precision and much faster travel rates
than FDM 3-D printers.  (I should insert some notes here from
laserboot.)

A thing not included in the voxel-power measurement is sort of the
fullness of the part.  A laser cutter cuts out a solid chunk of a
sheet, so it can make a solid object immensely faster than an FDM
printer, in a way not taken into account by my voxel-power
measurement.  And, all else being equal, it's more useful to be able
to cut out either a 1.5-mm-thick sheet, a 3-mm sheet, or a 6-mm sheet,
than to only be able to cut out a 1.5-mm-thick sheet.  The
milling-machine example is even more extreme: you get the same "voxel
power" whether you're making a 50-μm-deep cut in Z or a 30-mm-deep
cut, but the ability to make either of these cuts instead of just one
of them is very valuable, and the second cut enables you to make a
much stronger part, which is important at large scales.

Of course, material choice also plays into strength: an MDF part cut
with a low-power laser cutter can bear heavier loads than a PLA part
from an FDM printer, which in turn can bear less loads than aluminum,
which bears less than brass, which bears less than steel.  All else
being equal, it's usually best to cut everything from steel or cast
iron; exceptions include:

1. when weakness is desired, for example for safety or to permit easy
   cutting or breaking later;

2. to resist corrosion, for example for food contact or at
   submillimeter scales, though sometimes painted steel is adequate,
   and stainless is a common alternative to steel in this case;

3. for parts so large that the cost of the steel or cast iron would be
   significant;
   
4. when ferromagnetism or electric conductivity would be a problem;

5. when injection molding lowers the cost of a part, where steel would
   require expensive machining, and stamping from steel sheet is not
   an option;

6. for transparency;

7. when steel's TCE of 11 ppm/° is too high, though often cast iron
   can help here;

8. when the density of steel is too high;

9. when higher conductivity like that of copper is needed;

10. when nonsparking surfaces are needed, for example for tools for
    inflammable environments;
    
11. when steel's thermal conductivity would be too high (to avoid
    thermal bridges) or too low (heatsinks, heat exchanger walls);

12. when steel is too soft, for example for abrasive blades for
    cutting ceramic tiles;
    
13. to have lower friction on steel than steel does, for example for
    bearing bronze;

14. to comply with tradition.

The most common reason to not make things out of steel is because it's
too expensive.
