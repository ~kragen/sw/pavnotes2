Fuzzy logic uses some generalization of Boolean
logic to continuous variables.  The most common set
of fuzzy logic operators are the Zadeh operators:

    a ∧ b ↔ (a < b ? a : b)  # a && b  a and b  min
    a ∨ b ↔ (a < b ? b : a)  # a || b  a or b   max
    ¬a ↔ (1 - a)             # !a      not a

These are continuous and provide the correct truth
tables for a, b ∈ {0, 1}, but they are not
differentiable; the derivative fails to exist where
a = b.  (They’re differentiable *almost
everywhere*, but they aren’t differentiable.)

Fairly simple analog circuits can compute these
functions accurately.

Although older artificial neural networks generally
used a biologically-inspired sigmoid activation
function, current work in the field instead heavily
uses a biologically implausible function called
ReLU, “rectified linear unit”:

> ReLU(x) = (x < 0 ? 0 : x)

Like the Zadeh operators, ReLU is differentiable
almost everywhere.  If we remove the usual
restriction of the values to the range [0, 1], we
can express it with the Zadeh operators as just x ∨
0 (x || 0).

It occurs to me that we can express it the other
way around.  a - (a ∧ b) = (a < b ? 0 : a - b).
Because a - b is positive exactly when a < b is
false, that’s ReLU(a - b).  That means we can
define the Zadeh ∧ operator as

    a ∧ b = a - ReLU(a - b)

Analogously, (a ∨ b) - a = (a < b ? a - b : 0),
which is -ReLU(b - a).

    a ∨ b = a + ReLU(b - a)

However, because fuzzy logic is no longer in vogue,
current artificial-neural-network large language
models using ReLU are not called "fuzzy logic
networks".
