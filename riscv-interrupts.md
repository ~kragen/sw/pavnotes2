dh\` explained the following technique to me, as explained to him by
jrtc27:

19:42 < muurkha> I haven't actually implemented RISC-V trap handling so I know I'm kind of treading on thin ice there
19:47 < muurkha> and unfortunately the RISC-V Privileged Architecture manual is not nearly as nicely organized as the user-mode ISA 
                 manual; there's no section that says "Here's how trap handling works" or even "the registers affected by the ECALL 
                 instruction are the following"
19:48 -!- greaser|q [greaser@antihype.space] has quit [Changing host]
19:48 -!- greaser|q [greaser@user/greasemonkey] has joined #riscv
19:48 -!- greaser|q is now known as GreaseMonkey
19:57 < dh`> the conventional way to do it is to leave the kernel stack address in sscratch while running in userland, and null when in 
             the kernel
19:58 < dh`> then on trap entry you exchange sp with sscratch, if it's null exchange back, then push all the registers on the stack, 
             then you can do real work
19:59 < muurkha> oh right because sscratch is preserved between trap handler invocations
19:59 < muurkha> how do you exchange sp without using a third register as scratch space?
19:59 < dh`> taking a trap disables interrupts and there's an assumption in most environments that it's ok for faults in the trap 
             handler to be nastily lethal
20:00 < dh`> that's why the control register access instructions are so complex
20:01 < muurkha> hmm, sounds like I need to review Zicsr
20:03 < muurkha> to whom should I credit this explanation?  dh`, dholland, David Holland, David A. Holland, or "a helpful person"?
20:03 < dh`> it is not much headroom and it's not really an optimal arrangement
20:03 -!- ___nick___ [~quassel@cpc68289-cdif17-2-0-cust317.5-1.cable.virginm.net] has quit [Ping timeout: 250 seconds]
20:04 < dh`> credit jrtc27
20:04 < muurkha> I was thinking that sometimes you want recursive interrupt handling
20:04 < muurkha> haha
20:04 -!- frkzoid [~frkazoid3@2603-9000-cf00-31f9-a0c8-f914-33f7-c0b4.inf6.spectrum.com] has joined #riscv
20:04 < dh`> yeah, but you don't turn interrupts back on until you've finished saving state
20:04 < muurkha> right
20:04 < muurkha> but she has been silent!  is she using your terminal?
20:04 < dh`> otherwise it's unrecoverable
20:04 < dh`> shyeah
20:05 < dh`> no, she explained that technique to me a couple years ago after I failed to figure it out for myself
20:05 < muurkha> it seems like it would be adequate to save sscratch, sepc, scause, stval, and sip?
20:06 -!- frkazoid333 [~frkazoid3@2603-9000-cf00-31f9-a0c8-f914-33f7-c0b4.inf6.spectrum.com] has joined #riscv
20:06 < muurkha> well, if you don't mind, I'll explain, "dh` explained the following technique to me, as explained to him by jrtc27"
20:07 -!- freakazoid332 [~frkazoid3@2603-9000-cf00-31f9-a0c8-f914-33f7-c0b4.inf6.spectrum.com] has quit [Ping timeout: 250 seconds]
20:08 < dh`> if you really want to
20:08 < dh`> anyway, no, you have to save the entire register file on interrupt or you lose the prior machine state
20:09 -!- frkzoid [~frkazoid3@2603-9000-cf00-31f9-a0c8-f914-33f7-c0b4.inf6.spectrum.com] has quit [Ping timeout: 250 seconds]
20:11 < muurkha> well, you do have to save and restore the entire register file eventually, or at least whatever registers you're going 
                 to clobber
20:11 < dh`> well, in theory you can avoid saving the callee-save registers because your own code should preserve them, but nobody does 
             that
20:11 < muurkha> but what I meant was that I think you can re-enable interrupts before that
20:12 < muurkha> because if an interrupt comes in before you've saved registers like x8, that interrupt handler is responsible for 
                 saving and restoring x8, and then your own handler resumes execution and saves them
20:12 < dh`> ah
20:13 -!- davidlt_ [~davidlt@78-63-27-146.static.zebra.lt] has quit [Ping timeout: 272 seconds]
20:14 < dh`> possibly, but you can't save the control registers without first saving at least one general-purpose register
20:15 < dh`> and it's probably better to save a bunch of general-purpose registers and then pull the control registers all at once, 
             unless you know for certain that you're only executing on an in-order cpu
20:16 < dh`> also if you are relying on sscratch being null to know when you came from the kernel, you can't turn on interrupts until 
             you null sscratch
20:17 -!- dor [~erg@188.147.4.71.nat.umts.dynamic.t-mobile.pl] has quit [Remote host closed the connection]
20:18 < dh`> anyway in general in a kernel you want to examine the trap code before you do much of anything else, and it's much nicer to 
             do that in C code, so the normal thing is to just push everything and call into C code, which then figures out what to do.
20:18 < dh`> for small embedded device firmware there are different considerations (which is I assume how arm trap handling came to be 
             so weird)
20:20 < muurkha> yeah, I was thinking of small embedded devices where, as I understand it, minimizing worst-case interrupt latency is 
                 sometimes a key consideration
20:21 < dh`> yes
20:21 < dh`> in that case everything is different
20:21 < muurkha> does swapping x2 and sscratch back if sscratch was null imply that you're saving the registers on the user's stack?
20:22 < dh`> no, the idea is to always dump the trapframe on the kernel stack
20:22 < dh`> in order to do that, you need to (a) know if you came from userland or not and (b) if you did, locate the kernel stack
20:22 < muurkha> that seems more desirable yeah
20:23 < dh`> on a multi-cpu machine you need to be able to find the kernel stack without any easy means of knowing which cpu you are
20:23 < dh`> and without any headroom to speak of to work too
20:23 < dh`> hence sscratch
20:23 < muurkha> why do you need to know if you came from userland or not?  because if you came from userland the kernel stack pointer 
                 is in memory, while if you came from kernel-land it's in x2 (and may not be updated in memory)?
20:24 < dh`> if you came from userland, you need to fetch the starting kernel stack from somewhere and then dump registers into it
20:24 < dh`> if not, sp already has the stack you want to use
20:24 < dh`> and it's incorrect to reload it from the top because that destroys the existing state
20:25 < dh`> so yeah
20:25 < dh`> on machines with adverse layouts there are ways to cope but it gets expensive
20:26 < muurkha> hmm, I forget how you find out what hart you're running on
20:26 < dh`> not sure you do, particularly in s-mode
20:27 < dh`> tends to be a mainboard function rather than a cpu one
20:28 < dh`> also even when there's a way it tends to not be suitable for trap handling
20:28 < muurkha> but I guess on a single-hart machine you would overwrite the 0 you swapped from sscratch into x2 with a constant 
                 address for the stack pointer (either lui;addi or auipc;addi) and then a load to get the vaue of the stack pointer for 
                 memory
20:28 < muurkha> *from memory
20:29 < dh`> I have not really thought about the specific way you might do it on a uniprocessor riscv
20:30 < muurkha> so how do you cope on machines with adverse layouts?
20:30 < dh`> you still need to know if you came from userland and there isn't enough headroom to figure that by looking at the status 
             register, so you're still using sscratch to indicate
20:30 < dh`> at which point putting anything other than the kernel stack address in there is pretty silly
20:30 < dh`> typically you make a per-cpu memory mapping and spill enough there to make room to work
20:31 < muurkha> aah, I see
20:32 < dh`> on arm you don't need the per-cpu mapping but you pretty much do need a per-cpu spill area
20:32 < muurkha> yeah, I don't know enough about arm to know where you might stash its address there
20:33 < dh`> arm trap handling is very weird
20:33 < dh`> not recommended.
20:33 < muurkha> heh
20:33 < dh`> there's a different cpu mode for each trap type
20:33 < muurkha> well, I'll probably have to be doing it pretty soon, but fortunately on a uniprocessor
20:34 < dh`> and some of the registers are bank-switched depending on that mode
20:34 < muurkha> and it's a Cortex-M, so I'm not sure if it even has different CPU modes
20:34 < muurkha> yeah, bank-switching registers is a nice cheap way to do trap handling
20:34 < muurkha> I think the Z80 was the first time I saw that
20:34 < dh`> it does if it has traps, which it must
20:34 < dh`> it is not nice the way arm did it, and not cheap if you swap the whole register file
20:35 < muurkha> yeah, how do you save the registers that are bank-switched out if you decide you need to do a context switch on return?
20:35 < muurkha> you need some backdoor
20:35 < dh`> basically there's a different return address register for each mode and that's where the pre-trap program counter lands, so 
             you have to save that before switching out of trap mode
20:36 < dh`> depending on the version there may be ways to access the other modes' registers
20:36 < dh`> I have no idea what model they had in mind when designing it; it isn't consistent with any normal kernel proceedings
20:37 < muurkha> if you control the whole system you could reserve one of the ordinary registers for trap handling (x3, say) to get a 
                 little more headroom for trap handling
20:37 < dh`> most processors you can tell if you read the docs enough that there's a way you're supposed to do things
20:37 < muurkha> (on RISC-V, I mean)
20:37 < dh`> mips does that
20:37 -!- rburkholder [~blurb@96.45.2.121] has quit [Remote host closed the connection]
20:38 < dh`> arm has only 16 registers and they burn one slot by mapping the PC
20:38 < dh`> so that's not a realistic option there
20:39 < muurkha> yeah
20:40 < muurkha> it's also not useful for conventional OSes, which load arbitrary machine code and run it in user mode, relying on the 
                 CPU to protect the kernel from user-mode
20:41 < dh`> it is if it's part of the ABI
20:41 < dh`> also, if you're an OS you at least notionally control the compiler and can rearrange its register handling
20:41 < muurkha> well, one question is using x3 as an extra scratch register in trap handlers
20:42 < muurkha> which means user-mode code isn't entitled to assume its value is ever preserved
20:42 < muurkha> something different is your interrupt handlers relying on your background-execution code to never clobber x3, even 
                 temporarily, which the ABI does sort of strongly suggest
20:43 < muurkha> I think that at the point you depend on the compiler for security you're out of "conventional OS" territory and you 
                 don't really need separate user and kernel mode at all
20:43 < dh`> clobbering user registers isn't a security issue
20:43 < muurkha> which I think is a very interesting and under-explored design space!
20:44 < dh`> unless you leak kernel information
20:44 < muurkha> right, but "user" code clobbering "kernel" registers is
20:44 < dh`> sure
20:44 < dh`> using x3 as a _scratch_ register in trap handling so it's clobbered by every trap is one thing
20:44 < muurkha> yeah.  but, like, storing the "kernel" stack pointer in x3
20:44 < dh`> relying on user code not to change it won't do
20:45 < dh`> I used to think single-user systems that don't need protection were an interesting design space
20:45 < dh`> then browsers happened
20:46 < geist> it's a choice riscv made (though i think this stuff follows pretty closely what MIPS was already doing). there are no 
               banked registers, especially banked SP, that some other arches do to solve this problem
20:46 < dh`> it is not the same as mips
20:46 < geist> i do wish riscv had just a few more exception entry points, like one from when you were in U mode vs S mode which would 
               also solve it
20:46 < dh`> yeah
20:47 < geist> the sscratch trickery to figure out which mode you were from is pretty grody
20:47 < dh`> every extra config register is more hypervisor overhead and I'm sure that's the reason
20:47 < geist> yah and it's tuned for allowing there to be *one* entry vector for everything
20:48 < geist> which is a noble goal, but a little unweildy. but so it goes, only a handful of people really ever need to care about 
               this :)
20:48 < dh`> I've ranted at length before about how the riscv privileged stuff is all wrong
20:48 < muurkha> dh`: I'm not talking about systems that don't need protection; I'm talking about systems that implement the protection 
                 in a compiler rather than in hardware
20:48 < muurkha> oh really?  where can I read your rant?
20:49 < dh`> you've probably seen most of it here
20:49 < dh`> "implement the protection in a compiler" -> "has no protection" :-)
20:49 < muurkha> you mean, just now?
20:49 < geist> iirc, microbalze has a similar model but iirc i think the hardware simply blows away the 'k0' register. ie, r15 or 
               whatnot is simply overwritten by exceptions, so it's hardware forced that user had better not touch that reg
20:49 < dh`> no, I mean over the last couple years
20:50 < muurkha> that's a bummer, because I haven't kept logs :(
20:50 < geist> but i think it's cause they're even simpler than riscv. it puts what would be in the epc or whatnot in a GP reg
20:50 < muurkha> hmm, I think you can implement working protection in a compiler, at least for some architectures (not for Intel's 
                 amd64, obviously)
20:50 < dh`> for any architecture
20:50 < dh`> why not amd64?
20:51 < geist> but the nice thing is you already have a GP reg with the infos you need in it to do whatever logic at the top of your 
               exception
20:51 < dh`> the problem is that compilers are too complex to be correct so working doesn't imply secure
20:51 < muurkha> it would be feasible for amd64 in the abstract, but there have been a lot of timing leaks in recent years due to 
                 speculative execution
20:51 < dh`> unless you redo compcert
20:52 < muurkha> and the mitigations for spectre make context switches a lot more expensive
20:52 < geist> also a problem is in general kernels are user runtime agnostic. user space may agree to play along and not trash a reg, 
               but in general kernels cannot and shall not ever trust that user space follows the rules, or runs this or that runtime
20:52 < dh`> oh, in that sense you're out of luck, you will never avoid timing leaks with a compiler
20:52 < dh`> or maybe you will but that's a 5-10 year research program in secure compilation
20:52 < geist> ie, kernels shall always treat user code as hostile
20:52 < muurkha> I think it's feasible to avoid timing leaks with a compiler, but not the kind of compilers you're thinking about ;)
20:53 < muurkha> and only on hardware whose timing is pretty well nailed down
20:53 < dh`> there is no such hardware you'd want to run a general-purpose os on
20:54 < dh`> geist: it's reasonable to have a world where only binaries from the system compiler are allowed, and there are various 
             possible ways to enforce that
20:54 < muurkha> well, it certainly excludes anything like Linux, yeah
20:54 < geist> one could build some sort of tightly coupled appliance etc where you do that
20:54 < dh`> pff, you could do it with unix
20:54 < geist> OTOH those sort of systems also are in general not as suceptable to meltdown/spectre stuff because they aren't running 
               arbitrary code
20:55 < muurkha> you could do some kind of Unix
20:55 < dh`> (require that all binaries come from the system compiler)
20:55 < geist> yeah i mean you *could* do that. i just wouldn't want to use it
20:55 < muurkha> you could do some kind of Unix while providing protection between processes with only compilation
20:55 < dh`> without a lot of work it would basically require making the compiler setugid and there would be a million leaks, but it's 
             at least possible in principle
20:55 < geist> but yeah, totally
20:55 < muurkha> what I'm thinking is a very simple compiler which compiles from a pretty low-level abstract machine to the physical 
                 instruction set
20:56 < muurkha> so your compiler for C or Lua or whatever would target the abstract machine, and wouldn't be in the TCB
20:56 < dh`> it is hard to have a safe abstract machine that you can target with C
20:57 < dh`> oh also someone should have mentioned CHERI by now
20:57 < muurkha> well, webassembler kind of did it
20:57 < muurkha> yeah, I've been thinking about CHERI a lot
20:57 < muurkha> *webassembly
20:57 < dh`> I don't think you can write a fully compliant C compiler that targets webassembly
20:57 < muurkha> why not?
20:58 < dh`> because there are things you're allowed to do in C that are inherently unsafe
20:58 < muurkha> being compliant C doesn't mean that those unsafe things do anything useful though
20:58 < dh`> but in particular, you can inspect the representation of any data element
20:58 < muurkha> webassembly puts all your data elements in a "linear memory" made of bytes
20:59 < muurkha> so a single "module" (think "Unix binary") is running on top of a large byte array
20:59 < dh`> actually, I take it back. it's absolutely possible, it's just likely prohibitively expensive
20:59 < muurkha> current implementations of C on Webassembly are reasonably efficient though
20:59 < dh`> they probably aren't fully compliant
21:00 < dh`> and that's probably fine, too, but there will be things that don't work
21:00 < muurkha> and they don't keep you from, for example, reading an int as 4 or 8 bytes
21:00 -!- motherfsck [~motherfsc@user/motherfsck] has quit [Ping timeout: 268 seconds]
21:01 < muurkha> I admit I haven't looked at how llvm handles tricky cases, but it certainly intends to be ANSI-compliant
21:01 -!- Gravis [~gravis@user/gravis] has quit [Quit: No Ping reply in 180 seconds.]
21:01 < dh`> what happens if you send a pointer through a pipe?
21:02 -!- Gravis [~gravis@user/gravis] has joined #riscv
21:03 < dh`> you can make pointers be indexes into the linear memory and mask them at every use
21:04 < dh`> but that gets really expensive
21:04 < dh`> maybe that expense no longer matters; mostly performance no longer matters
21:05 < muurkha> yes, pointers are indexes into the linear memory
21:06 < muurkha> it's not usually that expensive because, though that implies a base-address addition and bounds check on every pointer 
                 dereference, you can usually hoist those out of inner loops
21:07 < dh`> more like "sometimes", loop analysis is Hard
21:07 < muurkha> but wasm also has another big advantage, that its virtual machine has local variables that aren't stored in memory
21:08 < dh`> and one of the problems with compiling is that the language gives enough latitude that you have to be very conservative
21:08 < muurkha> and, as I understand it, you can compile scalar C local variables into wasm local variables unless you take their 
                 addresses
21:08 < dh`> that's basically the same as registerization
21:08 < muurkha> yes
21:09 < dh`> just you get 256 or 65536 or whatever registers
21:09 < muurkha> right, and you don't have to save and restore them on call and return
21:09 < dh`> not explicitly at least
21:10 < muurkha> right, if the wasm compiler allocates registers to them, it has to save and restore them!
21:10 < muurkha> but it might just leave them in memory and index off a frame pointer
21:11 < dh`> er. s/one of the problems with compiling/one of the problems with compiling C/ above
21:11 < dh`> anyway
21:11 < muurkha> yeah, that's what I thought you meant :)
21:13 < muurkha> anyway I think the main design goal for wasm was for llvm to be able to compile any compliant C++ to it and get 
                 reasonable performance
21:13 -!- motherfsck [~motherfsc@user/motherfsck] has joined #riscv
21:14 < muurkha> so for example three years ago https://www.usenix.org/system/files/atc19-jangda.pdf found slowdowns of 45% for Firefox 
                 and 55% for Chrome for the SPEC CPU benchmarks
21:15 < dh`> on this topic, have a look at the rocksalt paper if you haven't yet
21:15 < muurkha> rocksalt paper?
21:15 < dh`> from... PLDI 2012
21:15 < dh`> verified runtime for google native client
21:16 < muurkha> aha, thanks!
21:16 < muurkha> yeah, native client is where wasm kind of came from
21:17 < dh`> right
21:17 < muurkha> I think the typical wasm slowdown is lower now than it was three years ago
21:18 < dh`> 50% is a lot even today
21:18 < muurkha> and I don't think that's based on breaking ANSI C or C++ compliance
21:18 < muurkha> I don't know, is it?
21:19 < dh`> I would describe 50% as "not performant" but then, I come from an era where we cared about 5%
21:19 < dh`> which people definitely do not today
21:19 < muurkha> people typically accept 2000% slowdowns by writing single-threaded C or 100% slowdowns by running their applications on 
                 the CPU instead of the GPU
21:20 < dh`> that is not the same
21:20 < muurkha> sometimes on top of an additional 500% slowdown by writing their code in JS or 4000% by writing in in Python
21:20 < muurkha> isn't it?
21:20 < dh`> python is, not spending six months to parallelize something is a bit different
21:22 < muurkha> I guess there probably aren't a lot of situations where compiling your code to wasm helps you to parallelize it across 
                 cores.  more the opposite actually
21:22 < dh`> I mean, in some sense it's a continuum, people choose to write in Python because they think writing in a more performant 
             environment will take much more work
21:22 < dh`> (and sometimes they're right)
21:22 < dh`> wasm is not concurrent at all iirc
21:22 < muurkha> but there might be other abstract machines implemented in a wasm-like fashion that *would* help you parallelize across 
                 cores
21:23 < dh`> unlikely
21:23 < dh`> parallelization is hard
21:24 < muurkha> well, in particular, if you can enforce STM with a JIT-compiling virtual machine inside your TCB, there are a 
                 substantial number of programs that would become easier to parallelize than using threads-and-locks approaches
21:24 < dh`> no?
21:24 < muurkha> and I feel that a JIT compiler from a simple virtual machine is a much more appealing place to implement transactional 
                 memory than in hardware
21:25 < dh`> I mean, apart from STM itself in general not being performant
21:25 < muurkha> Keir Fraser's dissertation seemed to show some pretty decent performance numbers for his STM implementation
21:26 < muurkha> faster than the non-STM state of the art in many cases, in fact
21:26 < dh`> I haven't looked in a long time but it used to be that STM slowdowns were measured in orders of magnitude or at least N 
             times slower, not percentages
21:26 < muurkha> this was before the STM work you were doing, I think, so maybe I'm just wrong?
21:27 < dh`> idk
21:27 < dh`> I am a long way from up to date on that topic
21:27 < dh`> and the work I was doing was very non-mainstream
21:28 < muurkha> this was in 02004: https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-579.pdf
21:28 < muurkha> pp. 91-100
21:30 < dh`> yeah, looks like 3-4x slowdown for STM
21:30 < muurkha> hmm, yeah, it does
21:30 -!- cwebber [~user@user/cwebber] has quit [Remote host closed the connection]
21:31 < muurkha> it was his MCAS implementations that were actually faster than the SOTA, not his STM implementations
21:32 < dh`> which is not surprising
21:32 < muurkha> well, it depends on the task
21:32 < dh`> the scaling result in figure 6.3a is interesting but it's also a stupid task
21:33 < muurkha> for skip lists (figure 6.1) his FSTM was about 2.5-4 times slower than CAS-based, MCAS-based, or per-node locks
21:34 < muurkha> well, let's say about 2.5× slower.  it was only Herlihy STM that was 4×
21:35 < dh`> anyway, if you are choosing platforms and aggressive scaling is a consideration, you want to be writing in a channel-based 
             language
21:35 < muurkha> for binary search trees (figure 6.2) he didn't do an STM implementation, and for 6.3 (red-black trees) he got much 
                 better performance with STM than with locks, but didn't try an MCAS-based implementation
21:35 < dh`> and forget about locking shared memory
21:36 < dh`> red-black trees are pretty much optimized for contention, you can't really conclude much from that
21:36 < muurkha> heh
21:37 < muurkha> well, his STM seems to have handled the contention better than his lock-based implementation, but I agree that that's 
                 not super compelling
21:40 < dh`> cache contention is absolutely awful so anything that doesn't do that will win, no matter how slow it is
21:40 < muurkha> in absolute terms it doesn't look too bad though
21:40 < muurkha> he was getting like 12 microseconds per update or read from a large skip list on 1.2 GHz UltraSPARCs, up to 4 
                 processors or so, so maybe 2000 clock cycles
21:41 < muurkha> wait what am I saying
21:41 < muurkha> 14000 clock cycles
21:42 < dh`> also all of that is obsoleted by rcu-style techniques where reads are free
21:42 < dh`> if you only contend for writes, in a _large_ skip list or tree you'll ~never contend
21:43 < muurkha> yeah, RCU is a pretty big win there, and it doesn't have the impedance mismatches with things like I/O and locks that 
                 transactions do
21:43 < dh`> at least if accesses are reasonably distributed around the tree
21:43 -!- awita [~awita@user/awita] has joined #riscv
21:43 < muurkha> but I don't have experience programming with RCU and I have the impression that it's extremely hairy?
21:44 < dh`> neither do I, because patents are evil, but yes and no
21:44 < dh`> we have been using a related technique in netbsd and the interface is not much different from a lock
21:45 < muurkha> that sounds extremely damning ;)
21:45 < dh`> but that's not multiversion
21:45 < dh`> anything multiversion gets horribly messy unless you have language-level support
21:45 -!- pedja [~pedja@user/deus-ex/x-7934090] has quit [Quit: Leaving]
21:46 < dh`> and if you do, it tends to get expensive because part of what you trade off for not having to see the gears turning is 
             control over allocation
21:46 < muurkha> yeah, I don't really understand how you're supposed to do allocation in Fraser's FSTM
21:47 -!- awita [~awita@user/awita] has quit [Client Quit]
21:48 -!- epony [~epony@user/epony] has joined #riscv
21:49 < dh`> my opinion for the moment is that if you have something that really demands massive parallelism, you should write it using 
             messages so you can run it on a shared-nothing cluster supercomputer
21:49 < dh`> and if you don't, it's not worth worrying about, and more than 32-64 cores on the desktop is a solution in search of a 
             problem
21:52 < dh`> and also, there are very few real user workloads that can actually make use of more than more than a handful of cores
21:52 < muurkha> 64 cores is already kind of a lot though
21:52 < dh`> and most of _those_ are limited by RAM rather than the core count
21:52 < dh`> like make -j
21:53 < muurkha> and maybe the reason real user workloads can't actually make use of more than a handful of cores is that they're 
                 written in paradigms where parallelizing things takes six months and requires dealing with interfaces that are not much 
                 different from a lock :)
21:53 < dh`> <dh`> anyway, if you are choosing platforms and aggressive scaling is a consideration, you want to be writing in a 
             channel-based language
21:56 < dh`> writing a kernel in a channel-based language is one of those things that's been on my "sometime" project list for years now
21:56 < dh`> (held up partly by the lack of a suitable language implementation)
21:57 < muurkha> it sounds like fun
21:57 < muurkha> you don't like golang or erlang?
21:57 < dh`> anyway we have drifted way, way offtopic
21:57 < dh`> (though I enjoy these kinds of discussions)
21:58 < dh`> go is not suitable for kernels (has a nontrivial runtime and a GC)
21:58 < dh`> and erlang has ... issues
21:58 < muurkha> true
21:58 < muurkha> erlang also has a nontrivial runtime and a GC
21:58 < muurkha> and unlike Golang you can't avoid generating garbage all the time
21:58 < dh`> for me it fails before that on being dynamically typed
21:59 < muurkha> yeah, that's not really my favorite attribute for kernels ;)
22:00 < dh`> it should be possible to avoid needing heap allocation at all (except for threads themselves) in a channel language
22:01 < dh`> without most of the contortions this requires in an ordinary language
22:01 < muurkha> yeah, Erlang is the opposite extreme from that
22:01 < muurkha> because everything is immutable and there is no linearity in the type system (because it doesn't have a nontrivial 
                 static type system)
22:01 < dh`> because anytime you need to have a persistent thing, you just make it a thread
22:02 < muurkha> right
22:02 < dh`> every vnode is its own thread
22:04 < dh`> some real questions about how the kernel provides threads that are written in this, but I'm sure it can be done
22:04 < dh`> however, the last thing I need is another kernel project...
22:13 -!- fanta1 [~fanta1@p200300e2b7214f007137ae43a4197faa.dip0.t-ipconnect.de] has quit [Quit: fanta1]
22:21 < muurkha> heh
22:22 < muurkha> basically the extremist Actors approach
22:26 < muurkha> if there's *no* allocation you can just point your frame pointer at the vnode or whatever; the point at which providing 
                 threads becomes complicated is when you need to dynamically allocate stacks for them
22:29 < dh`> right, but if _all_ you're allocating is threads that's a considerable simplification
22:29 < dh`> of course, it probably won't work, since there are such things as strings
22:32 -!- mz___ [~xmz@123.122.76.77] has joined #riscv
22:34 < muurkha> Hewitt doesn't shrink from creating an Actor for each string, and if their length is immutable string-append can 
                 allocate a new string, but at that point you start really wishing for a global GC
