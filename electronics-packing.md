In file `packing-list.md` I've been measuring the weight of things
that go into my backpack and trying to figure out how to maximize
mobility and resilience.  I thought I'd list the weights of a few
electronics hobbyist thingies:

- Artemis Apollo3 RedBoard (subthreshold Arduino-compatible): 20.45g.
- ESP32-C3 board: 7.35g.
- SparkFun TensorFlow Edge board (subthreshold devboard): 8.08g.
- Adafruit 400x240 2.7" SHARP Memory LCD breakout: 21.54g.
- Arduino Uno: 23.64g.
- Arduino Duemilanove: 27.56g.
- 1.5m (?) USB-A to B cable for the Uno or Duemilanove: 55.12g.
