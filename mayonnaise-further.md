I have just made 505 grams of mayonnaise in a 601-gram bowl with a
19-gram fork, totaling 1125 grams, including 52 grams of yolks (the
yolks of three 67-gram eggs), in 55 minutes.  I used 4 grams of salt,
which was a bit too much, and 18 grams of lemon juice; the other 431
grams was oil, mostly sunflower, but with 14 grams of olive oil to
start.  The last addition of oil was 59 grams and made a very
noticeable improvement in the consistency; it raised the oil:yolk
ratio from about 7.2 to about 8.3 (25:3, so lemon-juice:yolk:oil is
about 1:3:25).  I think I could probably get by with two or three
times that much olive oil, reducing the sunflower oil by the same
amount.

So the mayonnaise is 85% oil, 10.3% yolk, 3.6% lemon juice, and 0.8%
salt.  That was too much salt.

Oil is 9kcal/g, and egg yolk is [3.2kcal/g][0], and by weight the yolk
includes 16% protein, 27% fat, 3.6% carbohydrates.  85% of 9 is 7.6,
and 10% of 3.2 is 0.32, so that's about 7.9kcal/g, and I have just
whipped up 4000 kcal of mayonnaise.

[0]: https://fdc.nal.usda.gov/fdc-app.html#/food-details/172184/nutrients

[Bottled lemon juice][1] is by weight 93.6% water, 0.5% protein, 0.1%
fat, 5.7% carbohydrate, and 0.2 kcal/g, so it adds an insignificant
0.01 kcal/g to the mayonnaise.  It’s not much of a cost driver,
either, because the bottle is 500 grams and costs about US$1.50, about
0.3¢/g, so this mayonnaise contained 5.4¢ of lemon juice.

[1]: https://fdc.nal.usda.gov/fdc-app.html#/food-details/167802/nutrients

Eggs are $2400 for 12, and the dollar is at $1200 today, so that’s
US$2 for 12 eggs or 17¢ per egg.  Three eggs is 50¢.

900g of sunflower oil costs US$1.40, 0.16¢ per gram, so 431 grams
would be 69¢.

Retail salt is about US$3/kg, so 4 grams is 1.2¢.  [Wholesale salt][2]
is only 40¢/kg.

[2]: https://articulo.mercadolibre.com.ar/MLA-870878775-sal-entrefina-seca-321-x-25-kg-_JM

My total BOM cost, then was US$1.25: 55% oil, 40% eggs, 4% lemon
juice, 1% salt.  That’s US$2.47/kg, which is almost exactly the same
price as supermarket mayonnaise (which I calculate as US$2.51/kg from
prices on Mercado Libre), but it tastes better and is nutritious.  An
advantage I didn’t employ here is that I can add things like garlic or
onions to the mayonnaise as I make it.

I also fried up the whites, which totaled 110 grams, and discarded 28
grams of shells, so the 199 grams of eggs were 14% shell, 55% white,
and 26% yolk.  9 grams of egg mysteriously disappeared, a 5% error.

I added the oil as 14 grams of olive oil to bring the total weight to
687 grams at 17:32, then 19 grams of sunflower oil to 707 grams at
17:35, 21 grams to 728 grams at 17:36, 40 grams to 768 grams at 17:39,
61 grams to 829 grams at 17:43, 58 grams to 887 grams at 17:48, 62
grams to 949 grams at 17:55, 56 grams to 1005 grams at 17:59, 61 grams
to 1066 grams at 18:08, and 59 grams to 1125 grams at 18:15.  In total
these ten additions total 451 grams, not 431, so there’s a 20-gram
weighing error somewhere (maybe from repeated tasting).  I probably
could have done it much more quickly, but I was proceeding cautiously
in part because I didn’t know the actual proportions of my endpoint.

A more aggressive addition schedule totaling 451g would be something
like 40 grams, 40 grams, 80 grams, 120 grams, 171 grams.  I don’t
think the emulsion would “break” with less than the egg yolks’ own
weight of oil added.

(Two days later, when we finished the mayonnaise, it had separated a
few drops of oil, so perhaps it was not thoroughly emulsified.)
