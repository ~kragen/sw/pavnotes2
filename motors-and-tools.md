I was wondering what new possibilities there are for super-powered
hand tools, inspired by the perception that drone motors now permit
hand tools to be as powerful as bench-mounted tools historically have
been.

High-power quadcopter motors are amazing but need forced-air cooling
--------------------------------------------------------------------

A Turnigy A2204-14 brushless motor for quadcopters costs US$30, not
including the ESC, weighs 19 grams, runs at up to 7.5 amps, has a KV
of 1450 rpm/V, and is specified for 2-3 cells (i.e., up to 11.1 volts
and thus 16000 rpm).  Its rated consumption is thus up to 83 watts
(4.4 W/g); it’s rated for 74% efficiency, which would be 62 watts of
output, though probably its efficiency is lower at max load.

But that is a very small quadcopter motor.  Typical quadcopter ESCs
are 30 amps and cost US$25.  A US$90 ESC might handle 60 amps and up
to 16.8 V, which is a kilowatt.  I’m having a hard time finding such
high-power brushless drone motors, but for example the NTM 3536
(“35-36a”?) is KV = 1400 rpm/V, 121 grams, 14 volts max (thus 20
krpm), 45 amps max, 35 mm diameter, 36 mm length, and nominally 550
watts (4.5 W/g).  Dividing the nominal mass by the dimensions gives
3.5 g/cc.  It costs US$75.

I’m thinking that probably a 70%-efficient 1000-watt 20krpm brushless
drone motor is a thing, it probably weighs about 250 g, and it’s
probably about 44 mm diameter and 46 mm long and costs about US$150.
It surely needs forced-air cooling to dissipate its 300 watts of
wasted heat; if we assume a delta-T of 100 degrees (from, say, 20
degrees to 120), and 1 J/g/K for the cooling air, we need 3 grams per
second of cooling air.  At 1.2 g/l that’s 2.5 liters/sec, 5.3 cfm, or
0.15 m³/min, achievable but nontrivial.  Through a 44 mm diameter
circle that’s 1.6 m/s of airflow, which is pretty reasonable; leaf
blowers commonly manage 50 m/s and 2-4 m³/min.  Still, to do it in
that size, you probably need a pretty noisy device, likely a
centrifugal blower.

1kW at 20krpm (2090 radians/sec) is only 0.5 newton-meters of torque,
maybe 0.35 Nm if the motor output is only 700 W.  You need to gear
this down a lot for most uses.  There’s a differential setup for
planetary roller screws where the screw and the nut run in opposite
senses; if they are at slightly different pitches, the lead (effective
thread pitch) of the overall mechanism is the *difference* in the thread
pitch between inner and outer threads, in theory permitting
arbitrarily large mechanical advantages.  You could imagine, for
example, a two-tonne car jack being driven directly at 0.95 mm/s
through such a mechanism with an effective thread pitch of 350 threads
per millimeter, if the screw has a thread pitch of 1 mm and the nut
has a thread pitch of 1.0029 mm.  (But perhaps an earlier stage of
reduction would be desirable so that the two-tonne screw doesn’t have
to spin at 20k rpm.)

Sufficient fans are essential but can easily be added
-----------------------------------------------------

An [RS Pro 787-4013 ODB7530-24HB][0] fan is 24VDC, 160 mA (thus 4 W),
3000 rpm, 10 cfm, 75 x 75 x 30 mm, 38 dBA, 0.35 inches water
stagnation pressure (90 Pa), 0.17 inches water pressure at its max
efficiency of 5 cfm (45 Pa, thus 110 mW output) and US$30.  A [Nidec
Gamma 28 A33475-68][1] fan is 10 cfm max, 12 VDC, 290 mA (thus 3.5 W),
and US$7, 76 mm diameter, 29 mm thick, 450 g, with a 24 mm x 35 mm air
outlet.  A [Shyuan Ya AD50-10T axial fan][2] is 50 mm diameter, 20 Pa,
24-30 dBA, 10 mm thick, 15.3 g, 0.8-1.7 W, 8-10 cfm, 4000-5000 rpm,
and at max efficiency 12.5 Pa and 5 cfm.

[0]: https://uk.rs-online.com/web/p/centrifugal-fans/7874013
[1]: https://www.surpluscenter.com/Electrical/Blowers-Fans/DC-Centrifugal-Blowers/10-CFM-12-Volt-DC-Nidec-Gamma-28-Blower-16-1545.axd
[2]: https://www.coolingfanmanufacturers.com/Product/2394/50mm-portable-mini-low-watt-10-cfm-blower-fan

These numbers make me think that it should be pretty easy to whip up a
centrifugal or maybe even axial blower that is driven by under 1% of
the motor’s mechanical output power and is sufficient to cool it.  In
a variable-speed application, if you aren’t going to use a separate
motor to drive the fan, some cleverness is needed to ensure that the
fan is driven fast enough even when the motor is running at 10% of its
maximum speed.

AC motors are only barely cheaper and are much heavier
------------------------------------------------------

By contrast, a [non-sealed 1 horsepower = 750 W capacitor-started
single-phase AC motor][3] for a portable cement mixer costs US$90,
weighs 8 kg, and delivers 1500rpm.  This is probably slightly cheaper
than the ESC and drone motor of the same power capacity (US$25 for the
ESC plus US$90 for the motor, say) and doesn’t need a cooling fan, and
it might have higher reliability, but it weighs forty times as much
and can’t vary its speed.

[3]: https://articulo.mercadolibre.com.ar/MLA-1103248926-motor-hormigonera-trompito-hasta-180-l-1-hp-daf-mezcladora-_JM

Drone batteries are comparable in weight and cost to their motors
-----------------------------------------------------------------

Drone batteries are also extremely impressive.  A 7.4 V 2500mAh 30C
85mm x 34mm x 18mm battery theoretically costs US$30 and probably
weighs about 100 g; 30C means that you’re supposed to be able to
discharge it at the rate that would fully discharge it over a 30th of
an hour, which is to say, two minutes.  In this case that’s 75 amps
and 560 watts, on the order of 5 watts per gram.

And this is I guess why modern battery-powered power tools are so
impressive: a rechargeable one-kilowatt battery, motor, cooling fan,
and electronics can fit in your hand (though not quite in the palm of
your hand) and weigh 500 grams.  Another factor of two or so and you
have a version that can additionally run for 15 or 20 minutes without
recharging.  Black & Decker, DeWalt, Milwaukee, and Makita all have
their systems; a US$38 Milwaukee Red 4811-2659 rechargeable
lithium-ion battery pack for their M12 connector is nominally 2 amp
hours, 12 volts, and 200 g, comparable in energy density (420 J/g) and
energy capacity per dollar (2.3 kJ/USD) to the drone battery above but
presumably designed for a lower discharge rate.

Bicycle propulsion is probably better with lead-acid batteries
--------------------------------------------------------------

YouTuber Tom Stanton did a video on driving a bicycle with a kilowatt
drone motor, but he ended up driving it with a giant sprocket on the
bike wheel hub, which is goofy, and he overheated his motor because he
didn’t think about heat dissipation and the need for forced-air
cooling.  If you wanted to power a bicycle wheel at 30 km/h with such
a 20krpm motor you could theoretically use an 8-mm-diameter shaft
pressed up against the bike tire, but that might be a bit fragile.  If
instead you had a 16-mm-diameter wheel pressed up against the bike
tire or rim, like the kind that old generator-driven bike headlights
used, you could maybe gear the motor down by a factor of 2.

An interesting thing about the bicycle approach is that with
regenerative braking you can use it to charge a battery as well as
discharge it; potentially you can then disengage the same motor from
the wheel and use it for power-tool purposes at your destination.
Pedaling bicycles to run power tools is far from unprecedented;
traveling knife sharpeners here in Buenos Aires traditionally use
bicycle-mounted grinding wheels, belt-driven from a pulley mounted on
the rear wheel; the bike incorporates a four-legged stand to lift its
back wheel off the ground for this purpose.

For bicycle propulsion you probably want enough battery to last for
half an hour at 100 watts (180 kJ, 420 g of Li-ion), ideally 6 hours
at 300 watts (6.5 MJ, 15 kg).  Lead-acid batteries might be
better-suited to this, because although they weigh three times as much
as lithium-ion batteries, they are still *much* cheaper; a Motoma
SLA-MS6V10 sealed lead-acid AGM battery is 6 V, 10 amp hours (220 kJ),
1.53 kg (140 J/g), and US$14 (15 kJ/USD), and can provide 150 amps for
5 seconds (900 W, 0.6 W/g) or 10 amps for 40 minutes at 70%
efficiency.

You probably need *some* Li-ion batteries on top of that, because you
might need to accelerate and especially decelerate at well over 1000
W.  A small Li-ion buffer can likely soak up the large currents better
than the lead-acid battery.

### But now you can motorize rollerblades ###

Suppose you have a 1kW 70% efficient 20krpm drone motor with an
8mm-diameter shaft on it.  Mount it on a rollerblade with its axis
parallel to the wheels’ axes, and spring-load it to press its shaft
against one of the rubber wheels (they wear down over time so you
can’t use a fixed position).  If it’s 44 mm in diameter that’s a bit
over half the diameter of the wheel, so it’s not unreasonably large;
even the 50 and 75 mm diameter cooling fans I profiled above are
smaller than the 80mm wheels, at least when the wheels are new.

20krpm at a 4 mm shaft radius is 8.4 m/s, 30 kph or, in medieval
units, 19 mph, which is probably faster than it’s advisable to
rollerblade.  At that speed its 700 W of mechanical power output is
producing 84 N of thrust.  If you weigh 84 kg then the acceleration is
1 m/s/s, so you can reach max speed in 8.4 s, which is very zippy
indeed.

You probably want to put the 1.5 kg lead-acid battery in your
backpack.  It’s important for the control system to detect when the
wheel is and isn’t on the ground.

### Cooling options on a bike ###

What would be sufficient to cool the motor?  Let’s suppose it’s 70%
efficient (in the ballpark of the drone motors above) and consumes 500
watts; then it generates 150 watts of heat.  If we suppose that the
ambient temperature is 40° and that the motor can operate safely at up
to 80°.  Ideally the cooling air flowing through the motor would be
heated all the way up to the motor’s temperature, but that can never
be perfect, so let’s say the cooling air heats up to 70°, so we have a
ΔT of 30° down to ambient.  Air is about 1.2g/ℓ and 1J/g/°, so
1.2J/ℓ/°, so at 30° we have 36J/ℓ, which means we need 4.2ℓ/s of
airflow, which is 8.9 “cfm”, or about 18 “cfm” per kilowatt.

A drone motor is fairly small (this US$33 [generic 47-gram 17600rpm
kv=2200 190-watt jobbie with a 3.17mm shaft][4] is 27.5mm × 30mm), and
most of that space is occupied by solids that impede the air, so
getting the air to go through it might require a significant head.  A
bicycle might normally go 10 “kph”, which is about 3m/s; at that speed
an air scoop of only 14cm² (ø42mm) would provide enough airflow if it
had enough pressure.  Maybe I should just increase the air scoop area
by a factor of 10 or 32 and hope for the best?  Or maybe I should do
some actual tests if viscous pipe flow calculations are beyond me.

[4]: https://articulo.mercadolibre.com.ar/MLA-737878748-motor-cuadricoptero-kv2200-con-cubo-y-accesorios-quadcopter-_JM

Some motors might be suitable for liquid cooling, which could
potentially be much quieter, an important consideration when it comes
to not advertising having something profitably thievable.  Water
cooling probably requires windings on a stator that is outside the
rotor in order to have a hot surface to run water pipes over, but oil
mist cooling inside a hermetically sealed chamber might work for
conventional drone motors.

### Shaft coupling options on a bike ###

3.17mm is about 1.6mm radius, so that motor’s 17600rpm is about 2.9
meters per second at the surface of the shaft.  This is close to the
top ground speed of the bike, so if you could just couple that shaft
directly to the tire, you’d have a plausible system.  But I think it’s
a little short, and it’s small enough that it might damage the tire.
To exert its full possible output of 152 watts (assuming the touted
80% efficiency figure is at this maximum speed, when it’s probably at
a lower speed) at 2.9m/s, it would push at only 52N, the weight of
5.3kg.  If we assume [a static coefficient of friction of about
0.6][5] this means we would need a normal force of about 90N pressing
the shaft into the tire, the weight of about 9 kg.

Moreover, the same power at lower speeds, which would be more
realistic, would amount to even larger forces.  Suppose we get 152
watts at 70% of max speed, 12krpm, 2m/s.  That’s 76N rather than 52N.
If the mass of the bike+rider system is 120kg, the acceleration is
0.6m/s/s, about 0.06 gees.  That will get you up to speed pretty
quick.

To be more concrete about that, 120kg at 3m/s is 540J of kinetic
energy, which is 3.6 seconds of 152 watts.  Electric motors usually
have pretty constant power down to almost zero speed, so you might
actually be able to come close to this.  (Zero to sixty would still be
284 seconds, though.)

I used to have a generator-powered incandescent headlight on a bike;
the generator was powered by a little knurled plastic wheel that
rolled along the side of the tire, with a diameter of about 12mm,
which was pressed against the tire with a spring that rotated the
entire generator/lamp assembly around the bike’s roll axis.  The
headlight’s power was probably a few watts; you could certainly feel
the extra drag when you pedaled.

But here we’re talking about a significantly larger power, which
probably requires significantly more normal force if it’s going to be
transferred to the wheels by friction.  So it might be more practical
to spread it over a larger area than the shaft surface (as the
generator did with its larger wheel) and to apply it to the hard
surface of the rim, as rim brakes do, rather than the soft surface of
the tire.  This involves gearing the high-rpm motor shaft down to a
lower-rpm gear (without forcing the motor’s internal bearings to take
significant side loadings, which would probably wear them out quickly)
which rotates a larger, perhaps rubber wheel in contact with the rim,
or possibly more than one such wheel.

The gearing itself is another potential heat dissipation source: if
it’s carrying 150 watts and is 98% efficient (rather optimistic!),
then its friction dissipates 3 watts as heat, which is a potential
problem if it’s small, like the 3.17mm shaft of this motor is.

[5]: https://pleiger.com/wp-content/uploads/2022/04/Coefficient-of-Friction.pdf

[YouTuber SunShine, citing Stanton as inspiration, has built a
prototype][6] that drives the rear wheel with a drone motor using
3-D-printed helical gear sections that clip on to his (aero) rim in
between the spokes, basically as a means of building a dynamometer.
Using a helical gear on the wheel allows him to use a pinion with only
three teeth, almost more like a three-start worm gear than a helical
gear.  He reports that one of the motors he tried has KV = 12200 rpm/V
and 0.08Nm peak torque at 10 amps and 10krpm; a second one has lower
KV and 0.1Nm peak torque; and the third, which he says was a pancake
motor although the windings look like radial-field windings to me, has
even lower KV, a peak torque of 0.2 or 0.25Nm, and a max power of
about 60 watts (the other two were closer to 80).  And then his
plastic pinion broke.

At these low power levels he doesn’t seem to be doing anything special
for cooling.

[6]: https://www.youtube.com/watch?v=humqw8ApkSs

### How much power do you really need on a bike? ###

Today I got Mina’s old beach cruiser working again after 25 years.
Its wheel bearings are a little crunchy.  If I get up to about 8km/h
(2m/s) and stop pedaling, it slows to a stop after about 100 meters on
flat pavement.  Supposing that the bike with me on it weighs 120kg,
that’s 240J and thus 2.4newtons of friction; at 2m/s that’s 5 watts.
So, unless I’ve totally biffed this calculation, even 5 or 10 watts
would be enough to keep the bike moving at that speed indefinitely on
flat ground.

As it happens, the bike is about 8km from my house right now, 8km
which required 2½ hours to traverse by shitty buses which stunk of
dried piss accompanied by screaming babies, a trip which involved
getting bitten by possibly-dengue-carrying mosquitoes while waiting at
a bus stop for a bus that never came.  I didn’t want to chance riding
the bike home because lots of things could be fatally wrong with the
bike that I haven’t noticed yet, but I could have walked with a broken
bike faster than that, also completely avoiding the piss and babies.

10 watts for an hour is 36kJ, which would be 2700 milliamp hours on a
3.7-volt lithium-ion battery, slightly more than a single rechargeable
18650 cell.

25 watts would accelerate me to 2m/s (8km/h, 240J) in a bit under 10
seconds, using an average of 25N of force, the weight of 2.5kg.  This
is not exactly a racecar peelout.  If we assume that friction remains
at 2.4N, it reaches 25 watts at a ridiculously death-defying 10m/s
(38km/h), so a 25-watt motor could conceivably get the bike to go that
fast without any pedaling.  25 newtons is fine for compensating for
friction or light uphills to keep me moving, but regenerative braking
would require at least temporarily running the motor as a generator at
higher power than 25 watts.

You also don’t need super-high-power batteries to fuel such hour-long
trips; a 1C battery would be fine, or even 0.5C.  But regenerative
braking, again, might be more demanding.  It might be worthwhile to
use a small high-discharge-rate quadcopter battery (15C, say) as a
buffer to soak up regenerative-braking currents which can then be
trickled into a bigger battery.

A friend of mine tells me he needs to cross a 200-meter-tall hill to
get to the city, which would require about 200kJ of energy.  25 watts
could haul 120kg of weight up about a 1% slope at 8km/h; at lower
speeds it could handle a steeper slope, but a larger motor might be
better.

Drone batteries are power-dense enough for handheld arc welders
---------------------------------------------------------------

A bargain-basement Lüsqtoff Iron-100 arc welding machine for stick and
TIG welding is 3.5 kW, US$76, 3.1 kg, at 10-80 amps and thus probably
45-240 volts AC/DC output.  Continuous welding of that form from 5 W/g
batteries like the drone batteries mentioned above would require 700 g
of batteries (which would be discharged after a minute or so, so the
welding isn’t *very* continuous).

Probably it would be ideal to put most of the electronics and
batteries on the “ground clamp” part of such an apparatus.

### But spot welders would be a lot easier ###

If you could limit yourself to spot welding you could maybe get by
with lower-power batteries and use a capacitor or inductor bank to
store the weld energy.  Suppose you want to spot-weld 1-mm-thick sheet
steel with a 1-mm-diameter weld nugget between the sheets; in the
ideal case this will require heating 0.52 mm³ (4.1 mg) of steel up by
about 1600 degrees in a short time, which is in theory only about 7 J.
If you can store 100 J in some kind of multi-kilowatt store and
release it as low-impedance electricity in tens of milliseconds you
should be able to do it quite reliably.  Regular electrolytic caps
should be plenty fast, and at the 340 volts conventionally used for
offline SMPSes, 100 J = ½CV² requires less than 2000 μF.  You probably
need a significant stepdown transformer as part of the setup; if steel
is 10⁻⁷ Ωm then the resistance of your 1mm-long 1mm-diameter cylinder
is 0.13 Ω, so I²R will only reach 3500 W at 170 amps and 20 volts.  So
your transformer output needs to be wound for 170 amps, and the total
stepdown from the 340V caps needs to be 17:1, but probably 350 amps
and 35:1 to be on the safe side.

Such a portable spot welder could do one weld per second at 100 watts
or one weld per five seconds at 20 watts.  Accordingly you could
probably run it off a single lithium-ion pouch cell or a USB
powerbank, though the hundred-joule caps would be a lot bigger than
the battery.

Battery-powered plasma cutters might be a bit more awkward but are still doable
-------------------------------------------------------------------------------

Plasma cutters seem to start a bit pricier; the Motomel MSCP40 is
US$370, 6400 W, 40 A, 96 V, 85% efficient, and 10.3 kg, and this
doesn't include the hoses, gas, torches, etc., just the power supply
and control circuitry.  Still, 6400 W at 5 W/g is still only 1.3 kg of
battery, which would be about US$400.
