Suppose I want to move 40 g/s of air (in medieval units, 70 "cfm") in
and out of my house through a regenerator system; this is more or less
what's needed to keep two people from driving the CO₂ level above 800
ppm (see file `cryoziggurat.md`).  If the indoor air is 20° and the
outdoor air is 35°, at 1 J/g/K this works out to 600 watts of heat
loss.  For regular uninsulated houses, that's probably not
significant, but for superinsulated houses, it would be nice to reduce
this to below 100 W.  This basically means moving 85% of the heat from
the outgoing air to the incoming air.  Recuperator appliances are
available to do this but are expensive.  A regenerator, similar to a
nose, is a feasible way to do this, and might be much cheaper.

[120mm 1200rpm case fans can do about 30 cfm and are pretty quiet][0]
so three of them ought to be able to do 120 cfm, or 57 ℓ/s in modern
units.  At 120mm × 360mm, 57 ℓ/s should be an airflow speed of 1.3
m/s, which is a relatively reasonable air speed.  [Such fans cost
about US$6][1] on MercadoLibre with a 4-pin PWM interface and can
handle over 70 "cfm" by themselves at a higher noise level.

[0]: https://linustechtips.com/topic/1310047-what-is-considered-a-good-cfm-for-a-case-fan/
[1]: https://articulo.mercadolibre.com.ar/MLA-614371259-cooler-120mm-id-cooling-xf-12025-sd-pwm-1800rpm-pwm-4-pines-_JM "id-cooling XF-12025-SD, 700-1800rpm, 3W, 74.5cfm, max pressure 2.15mmH₂O"

(It's pretty astounding that even at everyday temperatures like this a
3-watt fan can move 600 watts of heat.)

I don't know how to calculate the thermodynamics of a regenerator but
here's my crude estimate.  Suppose our regenerator is a bucket of
concrete rubble; how big should the bucket be and how big can the
rubble be?  If it's too fine it will have too much air resistance, but
if it's too coarse the heat will stay in the air instead of going into
the concrete because there won't be enough surface area.  If it's too
small the air won't have enough residence time in it to lose its heat,
and if it's too big, the air we suck in will be the same air we blew
out last cycle instead of the fresh air we wanted.

The air we suck in needs to be cooled to 22.5°, so probably only a
countercurrent setup is viable, and the whole temperature difference
driving the countercurrent heat flow is only 2.5°, and we need 500
watts of heat flow between the outgoing air and the incoming air at
that speed, 200 W/K.  [Air is about 0.025 W/m/K][2] and [concrete or
stone about 2-10][3], while the specific heat of concrete [is about 1
J/g/K][4], just like air and most other materials (maybe 0.8 in the
case of concrete), and its density is about 2.4 g/cc, giving a
volumetric specific heat of about 2 J/cc/K.  Air's volumetric specific
heat is about 600 times lower at 1.2 mJ/cc/K.

[2]: http://hyperphysics.phy-astr.gsu.edu/hbase/Tables/thrcn.html
[3]: https://www.engineeringtoolbox.com/thermal-conductivity-d_429.html
[4]: https://www.engineeringtoolbox.com/specific-heat-solids-d_154.html

Handwaving wildly, I think that if concrete's volumetric specific heat
were ten times higher, the heat would need to penetrate one tenth as
far into it so the ideal rubble size would be one tenth as big, and if
its thermal conductivity were ten times higher, the heat would
penetrate ten times as deep into it in a given time so the ideal
rubble size would be ten times bigger, so to get an indication of the
scale I want to divide the thermal conductivity (2 W/m/K) by the
volumetric specific heat.  The result is one square millimeter per
second.  But what does that mean?  I don't know, handwaving only gets
me so far.

Suppose that the air's insulance is negligible and so we have 1.25° of
temperature difference from the surface of the concrete to some
almost-constant-temperature core, say, 5mm.  Then we have a heat flux
of 6000 W/m², so to get our 500 watts we would need .083 m², 830 cm²,
of surface area.  If our concrete were in 10mm-thick slabs this would
be only about 0.83 ℓ of concrete, 2 kg.  We'd be using about half of
this concrete's heat capacity (since the surface would
temperature-swing by 2.5° between the flow directions, the core
wouldn't change at all, and things in between would temperature-swing
by distances about proportional to their distance from the surface;
actually there are exponentials here but for this crude estimate I'm
going to ignore them) so at .8 J/g/K, 2 kg of which half is used, and
2.5 K, we have about 2000 J.

That's about three seconds of this airflow; after a few seconds the
concrete will be sort of saturated with this 2.5° temperature rise and
will pretty much cease transferring energy to the air.  This is
probably a little too fast to control the airflow by turning PC case
fans on and off.

This seems like surprisingly fast for heating and cooling a slab of
concrete, so consider a 10mm × 10mm × 5mm block of concrete in between
a 20° reservoir and a 22.5° reservoir (or equivalently between a 32.5°
reservoir and a 35° reservoir).  It sustains a thermal gradient of
0.5°/mm or 5000°/m, producing a heat flow rate of 1000 W/m² and thus
0.1 W, which intuitively sounds reasonable.  If one of the reservoirs
was disconnected, the heat needed to heat it up or cool it down to the
uniform temperature of the other reservoir would be ½ 2.5° (2.4 g/cc)
(.8 J/g/K) (.5 cc) or 1.2 J, so in a sense the "residence time" of the
heat in the "pipe" is 12 seconds: if heat continued to flow at the
same rate after the source or sink were disconnected, it would take 12
seconds to "fill" or "empty" the "pipe" (though of course the heat
flow rate would immediately start to slow and eventually you'd have an
asymptotic approach to equilibrium).  So I might have an error of a
factor of 2 or 4 above but apparently not orders of magnitude.

Note that to provide the same thermal resistance as these 5 mm of
concrete we need a stagnant boundary layer of only 63 μm of air, which
probably means submillimeter air passages, so in practice most of the
thermal resistance might come from the air rather than the solid phase
in the regenerator.

A more plausible target to shoot for is 20 kJ, 30 seconds, which might
mean up-to-100-mm-thick concrete chunks and 10-mm-thick air spaces
between them (maybe created by including smaller chunks in between).
20 kg of concrete should be about right for this, which is 8.3 ℓ; the
air in the regenerator air spaces might be 2 ℓ.  (And similarly for a
second regenerator bed to provide balanced flow.)  All in all it seems
like the whole apparatus ought to almost fit in a 20-liter paint
bucket.

The chunks can be made smaller up to a point to increase the heat flow
rate.

At 57 ℓ/s the residence time of the air in this 2-liter air space
would be only 35 ms, which seems probably too short for the air to
change much in temperature.  If I do the same handwaving computation
as for the concrete, .025 W/m/K / (1.2 mJ/cc/K), I get about 21 square
millimeters per second, which is 21 times higher than for the
concrete, which I think means heat will penetrate the air 21 times as
fast as it penetrates the concrete.  So maybe instead of on the order
3 seconds to get through 5 mm it should take on the order of 140
milliseconds.  So maybe 100 or 200 or 500 ms would be enough even if
35 ms isn't.  And that still means we can switch direction every 5–20
seconds without sucking in more than a tiny amount of exhaust air.

Water encapsulated in a solid, such as polyethylene bags, would be a
much better heat storage medium, requiring a quarter the mass and half
the volume of concrete, and it's even cheaper, but does have some
other drawbacks.  MDF, paraffin wax, and HDPE are apparently other
materials with about 2 J/g/K and density similar to that of water.
Recycled HDPE, LDPE, and polypropylene all cost a bit less than
US$1/kg, and LDPE's specific heat is as high as 2.6 J/g/K; see file
`recycled-plastics.md`.  While this is dramatically more expensive
than concrete or gravel we are still talking about a price on the
order of US$7 for 8 kg of material, much less than the fans.  Of
course most of these media have lower heat conductivity but they're
also easy to make into thin shapes.

If you wanted to make a really big regenerator you would presumably
want to use rocks, maybe synthetic alumina or silicon carbide if you
need heat resistance, but otherwise just regular rocks.  Quartz has a
specific heat of only 0.7 J/g/K but it costs a few dollars a tonne and
gets over 1000° without complaining.

Another possibility is to gelify water with something like sodium
silicate or silica gel to get nearly 4 J/g/K as long as you don't get
hot enough to evaporate the gel.  To the extent that you do exchange
humidity with the gel it might be profitable.

Silica gel costs [about US$2.30–3.50/ℓ at retail as cat litter][11] at
density 0.5 g/cc, so about US$5–8/kg, and [US$8/kg as a desiccant from
HUMISUR in Quilmes][12].

[11]: https://articulo.mercadolibre.com.ar/MLA-885980116-piedras-sanit-can-cat-silica-gel-family-pack-limon-76lts-_JM
[12]: https://articulo.mercadolibre.com.ar/MLA-859248595-silicagel-10-azul-con-indicador-de-saturacion-1-kg-_JM

Now that I've tried to analyze the problem, I'll check out the
literature.  [Bohuslav Kilkovský wrote a CC-BY open access Review of
Design and Modeling of Regenerative Heat Exchangers three years
ago][4] which looks good.  The first interesting thing is that his
first working model doesn't try to take into account the thermal
insulation of the packing material itself; it assumes it's zero!  He
then describes Hausen's simple model from 01942 which incorporates
that insulation, a model I don't understand yet.  ([Hausen is also the
one who realized noses were regenerators in 01976][6].

[4]: https://res.mdpi.com/d_attachment/energies/energies-13-00759/article_deploy/energies-13-00759-v2.pdf "Energies 2020, 13, 759; doi:10.3390/en13030759"
[6]: https://www.thermopedia.com/content/1087/

On reading this it occurs to me that if the solid heat-exchange medium
is in thin enough sections, you can use its whole heat capacity
instead of half of its heat capacity, so you can use half as much
material: 4.2 kg per regenerator side instead of 8.3 kg.  For the
pressure drop to stay reasonable, you really just need big enough and
abundant enough air passages; it doesn't matter that much how far
apart the solid medium separates the air passages.
