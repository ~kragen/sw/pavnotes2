16 words on each of several possible things to try:

AST parsing and serialization with a grammar described as mutually
recursive struct and union type definitions.

Enhancing Meckle to at least handle action buttons, table layout,
checkboxes, radio buttons, text, and dropdowns.

An essay on the transformational/reactive, sequential/random-access,
and functional/imperative spectra and their connections.

A minimal transactional bytecode interpreter and a compiler thereto
from a reasonable high-level programming language.

A CPU like Smecher's Minimax, where most opcodes trap to microcode,
but with less primitive opcodes.

My own straightforward RISC CPU like Wirth's RISC in Verilog,
simulated on the ICE40.

A digital logic simulator for my notation where each line is a 2-input
NAND.

A more complete paint program for Yeso: draw, erase, load, save,
rectangle select, copy, paste, fill.

A portal paint program where copy-pasting circles creates live
portals, enabling fractals.

A recursive reptile voxel game world where you can build voxels from
scaled subvoxels, including themselves.

Basic IMGUI widgets for Yeso: text, buttons, radio buttons,
checkboxes, dropdowns, menus, tree views.

Spread-spectrum CDMA ultrasound data transmission inside the house for
things like thermometers, calipers, and scales.

A nested-table-layout text editor with tabs between columns and
proportional fonts.

Making Qvaders properly handle a range of requestAnimationFrame
speeds, from under 20 Hz to 120 Hz.

Using the Cohn-Lempel fast M-sequence transform for reverb impulse
response measurement and desynchronized CDMA.

A simple barrel processor in an FPGA to experiment with extremely
aggressive pipelining and constant time.

A tuple-space OS where all persistent storage and communication is in,
out, rd, and eval.

Using gradient descent optimizing the L1 norm to find a sparse
approximation to a convolution.

