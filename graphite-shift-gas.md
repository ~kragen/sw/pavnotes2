Normally graphite is hard to burn, because of its ridiculously high
thermal conductivity, but it does technically have an ignition
temperature.  And it has some advantages: you can heat it up with
electrical resistance heating, for example.  If you want to produce
gas to power a steam engine, a possible way to do it is to inject
water into a pebble bed of hot graphite; it will produce water gas,
the old [“town gas][2]” that preceded natural gas in many places, and
the water gas it produces will contain twice as many molecules as the
steam that preceded it.  However, I think it might be an endothermic
reaction:

> C(s) + H₂O(g) → CO(g) + H₂(g)

The standard enthalpies of formation of the non-elemental compounds
here are:

- H₂O: [-285.83 kJ/mol][0] (18.015 g/mol)
- CO: [-110.5 kJ/mol][1] (28.010 g/mol)

I’m incorrectly using the *standard* enthalpy of formation, which is
only correct at room temperature, but hopefully the error is not too
great; [such syngas production is known to be “strongly
endothermic”][6].

[0]: https://en.wikipedia.org/wiki/Properties_of_water
[1]: https://en.wikipedia.org/wiki/Carbon_monoxide
[2]: https://en.wikipedia.org/wiki/Coal_gas
[6]: https://en.wikipedia.org/wiki/Syngas#Composition,_pathway_for_formation,_and_thermochemistry

So at a millimolar scale you combine 12.01 mg of graphite, 18.02 mg of
steam (which you had to boil at a heat-of-vaporization cost of
40.65 J/mmol (2257 kJ/kg) and also heat to boiling at a cost of about
4.184 J/g/K (75.40 J/mol/K, 7.5 J/mmol for ΔT = 100°)), and 175.3 J of
heat, and get 28.01 mg of carbon monoxide and 2.016 mg of hydrogen,
comprising 2 mmol of gas.  At 600° two millimoles of ideal gas is 143
milliliters.  That’s probably close to the lowest temperature you can
do this at a human speed; [the carbon must be incandescent][5].

[5]: https://en.wikipedia.org/wiki/Water_gas#Production

The WP page for the [water-gas shift reaction][3], which reacts carbon
monoxide with water, mentions an interesting historical alternative:
reacting iron with high-pressure steam.  Supposing that what you get
out of this is magnetite, that’s:

[3]: https://en.wikipedia.org/wiki/Water%E2%80%93gas_shift_reaction

> 3Fe + 4H₂O → Fe₃O₄ + 4H₂

To our list above we can add:

- Fe₃O₄: [-1120.89 kJ/mol][4] (231.533 g/mol)

[4]: https://en.wikipedia.org/wiki/Iron(II,III)_oxide

This is still endothermic, but barely so.  We’re trading
-1143.32 kJ/mol of water for -1120.89 kJ/mol in the magnetite.  Mixing
a little air into the water should be enough to make up the
difference.

There are other metals that are more avid to combine with oxygen, such
as aluminum and magnesium, but they melt at the requisite
temperatures; this would make it difficult to sustain the
hydrogen-production reaction at a high rate, because melting greatly
reduces the surface area available for reaction.  Even iron might be
problematic here because, while [Fe₃O₄ doesn’t melt until 1597°][7]
and [hematite, Fe₂O₃, until 1539°,][9], [wüstite, FeO, melts at only
1377°][8].  All of these temperatures are much higher than what is
necessary for the rapid reaction, but thermal runaway could easily
reach them, and the thermal conductivity of iron and especially its
oxides are much lower than that of graphite.

[7]: https://en.wikipedia.org/wiki/Iron(II,III)_oxide
[8]: https://en.wikipedia.org/wiki/Iron(II)_oxide
[9]: https://en.wikipedia.org/wiki/Iron(III)_oxide

This is related to my desire for a charcoal-fueled kiln, where airflow
to a small chamber of burning charcoal is kept choked, and its output
gas is used to heat a much larger well-insulated kiln chamber, slowly.
If the hot output gas is CO, hydrogen, or a mix, it could provide a
strongly reducing atmosphere for metals or metal ores.  See file
`charcoal-muffle-kiln.md`.

Mixing oxygen or air into the hot output gas permits recovering the
carbon’s heat of formation that was lost in the original water-gas
formation.  This also potentially permits adjusting the atmosphere to
neutral or oxidizing.
