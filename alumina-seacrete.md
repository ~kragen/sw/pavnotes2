I’ve previously written about process intensification for seacrete,
using, for example, sand soaked in a magnesium-chloride brine.  But
suppose you use a soluble aluminum-bearing compound instead?  You can
electrodeposit hydrargillite (Al(OH)₃) on an electrode.  By shaping
the electrode ahead of time and controlling the time and current
density of electrolysis, you can get a stone of controllable geometry.
Calcining it can convert it to sapphire ceramic, making it very hard
and strong; incorporating dopants and/or granular and/or fibrous
fillers can also enhance strength, hardness, calcinability, and other
properties.

Candidate electrolytic systems
------------------------------

The standard water-soluble aluminum salts are the nitrate, the
chloride, and (perhaps surprisingly) the sulfate; the perchlorate is,
it seems, even more water-soluble.  Sodium aluminate (NaAl(OH)₄),
[analogous to sodium zincate][7], is another possibility; I think it’s
even more water-soluble than the salts of the aluminum cation, and it
doesn’t require an acid-resistant anode, because the highly basic
sodium aluminate will passivate most transition metals you might use
as the anode.

[7]: https://www.nmfrc.org/pdf/psf2001/070148.pdf

The sodium zincate reaction is, [if I understand it correctly][8]:

> - Na₂Zn(OH)₄ + 2*e*⁻ → Zn + 2NaOH + 2OH⁻ (cathode)
> - 4OH⁻ → 2H₂O + O₂ + 4*e*⁻ (anode)

[8]: https://youtu.be/3X9c6epL7HQ

Because what’s being oxidized at the anode is hydroxyls, there’s no
risk of a metal anode getting oxidized, though amphoteric metals
subject to attack by bases (like zinc!) might corrode from the
basicity of the solution.

[Sodium aluminate][9] only has one sodium; if the reaction were
analogous, it would be:

> - NaAl(OH)₄ + 3*e*⁻ → Al + NaOH + 3OH⁻ (cathode)
> - 4OH⁻ → 2H₂O + O₂ + 4*e*⁻ (anode)

But of course that’s completely implausible; you aren’t going to
electrolytically reduce aluminum from hydroxide to metal in water, or
Hall and Héroult wouldn’t have had to go to such absurd lengths to
find a way to reduce aluminum from hydroxide.  Surely you’ll end up
with some kind of aluminum hydroxide or [oxy-hydroxide][10] at the
cathode instead.  But what kind?  Maybe hydrogen will be reduced,
since neither sodium nor aluminum can plausibly be:

> - NaAl(OH)₄ + 4*e*⁻ → AlO(OH) + NaOH + H₂ (cathode)

But that can’t be quite right; where did the negative charge go?

It’s possible that this isn’t what would happen at all; aluminum
hydroxide always exists in an equilibrium with sodium aluminate, an
equilibrium which favors sodium aluminate when the pH rises, and the
more alkaline conditions at the cathode might inhibit precipitation
rather than causing it; but in that case we can just use precipitation
at the *anode*.

[9]: https://en.wikipedia.org/wiki/Sodium_aluminate
[10]: https://en.wikipedia.org/wiki/Aluminium_hydroxide_oxide

It’s worth pointing out that lead (dioxide) anodes should withstand
chloride or sulfate electrolysis just fine, so sodium aluminate is not
an essential part of this process, just a convenient one.

Here’s a list of candidate electrolytes [with solubilities][14] where
possible.

- [NaAl(OH)₄][11], “high solubility” (supersaturation common), 22.9%
  Al
- [Al(NO₃)₃][13], 60—160g/100mℓ, 12.7% Al, so 7.6—20.3g Al/100mℓ
- [Al₂(SO₄)₃][16], 31.2—89.0g/100mℓ, 15.8% Al, so 4.9—14.0g Al/100mℓ
- [Al(ClO₄)₃][15], 122—133g/100mℓ, 8.3% Al, so 10.1—11.0g Al/100mℓ
  (but plausibly higher at higher temperatures; 133 is at 20°)
- [AlCl₃][12], 43.9—49.0g/100mℓ, 20.2% Al, so 8.9—9.9g Al/100mℓ
- [Al(CH₃COO)₂OH][17], >13g/100mℓ, 16.6% Al, so >2.2g Al/100mℓ
- [Al(HCOO)₃][18], [possibly low solubility][19], 16.7% Al

[11]: https://en.wikipedia.org/wiki/Sodium_aluminate
[12]: https://en.wikipedia.org/wiki/Aluminium_chloride
[13]: https://en.wikipedia.org/wiki/Solubility_table
[14]: https://en.wikipedia.org/wiki/Solubility_table
[15]: https://pubchem.ncbi.nlm.nih.gov/compound/Aluminum-perchlorate
[16]: https://en.wikipedia.org/wiki/Aluminium_sulfate
[17]: https://en.wikipedia.org/wiki/Aluminium_diacetate
[18]: https://en.wikipedia.org/wiki/Aluminium_formate
[19]: https://www.science.org/doi/10.1126/sciadv.ade1473#sec-3

Calcination
-----------

Although the hydrargillite is no harder than the brucite (Mg(OH)₂)
deposited by magnesium electrolysis, and softer than the calcite that
can be deposited from a calcium bicarbonate solution, it has a
singular virtue: it can be calcined to sapphire!  The transformation
does not happen immediately upon dehydration, but goes through many
intermediate aluminas at low temperatures.

However, you will not get transparent corundum by this method;
polycrystalline corundum is invariably translucent, like Lucalox, even
without the porosity that will be produced by the dehydration.

Doping
------

Hydrargillite is itself fairly translucent, so small amounts of
impurities of other metals can give it appealing colors.  These
impurities should also color the resulting corundum if calcined;
chromium in particular can produce ruby, while WP says sapphires are
commonly colored with iron (yellow, green, or blue), titanium (blue),
cobalt, lead, chromium, vanadium, magnesium, boron, and silicon.  All
of these elements except boron and silicon have soluble salts and
insoluble hydroxides and can therefore be precipitated from solution
by electrolytically lowering the pH.  There are also oxoanions with
water-soluble alkali salts made from [iron][1], [chromium][0],
[vanadium][2], [manganese][4], [tungsten][5], [molybdenum][6], zinc
(as mentioned above), and [silicon][3], giving solutions analogous to
the aluminate; I think these can be electrolytically co-precipitated
from solution as impurities in the hydrargillite.  Boron certainly
forms an anion with water-soluble alkali salts, but it doesn’t have an
insoluble hydroxide.  Titanium, cobalt, lead, and magnesium do not, to
myknowledge, form oxoanions with water-soluble salts.

[0]: https://en.wikipedia.org/wiki/Sodium_chromate
[1]: https://en.wikipedia.org/wiki/Potassium_ferrate
[2]: https://en.wikipedia.org/wiki/Sodium_orthovanadate
[3]: https://en.wikipedia.org/wiki/Sodium_silicate
[4]: https://en.wikipedia.org/wiki/Sodium_manganate
[5]: https://en.wikipedia.org/wiki/Sodium_tungstate
[6]: https://en.wikipedia.org/wiki/Sodium_molybdate

Functional fillers
------------------

Using a granular or colloidal filler material such as ruby, sapphire,
feldspar, or quartz sand, an acicular filler such as mullite, basalt
fiber, or glass fiber, or a platy filler such as clay, has several
potential advantages:

- The filler can add strength and, especially, resilience; acicular
  fillers such as mullite should be especially beneficial here, and
  especially in the case where you don’t calcine the piece, in which
  case the hydrargillite is merely a weak binder, and the filler
  provides most of the piece’s final strength.
- By reducing the amount of hydrargillite that needs to be deposited,
  the filler reduces the time and energy cost of growing the piece,
  perhaps by as much as an order of magnitude.  Filler systems
  containing multiple sizes of particles will be superior here.
- The filler can determine the surface texture of the resulting piece,
  for example for use as a grinding wheel.
- The porosity and geometric distortion produced by shrinkage of the
  hydrargillite binder during calcination can be reduced by having
  hydrargillite compose a smaller fraction of the final piece.  Quartz
  is not a good material in this case because of dunting; an alumina
  or mullite filler should be superior.
- The filler can also control properties such as color and reactivity.

By using a hydrophilic filler with a small particle size, such as a
clay, and deflocculating it (which may impose difficult constraints on
the electrolyte), you can get a very homogeneous, predictable material
with a high filler loading (>80%) and minimize the geometric error
introduced by the filler.
