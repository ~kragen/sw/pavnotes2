If your right hand is on the mouse and your left hand is on the
keyboard, you can point at objects with the mouse while invoking
operations on them with the keyboard.  The left-hand keys on a QWERTY
keyboard are:

    `  1  2  3  4  5
    Tab q  w  e  r  t
    Ctrl a  s  d  f  g
    Shift z  x  c  v  b
    Ctrl Linux Alt space

This is a menu of some 28 keys that can be quasimodally pressed and
released individually while your other hand is still on the mouse with
its now-conventional three buttons and click wheel, providing 33
buttons in all.  This kind of interface can be difficult to learn, but
it supports richer direct manipulation than just a mouse alone.

“Interclicking” other keys while carrying out an operation may be a
useful way to modify the operation; for example, maybe you don’t want
all your resistors to be 1kΩ.  Bringing up a dialog box is the
standard way to do this, but it’s clumsy.

Two-handed interaction on a multitouch device can work similarly, with
one finger indicating objects and the other finger indicating
operations.

Schematic capture
-----------------

An example of where this might be useful is electronic schematic
capture, where there are a large number of operations you might want
to do at any time:

- add a resistor
- add a capacitor
- add a wire
- add an npn
- add a pnp
- add an n-channel MOSFET
- add a diode
- add an LED
- add some other component
- change the value of a component
- move a component
- resize a component
- delete a component
- undo
- cut
- copy
- paste

Being able to add a resistor by pressing and releasing “r” as you move
the mouse, or a wire by pressing and releasing “w”, seems vastly
superior to being in some invisible mode which determines which
component appears whenever you click the mouse, something with which I
constantly make errors in [Paul Falstad’s circuit.js][0].  It also
distinguishes between resizing components and moving them based on
where the mouse is, and I constantly make errors with that too.

[0]: https://github.com/pfalstad/circuitjs1

3-D navigation and creation with mouselook
------------------------------------------

Videogames already use this technique; the standard mouselook + WASD
paradigm works this way.  The WASD keys are quasimodal or nonmodal
movement keys that move you forward (W), backward (S), left (A), or
right (D), while mouse movement rotates you and thus changes which
directions these are.  Typically Space, the mouse buttons, and other
left-hand keys activate other functions such as jumping, descending,
using a shield, or attacking.  Shift is an especially popular key
because it’s easy to reach and modifier keys generally don’t interfere
with other keys, even on non-[NKRO][2] keyboards.

[2]: http://plover.stenoknight.com/2018/04/more-laptops-with-nkro.html

Sometimes the WASD plane is constrained to be horizontal even when
you’re looking up or down with the mouse; in other cases, it’s
entirely aligned with your view plane.  In most programs, your
[view-up][1] vector is constrained to point towards the zenith, so
your look-at vector is sufficient to define the whole view, and no
separate up-vector-rotation operations are needed to recover from
unwanted roll.

[1]: http://learnwebgl.brown37.net/07_cameras/camera_introduction.html

Often the object you’re looking at (in the center of your field of
view) is also the target of any actions you might take, such as firing
a weapon, so mouselook functions simultaneously as a
target-designation interaction and as a navigational interaction.
It’s sort of like Scroll Lock (at least in Emacs), which keeps your
text cursor always on the same line of text on the screen (when
possible), so the same movement commands both select what’s in view
and where text you type will go.

It occurs to me that you could use a quasimodal key to lock that
orientation, temporarily disabling mouselook, and also lock a
particular target object.  For example, the T key might activate a
“translation” quasimode in which mouse movements move the target
object instead of changing your look angle (though the T/G/B column is
often excluded from [gaming half-keyboards][3], the Redragon K585
being an exception), and the R key might activate a “rotation”
quasimode in which your mouse movements rotate the object around its
center like a trackball.  Other parameters that make sense to vary
along one or two continuous axes like this include color, scale, and
more model-specific parameters such as length or head size.

[3]: https://www.pcworld.com/article/835679/we-need-more-left-hand-keypads-for-pc-gaming.html

For mechanical-CAD-like applications, it might make sense to invert
the normal sense of mouselook, since you’re usually looking at your
part from the outside, and it’s useless to rotate your view to look
away from it; but rotating the object in your view (“orbiting”) is a
very normal and common thing to do.  With mouselook+WASD the way to do
orbiting is [“circle strafing”][14]: press A, moving you to the left,
while moving the mouse continuously to the right to compensate, or
press D while moving the mouse to the left.

[14]: https://en.wikipedia.org/wiki/Strafing_%28video_games%29#Circle_strafing

### SketchUp ###

Taking [SketchUp][4] as an initial point of reference for 3-D design,
the most basic commands, aside from orbiting and zooming (with the
mousewheel), are to draw lines between points to form a face, and to
“push/pull” a face (extruding or pocketing).  Other commands include
drawing polylines (which get filled if you close them, and which snap
to paraxial angles, indicated by R/G/B colors; vertices snap to other
candidate constraints (“inferences”) displayed with dotted lines
(“guides”) to bullseyes, with tooltips (“ScreenTips”) to explain the
constraint types (“inference types”)).  Typing numbers modifies a
mouse movement by constraining it to a particular dimension, such as
35mm.  Moving *parts* of an object, such as vertices, faces, or edges
of a polyhedron, provides more freedom; this requires the ability to
shift between different levels of selection, which is done with the
[MacDraw-style][7] ([manual page][8]) group-ungroup operations common
to most 2-D vector drawing programs, which SketchUp calls “Make group”
and “Explode”.  Extra virtual “handle” objects provide targets for
multi-argument operations like scaling, which can scale along one,
two, or three axes, depending on which handle you drag with the mouse.

SketchUp also has a mode for drawing paraxial rectangles (activated by
the R key), but, in a sense, that is redundant with the paraxial
constraint inference on line drawing, since you can draw a paraxial
rectangle just by drawing four paraxial lines.  Because SketchUp
infers faces within coplanar polygons and infers solids within closed
faces, just drawing lines is in some sense fully sufficient for
modeling polyhedral objects; even push/pull is redundant.  Drawing
lines across existing faces splits them, which is useful for
pushing/pulling part of a face or for deleting part of it.
Additionally, you can enter absolute coordinates (`[3', 5', 7’]`) or
relative coordinates (`<1.5m, 4m, 2.75m>`) instead of just lengths.
And at least [for rectangles you can type `12,8` as well][21].  I
don’t know if there’s an input syntax for a length and an angle, as in
AutoCAD.

By default, in SketchUp, the plane in which mouse movements are
applied is context-sensitive; in some cases it’s parallel to an
existing face, while in other cases it’s perpendicular to an axis of
space, usually the vertical axis.  In the case of drawing a freehand
line it chooses a paraxial plane based on, perhaps, which one gives
the shortest line length.  They call these heuristics the [“SketchUp
inference engine”][16].  You can press some keys such as the arrow
keys (modally, not quasimodally!) to “lock” certain constraints,
mostly the paraxial ones, and the Shift key functions (quasimodally!)
to “lock” the currently active constraint.

[4]: https://youtu.be/qgt2s9RzvKM
[7]: https://youtu.be/_PBtUodX_dM?t=622 "MacDraw 1.0 from 01984 has the Group/Ungroup operations on its Arrange menu"
[8]: https://youtu.be/svOsoFH1uNc?t=772 "MacDraw 1.0 manual page 97 documents Group/Ungroup"
[16]: https://help.sketchup.com/en/sketchup/introducing-drawing-basics-and-concepts
[21]: https://youtu.be/t3Vq-nSBRCc?t=1m10s

Another context-sensitive heuristic thing is what depth a point is
placed at; by [default it’s placed on the planes of the axes if
there’s no “support face” at the mouse position.][17] For example, it
might be on the “ground”, the X-Y (red-green) plane.  Of course there
may be more than one axial plane at the mouse position, maybe all
three; I don’t know if it picks the nearest one, the one where the
point would be closest to the origin, or what.

[17]: https://forums.sketchup.com/t/drawing-on-a-higher-plane/218154

Just by implementing drawing of lines and inference of faces and
solids, plus some way to delete lines, you could have a 3-D modeling
program useful for some purposes.  By adding circles (SketchUp’s C
key), extrusion or pocketing (SketchUp’s P key, which is on the wrong
hand), and a way to modify existing constraints, you get already quite
a bit of modeling power.  By making these operations all quasimodal
rather than modal and by using a first-person view, you’d have much
better immersion and also better ease of use.

SketchUp does support [quasimodal zooming, panning, and orbiting with
the mouse][15]: scroll wheel to zoom, middle-button drag to orbit, and
shift-middle-button-drag to pan.  The status bar displays the pan
option and a “suspend gravity” option once the middle button is
pressed, but doesn’t suggest pressing the middle mouse button to orbit
normally.

[15]: https://youtu.be/I_bJPNnO3HQ?t=6m28s "Getting Started with SketchUp Free - Lesson 1 - Beginners start here: 'The better way to [navigate] is if you have a three-button mouse with a scroll wheel...'"

SketchUp’s interface is designed around a conventional WIMP GUI, with
toolbars, menus, and the consequent abundance of mode errors, where
users perform an action in the wrong mode, resulting in the wrong
action.  This also makes SketchUp feel considerably less immersive
than something like [Dark Souls][6] or [Minetest][5], but its barrier
to entry is much lower.

[5]: https://youtu.be/va-YHeytvFQ
[6]: https://youtu.be/eXcoQb729N4

### Blender ###

Blender is also mostly a conventional WIMP GUI that is profoundly,
fractally modal.  [Entering the “move selected items” mode][18], for
example, can be done with the G key, a toolbar button, or an item on
the shift-spacebar popup menu.  As with SketchUp’s “scale” operation,
the toolbar button adds virtual “handles” to the scene that you can
use to do particular kinds of movement.  Left-clicking exits the move
mode, and Esc or right-click exits and cancels the operation; typing
“x”, “y”, or “z” sets an additional paraxial-move mode constrained to
go parallel to a given axis of space.  (As with SketchUp, Z is the
vertical axis.)  R is rotate, S is scale, and they also add clickable
handles that provide more arguments.  Scroll wheel zooms,
middle-mouse-drag orbits, and shift-middle-drag pans, same as in
SketchUp; ctrl-middle-drag zooms.  There’s a quasimodal pie menu of
common commands that pops up on the `` `~ `` key.

Blender uses mouse-capturing drags from UI widgets pretty extensively,
including an orbit widget in the upper right and also its substitute
for sliders in dialog boxes, which last also permit typing numbers and
have increment/decrement arrows like a scrollbar.  Ctrl
(quasimodally!) makes these drags snap to a grid.

[18]: https://youtu.be/nIoXOplUvAw?t=2m53s

When you perform an operation in Blender, [it also adds a HUD-like
overlay with parameters for the operation][19], which function similar
to the in-world handles for the Rotate and Scale operations, though by
default they’re collapsed.  The “parameters of the last action”
dialog, also available with F9, is interesting; it means you see the
results of your action even before you supply the parameters,
providing more immediacy.

[19]: https://youtu.be/imdYIdv8F4w?t=1m49s

Modifying parameters for the action also modify the defaults for those
parameters for future actions.  This is occasionally annoying but more
often super convenient; in [My Very First Raytracer][20] I used
exactly the same approach for a file format for raytracing, so if I
had a series of objects that was the same color or the same texture or
along the same Z-paraxial line, I could specify the color or texture
or X and Y coordinates only once at the beginning of the object.

[20]: http://canonical.org/~kragen/sw/aspmisc/my-very-first-raytracer

Blender also has abundant keys to pop up menus at the mouse:
conventional right-click and the `` `~ `` that I mentioned above, but
also F9 for the parameters of the last action, shift-A to add objects,
ctrl-A to apply transformations, ctrl-tab for a pie menu of modes, and
so on.

Blender’s “move” action is always parallel to the viewport, unless
specified otherwise, rather than attempting to guess a reference plane
like SketchUp.  In part I think this is because Blender is more
optimized for sculpture while SketchUp is more optimized for
architecture and mechanical design.  Blender’s “proportional move”
mode for vertices is another example of this; it has an
adjustable-size radius within which it moves *all* the vertices of the
active object, but moves the nearer vertices by more, I think with a
Gaussian or something.  This is great for sculpture but pretty useless
for mechanical design.

### Diegetic user interfaces in FPSes ###

In 3-D games, there’s an ongoing discussion about so-called [“diegetic
user interfaces”][9], a term [invented by Erik Fagerholt and Magnus
Lorentzon for their (‽) masters’ thesis in 02009][12] and possibly
popularized by [a now-deleted Gamasutra article by Anthony
Stonehouse][11].  In a departure from the standard usage, in which
everything on the screen and everything that happens in response to
user actions is the “user interface”, 3-D game designers commonly use
“user interface” or “UI” to refer to 2-D GUI elements overlaid on the
3-D view, more precisely called the “HUD”, or “heads-up display”, and
2-D GUI elements that replace the 3-D view entirely.  The “diegetic”
movement is to move these elements into the game; the most commonly
cited example is how 02008’s Dead Space moved the standard [FPS][10]
health bar from the HUD onto a backpack on the back of the player
character, but a free-software example is how in the NodeCore
derivative of Minetest by Aaron Suen (Warr1024), [you craft new
objects through in-world actions instead of in the separate 2-D
crafting user interface Minetest copied from Minecraft][13], and you
store your accumulated goods on in-world shelves instead of a separate
2-D chest UI.

[9]: https://codechangers.com/blog/User-Interface-Elements-in-video-games-part-3-diegetic-ui/
[10]: https://en.wikipedia.org/wiki/First-person_shooter
[11]: http://web.archive.org/web/20160324100740/http://www.gamasutra.com/blogs/AnthonyStonehouse/20140227/211823/User_interface_design_in_video_games.php
[12]: https://publications.lib.chalmers.se/records/fulltext/111921.pdf "Beyond the HUD: User Interfaces for Increased Player Immersion in FPS Games, Erik Fagerholt, Magnus Lorentzon"
[13]: https://youtu.be/8WXpBapUrtg?t=192

So, I’m not exactly proposing a diegetic UI for 3-D modeling, but
something somewhat similar: a *more-direct-manipulation* UI, using
quasimodes.
