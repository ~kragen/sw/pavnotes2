02022-09
--------

I mixed about 3% w/v carboxymethylcellulose into tap water and got a
very viscous liquid that seemed similar to hair gel.  [Ionotropic
Gelation Fronts in Sodium Carboxymethyl Cellulose for Hydrogel
Particle Formation][0] suggests that if it’s not already a gel, I
should be able to make it one by adding alum.  It was full of bubbles;
diluted about 10:1 with more tap water, it instead formed a sort of
thin syrup, a situation which seems like it might be more helpful for
removing bubbles without a vacuum pump.

Dispersing it in the water was a pain in the ass because I just dumped
the stuff in, and the CMC formed skins that encapsulated the remaining
dry powder, much as some dry clays do.  People say the same problem
arises with things like xanthan gum; one suggested remedy is to
disperse the dry gum in glycerin or something similar before mixing
into water.  Sonication is another solution.  Instead I stirred the
damn stuff with a fork for almost half an hour before I had a
transparent gel without opaque clumps of CMC suspended in it.

[0]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8167666/

### 02022-09-28 ###

I bought some dirt at the dirt store (see file
`material-sourcing.md`).

#### Vitriol crystals ####

53 g of smoothly-flowing blue vitriol powder, placed in a jar and
tared.  12 g of room-temperature water (probably about 25°) and 3 g of
chopstick to stir with: it becomes a paste.  14 g more water (26 g
total) yields a liquid opaque blue suspension.  Another 15 g water
(41 g total) yields a thinner suspension which defecates in a minute
or so, the liquid being darker and greener than the solid and of about
equal volume.  Another 21 g water (62 g total water) increases the
volume of the liquid without reducing the solid much.  It smells
slightly acidic and makes me sneeze, a bit like phosphoric acid.

At this point the fucking scale turned off.  It had been registering
-256 g when the jar was off of it, and now it registers -321 g, so the
jar and vitriol were 256 g and the water was apparently 65 g.  I’ve
added 50 g more of water (115 g total water) and the situation is
basically unchanged but with more liquid and less solid.  After an
additional power cycle and tare from empty it registers 371 g.  I
added another 52 g water to bring the total weight to 423 g (167 g
water) and the powder layer that settles at the bottom is now almost
gone, just a millimeter or two.  So this is a saturated solution of
about 50 g of blue vitriol in 167 g water, about 30 g/100 mℓ.

Wikipedia gives the solubility in water as “1.26 molal” at 20° and
“1.502 molal” at 30°, so the expected solubility should be about 1.381
mol/kg.  The pentahydrate should be 249.685 g/mol, so we should expect
about 34.5 g to dissolve in 100 mℓ of water.  This is a rather
imprecise result, about 15% higher than I’m getting, but given that
I’m not using a thermometer, maybe no more imprecise than you’d
expect.  At least I know I’m not confusing mol/kg with mol/100mℓ or
anything like that.

After a longer period of decantation, less than an hour, the deep blue
liquid has become quite transparent, though the defecate is not
visibly deeper.

After further hours of decantation I poured 89.17 g of the clear blue
saturated liquid into a shallow container in the interests of getting
some crystals and hopefully a more precise measurement of solubility.
I added a small amount (less than 10 mg) of the crystals from the
bottom to what I hope was the center.  Having done all this, I now
realize I should have weighed the container instead of just taring it,
so that I can tell how much the crystals weigh later.  It was
something like 16 grams.

#### Malachite/Green verditer ####

I’m going to try making some malachite.  Quoting Cooley from 01851:

> COPPER, CARBONATE OF.  *Syn.* Dicarbonate of Copper.  Mineral Green.
> *Prep.* Add a solution of carbonate of soda or potassa to a hot
> solution of protosulphate of copper.
> 
> *Remarks*.  The beautiful green mineral called *malachite*, is a
> hydrated dicarbonate of copper.  If the solution of copper in the
> above formula be employed cold, the precipitate has a bluish-green
> color.  (See Verditer.)

For better or worse Cooley doesn’t have articles on “protosulphate”,
“vitriol”, “copper, sulphate of”, “copper, protosulphate of”, or
“sulphate of copper”.  Dick’s 01872 version of Cooley has recipe 4096:

> **4096. Sulphate of Copper.** The *blue vitriol* of commerce is
> obtained from the native sulphuret of copper (copper pyrites).  Pure
> sulphate of copper is made by the direct solution of the metal, or
> preferably, of its oxide or carbonate, in sulphuric acid.  It
> consist of fine blue crystals, slightly efflorescent in the air.  By
> heat it loses its water of crystallization and crumbles into a white
> powder.  (*See No.* 120.)

And 5902–5904:

> **5902. Antidotes for Corrosive Sublimate [mercury chloride], or
> Calomel.** The white of eggs beaten up with cold water is the best
> antidote for these.  If eggs are not at once to be had, milk may be
> used with great success.  Warm water should be given afterwards, to
> induce vomiting, also free purging in most instances.
> 
> **5903. Antidote for Corrosive Sublimate.** In case of poisoning by
> corrosive sublimate, if a dose of the hydrated protosulphuret of
> iron (*see No.* 4149) be administered it instantly renders the
> poison innocuous.  This antidote is almost useless unless taken
> within 15 or 20 minutes of swallowing the poison. [I think this
> “protosulphuret” is ferric sulfide, as he says it’s a black
> substance rapidly decomposed by exposure to the air, and his recipe
> involves precipitating a soluble iron salt (“protosulphate of iron”,
> which I think is ferric sulfate) with “sulphuret of potassium.”]
>
> **5904. Antidotes for Verdigris and Sulphate of Copper.** The
> treatment is the same as for corrosive sublimate.  *See No.* 5902.

Dick omits Cooley’s carbonate recipe and verditer articles.  Dick’s
No. 120 is the article on making “Blue Stone”, which is just blue
vitriol again.

WP says 43.6 g of anhydrous Na₂CO₃ dissolves in 100 mℓ water at 100°,
which at 105.9888 g/mol would be 0.411 mol, made from 0.823 mol of
NaHCO₃, which at 84.0066 g/mol would be 69.1 g.  So I can put tens of
grams of baking soda into the few hundred milliliters of tap water I
have boiling here.  I measured out 52.53 g on a weighing boat and
dumped it in, provoking excited bubbling, but some stuck to the boat,
so I tried washing off the boat in the nearly-boiling water, but I
think I ended up with more soda on the boat than before, so there’s
something like 52 grams of baking soda in there.

I also spilled a bunch of baking soda on the scale in the process.
But that sort of doesn’t matter because I don’t know how much water
was in the pot when I started adding the baking soda, much less how
much was left when I added the soda.

The bubbling calmed down a bit, so I turned off the flame.  The soda
seems to be totally dissolved except for the substantial amounts dried
on the sides above the water line.

To a 180-gram jar I added 53 grams of the saturated clear blue liquid
(by coincidence the same as the amount of solid blue vitriol I used to
prepare the solution) but I stirred up a bit of solids in the process
apparently because it’s slightly cloudy.  I added some of the not
saturated but slightly cloudy soda solution, 29 g (262 g total
including the jar) and got the expected abundant cyan bubbling.  But I
spilled soda solution all over the scale in the process.  After drying
off the scale and rebooting the fucking scale again, it read 264 g,
perhaps because at some point I added a 3-gram chopstick to stir with.

I added more soda solution to get to 360 g, thus 180 g of contents.
Bubbling happened more slowly.  The solution looked like blue-green
paint of the standard basic copper carbonate color, and began to
decant and leave a transparent layer at the top, darker and bluer,
after a few minutes.

A further addition of the soda solution provoked no further bubbling,
so I guess there’s an excess of soda in there now.  Now the weight is
363 g so I guess I added about 3 g of soda solution for a total of
183 g of contents.

In theory almost all the copper now should be in the form of malachite
suspended in the solution, despite the vivid blue color of the
supernatant.  53 g of saturated solution at what my thermometer tells
me is a room temperature of 22½°…

In the process of finding the thermometer, in Keystone Kop fashion, I
managed to knock over the shallow container of saturated solution onto
my roll of cleanup towels, spilling part of its contents.
Its seed crystals (if any) are presumably
no longer in the center, and which now has a lower weight of 93.86
grams, including both the container and the liquid.  So I probably
lost about 11 grams.

Anyway, back to how much malachite to expect.  Saturated CuSO₄
solution at 22½° should be about 1.321 molal.  1.321 mol/kg ×
159.60 g/mol = 210.8 g/kg, so every 1210.8 g of solution ought to
contain 1.321 mol of copper sulfate and thus of copper.  53 g should
give us 0.058 mol.  The malachite formula Cu₂CO₃(OH)₂ gives a molar
mass of 221.116 g/mol; Wikipedia says 221.114.  0.029 mol of the stuff
should be about 6.4 g, a number which seems suspiciously low to me.

Let’s look at it another way.  Anhydrous copper sulfate is 39.82%
copper by my calculations (WP says 39.81%).  The saturated solution
should be 17.4% copper sulfate and thus 6.9% copper: 3.7 g.  Ideal
malachite is 57.48% copper, so 3.7 g of copper should give us about
6.4 g, just as I derived above.  But that 6.4 g, or some hopefully
large fraction of it, is now dispersed in 177 g of water and sodium
sulfate and miscellaneous impurities.

Copper sulfate pentahydrate, 249.676 g/mol, is 63.92% anhydrous copper
sulfate.  So if I were to convert my whole 250-gram bag of copper
sulfate (presumably pure pentahydrate) to malachite I should expect
250 g × 63.92% × 39.82% / 57.48% = 111 g of malachite.

It’s defecating very slowly indeed; I guess reacting such concentrated
solutions resulted in a very fine particle size, which probably also
results in less color saturation than would be ideal.  Filtering it
through infusorial earth would dilute the pigment nanoparticles with
more white crap, too.  I don’t have an aspirator for vacuum
filtration, but maybe gravity filtration would be fine.

    |  time | mm from top of |   distance |    minutes |         speed |
    |       |  jar to solids | from start | from start |         (m/s) |
    |-------+----------------+------------+------------+---------------|
    | 21:58 |             55 |          0 |          0 |          0./0 |
    | 22:07 |             57 |          2 |          9 |  3.7037037e-6 |
    | 22:19 |             56 |          1 |         21 |  7.9365079e-7 |
    | 22:53 |             54 |         -1 |         55 | -3.0303030e-7 |
    #+TBLFM: $3=$2-@3$2::$5=.001*$3/($4*60)

The solids layer is still like 70 mm tall, but the liquid layer at the
top is already about a third of the thickness of the layer with
suspended solids, like 26 mm or so.  But there’s less than 3 mm of
progress over the last hour, maybe zero.  Maybe I need a flocculant or
something if I want decantation to continue.

It might be better to redo the reaction while adding the reagents
dropwise over the course of a day to a vessel being continuously
stirred, in order to get a larger particle size.  Wikipedia gives the
solubility product Kₛₚ of basic copper carbonate as 7.08 × 10⁻⁹, which
I guess is the product [Cu]²[CO₃], which I guess is roughly the cube
of one third of the concentration of dissolved malachite.  Like, if
0.006 of a solution was dissolved malachite, about 0.004 of it would
be copper and 0.002 would be carbonate, and the product would then be
32 × 10⁻⁹?  So the correct number should be around .003 of the
solution?  (WP says the solubility S = ∛(¼Kₛₚ) for a salt with three
ions like this.)

But 0.3 g per 100 mℓ seems like an awfully high solubility for a
substance that is normally considered insoluble; people say things
like, “Malachite is sensitive to heat, acids, ammonia, and hot
waters. Rinsing malachite in clean water is safe,” and, “Because of
its softness, malachite can easily be scratched even by dust. Take
care when wiping dry malachite as this can cause scratches and reduced
luster. The best way to clean malachite is by using a mild liquid soap
and warm water. For clinging dirt or grime, use a soft toothbrush or
cloth to gently clean the stone. Rinse thoroughly to remove any soapy
residue. Once washed, dry the jewelry well prior to wearing or
storing, as this will keep it free from water stains.  Malachite is
sensitive to heat so keep away from hot water, sunlight and sources of
heat.”

So I must be misunderstanding something.  But maybe adding a drop
(50 μℓ?) at a time of a somewhat diluted solution to a stirrer that’s
stirring a liter would be slow enough.  That would be 5 μg/ℓ rather
than the 300 g/ℓ or so I was using here, so it seems likely it would
have a different result.  But maybe I don’t have to jump down all 8
orders of magnitude at once, particularly since I don’t have the
apparatus handy.

Before I try that, I ought to try a gravity filtration in a soda can
with a coffee filter.

Okay, that seems to be working, and the malachite is actually kind of
clumpy, so maybe it’s not in quite as tiny nanoparticles as I thought.
But I still can’t feel it; water with the stuff in it just feels like
water to my fingers.

I’ve set the shallow dish of saturated solution on the back of the
cabinet with a sheet of aluminum foil over it to keep dust from
falling in.

After filtering for a while, the filter is full of a bright cyan
malachite mud which feels like a gel.  There is no perceptible
grittiness to it at all.  But it seems to be retaining a lot of water
instead of passing it through.  The can below is full of perfectly
transparent blue liquid.  I ran about 300mℓ more tap water through the
filter to see if that would wash out some of the soluble ions and
maybe help a bit with the enthusiastic water retention.  I also took
this opportunity to rinse the jar I did the reaction in originally.
Twice I overflowed the filter funnel a bit and lost some malachite
onto the table.

This later filtrate seems to be completely transparent, or maybe just
very faintly blue, and the material in the filter might be decreasing
in volume, so I think it’s working.  Though it’s certainly much
smaller than when it was merely decanting, it still feels like a
squishy mud or gel, not a wet solid.  Maybe tomorrow I’ll see if boric
acid, muriate of lime, epsom salts, or alum can flocculate it.

### 02022-09-29 ###

#### Copper salts ####

Upon looking at the jar of early filtrate from the malachite from last
night, it appears to be not only considerably thinner but also
considerably greener in hue than the original liquid.  Does this mean
it has just oxidized from exposure to the air?

Reading Adam Seychell’s guide, “Etching with Air Regenerated Cupric
Chloride”, he explains that dissolved air and acid can reoxidize Cu⁺
(cuprous) ions back to Cu⁺⁺ (cupric) to regenerate that etchant;
Wikipedia tells me that dry CuCl also oxidizes in air to CuCl₂.  The
CuCl₂ solution he’s going for is bright green, while CuCl
contamination turns it not blue but brown to black.  So, yes, Cu⁺⁺ is
green, but the copper in the blue vitriol is already Cu⁺⁺.  It’s the
aquo complex that turns it blue.

Possibly the early filtrate is greenish because it’s contaminated with
malachite particles, even though it doesn’t look cloudy.  Even though
it’s much more dilute than the saturated blue vitriol solution I
started with, some kind of solid has decanted out of it.

The defecate in the filter had cracked from air-drying and
contracting.  I passed another couple hundred mℓ of tap water through
the stuff, and it seems to be both passing through more quickly and
leaving a thicker malachite mud.

It occurs to me that flocculating might require a different flocculant
for malachite than for clay.  Clay particle surfaces (except kaolin)
are generally negatively charged due to clay’s layer structure, but I
don’t know whether the malachite particles have a negative or positive
surface charge.  I guess I should just try things.

#### Literature review of ionotropic carboxymethylcellulose gelation ####

CMC is a nice cheap thickener and glue.  It’s also considered
food-safe, though [there are some concerns it might pose undetected
health risks][1].

[1]: https://pubmed.ncbi.nlm.nih.gov/18844217/

CMC is water-soluble because its carboxy substituents are negatively
charged, making it I guess an ionomer.  You’d think that maybe
polyvalent cations can ionically cross-link it into an insoluble gel,
like borate does to PVA glue and calcium does to alginate, and it
turns out that’s true.  In fact, historically speaking, the big
problem is that it’s *too easy* to make insoluble: calcium
precipitates it instead of just making a gel, and aluminum salts make
it gel so quickly that the mixture tends to be nonuniform, which often
is not desirable.

[The open-access article I linked earlier][0] gives the recipe they
used for their Fe⁺⁺⁺ ionotropic gelation.

02022-10
--------

### 02022-10-01 ###

#### Malachite ####

The cyan filtrate has lost all its cyan; it has deposited on the
bottom in the form of, apparently, larger malachite crystals (large
enough to sparkle, which the fine malachite dust does not).  Some of
the malachite in the filter has dried out; when dry its color is much
whiter, as I had feared.

#### Triethyl borate ####

I added some 95% ethanol to boric acid to form triethyl borate and set
it on fire to see the green flame.  Visually, this worked best when
the surface was small enough to prevent a yellow diffusion flame from
forming.  The white boria smoke was perhaps slightly irritating to my
lungs, but it’s hard to tell because we’ve been coughing a lot lately
anyway.

Adding the triethyl borate liquid, mixed with the residual water and
probably residual alcohol (since I’d waited less than a minute) to a
RAD-60 bottle, I got nice blowtorchy puffy rockety effects of green
flame.  This confirms that the green was not due to the solid boric
acid, but to the liquid.

### 02022-10-04: malachite ###

The cyan malachite defecate is almost entirely dry now.  Soon I can
weigh it.  Parts that have flaked off the coffee filter are slightly
yellower or greener on the filter side.

### 02022-10-06: blue vitriol crystals and malachite ###

The cyan malachite defecate is still almost entirely dry, but I have
not weighed it yet.  A tiny piece rubbed on the palm spreads with a
greasy feel and great opacity; further rubbing leaves it on the tip of
my finger in oil-bound specks of a slightly darker color.

The jar of saturated blue vitriol was growing thin crystals climbing
up the walls.  They were easily scraped off the walls with the
chopstick, which has acquired a blue-green color.

The crystal-growing dish has experienced less of this crystal
climbing, fortunately, and several large crystals have formed in the
bottom.

### 02022-10-09: blue vitriol crystals and dry malachite ###

The crystal-growing dish has gone completely dry, with several
beautiful large blue crystals.  The malachite also seems to be
completely dry.  The jar with the saturated solution has crystals
creeping up the walls again, but from near the bottom.

### 02022-10-10 ###

#### Molten boric acid failing to dissolve construction sand ####

I put some expanded perlite in a metal bowl, with some sand in a
depression in the perlite, and some boric acid in a depression in the
sand, and placed an arch-shaped broken fragment of waterglass-cemented
vermiculite over it to shield heat loss.  With a handheld butane torch
I melted the boric acid for a few minutes, which bubbled and produced
green flame shooting out under the other side of the vermiculite
refractory.  At maximum heat it glowed bright orange.  When I stopped
and lifted the primitive refractory arch off the mass, it drew out a
bit of what seemed to be glass fiber from the solidified mass, and
lifted it up; it evidently had soaked down to the bottom of the sand.

The bottom surface had perlite stuck to it, and the top surface looked
wet even after cooling, but mostly it looks like a clump of sand, or
sandstone.  It is hard, but with a sharp blow using two chunks of
granite, I am able to break it into several pieces.  Internally it is
somewhat foamy and glittery; I suspect that the glitteriness mean that
the mostly-silica sand remains crystalline rather than dissolving into
the liquid boria.

I suppose that one difference between glassy boria with embedded
silica and borosilicate glass is that the former is water-soluble, so
I heated this solid in another metal bowl in some water for a while,
and in a few minutes it reverted to being just piles of sand under
cloudy water, with bits of perlite floating on the surface.

### 02022-10-21 ###

#### Blue vitriol results ####

The original jar of saturated blue vitriol is quite dry now, with a
white powdery-looking precipitate on the floor between the blue
vitriol crystals; this might be an impurity from the water or an
impurity present in the original blue vitriol, but it seems to be
mostly or completely absent from the shallower crystallization dish,
which is of course still quite dry, as is the (still unweighed) green
verditer in the filter.

The blue vitriol crystals adhered to the PET crystallization dish very
little if at all.  The masses of tiny crystals that formed climbing up
the walls are very sparkly and look a bit cyan.  They have a glassy
sound when they break.  The larger single crystals found in the bottom
are more of a deep blue.

#### Carboxymethylcellulose clay body ####

I mixed some kaolin with a smaller amount of beach sand, some
carboxymethylcellulose, and some tap water in a Maruchan styrofoam
cup.  It formed a bisque-colored paste.  From the color I suspect the
kaolin is not very pure.  I guess I should have measured the
ingredients; then I would know how much water is a little too much,
because this is a little too much.

Between my teeth the kaolin feels slightly gritty, another sign that
it’s not very pure.

### 02022-10-22 ###

#### Carboxymethylcellulose clay body ####

The clay body was still a viscous, slowly flowing liquid.  I added
some bentonite and seemed to get it to a kind of wet gelled paste
consistency.  It’s very moldable but still too wet.  I’m going to let
it rest another day.  I suspect dry-mixing the clays (as I did with
the kaolin, CMC, and sand) would probably give a good mix with much
less effort.

It was still rather sticky, coating my hands with bisque-colored mud,
so I spread it out to dry a bit on a styrofoam supermarket-products
tray using what I think is a polyvinylidene chloride film.  It did not
stick to the film except a very small amount, nor to the styrofoam.

A few hours later, it was very plastic but kind of rubbery, like
Play-Doh rather than clay.  This should make it easy to build
super-thin structures you can’t normally make out of clay, but it’s
terrible for geometric precision or picking up surface texture.  (Then
again, the coarse beach sand I added as a temper is also terrible for
picking up surface texture.)

I think I used about an order of magnitude too much CMC.  This is
about 236 grams of clay body (finally I did bother to do some kind of
quantitative measurement) plus maybe 1–2 g of styrofoam tray.
Probably about 50 g of that is water, and I had added a whole tiny
heaping spoonful of the CMC, maybe a gram, so it’s maybe not
surprising that it was so gummy.

For some reason it smells like some kind of cleaning-product perfume
now.  Maybe it picked up perfume from my hands (soap?) or from the
styrofoam tray.

I was astonished to convert a clay body into a rubbery substance in
this way.  I suspect that I can cross-link the carboxymethylcellulose
with borate by using boric acid and get a similar, but
water-insoluble, effect with much less CMC; and if I end up firing the
result, the boric acid ought to act as a flux to permit sintering and
full vitrification of the clay body at a relatively low temperature.

In the limit maybe I can get a plastic “clay body” with almost no clay
that can then fire into a ceramic, consisting of boric acid (or
borax), a silicate filler (perhaps quartz, cristobalite, infusorial
earth, perlite, or pulverized soda-lime glass), a small amount of a
borate-crosslinkable ionomer like CMC (?) (or xanthan gum, polyvinyl
alcohol, carrageenan, gelatin, gelatinized starch, etc.), and some
clay, maybe something like bentonite.

The idea is that when you fire it, the clay will hold the filler
grains in place after the organic binder burns out and the boric acid
melts, and then a borosilicate glass matrix will form by means of the
hot boric acid dissolving a bit of the silicate.

Mixing the dry ingredients thoroughly in powder form might help to
prevent the borate-linked gel from blocking absorption of the water.
It does seem to have avoided the mixing problem I had previously had
when I just dumped CMC into water.

It also occurred to me that some blue vitriol might be useful if the
CMC starts to rot.

I’m leaving the brownish pink clay body squished out flat to dry a bit
more and see how it acts in the morning.

#### Leftover loose ends ####

I still haven’t weighed the damn malachite.  Or, for that matter, the
crystals in the crystallization dish, which I think was originally
89.17 g of saturated liquid (see above under 02022-09-28) and then
weighed 93.86 g after I spilled it, including the container (same
day).  Because the crystals unstick from the PET container so easily,
it should be easy to weigh them separately from the container and thus
derive the precise weight of the container and thus a solutes
concentration of the original liquid good to three decimals or so.  I
only know the temperature to within a degree or so, though, which
should give rise to about a 2% variation in solubility, 20 times
greater than the likely error in weight measurement.

### 02022-10-23: carboxymethylcellulose clay body ###

Unsurprisingly in the morning the edges of the clay-body pancake were
hard and crumbly and the center, though still plastic, easily
fractured.  I rolled a sausage (still possible) of perhaps 10mm
diameter and put it on the stove to cook.  It first whitened with the
heat (from drying, I suppose) and then blackened, presumably from
charring the CMC.  The blackening did not seem to be a matter of soot
deposition from the (nominally premixed methane) flame, because it did
not begin blackening until after it whitened, and then crept around
the back where the flame didn’t impinge directly.

The lower part of the sausage, directly in the flame, heated to
red-orange.  It swelled and sagged slightly in the middle, suggesting
that the heat produced steam within it but that the CMC retained
significant plasticity.

After a couple of hours of cooking this way, the blackened part had
mostly turned light grey, suggesting that the charred CMC had been
burned away.  I lifted it from the stove using wadded-up aluminum foil
for my welding gloves and then used the wad of aluminum foil as a
cooling stand to let it cool.

It would be surprising if heating to a mere red-orange had resulted in
any sintering at all, since this clay body is mostly kaolin; I’ve
probably just dehydrated it into metakaolin.  It took several minutes
to cool down and then was fragile, like dried mud, confirming no
sintering.  A little blackness was still visible at the ends of the
sausage, where it wasn’t directly exposed to the flame, but inside the
broken surface there was no blackness, so the CMC inside had all
finished burning away.

A toothpick stuck into the interior of the broken surface is able to
crush its way in without splitting the whole sausage, showing
significant porosity.

### 02022-10-23 ###

The clay body has largely dried out around the edges.

#### Coffee ####

I’m making coffee with San Regim “Nueva Fácil Disolución” nonfat
instant milk.  To an empty styrofoam cup I added 11.06 grams of dried
milk, “Arlistan” brand adulterated instant coffee, “suave y espumoso”,
to 22.03 grams (10.97 g of coffee), then removed the cup from the
small scale to add water, giving a reading of -27.52 g, meaning the
styrofoam itself weighed 5.49 g.  To prevent the milk from scalding
when adding hot water, I added cold tap water to a reading of 82.12 g
(computed 109.64 g total, 60.09 g cold water) and tared it.  Then I
added 190 mg of “La Parmesana” brand fake vanilla extract.

Upon lifting the cup from the scale I got a reading of -109.76 g.
This should have been -109.64 g, indicating drift of about 0.1% or
120 mg.

On the large scale, I tared the black coffee cup (247 g) and
transferred the cold mix to it, which resulted in a reading of 114 g
on the large scale and -100.92 g on the small scale.  These numbers
ought to sum to zero, since both scales were tared before the
transfer, and the difference suggests errors on the order of 10% on
one or both scales.

To rinse the residues from the styrofoam mixing cup, I added hot water
(just boiled, but perhaps cooled down too much) to the styrofoam cup
on the small scale to a reading of +18.84 g, giving a total of
119.76 g of hot water.  Upon transferring this to the black coffee cup
on the large scale, the reading was 236 g, having been 114 g before,
meaning a transfer of 122 g, 2.14 g more than the small scale
registered, even though surely some drops remained in the styrofoam
cup.

The dirty styrofoam cup weighed 6.93 g after drying out for a bit,
with noticeable amounts of coffee contamination on the styrofoam.
After washing and drying it, it’s down to 6.16 g, suggesting about
770 mg of water and other coffee ingredients failed to get
transferred.  But the styrofoam is still stained brown, so some coffee
is left in it.  The total coffee depletion induced by the extra mixing
step could have been as high as 10%.

The resulting coffee was too bitter and too cold.  I made it tolerable
with 15 drops of “Hileret Clásico Forte con Zinc” (3200 mg sodium
cyclamate per 100 mℓ, 2000 mg per 100 mℓ sodium saccharine, plus some
amount of zinc sulfate, INS 221 preservative [sodium sulfite, an
oxygen scavenger], and INS 330 acidifier [citric acid]).

15 drops of aqueous solutions is nominally 0.75 mℓ.  But 40 drops of
the Hileret weighs in at 3.94 g, 99 mg per drop, so 15 drops is
probably more like 1.48 mℓ.  The cup containing these 40 drops weighs
9.97 g after restarting the scale (6.16 g + 3.81 g liquid, 0.13 g less
than there should be); 20 more drops bring it to 12.03 g, 2.06 g over
the 9.97 and thus 103 mg per drop.  Washing and drying the cup again
brought its weight back to 6.19 g, 30 mg higher than before, but
600 mg higher than at the start of this process when it weighed in at
5.49 g.  Some of the Maruchan paint on the outside of the cup is
coming off because I washed it with alcohol.

So 15 drops, 1.48 mℓ, is probably 47 mg of sodium cyclamate and 30 mg
of sodium saccharine.

The final recipe for café cortado that is too strong, not hot enough,
and not milky enough is thus:

- 11.06 g dried “San Regim” nonfat milk
- 10.97 g “Arlistan” instant coffee
- 179.85 g tap water, of which:
    - 60.09 g cold (≈20°)
    - 119.76 g just boiled (≈80°)
    - 60° average temperature
- 15 drops “Hileret” sweetener (1.5 mℓ), of which:
    - 50 mg sodium cyclamate
    - 30 mg sodium saccharine
- 190 mg “La Parmesana” fake vanilla extract

That’s about 204 g in total, and it filled the cup almost perfectly.

The coffee probably cooled a lot as I was dashing back and forth to
take down weights.

Based on this, a better recipe would probably be:

- 20 g dried “San Regim” nonfat milk
- 8 g “Arlistan” instant coffee
- 180 g tap water, of which:
    - 50 g cold (≈20°)
    - 130 g boiling (100°)
    - 78° average temperature
- 10 drops “Hileret” sweetener (≈1.0 mℓ), of which:
    - 32 mg sodium cyclamate
    - 20 mg sodium saccharine
- 5 drops “La Parmesana” fake vanilla extract (≈500 mg)

[Temperatures of above 83° will scald milk][sm], which can turn it
into nasty little chewy particles of cheese.  Hopefully 78° is a safe
temperature.

[sm]: https://en.wikipedia.org/wiki/Scalded_milk

With this untested recipe, a jar of 100 g instant coffee will make 12½
coffees.  Using the prices from file `food-prices-02022.md` a coffee
 costs 14.4¢ of instant coffee and 12.1¢ of dried milk, plus probably
very small amounts of energy, sweetener, and vanillin, for a total of
about 27¢ (AR$78).

The Arlistan bottle recommends adding, to 100 mℓ of water, “one
teaspoon” of instant coffee, which it claims should be 1.5 g, of which
1.1 g is carbohydrates (corn syrup and caramel coloring, i.e., brown
dye).  That would be about 2.7¢.  Then you add milk!  So I made it
roughly 7× as concentrated as the bottle recommends and I think it
will be tasty if you make it only 5× as concentrated.

The San Regim box recommends 100 g of milk powder to 900 g of water,
coincidentally exactly the 20:180 ratio I settled on above.

A mateful of yerba weighed in the styrofoam cup is 35.58 grams; the
cup weighs in now at 5.46 g, which is quite close to 5.49 g it weighed
originally.  This suggests that maybe the 6.16 and up numbers were not
measurement errors as I thought, but just 600 mg of alcohol that
hadn’t evaporated yet.

This mateful of yerba is equivalent to about two coffees, since it
serves to caffeinate about two people; it costs 9.9¢.

I can buy food-grade sodium cyclamate by the kilogram for
AR$4000–6000/kg (US$13–21) and presumably some similar price for
saccharin, but it might take quite a while for that investment to pay
off.  A 500mℓ bottle of Hileret costs AR$370 (US$1.28), contains 26
grams of these sweeteners, and should last about 21 months, so you
couldn’t start saving money that way until your 11th bottle of
Hileret, and even then you only save about ⅔; the imputed price at
AR$370 is AR$14000 per kg of dissolved sweetener.

### 02022-10-24: coffee ###

I made a coffee today:

- 20.01 g dried milk
- 8.28 g instant coffee
- 0.49 g fake vanilla (20 drops!  Within 2% of the 50mg/drop units(1)
  claims!)
- 10 drops of sweetener (0.79 g, 79mg/drop, about halfway in between
  50mg and the previously measured 100mg)
- 53.01 grams of cold tap water
- remainder (&lt;150 g) of boiling tap water

This time instead of a styrofoam cup I used a thin nickel-plated steel
bowl, which weighed 27.76 g after being rinsed out with the boiling
water.  Before adding the tap water, I registered a -56.75 g tare
weight, which I guess gives (- 56.75 20.01 8.28 0.49 0.79) = 27.18 g
for the metal bowl.

I weighed 149 g of tap water before boiling, but upon trying to weigh
the amount I added to the coffee mug, the large scale ran out of
battery.  I think it was about 130 g because the mug was very nearly
as full as previously.

The coffee was tasty this time, creamy with a hint of vanilla, but
still a bit too strong.  The milk apparently did scald a little
though, possibly only the part I rinsed out of the bowl with boiling
water (burning my hand through the thin bottom of the bowl, spilling a
little bit).

For tomorrow I think I’ll try:

- 20 g dried milk
- 5 g instant coffee
- 0.5 g fake vanilla (20 drops)
- 0.8 g of sweetener (10 drops)
- 50 g cold tap water
- 130 g boiling tap water

### 02022-10-25: coffee ###

I attempted a coffee according to the recipe above, using the same
bowl for weighing ingredients.  The bowl initially weighed 27.26 g,
500 mg less than yesterday, perhaps because it was dry.  I added:

- 4.96 g adulterated instant coffee
- 20.09 g dried milk
- 0.95 g of fake vanilla (20 drops)
- 0.76 g of sweetener (10 drops)

I put 148 g of tap water on to boil, dumped the bowl into the same
black coffee cup (previously weighed as 247 g), added 52 g of cold
water, and mixed with a lacquered chopstick.  Only a little bit of
powder remained stuck to the bowl, because I was able to use the
chopstick to scrape it out, and since I used the slightly sticky end
of the chopstick to mix the liquid and powders in the cup, relatively
little was left on the chopstick either.

Taring the coffee cup with cold mix and chopstick, I added all the
boiled water, which was at this point 124 g and nearly filled the cup.

I weighed the empty bowl with a little powder stuck to it as 27.41 g,
so I probably lost 150 mg of coffee and dried milk to the bowl.

I tared the whole cup and removed it, resulting in a reading of
-330 g, but I don’t remember if this was with or without the
chopstick.  Evidently it was without the hot water.  The 330 g should
have been (+ 247 52 4.96 20.09 0.95 0.76 -.15) = 325.61 g without the
chopstick, which itself weighs 4.41 g dry.  If the chopstick was
included that would be 330.02 g, but that’s probably more precision
than it’s reasonable to expect from these scales; it’s probably a
coincidence.

The coffee turned out creamy, flavorful, foamy, of properly balanced
strength, neither too sweet nor not sweet enough, and the right
temperature for drinking.  There were no scalded milk particles this
time.  The foaminess is probably mostly due to beating it with the
chopstick after mixing.  Summarizing, that recipe is:

- 4.96 g adulterated instant coffee (“Arlistan suave y espumoso”)
- 20.09 g nonfat dried milk (“San Regim”)
- 0.95 g of fake vanilla (20 drops) (“La Parmesana”)
- 0.76 g of sweetener (10 drops) (“Hileret Clásico Forte con Zinc”)
- 176 g of tap water, of which:
    - 52 g cold
    - 124 g boiling

This was twice as much vanilla as I had intended to use, but
apparently it was about the right amount.  The drop size of the
vanilla extract seems to be consistently larger than that of the
Hileret sweetener, and both are fairly consistent from day to day, to
within about 5%.  I don’t know why I got a drop size so much larger
for the Hileret the other day; I suspect measurement error, but the
100mg drops were consistent in three different weighings that day.

Transferring the powders before mixing them with cold water cut my
transferral losses from 770 mg (with the styrofoam cup) or 580 mg
(with the bowl) to only about 150 mg (3% if it was all coffee,
probably closer to 1%); this is probably lower loss even though all of
it was dry.  I could have lost less if I had made a pit in the middle
of the dried milk to contain the liquid sweetener and vanilla extract
so they couldn’t reach the edges.  A vibrator would also help.  150 mg
is only a little larger than the small scale’s typical random
measurement inconsistency of 50 mg or so.

I probably spilled a little powder in the transfer, which I don’t have
a way to measure.

I think it also would have helped if, after transferring the powder
from the bowl into the cup, I had weighed the cold water in the bowl,
thus rinsing the powder out of it.

Measurements:

- 27.27 g empty bowl (10mg more than yesterday)
- 5.04 g coffee
- 20.23 g milk
- 1.23? g of 20 drops of fake vanilla
- 0.70 g of sweetener
- 28 g of this mix transferred to the black coffee mug (which was
  tared on the large scale; should have been (+ 5.04 20.23 1.23 0.70)
  = 27.2 g, so I guess the scales’ calibration isn’t as far apart as I
  thought)
- 50.04 g of cold tap water, used to rinse the weighing bowl this time
- 77 g reading on the large scale after transferring the water to it,
  suggesting that the scales are even closer
- -324 g reading upon taring the large scale and lifting the cup to
  stir (should be (+ 247 50.04 27.2) = 324 g)
- 131 g of boiling tap water added (overshot by 1 g)
- -323 g reading upon lifting it again

I made a pit in the milk powder to contain the vanilla and sweetener
liquids, and this seems to have kept the liquids from sticking to the
bowl.  After rinsing the bowl with the 50.04 g cold water, I
transferred the water to the mug and stirred with the fork before
adding the hot water.

I’m not sure how I added so much vanilla; I think I was using too much
pressure at first and got drops that were too big, and I was having a
hard time seeing the scale readout.

Mina reports this was hotter than yesterday, stronger, and creamier,
but very tasty — a little too much vanilla.  Apparently the vanilla
makes the coffee taste stronger!

### 02022-10-31: coffee ###

I made another coffee.

I tared the metal bowl at 27.21 g.  To this I added 23.27 g of dried
milk (oops), this time La Serenísima “Leche Descremada en Polvo,
Liviana”.  Then I added 5.19 g of the same coffee, partly compensating
for the excess.  Taring again, I got a reading of -55.69 g upon
lifting the bowl; (+ 27.21 23.27 5.19) = 55.67 is the theoretical
value, so there’s a 20 mg error.

Then I made a little pit on top of the coffee and added 10 drops of
the same sweetener (1.07 g) and 10 drops of the same vanilla (0.48 g);
taring again, I got -57.22 g, precisely the theoretical value.  On the
large scale, I weighed the same cup again, getting this time 248 g
(possibly because it was wet), and upon taring added the bowlful to
it, getting 30 g of additional weight (should be (+ 23.27 5.19 1.07
0.48) = 30.01 g).  There was almost no powder stuck to the bowl,
though I did spill a little onto the scale.

I weighed the metal bowl again and got 27.26 g, 500 mg more than
earlier, which might be an estimate of how much powder was stuck to
it, or just a 500-mg measurement error.

I added 50.36 g of cold tap water to the bowl, swished it around a
bit, then added it to the cup, getting a reading of 81 g (30 g powder
mix + 50 g cold water).  Upon putting the bowl back on the small scale
it indicated I had 0.37 g water left.  Then I tared the cup and added
131 g of boiling tap water to the it.  Upon removing it from the scale
I got -328 g tared weight, which is precisely the theoretical result
of 30 + 50 + 248.

Mina reports that the coffee was very strong and creamy, more intense
than before, with more flavor, but maybe the milk got scalded a
little.  Maybe the different milk accounts for the flavor difference.

The milk is lot “4140 15-226”, expiring “20/MAY/23”.  The coffee is
lot 2062, expiring “06/06/25”.  The vanilla is lot 210902 expiring
“SEP 23”.  The sweetener is lot “1L 121007 31” expiring “07/23”.

02022-11
--------

Za3k reports: “At my local US grocery store, coffee is $0.39-$0.83/oz,
and instant coffee is $0.62-$0.87/oz. My particular instant coffee
suggest “1 rounded tablespoon per cup”.”

### 02022-11-08 ###

#### Styrofoam sheets for the Cryoziggurat ####

I want to build the Cryoziggurat (see file `cryoziggurat.md`), so I
went to the home improvement supplies store and bought styrofoam.
This is expanded polystyrene made by Estisol SACIF, and it comes in
one-meter-square sheets of different thicknesses.  (There are also
somewhat interlocking styrofoam bricks, which I didn’t buy and don’t
know the price of.)

    | kg/m³ | mm | nominal kg | AR$ | AR$/m³ | AR$/kg | US$/m³ |
    |-------+----+------------+-----+--------+--------+--------|
    |    12 | 10 |       0.12 | 159 |  15900 |   1325 |  55.02 |
    |    12 | 20 |       0.24 | 300 |  15000 |   1250 |  51.90 |
    |    12 | 25 |        0.3 | 390 |  15600 |   1300 |  53.98 |
    |    15 | 30 |       0.45 | 750 |  25000 |   1667 |  86.51 |
    #+TBLFM: $3=$1*$2/1000::$5=$4*1000/$2::$6=$4/$3;%.0f::$7=$5/289;%.2f

Here we can see that the lightest stuff, which is the best insulation,
is actually cheaper per kg than the heavier (and stiffer) stuff, which
is the reverse of what I would expect.  Also the thinner sheets only
come in the lowest density, and the thicker ones only come in higher
densities, which is also not what I would expect.

I ended up buying 0.085 m³ (85 ℓ) of styrofoam weighing 1.11 kg for
AR$1599 (US$5.53).

The weight as such is not really an issue for me (I don’t think the
Cryoziggurat will be too heavy; in fact I might have to weigh it down
to keep it from being blown around by the air conditioner) but the
insulation value is.  So the crucial question to answer here is how
well the stuff insulates.  That would be maybe easier with a
thermometer.

The 25mm stuff is actually 23.5 mm, the 30mm stuff is actually 28.6,
the 20mm stuff is actually 18.7, and the 10mm stuff is actually 7.8.
These vary by about 200 microns, as the blade-cut bead surface is
pretty rough.

Using the large kitchen scale, the 30mm stuff actually weighs 435g,
the 20mm stuff 202g, the 10mm stuff 108g, and the 25mm stuff 238g
(with some chunks missing).

    | nominal |   nominal | nominal | real |   real |    real |      |         |
    |    mass | thickness | density | mass | thick- | density | mass | density |
    |     (g) |      (mm) | (kg/m³) |  (g) |   ness |         |    % |       % |
    |---------+-----------+---------+------+--------+---------+------+---------|
    |     120 |        10 |      12 |  108 |    7.8 |    13.8 | 90.0 |   115.0 |
    |     240 |        20 |      12 |  202 |   18.7 |    10.8 | 84.2 |    90.0 |
    |     300 |        25 |      12 |  238 |   23.5 |    10.1 | 79.3 |    84.2 |
    |     450 |        30 |      15 |  435 |   28.6 |    15.2 | 96.7 |   101.3 |
    #+TBLFM: $1=$3*$2::$6=$4/$5;%.1f::$7=$4*100/$1;%.1f::$8=$6*100/$3;%.1f

Hey!  Those motherfuckers at Easy cheated me out of 3.3–21.7% of my
fucking styrofoam!  (And the missing chunks definitely aren’t even as
large as 1%.)  I probably shouldn’t take the thickness measurements of
the thin sheets too seriously given how easily I could fudge them by
up to 10% by squeezing the calipers harder, but the mass measurement
is probably not off by more than 1%.  But actually the lower density
probably means I’m getting more insulance — maybe I shouldn’t complain
too much, but I also probably should assume that sometimes “12 kg/m³”
will be 15 or 9 kg/m³.

#### Styrofoam buckling tests ####

The 10mm stuff (now weighing in at 107 g) buckles at around 600 g
total reading, which means about 500 g gravity applied load.  The 20mm
stuff still weighs 202 g and with a buckling load on it the reading is
about 2100 g, which means about 1900 g gravity applied load,
conforming very nicely to the theoretical quadratic curve.  The 25mm
stuff buckles at a reading of about 3700 g, which (since it weighs
238g) is about 3500 g gravity applied load, a little higher than the
quadratic extrapolation from the 20mm data point, which would be
3.3 kg gravity.  (Possibly the thickness of the squared-off ends meant
that they weren’t as fully free to rotate as before.)  From this
buckling curve I ought to be able to compute the Young’s modulus of
the “12kg/m³” styrofoam to probably within about 10%.

Extrapolating to 30 mm we get a buckling load 4.7 kg gravity, which
(combined with the weight of the styrofoam itself) is too much for the
kitchen scale.  Unless I cut it in half, I guess.

[MercadoLibre has a listing by DNU Acoustics for a 12kg/m³ 1m×1m×20mm
sheet](https://articulo.mercadolibre.com.ar/MLA-1109655789-placa-plancha-telgopor-eps-1x1-m-20-mm-12-kg-_JM)
for AR$451.50, not including delivery.  This is 50% more expensive
than the AR$300 I just paid.  [At “high density” 30 kg/m³, 20 mm costs
AR$1174.74](https://articulo.mercadolibre.com.ar/MLA-1107970654-placa-de-telgopor-eps-1m-x-1m-x-20mm-x-30-kg-_JM)
or
[AR$1085.56](https://articulo.mercadolibre.com.ar/MLA-617568191-placa-telgopor-2-cm-alta-densidad-_JM)
and a brand-name [Polipor 12-kg/m³ 20-mm sheet costs
AR$550](https://articulo.mercadolibre.com.ar/MLA-774397597-plancha-de-telgopor-20-mm-densidad-standard-1-x1-m-oferta-_JM),
which is apparently “normal density”.

### 02022-11-09 ###

#### More styrofoam panels ####

I bought another styrofoam panel, this time at the “dry construction
materials” place down the street, which is a good bit closer than
Easy.  But this one is nominally 25 kg/m³ and 25 mm thick.  It’s
visibly much denser and smoother than the others, with a smaller grain
size.  It measures 23.3 mm, 22.8 mm, and 23.3 mm at different points,
and weighs 608 g.  I paid AR$1460 ≈ US$5.03 for it (the peso has lost
value since yesterday).

Adding it to the tables from yesterday, readjusting the exchange rate,
and adding a US$ (per square meter) column:

    | kg/m³ | mm | nominal kg |  AR$ |  US$ | AR$/m³ | AR$/kg | US$/m³ |
    |-------+----+------------+------+------+--------+--------+--------|
    |    12 | 10 |       0.12 |  159 | 0.55 |  15900 |   1325 |  54.83 |
    |    12 | 20 |       0.24 |  300 | 1.03 |  15000 |   1250 |  51.72 |
    |    12 | 25 |        0.3 |  390 | 1.34 |  15600 |   1300 |  53.79 |
    |    15 | 30 |       0.45 |  750 | 2.59 |  25000 |   1667 |  86.21 |
    |    25 | 25 |      0.625 | 1460 | 5.03 |  58400 |   2336 | 201.38 |
    #+TBLFM: $3=$1*$2/1000::$6=$4*1000/$2::$7=$4/$3;%.0f::$8=$6/290;%.2f::$5=$4/290;%.2f

    | nominal |   nominal | nominal | real |   real |    real |      |         |
    |    mass | thickness | density | mass | thick- | density | mass | density |
    |     (g) |      (mm) | (kg/m³) |  (g) |   ness |         |    % |       % |
    |---------+-----------+---------+------+--------+---------+------+---------|
    |     120 |        10 |      12 |  108 |    7.8 |    13.8 | 90.0 |   115.0 |
    |     240 |        20 |      12 |  202 |   18.7 |    10.8 | 84.2 |    90.0 |
    |     300 |        25 |      12 |  238 |   23.5 |    10.1 | 79.3 |    84.2 |
    |     450 |        30 |      15 |  435 |   28.6 |    15.2 | 96.7 |   101.3 |
    |     625 |        25 |      25 |  608 |   23.1 |    26.3 | 97.3 |   105.2 |
    #+TBLFM: $1=$3*$2::$6=$4/$5;%.1f::$7=$4*100/$1;%.1f::$8=$6*100/$3;%.1f

So it’s still lower than the nominal mass, but by less than the
others, and roughly twice as expensive per kg.

#### Adhesives ####

I also bought a liter of Sinteplast Recuplast Interior Mate Latex
paint, which claims 12–14 m² coverage per coat of paint.  The
objective here is to laminate stiffening material, such as paper or
fiberglass window screening material, onto the surface of the
styrofoam.  It cost me AR$1900 (US$6.55, ≈50¢/m²).  Unfortunately the
hardware store where I would normally buy window screen was closed.

Yesterday I also bought a roll of “Python” brand duct tape (an Akapol
brand, but imported from China), 48 mm × 9 m, for AR$819 (US$2.83,
US$6.56/m²).  This may work to hinge together panels or otherwise
connect them.

My father suggests silicone caulk may work well to laminate styrofoam
sheets together, maybe after moistening them with a sponge.

Beyond sticky tape, silicone, and latex paint, other adhesives that
might be worth trying include epoxy, plaster of Paris, portland
cement, waterglass, and quicklime; and other reinforcements that might
be worth trying (in addition to paper, fiberglass window screening,
and indoor-cultivation aluminized mylar) include aluminum foil,
burlap, *friselina* nonwoven polyester, cardboard, fiberglass cloth,
and masonite.  Quicklime, (acetic) silicone, and latex paint need
exposure to air to solidify, portland cement needs to be protected
from drying out, and AFAIK plaster of paris, epoxy, and sticky tape
work either way.

I’m thinking I can test the different adhesives (except Portland
cement) by cutting a bunch of styrofoam into smaller slabs,
overlapping their ends by a well-controlled amount, and gluing them
together across the overlap area, and then I can use a kitchen scale
to test how much force is needed to break the adhesive by flexing the
compound beam or by pulling on its ends.

#### More sandwich materials ####

I bought a roll of aluminum foil tonight at the supermarket as a
possible reinforcement material: 4 m × 28 cm for AR$400 (US$1.38).  8
layers measure 80 μm, 16 layers measure 160 μm, 32 layers measure
330 μm, 64 layers measure 660 μm, and a 27.5 mm × 16.8 mm piece of 64
layers weighs 560 mg, giving a nominal thickness per layer of 10 μm
and a density of 1.8 g/cc, well below aluminum’s nominal density of
2.7 g/cc.  This probably means the thickness is more like 7 μm and
wrinkling is trapping some air inside the folded aluminum-foil book.
I think I can more confidently say that this is about 19 g/m².

I also bought a nonwoven dish towel as a possible alternative
stiffening material for $100 (34¢).  It weighs 18.99 g and measures
370 mm × 400 mm, giving 130 g/m².

The dish towel’s 34¢ for 18.99 g and 370 mm × 400 mm gives
US$17.90/kg, or US$2.30/m².  It has a little stretchiness in one
planar dimension and almost no stretchiness in the other and is about
500μm–1mm thick depending on pressure.

This plastic can of latex paint I bought today weighs 1368 g.

See file `sandwich-material-pricing.md` for more notes on possible
reinforcement materials.

#### Styrofoam paint setup ####

Okay, stand back, I’m going to make an actual observation now.  I’m
going to:

- cut out six small pieces of styrofoam from the 25mm 12kg/m³ sheet,
- label them A–F,
- weigh them,
- open the paint,
- put drops of paint on pieces A–D of styrofoam with a disposable
  stick,
- weigh them again,
- put styrofoam down on top of two of the drops of paint (piece E on A
  and piece F on B),
- let the paint dry, and
- weigh them again

This will tell me if the paint adheres well to the styrofoam, if the
paint dries between two layers of styrofoam, and how much weight the
paint loses in drying (and thus how diluted it is).  The difference in
weight loss between the pairs of pieces that are treated the same
should give me some kind of indication of how bad my experimental
error is.

    | piece | weight 1 | weight 2 | painted | paintΔ |
    |-------+----------+----------+---------+--------|
    | A     | 220mg    | 200mg    | 470mg   | 260mg  |
    | B     | 300mg    | 290mg    | 590mg   | 295mg  |
    | C     | 280mg    | 240mg    | 890mg   | 630mg  |
    | D     | 290mg    | 280mg    | 740mg   | 455mg  |
    | E     | 280mg    | 270mg    | 280mg   | 5mg    |
    | F     | 250mg    | 260mg    | 270mg   | 15mg   |
    | G     |          |          | 130mg   |        |

The voice recorder on my phone was very helpful for weighing the
pieces: I was able to take 12 measurements (in column order) in a
minute and a half while reading the measurements out loud, then play
back the recording as I entered the data here.

That’s a rather alarming amount of measurement error on piece C, isn’t
it?  Like, ±8%!  Operator error?

I cut the pieces out with a bare box-cutter blade, which cut fairly
cleanly and with very little effort.  Some of them have some ragged
edges on them, which hopefully shouldn’t matter for this test.

I marked them (before weighing!) with a Sharpie permanent marker.
This does seem to have eaten away the styrofoam a bit.

I stirred up the paint for a while, with the objective of getting a
consistent mix, but I was using a popsicle stick that wasn’t long
enough to reach the bottom of the paint can, even though it was only
one liter.  I realize now that it would have been a good idea to add
additional paint to the pieces in reverse order to reduce the possible
effects of a shift over time of the paint mix being dripped.

In all I tested about 1.64 g of paint and probably used about 2 g.
I’m using 1.59 g of the styrofoam out of 238 g in that panel and so
it’s probably about 67 square centimeters (67 cm²).

Belatedly I realized I ought to have a control piece, one that wasn’t
going to have paint on it and which should therefore not change in
weight over the course of the observations, so I added piece G.

The “painted” weights for pieces E and F (and G) are the same because
they didn’t have any paint on them; I was just weighing them at the
same time as the painted pieces, the point being to ensure that the
scale was still producing reasonably consistent weights.

Inspired by the Sharpie, I broke off an additional piece from the same
slab and dripped some “nail varnish diluent” onto it (ethyl acetate,
butyl acetate, and isopropanol).  Three drops ate a vast cavity about
halfway through the (nominally 25mm) thickness; two drops from the
other side completed the tunnel, which is screened by a fibrous web of
polystyrene that looks like a micrograph of a blood clot.  On the
other hand, some ethanol from the spray bottle (nominally 96%) had no
visible effect.

After about an hour I did another weighing.  This was a disaster: a
small but unknowable amount of paint had dripped off the C and D
pieces onto the tray where I had set them, and in the process of
trying to set them back on the tray after weighing them, I lost more
of the paint.

    | piece | prev   | weight | second | Δ      |
    |       | weight |        | weight |        |
    |-------+--------+--------+--------+--------|
    | A+E   | 750mg  | 740mg  |        | -10mg  |
    | B+F   | 850mg  | 830mg  |        | -20mg  |
    | C     | 890mg  | 690mg  | 660mg  | -230mg |
    | D     | 740mg  | 650mg  |        | -90mg  |
    | G     | 130mg  | 140mg  |        | +10mg  |

So uh I guess in an hour not more than 10% of the paint dried in
between pieces A and E or B and F, and maybe none of it at all.  And
because I totally fucked up the pieces that were supposed to tell me
how much solvent the paint lost, I don’t have any way to know whether
the solvent content is 1%, 10%, 30%, 60%, or 90% of the paint.  Also I
got paint on the weird rubbery clay body I had made with CMC, though
probably less than 150 mg, so it won’t affect its weight too much.

Hopefully in the morning I can attempt this again with less than than
40 mg of paint per square centimeter of styrofoam; the 57 and 41 on C
and D respectively were evidently way too much.  But the bigger
problem was that then I tried to solve the problem by standing them on
end, and that resulted in the heavy paint side falling face down on
other random things and losing paint onto them.  (Even in the more
lightly painted cases of A and B, the paint weighed as much as the
styrofoam or more).  All this was in part because I cut the original
pieces much smaller than I should have because I wanted them to easily
fit onto the small scale.

The latex paint does seem to be wetting the styrofoam nicely and
doesn’t seem to be eating it, so I should be able to test peeling it
off tomorrow if it’s dry and see how good its adhesion was, even if
measuring the mass loss is a lost cause.

### 02022-11-10 ###

#### Styrofoam paint preliminary results ####

The latex paint on C and D looks quite dry, with a very visible loss
of most of its volume, so I am guessing that it probably also lost
most of its mass.  The paint drips that dripped off onto the tray
(also styrofoam, but extruded or rather molded from liquid) are also
fairly dry, but not totally inflexible; with some difficulty I am able
to peel most of it off the tray with no visible damage to the tray.

This strongly suggests that the paint’s adhesion to styrofoam is
weaker than the styrofoam’s internal cohesion, even though the
paint/tray interface has presumably no porosity.  This in turn
suggests that the styrofoam/paint bond won’t be limited by the
strength of the styrofoam.

Block C was stuck to the tray rather firmly by way of one of the
drops, though, so in absolute rather than relative terms I feel like
the paint bond is probably going to be pretty useful.

Having been able to peel almost all of the the paint drips off the
tray means that I can probably recover from my disastrous mis-weighing
last night.

#### Hardware store trip, misc. ####

So my plan right now is to go and try to buy some window screen,
portland cement, acetic silicone, spray adhesive, and doublestick tape
at the hardware store, then come back and weigh the various styrofoam
bits I put drops of paint on 13 hours ago.

Okay, the hardware store had window screen (but not the fiberglass
type: PVC, AR$1000 for two meters by 80 cm, US$2.16/m²) and gray
portland cement (AR$160 for a 1-kg bag, 55¢).  But they didn't have
non-foam doublestick tape or spray adhesive, and I forgot the
silicone.  I also lost my facemask in the process, so I guess I wasn’t
very mentally functional.

It’s only 24° but it’s fucking hot because the humidity is terrible.
Even normal people are complaining.

Mina reports that Espadol brand wet wipes are especially good for
hygienic purposes.

I was hoping that the window screen would actually be plastic over
fiberglass, but melting it with a flame reveals that no, it’s just PVC
(with additives), just as advertised.  So I guess I need to get
fiberglass from elsewhere.  The roll of probably pretty useless window
screen weighs 178 g (110 g/m², US$19/kg).

Portland cement and quicklime are not very friendly to fiberglass
reinforcement, but latex paint and plaster of Paris should be fine.

#### More styrofoam paint results ####

Weighing the bits of painted styrofoam:

    |     | wt1   | wt2   | wt3   | wet   | prev  | Δ₁     | Δ₂     | paint | %  |
    |-----+-------+-------+-------+-------+-------+--------+--------+-------+----|
    | C   | 500mg |       | 470mg | 890mg | 660mg | -420mg | -190mg | 630mg | 67 |
    | D   | 460mg | 490mg | 450mg | 740mg | 650mg | -290mg | -200mg | 455mg | 64 |
    | A+E |       |       | 580mg | 750mg | 740mg | -170mg | -160mg | 260mg | 65 |
    | B+F |       |       | 690mg | 850mg | 830mg | -160mg | -140mg | 295mg | 54 |
    | G   |       |       | 160mg | 130mg | 140mg | +30mg  | +20mg  | 0     | ∞  |

As I was weighing D the scale kind of went crazy, and so after it
settled down I weighed C again, and then the others, thus “wt1”,
“wt2”, “wt3”.  I’m going to use the third column here.  “wet” is the
weight immediately after applying the paint (which in the case of G
didn’t involve actually being wet), and “prev” is the weight from the
final weighing last night.  “Δ₁” is the change from immediately after
applying the paint, which should be a reasonably good estimate of the
amount of solvent that has evaporated (except for C), and “Δ₂” is the
change from the last weighing last night, which in some cases is
missing the paint that dripped off.  “paint” is the amount of paint
copied from the earlier table, and “%” is the percentage of weight the
paint seems to have lost by evaporation (and spilling), dividing Δ₁ by
paint.

A tentative interpretation of all this is that the paint is pretty
much dry (after 16 or 17 hours of exposure to air at around 24° and
rainy-day humidity), and was about 65% solvent by weight, which seems
like a lot, and that A+E has mostly dried through the styrofoam but
B+F hasn’t.  But 65% of the original 295mg of paint on piece B would
be 192mg, which is only 30mg more than B+F is measured to have lost,
so that’s within measurement error.  It might be dry too!

C has lost a little more weight than the other three pieces
percentagewise, which might be measurement error, actual paint loss,
or a combination.

The paint being able to dry through the 25 mm of 12 kg/m³ styrofoam in
less than a day is a very promising sign, because it suggests that the
styrofoam is permeable enough that *any* of the candidate adhesives
would work between styrofoam layers (except maybe spray adhesive,
which might eat the styrofoam).  It’s unfortunate that I didn’t manage
to weigh it earlier; I’m guessing that the difference in drying rates
between the exposed paint and the covered paint is significant, and
the fact that they’ve mostly converged probably means that the
between-layers paint is mostly dry and the exposed paint is super dry.

If I had used something like 10 cm² per piece instead of 1.1 cm², I
could have put more paint on them.  The small scale’s stage is about
55 mm × 50 mm, so I could probably have used 50 cm² per piece or more
without obscuring the scale’s readings, and probably placed 2 g of
paint on each one.  This would cause the scale’s ±30 mg repeatability
failures to amount to ±1.5% instead of ±10%, the stabler pieces would
be less likely to flip over, and the smaller amount of paint would be
less likely to drip.

I broke the expensive panel (25 kg/m²) in half trying to fan Mina with
it.

#### Thoughts on solvent-welding styrofoam ####

I dripped a bit more of the nail-polish diluent on a piece of it; it
made craters, just like in the lower-density stuff, but a bit slower.
Also I managed to glue my thumb to it in the process.  It occurs to me
that maybe by diluting whatever the PS-dissolving diluent component is
(ethyl acetate probably) in a much larger quantity (10×, 100×, 1000×)
of something inert like ethanol, I might be able to spray it over a
wide area and make the wide area slightly gummy, then press it against
another piece of styrofoam to get them to glue together.  Once the
solvent diffused out of the area the joint would be permanent.  [The
stuff costs US$30 a liter as nail polish
diluent](https://articulo.mercadolibre.com.ar/MLA-1101566019-diluyente-de-esmalte-unas-las-varano-profesional-60ml-_JM).

Nathan Laredo suggests using acetone vapor, but I can’t do that
because acetone is classified as a “drug precursor”.

#### Coffee ####

I replaced the two AA cells in the large scale because it was refusing
to weigh the window screen.

I’m going to make Mina a coffee; planned:

- 5 g coffee
- 20 g milk
- 20 drops vanilla (1 g)
- 10 drops sweetener (0.5 g)
- 175 g tap water, of which:
    - 50 g cold
    - 125 g boiling

Actual results:

The metal bowl weighed 27.22 g.  I added 4.99 g of the coffee, 20.25 g
dried milk (San Regim), 10 drops of the sweetener (0.88 g), and 20
drops vanilla (1.04 g).  The bowl with all these ingredients tared to
-54.34 g.  The black coffee cup weighed 248 g, and I added the bowl
full of powders and small amounts of liquids to it, but some of the
liquid stuck to the wall, and I spilled a little on the scale, 25 g.
After scraping the glob of wet crap off the bowl wall and into the cup
with the fork, the large scale read 27 g of contents in the black
coffee cup.  The metal bowl with its powdery residues weighed 27.51 g,
and so I added 56 g of water to it from the pink cup (which is poorly
suited for pouring), then poured water back into the pink cup to
reduce the weight to 50.71 g.  Transferring to the large scale, this
registered as 51 g, and then I burned my hand on the boiling pot,
added 18 g of boiling water directly to the unmixed ingredients,
realized I needed to stir it, stirred it with the fork (which weighs
21 g), and increased the amount of boiling water to 126 g.  I stirred
it inadequately, tared without the fork, and upon removing from the
scale it read -450 g.

Mina reports that there were like undissolved bits of coffee floating
in it, but that it was super creamy and flavorful, with a very strong
coffee flavor and the precise sweetness, and that it was very hot.

The bowl of ingredients *should have* weighed (+ 27.22 4.99 20.25 0.88
1.04) = 54.38 g, but it measured 54.34 g, so there was a 400-mg
measuring error there.  The bowl regularly measures between 27.21 g
and 27.26 g, so this was within the range.  The coffee cup measured
248 g as usual.  The mix in the bowl should have weighed 27.12 g, so
27 g upon transferring is good.  Weighing 27.51 g suggests that there
were between 250 and 300 mg left in the bowl, which wouldn't be so bad
except that it's probably mostly vanilla and sweetener stuck to the
wall; I should have covered them up with powder to keep them from
sticking.  The final tare weight of the coffee cup should have been (+
248 27 51 126) = 452 g, which is 2 g higher than what it actually
registered, suggesting ½% measurement drift during the 9' that the
coffee preparation took.

The bowl, having been rinsed with the cold water, emptied into the
cup, and left to dry for several minutes, now weighs 27.80 g, so it
probably has about 480 mg of residues in it, mostly water.  Also I
dumped about 8% of the water back into the pink mug, which probably
took something like that fraction of the 300 mg remaining (24 mg) with
it back into the cup.  And another ≈1% of the water and residues
evidently remained even after I rinsed it, probably largely due to the
vanilla and stuff sticking to the wall.

So the final coffee might have been missing as much as 15% of its
intended vanilla and sweetener.  The vanilla and sweetener diffused
into one another as I added the vanilla, so probably whatever part of
those 300 mg was the liquids, it was pretty evenly distributed among
them.

So, the mix I ended up with was:

- 4.99 g adulterated coffee (±6%)
- 20.25 g dried milk (±1.5%)
- 0.88 g Hileret (10 drops) (±20%)
- 1.04 g fake vanilla (20 drops) (±15%)
- 177 g water, of which:
    - 50.71 g cold water (±1%)
    - 126 g boiling water (±1%)
- inadequately stirred

There were no nasty little scalded milk floaties apparent until the
bottom of the coffee, but I guess pouring the boiling water in before
mixing did take its toll.

Making the coffee took 9 minutes; writing these notes took 24 minutes.

I’ve bought a box of powdered Hileret packets, with the idea of
premixing sweetener, milk, and coffee, thus reducing the coffee recipe
from six ingredients to four.

#### More styrofoam paint results: a viable adhesive ####

All weights in mg.

    | block | wt1 | wt2 | wet | last | wetΔ | lastΔ | paint |     % |
    |-------+-----+-----+-----+------+------+-------+-------+-------|
    | A+E   | 580 | 570 | 750 |  580 | -180 |   -10 |   260 |    69 |
    | B+F   | 670 | 650 | 850 |  690 | -200 |   -40 |   295 |    68 |
    | C     | 490 | 480 | 890 |  470 | -410 |    10 |   630 |    65 |
    | D     | 490 | 480 | 740 |  450 | -260 |    30 |   455 |    57 |
    | G     | 180 | 150 | 130 |  160 |   20 |   -10 |     0 | -2000 |
    #+TBLFM: $6=$3-$4::$7=$3-$5::$9=100*-$6/$8;%.0f

The scale seems to have drifted significantly during the 2 minutes I
was doing the weighing; after the end of the “wt1” column it read 30mg
on an empty tray.  I’m using the “wt2” column.  “last” is the “wt3”
column from about 6 hours ago; “wetΔ” is the change from “wet” to
“wt2”; “lastΔ” is the change from “last” to “wt2”; “paint” is the
paint mass copied from the table earlier today; and “%” is the
percentage of the paint mass that seems to have evaporated (or
otherwise been lost).  On average it seems like the weight is
unchanged from 6 hours ago, so I guess the paint was already dry then.
The average seems to be about 65%, but we can say 54%–69%.  Six of the
eight data points were in the 65%–68% interval.

It’s possible to peel the latex paint off the styrofoam, but it tears,
and it takes a thin layer of styrofoam with it.  Peeling two blocks
apart is relatively straightforward and reveals that contact was made
only over part of the surface, presumably because of the irregularity
of the surfaces and not being clamped.  *Pulling* the other two blocks
apart, rather than peeling, took substantial effort for me and ended
up fracturing one of the blocks through the middle.

I think this shows that this latex paint, even undiluted and
unclamped, is viable as an adhesive for laminating this styrofoam, but
it would be probably a factor of 10 stronger if clamped.

So that means the AR$1900 (≈US$6.55) per liter (≈1.2 kg) buys you
about 400 g of latex, the rest being water, so it’s about US$16/kg of
latex adhesive.  If we figure that we need about 40mg/cm² of the
original paint, that would be 400g/m², four times the recommended
thickness as paint; 13mg of dried paint per cm², which would be a
130μm if the latex were 1 g/cc, which probably isn’t too far off for
gummy organic polymers.  Conceivably a thinner layer would work,
especially since in this use the little latex spheres in the emulsion
don’t have to bond to each other to work; they can bond directly to
the styrofoam even if they’re spread somewhat far apart.

400g/m² would be US$2.62/m², which is significantly more expensive
than the styrofoam itself, so if the paint can’t be either greatly
diluted or more thinly spread (like 10×) and still work as glue, it
would be nice to find an alternative.  I’m optimistic that it can be,
because the recommended coverage for use *as paint* is 12–14 m² per
liter, 400g/m² is only 3 m² per liter, and I think that for use as
adhesive it probably doesn’t need to make as thick a layer as paint.

### 02022-11-11 ###

Happy Armistice Day.

#### Dammar varnish ####

Mina has a bottle of “EQArte Barniz Damar Oleo”, a cloudy yellowish
liquid that smells of turpentine marketed as a protective clear
coating for oil paintings.  A drop of this on the (25kg/m³) styrofoam
soaks in; after several minutes it has made the styrofoam mushy.  The
mushy styrofoam is somewhat sticky when dug out of the still-hard
styrofoam around it, but the surface where the drop fell is not.  I
suspect this is [dammar varnish][dvwp], which Wikipedia says is a
resin called “dammar” (from *Shorea hypochra*, the white meranti;
*Shorea robusta*, the sacred sal tree mentioned in the Ramayana; or
*Vateria indica*, the white dammar) dissolved in turpentine.

[dvwp]: https://en.wikipedia.org/wiki/Dammar_gum

To find out how much of the dammar varnish is volatile solvent, at
22:27, I made a little weighing boat of aluminum foil, put it on top
of a piece of styrofoam, and weighed them at 2.58–2.60 grams.  Then I
added enough dammar varnish to get to 2.87 g, meaning about 280 mg.
After a few minutes it was down to 2.84 g, which... might just be
drift on that fucking scale, who knows.  It’s 26° in here so it should
evaporate reasonably fast.

Before putting stuff in the boat I turned off the air conditioner to
reduce the risk that the boat would blow away and spill 280 mg of
dammar everywhere.

I tried lighting a couple of small pieces of styrofoam (both nominally
12kg/m³ and 25kg/m³) with a stove lighter and a butane blowtorch, and
although I could easily burn them with either flame, the flames were
self-extinguishing.  This was not at all what I expected; I thought
they would burn almost explosively.  It’s somewhat reassuring from a
fire-safety perspective that that isn’t the case.

Half an hour after putting the dammar varnish in the boat, it weighs
2.82 g, which is 210 mg more than before adding it.  This suggests
it’s drying out more slowly now, maybe.  Then 2.79 g 56 minutes after
putting the varnish in.

I guess I’ll let it sit overnight.  I don’t think the house will heat
up much overnight even without the air conditioner, though the
thermometer is up to 28° now.  I’ll just put the fan on me in bed.

### 02022-11-12 ###

#### The dammar varnish wasn’t ####

It’s been about 12 hours, and now the weighing boat weighs 2.70 g,
which is like 110 mg more than before adding the varnish and 170 mg
less than immediately afterwards, so the varnish was, by weight,
something like 60% turpentine and 40% resin.  Also it’s 26.5° so I am
turning the air conditioner back on.

Mina tells me that the dammar varnish is mixed with some linseed oil
(presumably with metallic activators).

It turns out that the varnish did leak out of the aluminum-foil boat
and soak into the styrofoam (and almost ran off of it), eating a
substantial crater into it.  I guess the aluminum foil wasn’t that
impermeable.  Good thing I put it on styrofoam!

Four hours later it weighs 2.73 g.  Maybe it’s gained 30 mg by me
touching it with my grubby fingers but I think more likely it weighs
something more like 2.715 g and this is just measurement error.  At
any rate it’s probably not still evaporating.

Another three hours later it’s 2.71 g.

#### Coffee ####

I made Mina coffee again.  Today I used half as much vanilla as
previously — because I forgot!

- 5.12 g adulterated coffee (initially weighed at 5.10)
- 20.50 g milk
- 10 drops vanilla (0.49 g, initially weighed at 0.48)
- 10 drops sweetener (0.85 g, briefly weighing 0.87)
- tap water:
    - 53.91 g cold (oops)
    - 126 g boiling

The steel bowl initially weighed 27.32 g.  The black coffee cup
weighed 247 g initially, 1 g lower than usual.  The bowl with the
first four ingredients was 54.24 g, 40 mg lower than it should have
been; 27 g ingredients were registered in the coffee cup on the large
scale, -26.89 on the small scale.  After transferring the large scale
registered 81 g (27 + 54).  I stirred the mix cold with a 28-gram
spoon, spilling a little, before adding the cold water and then again
after adding the hot, for a total weight of 481 g (247 g cup + 27 g
initial ingredients + 51 g cold water + 126 g hot water + 28 g spoon
gives 479).

Mina reports that the coffee is strong, very tasty, with lots of foam
and the ideal sweetness and temperature.

After serving, the bowl and spoon weigh 55.46 g, of which 27.74 is the
wet bowl (420 mg residues, mostly water).  I unfortunately forgot to
weigh the spoon dry, so I don’t have a very good picture of how much
coffee was left stuck to the spoon, but it can’t have been more than
about a gram.  So that’s less than a 1% error.

The worst error (aside from the vanilla) was in the cold water, where
I added 8% more than intended.  The other errors were generally 2.5%
or less.

The process of making the coffee took only 7' this time but writing
this up still took me more than 20'.

### 02022-11-14 ###

Did nothing yesterday.

#### Dammar varnish final death ####

The boat and styrofoam with the dammar varnish still weighs 270mg, so
I guess it really was pretty much dry after 12 hours.  But upon
unfolding the aluminum-foil boat, it still smells quite a bit like
turpentine, so the turpentine wasn’t completely gone from between the
layers of foil even though it’s been three days.

#### Tea tree oil eats styrofoam ####

Bought 10cc of tea tree oil at a health food store for $1100 (US$3.76
at AR$292/US$).  Dripped a few drops on styrofoam (the styrofoam from
the dammar varnish test).  It soaked in in a few seconds, and after a
minute or two left huge craters in the styrofoam.  Seems maybe slower
but at last at least as aggressive as the ethyl acetate.

It in fact continued eating its way through over the next hour or so
and nearly reached the other side of the styrofoam.  After another
half hour it left only a translucent paper-thin layer on the other
side.

#### Hydrating some silica gel ####

Bought 3.8 ℓ of CanCat brand silica gel cat litter at petcrows.com.ar
for $2700, US$9.25.  It contains some cobalt-colored indicator weighs
1603 g, just as vendors on MercadoLibre said it would, but the price
was a bit lower than from them, and also lower than industrial
desiccant silica gel.  This is US$5.78/kg.  I want to find out how
much water it absorbs and what its specific heat is when fully
hydrated; I suspect it should be upwards of 3 J/g/K, which far exceeds
most solid substances.

I put the usual metal bowl onto the small scale (27.22 g) and added
67.16 g of the silica gel and 119.66 g tap water.  A fine mist of
water arose, but the water felt only slightly warm through the bowl.
Later, many tiny bubbles arose, presumably from air trapped inside the
silica gel, which died away to just the occasional bubble over the
next hour or so.  The cobalt-blue indicator stones are not yet visibly
changing color, so the hydration process is evidently kind of slow.

[WP](https://en.wikipedia.org/wiki/Silica_gel) says it should be able
to absorb 37% of its weight in water.  [WP also
says](https://en.wikipedia.org/wiki/Fused_quartz) the specific heat of
amorphous silica is 45.3 J/mol/K, which should be 0.754 J/g/K.  27%
4.184 J/mol/K and 73% 0.754 J/g/K predicts a specific heat of
1.68 J/g/K.  But the reality is probably lower.

Epsom salt seems like another appealing solid with high water content,
but [apparently its heat capacity around room temperature][eshc] is
about 375 J/mol/K, which works out to only about 1.52 J/g/K.  I guess
immobilizing all those water molecules in the crystal structure really
reduces their degrees of freedom a lot and therefore their specific
heat.

[eshc]: https://www.researchgate.net/publication/249528974_Heat_capacity_and_thermodynamic_functions_of_epsomite_MgSO_4_7_H_2_O_at_0-303_K

I put a plastic shopping bag over the bowl of water and silica gel to
keep dust from falling in and reduce evaporation.  If I manage not to
spill it I’ll weigh it tomorrow, filter off the water, and weigh it
again.  The weight gain in the silica gel will tell me how much water
it picked up.  Then I’ll probably add water again and see if it soaks
up more over the next day.

#### Orange oil eats styrofoam ####

I squeezed a little bit of peel from a bitter orange that fell in the
street through a lighter flame, producing the usual gouts of flame.

I squeezed a little more onto some styrofoam, squirting a tiny amount
of essential oil onto it.  The surface became rough and pitted, losing
its shininess, showing that the orange oil was dissolving the
styrofoam; I think I would have gotten a crater if I had entire drops
of orange oil to work with.

### 02022-11-17 ###

A Monster can weighs 15.75 g.

The bowl of silica gel weighs 190.51 g, including a little water that
is still liquid.  This suggests that about 24 g of water has
evaporated.  The indicator stones are still blue, so I added water to
get to 244 g (on the large scale; the small scale is overloaded).

02023-07
--------

It turns out that the indicator stones are fake; over time the blue
dye diffused out to nearby stones through the water, but it never
turned pink.

The filter full of green verditer weighs 8.33 g.  An empty filter
weighs 1.93 g.  So I probably made 6.40 g of green verditer.

