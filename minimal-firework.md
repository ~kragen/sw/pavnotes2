Suppose you want to launch a transonic firework, say at mach 0.9,
which will explode with a 100-joule bang; and you want it to be
guided, so that it can produce its bang at a precisely timed place and
time.  Mach 0.9 is 300 m/s and 45 kJ/kg.

Fuel for a traditional rocket engine, where the oxidizer is mixed into
the fuel rather than gathered from the environment, will have energy
density close to 5 MJ/kg, rather than the 45 or so available from
things like gasoline.

This means that with a 100%-efficient rocket motor and no air
resistance, your firework could be only about 0.9% rocket fuel.
Practical rocket engines are under 60% efficient, typically more than
50%, so you might end up needing 2% rocket fuel.

The bang fuel also has energy density close to 5 MJ/kg, so 100 J is
20 mg.  The additional 2% in fuel brings the minimal mass to 20.4 mg.
But such a tiny rocket would probably be doomed by overwhelming air
resistance, so that is probably the limiting factor.

