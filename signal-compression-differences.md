LZ77 and the like don’t do very well at compressing data that doesn’t
repeat.  So, if you’re compressing image data, a gradient consisting
of colors that haven’t occurred before is kind of incompressible.
Here’s a sequence of RGB values:

    20 168 255 28 158 251 37 147 246 46 136 242 54 126 237
    62 116 233 71 105 229 80  94 224 88  84 220 96  74 215

But that’s just a gradient.  The [first backward differences][0] (of
corresponding colors of) a linear gradient are quite repetitive; here
I’ve wrapped them to 8 bits:

    20 168 255 8 246 252 9 245 251 9 245 252 8 246 251
     8 246 252 9 245 252 9 245 251 8 246 252 8 246 251

[0]: https://en.wikipedia.org/wiki/Finite_difference#Basic_types

This byte sequence is evidently easily compressed by algorithms like
LZW or LZ77; the first three bytes give the starting color, and the
following bytes give the color change from the previous pixel.

I think [Paeth-predictor][1] residuals typically give the same result
in a case like this, because the Paeth predictor is constrained to
predict that the new pixel will be exactly equal to one of the three
context pixels.

[1]: https://en.wikipedia.org/wiki/PNG#Filtering

If we look at its second differences rather than its first
differences, they are in some sense even more compressible:

     20 168 255 244  78 253   1 255 255   0   0   1 255   1 255
      0   0   1   1 255   0   0   0 255 255   1   1   0   0 255

Here everything after the initial setup sequence (which describes the
initial color and the gradient gradient) is just 0, 1, or -1, which
appear in an apparently nearly-random pattern.  Since those three
values are likely to have occurred frequently before, including in
those particular combinations, they’ll probably be very compressible
indeed.

Results from a minimal experiment
---------------------------------

I made up a 100-pixel version of this gradient and wrote it to a raw
300-byte file.  ncompress “compressed” that file to 336 bytes.  The
first differences compressed to 91 bytes.  The second differences,
contrary to expectations, compressed to 106 bytes.  The third
differences compressed to 129 bytes.  (gzip -9 was mostly a little
better but qualitatively similar, but actually did worse on the
higher-order differences.)  These were the third differences (as
before, of corresponding pixel components):

     20 168 255 217 175   2  19 169 254   0   0   2 255   0 254
      2   0   2 255   0 254   0   0   2   0   0 254   0   0   2
    255   0 255   2 255 255 255   2   2   0 255 254   0   0   2
      0   0 254   0   0   2 255   0 255   2   0 255 255   0   2
      0   0 254   0   0   2   0   0 254   0   0   2 255   0 254
      2   0   2 255   0 255   0   0 255   0   0   2   0   0 254
      0 255   2 255   2 254   2 255   2 255   0 254   0   0   2
      0   0 255   0   0 255 255   0   2   2   0 254 255   0   2
      0   0 254   0   0   2   0   0 255   0   0 255 255   0   2
      2   0 254 255   0   2   0   0 254   0   0   2   0   0 254
    255   0   2   2 255 255 255   2 255   0 255   2   0   0 254
      0   0   2   0   0 254 255   0   2   2   0 254 255   0   2
      0   0 255   0   0 255   0   0   2   0   0 254 255   0   2
      2   0 254 255   0   2   0   0 255   0   0 255   0   0   2
      0 255 254 255   2   2   2 255 254 255   0   2   0   0 254
      0   0   2   0   0 255 255   0 255   2   0   2 255   0 254
      0   0   2   0   0 254   0   0   2   0   0 254 255   0   2
      2   0 255 255   0 255   0   0   2   0   0 254   0   0   2
    255   0 254   2 255   2 255   2 255   0 255 255   0   0   2
      0   0 254   0   0   2 255   0 254   2   0   2 255   0 254

(I can’t compare to the Paeth predictor here because this is just a
one-dimensional pixel array.)

This is somewhat less compressible.  Considered as an LTI filter, the
impulse response of first differences is 1 -1.  The impulse response
of second differences is 1 -2 1.  The impulse response of third
differences is 1 -3 3 -1.  So we see a lot of +2 and -2 here, as well
as 0 and -1, and interestingly no +1s at all.  But it's still far
better than the original 336 bytes.

Repeating sequences
-------------------

The first differences of a repeating sequence are also repeating,
although the repetition is one item shorter.  Consider this sequence,
which repeats five times:

    3 4 5 10 20 30 3 4 5 10 20 30 3 4 5 10 20 30
    3 4 5 10 20 30 3 4 5 10 20 30

Its first differences are:

      3   4   5 7 16 25 249 240 231 7 16 25 249 240 231 7 16 25
    249 240 231 7 16 25 249 240 231 7 16 25

These, too, repeat, but only four and a half times.  Going to second
differences reduces the repetitions to four:

      3   4   5   4  12  20 242 224 206  14  32  50 242 224 206  14  32  50
    242 224 206  14  32  50 242 224 206  14  32  50

Different strides
-----------------

A sort of *deus ex machina* in the above is that the 
