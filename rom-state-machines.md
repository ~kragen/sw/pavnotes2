I was just looking at [Jeff Laughton’s CMOS one-bit computer][0],
which has two addressable bits of data memory, five output lines, six
input lines, and an eight-bit program counter which cycles through
ROM.  [Rudiger Mohr also built an implementation, for some reason in
TTL][1].  The design is largely just the standard EPROM-driven
finite-state-machine where the output data lines get latched into the
address register on each clock cycle.

[0]: https://laughtonelectronics.com/Arcana/One-bit%20computer/One-bit%20computer.html
[1]: https://github.com/ruenahcmohr/OneBit

However, it uses an ingenious trick: the I/O lines are routed through
a second register which is clocked with the opposite polarity, and the
clock line is wired up to one of the address bits.  This in effect
multiplexes the ROM data output lines between I/O bits and
state-transition bits.  The I/O lines are also wired up through
mux/demux chips, with a latch on the output, with only one of (each
of) them accessed on each clock cycle.  Laughton’s two addressable
bits of memory are wires connected from the output addresable latch to
the input mux.

This provides a surprisingly interesting device in only six chips, all
SSI except the ROM; in Laughton’s version, a 2716, two 74C374s, a
CD4051, a CD4099, and a Schmitt inverter chip; and in Mohr’s version,
a 28C64 (an EEPROM version of the 27C64), two 74(LS?)574s, a 74LS151,
a 74LS259, and a 74(LS?)04.  (There are a couple more unidentified
chips in the breadboard photo, maybe a 555 and some kind of driver for
the LEDs.)

This alternates between clocking in an I/O control byte into one of
the registers, on one clock edge, and clocking in a new address into
the program counter on the other edge.  The I/O control byte has an
unused bit, a data bit to output, three bits of output address, and
three bits of input address; the input-address bits aren’t registered,
but rather directly connected to the input mux, whose *output* bit is
registered (with the register connected to a ROM address line).  So
only five bits of the I/O register are used, or six in Laughton’s
design, which uses a bit to register the reset line.  Laughton also
uses *half* of the 2716’s contents to *process* the reset line, while
Mohr simply disables the program-counter register and pulls all the
address bits low.

You can get [a Winbond W27C512-45Z 512-kibibit EEPROM][2] for US$3.53
(from Tecnoliveusa) which is closely analogous to the 2716, just with
16 address lines instead of 11, and [the datasheet says it responds in
45ns][3] and can therefore be clocked up to 20MHz, so there isn’t a
strong reason to use a small ROM.  Digi-Key lists it as “obsolete” but
[things like the US$7 120-ns OTP 4-mebibit AT27LV040A-12VI][4], with
the same asynchronous interface, as “active”.

[2]: https://articulo.mercadolibre.com.ar/MLA-916916182-memoria-eeprom-w27c512-w27c512-45z-27c512-_JM
[3]: https://mm.digikey.com/Volume0/opasdata/d220001/medias/docus/900/W27C512.pdf
[4]: https://www.microchip.com/en-us/product/AT27LV040A

It occurred to me that you could take the same multiplexing approach
to go quite a bit farther with this, maybe even without adding any
more chips.

I think you can drive the write-enable pin 14 of the 74LS259 from a
registered bit of the I/O instruction word, instead of from the clock
as Mohr does; this would enable you to use the same three lines for
input addresses and output addresses, because you simply disable the
latching of an output bit when you want to read input (unless you
happen to want to read the corresponding input bit).  This also lets
you use the data-output bit as an input address bit, giving you 4 bits
of input address.  Instead of having one unused bit in the I/O control
word, then, you have three, plus one bit to select between input and
output, one bit of output data or input address, and three bits of
output address or input address.

If you register two or all three of the unused bits, you can use them
to drive more address lines on the ROM.  You could say that these are
the “bank select” bits, changing at a different point in the
instruction cycle, which sort of complicates the layout of
instructions.  But this enables you to write programs of 1024 or 2048
instructions instead of 256.

There are bigger register chips available; the [CY74FCT16823CTPVC][5]
is an obsolete 18-bit 10-nanosecond 56-pin part that Digi-Key lists at
US$3.59, and they list several similar parts under “flip-flops”.  But
it’s not obvious to me that this is better in any way than just using
more 8-bit register chips.

[5]: https://www.digikey.com/en/products/detail/texas-instruments/CY74FCT16823CTPVC/1508442

Unfortunately, it’s not clear to me that in 02023 (as opposed to, say,
01993) the overall design is better at anything than using a small
FPGA or microcontroller.  It probably costs about US$15 to build, and
Mohr reports only being able to run it at 4 MHz.  You could probably
run it a 20 MHz on a protoboard with proper decoupling, but I think
you can’t get async parallel EEPROMs that go much faster than that;
there are lots of 133MHz Flash chips, but you can’t use them to
replace combinational logic like this.  And an [iCE40-LP8K FPGA][6]
will run you US$12, with 131072 bits of block RAM (in 32 blocks) and
7680 4-LUTs and 93 GPIOs, is 1.5 millimeters square, supports
differential I/O, runs on 250 μA when idle, and can run a 16-bit
counter at 175 MHz.

[6]: https://www.digikey.com/en/products/detail/lattice-semiconductor-corporation/ICE40LP8K-CM121/3083584

If you could get an EEPROM with LVDS output that ran at 500 MHz or
something, that would be a different story.
