What if you changed Forth to use the COMFY control flow primitives?  I
think it might get simpler.

How Forth control flow words work
---------------------------------

The Forth compiler keeps control-flow information on the operand stack
at compile-time in order to properly compile nested control
structures.  This is somewhat standardized in ANS Forth and the Forth
2012 standard, I think in order that you can write portable words that
interoperate with the standard control structures.  As with languages
like C and Lisp, Forth control-flow constructs consume a Boolean value
computed by some arbitrary expression, on the operand stack in the
Forth case.

### Indefinite looping ###

The standard `while` loop we know from other languages is called
`begin while repeat` in Forth.  [The definition of `begin`][0]
documents its compile-time stack effect as follows:

> Compilation: ( C: -- *dest* )
>
> Put the next location for a transfer of control, *dest*, onto the
> control flow stack. Append the run-time semantics given below to the
> current definition.

[0]: https://forth-standard.org/standard/core/BEGIN

The stack comment with the “--” means that on the control-flow stack
(“C:”) it consumes nothing and leaves one item on the stack, called
*dest*.

The [control flow stack is documented][1] in part as follows:

> The elements of the control-flow stack are system-compilation data
> types.
>
> The control-flow stack may, but need not, physically exist in an
> implementation. If it does exist, it may be, but need not be,
> implemented using the data stack. The format of the control-flow
> stack is implementation defined.

[1]: https://forth-standard.org/standard/usage#usage:controlstack

[The definition of `while`][2] says about its compile-time stack
effect:

> Compilation: ( C: *dest* -- *orig* *dest* )
>
> Put the location of a new unresolved forward reference *orig* onto
> the control flow stack, under the existing *dest*.  Append the
> run-time semantics given below to the current definition. The
> semantics are incomplete until *orig* and *dest* are resolved (e.g.,
> by REPEAT).

[2]: https://forth-standard.org/standard/core/WHILE

The run-time semantics are to pop a boolean and jump if it’s false.
And its unresolved forward reference is backpatched by [`repeat`][3]
so `while` can jump out of the loop:

> Compilation: ( C: *orig* *dest* -- )
>
> Append the run-time semantics given below to the current definition,
> resolving the backward reference *dest*. Resolve the forward
> reference *orig* using the location following the appended run-time
> semantics.

[3]: https://forth-standard.org/standard/core/REPEAT

`Repeat`’s run-time semantics are to jump to *dest*, which, remember,
was pushed on the stack by `begin`.

These aren’t the only indefinite looping words; there’s also `again` for endless
loops and `until` for loops with the test at the end instead of the
beginning.

### Conditionals ###

The usual conditional is `wibbled? if handle-wibbled-case else
handle-nonwibbled-case then`, which is what we would write in C as `if
(wibbled()) { handle_wibbled_case(); } else {
handle_nonwibbled_case(); }`; as in C, the else-clause can be omitted.
`If else then` is handled by backpatching and compiling jumps in the
same way as `begin while repeat`, etc.  Here are the compile stack
comments from the three words from the standard:

- [If][4]: ( C: -- *orig* ) where *orig* is a new unresolved forward reference
- [Else][5]: ( C: *orig¹* -- *orig²* )
- [Then][6]: ( C: *orig* -- ) 

Because there are no backward jumps, there is no need to push a jump
destination address onto the stack for them, just addresses at which
to backpatch the forward jumps.  You can deduce from the stack effect
of `else` that you can intercalate the consequent and alternate cases,
switching between them with extra `else`s.

These aren’t the only conditional words; there’s also `case of endof
endcase`.

[4]: https://forth-standard.org/standard/core/IF
[5]: https://forth-standard.org/standard/core/ELSE
[6]: https://forth-standard.org/standard/core/THEN

### Definite looping ###

The most common form of definite looping is `do loop`, which
unfortunately always runs the loop at least once.  The loop counter is
called `i`.  For example:

    : ascii-table 127 32 do i 7 and 0= if cr then  i emit s" : " type i . loop ;

This produces a somewhat sloppy table looking in part like this:

    P: 80 Q: 81 R: 82 S: 83 T: 84 U: 85 V: 86 W: 87 
    X: 88 Y: 89 Z: 90 [: 91 \: 92 ]: 93 ^: 94 _: 95 
    `: 96 a: 97 b: 98 c: 99 d: 100 e: 101 f: 102 g: 103 
    h: 104 i: 105 j: 106 k: 107 l: 108 m: 109 n: 110 o: 111

(Formatted numeric output in Forth is standardized but a bit verbose.)

Compile-time control-flow stack effects:

- [Do][7]: ( C: -- *do-sys* )
- [Loop][8]: ( C: *do-sys* -- )

Interestingly, though, there’s a word [`leave`][9] to abort a `do
loop` early, and you can use it any number of times inside the same
loop, which means that `loop` may have to backpatch an arbitrarily
large number of conditional jumps.  And it is documented as not
changing the control-flow stack.  [Anton Ertl explains][10]:

> A classic technique (probably used by many systems) is to use the
> space for the target address of each leave branch to store a link to
> the previous LEAVE. Gforth stores more than fits there, so it uses a
> separate LEAVE stack.

In fact, such a `leave` can even be placed inside other nested
control-flow constructs; it will almost always be inside at least an
`if then`.  So Ertl explains it can’t simply rely on the *do-sys*
being on top of the control-flow stack when it’s compiled:

> And if [ruv’s proposed] restriction [that a *do-sys* be on the
> control-flow stack when `leave` is compiled] was standardized, we
> would not make use of it in Gforth, because it’s simpler to
> implement a separate stack than to keep track of the latest do-sys
> in the data stack, make room for additional information by moving
> the closer-to-top stack iterms, and storing the leave information
> that.

[7]: https://forth-standard.org/standard/core/DO
[8]: https://forth-standard.org/standard/core/LOOP
[9]: https://forth-standard.org/standard/core/LEAVE
[10]: https://forth-standard.org/standard/core/LEAVE#reply-632

A general outline of implementing Comfy combining forms in a Forth
------------------------------------------------------------------

I’m reluctant to say that this is “a Forth” because its control flow
looks so different from normal Forth; but I think it’ll be simpler.

Here’s [my latest formulation of Comfy combining forms from
today][12]; `go` is the entry point of a tree node, `yes` is its
success exit, and `no` is its failure exit:

    (a ; b) : go = a.go, a.yes = b.go, yes = b.yes, no = a.no = b.no
    (a ∨ b) : go = a.go, a.no = b.go, no = b.no, yes = a.yes = b.yes
    (¬a)    : go = a.go, no = a.yes, yes = a.no
    (a | b) : go = a.go, a.yes = b.go, b.yes = go, a.no = yes, b.no = no

The last one, `a | b`, is a while-loop.

[The compiler Henry Baker presented for COMFY-65][11] works, unsurprisingly,
in a Lispy recursive way; code generation proceeds from the leaves of
the abstract syntax tree up toward the root as recursive calls to the
compiler return the entry point of the code they’ve emitted.  Forth
compilation instead works by compiling words one at a time in, mostly,
the same order they’ll be executed in.

Comfy divides executable instructions into *jumps*, *actions*, and
*tests*.  *Jumps* are things like the return instruction which
terminates execution of a subroutine; as far as the compiler for that
subroutine is concerned, it’s a control-flow sink.  *Actions* are
normal instructions, and always succeed, which usually amounts to
passing control to the following instruction; in Forth things like
numeric constants and arithmetic would be actions.  And *tests* are
conditional jumps which might succeed, passing control to the
following instruction, or fail, passing control to the current
no-handler.

The lexical nesting of Comfy control-flow combining forms corresponds
to stacks of yes-handlers and no-handlers.  When you exit a form, you
pop those stacks.  Since you pop them at the same time, it’s fine to
put them onto the same stack.

The sequencing construct I called `;` above needs to be implicit
between adjacent actions, and so probably between most Forth words in
general.  And for reasonable compilation output on modern computers,
we also need the compiled yes control-flow transfer to not insert a
jump instruction in these cases.  This means that in the usual case
the yes-exit is the following instruction; when you exit a form,
you’ll have to insert an unconditional jump if the yes-exit is
something else.  This technique is generally applicable, though
generating inefficient code for tight loops.

Most of the jump destinations are forward jumps, so generally what’s
on these stacks is going to be unresolved forward references.  (This
seems like it could pose some difficulties for determining whether the
yes-exit is something else or not.)  Baker’s compiler inserts an extra
backwards unconditional jump at the end of loops, later backpatching
it, to handle the rare backward-jump exception to this rule.

[11]: https://dl.acm.org/doi/pdf/10.1145/270941.270947
[12]: https://news.ycombinator.com/item?id=41191683

### A first attack on the problem ###

I think you’ll need bracketing words to delimit the scope of the
desired Comfy forms.  So let’s consider this example code:

    a b if c d then e f else g h fi m n

This is using an `if then else fi` construct similar to what you'd see
in Lua or the Bourne shell.

Let’s call the control-flow context on top of the control-flow stack
at this point α.

The `if` word here doesn’t emit any code; it just pushes a fresh
control-flow context onto the control-flow stack, call it β.  That way
`c` and `d` are compiled in context β, so that if either of them is a
test that fails, they'll jump to β.no, which will eventually take us
to `g h`, but we don’t know that yet.  So it may be best to add any
such conditional jump address fields onto a linked list whose head is
stored on the control-flow stack, as Ertl explains `leave` often does.

`Then` also doesn’t emit any code, because if `a b` succeeded, we want
control flow to pass straight through to `e f`.  It resolves β.yes to
the current location.  But it needs to compile `e f` in the context α,
so any failures will take us to α.no.  I think this means it does the
control-flow-stack equivalent of `swap` or `2swap`, moving α on top of
β on the stack, but not eliminating β.  (It can’t copy it if it
contains the head of a linked list that we’re adding things to.)

`Else` has to do four things.  First, it compiles an unconditional
jump to `m n`.  But what is `m n`?  It’s the yes-exit for the whole
`if fi` structure.  This means that `if` needed to push *two*
control-flow contexts: say γ for the `if fi` as a whole, and β for the
condition.  `Else` needs to reach down and use that outer control-flow
context to get the jump target.  Second, it has to resolve β.no to
point to the code following the unconditional jump, which requires
walking over the list of any jump targets in `c` or `d` to backpatch
them.  Third, it has to discard β, since it’s fully resolved now.
Fourth, it has to set up to compile `g h` with the same α failure
handler, which actually requires no action since that’s what `then` is
already doing.

Finally, `fi` has to resolve the conditional jump target from γ (to
which `else`’s unconditional jump is tied) and then discard γ from the
control-flow stack, leaving only α, so `m n` gets compiled with α as
the context.

This suggests that what we have here is not really a stack of yes/no
contexts; it’s a stack of unresolved forward reference labels.  And
it’s not really all that stacky actually, because we keep having to
shuffle it around.

I need to think about this more.

### Algebraic negation ###

Given the first three rules:

    (a ; b) : go = a.go, a.yes = b.go, yes = b.yes, no = a.no = b.no
    (a ∨ b) : go = a.go, a.no = b.go, no = b.no, yes = a.yes = b.yes
    (¬a)    : go = a.go, no = a.yes, yes = a.no

We can see that `¬(a ; b) ≡ (¬a ∨ ¬b)` and `¬(a ∨ b) ≡ (¬a ; ¬ b)`,
which is a formulation of De Morgan’s Law for short-circuiting Boolean
operators.  Together with `¬¬a ≡ a` this gives us a normalization rule
for loop-free code that pushes all negation out to the leaves of the
abstract syntax tree.  This is a useful thing to do for compilation,
because to implement negation in the straightforward way by
interchanging the yes-exit and the no-exit, we’d have to be getting
the yes-exit off the control-flow stack all the time instead of just
passing it to the next instruction, so it’s useful if we can just
negate each instruction.

Suppose a test like `<` works by jumping to the no-exit if it fails,
so that in `if a b < then foo fi`, if `a` is in fact not less than
`b` — it’s greater than or equal to `b` — we jump to the `fi`.  Then
we can negate tests by inverting their sense.  Here a `u` prefix
denotes unsigned comparison:

    ¬<  ≡ >=    ¬u<  ≡ u>=
    ¬>  ≡ <=    ¬u>  ≡ u<=
    ¬>= ≡ <     ¬u>= ≡ u<
    ¬<= ≡ >     ¬u<= ≡ u>
    ¬!= ≡ ==
    ¬== ≡ !=

Negating *actions* amounts to inserting an unconditional jump to the
no-exit after them.  We can call that jump `fail`; its negation is
simply a nop.

Negating *jumps* leaves them unchanged, since jumps don't use either
of their exits; recall that jumps are black-hole instructions such as
a subroutine return instruction.

Algebraically inverting the loop `a | b` requires a second looping
construct which is a loop with a test at the bottom:

    (b when a) : go = b.go = a.yes, no = b.no, b.yes = a.go, a.no = yes

Then we have `¬(a | b) ≡ (¬a when ¬b)` and conversely `¬(a when b) ≡
(¬a | ¬b)`.  This completes our ability to algebraically push negation
down to the leaves of our abstract syntax tree.

However, consider this case:

    not x 10 < ton

Intuitively we want this to mean `!(x < 10)`, *i.e.*, x ≥ 10.  What
we’d like this to compile to is something like

            call x
            pushconst 10
            jlt initial-no-handler

Algebraically rewriting the AST `¬(x ; 10 ; <)` as described above
would give us `(¬x ∨ ¬10 ∨ >=)`.  But a straightforward implementation
will give us something like this instead:

            call x
            jmp no-handler-1
    no-handler-1:
            pushconst 10
            jmp no-handler-2
    no-handler-2:
            jlt initial-no-handler

A better way of handling this, rather than handling action negation
*at* the leaf, might be to have separate compilation rules for `(a ;
b)` and `(¬a ; b)`, and similarly for `∨`, and define both of them to
associate to the right.  Specifically, `(¬a ; b)` and `(a ∨ b)` both
insert `fail` in between the compilation of the operands, while `(a ;
b)` and `(¬a ∨ b)` do not.

So the rewriting sequence for `¬(x ; 10 ; <)` is:

    ¬(x ; 10 ; <).compile
    ¬(x ; (10 ; <)).compile
    (¬x ∨ ¬(10 ; <)).compile
    (¬x ∨ (¬10 ∨ ¬<)).compile
    (¬x ∨ (¬10 ∨ >=)).compile
    x.compile || (¬10 ∨ >=).compile
    x.compile || 10.compile || >=.compile

which gives us the result we were looking for.

We do still need the rewrite rule that inserts `fail` after a negated
action in the unusual case of a failing action at the end of a negated
sequence.  So I wonder if we can maybe formulate the fail-insertion
rules as algebraic rewrite rules as well?  Maybe by tweaking the De
Morgan rules?  I’m not exactly sure how.

But, returning to the Forth context, how would we handle negation with
a control-flow stack compiling words one at a time?  Maybe we could
have a single bit telling us if we’re in a negated context or not,
which both `not` and `ton` flip in sequences like `not x 10 < ton`.  I
think it wouldn’t affect the compilation of actions like `x` and `10`,
since their negation in such sequences comes out in De Morgan’s wash,
but it would reverse the sense of `<`.  And in the case where the
sequence compiled inside of it *doesn’t* end in a test, `ton` would
have to insert a `fail`.

This negation bit would have to get saved on the stack when we enter
the conditional of an `if`, but then it gets restored when we proceed
through the `then`.

It would be pretty nice if you could rewrite `if (!x) { y(); } else {
z(); }` to `if (x) { z(); } else { y(); }` but the in-order Forth
compilation paradigm makes this impossible.  However, the
transformation is pretty easy to do by hand.

See file `comfy-expressions.md` for a clearer perspective.
