I've been looking at the assembly language of the [ARM][3], and one of
the nice things about it is that, despite nominally being RISC, you
actually have a quite rich set of addressing modes, which especially
matters for in-order "scalar" cores.  In particular, both for directly
addressing memory and for computing effective addresses, you can
left-shift one of the operands.  XXX examples

[3]: https://www.cs.umd.edu/~meesh/cmsc411/website/proj01/arm/armchip.html

In a discussion with Jeremiah Orians, he raised the concern that
having the barrel shifter in the datapath to the ALU might be
unreasonably costly.  It certainly does eat up a lot of instruction
bits, but does it [cost a lot of transistors][11] or slow down the
clock speed?

[11]: https://en.wikipedia.org/wiki/Transistor_count

ARM2 was the first non-prototype; it came out in 01986 at 8 MHz.
Contemporary processors were we have SPARC MB86900 (RISC, 17 MHz), and
[NEC V60][0] (CISC, 16 MHz), both considerably higher in clock speed,
but the V60 only got about 0.22 instructions per clock, while the ARM
was about 0.53 and the SPARC was close to 1, so the V60 was actually
slower.  It's perhaps relevant that those were respectively 4× and 14×
the size of the 27'000-transistor ARM2.

[0]: https://en.wikipedia.org/wiki/NEC_V60

The next version that came out was ARM3 in 01989, at 25 MHz and with a
cache.  Other contemporary hardware included the 1.2-million-transistor
[80486][8] (CISC, 25 MHz), the 1-million-transistor [i860][9] (VLIW,
40 MHz), and maybe the 6.9-million-transistor [POWER1][10] (RISC, a bit
later, 30 MHz), also all much bigger than the 310'000-transistor ARM3.

[8]: https://en.wikipedia.org/wiki/I486
[9]: https://en.wikipedia.org/wiki/Intel_i860
[10]: https://en.wikipedia.org/wiki/POWER1

In 01992 the ARM250 came out (12 MHz, [overclockable to 26MHz][1]),
and the ARM700 came out in 01993 (33 MHz).
                 
[1]: https://www.vectorlight.net/retro/computers-and-modifications/acorn-archimedes-a3010-arm250-overclock                 

00:50 < muurkha> contemporary to ARM700 we have Alpha 21064 (RISC, 200 MHz), the 68060 (CISC, 50 MHz), [P5 Pentium][4] (CISC, 66 
                 MHz), and POWER2 (RISC, 72 MHz)
                 
[4]: https://en.wikipedia.org/wiki/Pentium_(original)#P5

So maybe at that point ARM's clock speeds were starting to lag?  But
that was also the time when ARM was refocusing on low-power mobile
devices.

By comparison, DEC's [StrongARM SA-110][6] shipped in 01996 with a 233
MHz clock.  A contemporary CISC part was the [AMD K5][5] (100 MHz),
and the (RISC) Alpha αxp had by that time been pushed to the [EV5
21164A][7] at 400 MHz.

[5]: https://en.wikipedia.org/wiki/AMD_K5#SSA/5
[6]: http://web.archive.org/web/20230314234724/www.hpl.hp.com/hpjournal/dtj/vol9num1/vol9num1art5.pdf "A 160-MHz, 32-b, 0.5-W CMOS RISC Microprocessor.  Reprinted, with permission, from IEEE Journal of Solid-State Circuits, volume 31, number 11, November 1996, pages 1703–1714."
[7]: https://en.wikipedia.org/wiki/Alpha_21164

So I don't see a super compelling argument that the barrel shifter in
the datapath cost a lot of transistors or a big clock-speed penalty.
Why, then, have they not been copied in other architectures and
dropped in ARMv8 (aarch64 aka arm64)?

Orians points out that in a superscalar design you have many
functional units, and supporting these shifts in every ALU instruction
means that you have to route the instructions that use them to
functional units equipped with an extra shifter, or put a shifter on
every functional unit, even those for instructions that are very
unlikely to benefit from the shifts.  And going superscalar generally
eliminates the justification of saving a cycle when you want to shift
and add.  And they eat up 8 bits of the most common instruction
format; while the early ARMs up to ARM3 didn't have the transistor
budget for a larger register file, having only 16 GPRs increasingly
looks like a bad tradeoff.
