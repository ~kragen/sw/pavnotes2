Springs have a lot of interesting features for energy storage.  They
can last a very long time, orders of magnitude longer than batteries
(many springs fabricated in the 18th century are still good, and they
can even *store* energy for centuries), and they can be designed for
extremely high power density.  Compensating for these advantages, they
generally have very low energy density.

Spring type comparison
----------------------

Let's consider the ways we could store 700 J and release half of it in
1.5 milliseconds (230 kilowatts), in a machine a human could easily
lift.

### Steel springs ###

First, consider tensile steel springs.  The tensile resilience of a
spring steel with yield stress *σᵧ* = 1 GPa and a Young's modulus *E*
= 200 GPa is *σᵧ*²/2*E* = 2.5 MJ/m³ = 2.5 J/cc.  The results for
torsion are a little different but this is not a desirable order of
magnitude; 700 J would require 280 cc = 2.2 kg of such a spring steel.
Music wire may have yield stress of over 3 GPa but we'd still be at an
uncomfortably high mass of 0.24 kg.

Some "spring steels" like AISI 5160 have [much lower yield
stresses][5] like 530 MPa, adding an additional factor of about 4 in
weight.  (Some sources give an even lower number of 275 MPa, but this
may be an error.)  I think the advantage of these steels over music
wire is that they are much cheaper and, because of the plastic
deformation region above their yield stress, much tougher when
subjected to impacts.

[5]: https://www.matweb.com/search/datasheet.aspx?matguid=972ec49b746d47c2a31db406e9213247&ckck=1

Without bothering to do the integral, if your 2.2 kg spring is moving
25.2 meters per second, it has 700 J of kinetic energy.  That means
that if one end of the spring is moving tens of millimeters during
this 1.5-millisecond release time, it's going to be spending pretty
much all of its potential energy on accelerating its own mass and
almost none of it on doing the work we want done.  This means that, to
achieve good efficiency, we probably want it set up to move 5 mm or
less; releasing 350 J in 5 mm requires an average force of 70 kN, 7
tonnes.  This requires a spring rate on the order of 1 kN per mm,
which is pretty stiff; plenty of ordinary steel objects not designed
as springs probably have that much compliance or more.

This holds *a fortiori* for the lower-yield-stress steels mentioned
above; to get good efficiency they need to move even less distance,
which means even higher spring rates; while higher-yield-stress steels
like music wire could manage significantly lower masses and thus
larger displacements and compliances.

Still, it's astounding to realize that we've derived that a lump of
2.2 kg of steel, deployed as an I-beam leaf spring, can deliver power
pulses of 230 kilowatts for thousands of microseconds.

### Steel air springs ###

Air springs, though, seem like a much better bet.  But the air needs
to be compressed from the start.

[Some random gas law calculator I found online][0] claims that
adiabatic compression of 750 ml of 300-kelvin nitrogen from one
atmosphere to 3000 psi (21 MPa) will reduce it to 16.8 ml and raise
its temperature to 1370 K, which is "only" 1098°, while storing 679 J
of energy in it.  This compressed gas could be a cylinder 26.6 mm in
diameter and 30 mm long, which seems like you could contain it in a
much smaller amount of steel or aluminum than 2.2 kg.  Like, if it had
to be 8 mm thick, the ends and corners would be 22.8 cc, and the walls
would be another 26.1 cc, which makes 48.9 cc in total, much less
solid volume than the springs.  But, even if you could make the walls
very thin, the overall volume of the uncompressed gas is unwieldy, and
the stroke of the piston is probably too long.

You probably can't do better by trying to use an isothermal process
rather than an adiabatic one.

[0]: https://www.omnicalculator.com/physics/thermodynamic-processes

But if we start with the air already under pressure we can get much
more reasonable results: if 110 cc of nitrogen starts at 300 K (27°)
and 1000 psi (6.9 MPa) and gets compressed to 3000 psi (21 MPa), it
heats to only 137° and 50 cc while storing 700 J.

Suppose our cylinder is 35.6 mm in diameter, with a circular piston,
which with 110 cc of total capacity gives a 110-mm-long cylinder.

Thin-walled cylinder hoop stress *σₕ* = ½*Pd*/*t*, where *t* is wall
thickness, *P* is the pressure difference, and *d* is the diameter.
So with *r* = 17.8 mm, *d* = 35.6 mm, shell thickness *t* = 1.4 mm,
*P* = 3000 psi (20.7 MPa), *E* = 200 GPa, and *μ* = 0.28, the hoop
stress *σₕ* would be 260 MPa, which is too much for mild steels like
ASTM A36.  If we expand *t* to 2 mm, we get 184 MPa hoop stress, and
the longitudinal stress would be half of this.  But this gets us out
of the "thin shell" regime for which this approximation is valid.

Let's suppose it's still close enough, though.  The steel walls of
this 110-mm-long, 35.6-mm-diameter, 2-mm-thick cylinder then occupy
26.0 cc and weigh 205 g, which is a significant but not overwhelming
weight in this context.  It's certainly a great deal less than the
weight of an equivalent-energy-capacity spring made of the same mild
steel material.

That leaves out the required weight of the piston or pistons, which
depends greatly on their structure.  Suppose one piston travels 30 mm
in this 1.5 ms when we relese the energy; this is an average of 20
m/s.  If this speed is almost constant and the piston weighs 176 g,
then the piston's kinetic energy will be 35 J, which is 10% of the
total 350 J being expended.  This seems a great deal more manageable
than the analogous response-time problem with steel springs; very
plausibly we can make the piston more like 10 g.

At 20 m/s, the piston in this cylinder has 200 J/kg, so if its own
mass is, say, 50 g, it will only have 10 J of kinetic energy.  This
seems like an acceptably small sort of loss.

The nitrogen (or air) contents of the cylinder are about another 9 g.

As a double-check, 21 MPa on our 35.6-mm-diameter piston works out to
a force of 21 kN, which would be capable of accelerating a 176-gram
piston at 119 km/s/s, about 12000 gees; at the end of 1.5 ms an
unrestrained piston would be traveling 178 m/s (Mach 0.54 at STP) and
would have traveled 134 mm.  In 30 mm 21 kN works out to 630 J,
suggesting I've made an error of a little over a factor of 2
somewhere.  But it definitely reinforces the calculation that an air
spring can achieve enormously faster response times than a steel
spring.

It's worth noting that the 21 kN noted here is much lower than the 70
kN estimate above for steel springs.

So here we have derived an even more surprising result: we can get our
desired 230 kW out of 10 g of air compressed into a 200-g steel
cylinder, which is an order of magnitude improvement over the steel
spring above.  Moreover, if we have a better grade of steel available,
not only can we use proportionally lighter steel springs; we can use
thinner walls to get an almost proportionally lighter air spring.

### Fiberglass air springs ###

But, because the cylinder wall requires only the strength and rigidity
of the steel, not its resilience, you could substitute a glass fiber
wrap instead, as people commonly do for compressed natural gas tanks.
This would save you about 75% of the weight, putting you in the
neighborhood of 50 g.  [Ordinary E-glass fiber][2] has a tensile
strength of 3400 MPa and a Young's modulus *E* = 72 GPa, while S-glass
has tensile strength of 4600 MPa and *E* = 89 GPa, so in theory you
might be able to get by with as little as 100 microns of average glass
fiber thickness (1.23 cc total, 3.1 g, not counting the resin); but
the cylinder would expand very noticeably, like, over a millimeter,
which could cause a lot of trouble with sealing.

[2]: https://jpscm.com/products/e-glass-s-glass/

Getting enough rigidity out of the fiberglass walls to allow a piston
to slide in at atmospheric pressure, while still sealing at high
pressure, might require reinforcing the walls with orders of magnitude
more fiber.

### Using N air springs in parallel requires N times as much wall material ###

Using multiple smaller cylinders would add weight (using half the
diameter permits using half the thickness at the same pressure and
hoop stress, but requires four cylinders, for an overall doubling of
material) but would reduce the absolute deformation of the shell
proportionally.  But just using more thickness at the original size
would also reduce elastic deformation proportionally while also adding
to the margin of safety.

Still, it might be a worthwhile thing to do in order to fit the
cylinders into a narrow space; weights on the order of 3 g provide a
significant amount of leeway.

### Fiberglass tension springs ###

If we apply the same *σᵧ*²/2*E* formula to S-glass, we get a
resilience of 120 MJ/m³ = 120 J/cc, 48 times higher than the 2.5 J/cc
energy density of the steel springs we were considering; considered as
a specific energy by dividing by 2.5 g/cc, it's 48 J/g (= 48 kJ/kg),
compared to that steel's 0.32 J/g (= 320 J/kg), differing by a factor
of 150.

Incidentally we can interpret 320 J/kg and 48 kJ/kg as speeds: they're
25 m/s (Mach 0.076) and 310 m/s (Mach 0.93) respectively.  These are
the speeds to which tension springs made of these materials could
potentially accelerate *themselves*, more or less in the fashion of
shooting a rubber band off the end of your finger.

I'm sort of at a loss how to actually make usable macroscopic tension
springs out of glass fiber, though.  I think the I-beam-leaf-spring
construction I suggested above for steel won't work because the glass
fiber doesn't have any significant strength in compression, because it
buckles whenever the fiber has lengths of a millimeter or more.  And
while you can get significant compressive strength by embedding the
fiber in something stiff, it won't have compressive strength or
rigidity comparable to the glass fiber's tensile strength or rigidity
unless the composite is heavily filled with some material that's much
stiffer than the glass fiber, such as steel or corundum.  But then
most of the composite's mass is in this other filler and not the glass
fiber itself.

Archival power sources
----------------------

A thing I've been pondering is how to make a computer that could
survive a century or more of burial while retaining its data intact
(the "Egg of the Phoenix").  A great difficulty is that
electromechanical data storage like hard disks is dependent on
lubricants, which tend to harden during long periods of inactivity,
while non-electromechanical data storage like Flash and FRAM is
generally only specified for 10- or 20-year data retention.

If you could rewrite your Flash every 5 years the problem would be
solved, but you need a power source for that.  And all the
currently-manufactured batteries I can find information about are only
good for a shelf life of about 10 years.  Lithium thionyl chloride
batteries last a little longer but are unobtainium.  The Clarendon Dry
Pile has continued to produce power for 182 years so far, but nobody
knows how it was made any more, and quite possibly even its original
maker didn't know how to do such a thing reproducibly.

Very roughly rewriting Flash needs about a microjoule per byte, so
about a kilojoule per gigabyte.  To rewrite one gigabyte every 5
years, you need about 6 microwatts.

### Wind-up archives ###

So maybe we could pre-store all the energy we need for that rewriting
in a wind-up spring, like a mechanical watch?

For 100 years and 1 GB rewritten every 5 years, we'd need about 20 kJ.
If we take the 320 J/kg number for steel from above, we could store
that in about 60 kg of steel, which is an uncomfortably large machine
but still a human-scale one.  If we can somehow use S-glass tension
springs instead, with their 48 kJ/kg, we would only need 400 grams of
them.

#### Creep may kill this idea ####

Springs lose their energy over time due to [creep][9], which WP says
becomes noticeable in metals at 35% of their melting point and in
ceramics at 45% of their melting point.  Iron melts at 1538° = 1811 K,
so 35% is 633 K = 361°, so maybe we should be fairly safe as long as
the archive remains near room temperature?

The general creep equation is not very helpful; it just says the creep
rate due to a given creep mechanism is proportional to exp(-*Q*/*kT*)
where *Q* is the activation energy of the creep mechanism, *k* is
Boltzmann's constant, and *T* is the temperature.  But that means that
if we don't know *Q* we don't know the exponential growth rate; one
creep mechanism might double in speed every 2 K while another doubles
in speed every 200 K.

The [early creep test results from 01928][7] are not extremely helpful
in a quantitative sense, but they do show undetectably small amounts
of creep over 900 hours at room temperature, while creep caused sample
breakage at smaller loads after 300 hours or less in specimens of the
same steel at 295°, which is colder than the 361° above.

In fact, in their Figure 7, we see what sure looks like detectable
creep over only 1000 hours *at room temperature* in a hot-finished
tool steel.  The creep strain in that case amounted to about 0.01
"inches per inch" when stressed to just below the breaking point, 640
MPa, a stress which resulted in an initial strain of about 0.08
"inches per inch".  However, I wonder if I'm reading this right, or if
they recorded it right; that would seem to imply give it a Young's
modulus of only 8 GPa, not the 68 GPa they were assuming nor the 200
GPa I cited above.

[9]: https://en.wikipedia.org/wiki/Creep_(deformation)
[7]: https://nvlpubs.nist.gov/nistpubs/nbstechnologic/nbstechnologicpaperT362.pdf "Creep in Five Steels at Different Temperatures, by French, Cross, and Peterson, Technologic Papers of the Bureau of Standards, No. 362, January 10, 01928"

Another paper I looked at found creeps of about 1000 ppm at room
temperature over only 70 hours at room temperature in a steel.

### Other alternative archival power sources ###

The one technique I'd run across previously that I'm confident would
work in this context is the one used in the air-pressure-driven clock
invented by Cornelis Drebbel, now marketed under the name Atmos: a
hermetically sealed bellows filled with a room-temperature-boiling
liquid such as chloroethane drives a piston.  Daily fluctuations in
either atmospheric pressure or temperature cause the bellows to expand
and contract, driving a ratchet mechanism.  These atmospheric tides
affect the pressure even through tens of meters of soil.  The Beverly
Clock at the University of Otago, using this principle, has not been
wound since its construction in 01864, and it is currently running,
though it has occasionally stopped.

Normal atmospheric tides are, to simplify, 100 Pa with a period of
every 12 hours; 100 Pa is 100 mJ/liter, so this is potentially 100
mJ/liter every 6 hours, about 4.6 microwatts/l.  But it is difficult
to even approach this ideal efficiency; the Beverly Clock uses a
28-liter box, but only harvests something like a microwatt from it.  I
think the Atmos designs are a little better here.

Betavoltaic batteries and the like are another possibility, but
they're hard to get.

Fuel cells and flow batteries are another possibility, if they can
remain in working order for long periods of time.

Some heat-producing fuel mixes certainly have the necessary shelf
life, but may pose some practical difficulties in harnessing them.
For example, a powdered magnesium reducer with a tenorite oxidizer
theoretically produces heat with Mg + CuO (-156 kJ/mol) = MgO (-601.6
kJ/mol) + Cu, with a molar mass of (40.304 + 63.546 = 103.850) g/mol
and thus 446 kJ/103.85 g = 4.29 kJ/g.  The 20 kJ we wanted to store in
60 kg of steel springs or 400 g of S-glass springs would thus require
only 5 g of this fuel mix.  Or, say, 50 g if your conversion to useful
energy was 10% efficient.

But how do you turn that into electricity instead of just a sad pile
of slag where your archival computer used to be?
