Conventionally, CPUs have comparison instructions.  On amd64 you have

        cmp %rax, %rbx
        setl %cl

and on RISC-V you have

        slt t1, a0, s0

and each of these will load either a 0 or a 1 into a destination
register.  Even more conventionally (SETcc wasn't added to the 8086
family until the 80386) there's a conditional branch instruction, so
that if you want to set a register to the lesser of itself and some
other register, I think it's (untested):

        cmp %rax, %rbx
        jl 1f
        mov %rax, %rbx
    1:

Or on RISC-V (tested):

        blt a0, s0, 1f    # jump to 1 forward if a0 < s0
        mv a0, s0         # overwrite a0 with s0
    1:

So you can express pairwise min (or max) in terms of an order
comparison instruction.  But, if you have an instruction for pairwise
min (or max) and an equality comparison instruction, you can instead
express order comparison in terms of those.  For example, if the
instruction `min p, q, r` sets p to the minimum of q and r, you can
write

        min t0, a0, s0
        beq t0, a0, somewhere

rather than (I think)

        blt t0, a0, somewhere

Some CPUs do have min/max instructions but generally they do not omit
order comparison instructions as a result.  And this is probably the
right call, because even though you can express blt in terms of
min/max or vice versa, min/max is probably less common than branching
on the result of a comparison, and it is probably better to privilege
the common case rather than the uncommon one if you're just going to
implement one of them.

How odd, then, that when it comes to bitwise operations, CPUs mostly
provide AND and OR, which are bitwise minimum and maximum, and
negation and XOR.  Bitwise "greater than" is abjunction, which can be
written as A & ~B, and some CPUs do have an instruction for this; it
seems to me that it's as common an operation as AND and more common
than OR, except in cases where addition would work fine as an OR
substitute.  Golang has the `&^` operator for it.
