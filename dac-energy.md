<https://pubs.rsc.org/en/content/articlehtml/2022/ee/d1ee03523a> is
recent and seems to be well written, but i haven't finished reading
it.

Ozkan et al. 02022
------------------


<https://doi.org/10.1016/j.isci.2022.103990>

<https://www.cell.com/iscience/fulltext/S2589-0042%2822%2900260-7>

this is from 02022, by Ozkan, Nayak, Ruiz, and Jiang

<https://www.volker-quaschning.de/datserv/CO2-spez/index_e.php>

18:26 < muurkha> this gives the numbers of 436 g CO₂ per kWh *electrical* for natural gas (mean), up from the 433 g CO₂ per kWh 
                 electrical cited in the sneer video, and 970 g CO₂ per kWh electrical for hard coal (down from the 1001 g CO₂/kWh(el) 
                 cited in the sneer video)
18:26 < muurkha> including upstream emissions
18:27 < muurkha> but on the very same page Quaschning also gives a table of thermal outputs: 200.8 g CO₂/kWh(pe) for natural gas, 338.2 
                 g CO₂/kWh(pe)
18:30 < muurkha> taking the reciprocals, Chalmin's hitpiece figure for Climeworks of 650 kWh(el)/tonne CO₂ gives us 1538 g CO₂ per 
                 kWh(el), 3.5 times more CO₂ than would be emitted to provide that electrical energy by burning gas and still 59% more 
                 than would be emitted to provide it by burning coal


Broehm, M., Strefler, J., and Bauer, N. (2015). Techno-Economic Review
of Direct Air Capture Systems for Large Scale Mitigation of
Atmospheric CO2 (SSRN Scholarly Paper No. ID 2665702) (Social Science
Research Network). <https://doi.org/10.2139/ssrn.2665702>.

Kiani et al. 2020
-----------------

Kiani, A., Jiang, K., and Feron, P. (2020). Techno-economic assessment
for CO2 capture from air using a conventional liquid-based absorption
process. Front. Energy Res. 8,
92. <https://doi.org/10.3389/fenrg.2020.00092>
<https://www.frontiersin.org/articles/10.3389/fenrg.2020.00092/full>
CC-BY

This is a monoethanolamine paper unfortunately.  I say “unfortunately”
because I suspect we'll have scalability limits with MEA; also, it’s a
simulation paper.  Also, their units don’t make sense; they’re
measuring gas flow rates in “Nm³/h”, which sounds like “newton cubic
meters per hour”.  Ultimately they calculate that their proposed
process is not competitive with existing processes; they calculate a
total electrical energy requirement of 1.452 MWh/tCO₂, which is to say
5.227 GJ/tCO₂, and later they say the thermal energy required is 10.7
GJ/tCO₂, which I think is added to the electrical cost.

They say Carbon Engineering uses an alkali-catalyzed lime calcination
process like my baseline process (but using potassium rather than
sodium), while Climeworks and Global Thermostat are using amine-based
solid sorbents.

National Academies of Sciences (2018). Negative Emissions Technologies
and Reliable Sequestration: A Research Agenda (National Academies of
Sciences). <https://doi.org/10.17226/25259>.

Socolow, R., Desmond, M., Aines, R., Blackstock, J., Bolland, O.,
Kaarsberg, T., Lewis, N., Mazzotti, M., Pfeffer, A., Sawyer, K., et
al. (2011). Direct Air Capture of CO2 with Chemicals: A Technology
Assessment for the APS Panel on Public Affairs (American Physical
Society). <http://refhub.elsevier.com/S2589-0042%2822%2900260-7/sref104>
<https://infoscience.epfl.ch/record/200555/files/dac2011.pdf>

Chatterjee, S., and Huang, K.-W. (2020).  Unrealistic energy and
materials requirement for direct air capture in deep mitigation
pathways.  Nat. Commun. 11,
3287. <https://doi.org/10.1038/s41467-020-17203-7>.

Lebling, K., McQueen, N., Pisciotta, M., and Wilcox, J. (2021). Direct
Air Capture: Resource Considerations and Costs for Carbon Removal
(World Resources Institute).
<http://refhub.elsevier.com/S2589-0042%2822%2900260-7/sref63>
<https://impakter.com/direct-air-capture-resource-considerations-and-costs-for-carbon-removal/>
(evidently no DOI)
