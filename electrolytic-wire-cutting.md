What could an electrolytic wire saw for cutting metal be like?

Iron is easiest to dissolve in water as trivalent ions (for example,
as ferrous sulfate), though many divalent salts also have significant
solubility.  Gamburg and Zambari's textbook tells us (see file
`electrolytic-cyclic-fabrication.md`) that the "volume electrochemical
equivalent" of trivalent iron is 8.83 micron decimeter squared per
amp-hour (24.5 nanoliters per coulomb), and you only need like 2-3
volts to electrolytically dissolve iron, depending on your counter
electrode.  If it's 3 volts, you get 0.93 nanoliters per joule.

Cellphone repair shops use 100-micron-thick molybdenum wire to
delaminate cellphone displays for repair.  You could imagine an
electrolytic wire saw machine that circulates such a wire in an
electrolyte to cut through steel, leaving, say, a 150-micron kerf.
Cutting through a square centimeter of steel in this way would involve
electrolytically dissolving 15 microliters of iron, which at ideal
Faraday efficiency would cost 610 C or 16 kilojoules.  Or maybe 1.8?

XXX most of what's below is based on this 16 kJ figure, but I think
it's a miscalculation.

According to file `motors-and-tools.md`, lithium-ion batteries hold
420 J/g, so 16 kJ is just 38 grams of lithium-ion battery capacity.
This is an eminently practical amount to carry around with you.

A carefully designed power supply would avoid burning the wire when it
shorted out to the workpiece; a carefully designed motion control
system would then move back.

Similar thicknesses of copper wire are easily available as strands of
stranded conductors; the potential advantage of molybdenum is that
it's more resistant to breakage.  Circulating the wire through the cut
is supposed to help the electrolyte circulate and average out
unevenness due to asperities on the wire.

But how fast is this?  Suppose you're cutting a 10 mm x 10 mm square
bar parallel to its square edges.  If you could anodically dissolve it
at 10 A/dm², which is in the range of normal iron electrolysis
currents, well, your surface area is 1.5 mm², so you can only apply
1.5 mA, or, say, 4.5 mW.  At this speed it will take 41 days to cut
the entire centimeter thickness.  So unless you can use *much* higher
currents for this process than are used in conventional electrolytic
iron processes, it will be unusably slow for macroscopic things.

However, the same reasoning would seem to show that PECM would take 41
days to cut through 10 mm of metal regardless of the shape of the cut,
whether you're disintegrating the whole metal surface or just cutting
a narrow kerf like this.  And in fact that isn't true at all; PECM is
capable of quite decent material removal rates in practice, though
cutting a narrow kerf like this should be almost its worst case, with
only small hole drilling being worse.  I think the answer is that
electrolytic dissolution can in fact use much higher currents than
processes like electrocleaning and electroforming, particularly when
pitting can be tolerated, but I don't have a way to estimate how much
higher.

[The Electrochemical Society Electrochemistry Encyclopedia article on
ECM][39] says that a typical feed rate for die-sink ECM is 0.02 mm/s
with a process gap of 0.4 mm between the electrodes, 10 V, and 3–30
m/s of electrolyte speed.  At 0.02 mm/s these 10 mm would take 500
seconds to cut.  The article also says, talking of surface finishes:

> Occasionally, metals that have undergone ECM have a pitted surface
> while the remaining area is polished or matte. Pitting normally
> stems from gas evolution at the anode; the gas bubbles rupture the
> oxide film.
> 
> Process variables also affect surface finish. For example, as the
> current density is raised the finish generally becomes smoother on
> the workpiece surface. A similar effect is achieved when the
> electrolyte velocity is increased. In tests with nickel machined in
> hydrochloric acid solution the surface finish has been noted to
> improve from an etched to a polished appearance when the current
> density is increased from about 8 to 19 A/square centimeter with
> constant flow velocity.
> 
> ... Full-form shaping utilizes a constant gap across the entire
> workpiece and the tool is moved mechanically at a fixed rate toward
> the workpiece in order to produce the type of shape used for the
> production of compressor and turbine blades. In this procedure,
> current densities as high as 100 A/square centimeter are used, and
> across the entire face of the workpiece, the current density remains
> high.

[39]: https://knowledge.electrochem.org/encycl/art-m03-machining.htm

8–19 A/cm² is 800–1900 A/dm².  Over a surface area of 1.5 mm² that's
120–290 mA, and if it were at 10 V, we'd be talking about 1.2–2.9 W.
That would still take hours to make this cut, but not months.

If you could somehow get 1 kW of power into the kerf at 3 V, which
would be 333 amps (and thus unlikely) then you could complete the cut
in 16 seconds, which is slower than conventional approaches to cutting
steel but not absurdly so; it's also comparable in power requirements,
though perhaps making a narrower kerf.  To the extent that the
limiting factor is resistive heating of the "saw" wire, this can be
ameliorated by using a thicker wire and a wider kerf; the current
required to advance the kerf at a given feed rate is ideally
proportional to the width of the kerf, while the current capacity of
the wire is ideally proportional to the square of the width of the
kerf.  But the wider kerf also requires proportionally more energy to
cut through a given cross-section, so it would be a much more
energy-intensive approach to cutting than conventional mechanical
methods.

That knot can be cut by using a cathode made of a strip of metal foil
which is insulated except at its leading edge.  The whole width of the
strip can carry current, but the kerf width is still only the foil
thickness.

I've previously suggested that using a cutting cathode shaped like an
actual saw blade, or a comb, might ameliorate this problem by allowing
the feed rate of the cathode into the workpiece to be many times
greater than the speed at which the surface of the workpiece is
receding.  By moving the cathode alternately parallel to one edge of
its teeth or the other, perhaps we can avoid the situation where the
dissolution of the workpiece around the tooth point becomes the
bottleneck.  If this approach works, it would permit cutting speeds
that are orders of magnitude faster still.
