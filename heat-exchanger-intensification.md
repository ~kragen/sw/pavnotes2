For [process intensification][0] purposes it may be interesting to see
how fast you can get a regenerator-type heat exchanger to run, because
then it can be smaller.

[0]: https://en.wikipedia.org/wiki/Micro_process_engineering#Process_intensification

In a continuous-operation [regenerator][2] you have, typically, two
pebble beds with gas flowing through them.  One is heating up the cold
gas that flows through it; the other is cooling down the hot gas that
is flowing through it.  When the first has cooled down enough and the
second has heated up enough, you switch the gas streams.  If they are
running in opposite directions ([countercurrent][1] heat exchange),
you can transfer nearly all of the heat from the hot gas to the cold
gas, without much mixing of the gas itself.

[1]: https://en.wikipedia.org/wiki/Countercurrent_exchange
[2]: https://en.wikipedia.org/wiki/Regenerative_heat_exchanger

Unlike [recuperator-type heat exchangers][3], which [tend to be much
larger than regenerators][5], the fluids in a regenerator are not
isolated from one another, and can exchange mass as well as heat; in
particular, [noses are intermittent-operation regenerators][4] which
transfer most of the water from exhalation back into inhalation.

[3]: https://en.wikipedia.org/wiki/Regenerative_heat_exchanger
[4]: https://en.wikipedia.org/wiki/Nasal_concha#Other_animals
[5]: https://en.wikipedia.org/wiki/Regenerative_heat_exchanger#Advantages_of_regenerators

The heat being transferred in the regenerator temporarily resides in
the solid thermal storage medium, the pebble bed or whatever.  The
thermal resistance to heat transfer between the solid and the fluid
determines the required residence time of the fluid and consequently
the time required for the heat exchange and the mass of thermal
storage medium required.  Part of this resistance comes from the
solid, but most of it typically comes from the fluid, especially when
it’s a gas.  Gases [typically have thermal conductivities in the range
of 0.01 W/m/K][6], while solids are typically closer to 1 W/m/K, and
metals and network solids like alumina and silicon carbide are in the
range 30–1000 W/m/K.  To some extent this can be dealt with by using
much thinner gas passages than solid material.

[6]: https://en.wikipedia.org/wiki/Thermal_conductivity#Experimental_values

However, the thermal conductivity alone is not the correct figure of
merit for the thermal-storage-medium application, aside from the
necessity to reject unsuitable materials such as those that will react
with the gases in question or those that will melt or crumble at the
necessary temperature, or the question of strength.  This is because a
material with higher [volumetric heat capacity][7] can be used in
thinner layers, which therefore will transfer heat more rapidly.

[7]: https://en.wikipedia.org/wiki/Volumetric_heat_capacity

I thought that maybe the figure of merit that was really most relevant
for the thermal storage medium in a regenerator was [thermal
diffusivity][8], which is the ratio of thermal conductivity (say,
W/m/K) to volumetric heat capacity (say, J/m³/K; the product of
density and specific heat).  The resulting dimensions are m²/s.  (This
is not the same as thermal effusivity.)  But I think that’s not quite
right.

[8]: https://en.wikipedia.org/wiki/Thermal_diffusivity

Let’s consider how this varies with thickness.  Consider a regenerator
made up of a bunch of parallel plates with a total volume of 1 cm³ and
a total thickness of 1 cm, so each plate is 1 cm², with a surface area
of 2 cm², disregarding the edges.  The centerplane of each plate is at
1100°, and its surface is at 100°.  How fast is heat flowing?

If the thickness of the plate is *t*, so the distance from its
centerplane to its surface is ½*t*, we have a thermal gradient of
2·1000°/*t*.  And we have a surface area of 2 · 1 cm² · (1 cm / *t*).
So if our thermal conductivity is some number *λ* the power will be 4
· *λ* · 1000° · 1 cm³ / *t*².

To be concrete, suppose that for hot [copper][9] (melting point
1084.62°, so we have to fudge a little) the [conductivity averages 380
W/m/°][10] and we are using *t* = 200 μm, rather robust copper foil
(32-gauge copper sheet is 201 μm, because copper is measured with the
AWG system; [“a heavy-duty tooling foil”][11] that “can be cut fairly
easily with scissors and is still very pliable”.)  The power should be
38 megawatts:

    You have: 4 * 380 W/m/K * 1000 K * 1 cm^3 / (.2 mm)^2
    You want: MW
            * 38
            / 0.026315789

[9]: https://en.wikipedia.org/wiki/Copper
[11]: https://basiccopper.com/thicknessguide.html
[10]: https://en.wikipedia.org/wiki/List_of_thermal_conductivities#Analytical_list

By contrast, if we were to use 1-mm copper, five times thicker (“18
gauge”, “[can be cut][11] with heavy duty snips or sheet metal
shears. It is very difficult to bend by hand”), the heat would have to
travel five times as far (the thermal gradient would be five times
smaller), and the surface area is also five times smaller.  This cuts
the power to only about 1.5 MW:

    You have: 4 * 380 W/m/K * 1000 K * 1 cm^3 / (1 mm)^2
    You want: MW
            * 1.52
            / 0.65789474

But the crucial factor that determines how soon we can switch between
“beds” is how fast we can cool down the copper.  Its specific heat at
room temperature is [24.440 J/mol/K][9] at 63.546 g/mol, which works
out to .38460 J/g/K, but its high density of 8.96 g/cc means that’s
actually 3.45 J/cc/K, almost as good as water (which can’t be heated
to 1000+° in solid form).  So this 1.5 MW divided by 3.45 J/K for our
1 cc gives us an initial cooling rate of 441000°/s, so cooling from
1000° takes on the order of a few milliseconds:

    You have: 1.52 MW / (3.45 J/K)
    You want: K/s
            * 440579.71
            / 2.2697368e-06

If we had some material with the same specific heat but much higher
density, or the same density but much higher specific heat, it would
take much longer, just as if we had much lower conductivity.  However,
[the volumetric heat capacity of solids is nearly constant][12] at
around 2.1–3.8 J/cc/K, while their conductivity varies enormously.

So that’s the sense in which thermal diffusivity is the relevant
figure of merit for choosing a heat storage medium.

The physical shape of the thermal storage medium is a constant factor.
Rods have twice the area per volume that plates do, and chunks such as
cubes or spheres have roughly three times; moreover, more of the
material is close to the surface.  So using chunks is like using
plates that are about .6 times as thick.  The more important question
is probably how this affects fluid flow.

[12]: https://en.wikipedia.org/wiki/Volumetric_heat_capacity#Volumetric_heat_capacity_of_solids

If we decrease the thickness too much, I think the heat exchanger
won’t be robust to rapid fluid flow; moreover, we also have to
decrease channel size, and then the viscosity of the fluid will
restrict heat transfer.  A possible way around this is to use a pebble
bed in which the thermal storage medium proper is coated onto the
surface of a “thermal-storage medium support material” in order to
increase its area, like the humans do with catalysts.  Ideally the
support material would have no volumetric heat capacity, but making it
out of something with low thermal conductivity might be almost as
good.  I thought maybe you could foam its interior to further reduce
its thermal mass and conductivity, but sealing that foam against the
ingress of the process gas seems problematic, particularly in light of
the intense thermal cycling.

Fused silica would be a good candidate: [its thermal conductivity is
about 1.4–3.3 W/m/K][10] over the relevant range, and [its super-low
thermal expansivity of 0.55 ppm/K][13] would be helpful to prevent
cracking.  Other alternatives might include [wollastonite][14]
(0.8 W/m/K), zirconia (1.9 W/m/K), porcelain (2 W/m/K), or stainless
steel (17 W/m/K).  Reasonably conductive refractory substances for the
surface might include copper (380 W/m/K as mentioned above), nickel
(90 W/m/K), sapphire (30 W/m/K), diamond (1000 W/m/K), [carborundum
(120 W/m/K)][16], graphite, tungsten, or gold (300 W/m/K, [melts at
1064.18°][15]).  Graphite, diamond, and tungsten are especially
vulnerable to oxidation, though.

[13]: https://en.wikipedia.org/wiki/Fused_quartz#Properties_of_fused_quartz
[14]: https://www.makeitfrom.com/material-properties/Wollastonite
[15]: https://en.wikipedia.org/wiki/Gold
[16]: https://www.imetra.com/silicon-carbide-material-properties/

(Rather than a *pebble* bed, the best shape might be something like
the parallel channels in the catalytic converter off your Ford.)

This approach would allow you to use a layer of thermal-reservoir
material that would otherwise be impractically thin.  The support
material on the other side of it represents a significant
inefficiency.

This thin, wide layer would also increase the surface area for gas
contact, reducing the problem of the thermal resistance of the gas
boundary layer.  You might also be able to reduce the thermal
resistance of the fluid by [nucleate boiling][17] or compression;
compressed gases have much higher thermal conductivity, and nucleate
boiling provides incredibly fast heat transfer.  Compression is also
potentially useful in preventing nucleate boiling from erupting into
[film boiling][18], which dramatically slows the heat transfer.

[17]: https://en.wikipedia.org/wiki/Nucleate_boiling
[18]: https://en.wikipedia.org/wiki/Leidenfrost_effect

If you could really get several megawatts of heat exchange into a
cubic centimeter just by pushing the frequency of switching streams up
into the kilohertz, that would be a pretty amazing achievement.  A
[rotary regenerator][19] that alternates between hot and cold gas
streams several times per cycle might be a practical way to achieve
this.

[19]: https://en.wikipedia.org/wiki/Thermal_wheel
