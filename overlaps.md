(Annotated, edited transcript of dialogue with IPython.)

A friend of mine was given a job interview question of computing the
set of available hotel rooms for a proposed stay.  Some existing rooms
may already be booked up for other guests’ stays that overlap the
proposed stay, so you want to compute which rooms are available, if
any.

Basic linear-time solution
--------------------------

Here’s the first basic solution I showed:

    In [53]: from collections import namedtuple

    In [45]: Interval = namedtuple('Interval', ('start', 'end'))

    In [46]: res1 = Interval(1, 6)

    In [40]: def overlaps(interval, interval_i):
        ...:     return not (interval.end <= interval_i.start or interval_i.end <= interval.start)
        ...: 

    In [42]: overlaps(Interval(5, 7), Interval(1, 6))
    Out[42]: True

    In [49]: def available(interval, rooms, previous):
        ...:     return {room for room in rooms if not any(overlaps(interval, interval_i) for interval_i in previous[room])}
        ...: 

Some people might find that hard to read, so here I have it
hand-compiled into lower-level code:

    In [50]: def available_java(interval, rooms, previous):
        ...:     result = set()
        ...:     for room in rooms:
        ...:         rejected = False
        ...:         for interval_i in previous[room]:
        ...:             if overlaps(interval, interval_i):
        ...:                 rejected = True
        ...:                 break
        ...:         if not rejected:
        ...:             result.add(room)
        ...:     return result
        ...: 

Both of these work on at least the simplest test cases:

    In [43]: available(Interval(6, 7), {'1A', '1B'}, {'1A': [res1], '1B': []})
    Out[43]: {'1A', '1B'}

    In [44]: available(Interval(5, 7), {'1A', '1B'}, {'1A': [res1], '1B': []})
    Out[44]: {'1B'}

    In [51]: available_java(Interval(5, 7), {'1A', '1B'}, {'1A': [res1], '1B': []})
    Out[51]: {'1B'}

    In [52]: available_java(Interval(6, 7), {'1A', '1B'}, {'1A': [res1], '1B': []})
    Out[52]: {'1A', '1B'}

Their initial approach to the solution was to maintain the database as
a list of intervals annotated with room numbers; working from that
representation, you can solve the problem like this by using a hash
table to keep track of which rooms had been rejected:

    In [47]: def available2(interval, rooms, previous):
        ...:     return set(rooms) - {room for interval_i, room in previous if overlaps(interval, interval_i)}
        ...: 

Here it is, hand-compiled assembly-language-style:

    In [54]: def available2_java(interval, rooms, previous):
        ...:     room_set = set()
        ...:     for room in rooms:
        ...:         room_set.add(room)
        ...:     for interval_i, room in previous:
        ...:         if overlaps(interval, interval_i) and room in room_set:
        ...:             room_set.remove(room)
        ...:     return room_set
        ...: 

    In [55]: available2_java(Interval(6, 7), {'1A', '1B'}, [(res1, '1A')])
    Out[55]: {'1A', '1B'}

    In [56]: available2_java(Interval(5, 7), {'1A', '1B'}, [(res1, '1A')])
    Out[56]: {'1B'}

Technically `available2` above reifies two separate sets in memory
(the candidate rooms and the rooms to eliminate), then computes the
set difference, while `available2_java` is a little bit different
because it incrementally computed the set difference instead of
building three separate sets.  Hopefully it’s understandable anyway.

Worse-than-linear-time solution
-------------------------------

Both of those approaches take linear time (|rooms| + |previous|), at
least if we make the standard assumption that hash table insertion and
lookup are amortized constant time.  The following approach is both
more complex and also slower (|rooms| × |previous|), but it does avoid
needing any hash tables:

    In [48]: def available3(interval, rooms, previous):
        ...:     result = []
        ...:     for room in rooms:
        ...:         if not any(overlaps(interval, interval_i) for interval_i, room_i in previous if room == room_i):
        ...:             result.append(room)
        ...:     return result

Of course it’s good to make things work before you make them more
complex in order to make them more efficient, but in this case, this
solution is not just much slower but also more complex.

More efficient solutions
------------------------

So that’s the basic solution covered, but it takes time linear in the
database size, inspecting literally every reservation in the database
for the relevant rooms, no matter how many years ago or in the future
the reservation might be, or how short the desired reservation period.
There are more efficient data structures for this, such as interval
trees.  If you wanted to solve this problem in real life, you’d
probably reuse an existing solution; Postgres supports [GiST indices
which can evaluate these queries efficiently][0], even though it’s
more commonly used for geospatial indexing.

[0]: https://stackoverflow.com/a/68634567

However, an easier thing to implement, which is a big speedup when
your database is large, is just a hash table of (room, day) pairs.
Let’s look again at the structure I’ve been using above for the
reservations, a map from rooms to interval lists:

    In [36]: res = {'1A': [res1], '1B': [], '1C': [Interval(1, 2), Interval(7, 8)]}

    In [37]: res
    Out[37]: 
    {'1A': [Interval(start=1, end=6)],
     '1B': [],
     '1C': [Interval(start=1, end=2), Interval(start=7, end=8)]}

We can transform this into a (room, day) set as follows:

    In [25]: def taken_days(reservations):
        ...:     return {(room, day) for room in reservations.keys()
        ...:                         for interval in reservations[room]
        ...:                         for day in range(interval.start, interval.end)}
        ...: 

    In [26]: taken_days(res)
    Out[26]: {('1A', 1), ('1A', 2), ('1A', 3), ('1A', 4), ('1A', 5), ('1C', 1), ('1C', 7)}

Then, we can answer our original query in a probably more efficient
way as follows:

    In [38]: def available_eff(interval, rooms, taken):
        ...:     return {room for room in rooms if not any((room, day) in taken
        ...:                                               for day in range(interval.start, interval.end))}
        ...:

Here, if we have 100 rooms with 200 reservations each, to book a 3-day
stay, we just probe the hash table 300 times or less.  The previous
solution would have looked at all 20000 reservations, which is
probably slower.  This works too, although of course with only three
reservations in our database, we can’t show that it’s faster:

    In [28]: available_eff(Interval(5, 7), {'1A', '1B', '1C'}, taken_days(res))
    Out[28]: {'1B', '1C'}

    In [29]: available_eff(Interval(5, 8), {'1A', '1B', '1C'}, taken_days(res))
    Out[29]: {'1B'}

    In [30]: available_eff(Interval(6, 8), {'1A', '1B', '1C'}, taken_days(res))
    Out[30]: {'1A', '1B'}

Here’s a version hand-compiled down to imperative loops:

    In [32]: def available_eff_java(interval, rooms, taken):
        ...:     result = set()
        ...:     for room in rooms:
        ...:         rejected = False
        ...:         for day in range(interval.start, interval.end):
        ...:             if (room, day) in taken:
        ...:                 rejected = True
        ...:                 break
        ...:         if not rejected:
        ...:             result.add(room)
        ...:     return result
        ...: 

    In [33]: available_eff_java(Interval(5, 8), {'1A', '1B', '1C'}, taken_days(res))
    Out[33]: {'1B'}

    In [34]: available_eff_java(Interval(6, 8), {'1A', '1B', '1C'}, taken_days(res))
    Out[34]: {'1A', '1B'}

    In [35]: available_eff_java(Interval(6, 7), {'1A', '1B', '1C'}, taken_days(res))
    Out[35]: {'1A', '1B', '1C'}

This solution still takes “linear time”, but now it’s linear in the
number of days to search instead of the size of the database.  (Its
execution time is actually jointly proportional to the number of days
and the number of rooms to search, and “linear time” is sort of a
false implication because a small input like Interval(1, 1000000) can
provoke a large execution time because it describes a large number of
days, while normally a “linear-time algorithm” is linear *in the size
of the input*.)

It’s also still slow if you have a lot of rooms, which is probably a
lot more common than having a lot of days, so it’s worth looking at
how to optimize that case.

In Java or Golang you could represent the set of (room, day) tuples as
a map from a user-defined room-day-pair type to boolean, or probably
more conveniently as a map from rooms to maps from days to booleans,
or equivalently as a map from days to maps from rooms to booleans.
And this last representation suggests a way to eliminate iterating
over all the full rooms: just keep the available rooms in the map.

In Python that looks like this (apologies for the lack of blank lines;
the Python REPL is not forgiving of blank lines):

    In [40]: class Booker:
        ...:     def __init__(self, rooms):
        ...:         self.rooms = rooms
        ...:         self.vacancy_map = {}
        ...:     def vacancies(self, day):
        ...:         return set(self.rooms) if day not in self.vacancy_map else self.vacancy_map[day]
        ...:     def book(self, room, day):
        ...:         if day not in self.vacancy_map:
        ...:             self.vacancy_map[day] = set(self.rooms)
        ...:         self.vacancy_map[day].remove(room)
        ...:     def available(self, stay):
        ...:         result = self.vacancies(stay.start)
        ...:         for day in range(stay.start + 1, stay.end):
        ...:             result = result & self.vacancies(day)  # set intersection operation
        ...:         return result
        ...: 

    In [41]: hotel = Booker({'1A', '1B', '1C'})

    In [42]: for i in range(1, 6): hotel.book('1A', i)

    In [43]: hotel.book('1C', 1)

    In [44]: hotel.book('1C', 7)

    In [48]: hotel.vacancy_map
    Out[48]: 
    {1: {'1B'},
     2: {'1B', '1C'},
     3: {'1B', '1C'},
     4: {'1B', '1C'},
     5: {'1B'},
     7: {'1A', '1B'}}

    In [45]: hotel.available(Interval(5, 8))
    Out[45]: {'1B'}

    In [46]: hotel.available(Interval(6, 8))
    Out[46]: {'1A', '1B'}

    In [47]: hotel.available(Interval(6, 7))
    Out[47]: {'1A', '1B', '1C'}

This will still iterate over all your rooms if you query a day that
you don’t have anything booked (the iteration is hidden inside the `&`
operator), but that’s arguably less harmful, because you would have to
return all of them anyway if the query is a one-day stay.  Once the
set of candidate rooms has gotten small, the CPython implementation of
`&` will only iterate over that small set for each new day, rather
than the usually much larger set of all possible rooms like
`available_eff`.

GiST indices can sometimes do a better job optimizing multicolumn
queries like this when necessary, and of course you can think up data
structures to optimize them yourself.

About logic
-----------

This problem is fairly easy to solve if you can formalize the problem
statement logically, as in the Python set comprehensions and generator
expressions like `{room for room in rooms if not
any(overlaps(interval, interval_i) for interval_i in
previous[room])}`.  That’s pretty much just a logical formalization of
the problem you’re trying to solve.  (I still managed to keep screwing
it up for several minutes because I’d gotten the definition of
`overlaps` wrong, possibly because I was running on five hours of
sleep.)

Some programmers might think that that isn't a useful skill because
they have to program in languages that don’t have such high-level
constructs, and they find the formalization hard to understand.  I
think that those are precisely the circumstances where learning to use
logic is the most important, because it will greatly reduce the
disadvantages you are struggling against when you try to do things:

- By thinking in terms of logical high-level concepts such as “the set
  of rooms where there is not any overlap” instead of step-by-step
  imperative transformations, you can figure out whether a solution
  approach will work, and even compare the possible efficiencies of
  different solutions, before you start translating it into the
  low-level code you have to use in Java or Golang.
- The translation into lower-level code is purely mechanical, so even
  if you’re doing it by hand, it goes very quickly, as I think you
  observed.
- By recognizing those high-level concepts in other people’s low-level
  code, you are trying to maintain, you will be able to understand
  what it is doing more rapidly, and then you will be able to modify
  it more easily.
- When you’re refactoring or optimizing a piece of code, recognizing
  those high-level concepts will enable you to tell whether your
  changes have successfully preserved its functionality.

So learning to express such problems in logical terms like this will
enable you to spend your limited finger-typing time on
better-thought-out code, it will speed up the finger-typing, it will
speed up the process of understanding an existing codebase, and it
will speed up the processes of refactoring and optimization.  And all
of that is more true, not less so, if you’re programming in a language
that doesn’t support things like set data types, tuples, and dict
comprehensions directly.

Formulating problems logically isn’t easy.  It takes a lot of
practice, and it doesn’t come naturally.  But you already have a lot
of experience laying the groundwork for it, so it’ll be easier for you
than for most people.  First, though, you’ll have to decide that it’s
worthwhile (even if people don’t always keep their comments up to
date).  I think it’s one of the things that will help you most.

Testing
-------

I wasted several minutes with a broken version of the `overlaps`
function.  I thought I’d see if Hypothesis could have saved me from
that, so I wrote this test:

    from hypothesis import given
    from hypothesis.strategies import integers, tuples, composite


    def includes(interval, point):
        start, end = interval
        return start <= point < end

    def overlaps(a, b):
        sa, ea = a
        sb, eb = b
        return not (ea <= sb or eb <= sa)

    def nonempty(interval):
        s, e = interval
        return e > s


    points = integers(min_value=0, max_value=100)

    @composite
    def intervals(draw):
        "A Hypothesis strategy to generate a nonempty interval."
        a = draw(points)
        b = draw(points)
        return (a, b) if a < b else (b, a) if b < a else (a, a+1)

    @given(intervals(), intervals(), points)
    def test_overlaps(a, b, p):
        assert nonempty(a)
        assert nonempty(b)
        #open('/dev/tty', 'w').write(f'{a} {b} {p}\n')

        if overlaps(a, b):
            # If they overlap, they should have *some* point in common.  I
            # claim that at least one of their start points should be such
            # a point.
            assert includes(a, b[0]) or includes(b, a[0])
        else:
            assert not (includes(a, p) and includes(b, p))

This does immediately find the bug, but writing the test, and
especially revising it into this shape, took me much longer than
writing the function in the first place.  Maybe that’s because I’m out
of practice with Hypothesis.
