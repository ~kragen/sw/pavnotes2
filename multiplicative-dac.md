R-2R DACs and resistor-ladder DACs are two ways
to convert a digital count into an analog
voltage, one using a maximum number of resistors
and one using a minimum (logarithmic) number.
It occurred to me that if your input digital
code could be read from a lookup table instead
of literally being a binary number, you could
structure it differently.  A resistor ladder is
a multiplicative DAC, and it occurred to me that
you could cascade several of them to get an
output that is the *product* of their values.
Maybe this is interesting because it gives you a
better tradeoff between component count and
nonlinearity.

Cascading two resistor ladders might be as
simple as a single pass transistor to connect
their power rails together.  I’m realizing I
don’t feel confident enough about transistors to
be sure I know how this will work, but the idea
is that by default the second resistor ladder is
powered from the lowest tap on the first one, so
their multiplicative factors multiply.  If the
first one divides to ⅓ and the second one
divides to ½, you get ⅙ output.  But with a
transistor switch connecting the power rail of
the second one to the power rail of the first
one, the first divider is inoperative, and you
get ½ output, or basically that minus a Vce
voltage drop.  This also runs extra current
through the bottom resistor of the first ladder,
but that doesn’t matter.

By composing, for example, ½, ⅓, and ⅘, you can
get 2/15, 1/6, 4/15, 1/3, 2/5, 1/2, 4/5, and 1
as outputs.  This is not a super even
distribution (there’s a gap of 3/10 from 1/2 to
4/5) but it’s not that terrible for 3 bits.  In
general this approach will need to use more bits
to drive the DAC than you get in precision.

I don’t think this would suffer too much from
temperature problems because Vce is pretty
stable and the equivalent is basically zero for
MOSFETs.

You can offer more than one tap per divider, at
a slight penalty in number of bits required.
And you can short out portions of a divider
(with a transistor switch) to change its
denominator; this might permit using even less
resistors than an R-2R ladder, like one resistor
per bit instead of two.  This could be useful
for on-chip implementations where resistors are
area-hungry.
