Suppose you make an ECM electrode in the shape of a hollow solid of
rotation, or rather, a segment thereof, cut from the whole along a
couple of radial planes.  You could, for example, revolve a U shape
around the top of the U.  Perhaps the radius of the U is 200 microns,
so it’s 400 microns across, but the thickness of the metal is only,
for example, 10 microns, like household aluminum foil.  By rotating
this shape around the center of revolution, you can sweep out the
whole solid of rotation that it’s a segment of.  Only the U-shaped
leading edge of the metal is exposed; the rest of it is covered with a
thin insulator.

This allows you to scoop out a chunk of metal from a workpiece,
dissolving it along the U-shaped edge but leaving the chunk inside the
hollow solid untouched, until you finally detach the chip from the
underlying workpiece.  If your kerf width is 20 microns (you can
jitter the position of the electrode around to increase the kerf
width) you are removing a 400-micron-sized chunk of metal for the
energy consumption of removing 40 microns of kerf.  So you should be
able to increase ECM’s dismal energy efficiency and already-impressive
material removal rate by about an order of magnitude.

The limiting factor on how thin the U can be made is probably its
rigidity; it will deform as the electrolyte is circulated through it.
If you put a rib on the inside of the U (making it a sort of ϵ shape)
you can increase its rigidity along one axis.  You can include
multiple ribs, and you can use them as tiny pipes for delivering fresh
electrolyte to the working face.
