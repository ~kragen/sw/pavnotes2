I was thinking again about the tone detection problem for small
microcontrollers, and looking at spectrograms of my own whistles.
Commonly they are 10dB or more above the background noise, with a
fairly narrow bandwidth, a Q of 20 or higher, in the frequency range
500 to 3500 Hz, and no harmonics.  These characteristics are pretty
distinctive. 

One typical test had a whistle at 1463 Hz at -39dB with its -3dB
points at about 1460 and 1465 Hz, which I think suggests a Q of
several hundred.  The noise floor at that moment was -90dB, and the
peak at 2925Hz was at about -76dB, 43dB below the fundamental.  At
4390Hz there’s what might be a detectable peak.  I suspect my Q here
is limited by the 8192-sample FFT I’m doing, which can only measure
frequency bin by bin, and 44100Hz/8192 is about 6 Hz.

If you know the frequency it’s pretty trivial to detect; you can use a
CIC/Hogenauer filter to downsample the signal to four times the
frequency of the whistle, then subtract alternating samples of the
interleaved I and Q signals to get amplitude and phase data.  This
approach is multiplication-free, requiring three or four additions per
input sample and then a few subtractions per cycle at the target
frequency, and only a few bytes of buffer memory.  Even with an
approximate frequency you can use this approach over short enough
windows to track the phase shifts from one window to the next.  Then
you also want to measure the second harmonic in order to reject
non-whistling sounds in the same frequency range.

But when you don’t know the frequency, Kenneth, it would be nice to be
able to find it without an 8192-sample FFT.  A small microcontroller
might struggle with the number of multiply-accumulates.

I feel like a phase vocoder is a fairly easy answer to this problem.
It allows you to use a fairly small FFT, since you’re just trying to
separate your desired tone, which is far more powerful than any other
signal anywhere nearby in frequency, from faraway noise, and measure
its frequency.

So you could do, for example, a 32-bucket FFT on a signal downsampled
to 8ksps.  This requires, I think, 10 multiply-accumulates per sample,
and thus 80000 per second.  This should be feasible even without a
multiplier.

But actually you don’t even need that.  The approach I described
earlier gives you an I and Q signal for a given frequency band, a
center frequency and lowpass characteristic determined by the 4*f* you
have downsampled to, and the Q factor determined by the number of
oscillations your window covers.  I and Q signals for a frequency band
for a sequence of windows over time is all you need for a phase
vocoder to detect a frequency by unwrapping the phase and measuring
the phase drift to get a frequency offset from the frequency the I and
Q signals are referenced to.
