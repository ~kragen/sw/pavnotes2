I thought I’d check out the prices for locally available retail
sources of phosphate for refractory cements on MercadoLibre.  It’s
02023-08-26 and the dollar is at $720 × 730.

Phosphate pricing
-----------------

    | Material | kg | pesos |   US$ | Formula    | % PO₄ | $/kg PO₄ | US$/kg PO₄ |
    |----------+----+-------+-------+------------+-------+----------+------------|
    | acid 0   |  5 | 28277 | 39.00 | H₃PO₄ 85%  |  82.4 |  6863.35 |       9.47 |
    | dap 1    | 10 |  6980 |  9.63 | (NH₄)₂HPO₄ |  71.9 |   970.79 |       1.34 |
    | dap 2    | 25 | 18376 | 25.35 | (NH₄)₂HPO₄ |  71.9 |  1022.31 |       1.41 |
    | mkp 3    |  1 |  8530 | 11.77 | KH₂PO₄     |  69.8 | 12220.63 |      16.86 |
    | dap 4    |  1 |  1800 |  2.48 | (NH₄)₂HPO₄ |  71.9 |  2503.48 |       3.45 |
    | dap 5    | 10 | 13083 | 18.05 | (NH₄)₂HPO₄ |  71.9 |  1819.61 |       2.51 |
    | map 6    |  5 | 11578 | 15.97 | NH₄H₂PO₄   |  82.6 |  2803.39 |       3.87 |
    | map 7    | 25 | 58035 | 80.05 | NH₄H₂PO₄   |  82.6 |  2810.41 |       3.88 |
    | map 8    | 25 | 62310 | 85.94 | NH₄H₂PO₄   |  82.6 |  3017.43 |       4.16 |
    | tsp 9    |  1 |  2000 |  2.76 | Na₃PO₄     |  57.9 |  3454.23 |       4.76 |
    | tsp 10   |  5 | 23048 | 31.79 | Na₃PO₄     |  57.9 |  7961.31 |      10.98 |
    | tcp 11   |  1 |  7073 |  9.76 | Ca₃(PO₄)₂  |  61.2 | 11557.19 |      15.94 |
    | stpp 12  |  5 | 23639 | 32.61 | Na₅P₃O₁₀   |  77.5 |  6100.39 |       8.41 |
    | acid 13  |  8 | 39892 | 55.02 | H₃PO₄ 85%  |  82.4 |  6051.58 |       8.35 |
    #+TBLFM: $7=$3/($6/100)/$2;%.2f::$8=$7/725;%.2f::$4=$3/725;%.2f

* 0: [85% phosphoric acid][0], food grade, for lowering pH in brewing
  beer, Breaking Lab brand
* 1: [diammonium phosphate][1], granulated fertilizer, Eurochem brand,
  sold by El Fortín Agropecuaria
* 2: [diammonium phosphate][2], granulated fertilizer, Eurochem brand,
  sold by El Trebol Agropecuaria
* 3: [monopotassium phosphate][3], granulated fertilizer, Innophos
  brand, sold by Planeta Verde
* 4: [diammonium phosphate][4], granulated fertilizer, Bertinat brand,
  sold by Emprendimientos LyL
* 5: [diammonium phosphate][5], granulated fertilizer, Profertil
  brand, sold by Planeta Verde
* 6: [monoammonium phosphate][6], granulated fertilizer, no brand,
  sold by Planeta Verde
* 7: [monoammonium phosphate][7], granulated fertilizer, no brand,
  sold by Planeta Verde, but in a bigger bag
* 8: [monoammonium phosphate][8], granulated fertilizer, no brand,
  sold by El Fortín Agropecuaria
* 9: [trisodium phosphate][9], cleaning supply, no brand, sold by
  Full-Black SRL in Hurlingham
* 10: [trisodium phosphate][11], cleaning supply or detergent
  ingredient, Cotton Fields brand, sold by [Laboratorios Cotton
  Fields][10] or Química Cotton Fields in Wilde
* 11: [tricalcium phosphate][12], for making bath salts or baking
  powder or toothpaste, Bestchem brand, sold by [Laboratorios Cotton
  Fields][10] in Wilde
* 12: [sodium tripolyphosphate][13], for making detergents and
  cosmetics, Química Cotton brand, sold by [Laboratorios Cotton
  Fields][10].  This doesn’t actually contain the phosphate ion but
  rather the triphosphate ion.
* [85% phosphoric acid][14], food grade, Cotton Fields brand

[8]: https://articulo.mercadolibre.com.ar/MLA-881351254-fertilizante-fosfato-monoamonico-soluble-x-25-kg-_JM
[14]: https://articulo.mercadolibre.com.ar/MLA-1122967214-acido-fosforico-85-8-kg-quimica-cotton-fields--_JM
[13]: https://articulo.mercadolibre.com.ar/MLA-1365902779-tripolifosfato-de-sodio-x-5-kg-calidad-premium-_JM
[12]: https://articulo.mercadolibre.com.ar/MLA-1168306778-fosfato-tricalcico-x-1-kg-_JM
[9]: https://articulo.mercadolibre.com.ar/MLA-788648580-fosfato-trisodico-_JM#is_advertising=true&ad_domain=VQCATCORE_LST&ad_position=10&ad_click_id=ZDU4YjhiNTgtYWI0Zi00N2UzLTg1ZjItMGE1NmFmZDViOWUy
[11]: https://articulo.mercadolibre.com.ar/MLA-1144064494-fosfato-trisodico-x-5-kg-calidad-premium-_JM?variation=
[10]: https://listado.mercadolibre.com.ar/_CustId_59244227?item_id=MLA1144064494&category_id=MLA436099&seller_id=59244227&client=recoview-selleritems&recos_listing=true
[0]: https://articulo.mercadolibre.com.ar/MLA-1135930886-acido-fosforico-85-5-kg-cerveza-artesanal-corrector-de-ph-_JM
[1]: https://articulo.mercadolibre.com.ar/MLA-879921162-fertilizante-iniciador-fosfato-diamonico-dap-x-10-kg-_JM
[2]: https://articulo.mercadolibre.com.ar/MLA-1102690228-fosfato-diamonico-fertilizante-bolsa-x-25-kgs-_JM#is_advertising=true&ad_domain=VQCATCORE_LST&ad_position=20&ad_click_id=MDQwYTQ0MDEtNzQ3Ni00ZTQ0LTllYmQtMzI0ZGExODBkZTMy
[3]: https://articulo.mercadolibre.com.ar/MLA-1316259060-fosfato-monopotasico-soluble-x-1-kg-_JM
[4]: https://articulo.mercadolibre.com.ar/MLA-788997362-fosfato-diamonico-bertinat-x-1-kg-_JM
[5]: https://articulo.mercadolibre.com.ar/MLA-1316291166-fosfato-diamonico-x-10-kgs-profertil-_JM
[6]: https://articulo.mercadolibre.com.ar/MLA-1372432877-fosfato-monoamonico-5kg-granulado-_JM
[7]: https://articulo.mercadolibre.com.ar/MLA-841684337-fosfato-monoamonico-granulado-bolsa-x-25kg-envio-_JM

I couldn’t find any monosodium phosphate, dipotassium phosphate, or
superphosphate.

An interesting thing about this is that, contrary to what Wagh’s book
reported, phosphoric acid was not the cheapest form of phosphate.  The
two phosphoric acids on the list are US$8.35 and US$9.47 per kg of
phosphate.  Possibly this is because they are food-grade.  The (quite
impure) diammonium phosphate fertilizer is only about 20% as
expensive, but even the monoammonium phosphates (advertised with
photos that make them look quite pure) are less than half the cost.

Cation pricing
--------------

To combine with the phosphate to make a ceramic, you need polyvalent
cations; Wagh says magnesium, ferrous, aluminum, zinc, and calcium
cations have been used successfully, though some are more difficult
than others.

### Magnesium ###

I’m not sure how to buy magnesium; perhaps in metallic form as a
hot-water-tank anode?

    | item    | length | diameter |       | pesos/ | pesos/ |  US$/ |
    |         |   (cm) |     (cm) | pesos |     cc |     kg |    kg |
    |---------+--------+----------+-------+--------+--------+-------|
    | baldo 0 |    120 |      1.8 | 16878 |  55.27 |  31801 | 43.86 |
    | basic 1 |     66 |      1.9 | 10440 |  55.79 |  32100 | 44.28 |
    | plata 2 |    100 |     1.95 | 12999 |  43.53 |  25046 | 34.55 |
    #+TBLFM: $5=4*$4/$2/3.14159/$3/$3;%.2f::$6=$5*1000/1.738;%.0f::$7=$6/725;%.2f

* 0: [a 120cm-long 18mm-diameter anode][16], PROGAR brand, sold by
  Baldo.ref in the capital.
* 1: [a 66cm anode][17], sold by Home Basic
* 2: [a 100cm anode][18] sold by Anodos del Plata

[16]: https://articulo.mercadolibre.com.ar/MLA-1161018698-anodo-magnesio-anticorrosivo-termotanque-120cm-_JM
[18]: https://articulo.mercadolibre.com.ar/MLA-1140636898-anodo-barra-magnesio-para-termotanque-100cm-10000mm-_JM
[17]: https://articulo.mercadolibre.com.ar/MLA-1145464178-anodo-barra-de-magnesio-66cm-para-termotanque-original-_JM

These are feasible, and might be worth it for other purposes that
demand metallic magnesium, but seem a bit pricey.  Maybe magnesium is
cheaper in an oxidized form?

[Natural Whey sells magnesium hydroxide as a nutritional
supplement][19] for AR$16328/kg.  Mg(OH)₂ is 41.7% magnesium, so
that’s AR$39200 per kg of magnesium, which is US$54/kg Mg.  That’s
actually slightly *more* expensive.  [Tatiana Tortolani sells 250g of
magnesium carbonate][20] for the same purpose for AR$3660, which is
AR$14640/kg, and MgCO₃ is 28.8% magnesium, so that’s worse.  But you
can get magnesium chloride for [AR$2599/kg from Breaking Lab][21] or
[AR$2042/kg from Natural Whey][22], and anhydrous MgCl₂ is 25.5% Mg,
so that’s AR$8000/kg Mg or US$11/kg Mg.  So magnesium chloride is
likely the winner so far.  But [it forms hydrates][24] up to the
dodecahydrate; if what’s being sold is the dodecahydrate, that’s only
7.8% magnesium, which would make it US$36/kg Mg.

[19]: https://articulo.mercadolibre.com.ar/MLA-1164930906-hidroxido-de-magnesio-en-polvo-leche-magnesia-1-kg-vip-_JM
[24]: https://en.wikipedia.org/wiki/Magnesium_chloride
[22]: https://www.mercadolibre.com.ar/cloruro-de-mg-puro-de-israel-dead-sea-works-1-kilo-alb/p/MLA22198563
[21]: https://www.mercadolibre.com.ar/cloruro-de-magnesio-importado-x-1-kilo/p/MLA20080435
[20]: https://www.mercadolibre.com.ar/carbonato-de-magnesio-puro-usp-250-gr-liviano-4-sabor-caracteristico/p/MLA25938618

In addition to the hydroxide, [you can get magnesium oxide as a
supplement][34] for AR$10656/kg from Natural Whey, which works out to
AR$18000/kg Mg at 60.3%, US$24/kg Mg.

[34]: https://articulo.mercadolibre.com.ar/MLA-1165077489-oxido-de-magnesio-grado-usp-999-apto-consumo-1kg-_JM

This is ridiculous.  [Magnesium is the eighth most abundant element,
making up 2% of the Earth’s crust][23], almost 30 million tonnes of
magnesite are mined per year, and [wholesale caustic magnesia cost
US$259/tonne five years ago][25], which is 25¢/kg, or 40¢/kg Mg,
though dead-burned magnesia was more like US$1/kg or US$1.70/kg Mg.
Magnesium is “mainly used as refractory material in furnace linings”,
and the chloride is commonly spread on roads for deicing and dust
control (because it’s sticky), and much of it is also used for water
treatment, stack gas scrubbing, and fertilizer.  “Magboard” is a
cheaper alternative to drywall made from caustic magnesia instead of
alabaster.

[23]: https://www.usgs.gov/centers/national-minerals-information-center/magnesium-compounds-statistics-and-information
[25]: https://pubs.usgs.gov/myb/vol1/2018/myb1-2018-magnesium-comp.pdf

Hmm, [magnesium sulfate fertilizer][26] imported from Germany costs
AR$30396 for *25* kg from Planeta Verde, which sounds more promising.
But this fertilizer is rated at only 16.5% magnesia (MgO), which means
only 9.9% magnesium, so AR$1215/kg works out to US$17/kg Mg.
[Presumably it’s epsomite, the heptahydrate][27].  This is better than
US$36/kg Mg but still not in the neighborhood of 40¢.  [Other
fertilizer vendors list it at more than twice the price][31], perhaps
due to a lack of domestic production.  [Breaking Lab sells epsom salt
for bath salts for AR$17299 for 10 kg][28], which is a little worse.

[26]: https://articulo.mercadolibre.com.ar/MLA-1151563727-fertilizante-sulfato-de-magnesio-epso-top-compo-alemania-25k-_JM
[27]: https://en.wikipedia.org/wiki/Magnesium_sulfate
[28]: https://articulo.mercadolibre.com.ar/MLA-926376024-sales-de-epsom-sulfato-de-magnesio-puro-999-10-kilos-usp-_JM
[31]: https://articulo.mercadolibre.com.ar/MLA-1271757841-sulfato-de-magnesio-bolsa-de-25-kilos-_JM

[Dolomite is sold as a fertilizer][29] for AR$1880/kg, [which is
CaMg(CO₃)₂][30], making it 13% magnesium, AR$14300/kg Mg, US$20/kg Mg.
This is more expensive as a source of magnesium than the epsom-salt
fertilizer, and also it comes with an equal amount calcium which you
might want to separate.  On the plus side, the calcium is relatively
insoluble, which might favor the slow reactions needed to form
phosphate ceramics, so it’s more like US$7 per kg of polyvalent
cations.

Experiments at the College of Materials Science and Engineering at
Chongqing University in 02020 seem to show that this is sort of
feasible; they calcined the dolomite with quartz, or in another
experiment bauxite and gypsum, to further bind the calcium into
respectively calcium silicates or calcium sulfoalminate (ye’elimite).

[29]: https://articulo.mercadolibre.com.ar/MLA-1177774856-dolomita-regulador-ph-fertilizante-500-gr-horus-grow-_JM
[30]: https://en.wikipedia.org/wiki/Dolomite_(mineral)

As sporting equipment it’s even more expensive: [AR$19000/kg][32] or
[AR$13500/kg][33] for magnesium carbonate chalk.

[32]: https://articulo.mercadolibre.com.ar/MLA-1124105673-carbonato-de-magnesio-1-kg-en-polvo-crossfit-gimnasio-_JM
[33]: https://articulo.mercadolibre.com.ar/MLA-830632455-carbonato-de-magnesio-polvo-por-kg-full-fitness-solutions-_JM

I strongly suspect there are foundry-supply houses here in Argentina
that will sell you a tonne of magnesia for closer to US$2/kg Mg than
US$20/kg Mg, but maybe they don’t list all their products on
MercadoLibre.

### Iron ###

Wagh claims that ferric (trivalent iron) has not really been used with
success (perhaps because ferric phosphate is significantly
water-soluble; it’s used as a molluscicide), but that magnetite and
wüstite have been used; wüstite is a bit rare, but he made his by
reducing hematite with elemental iron.  He suggests that
Golynko–Wolfson et al. had partly reduced their hematite to magnetite
by calcining it at 600° in order to get a ceramic by reacting it with
phosphoric acid.  The binders formed are initially Fe(H₂PO₄)₂ and
later FeHPO₄.

### Calcium ###

For calcium, he says the best options are calcium silicate (especially
wollastonite) and [monocalcium aluminate][15].  But pure forms of
these are relatively difficult to buy.  What’s easy to buy is
quicklime or slaked lime, which is fairly pure calcium oxide or
hydroxide.  [25 kg of El Milagro brand quicklime costs AR$3996][35],
which is 22¢/kg; that’s 71% calcium, so that’s 31¢ per kg of calcium
ions.  The [FGH brand is more expensive at AR$6010/23kg][36] via
Mateweb.  [Slaked lime is even cheaper at AR$1276/25kg][37], 7¢/kg,
and that’s 54% calcium, so 13¢/kg of calcium ions.

[15]: https://en.wikipedia.org/wiki/Calcium_aluminates
[37]: https://articulo.mercadolibre.com.ar/MLA-1291719753-cal-25kg-hidrat-extra-cementos-avellaneda-_JM
[36]: https://articulo.mercadolibre.com.ar/MLA-918732332-cal-viva-x-23-kg-aprox-_JM
[35]: https://articulo.mercadolibre.com.ar/MLA-1483085108-cal-viva-el-milagro-25-kg-alta-calidad-_JM

### Zinc ###

I think zinc is too expensive.

### Aluminum ###

Aluminum is the most abundant candidate cation, and it also forms the
best phosphates: they’re stronger, stiffer, and more refractory than
the others.  I don’t know where to buy aluminum hydroxide, but
[aluminum ingots cost AR$800/kg in Mendoza province][38] (US$1.10/kg)
and probably here too, and recyclers around here buy aluminum for
about half that price.  For a water-soluble option, [you can buy
aluminum sulfate to flocculate your swimming pool][39] for
AR$4500/5kg, which sounds cheap, but that’s only 15.8% aluminum, so it
works out to US$7.90/kg Al.  So it’s probably better to just get some
metallic aluminum and dissolve it, either electrolytically or with
caustic soda.

[38]: https://articulo.mercadolibre.com.ar/MLA-1322682526-lingotes-de-aluminio-tipo-perfilblandocarterpeso-5kg-_JM
[39]: https://articulo.mercadolibre.com.ar/MLA-1305246128-sulfato-de-aluminio-5-kg-decantado-de-piscina-clarificador-_JM

