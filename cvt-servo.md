I might have written this idea up previously, but I’m not sure.

The so-called “torque amplifier” was the key invention that made
Vannevar Bush’s differential analyzer practical: a cloth belt anchored
at its ends to two different movable arms, in the middle wrapped
around a rotating shaft.  If the angle difference between the arms
decreased, it tightened the belt around the rotating shaft, increasing
friction; this pulled the belt in such a way as to move the lagging
arm.  If the angle increased, the belt slackened.  A second belt
between the same pair of arms, wrapped the other way around a shaft on
the same axis but the opposite rotation, completed the system.  This
formed a negative-feedback system which maintained a constant angle
between the arms, forcing the slave arm to follow the master closely,
whatever its movements.  Graphite lubrication on the belt was found to
make the system work more smoothly.

A difficulty here is that the torque that can be transmitted is quite
small, and the system is not very efficient because the belts are
always slipping.

Suppose that instead of using the angle difference between the two
arms to adjust the strength of such a frictional clutch, we use it to
alter the gear ratio of a continuously variable transmission, of which
there are many types.  Most CVTs rely on friction rather than positive
engagement (the exception being swashplate-driven hydraulic CVTs) but
often the friction involves very little loss; for example, it may be a
belt on two variable-sized pulleys, or sprag clutches converting
variable-sized reciprocating motion back to continuous rotary motion.

By connecting the output of the CVT to a differential which computes
the difference between the CVT’s input and output, it becomes a
reversible IVT (“infinitely variable transmission”) whose output shaft
remains stationary as long as the CVT is at a 1:1 gear ratio, but can
then vary in either direction.  This evidently provides the
proportional control required for a servo, as long as the
differential’s output shaft turns in the direction required to move
the CVT’s gear ratio back towards unity.

By itself this may not be a very good servo, both because it will only
asymptotically approach zero difference between the set point and the
measured plant output, and because in the moments when the difference
is large (indicating that the control system has fallen behind) it
will tend to decrease its mechanical advantage in order to get rapid
movement, which may be counterproductive if it is falling behind
because it is meeting resistance.  These problems can be fixed by
deriving the control input to the CVT, which is very low in mechanical
power, in a more complex way, possibly involving springs to integrate
movement over time or to compute a weighted sum of several different
quantities.
