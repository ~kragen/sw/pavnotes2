I was wondering if there was a risk of metal being oxidized by sugar
or similar substances.  Probably under normal circumstances the
activation energy is too high to worry about this.

[The Wikipedia page for glucose][0] says its standard enthalpy of
formation is -1271kJ/mol, and it contains 6 moles of O, so -212kJ per
mole of atomic oxygen.  [Magnetite, Fe₃O₄][1], is -1120.89kJ/mol,
which is -280kJ per atomic mole of oxygen, so this hypothetical
reaction would be slightly exothermic:

> 2C₆H₁₂O₆ + 9Fe → 12C + 12H₂ + 3Fe₃O₄ + 816kJ/mol

[0]: https://en.wikipedia.org/wiki/Glucose
[1]: https://en.wikipedia.org/wiki/Iron(II,III)_oxide

Because this reaction liberates gas, it could potentially cause a
hazardous pressure buildup; by the same token, though, the escaping
gas will tend to carry away heat and physically disperse the reagents,
slowing down the reaction.  However, because the gas is itself
combustible, a possible reaction with air could liberate further
energy after it escapes.

The mass of the reagents in the equation above is 862.9g/mol, so this
is only 0.946MJ/kg; the total amount of energy thus stored is not very
large compared to more hazardous substances like [gasoline
(46.4MJ/kg)][16].

[16]: https://en.wikipedia.org/wiki/Energy_density#Chemical_reactions_(oxidation)

Other plausible iron-oxygen compounds have similar per-oxygen
enthalpies of formation.  [Hematite, Fe₂O₃][3] is -824.2kJ/mol, which
is -274.7kJ per mole of atomic oxygen, and so I guess would dominate
over magnetite at a high temperature.  [Fe(OH)₂][4] is -574.04kJ/mol,
which is -287.02kJ per mole of atomic oxygen, and [FeO(OH), one of
whose forms is ochre, is reported to be -562.9 ± 1.5 kJ/mol][5], which
works out to -281.5kJ per mole of atomic oxygen.  You might think
cementite (Fe₃C) would contribute something, but [cementite’s standard
enthalpy of formation][6] is actually positive, at 25.1kJ/mol.

As a point of reference, [the standard enthalpy of formation of
H₂O][2] is −285.83kJ/mol.  So I guess heating up sugar to dehydrate it
is about equally exothermic with and without iron.  Or, from another
point of view, water is very nearly equivalent to goethite as a
hazardous oxidizer, oxides of iron are slightly more oxidizing than
water or goethite, and sugar is slightly more oxidizing than they
oxides of iron.

[2]: https://en.wikipedia.org/wiki/Properties_of_water
[3]: https://en.wikipedia.org/wiki/Iron(III)_oxide
[4]: https://webbook.nist.gov/cgi/cbook.cgi?ID=C18624447&Mask=2
[5]: https://pubs.geoscienceworld.org/ejm/eurjmin/article-abstract/6/6/967/62674/Thermodynamic-properties-of-iron-oxides-and?redirectedFrom=PDF
[6]: https://en.wikipedia.org/wiki/Cementite

But [hydroxylation of iron is only -0.89 volts][7].  Metals more eager
to form oxides or hydroxides according to Wikipedia’s standard
electrode potential list include zinc (-1.199V), titanium (-1.31V),
zirconium (-1.553V), boron (-1.79V), aluminum (-2.31V), and magnesium
(-2.69V).  Also listed are simple ionizations of lanthanum (-2.379V),
cerium (-2.336V), beryllium (-1.847V), aluminum (-1.667V), and
manganese (-1.185V).  The varying number of electrons involved
correspond to the varying numbers of oxygen atoms they would combine
with.  So we’d expect most of these to have a potentially hazardous
exothermic reaction with sugar.

[7]: https://en.wikipedia.org/wiki/Standard_electrode_potential_(data_page)

And we see that [magnesium hydroxide][8] is -924.7kJ/mol (-462.35kJ
per mole of atomic oxygen), [magnesium oxide][9] is -601.6kJ/mol
(-601.6kJ per mole of atomic oxygen), [aluminum hydroxide][10] is
-1277kJ/mol (-426kJ per mole of atomic oxygen), and [alumina][11] is
-1675.7kJ/mol (-559kJ per mole of atomic oxygen).  [Cerium dioxide
(CeO₂)][12] was reported in 01969 to be only -260.6kJ/mol (-130kJ per
mole of atomic oxygen), but that seems improbable.  [La₂O₃ is reported
in a review in 02014][13] to be -1791.6 ± 2.0 kJ/mol (-597kJ per mole
of atomic oxygen), and for CeO₂ the same review gives a much more
plausible -1088.6 ± 1.4 kJ/mol (-544.3 kJ per mole of atomic oxygen).
[Zinc oxide is -350.46 ± 0.27 kJ/mol][14] (and per oxygen), and [zinc
hydroxide][15] is -642kJ/mol (-321kJ per mole of oxygen).

Alumina’s 559kJ/mole of oxygen is 280kJ per mole of electrons, which
works out to 0.465 attojoules per electron, and thus 2.90 electron
volts per electron.  This is in the ballpark of the -1.667V and -2.31V
numbers from the standard electrode potentials, but it’s significantly
higher.  O₂ oxygen’s standard electrode potential to become hydroxyls
is +0.401 volts, which is suggestively close to the difference 2.90 -
2.31V, and the -2.31V was for Al(OH)₃ ↔ Al + 3OH⁻.

[8]: https://en.wikipedia.org/wiki/Magnesium_hydroxide
[9]: https://en.wikipedia.org/wiki/Magnesium_oxide
[10]: https://en.wikipedia.org/wiki/Aluminium_hydroxide
[11]: https://en.wikipedia.org/wiki/Aluminium_oxide
[12]: https://www.sciencedirect.com/science/article/abs/pii/S002196147180068X
[13]: http://thermophysics.ru/pdf_doc/konings2014.pdf (The Thermodynamic Properties of the f-Elements and their Compounds. Part 2. The Lanthanide and Actinide Oxides, by Rudy J. M. Konings, Ondrej Beneš, Attila Kovács, Dario Manara, David Sedmidubský, Lev Gorokhov, Vladimir S. Iorish, Vladimir Yungman, E. Shenyavskaya, and E. Osina. Journal of Physical and Chemical Reference Data 43, 013101 (02014); doi: 10.1063/1.4825256)
[14]: https://en.wikipedia.org/wiki/Zinc_oxide
[15]: https://en.wikipedia.org/wiki/Zinc_hydroxide

So where iron yields only about 280 - 212 = 68kJ per mole of
sugar-sourced oxygen, magnesium might yield 390kJ per mole of oxygen
with this reaction:

> C₆H₁₂O₆ + 6Mg → 6C + 6H₂ + 6MgO + 2340kJ/mol

A mole of reagents is 326 grams, so this is about 7.18MJ/kg, enough to
be quite hazardous, even if it’s only a fraction of the 24.7MJ/kg
magnesium will yield by burning in oxygen.

As a result, I think we can conclude that all of these metals have
potentially hazardous reactions with sugar, reactions which under
normal circumstances are blocked by things like oxide layers and
activation energies.  At least the reaction products are in general
nontoxic.

Metals nobler than iron pose no such hazard; these include nickel
(-0.72V), copper, cadmium, cobalt, vanadium, cobalt, molybdenum, tin,
lead, mercury, silver, gold, and the platinum group.
