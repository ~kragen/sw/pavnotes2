Taiwan’s GDP is [US$791.61 billion per year][0].  Of this,
international trade is a dominant part; [its exports in 02023 are
US$479.415 billion][1] and its imports are US$428.082 billion, which
presumably means that about US$312 billion is production for domestic
consumption.  Its dominant export markets are the rest of China
excluding Hong Kong (US$121.092 billion), the US (US$75.052 billion),
and Hong Kong (US$64.782 billion), so Taiwan has about US$186 billion
of exports per year to the rest of China, more than twice its exports
to the US, 60% of its production for domestic consumption, and 39% of
its total exports.  (Note that these percentages do not agree with
those in the source cited, so either their totals are wrong or their
percentages are; 121 is 25% of 479, not 22.14%, and 64.8 is 13.5% of
479, not 13.07%.)

To look at it slightly differently, of its production destined for
China (including Taiwan and Hong Kong), 63% is locally consumed by
Taiwan itself, and 37% goes to the rest of China.

[0]: https://en.wikipedia.org/wiki/Economy_of_Taiwan
[1]: https://taiwaninsight.org/2024/02/14/taiwans-trade-dynamics-in-2023-challenges-and-partners-shifting/

Imports in 02023 are significantly different.  Only US$84.002 billion
of those 02023 imports come from the rest of China excluding Hong
Kong, followed by Japan at US$54.626 billion and the US at US$45.69
billion.

This means that, even as it’s threatened by military posturing,
Taiwan’s economy is heavily dependent on its relations with the rest
of China, so there’s a very strong incentive to find a peaceful path
of reunification.
