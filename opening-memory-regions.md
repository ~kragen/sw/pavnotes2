I’ve been thinking again about amortizing the overhead of dataflow
tracking over multiple data accesses.  Specifically what I’m
interested in here is tracking and intercepting reads and especially
writes to memory.

Motivating use cases
--------------------

Why is it important to track and intercept reads and writes?  And why
is it important to optimize that tracking and interception?

1. **Incremental recompilation**. You compile a program.  The compiler
   reads some data files and writes some data files.  You want to
   memoize its output so that you can avoid running the compiler again
   if none of its inputs have changed, because that implies that its
   outputs would be the same unless it’s nondeterministic.  This way
   you can get incremental recompilation.  Doing this reliably
   requires being able to find out which data files the compiler read.
2. **Live database queries.** You display some database query results
   on your screen.  It happens that you’d like the results to be
   updated whenever the database changes in such a way that they would
   differ.  (Sometimes this is not what you want, but here I’m
   considering the case where it is.) But executing the whole query is
   costly, so you’d prefer to avoid executing it when the database
   hasn’t changed, or has only changed in irrelevant ways.  One way to
   handle this is at the physical level: by logging which data pages
   or specific records the query read during execution, we have a
   conservative approximation of what it depends on, so we can delay
   re-executing it until at least one of the pages it depends on has
   changed.
3. **Bounds checking.**  You want to run a program that indexes into
   some records or arrays.  The simplest way to implement this is with
   pointer arithmetic, but unconstrained pointer arithmetic can
   violate array bounds and either read or write unrelated data,
   possibly violating both access control and type safety.  So it’s
   necessary to check memory reads and writes before allowing them to
   proceed.  Doing this in software by inserting bounds checking
   instructions before every memory reference imposes significant
   overhead, so various schemes are used to hide or eliminate this
   overhead.
4. **Write barriers.**  Incremental and generational garbage
   collectors must guard against the mutator writing references to
   objects that might be recycled into objects that the garbage
   collector is not going to check.  Generally this is done by doing a
   little extra work on writes — if the object being written is an
   object that the GC is not going to check, the so-called “write
   barrier” adds it to the to-check set.
5. **Software transactional memory.**  To ensure serializability of
   concurrent memory transactions, we log the memory reads and writes
   of each transaction in order to be able to detect conflicts, where
   a transaction wants to commit writes after having read data that
   another transaction has overwritten.  This makes concurrency easy
   to reason about and therefore makes it easier to achieve high
   parallel performance.
6. **[Cached screen updates][0].**  We’re redrawing a graphical screen
   by running some arbitrary functions with some arbitrary input data,
   but we’d like to avoid the work of redrawing parts of the screen
   that haven't changed.  By tracking which parts of the screen each
   function wrote to and which data it read from, we can avoid
   rerunning functions to redraw regions of the screen that can’t have
   changed.  In this case, unlike the compiler case, we may need to
   rerun some functions whose inputs haven’t changed, in order to
   redraw regions of the screen that *did* change.
7. **[User interface undo][1].**  [Undo is an important feature for
   usable UX][2] in almost all software, but implementing it can
   require a lot of bug-prone code, including new code every time you
   add a new command.  Alternatively, if we log the memory writes of
   the code that needs to be undone, along with the previous contents
   of that memory, we can undo it by rewriting the previous contents.
8. **Record and replay, and reverse execution.**  It’s very valuable
   to be able to replay the execution of a program at a later time for
   debugging; among other things, this allows us to back up the
   execution of the program in a debugger to a previous state.  If
   it’s deterministic, we can do this just by recording its inputs,
   then re-execute the program from the beginning with the recorded
   inputs.  But this can take a very long time, so it’s desirable to
   be able to quickly restore the program’s internal state to a
   previous recent state, ideally by only changing some of memory
   instead of all of it.  This is sort of an external “undo”.
9. **The Prevalent System design pattern.**  [Prevayler][3],
   [announced on Advogato][4], applies the record-and-replay approach
   to replace databases: by periodically snapshotting the state of
   memory to disk and journaling the writes to it in between
   snapshots, you can rapidly do point-in-time recovery to restore
   your program’s state to its last state before a system failure or
   other shutdown.  (Because Prevayler is in Java, the programmer has
   to insert the write tracking into their code by hand.)  This write
   journaling can provide PITR in a way that the orthogonal
   persistence in KeyKOS or EUMEL cannot (for most processes.)
10. **Permission checking.**  When a program tries to read or write
    some data, we often have some kind of access control policy that
    governs whether it will be allowed to do so or not.  Unix
    filesystem permissions are one example.  These permission checks
    can involve arbitrary Turing-complete computations, so doing them
    on every memory access is not only undesirable, it would in most
    cases actually involve infinite recursion.
11. **Watchpoints.**  A common question when debugging is how some
    variable got the wrong value.  Debuggers commonly offer
    “watchpoints” to answer this question, memory locations which will
    trap to the debugger when accessed.  This requires some kind of
    interposition on writes; this feature is so popular that embedded
    processors commonly include hardware support for it, while I think
    on Linux it’s usually implemented with virtual memory hardware.
12. **Virtual memory.**  Interposition on memory reads and writes
    makes it possible to provide access to a larger virtual memory
    space than is physically present in the machine by paging some of
    it out to disk.  This immediately offers additional possibilities
    such as memory-mapped access to files, sharing pure executable
    pages between processes, compressed RAM, copy-on-write memory
    after Unix fork(), overcommit, rapid copy-on-write system
    snapshots for orthogonal persistence as KeyKOS did, etc.  Normally
    this is only done with hardware support.
13. **Transparent distribution.**  Virtual memory includes the
    possibility of swapping over a network to another computer with
    more RAM, to a remote disk, or to a remote memory-mapped file.  In
    the standard case, where dirty pages get written back to the
    underlying storage in an effectively random order, this is not a
    very good way to write to things, but if you only use
    memory-mapping for reads (like LMDB, [which uses `pwrite` for
    writes][8] and `mmap` for reads to [beat SQLite’s performance by
    an order of magnitude][9]) or if you use this approach to extend
    software transactional memory across a network, it may offer an
    appealing way to build a nearly transparently distributed system.
14. **Fluid binding.**  Common Lisp switched to mostly using Scheme’s
    lexical binding, but retains the ability to declare [“special
    variables][5]” that support dynamic binding, so that you can, for
    example, locally bind [`*standard-input*`, `*error-output*`][6],
    [`*print-base*`][7], or [other reader and printer control
    variables][10].  Graphical output commonly has many such
    variables, such as the current color, current position, current
    path, current line width, current font, current transformation
    matrix, etc.  PostScript and `<canvas>` offer operators to save
    and restore the entire current set of such variables; as with undo
    and reverse execution, this can be done much more efficiently when
    writes to the protected variables can be detected and intercepted.
    In Lisp the restoration is implicit, while in PostScript and
    `<canvas>` it is explicit.
15. **Backtracking search.**  Allain et al. recently published a
    fascinating paper called [“Snapshottable stores”][11] which
    presents a really nice way to intercept writes in OCaml with
    minimal (but nonzero) burden on the programmer.  The motivating
    example is backtracking the union-find data structure while
    attempting a unification during type-checking, but their data
    structure is also applicable to fluid binding (in fact, they
    derive it from an implementation of fluid binding Greenblatt
    proposed for the CADR in 01974), backing up execution to a
    snapshot, system prevalence, user interface undo, write barriers,
    live database queries, and incremental recompilation, but not the
    other cases mentioned above.  Their careful discussion of the
    performance tradeoffs and potential pathological cases is very
    worth reading.
16. **Fuzzing.**  American Fuzzy Lop tests software so extensively
    that it was able to [construct a valid JPEG file][12] by testing
    djpeg, by virtue of tracing the control flow inside the program
    under test and being able to restart it from checkpoints an
    astronomically large number of times.

[0]: https://rxi.github.io/cached_software_rendering.html
[1]: https://rxi.github.io/a_simple_undo_system.html
[2]: https://www.nngroup.com/articles/ten-usability-heuristics/#toc-3-user-control-and-freedom-3
[3]: https://prevayler.org/
[4]: https://web.archive.org/web/20090225073626/https://advogato.org/article/398.html
[5]: http://clhs.lisp.se/Body/d_specia.htm#special
[6]: http://clhs.lisp.se/Body/v_debug_.htm#STerror-outputST
[7]: http://clhs.lisp.se/Body/v_pr_bas.htm#STprint-baseST
[8]: https://news.ycombinator.com/item?id=36563187
[9]: http://www.lmdb.tech/bench/microbench/
[10]: http://clhs.lisp.se/Body/m_w_std_.htm#with-standard-io-syntax
[11]: https://dl.acm.org/doi/10.1145/3674637
[12]: https://lcamtuf.blogspot.com/2014/11/pulling-jpegs-out-of-thin-air.html
