I was thinking (based on some paper I read) that it would be nice to
make some xerogel beads full of muriate of lime as a non-deliquescent
desiccant capable of absorbing its own weight in water; the original
paper used alginate.  I have a jar of carboxymethylcellulose powder
(US$15/kg), and another one of xanthan gum (US$30/kg), and various
vendors sell carbomer (sodium polyacrylate) for US$30/kg, which I
think can gel at even lower fractions.  By contrast, the local sources
I’ve found for sodium alginate charge US$80/kg or US$900/kg (!).

See file `canister-stillsuit.md` for what is probably the most
important purpose for such a thing.

I think the hydrophilic radicals in all three of these which make them
water-soluble are carboxylate (COOH, or rather COO⁻) moieties, so it
seems plausible that they’d all be subject to ionotropic gelation with
polyvalent cations.  Such as, in particular, calcium and copper.

Sharratt et al. gelated CMC with FeCl₃
--------------------------------------

[An open-access paper from 02021 by Sharratt et al. supports this
idea][0] for CMC:

> Here, we explore the gelation of sodium carboxymethyl cellulose
> (NaCMC), a model anionic polyelectrolyte, with Fe³⁺ cations in
> water. (...)  NaCMC with a degree of substitution (D.S.) > 1,
> characterising the number of hydroxyl groups on the cellulose
> backbone substituted (out of a maximum of 3), is molecularly soluble
> in salt-free solutions [28] (see Figure 1a). Upon the addition of
> monovalent salts (*e.g.*, Na⁺), electrostatic interactions between
> NaCMC chains are screened and the polyelectrolyte behaves
> analogously to a neutral polymer [29, 30]. Addition of multivalent
> ions to NaCMC solutions can also solely screen electrostatics (e.g.,
> Mg²⁺) and can cause aggregation (e.g., Ca²⁺, Ba²⁺, Zn²⁺) and even
> precipitation [31]. Solutions of CMC with divalent counterions
> display higher entanglement densities than those of NaCMC, but no
> gel formation formation [sic] is observed [11]. Trivalent ions
> (e.g., Al³⁺ or Fe³⁺) have been reported to cause gelation [32–34],
> rationalized in terms of the crosslinking of multiple carboxylate
> functionalities off the cellulose backbone as illustrated in Figure
> 1b.

[0]: https://mdpi-res.com/d_attachment/gels/gels-07-00044/article_deploy/gels-07-00044-v2.pdf?version=1618802580 "Ionotropic Gelation Fronts in Sodium Carboxymethyl Cellulose for Hydrogel Particle Formation, by Sharratt, Lopez, Sarkis, Tyagi, O’Connell, Rogers, and Cabral, Gels 2021, 7, 44. https://doi.org/10.3390/gels7020044"

This makes me wonder what the relationship is between CMC (which
apparently doesn’t form gels with divalent counterions) and NaCMC
(which maybe does?)

Mixing the goo ahead of time with food coloring ought to help to
produce spheres with appealing colors; dripping the goo into oil of
lime doped with a bit of blue vitriol might gelate its surface, and it
ought to be possible to get that to happen fast enough to keep
subsequent spheres from sticking to it.  If gelation doesn’t happen,
though, maybe I could dope the oil of lime additionally with aluminum
chloride or ferric chloride to get it to gel?  Sulfates wouldn’t be a
good combo with oil of lime, you’d just get alabaster.

The paper has the clever hack of forming small drops of aqueous NaCMC
solution (degree of substitution 1.15–1.35 out of 3) by mixing it into
oil (oleic acid), which then carries the drops onto the salt bath,
where I guess they sink out of the oil film and into the salt bath.
They got gelation with 8.5–43 grams per liter of NaCMC, which I guess
is 0.85 wt% to 4.3 wt%.  (I’m looking for a recipe here more than
quantitative thermodynamics.)

It points out that generally the diffusion layer thickness grows
proportional to the square root of time, and show it being on the
order of a micron after on the order of ten minutes, which is
quantitatively a bit disappointing.  Lighter ions like aluminum might
diffuse faster.

They point out that the NaCMC is hygroscopic, so they were actually
adding 10–50 grams per liter of water, but I guess they figured the
dry stuff was already 15% water.

This was a really good paper.

Lin et al. gelated CMC with CaCl₂ and NaOH
------------------------------------------

[Lin et al. in 02009][1] studied “Anisotropic gelation of
carboxymethylcellulose (CMC) in aqueous NaOH sandwiched between two
circular cover glasses by immersing it in aqueous CaCl₂”, using
“sodium-type carboxymethylcellulose” with degree of substitution 2.2
(out of 3 possible) mostly at 5 wt%, but sometimes 2–10 wt%, with
0.4-molar lye and immersing it into 1-molar CaCl₂ at 25°.  So I guess
you *can* gel carboxymethylcellulose with calcium?  Maybe only at high
pH?

> Since calcium cations chelate with carboxymethyl functional groups
> of CMC intermolecularly, cross-linking proceeds with the inflow of
> CaCl₂.

What they’re really interested in is the anisotropy of the gel
formation, though.

[1]: https://doi.org/10.1007/s00396-010-2193-y "Phase relationship and dynamics of anisotropic gelation of carboxymethylcellulose aqueous solution, by Lin, Minamisawa, Furusawa, Maki, Takeno, Yamamoto, Dobashi, 02009"

Zheng et al. liked alginate better
----------------------------------

[Zheng et al. in 02019][2] are mostly investigating the gelation of
alginate rather than CMC, but they do point out:

> Sodium carboxymethyl cellulose (CMC) is the main representative
> product of ionic cellulose gum and is utilized by food industries
> for its bulking, thickening, water absorption, stability, and
> gelation abilities (Du et al., 2009). The blending of CMC and other
> hydrophilic colloids can have a considerable enhancing effect on
> foods.

So, at least for them, CMC is pretty much just NaCMC.  Also they were
using gluconic-acid-δ-lactone (“as an acidifier”) to release their
calcium ions gradually (from EDTA?) to get a more uniform gel.

[2]: https://www.researchgate.net/profile/Jiong-Zheng/publication/330107809_Effects_of_sodium_carboxymethyl_cellulose_on_rheological_properties_and_gelation_behaviors_of_sodium_alginate_induced_by_calcium_ions/links/620db081f02286737ca4be2a/Effects-of-sodium-carboxymethyl-cellulose-on-rheological-properties-and-gelation-behaviors-of-sodium-alginate-induced-by-calcium-ions.pdf "LWT - Food Science and Technology 103 (02019) 131–138"

Patil, Chavanke, and Wagh are a train wreck
-------------------------------------------

[Patil, Chavanke, and Wagh wrote a dubiously titled paper in 02012][3]
in the dubiously named “International Journal of Pharmacy and
Pharmaceutical Sciences”.  Their figure outlines the ionotropic
gelation process for drug “gelisphere” preparation as working by
adding a polyelectrolyte solution consisting of the drug and a
polyelectrolyte (either chitosan, for a cationic polyelectrolyte (why
not PVOH?), or for an anionic polyelectrolyte, sodium alginate, gellan
gum, CMC, and/or pectin) dropwise under magnetic stirring through a
needle into a counterion solution consisting of either calcium
chloride (for polyvalent cations) or sodium tripolyphosphate (for
polyvalent anions).  The paper is full of grammatical errors, and the
text contradicts the figure, entirely omitting the possibility of
cationic polyelectrolytes.  Also they list potassium as a multivalent
cation.  The paper is basically a train wreck if you’re looking for
any kind of reliable information.

[3]: https://www.academia.edu/download/34038473/4488.pdf "A Review on Ionotropic Gelation Method: Novel Approach for Controlled Gastroretentive Gelispheres"

However, it might be an interesting source of inspiration, and a
source of useful rumor and hearsay.  In the CMC section they say:

> The CMC can be cross-linked with ferric/aluminum salt to get
> biodegradable hydrogel beads.

Which is what I was thinking was needed from the Lin et al. paper; but
maybe they just read Lin et al., since they published three years
later, though the only relevant thing they cite is a paper from 02010
by Kulkarni et al., “Carboxymethylcellulose-aluminum hydrogel
microbeads for prolonged release of simvastatin”, Acta Pharmaceutica
Sciencia 2010; 52: 137-143.

Boppana et al. gelated CMC with aluminum chloride
-------------------------------------------------

Another version of which is [Boppana et al.][4], or maybe Patil et
al. just screwed up their bibliographic references the same way they
screwed up their facts.  Boppana et al. used NaCMC (3–5% w/v) and
gelated it with hydrous aluminum chloride (2–8% w/v) and NaOH (they
forgot to say how they used this in the paper) by extruding it through
a hypodermic under magnetic stirring.  They explain:

> When a dispersion of drug and NaCMC was extruded through the needle
> into a solution containing Al³⁺ cations, the beads were formed
> instantaneously.  As soon as the trivalent cations (Al³⁺) are bought
> [sic] in contact with NaCMC, they form ionic cross-links (...) These
> ions are ionically substituted at the carboxylate site and a second
> strand of NaCMC can also be connected with Al³⁺ forming a link in
> which the cations are attached to three NaCMC strands together.

[4]: https://www.actapharmsci.com/uploads/pdf/pdf_227.pdf

They don’t explain why it doesn’t stick to the needle if it
cross-links instantaneously, but apparently it doesn’t.  Maybe the
needle didn’t touch the salt bath.

2–8% is low enough that it wouldn’t affect the amount of desiccant in
my beads much.

Mohamood, Halim, and Zainuddin made CMC and gelated it with CaCl₂
-----------------------------------------------------------------

[Mohamood, Halim, and Zainuddin][5] wrote an open-access paper in
02021 where they gelated CMC made from oil palm biomass waste with
calcium chloride; the best result was 28.11% gel content (??) made
with 20% w/v CMC and 1% w/v CaCl₂.  But they were willing to wait for
12–96 hours for it to crosslink at 60°, and they report that they were
able to *dissolve* 10–30% CMC solution in 1–5% CaCl₂, which suggests
that the reaction was not rapid at all.

[5]: https://mdpi-res.com/d_attachment/polymers/polymers-13-04056/article_deploy/polymers-13-04056.pdf?version=1637652159 "Carboxymethyl Cellulose Hydrogel from Biomass Waste of Oil Palm Empty Fruit Bunch Using Calcium Chloride as Crosslinking Agent"

I’m not sure what they mean by “gel content” but most of the paper is
about how much gel content they could get with different recipes and
about how they made the CMC from the waste.  The paper is pleasant to
read with colorful figures.

If they mentioned the degree of substitution they got in the CMC they
made, I missed it.  That would have been useful information and might
be related to why they were able to get gelation where some other
groups needed trivalent cations.

Aluminum chloride
-----------------

So probably I’d need aluminum chloride for the kind of drippy
spherification I’d envisioned.  You can make it by attacking aluminum
with hydrochloric acid, which I don’t have.  Maybe I can make it
electrochemically?

Apparently [electrolysis of aluminum in chloride can produce something
called polyaluminum chloride instead][6], which is some kind of
bizarre hydroxylated fragment of alumina with 13 aluminum atoms.  But
that might not be a real problem in this case; that might work fine.

[6]: https://doi.org/10.1016/s0043-1354(98)00275-9

Summary of the ionotropic gelation literature
---------------------------------------------

Yeah, so this looks like a viable thing to do, just not quite in the
simple form I initially thought; I’d need to premix my desiccant with
some aluminum chloride, which I can probably make electrolytically
from, say, muriate of lime and aluminum, and I guess maybe some kind
of acid or buffer to keep the aluminum from precipitating as a
hydroxide.  Probably something like 5% aluminum chloride by weight,
which is 0.5% aluminum by weight, so every liter of the salt bath
involves dissolving 5 grams of aluminum.  Then I could mix up a gooey
CMC mixture with whatever dyes I want, with probably 3% or so CMC in
the water, and then drip it into the salt bath.  Then I can leave it
for days (ideally, at a temperature like 80° or so) to diffuse the
desiccant salt into the spheres, and then wash and dry them.

The cost of the materials is pretty low.  Muriate of lime basically
costs the cost of shipping until you’re buying it in hundreds of kg,
and the resulting spheres will be something like 80% muriate of lime,
1% aluminum (which is US$2/kg, so this would contribute 2¢/kg to the
cost of the spheres), and 5% CMC.  So we’re looking at something like
a dollar or two per kg, and most of the cost is the CMC.  So, if you
want tonnes of the stuff, it might be worthwhile to see if lower CMC
contents might be adequate, or try some more aggressive gel-former
like maybe carbomer (carbopol, polyacrylate).

For my experimental purposes, though, it should be fine.

A simpler approach: an alum bath followed by the desiccant salt bath
--------------------------------------------------------------------

Instead of forming the sphere skins with contact with the same salt
bath that infuses them with desiccant, you can use two separate baths;
for example, a bath of alum or ferric chloride to form the sphere for
a few minutes, followed by lifting the still-liquid-inside spheres out
of the spherifying bath, rinsing them, and dropping them into the
desiccant salt bath, perhaps muriate of magnesia, to fully solidify.
I have more muriate of magnesia handy, though it’s more expensive, and
it might be safer in the sense that its bitter taste would lead
foolish people who tried to eat the spheres to spit them out quickly
before they could get burned.  [It can reduce the relative humidity to
about 32%][8], improving on the 40% or so delivered by muriate of lime
(which improves at high temperatures and bests that of magnesia above
75°) and has been in use for dehumidification since [at least the
01930s][12].

[8]: https://www.oxy.com/siteassets/documents/chemicals/products/other-essentials/173-01882-CaCl2-vs-MgCl2-Dampness-in-Dust-Control.pdf "Figure 3, CaCl₂ and MgCl₂ capacity to form solutions, in ‘Calcium Chloride vs. Magnesium Chloride: Dampness in Dust Control Applications’, author anonymous, published by oxy.com"
[12]: https://apps.dtic.mil/sti/trecms/pdf/AD1151845.pdf "Second Partial Report on Investigation of Dehumidifying Agents for Submarines - Characteristics of Magnesium Chloride as a Desiccant, by J.O. Clayton and P. Borgstrom, NRL Report No. P-1277 or FR-1277, 01936-06-16"

This would reduce the amount of aluminum in the final spheres by
orders of magnitude, probably resulting in a measurable increase in
the desiccant capacity of the spheres.

Acid buffering, if that’s necessary
-----------------------------------

If the muriate is subjected to repeated thermal cycling, which is part
of the intended application in this case, it can produce muriatic
acid, which is probably undesirable; it might catalyze hydrolysis of
the polysaccharides, for example.  If air circulation (also part of
the intended application in this case!) is insufficient to keep the
acid down, it may be desirable to buffer the spheres with something to
neutralize the acid.

(This might all be unnecessary because the acid will diffuse out of
the spheres into the air, and would probably be present at low enough
concentrations to not be a concern in that context.)

In the context of using oil of lime as a coolant inside sealed piping,
I’ve seen a paper that reported success by incorporating a few percent
of magnesium shavings into the coolant; the acid would oxidize the
magnesium, neutralizing the acid back into water.  Aluminum shavings
would presumably work adequately in the same context and be much
cheaper than magnesium.  But these spheres are exposed to air, and
oxygen can diffuse into them, so I think that approach might not work
that well here; the muriates would depassivate the aluminum or
magnesium, catalyzing its relatively rapid oxidation with diffused
oxygen, though that would still leave a basic metal hydroxide which
could neutralize acid.

A simpler approach with the same benefit would be to include the base
or buffer directly in the spheres.  Hygroscopic bases or buffers might
not even diminish the water absorption capacity of the gel.  Borax in
particular comes to mind, and neutralizing it wouldn’t produce gas.  I
thought that, because it’s water-soluble, it would tend to diffuse out
of the spheres into the salt bath during the desiccant-infusion
process; but, in fact, [on contact with calcium ions, it will just
precipitate as calcium borate, with aqueous solubility of 0.1–0.3%
w/v][7], so it would probably stay there.  But it might not buffer
against acid in that form.

[7]: https://patents.google.com/patent/US4233051A/en "Embarrassingly obvious US patent 4,233,051, Walter Eastes, Owens-Corning Fiberglass, granted 01979"

Carbonate of calcium (or magnesium) would be cheaper than borax and
could be produced by incorporating washing soda into the spheres
initially, precipitating insoluble carbonate; but it might make the
spheres translucent rather than transparent, reducing the saturation
of the color, and it would produce carbon dioxide gas when
neutralizing acid, which would then have to diffuse out of the sphere.
Again, though, this is probably a slow enough process that the
diffusion wouldn’t be a problem.

The lower cost might be irrelevant, though, because we’re probably
talking about concentrations below 1%.

Phosphates of calcium or magnesium would probably also work, without
any gas production, and could be produced in the same way by the
incorporation of soluble phosphates in the goo, such as dipotassium
phosphate, diammonium phosphate, or trisodium phosphate, to name three
that are easily available at retail locally.

Citrate is probably just as good as phosphate, though heavier; citric
acid has three protons and molecular mass 192.123g/mol, while
phosphoric acid is only 97.994g/mol.

Doing without the industrial supply chain
-----------------------------------------

Muriate of magnesia has been available for centuries as bittern from
sea salt extraction (*nigari*), which is usually a waste product, but
not a very plentiful one.  I suspect muriate of lime is easier to
obtain.  With the industrial supply chain, it’s a byproduct of the
Solvay process, in which sodium chloride solution is alkalized to
bicarbonate first with ammonia and then with carbon dioxide, producing
ammonium carbonate which is re-alkalized with quicklime obtained by
calcining limestone.

But I suspect you can make it easily with limestone at an anode in a
sodium chloride electrolyte.  And if you have high heat available, you
can pass NaCl vapor with superheated steam over quartz to get sodium
silicate and gaseous acid, which can then be absorbed into water and
used to digest limestone.  The classic way to get the acid is by
digesting salt with a stronger acid.  And, at a high enough
temperature, I think you can get the gaseous acid from a mixture of
phosphoric acid and any chloride.

Mohamood, Halim, and Zainuddin gave a process for making CMC from
waste cellulose, but mucilage from flaxseed or chia seed is a possible
alternative gum.  Flaxseed costs about US$1/kg around here whether
[golden][13] or [brown][14], and [chia is about US$5/kg][15].  Chia is
[about 5–6% mucilage][16] (see also [Muñoz's dissertation][17] which
claims “7% of yield” and “a sample of 100 mg of mucilage was able to
absorb 2.7 g of water, 27 times its own weight”), while [flaxseed is
3–9% mucilage][18], or [6.5% if you extract with 80° water for 30
minutes][19], or [6%][20], which I guess makes its mucilage cost
US$11/kg to US$33/kg and probably about US$15/kg if extraction is
free; that makes it cheaper than chia mucilage and competitive with
industrial products like CMC and xanthan gum.

(Other possibilities include chitosan, pullulan, nopal mucilage, and
aloe mucilage.)

[13]: https://articulo.mercadolibre.com.ar/MLA-912401297-semillas-lino-dorado-x-5-kg-_JM
[14]: https://articulo.mercadolibre.com.ar/MLA-1191796820-semillas-lino-marron-x-15kg-primera-calidad-100-natural-_JM
[15]: https://articulo.mercadolibre.com.ar/MLA-909814996-semilla-de-chia-x-1-kilo-_JM
[16]: https://www.sciencedirect.com/science/article/abs/pii/S0260877411004560 "‘Chia seeds: Microstructure, mucilage extraction and hydration’, by Muñoz, Cobos, Diaz, and Aguilera, Journal of Food Engineering, Volume 108, Issue 1, January 02012, Pages 216-224, DOI 10.1016/j.jfoodeng.2011.06.037"
[17]: https://repositorio.uc.cl/server/api/core/bitstreams/7c7a3be2-982a-4ca1-a2ac-3fdcd16559c6/content "Mucilage from Chia Seeds (Salvia hispanica): Microestructure [sic], Physico-chemical Characterization and Applications in Food Industry, Loreto Muñoz H., PUC Chile, 02012"
[18]: https://pubmed.ncbi.nlm.nih.gov/29874558/ "Characterization of mucilages extracted from different flaxseed (Linum usitatissiumum L.) cultivars: A heteropolysaccharide with desirable functional and rheological properties, by Kaur, Kaur, and Punia, Int J Biol Macromol. 02018 Oct 1:117:919-927, DOI 10.1016/j.ijbiomac.2018.06.010."
[19]: https://www.sciencedirect.com/science/article/abs/pii/S0268005X11001652 "Laws of flaxseed mucilage extraction, by Ziolkovska, Food Hydrocolloids, Volume 26, Issue 1, January 02012, Pages 197-204, DOI 10.1016/j.foodhyd.2011.04.022"
[20]: https://www.mdpi.com/2304-8158/11/12/1677 "A Review of Extraction Techniques and Food Applications of Flaxseed Mucilage, by Puligundla and Lim, licensed CC-BY, Foods 02022, 11(12), 1677; DOI 10.3390/foods11121677"

There’s still the question of whether flaxseed mucilage is subject to
ionotropic gelation.  Apparently that [it gels by itself at low
temperatures, and low concentrations of calcium ions (<0.3%) increase
its gel strength][21], but higher concentrations weaken it.

[21]: https://www.sciencedirect.com/science/article/abs/pii/S0260877405004425 "Gelation properties of flaxseed gum, by Chen, Xu, and Wang, Journal of Food Engineering, Volume 77, Issue 2, November 02006, Pages 295-303, DOI 10.1016/j.jfoodeng.2005.06.033

Beans and rice commonly swell to absorb about one or two times their
own dry weight and volume in water, even when soaked cold, but it I
suspect that if you soak them in oil of lime instead, they won’t
absorb as much, perhaps nothing.  Still, in a more dilute solution,
you can surely get them to absorb enough muriate of lime to keep them
from rotting, and maybe enough to be a useful desiccant.
