I was thinking about long-distance low-data-rate communication, and it
occurred to me that if you transmit a predetermined random bit string
in an ultrawideband way, you ought to be able to do long-distance
communication without interfering with traditional communications
media.

Skywave propagation can carry signals in the 3 MHz to 30 MHz band
halfway around the world some of the time; at bad times, like at night
or when there are no sunspots, the maximum usable frequency drops to
15 MHz.

Antipodal communication on HF requires about 1-2 J per bit
----------------------------------------------------------

[FT8](https://www.sigidwiki.com/wiki/FT8) is a popular amateur radio
mode for DX QSOing; it sends 13 characters in 12.6 seconds in 50 Hz of
radio bandwidth and can handle SNR down to -21.6 dB, so [on 10 watts a
UK operator can usually reach all over Europe and sometimes to
Malaysia](https://www.essexham.co.uk/ft8-basics-explained).  [The
13-character message is 77 bits plus a 14-bit CRC and 83 bits of
FEC](https://physics.princeton.edu/pulsar/k1jt/FT4_FT8_QEX.pdf), so
that’s 6.1 bits of message data per second and 1.6 joules per bit, but
emitting a spectral power density of 200 mW/Hz.  It uses 8 symbols, so
each symbol represents 3 bits; I think that means it sends about 1.5
symbols per second.  From a certain point of view, [FSK modes like FT8
and JT65 actually have rather high SNRs][1].

[1]: https://tapr.org/pdf/DCC2018-KC5RUO-TheReal-FT8-JT65-JT9=SNR.pdf

FT8 relies on a GPS-driven global clock to organize each minute into
four timeslots.

In the power-limited regime your bit rate doesn’t depend on how much
spectral bandwidth you use, because by spreading your signal across
more bandwidth you’re also requiring your receiver to discriminate it
from more noise.  It just depends on whether your error correction is
any good and how much power you put into your signal, so we should
figure that 1-2 joules per bit is about the limit for these bands.

Suppose we were to back off these parameters rather aggressively:
transmit white noise that spans the whole HF 3-30 MHz range, but shoot
for 0.06 bits per second, 648 bytes per day, so instead of
transmitting at 10 watts we’re transmitting at 100 milliwatts.

Is 100 milliwatts a tolerable spread-spectrum power level on HF?
----------------------------------------------------------------

100 milliwatts is very low spectral power density: 3.7 nW/Hz, 77 dB
lower than the FT8 signal.  You can apparently receive a 10-watt FT8
signal from 20000 km away, though presumably at that point it’s 20 dB
below the noise.  If we simplify skywave propagation as having
irradiance inversely proportional to distance, our fifty-million-times
weaker signal would require you to be less than a meter away to see it
with the same spectral irradiance, but of course at that distance it’s
not skywave anymore.  So let’s say that at 100 km it’s 200 times
stronger than at 20000 km --- now only 270270 times weaker --- and
from there it goes as the inverse square, so at 500 times closer than
that, 200 m, it equals the spectral power density of the FT8 signal.

But that’s still like 20 dB below the noise, so you have to be another
10 times closer to actually notice it, 20 meters away.

There’s a plot of [atmospheric noise][anoise] copied from CCIR Report
322 in [the Wikipedia page covering loopstick antennas][loopstick]
which seems to say that thermal noise is -174 dBm/Hz, but that
atmospheric noise is usually about 30 dB louder in the 3-30 MHz band,
so about -144 dBm/Hz, while man-made noise is higher than this up to
about 7 MHz, starting at 40 dB louder than thermal noise at 3 MHz
(-134 dBm/Hz).  -140 dBm/Hz is 10 attowatts per Hz, 370 million times
weaker than this 3.7 nW/Hz figure, so we don’t fall below atmospheric
noise until you’re only receiving a 370 millionth of the signal, so
you’re 19000 times further away from the transmitter than your
receiving antenna’s effective size, maybe typically 100 mm, giving 1.9
km.  This is kind of bogus though because the atmospheric noise power
level presumably increases with antenna size, so I’m missing some kind
of denominator here.  Still, it’s reassuring!

[loopstick]: https://en.wikipedia.org/wiki/Loop_antenna#Small_receiving_loops
[anoise]: https://en.wikipedia.org/wiki/Atmospheric_noise

Also relevant is AN/TPS-71 [ROTHR][OTH], a shortwave over-the-horizon
radar the US Navy operates in Puerto Rico with 3000-km range.
Previous US OTH systems have operated at a megawatt.  Australia’s
Jindalee OTH shortwave radar system is 560 kW with similar range.
Russia’s similar “Container 29B6” system (29Б6 «Контейнер») has
similar range and covers Europe, the Mediterranean, and the Black Sea.
At least Контейнер is narrowband (14 kHz); the older Soviet Duga
(«Дуга») system ran at 10 MW (EIRP though!), and was narrowband (40
kHz), but used random frequencies.  Контейнер and Дуга caused
annoyance over most of the continent.  Iran’s Sepehr (“سپهر”) OTH
system has a similar ranges and so probably also similar power.

[OTH]: https://en.wikipedia.org/wiki/Over-the-horizon_radar#U.S._Navy

HAARP irradiates the ionosphere with a signal in the 2.6-10 MHz range
at 3.6 megawatts, but the signal is narrowband (100 kHz), and they
assiduously limit themselves to bands where the experiment is
licensed.

So I think you can probably get away with transmitting an entire watt
if you spread it over such a wide spectral range, and thus broadcast
0.6 bits per second to the entire world, 6480 bytes per day.  If
you’re transmitting over a shorter distance, such as 10 km, you can
reduce the transmit power by orders of magnitude (say, to 50
microwatts), and you should probably also use higher frequencies so
your radio signal doesn’t propagate any farther than necessary and you
can efficiently use a smaller antenna.

Up above HF, [the US’s “analog low-power TV” regulations][lptv]
permitted operation at up to 3 kilowatts EIRP VHF and 150 kW UHF.  I
presume that this means that regular broadcasting was done at an order
of magnitude higher powers.  [Apparently analog UHF TV in the US was
limited to 5 MW][uhf], but for digital TV it’s 1 MW, and the old VHF
limit was about 500 kW and the new one 160 kW.  I think these numbers
are EIRP.

[lptv]: https://www.fcc.gov/consumers/guides/low-power-television-lptv-service
[uhf]: https://en.wikipedia.org/wiki/UHF_television_broadcasting

How strong is the signal common radio receivers receive, and in
particular how much noise do they experience?  Very roughly, if you
are 100 km from a 500-kW transmitter and your antenna is a square
meter, the isotropic sphere you’re on is 130 billion square meters and
so you are receiving 4 μW (at an irradiance of 4 μW/m²), or 0.4 μW
with a smaller antenna.  A Wi-Fi signal at -80 dBm, which is about the
level where reception usually gets iffy, is five orders of magnitude
weaker at 10 picowatts, but that can only be received successfully
thanks to coding gain; an 11Mbps 802.11b channel occupies 22 MHz of
bandwidth, suggesting that the chip rate of its direct-sequence spread
spectrum modulation (DSSS) is about 44 MHz, which I naively think
means the coding gain is about a factor of 4, which suggests that
typical noise in the 2.4 GHz spectrum is in the range 10-100 picowatts
in the size of a Wi-Fi antenna, about 10-100 nW/m².

So generally, in a narrowband radio receiver (Q about 100 for Wi-Fi or
UHF TV), noise is hundreds of nanowatts and/or hundreds of nanowatts
per square meter.  As a spectral intensity in the VHF and UHF bands
this is a bit below a picowatt per Hz, less than four orders of
magnitude lower than the 3.7 nW/Hz I cited above as the *transmit*
power for global communication on HF.  (Of course these noise numbers
aren’t on HF, they’re on VHF and UHF and microwaves.)  This suggests
that maybe if you’re more than 100 antenna widths away from the
suggested 100 mW transmitter (say, 500 meters for a 5-meter antenna
aperture) it will be quieter than other sources of noise in your radio
system, if noise levels in HF and UHF and microwave receivers are
similar.

Maybe there are another three or four orders of magnitude in good
devices; in [the 02007 Microsoft White Spaces device test][ws],
Microsoft’s prototype device detected (UHF) digital TV broadcast
signals at -114 dBm in order to avoid transmitting on already-used
frequencies.  I don’t know if coding gain was involved here but I
imagine not.  The rated FM sensitivity of the man-portable AN/PRC-117
is -118 dBm.

[ws]: https://en.wikipedia.org/wiki/White_spaces_(radio)#Preliminary_test

Various countries have different EMI/EMC (“electromagnetic
interference”/“compatibility”) regulations.  The US’s influential
Class A (industrial)/Class B (residential) regulations for
*unintentional* radiation are perhaps typical.  For *conducted*
emissions, powerline EMI, class B is limited to 250 microvolts from
450 kHz to 30 MHz, and class A is limited to 1000 microvolts below
1.705 MHz and 3000 microvolts above.  For *radiated* emissions, class
A devices are limited to 49.5 dBμV/m at 10 meters away from 30 MHz to
88 MHz, 54 dBμV/m from 88 MHz to 216 MHz, 56.5 dBμV/m from 216 MHz to
960 MHz, and 60 dBμV/m from 960 MHz up (not sure how high).  Class B
device limits are typically 10 dB lower and measured at 3 meters.

It’s interesting that these numbers start above the HF band I’m
interested in, but I suspect that may be due to an unreliable source.

These tests are required to be done with a receiver with a bandwidth
of at least 100 kHz so that very narrowband, low-power emissions won’t
fail it.  [Rex Frobenius’s calculator][rf] and [Cantwell Engineering’s
calculator][cant] say 56.5 dBμV is -50.49 dBm with a 50-ohm system
impedance or -59.26 dBm with a 377-ohm system impedance.  If we were
to crudely estimate that 56.5 dBμV/m is -55 dBm/m² (3 nW) and a
10-meter sphere covers 1300 m², we’re talking about 4 microwatts
distributed over 100 kHz, or 40 microwatts per MHz.  So a device could
probably just about pass the EMC tests for a Class A *unintentional*
radiator even if it was radiating an entire milliwatt over the 3
MHz-30 MHz HF range.  (Or 10 milliwatts over 30 MHz-300 MHz, or 100
milliwatts over 300 MHz-3 GHz.  It’s interesting to note that 10
milliwatts is a lot higher than 50 microwatts.)

[rf]: https://www.rfmentor.com/content/dbm-dbw-dbuv-calculators
[cant]: https://www.cantwellengineering.com/calculator/convert/dbuv

The relevant IEC/CISPR standards are apparently CISPR 11/EN 55011,
emission limits and methods for ISM equipment; CISPR 32/EN 55032,
emission limits and methods for multimedia equipment; CISPR 14/EN
55014-1, emission limits and methods for household appliances; IEC
61000-6-3/EN 61000-6-3, emission limits for residential environments;
IEC 61000-6-4/EN 61000-6-4, emission limits for industrial
environments; and IEC 61000-6-8/EN 61000-6-8, emission limits for
commercial and light industrial environments.  EN 61000-6-4:2001 seems
to instead specify 30 dBμV/m at 30 m from 30 MHz to 230 MHz and 37
dBμV/m from 230 MHz to 1 GHz, though I’m not entirely sure.  I think
that in the far field this would be equivalent to 40 dBμV/m and 47
dBμV/m measured at 10 meters.  If my numbers above are correct then
we’re talking about 100 microwatts spread over 3-30 MHz or 1 milliwatt
over 30 MHz-300 MHz.

US MIL-STD-461G §4.3.10.3.1 specifies 100 kHz (6dB) measurement
bandwidth for emissions testing in the 30 MHz-1 GHz band but 10 kHz
for 150 kHz-30 MHz.  It also seems to be the most complete public
description of EMC test procedures, in particular §5.18.  The downside
of this is that the procedure is not the same as the FCC procedure or
the various IEC procedures.  For belowdeck applications their limit
ranges from about 68 dBμV/m at 3 MHz down to 56 dBμV/m at 100 MHz and
then back up to about 68 at 300 MHz, but this is measured at 1 meter,
[adding 20 dB to the measurement compared to the 10m
measurement][aemc]; for abovedeck or exposed belowdeck applications,
where I guess they want to be stealthy, the levels are 20 dB lower.
If I’m back-of-the-enveloping the figures correctly, this suggests
that, around 100 MHz, 30 picowatts per 100 kHz is the maximum you can
emit (and thus 10 nanowatts overall up to 300 MHz) without tipping off
the enemy to your presence.  Aircraft, army, and submarine
applications have more stringent limits, as low as 24 dBμV/m from 2
MHz to 100 MHz for aircraft.

[aemc]: https://www.academyofemc.com/emc-standards

This is all extremely crude, back-of-the-envelope stuff that might be
off by an order of magnitude or more, but it does seem to suggest that
global broadcasting over HF is likely to be easily detectable and
possibly even annoying regardless of how you modulate it unless the
effective bit rate is down in the millibauds, but spread-spectrum
line-of-sight communication over VHF and UHF should be doable even at
kilobaud speeds without creating even the most minimal interference
with existing systems.

Baseband spread-spectrum circuitry and decoding algorithms
----------------------------------------------------------

At up to 30 MHz, you could generate the noise signal as a 60
Mbps apparently random bit sequence and just switch the antenna
between a transmitting voltage and ground with a 100 MHz transistor
totem pole, NRZ style.  Suppose the bit sequence is predetermined and
known to the receiver.  A receiver can Fourier-transform their
baseband signal, notch out whatever frequencies have narrowband
interference (such as nearby shortwave radio stations; we really only
want to use frequency bands that contain only noise, from the
perspective of narrowband transmitters and receivers), and correlate
what’s left in the frequency domain with the (known) random bit
sequence, which can very reasonably repeat every few seconds.

For short-range transmission, avalanche transistor pulse generators
built with common transistors can generate wideband pulses with energy
up to a couple of GHz, and even a simple Schmitt trigger with
garden-variety transistors can generate steps at up to a few hundred
MHz.

536870912 samples from a 60 Msps ADC would cover 8.9 seconds and
require a couple gigs of RAM and 29 levels of FFT; this seems
demanding but not totally insane.  After doing the inverse transform
back into the time domain the receiver can see a spike indicating what
the time offset of the random signal is, with I think 87 dB of coding
gain, though I might be calculating that wrong.

In the specific case of the bit sequence from an LFSR, there is an
approach that is about 1-2 orders of magnitude cheaper; see file
`m-sequence-transforms.md` for details.

But just finding a single time offset is not very interesting; it only
tells you your clock skew relative to the transmitter’s clock.  The
transmitter can instead combine several copies of the apparently
random bit sequence, for example by majority rule, and all of the
resulting time offsets will have significant spikes at them.  Then the
time offsets between the spikes can communicate useful data.

In a sense this turns the usual “time-hopping” approach inside out:
you’re still using time offsets of a pulse sequence to transmit
different bits, but the pulse sequence length is much longer than the
time slot in which you are transmitting each individual bit, so you’re
transmitting multiple bits concurrently.  My notes in file
`circulant-fully-distributed-representation.md` suggest that the
correlation with any given lagged stream will fall in proportion to
the number of lagged streams being combined together.

Overall, this is similar to the direct-sequence BPSK spreading used by
GPS, where a “pseudo-noise” sequence reverses the phase of a carrier
at rates of 1.023 and 10.23 megabaud in order to transmit a 50-bps
signal (XORed with the PN), and which is well-known to work down to
ridiculously low RSSI levels like -210 dBm; I’m just proposing to do
this at baseband, and with time-hopping modulation of the data rather
than transmitting bits one at a time, in order to simplify
demodulation.  GPS uses 1023-bit Gold codes for its PN, but I was
thinking of using single LFSRs.  Many GPS receivers do use the
Fourier-domain correlation approach I described above for carrier
acquisition.

I did [some computational experiments][ratruco] with an approach
similar to this, and the preliminary results were promising.  Quoting
the summary:

> In one experiment, I represented a 2500-bit message with 128
> timeslots per bit, each timeslot transmitting either zero, a unit
> positive pulse, or a unit negative pulse, according to a 4096-bit
> spreading sequence, with 99.4% of the slots being zero (so each
> pulse transmits about 1.2 bits), and decoded it simply by convolving
> it with the (time-reversed) spreading sequence. The bit error rate
> in the experiment is 0.8%, which is pretty terrible for no
> noise. However, I was optimistic about this, because, to the
> demodulation process, the intersymbol interference looks like noise
> that’s 8dB above the signal before despreading. So it seemed to me
> that this level of robustness against ISI probably meant it would
> work with white noise too.
>
> In another experiment, I added white noise to that sparse signal,
> and decoded it from 8dB below the noise floor with a 13–14% bit
> error rate (BER) without ECC.
>
> In a third, I added white noise to the dense signal it was derived
> from (128 samples per bit) and decoded the signal from 18dB below
> the noise floor with a 7–8% BER [also without ECC].

[ratruco]: https://nbviewer.org/url/canonical.org/~kragen/sw/dev3/random-truncated-correlation.ipynb

In those experiments I represented the original bit stream as positive
and negative impulses, inserted 127 zeroes between them ([thus
replicating the baseband frequency spectrum][stuff] over 7 octaves),
convolved with the spreading sequence (an LFSR M-sequence also
represented as positive and negative impulses), and to get the sparse
signal, I picked the 0.6% of samples with the largest magnitude.

[stuff]: https://www.dsprelated.com/showarticle/761.php

Of course, 18 dB is a long way from 81 dB.  I’d be more confident if I
had a 30 dB result with 1024x zero-stuffing.  Actually, it would also
be pretty useful to measure the BER of the 128x-stuffed signal at 21.1
dB below the noise.

Existing inconspicuous radio signals seem to be using OFDM or BPSK,
and MIMO for, among other things, jamming rejection.

Antennas
--------

For transmitting milliwatt-level signals, you might be able to use a
loopstick ferrite rod antenna from a mass-market radio receiver for MF
AM or shortwave AM, maybe rewound with much thicker wire.  Normally
loopsticks are nonviable for transmission because the ferrite
overheats, and they’re always inefficient, but it’s totally reasonable
for a plugged-in radio transmitter to use 10 or 100 watts, 24/7, or
for a solar one to use 1000 watts when the sun is shining, which is
precisely when HF shortwave propagates farthest.

The Wikipedia page mentioned above gives 50 dB as a typical loss due
to [radiation inefficiency][radeff], which would be 1 milliwatt out of
100 watts.  But of course if you’re heating up your antenna with 100
watts of ohmic losses you’re going to need to cool it pretty
aggressively to keep it from melting even if it isn’t the ferrite
that’s heating up!

[radeff]: https://en.wikipedia.org/wiki/Radiation_efficiency

If you can instead transmit from a spatially large antenna, maybe a
“loop-on-ground” (“LOG”) type or a curtain, you might be able to both
make the signal highly directional and distribute it over a large
enough area that it’s less likely to cause unwanted interference in
any given area.

Antennas are less of a difficulty for the VHF and UHF frequencies.
Quarter-wave dipole UHF antennas are in the 25-250 mm length range,
and “large loop” antennas on the order of the wavelength are also
commonly used for UHF TV reception.

Relevant frequency allocations
------------------------------

You might want to try to notch out transmissions in frequency bands
where interference is especially likely to anger people.  Some
relevant allocations:

* worldwide, 25-28 MHz: illegal “freeband” CB radios

* 27.12 MHz: RF diathermy

* US, 30–88 MHz: “Military VHF FM, including SINCGARS”, but:
    * 43-50 MHz is also cordless phones and RC toys
    * 50-54 MHz is the amateur 6-meter band
    * 50.8-51 MHz is ham RC model airplanes
    * 54-88 MHz is VHF “band I” including TV channels 2-6, non-ham
      model airplanes, and near-future digital TV
        * 74.8-75.2 MHz is air navigation beacons
    * SINCGARS is a frequency-hopping thing that operates on 2320
      25-kHz-wide channels filling up this range, so it’s already
      pretty resilient to interference

* Worldwide, 108–118 MHz: “Air navigation beacons VOR and Instrument
  Landing System localizer.”

* Worldwide, 118–137 MHz: “Airband for air traffic control, AM, 121.5
  MHz is emergency frequency”

* Worldwide, 156.8 MHz (25 kHz bandwidth): channel 16, VHF marine
  radio emergency frequency.

* US, 156.075 MHz, 156.150 MHz, 156.225 MHz, 157.050 MHz, 157.075 MHz,
  157.100 MHz, 157.125 MHz, 157.150 MHz, 157.175 MHz (each 25 kHz
  bandwidth): VHF marine radio channels prohibited for public use
  (typical transmit power a few watts)

* US, 225-420 MHz: “Government use, including meteorology, military
  aviation, and federal two-way use” including military aircraft radio
  including HAVE QUICK (which is frequency hopping over this region),
  and AN/ARC-164 (10 watts transmit power, supporting HAVE QUICK),
  emergency frequency 243 MHz

* US, 806–816 MHz: “Public safety and commercial 2-way (formerly TV
  channels 70–72)”

* US, 849–851 MHz, not: “Commercial aviation air-ground systems
  (Gogo)” which turns out to just be in-flight internet access.

* US, 960–1215 MHz: “Aeronautical radionavigation”

* US, 1350–1390 MHz: “Military air traffic control and mobile
  telemetry systems at test ranges”

I’m not going to think much about stuff over 1 GHz, but there’s a
bunch of aircraft safety and landing systems, GNSSs, and stuff up
there.
