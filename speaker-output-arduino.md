Shae Erisson drove an 8Ω speaker directly from an Arduino output pin
for [his alarm clock][0].  Driving a low-impedance load such as that
speaker or [the 共振喇叭 LB07 4Ω 5W dynamic-loudspeaker surface
transducer Adafruit sells][1] from a logic output tends to demand more
current than the logic output can safely supply, even when the
absolute power needed is reasonable.  And Shae says he did in fact
burn out a number of pins on that Arduino by driving them too hard.

[0]: https://github.com/shapr/HArdalarm/
[1]: https://www.adafruit.com/product/1784

The [ATMega328P][2] used on that Arduino (and most Arduinos) is rated
for 40mA on a pin.  If you were to pull a pin high in your Arduino
code and tie it to ground through an 8Ω resistor, you would expect to
get 5V ÷ 8Ω = 625mA, which would surely burn the pin out, being 15
times more current than it’s rated for.  In fact, the AVR family is
pretty good at avoiding that, most of the time, especially for brief
short-circuit conditions on a single pin; I forget but I think it only
delivers about 50mA in practice.  But you can definitely exceed its
limitations.  How can you avoid this?

[2]: https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf

The first helpful step is to eliminate the dc current.  When you’re
driving a speaker, what you’re after is sound, and dc current doesn’t
make any sound.  Worse, the actual dc resistance of the speaker is
much smaller even than the already-small 8Ω or 4Ω rating at signal
frequencies.  You can use a series ac-coupling capacitor so that the
speaker only sees the ac component of the signal output from the
microcontroller.  At dc a capacitor is an open circuit with infinite
impedance, and as *f* → ∞, its impedance approaches 0, a short
circuit.  This is what you want.  In particular, you want its
impedance to be small relative to the speaker’s rated impedance.  So,
maybe if the speaker is 8Ω, you want the capacitor to be 0.8Ω, say at
100Hz and up.  (For a hi-fi audio system, you'd shoot for 15Hz or so,
but that’s more challenging.)

The impedance of a capacitor of capacitance *C* at frequency *f* is
*Z<sub>C</sub>* = -*j*/(2*πfC*).  Ignoring the negative sign and the
imaginary unit *j* and solving for *C*, we have *C* =
1/(2*πfZ<sub>C</sub>*), which works out in this case to 1990μF.  You
can definitely get 2200μF electrolytic capacitors, and any of those
will work fine if you put them in series with the speaker with the
right end pointed up.

You still have the problem that, every time you change the polarity of
the pin, you get a theoretically 625mA current spike that then decays
with an exponential time constant of *τ* = *RC* = 18μs if we assume
that the 8Ω impedance is the relevant *R* here.  So you can still
overheat your pin by changing the polarity often enough.

One possible solution is to use an audio output transformer.  In this
case, the ATMega328P can safely drive an impedance of 5V ÷ 40mA = 125Ω
or more, and we only have an impedance of, say, 8Ω.  √(125÷8) ≈ 3.95,
so we want a transformer with a turns ratio of at least 4:1, so the
output voltage is one fourth the input (±625mV; the transformer output
is automatically dc-balanced) and the output current is four times the
input (160mA).  A higher ratio will work fine but won't deliver the
full 50mW the [2.5V ac component of the] AVR’s output is rated at.

Audio transformers are a [standard electronic product category][3],
but they’re fairly expensive ([US$5.88][4] for a high-resistance one
with 52Ω in its secondary, [US$10.75][5] for one with 1Ω in the
secondary which you could actually use for this) which I think is
because their frequency response is flat to very high precision,
sometimes only 0.5dB over their rated range — which usually only goes
down to 300Hz.

[3]: https://www.digikey.com/en/products/filter/audio-transformers/162
[5]: https://www.digikey.com/en/products/detail/triad-magnetics/MET-23-T/5400091
[4]: https://www.digikey.com/en/products/detail/triad-magnetics/TY-303P/242648

You could probably use a regular power transformer with a roughly 1:4
windings ratio, or wind one yourself on a ferrite core.

A more common approach nowadays is to use a very-low-impedance
amplifier stage to drive the larger current.  In the land of linear
circuits, you don’t need any voltage gain — an [emitter follower][6]
or [source follower][7] would be fine — but you need a lot of current.
If you want to pump 5W into the 4Ω surface transducer linked above,
you need √(5W/4Ω) = 1100mA rms, which for a sine wave would be 3100mA
peak to peak.  You can use a [71¢ TIP120 darlington][8] and be good to
5 amps with a guaranteed 1000× current gain which starts to roll off
around 20kHz; then you can get 3100mA out of it with only 3.1mA in,
which the ATMega328 can easily supply (with the necessary series
resistor of, say, 2.2kΩ).  At 5 volts minus the TIP120’s two 700mV
diode drops, though, the ac component of your output signal only has
3.6V peak-to-peak, 1.8V peak, and 1.3V rms, so *V*²/R is only 420mW.
Dank, dude, but that’s quieter than what this speaker can handle by
more than 10dB.  You’d need about to amplify the TIP120’s base signal
by a 3.5×-voltage-gain “preamp” running off a 14-volt-or-better power
supply to get this.  This is [a common way to build linear audio
amps][9] to drive speakers.

[6]: https://www.falstad.com/circuit/e-follower.html
[7]: https://en.wikipedia.org/wiki/Common_drain
[8]: https://www.digikey.com/en/products/detail/onsemi/TIP120G/920325
[9]: https://forum.allaboutcircuits.com/threads/is-there-any-use-for-the-active-region-of-a-darlington-transistor.168738/

A difficulty with this is that you’re always dropping 1.4 volts in the
emitter follower darlington and some more power in the emitter
resistor.  3.1 amps at 1.4 volts is 4.3 watts, which the TIP120 can
handle, but only with a hefty heatsink, and pretty disappointing if
you’re only getting 5 watts out of the speaker itself.  If you use a
class-D switching approach, you can basically eliminate those losses;
better still, you can double your output voltage by putting the
speaker across an H-bridge, because you can switch the speaker between
+5V and -5V.

A low-loss H-bridge is easiest with P-channel and N-channel MOSFETs
like the [IRLML6344][10] (52¢) and [IRLML6402][11] (46¢).  These are
“logic-level” MOSFETs which can be comfortably switched with a gate
voltage under five volts (threshold of 0.8V/-0.55 typ, 1.1V/-1.2V
max), and 22mΩ/50mΩ on-resistance with 4.5V/-4.5V on the gate.  With
2.4nC/2.8nC of total gate-to-drain charge, the ATMega’s rated 40mA
should theoretically be able to switch them in 60 or 70ns, so you
should be able to switch them at 1MHz or so before they start heating
up.

[10]: https://www.digikey.com/en/products/detail/infineon-technologies/IRLML6344TRPBF/2538152
[11]: https://www.digikey.com/en/products/detail/infineon-technologies/IRLML6402TRPBF/811437

These transistors are kind of overkill since they’re rated for 20 or
30 volts and 5 amps, so like potentially 100 watts of output.  But at
10 volts peak to peak, the maximum *V*²/R they could produce into 4Ω
would be about 6W, and only 3W with a sinusoidal output.  Which is
still probably plenty.

Around here, I think the only IRL\* MOSFETs I can get are
[IRLZ44N][12], US$2.48 each (though [at Digi-Key they’re
US$1.28][13]), so if I wanted to build a MOSFET H-bridge, I’d probably
want to use power MOSFETs and drive the gates of the MOSFETs with BJT
switches from a power supply that can supply the 10 volts or so needed
to turn them all the way on.  Local places do stock [2N7000s][14], of
course, for 30¢ (though they’re out of stock, and STMicroelectronics
has [declared their 2N7000 “obsolete”][15], [ONSemi][16],
[Diotec][17], and [NTE][18] still have theirs “active”), but those
have the same threshold-voltage issue for driving directly from the
microcontroller, and they’re too high impedance to drive such a
speaker.

[12]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRLZ44N
[13]: https://www.digikey.com/en/products/detail/infineon-technologies/IRLZ44NPBF/811808
[14]: http://www.sycelectronica.com.ar/articulo.php?codigo=2N7000
[15]: https://www.st.com/resource/en/datasheet/cd00005134.pdf
[16]: https://www.digikey.com/en/products/detail/onsemi/2N7000/244278
[17]: https://www.digikey.com/en/products/detail/diotec-semiconductor/2N7000/13164314
[18]: https://www.digikey.com/en/products/detail/nte-electronics-inc/2N7000/11654738

Example suitable power MOSFETs in stock locally might include
[IRF510][19] (100V, 5.6A, US$1.98), [IRF520][23] (100V, 9.2A,
US$2.48), [IRF630][20] (200V, 9A, US$1.98), [IRF640][21] (200V, 18A,
US$2.25), [IRF9530][22] (100V, 12A, US$2.85, P-chan), [IRF9640][24]
(200V, 11A, US$2.85, P-chan).  These are super overkill for this
application and consequently slower to switch, but you need a BJT or
something to drive them from an Arduino anyway.

[19]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRF510
[20]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRF630
[21]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRF640
[22]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRF9530
[23]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRF520
[24]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRF9640

A bit disappointing is that, though SyC does stock the [IRF740][25]
(400V, 10A, US$2.36) needed to switch the peaks of a 240VAC line
(nominally 339V), they don’t carry the complementary P-channel type,
so you have to bootstrap up to 10V above your 340VDC power supply to
use N-channel MOSFETs for your high-side switching too.

[25]: http://www.sycelectronica.com.ar/articulo.php?codigo=IRF740

As a half measure, you can build a push-pull amplifier output (half an
H-bridge) and drive the speaker from it with an ac-coupling capacitor.
This way you can pull the speaker up to +5V and down to 0V.  This
gives you potentially a quarter of the power of the full H-bridge.  If
you use BJTs for it, you lose maybe a couple hundred millivolts.  An
[S9015 pnp][26] can only handle 100mA, but costs under a penny from
JLCPCB (or 11.5¢ from Digi-Key), and gets you to 300mV from the
positive rail.  The [MMBT5401 pnp][27] is supposed to be good for
600mA and is more like 500mV saturation, and the [MMBT3904 npn][28] is
200mA and 300mV.  There are beefier surface-mount BJTs like the
[DNBT8105][29] (npn, 1000mA, 500mV).

[26]: https://www.digikey.com/en/products/detail/evvo-semi/S9015/21407132
[29]: https://www.digikey.com/en/products/detail/diodes-incorporated/DNBT8105-7/989945
[28]: https://www.digikey.com/en/products/detail/diodes-incorporated/MMBT3904-7-F/814494
[27]: https://www.digikey.com/en/products/detail/diodes-incorporated/MMBT5401-7-F/804878

To avoid shoot-through current, you can drive this push-pull amplifier
with a break-before-make waveform generated in software on two pins.
