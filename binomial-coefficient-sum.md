I was thinking about binomial coefficients as I went to the
supermarket last night, because I was thinking about designing
constant-weight codes (see file `base-4-barcode.md`).  It occurred to
me for the first time that the number of 6-bit bitstrings with 3 bits
set is the number of 5-bit bitstrings with 2 bits set (which
correspond to 6-bit bitstrings with an additional bit set, say the
first) plus the number of 5-bit bitstrings with 3 bits set (which
correspond to 6-bit bitstrings with that same additional bit not set).
This generalizes to bitstrings of *k* > 0 bits with 0 < *n* < *k* bits
set, and thus to binomial coefficients *n*C*k*.  And that’s why the
pairwise addition in Pascal’s triangle gives you the binomial
coefficients.

The special cases of Pascal’s triangle are for row 0, representing
0-bit bitstrings, for which the reduction to *k* - 1 bits does not
apply, so the recursion must bottom out there with 0C0 ≡ 1; for column
0, representing the bitstring with no bits set, so the newly added bit
must be clear; and for column *k*, representing the bitstring with all
bits set, so the newly added bit must be set.  The column special
cases can be pushed back a step by declaring that (-1)C*k* ≡ 0 ≡
(*k*+1)C*k*, but that doesn’t really eliminate them.

Wikipedia explains this as follows:

> The formula follows from considering the set {1, 2, 3, ..., *n*} and
> counting separately (a) the *k*-element groupings that include a
> particular set element, say “*i*”, in every group (since “*i*” is
> already chosen to fill one spot in every group, we need only choose
> *k* − 1 from the remaining *n* − 1) and (b) all the *k*-groupings
> that don’t include “*i*”; this enumerates all the possible
> *k*-combinations of *n* elements.

I’ve enjoyed Pascal’s triangle since I was 8, but I’d never understood
this explanation for the simple recursive rule for computing it.
Probably Martin Gardner and Lancelot Hogben tried to explain it to me,
but I didn’t understand it until I thought it up myself walking to the
supermarket.

Another way of analyzing the situation is that a group of 6 bits with
3 bits set, such as 010110, can be divided into a group of two
leftmost bits and a group of four rightmost bits: 01:0110.  Then there
are three possibilities: the left group has 0 bits set and the right
group has 3; the left group has 1 bit set and the right group has 2;
and the left group has 2 bits set and the right group has 1.  So we
should expect 6C3 to be 2C0·4C3 + 2C1·4C2 + 2C2·4C1 = 1·4 + 2·6 + 1·4
= 4 + 12 + 4 = 20, and indeed it is.  So we’ve taken the dot product
of row 2 of Pascal’s triangle with a segment of row 4 to get row 6.
And in general we can convolve row *m* ≥ 0 with row *p* ≥ 0 to get row
*m* + *p*.  This corresponds to the fact that polynomial
multiplication amounts to convolving coefficient vectors.

I don’t think that’s actually computationally advantageous, though.
