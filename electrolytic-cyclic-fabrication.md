There are a bunch of ways electrolytic processes can be useful to cyclic
fabrication systems.

Batteries
---------

Electrolytic processes are a great way to store energy for later use.

Electroforming or electrocrystallization
----------------------------------------

Among others, nickel, copper, zinc, lead, and iron can be productively
electrodeposited from aqueous systems, and electrodeposition can be
controlled at high resolution to produce arbitrary shapes.  As I wrote in
file `freezer-seacrete.md`, anodic deposition of minerals such as calcium
carbonate and magnesium hydroxide may offer even higher deposition rates,
and it has the advantage of being able to fabricate structures that are
not electrically conductive.

In particular for smaller-scale machinery the traditional thermal
methods of forming metals may be inefficient or even infeasible, while
electroforming keeps working all the way down to the nanometer scale; the
copper interconnects on modern integrated circuits are electrodeposited.

Even when not made selective by spatial control by machinery (“Ec
Printing”), metal electrocrystallization can be useful in several ways:
it can thicken existing conductive objects (even paper-thin coatings of
graphite paint), it can add other coatings with different properties to
them such as corrosion resistance, it can join together separate parts
like welding, and it can incorporate nonconductive materials such as
abrasives into the object being formed through so-called codeposition.

Perhaps even more interesting, some semiconductors can also be
electrodeposited.  Historically electrodepositing silicon or germanium
normally required a molten salt bath well above room temperature,
but nonaqueous room-temperature approaches have been found since the
01970s.  Similarly lots of III/V semiconductors like GaAs and InP have
been electrocrystallized, but only from molten salts.  However, lots of
II-VI semiconductors like CdS have been electrocrystallized from aqueous
solutions, and several researchers have reported the construction of
working CdS transistors (they didn’t use electrocrystallization to make
them, but so-called “CBD” deposition is common).  In fact, the very
first thin-film transistor 60 years ago was CdS.

Electrowinning
--------------

Many metals can be electrowon, in some cases even from aqueous solutions,
and electrometallurgy has been a significant refining process for a
century and a half.  Similar remarks about machinery scale apply here.

Precise voltammetry can in many cases separate metals that are otherwise
fairly similar.

Electrolytic machining
----------------------

Cutting metals with anodic dissolution is a particularly promising
process; alternating this with electrodeposition can reduce the problems
of dendrite growth and imprecision that occur at high electrodeposition
rates.

Electrolytic machining does not raise burrs and can be arranged to
either electropolish surfaces or roughen them.

There is no general relationship between metals’ resistance to mechanical
stress and their resistance to anodic dissolution, so electrolytic
machining is capable of cutting even very hard metals and sometimes
conductive ceramics such as tungsten carbide.  (Generally the hardest
metals are also the most refractory.)

Material systems
----------------

We can list metals by their volume electrochemical equivalent, which
tells you how much metal you get per unit of current assuming perfect
Faraday efficiency.  One micron decimeter squared per amp-hour is 0.01
milliliters per amp-hour or 2.78 nanoliters per coulomb; here are figures
from Gamburg and Zambari’s textbook:

- 30-40 microns decimeter squared per amp-hour: silver (38.37),
  monovalent gold (38.0), lead (34.09), and tin (30.33).
- 20-30: monovalent copper (26.52) and cadmium (24.24).
- 10-20: indium (19.56), zinc (17.11), platinum (16.96), palladium
  (16.51), bismuth (15.9), manganese (13.74), antimony (13.59), divalent
  copper (13.26), divalent iron (13.24), aluminum (12.4, not listed in
  the textbook), nickel (12.29), trivalent gold
  (12.7), cobalt (12.43), rhodium (10.32), and ruthenium (10.17).
- 5-10: trivalent iron (8.83), molybdenum (6.70), and tungsten (5.87).
- 0-5: rhenium (4.72) and chromium (4.49).

See file `ecm-notes.md` for the aluminum calculation.

It’s worth looking at the Pourbaix diagrams of each metal, though; some,
like aluminum, cannot be electrodeposited from aqueous solutions, and
others, like platinum, cannot be anodically dissolved.  I have this
vague idea that the energy-efficiency of deposition or dissolution,
if they’re possible at all, depends greatly on the voltage, but maybe
that’s not true.

Generally current density rises exponentially with overvoltage, though
different metals can require over an order of magnitude difference in
the overvoltage required to reach a given current density, and large
overvoltages create fine-grained, dense coatings.

Gamburg and Zambari also give typical cathodic efficiencies (Faraday
efficiencies) for different electrocrystallization processes in their
introduction: most are above 80%, but chromium from chromate is 18%;
copper from sulfate is 100% and from pyrophosphate is 99%; iron from
chloride is 90%, sulfate is 92%, and from fluoborate is 95%; nickel from
sulfate is 96%; antimony from citrate is 94%; tin from pyrophosphate is
90%; zinc from sulfate is 97%.  They also give numbers for gold, silver,
cadmium, cobalt, lead, palladium, and rhenium, and some other anions.
