[there is never a good reason to use bubble sort](https://news.ycombinator.com/item?id=37206668)

[plausible chemistries for non-water-based life](https://news.ycombinator.com/item?id=37935484)

[experience translating code between languages with gpt-4](https://news.ycombinator.com/item?id=38005767)

[the h-curve morphic word](https://news.ycombinator.com/item?id=38046595)

[an overview of surprisingly simple didactic compilers i've written](https://news.ycombinator.com/item?id=38184366)

[how the economics of nuclear and solar energy compare and have changed over time](https://news.ycombinator.com/item?id=38202759)

[how i overcame common beginner problems in ocaml](https://news.ycombinator.com/item?id=38235991)

[discussion of approachable parsers, font renderers, and dsl design](https://news.ycombinator.com/item?id=38242178)

[key points of programming with gpt-4](https://news.ycombinator.com/item?id=38257583)

[quantitative analysis of replacing fossil fuels with synfuels](https://news.ycombinator.com/item?id=38202197)

