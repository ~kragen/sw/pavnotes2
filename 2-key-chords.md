I just tested this piece of shit Genius LuxeMate 100 keyboard for
2-key rollover, and it passed.  It’s roughly the cheapest USB keyboard
you can buy, and the keys sometimes mechanically jam partway down if
you press them off-center.  So I think that if it passes, probably
every shitty keyboard you can get passes, even though 2-key rollover
isn’t really required for normal operation.  It just falls out of how
keyswitch matrices work when you write the firmware to avoid 2-key
ghosting: you can reliably detect key releases.

The keys I tested were this set of 38 easily reachable from the home
row:

    TAB q w e r t y u i o p [ ]
    CAPS a s d f g h j k l ; ' RET
    SHIFT z x c v b n m , . /
               SPC

I used Jamie Zawinski’s xkeycaps program.  For each ordered pair of
keys from this set, I pressed the first key down, pressed the second
key, released the first key, and released the second key, in that
order, verifying visually that all four events registered.  This took
me about 36 minutes, which is why I didn’t also test \\, `` ` ``,
backspace, and the number row.

It was easy to construct 3-key combinations that demonstrated jamming;
for example, of the set ‘ijk’, this keyboard can register any two
pressed at a time, but not all three, regardless of the sequence.  If
you hold all three keys and release one of the two keys whose pressing
has been registered, the keyboard recovers and registers a delayed
press on the third.  This is typical, though the particular key
combinations that jam vary somewhat.

I noticed that some 2-key chords from this set were immensely easier
than others, just at a biomechanical level.  Chords using one finger
from each hand, such as au or sm, are very easy.  Chords using two
adjacent keys in the same column, such as qa or mj, are the hardest.
Chords using multiple fingers from the same hand vary quite a bit; if
neither finger is the ring finger and either one finger remains on the
home row or both fingers are on the same row, they’re not too bad.
So, for example, we or jo is fine, almost as easy as alternate-hand
chords.  Chords that simultaneously require extending the ring finger
and retracting the pinky are almost as hard as adjacent keys in the
same column; these are qx, zw, .p, o/, and their four reversals.
Multiple keys that are supposedly on the same finger but in different
columns, like ft or nj, are not that bad, because without thinking
about it, I move my hand and press one of the keys with the wrong
finger.

I feel like these biomechanical observations merit further
investigation, using some kind of video game.

My objective here is to investigate quasimodal command-key user
interfaces.  (I say “quasimodal” but I’m definitely talking about
giving the letter keys a different *default* action than inserting
letters.)  The 38 first keys might, for example, designate 38
candidate objects to be selected, or actions whose repetition is
useful.  While holding down such a “noun” or “verb” key, you might
press one or more “operation” keys to apply to it.  Or, as is
conventional, some of the keys might be used as “shifts” to select a
different “layer” of the keyboard, multiplying the available number of
initial selections.

38² = 1444 operation combinations available in a single keychord,
though some too awkward to be desirable.  But, as I said, you could
keep appending or toggling options with single keystrokes before
releasing the initial key.

Another idiom that I’ve found workable in the past is to use a single
key as *either* a quasimodal modifier *or*, if no other key is pressed
while it’s down, as an action key.  I tried this with my half-keyboard
patch to the X server: holding the space bar mirrored the keyboard
left to right until you released it, so that you could type with only
your right hand, but pressing and releasing it without pressing any
other keys would insert a space.

Some example ideas:

- in a 3-D fighting game, you might use a right-hand key to open a
  weapons menu, a spells menu, or a potions menu, and while holding
  it, use a left-hand key to select an item from that menu.  Or, to
  permit using mouselook with the right hand, you could use an
  index-finger key (assuming WASD position, that would be e, c, r, f,
  or v, or with more difficulty, t, g, or b) to open those menus.
  Releasing the menu key without making a selection might close the
  menu without making a selection, or it might, for example, swap the
  primary and alternate weapon.

- When typing, the Caps Lock key might work as a second shift key to
  select another keyboard layer, like the Zeichen key on the Erika
  Modell 3 portable typewriter; in particular, this would be useful
  for putting digits on qwertyuiop and other symbols on the other
  letters and [];',./ keys, and maybe SPC and RET too.  Combined with
  the usual shift key, this would give a repertoire of 105 easily
  accessible graphical characters.

- A potentially more useful key to thus instrumentalize as a second
  shift would be ; or ', not just for mnemonic value, but because it
  could be combined with the normal left shift key to give four layers
  instead of three.  You can’t easily do that with the Caps Lock key
  because you have to either activate both shifts with your left pinky
  or move your left hand two keys to the left to use your pinky and
  ring finger, which both is awkward in itself and complicates typing
  letters like t or b.  And on this keyboard, the combination
  Shift/Caps-Lock/s is a jamming combination.

  Thus conscripting both “;” and “'” would give us six layers: plain,
  “;”, “'”, shift, shift-;, and shift-;, but that’s probably too many
  characters to learn.
  
  This does require three-key chords, which I’ve said aren’t reliable,
  and indeed on this keyboard Left-Shift/'/[ and Left-Shift/'/[ are
  also jamming combinations.  As it happens, they're also very
  difficult to type, but a different shitty keyboard might make worse
  jamming combinations here.  So maybe a better way to get four layers
  is with Shift, "'”, and “;” as quasimodal keys.
  
  A probably fatal drawback of the choice of “'” is that overlap of
  “'” with a letter in words like “won’t” is probably common.

- For autocompletion in a text-editing scenario, it would be nice to
  bring up a popup menu of completions explicitly with the TAB key,
  and you could type multiple graphical characters before releasing
  it.

- In a file management application, you might have single keys to
  scroll a file list, go back to the previous directory, or swap
  panes, but most other operations you might want to invoke require an
  operand, usually a file.  Common file operations are “open”, “copy”,
  “rename”, “move”, and “delete”.  In a dual-pane program like
  Midnight Commander, the destination of “copy” or “move” can be
  implicitly the other pane, but it’s common to want to apply the same
  operation to several files.  So it might make sense to have “copy”,
  “move”, and “delete” quasimodal keys, perhaps “c”, “v”, and “x”,
  during the holding of which you could press letters with the right
  hand to select any of about fifteen files in a “focused area”.
  You’d probably want to have an “undo” for the delete.  Other useful
  quasimodes in this context might include changing the sort order
  (interclick a letter to choose the sort column) and filtering by a
  search string (type the search string before releasing the find
  key).

- Alternatively, you could make your file management purely noun-verb,
  like the Macintosh Finder, with quasimodal letters to select a
  particular file on which to open a menu of operations.  This seems
  more useful for other applications, where different objects have
  different possible operations applicable, or where you’re likely to
  want to invoke a series of options on a single object.

- In a database query application, you might have a quasimodal letter
  for each visible column, which when pressed and released executes a
  common and harmless operation such as sorting by that column, but
  permits interclicking other letters for options like joining to
  another table, eliminating the column, moving it around in the
  layout, or changing its display width (though the mouse is probably
  a lot better for that).

