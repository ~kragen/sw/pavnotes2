I'm trying to hack together a low-power computer using the Ambiq
Apollo3.  One question is how to handle audio input on a submilliwatt
power budget.  Interestingly, the SparkFun Artemis RedBoard seems to
include a built-in microphone, which I haven't tried and whose power
usage I haven't measured.

Suppose you want to use a dynamic (moving-coil) speaker as a
microphone.  These are I think typically about 2% efficient as
speakers and a relatively low-impedance source, like 8Ω; to get
maximum energy transfer to a measurement thing you'll want a similar
impedance.  120 dB SIL is IIRC 1W/m² and a speaker driver might be
50mm in diameter, 0.002m², so someone talking at 70 dB is .00001W/m²,
so the energy hitting the speaker is 20nW.  If we assume the 2%
efficiency is the same both ways, we get 400pW.  Over 8Ω this is 60μV
and 8μA.  So it's not a super large signal.

What if we just try to feed it directly into the base of a
common-emitter BJT amplifier with some emitter degeneration?  We have
some biasing resistors on the base to bias it into conduction, and we
can probably count on a β of about 50 or so if not better, so our 8μA
could control a 400μA swing up and down from the basic bias current.
We don't really want that much current, though; let's say our budget
for this preamp stage is 50μW at 3.3V, which means the current should
average around 15μA, with possible swings up and down of as much as
15μA.  And we probably don't actually want to saturate at 70 dB SPI,
we just want to be able to detect it.

Do we really need a preamp at all for that?  60μV on a scale of 0 to
1.5V (the Apollo3's internal reference voltage in 1.5V mode) is
1/25000 of full scale, so with a 16-bit ADC, it would be a change in
the two least significant bits, about 87 dB below saturation.  But the
Apollo3 ADC is only 14 bits, so it would be just below detectability.
We'd rather have it, say, 30 dB below saturation, 57 dB higher.  So
we'd like a gain of about 800 in the voltage.  That is, 8Ω is not
enough impedance to directly connect to the ADC.

A perfectly passive approach is to connect our speaker to a 30:1
audio-frequency stepup transformer, which is connected to another 30:1
stepup transformer, and thus get 900 times more voltage, at the
expense of 900 times less current: 9nA.  In effect we've increased the
impedance by a factor of 810'000 to 6.4 megohms.  This is probably not
enough current because the ADC's leakage current is specified as 0.5nA
typical, 50nA max; but it might be, because channels 1-7 are specified
as 3.6GΩ typical, 180MΩ minimum.

Regardless, you don't really want megohm source impedances floating
around on a regular circuit board; a microamp of EMI will show up as a
volt, or in this case 6.4 volts, of interference.

Maybe a more sensible thing to do is to use a single 30:1 stepup
transformer which also acts as a balun, grounding one end of the
30N-turn secondary, which then has a more reasonable output impedance
of 7.2kΩ and a voltage swing for our 70dB sound of 1.8mV.  Then you
can use this single-ended signal to drive a single-transistor
amplifier to get, say, another factor of 10, before feeding it to the
ADC.

If we budget for a quiescent current of 15μA, we'll want, say, a
220kilohm collector resistor and a 22kilohm emitter resistor, giving
13.6μA.  XXX this is wrong because it ignores Vce.  The impedance
looking into the base is several megohms, which entitles us to use,
say, a couple of 220k resistors to bias the base.  XXX this probably
is on the right track but I don't remember how this stuff works.

Without knowing what I'm doing, I got this simplified version
simulating reasonably in Paul Falstad's circuit.js:

    $ 1 0.000005 42.05934401203833 60 5 53 5e-11
    r 208 176 208 272 0 470000
    R 160 160 112 160 0 1 40 0.05 0 0 0.5
    R 208 32 160 32 0 0 40 10 0 0 0.5
    r 208 32 208 144 0 470000
    c 208 144 320 144 0 0.000001 1.4131185440768723 0.001
    r 320 144 320 272 0 10000000
    g 320 272 320 304 0 0
    O 320 144 384 144 0 0
    w 208 176 256 176 0
    c 256 176 256 272 0 4.7e-7 8.454940787749566 0.001
    w 208 272 256 272 0
    f 160 160 208 160 0 1.5 0.02
    R 208 272 208 304 0 0 40 -10 0 0 0.5
    o 1 128 0 4098 0.078125 0.000048828125 0 2 1 3
    o 7 128 0 4098 10 0.0000030517578125 1 1

This has an N-MOSFET with a 1.5V threshold and β=0.2 operating as a
common-source amp with a 40Hz 50mV peak signal on its gate.  Its drain
is pulled toward +10V with a 470k drain resistor, while its source
goes to -10V through a 470k source-degeneration resistor; to get gain
that's bypassed with an 0.47μF cap in parallel.  The output (into a
10M output load) is taken from the drain, coupled through a 1μF
ac-coupling cap.  In Falstad's simulator the output is 2.735V at peak.
Typically the resistors are using about 20μA, but admittedly this is a
higher voltage.  But it doesn't cope well with higher frequencies like
4kHz, saturating quickly, and the sim omits things like Johnson noise.

This makes me think the preamp problem is not that hard to solve (even
without the audio transformer) but that I have a lot to learn.

In Horowitz & Hill 3ed., §2x.4.5 is about simulating a two-transistor
differential amplifier and simulating it reliably in SPICE.  A diff
amp would have the advantage of being mostly immune to EMI on the wire
to the speaker.  The design they have there is bipolar and uses 20mW,
but I think that, if a high output impedance is okay (as it is here),
it could be adapted to a MOSFET long-tailed pair and use much less
power.  Moreover, it could produce a differential output signal, and I
think the Apollo3 has a differential-input ADC.

