What’s the simplest way to make a register allocator that works
reasonably, always runs very fast, and doesn’t blow up in pathological
cases?  The standard approaches are Chaitin’s graph coloring from
01981, which is NP-complete; [puzzle-solving][2], which [for most
architectures finds an optimal register allocation in polynomial
time][3], but dismayingly [is patented until 02027][4]; and [Poletto
and Sarkar’s linear scan from 01999][5].

[2]: https://llvm.org/pubs/2008-06-PLDI-PuzzleSolving.pdf "Register Allocation by Puzzle Solving, Pereira and Palsberg"
[3]: http://compilers.cs.ucla.edu/fernando/projects/puzzles/intro/
[4]: https://patents.google.com/patent/US8225295B2/en
[5]: https://dl.acm.org/doi/pdf/10.1145/330249.330250 "TOPLAS, vol. 21, no. 5, pp. 895-913, 01999"

Poletto and Sarkar’s linear-scan algorithm
------------------------------------------

[WP says][w] this is the algorithm used by V8 and HotSpot client.

[w]: https://en.wikipedia.org/wiki/Register_allocation

Poletto and Sarkar say their generated code comes within 12% of the
performance of the graph-coloring approach “for all but two
benchmarks”, but allocates registers to variables in a single
linear-time scan; the puzzle-solving people said LLVM used this
linear-scan algorithm at the time.

Here’s the algorithm: they scan through the live intervals of all
variables in order of increasing start point, maintaining an array of
active registers, sorted by end point; each new live interval is
inserted into the array after removing any dead intervals and, if
necessary, removing the last (furthest-away-ending) interval to make
room, unless it ends earlier than the new live interval.  That’s it.

They seem to be using variables rather than SSA-style values as their
unit of analysis, so the possibility of moving a variable from one
register to another for free every time you change its value doesn’t
enter into the analysis.

They say this is inspired by (algorithms inspired by) Belady’s
furthest-first strategy for virtual memory (IBM Systems Journal,
volume 5, number 2, pp. 78–101, 01966).

Over a wide range of benchmarks, they report that their approach takes
about 200–300 cycles per output instruction on a 168MHz UltraSPARC-I.

200–300 cycles per output instruction is a lot actually tho
-----------------------------------------------------------

That’s not much compared to GCC or LLVM, I guess.  However, without
register allocation, I think my entire JIT compiler (see file
`jit-bootstrap.md`) can take on the order of 64 cycles per output
instruction on a Cortex-M4, which is an in-order microarchitecture
like the [UltraSPARC-I][6] they were testing on, but not even
superscalar.  It has a shorter pipeline, though, 4 stages instead of
9.

[6]: https://en.wikipedia.org/wiki/UltraSPARC

That’s for a compiler written in C, though; I think I can pump up the
speed to something like 5 cycles per output instruction by writing it
in assembly (see file `hadix-compiler.md`).  Neither of these numbers
is directly comparable to 200–300, though, because my dumb JIT is
emitting about four times as many instructions as it ought to, and the
lack of register allocation is the main reason.  For example, the ARM
code it emits for bytecode 0x13, fetch local variable at index 3 onto
the operand stack, looks like this:

                stmia r4!, {r5}
                ldr   r5, [sp, #12]

With register allocation, this would typically compile to zero or one
instructions, because commonly used variable values will be in
registers.  My plan for emitting this code is to have two handlers
similar to the following (broken) code.  At compile time, r5 is a
buffer used to accumulate 32 bits of machine code at a time to be
appended to the machine-code buffer r4 points to the end of; r3 is the
current bytecode, and r6 is the pointer into the bytecode being
compiled:

    local_even: movt r5, #0xc420          @ stmia r4!, {r5}
                add  r5, #0x9d00          @ sp-rel ldr instruction
                and  r3, #0xf             @ extract local var index
                add  r5, r3
                str  r5, [r4], #4         @ append r5 to machine code
                movs r5, #0
                ldrb r3, [r6], #1         @ fetch next bytecode
                ldr  pc, [r8, r3, lsl #2] @ jump through even table

Assuming a single zero-wait-state data bus for data and instruction
fetches, no branch prediction, and a two-cycle pipeline penalty for a
taken branch, these 8 instructions should take 13 cycles and emit 2
instructions.  This includes the dispatch overhead for the next
bytecode.  To handle the case where the output instruction stream is
not currently aligned on a 32-bit boundary, there’s a second handler
which emits the same code (pointed to from the “odd table”, which r7
points to) which adds the stmia instruction into r5, emits it via r4,
and then clears r5 and sticks the ldr instruction in its appropriate
half, which should also take about 13 cycles.

Although this code is broken, I think it’s a good approximation of how
much work a working handler would need to do.

Two of the instructions (`add` and `and`) could be eliminated by using
a separate handler for each local variable index.  By the same token,
allocating a particular local variable to a register could be handled
with no extra time by redirecting its handler pointers in the table,
perhaps even for the duration of the compilation of a single
subroutine.

Anyway, that’s about 6.5 cycles per output instruction, but it’s
emitting about 2 instructions per bytecode rather than the 0.5 a good
compiler would, so it’s more like 26 cycles per ideal instruction.  In
this context, spending 250 cycles per instruction seems very
unappealing.  I want to maintain a small cache of JIT-compiled machine
code in RAM to execute from, potentially evicting subroutines from it
while they’re still on the stack, and recompiling them from in-RAM
bytecode on demand.  The alternative is just straightforward bytecode
interpretation, at about 10 cycles per bytecode, or 20 per ideal
instruction.  So I’d really like to keep the compilation time on the
same order of magnitude, or compilation will yield a slowdown instead
of a speedup.

Isn’t there an in-between approach that would be closer to 64 than to
256?

Compile-time register stack allocation
--------------------------------------

I was thinking about the simple stack-based register allocation
advocated (but not shown) in [Crenshaw’s 01988 chapter on expression
parsing][0] and shown in [Darius Bacon’s infix expression compiler
tutorial example][1].

    """
    After http://www.vpri.org/pdf/rn2010001_programm.pdf
    """

    from parson import Grammar

    def assign(v, exp):  return exp(0) + ['sw r0, ' + v]

    def ld_const(value): return lambda s: ['lc r%d, %d' % (s, value)]
    def ld_var(name):    return lambda s: ['lw r%d, %s' % (s, name)]

    def add(exp1, exp2): return lambda s: (exp1(s) + exp2(s+1)
                                           + ['add r%d, r%d, r%d' % (s, s+1, s)])
    def mul(exp1, exp2): return lambda s: (exp1(s) + exp2(s+1)
                                           + ['mul r%d, r%d, r%d' % (s, s+1, s)])

    g = Grammar(r"""  stmt :end.

    stmt  :  ident ':=' exp0   :assign.

    exp0  :  exp1 ('+' exp1    :add)*.
    exp1  :  exp2 ('*' exp2    :mul)*.

    exp2  :  '(' exp0 ')'
          |  /(\d+)/ :int      :ld_const
          |  ident             :ld_var.

    ident :  /([A-Za-z]+)/.

    FNORD~:  /\s*/.

    """)(**globals()).expecting_one_result()

    ## for line in g('v := 42 * (5+3) + 2*2'): print line
    #. lc r0, 42
    #. lc r1, 5
    #. lc r2, 3
    #. add r1, r2, r1
    #. mul r0, r1, r0
    #. lc r1, 2
    #. lc r2, 2
    #. mul r1, r2, r1
    #. add r0, r1, r0
    #. sw r0, v

[0]: https://compilers.iecc.com/crenshaw/tutor2.txt
[1]: https://github.com/darius/parson/blob/master/eg_calc_compile.py

This in effect uses the registers r0, r1, r2, etc., as an expression
evaluation stack, maintaining the stack pointer in the compiler.  This
code contains no maximum register number; given the input `'v :=
x+(x+(x+(x+(x+(x+(x+(x + (x+(x+(x+(x+(x+(x+(x+(x +
(x+(x+(x+(x+(x+(x+(x+(x + (x+(x+(x+(x+(x+(x+(x+(x + (x+(x+(x+(x +
x)))))))))))))))))))))))))))))))))))'` it emits instructions like `add
r35, r36, r35`.

Spills are always necessary sometimes
-------------------------------------

Crenshaw points out that on a real computer you always have a finite
number of registers, so even on MMIX, a sufficiently complex
expression has to spill to memory at some point:

> Well, the 68000 has eight data registers.  Why not use them as a
> privately managed stack?  (...) What we’re doing in effect is to
> replace the CPU’s RAM stack with a locally managed stack made up of
> registers.  For most expressions, the stack level will never exceed
> eight, so we’ll get pretty good code out.  Of course, we also have
> to deal with those odd cases where the stack level DOES exceed
> eight, but that’s no problem either.  We simply let the stack spill
> over into the CPU stack.  For levels beyond eight, the code is no
> worse than what we’re generating now, and for levels less than
> eight, it’s considerably better.

Normal compilers, of course, allocate registers to values of
variables, not just temporaries that exist during expression
evaluation, but even the simplest register allocation makes a big
difference, and takes almost no time in the compiler.  It might
actually make the compiler faster because it can emit fewer
instructions.  And if you’re compiling from a stack-based bytecode,
this may be the best you can do.  But Crenshaw’s suggested fallback
doesn’t seem adequate to me, particularly in environments like Thumb-1
code under the ARM EABI, where there’s only one conveniently usable
stack, and it has onerous alignment requirements.

How about a sliding window?
---------------------------

We can use our eight or however many accumulators as a sliding window
onto a notional underlying value stack, like a circular buffer.  The
topmost stack values are always in registers; others have been evicted
to memory.

### Half-spaces are prone to pathological cases if you have branches ###

The first approach that occurred to me was to divide the window into
two half-spaces.  Push into r0, r1, ... r7, and if we want to push
again, we need to wrap around to r0, but it’s full.  So we can evict
it to memory.  On ARM, this might involve inserting one instruction:

            push {r0-r3}

On RISC-V, we might invoke a millicode routine to do the dirty work,
or just insert some store instructions.  Either way we probably want
to reserve space in the function’s stack frame at function entry.

Once this has been done, we can do the originally planned push and
three more pushes before getting back to the other half-space and
needing to evict it to memory as well.  In the other direction, before
needing to restore r0-r3 from memory, we can pop the value pushed,
plus four more pops, before any further memory traffic.  There’s no
pathological pattern of pushes and pops on the compile-time stack that
will produce excessive code growth or spend excessive time on runtime
stack manipulation.

Let’s suppose that the compile-time value stack must always have a
consistent height at a given static location in the code.  The
correspondence between this compile-time stack height and the runtime
registers mapping the stack is not bijective:

- 0: empty.
- 1: r0.
- 2: r0-r1.
- 3: r0-r2.
- 4: r0-r3.
- 5: r0-r4 or r4.
- 6: r0-r5 or r4-r5.
- 7: r0-r6 or r4-r6.
- 8: r0-r7 or r4-r7.
- 9: r4-r7 + r0, or r0.
- 10: r4-r7 + r0-r1, or r0-r1.  And so on.

This becomes slightly more complicated when branches are added, for
example for `a ? b : c` conditional expressions.  At control-flow join
points, such as the beginning of a loop, or the end of a conditional,
we must ensure that the runtime representation is the same on all
inbound control-flow arcs.  The simplest way to handle this is that,
when there are two possible representations of a state, to declare one
of those two representations canonical, and normalize to it before
each such control flow join point.

Any such choice means that there are two adjacent stack heights with
different sets of registers canonically containing stack values.  This
means that code like the following will provoke pathological behavior
for some stack height:

    a:      lc 1
            jmp b
    ...
    b:      subtract
            bnz a

Here the load-constant instruction increases the stack height by 1; if
that crosses a critical boundary, the compiler must insert a
half-space eviction instruction before the jmp.  Then the subtract
instruction decreases the stack height by 1 (popping the subtrahend
and the constant 1 we just pushed, then pushing the difference) and
again the compiler must normalize the register mapping before the
conditional branch.  If that involves saving and restoring a whole
half-space, that’s probably most of the execution time.

### Instead, flush values individually on wraparound ###

So, to avoid pathological slowdowns in the case of branches, you need
to track what your registers are being used for, and maybe do useless
work before control-flow join points to make them match up.

Then, when you want to push a ninth value onto the stack, you can
flush r0 to memory.  When you want to push a tenth, you can flush r1.
So you’re in a different state if you pop it back off.

If you don’t have any control-flow joins, or if they all happen with
the stack empty, this is fine.  You just have to keep track of the
“low-water mark” at which you have to load stuff from memory to
process it.  Otherwise you need to insert possible fixup code on one
of the control-flow arcs into each control-flow join point.

That’s plainly a workable solution, but we could wish for something
simpler.

*Carpe operandum*: commutativity and associativity
--------------------------------------------------

Consider the expression `(a*b)+(c*(d+c*(e+f)))`.  Darius’s example
compiler above renders this into the following code, to which I have
added some labels:

            lw r0, a
            lw r1, b
            mul r0, r1, r0
            lw r1, c
            lw r2, d
            lw r3, c
    0:      lw r4, e
    1:      lw r5, f
            add r4, r5, r4
            mul r3, r4, r3
            add r2, r3, r2
    2:      mul r1, r2, r1
    3:      add r0, r1, r0

With a sliding window of 4 registers (rather than the 8 suggested
above) we need to push values at points 0 and 1, then pop them off
again at points 2 and 3.  (With half-spaces, we could maybe just use a
single half-space.)

With this assignment, we assign stack level 0 (the deepest level) to
the final result, to `a`, and to `a*b`; assigning stack level 1 to
`b`, `c`, and `c*(d+c*(e+f))`; assigning stack level 2 to `d` and
`d+c*(e+f)`; 3 to `c` (again!) and `c*(e+f)`; level 4 to `e` and
`e+f`; and level 5 to just `f`.

Nothing good happens if we assign the levels bottom-up.  We could
assign register 0 to all the leaves, register 1 to all the depth-1
expressions, register 2 to all the depth-2 expressions, and so on, but
this is a disaster:

            lw r0, a
            mv r1, r0
            lw r0, b
            mul r1, r1, r0
            mv r5, r1
            lw r0, c
            mv r4, r0
            lw r0, d
            mv r3, r0
            lw r0, c
            mv r2, r0
            lw r0, e
            mv r1, r0
            lw r0, f
            add r1, r1, r0
            mul r2, r2, r1
            add r3, r3, r2
            mul r4, r4, r3
            add r5, r5, r4

What *does* help is rewriting the expression from
`(a*b)+(c*(d+c*(e+f)))` to `(((e+f)*c+d)*c)+(a*b)`, using the
commutativity of the operators to put the deeper expression tree on
the left.  This uses only 3 registers instead of 6:

            lw r0, e
            lw r1, f
            add r0, r1, r0
            lw r1, c
            mul r0, r1, r0
            lw r1, d
            add r0, r1, r0
            lw r1, c
            mul r0, r1, r0
            lw r1, a
            lw r2, b
            mul r1, r2, r1
            add r0, r1, r0

And, of course, this can be done even for non-commutative instructions
such as subtraction and division, because the instruction operands
identify which operand is the subtrahend and which is the minuend; you
just don’t have an infix syntax for the new structure of `a-b*c`
unless you invent a reverse subtraction operator.

For integer operations, we could also take advantage of the
*associativity* of some operations to minimize the weight of the
right-hand tree; `a+(b+(c+d))` is compiled to require 4 registers:

            lw r0, a
            lw r1, b
            lw r2, c
            lw r3, d
            add r2, r3, r2
            add r1, r2, r1
            add r0, r1, r0

but the equivalent `((a+b)+c)+d` requires only 2:

            lw r0, a
            lw r1, b
            add r0, r1, r0
            lw r1, c
            add r0, r1, r0
            lw r1, d
            add r0, r1, r0
            sw r0, x

In this particular case we could get the same result by taking
advantage of commutativity, but not in the case of, for example,
`(a+b)+(c+d)`, which requires reassociation to avoid any
right-nesting.

Floating-point operations are not associative and so such rewritings
are invalid for them, though the commutation optimization is fine.
The same is true of integer operations on substrates where overflow
behavior is troublesome; for example, if it can result in allocation
(as in Smalltalk) or undefined behavior (as in C).

This optimization doesn’t simplify the compiler or avoid pathological
cases.  But, at least if you have an AST, it’s a very simple and fast
optimization that gets better code out of a dumb register allocator.
(At least, it *can* be better code, but on an out-of-order machine it
exposes less ILP.)

I don’t know what the standard name for this is.  I’m tempted to name
it “eagerness” but that has an existing meaning.  I’ll call it “carpe
operandum”: do not put off until the right operand what you can do in
the left.

LRU allocation
--------------

What if you just allocate registers to the most recently used values?
Then you can reuse values loaded from local variables.  This sounds
simple but I think it works out to be more complicated than the
stack-window thing.  You can’t always recompute values that have been
flushed (the computation might have side effects) so you have to save
values to memory if they’ll be used later.  But if you save values to
memory that will never be used, you generate much worse code than the
stack approach.  That means you need to track where the last use of
each value is.

Backwards compilation
---------------------

Suppose you compile the code for a basic block backwards, starting
from the end of the function, like Henry Baker’s COMFY-65 and COMFY-80
do to knit together various basic blocks.  Then you always compile the
consumption of each value before its production, and it’s easy to
distinguish the last use, any non-last use, and the production of the
value (which doesn’t look like a use).

This could be done with a very simple sort of SSA-like triples
intermediate representation, similar to the above abstract assembly,
in which each newly created value gets a new “register”, and then at
control-flow joins we apply a φ function to all the variables that are
live on both sides of the join, in the form of inserting move
instructions on the second-compiled arc (either a backward jump or the
prologue to the target of a forward jump).  Then when we’re converting
the triples to native code, going backwards, we can assign a register
to each newly encountered value, if necessary freeing up a register
for it by evicting it to memory — which, since we’re living backwards
in time like Merlin, means loading that register from memory,
allocating a stack-frame slot for it if necessary.  And a “newly
encountered value” is generally not the *output* of a triple we read
backwards (if so, we can optimize it away, along with its inputs);
it’s either of its *inputs* if we haven’t seen that value before.

How should it choose which value to evict?  It can do better than LRU:
because it’s already seen the entire past, it can evict the value
produced or consumed furthest away in time.  The triples, upon
production, can be annotated with this data: at which program step
each value consumed was most recently accessed.  But this turns out to
be a slight variation on Poletto and Sarkar’s linear-scan algorithm,
so it probably isn’t much faster.

A nice thing about that approach is that it eliminates a lot of the
extra instruction count introduced by stack bytecode early on; the RTL
quads already have a roughly one-to-one mapping to ideal instructions,
because things like local variable fetches and discarding top of stack
only affect a compile-time stack, not generating any RTL.  This means
that the backward pass doesn’t need to handle nearly as many
instructions.

This seems like it will probably produce pretty decent register
allocation, plus some dead-code elimination, in two linear-time passes
over a subroutine in opposite directions.  Constant folding and the
dead-code elimination it enables would have to be done in the initial
pass that produces the basic blocks of triples.

Caller-saved registers
----------------------

Most calling conventions have a number of registers marked as
“caller-saved” or (my preferred term) “call-clobbered”, which is to
say, values in them will not survive a subroutine call.  These are the
cheapest registers to use for temporaries that don’t need to survive a
subroutine call, because they’re unallocated on entry to a subroutine
and after every subroutine call; you don’t have to evict anything to
memory to use them.

This is a thing to keep in mind for register allocation: a value whose
live range doesn’t include a subroutine call can be allocated in a
caller-saved register.
