I think I have a new SDF raymarching algorithm that is potentially
enormously more efficient than existing SDF raymarching algorithms or
conventional Whitted-style raytracing, and which requires no
precomputation, thus being better suited for highly dynamic scenes
than algorithms based on a bounding volume hierarchy (BVH).  Moreover,
it has anytime and kinetic variants which should permit animating
dynamic scenes even on fairly slow machines.

The branchy nature of this algorithm may not be well suited to GPUs,
but especially on CPUs it may provide a very interesting improvement
on the now-standard SDF raymarching approach.

SDF raymarching in general
--------------------------

In an SDF raymarching renderer, we march along a ray until we are
sufficiently close to a piece of scene geometry.  To ensure we don’t
go inside a solid, at each point we visit along the ray, we sample the
signed distance function (SDF) of the scene, which tells us exactly
how far away we are from the nearest solid — or, in some relaxations
of the algorithm, a lower bound on how far we are which reaches zero
at the proper place.  If the SDF at a given point is 3.4 meters, we
know we can proceed 3.4 meters in any direction without going inside a
solid, and in particular we can proceed 3.4 meters along our ray.  At
that point, we may be on the surface of a solid, or more likely not,
so we sample the SDF again, and repeat.

This is an appealing algorithm for a few reasons.  For many shapes,
the SDF is much simpler to compute than the closed-form ray-geometry
intersection required by a traditional Whitted-style raytracer, and
sometimes even more efficient.  Raytracers commonly require the
surface normal, which is the gradient of the SDF, and therefore comes
“for free” with automatic differentiation.  SDFs afford some
composability not available with Whitted-style intersection functions:
the minimum of two SDFs is the SDF of the union of the objects, the
maximum of two SDFs is a conservative approximation of the SDF of
their intersection, a smoothstep between two SDFs can provide a kind
of geometric interpolation, and a “soft minimum” such as the
Pythagorean sum can automatically fillet shapes together.  A
conservative approximation of the SDF of a small spatial perturbation
of a shape can be computed by spatially perturbing the SDF itself and
multiplying it by a safety factor.  Rounding a shape from its precise
SDF can be achieved simply by adding a constant to the SDF.  Inigo
Quilez (iq/rgba) has [written the best overview][0] on the topic.

[0]: https://iquilezles.org/articles/raymarchingdf/

Whitted-style raytracers require a single evaluation of the
intersection of each ray with each shape (at least each shape at the
top level of the bounding volume hierarchy).  SDF raymarchers require
many, perhaps an order of magnitude more, so reducing the bushiness of
the BVH would seem to be more important.  But it occurred to me that
perhaps you could reduce this with a priority queue.

Using a heap to avoid reëxamining faraway objects
-------------------------------------------------

Consider marching a ray from the eyepoint into an SDF scene containing
two pieces of scene geometry, whose SDFs there evaluate to 1.7 smoots
and 5.3 smoots.  You can safely march along your ray by 1.7 smoots
before evaluating the SDF again.  The standard approach is to
re-evaluate both parts of it; perhaps the 1.7-smoot-away shape now
gives you 0.5 smoots, and the 5.3-smoot-away shape now gives you 6.2
smoots.  But that second evaluation was, strictly speaking,
unnecessary; you already knew you couldn’t have hit that shape because
it could at most be 5.3 - 1.7 = 3.6 smoots away at that point.  You
could have avoided re-evaluating it until you were considering
sampling a point far enough along the ray that it *could* be inside
that shape.

To handle this in the general case, you insert each shape into a
priority queue, such as a binary heap, ordered by the earliest
parameter value *t* = *tᵢ* along your ray *x⃗* = *p⃗* + *tv⃗* that you
might encounter it.  Initially, *tᵢ* is the SDF *f* of shape *i* at
your ray start point *p⃗*, *fᵢ*(*p⃗*).  In order to have sampled up to
some point *t* = *tₙ* along the ray, you must have removed all the
entries from the priority queue that are less than *tₙ* by sampling
their SDFs at some middle point *tₘ* 0 < *tₘ* < *tₙ*, reinserting them
into the priority queue at a new value *tₘ* + *fᵢ*(*p⃗* + *tₘv⃗*).  With
the previous example, you can sample at *tₘ* = 1.7 and reinsert the
shape previously at 1.7 with the new value *tᵢ* = 1.7 + *fᵢ*(*p⃗* + 1.7
*v⃗*) = 1.7 + 0.5 = 2.2.  So you cruise along the ray, one by one
encountering phantasms of shapes that *might* be at a particular
location but probably aren’t, evaluating their SDFs to kick them
further down the ray until you can’t kick them a noticeable distance
any more.

Each kicking-down-the-road operation involves removing a single item
from the priority queue and then reinserting it, or perhaps reducing
the priority of the frontmost item in the queue, the so-called
decrease-key operation.  In a conventional binary heap this requires
O(log *N*) work, average and worst case, although possibly a Fibonacci
heap would be more efficient for large numbers of objects.  In this
case, though, the top of the heap contains nearby objects, while the
base of the heap is made of faraway objects.  Normally we don’t expect
the possibly-nearest object to suddenly become very far away; instead
we expect them to turn out to be a little bit further away than the
other possibly nearby objects.  So normally we don’t expect them to
sift very far down the heap.  Indeed, in the last several steps, we
only expect to have a single nearby object, to which we are
approaching much more closely to any other object.

This means that we can march *M* steps further along a ray into a
flat, unstructured scene of *N* objects by doing O(*M* log *N*) work,
including *M* evaluations of the SDF of a single object.  However,
usually, these *M* steps are each individually much smaller than the
steps taken in conventional SDF raymarching; *M* is at least the
number of objects closer to the eyepoint than the distance we have
reached.

Snapshotting the eyepoint’s priority queue 
-------------------------------------------

When marching many rays from the same origin point, as is done in
generating a raytraced image, each ray starts with the same
priority-queue contents.  A traditional binary heap is not an
FP-persistent data structure, so you cannot simply reuse the original
heap for each ray.  But this is a fairly easily solved problem.  One
approach is to divide the heap into nodes of, say, 15 distance/ID
tuples, each with pointers to 16 child nodes, and use path copying in
the usual way to make changes to a given node local to a single ray.
The memory for these local updates can be reclaimed very efficiently
because it follows a stacklike or nested-arena discipline: tracing a
ray may involve tracing one or more child rays from its termination
point, but generally only one of them at a time.

Binary heaps are perfectly balanced, and a four-level-deep tree of
such nodes can contain 4096 + 256 + 16 + 1 = 4369 nodes with up to
65535 scene objects.  If the nodes are 128 bytes each, that’s 559kB.
In the worst case, each marching operation along such a ray involves
copying 3 nodes (if we assume the root is already copied), totaling
384 bytes, so we can trace 10 steps deep in 3840 bytes of allocation,
worst case, which are then reclaimed.  However, 10 steps doesn’t get
you as far as you might think.

Clearing the thicket with branching rays
----------------------------------------

Unfortunately, copying the eyepoint’s priority queue doesn’t help us
that much in a complex scene.  If there are 10000 objects less than 5
smoots from the eyepoint, then any ray that proceeds more than 5
smoots into the scene must still evaluate the SDF of each of those
objects at least once, despite inheriting the eyepoint’s heap.  Though
rays that strike points near the eyepoint can get by with very little
evaluation, sooner or later we run into a “thicket” at the distance
where most of the scene objects are.  Any ray that proceeds further
from the eyepoint than half the scene objects needs to re-evaluate the
SDF for half the objects in the scene.  Then the next ray, say through
an adjacent pixel, has to redo all that work again.

So, while the conventional SDF-raymarching approach can get reasonable
results in 32 steps or less on many scenes, at each of which it’s
evaluating the SDF for the whole scene, this approach may need tens of
thousands of steps if there are tens of thousands of objects.

Consider a first ray and a second ray through adjacent pixels; they
are fairly spatially close to each other until they’re quite far into
the scene.  We can lower-bound the SDFs along the second ray by the
SDFs along the first ray, minus the distance between them at that
point.

And this is enough to avoid re-evaluating the SDFs along that second
ray.  We can snapshot the first ray’s heap at the last point where the
SDF is further than the distance to the second ray, and then after we
finish tracing the first ray, we can come back to this savepoint and
march *laterally* over to the second ray; we need only to add the
extra distance traveled to our odometer.  From there we can march
along the second ray as usual, but with slightly more conservative
SDFs in our heap than if we had traversed there directly from the
eyepoint in a straight line.

You can traverse laterally to multiple different rays, and this is
something I’d written about previously but haven’t implemented.  But
in order to traverse laterally to rays that are farther away, you have
to snapshot earlier along the first ray, because you need enough slack
in the SDF to cover the cost of the lateral traversal to the other
rays.

This suggests the following recursive approach:

1. Trace a single ray into the center of the scene, conceptually
   expanding a rectangle around it that represents the projected image
   of the focal plane.  Stop when the SDF is small enough that one of
   the corners of this projected focal plane could be hitting scene
   geometry.
2. Snapshot the heap and quadrifurcate the ray by traversing laterally
   to the centers of the four quadrants of the focal plane thus
   projected.
3. Now, trace rays aligned with the eyepoint through these centers
   further into the scene, with a smaller amount of slack
   corresponding to the size of these smaller quadrants, recursing to
   step 2 when it runs out, unless the thickness of the ray being
   traced is already small enough.  If one of the corners of the scene
   was in fact hitting scene geometry, that quadrant will run out of
   slack immediately.
   
With this approach there is no need to snapshot the eyepoint’s heap.

This recursive-snapshotting approach should do a better job of
avoiding redundant work; as a thick ray gets further away from an
object, its computations of the object’s SDF save all its descendants
from having to examine the object at all.

It isn’t necessary to stop at the single-pixel level; subpixel
rendering can provide antialiasing.  In other conditions, you might
want to stop tracing before the single-pixel level, for example when
rendering a smooth area without much surface detail, or when rendering
objects in the periphery of the screen, objects that an eye-tracker
indicates the user is not looking at, or objects of low intrinsic
interest.  Automatic differentiation can provide not just surface
normal information but also surface curvature, and when the surface
normals don’t match up with nearby z-depths, there may be
discontinuities that demand more rendering.

In this form, the algorithm still retains stack-like memory allocation
behavior, because the child rays from lateral traversal are traced one
by one, after which the parent ray can be discarded, just as with
reflection or refraction rays.

As an anytime algorithm
-----------------------

Alternatively, we could consider traversing the implicit tree
structure of these rays in some other order rather than depth first,
perhaps retaining the entire mess of FP-persistent heaps in memory.
Any truncation of the full tree still gives you a sort of image, but
maybe not a very-high-resolution one.  This allows you to prioritize
deepening the tree in some other way, such as breadth-first search or
the various prioritization schemes discussed above for adaptive
tracing resolution.  It also allows you to stop deepening when you run
out of time, so you can always meet a frame deadline, guaranteeing
interactive response even at the cost of image clarity.

This will require much more RAM.  If we guess that we have 16384
objects, the heap for the initial ray needs 128K.  If we have
1024×1024 pixels, we have 10 levels of branching.  Suppose that the
path to the average leaf involves rejecting each of those objects an
average of once, and that this is evenly distributed over the levels
of the tree, perhaps a bit optimistic.  Then the depth-first algorithm
needs only 256K, but materializing the whole tree would require a
million single-pixel heaps with 164 private entries in each of them.
I feel safe in saying that each pixel heap would fit in 128K, which
means the whole shebang fits in 128 gigs.  Hopefully it’s possible to
do better than that!

There’s an easy out, though.  You can materialize all but the last few
levels of the tree, generating those on demand and discarding them.
Dropping the last two levels saves 95% of the space.

As a kinetic data structure
---------------------------

Blender does an interesting anytime-algorithm thing with complex
meshes: as you rotate them it renders them very roughly, with a low
level of detail, but then improves the rendering over time when the
mesh is stationary.  This algorithm could do the same thing if we
conceptualize the transition from one frame to the next as another
kind of “lateral traversal”, but through time rather than space.  We
need to add a new surcharge to our odometer for movement; we can bound
the SDF change by the speed of the fastest object in the scene.  By
increasing our odometer by this amount and re-traversing the tree, we
will find that some of the treenodes remain valid, while others have
run out of slack and vaporize.  We can rematerialize them by
recalculating the SDFs of the objects that might have gotten too
close.

It may be worthwhile to build two or more separate trees for this
purpose, segregating them by speed, if there are many more slow-moving
or stationary objects than fast-moving objects.  The trees must be
traversed in parallel for rendering.

Use with BVH
------------

This algorithm can be combined with bounding volume hierarchies, which
would reduce the heap size.  But there is the danger of making it
slower rather than faster, because work done upon the opening of each
bounding volume must be redone for every ray that enters it, while
work that contributes to an earlier heap can be shared across all
rays that descend from it.
