Meckle: a simple text interface for GUIs
========================================

I was thinking about [RIPScrip][2], [NAPLPS][0] ([Reddy’s spec][1]),
[Videotex](https://hack.org/mc/blog/videotex.html) and similar
teletext systems, HTML forms, SGI’s Buttonfly, Tcl/Tk, text terminals,
[the MGR windowing system](https://hack.org/mc/mgr/), [Caleb Winston’s
STDG](https://github.com/calebwin/stdg), Dave Long’s window manager in
awk, and a conversation I had once at a party with someone who I
perhaps mistakenly remembered to be Richard Uhtenwoldt.

[0]: https://archive.org/details/byte-magazine-1983-02/page/n201/mode/2up "NAPLPS: A new standard for text and graphics"
[1]: http://www.martinreddy.net/gfx/2d/NAP.txt
[2]: https://16colo.rs/tags/content/ripscrip

I was thinking it would be interesting to write an “agent” with which
your program (“principal”) would converse by sending it a textual
description of a GUI in some fairly simple syntax:

    vbox mywarning {
         hbox {
             image bomb
             text "You have performed an illegal operation and will be shut down."
         }
         hbox {
             button "OK"
             button "Cancel"
         }
    }

The “agent” would draw the GUI and then send GUI events to the
principal, which the principal could then process as it saw fit.

    click Cancel
    click Cancel
    window close mywarning
    window close mywarning
    click Cancel

And then maybe your principal would send the agent other commands:

    close mywarning

And the agent would tell the principal when it had closed the window:

    closed mywarning

This sort of thing is perfectly adequate for menu-system and
file-manager sorts of things.

Tk’s widget list is instructive: button, canvas (vector drawing),
checkbutton, entry (text field), frame, hlist, label (non-editable
text), listbox, menu, menubutton, message, optionmenu, radiobutton,
scale (slider), scrollbar, text (multi-line text with multiplefonts
and stuff), and toplevel (dialog box).

Forms and semantically self-describing events
---------------------------------------------

A slightly more elaborate version of the event model would allow the
agent to send your principal a whole database record update, like an
IBM 3270:

    form asjdof {
         id: i032
         hbox {
              field name: "Marin County Superior Court"
              field lat: "60.321"
              field lon: "-41.321"
              submit "Submit"
         }
    }

Here you would have three text edit boxes and a submit button, which
would result in the delivery of an event line looking something like
this to the principal:

    submit asjdof id=i032 name="Marin County Sup. Ct." lat="60.321" lon="-41.321"

The principal could then, for example, update a SQL database and maybe
some other window on the screen.

Event subscriptions
-------------------

One of the hassles of X-Windows program is that, for efficiency, by
default, you don’t get any events.  You have to explicitly subscribe
to the events you want.  This adds significant complexity to small
programs.

To avoid having to specify events, the principal can just ignore
events it didn’t care about, though maybe the most voluminous ones
like mouse movement events should be turned off by default.  Winston’s
stdg uses “get mousex” and “get mousey” commands to poll for the mouse
position, and even “get keyispressed space” or “get keys” to poll for
keyboard input, which has obvious drawbacks.

As a middle way for the mouse, a simple long-polling approach could
follow a “mouse 38 22” response message from the agent with a later
“mouse moved” message, which the principal can respond to by
requesting the mouse position again if it still cares.  Buffering up
multiple mouse movements with timestamps could be useful for some
drawing programs but is not critical for most programs nowadays.

The same long-polling approach should bear fruit with things like text
field contents and select boxes (option menus or combo boxes).

On the other hand, if the principal wants to handle keyboard text
input (as opposed to using the form system), it’s probably not
acceptable to drop keystrokes because it didn’t poll in time.  Unlike
mouse input, though, the volume of keyboard text input is pretty
small, so it’s probably fine to just feed all the input characters to
the principal when they happen, except those intercepted by forms or
whatever.

Binary data
-----------

This kind of thing presumably can’t be performance-competitive with
shared-memory systems, though you could maybe do real-time 3-D and
other kinds of GPU rendering.

Still, it could handle things like vertex buffers, textures, and
images, either by prefixing them with a length or by representing them
SIXEL-style, with six bits per byte, for example encoded as the
printable non-whitespace characters ``@ABCDEFG HIJKLMNO PQRSTUVW
XYZ[\]^_ `abcdefg hijklmno pqrstuvw xyz;|=~?``.
For example, the principal might create
an agent-side pixmap and write 8×8 pixels of 32-bit BGRA image data
into it with commands to the agent like this, sending one scan line of
pixels per command:

    pixmap icon 8 8
    pixels icon 0 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    pixels icon 1 ```````````````````````````````````````````
    pixels icon 2 PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPooooooooooo
    pixels icon 3 [][][][][][][][][][][][][][][][][][][][][][
    pixels icon 4 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ccccccccccc
    pixels icon 5 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ccccccccccc
    pixels icon 6 TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTHHHHHHHHHHH
    pixels icon 7 TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTHHHHHHHHHHH

Then you can draw the pixmap thus defined, “icon”, in various places
in your UI.  For double-buffering you might want to write pixels into
it again and then redraw it.

Potential advantages and disadvantages with respect to existing systems
-----------------------------------------------------------------------

Unlike Tk, the principal can be written in any language, which is
particularly appealing for experimental programming languages.  Python
famously embeds an entire Tcl interpreter in order to invoke Tk.

Matching lines of text against patterns is something we have a lot of
tools for: not just awk, but sed, Perl, and lots of other things too.

Meckle can run over, for example, a socket, an encrypted ssh pipe, or
a serial connection to an embedded board, as well as local pipes and
things like that.

If the protocol is designed carefully, Meckle could permit
multiplexing like rmgr/mtx did for MGR.

Even without such multiplexing, it’s straightforward (on Unix) for a
single-threaded principal to fork off a background task which sends
updates on its status to the GUI.

Meckle could support things like user-controlled multiple windows on
the screen simultaneously, with the user closing them when they wish,
without the principal having to worry about when the windows are
visible, in a web-browser-like fashion.

Also, the agent can implement user-controlled zoom, skinning, and
“scrollback” to previous display states.  Unlike with REST, the
previous states are probably “dead” — you can’t interact with them.

However, sometimes, if you save the sequence of text lines to a file
and replay them at startup, you can reconstruct the whole application
state — you have to be careful that the event lines are sufficiently
self-describing, and you have to ensure that you don’t actually draw
the display until the replay is done.  Not everything can be done this
way, because many things would be too slow, but many things can.
Under most circumstances, this could be implemented *by the agent*.

Such a recorded event stream could also be useful for automated
testing.

You can draw a GUI layout in a text file, [like Winston’s stdg
example](https://github.com/calebwin/stdg#example), so that the
“principal” is just `cat`:

    start 400 400 Untitled

    background 255 255 255
    fill 255 0 0
    rect 50 50 100 100

    present forever

You could record a window contents as a text file (and the system
could have a “print screen” feature for doing this, similar to “view
source” in browsers).  More generally, you can support copy and paste
of parts of a GUI.

You could record a stream of graphical updates as a timed byte stream,
similar to asciinema, and replay it later.

Modifying tree structure
------------------------

The agent is somewhat similar to Tk’s “wish” command, and a Tcl-like
syntax could be used; I prefer HTML-like lexical nesting to Tk’s
identifier-tree approach, but if items are to be dynamically added
inside existing displayed items later, lexical nesting alone will not
suffice.  A thought I’ve had with respect to text terminals previously
is to leave a “marker” in your tree:

    hbox {
        button "OK"
        marker morebuttons
        button "Cancel"
    }

Then a later command can put more things at that marker:

    at morebuttons {
        button "Save"
    }

If we use a Tk-like structure it’s straightforward to supply a
position in the hierarchy:

    button .mywarning.buttons.ok -text OK
    button .mywarning.buttons.cancel -text Cancel
    # ... seven hours latair ...
    button .mywarning.buttons.save -text Save

But in this case we still need an additional mechanism to specify
which button it comes *after*; as in [OGDL](https://ogdl.org/spec) or
the Zork Z-Machine, the children of a tree node are ordered.

A different approach would be to use numerical tags, sort of like
BASIC programs:

    hbox .20.20
    button .20.20.10 -text OK
    button .20.20.20 -text Cancel
    # ... seven hours latair ...
    button .20.20.15 -text Save

If you supply both beginning and end positions for containers like
`hbox` you don't need the hierarchy and can just use fractions:

    hbox .20 .21
    button .201 -text OK
    button .202 -text Cancel
    # ... seven hours latair ...
    button .2015 -text Save

For this to be fully flexible after the fact you need something that
sorts before end-of-string.

I think this numerical stuff is a bad interface because it would be
unnecessarily hard to read.
