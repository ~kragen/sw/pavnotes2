Reading [MSR’s CHERI page][0] a different scheme for implementing
so-called temporal safety occurred to me, analogous to copy-on-write.
“Temporal safety” means “capability revocation”, for example, for
capabilities to memory allocations that have been freed.  CHERI uses
tag bits to mark in-RAM capabilities, using a hierarchical
page-table-like scheme to avoid storing lots of tag bits for large
swaths of capability-free memory.

[0]: https://msrc.microsoft.com/blog/2022/01/an_armful_of_cheris/

As I understand it from only reading the CHERI introduction paper,
CHERI’s current scheme for revocation involves sweeping through
process memory scanning for capabilities to revoke.  This means that
revocation has a lot of associated latency.  A COW-like scheme would
instead “fault in” capabilities on a page-by-page basis when
capability loads were attempted, validating the capabilities on each
page against a set of capabilities revoked since the page had last
been scanned.  Without special hardware support for this operation,
just using what CHERI has already implemented, you could literally use
the hardware’s virtual memory support and do it on a literal
page-by-page basis, essentially the same thing the OS does when it
brings in capability-containint pages from swap, although this would
incur an additional cost of also scanning pages when they were
accessed by non-capability load instructions.

Apparently Morello does do something like this, as explained in the
above-linked post.
