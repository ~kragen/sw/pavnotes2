Presses, hydraulic or manual, are used for a few different purposes in
the machine shop: forming sheet metal with dies, broaching keyways
with a broach, and uniting or separating press-fit parts.  Commonly
people who lack a real press will make do with a bench vise.  Bench
vises are not optimal for this, since their main purpose is
workholding and they have a lot of friction on the drive screw, but
they often still work despite attaining lower total forces than an
arbor press.

Analogously, people who lack a bench vise will sometimes make do for
workholding with vise-grips: four-jaw toggle linkages which can
achieve arbitrarily large mechanical advantage, in the rigid-body
approximation.  (In practice their mechanical advantage is limited by
frictional loading of joints and the compliance of their parts.]

Thus it occurs to me to wonder how far you could get using a vise-grip
as a substitute for a press, and in particular how far you could
engineer a vise-grip to work as a press.

(This was inspired by watching [a video documenting the construction
of a largely 3-D printed sewing
machine](https://www.youtube.com/watch?v=f2U4lxzaf48), which uses
metal parts for heavily loaded parts like shafts; the author used
slip-joint pliers to press-fit a retaining pin into a shaft.)

XXX
