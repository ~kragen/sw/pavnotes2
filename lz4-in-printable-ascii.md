[LZ4][4] is Yann Collet’s fast Lempel-Ziv compression, preceding zstd.
In [the LZ4 representation][2] ([spec][3]), the file consists of a
series of blocks.  Each block first inserts zero or more bytes of
literal data, then copies zero or more bytes from a previous
occurrence in the buffer.  The format is byte-oriented, so the
[4'452'069-byte King James Bible from Project Gutenberg][0] only
compresses to 1'604'872 bytes (36% of the original), while gzip -9
gets it to 1'392'385 (31%).  But being byte-oriented makes it very
fast: decompressing the file takes 15ms, compared to gzip’s 30ms or
cat’s 3ms.

[0]: http://canonical.org/~kragen/sw/netbook-misc-devel/bible-pg10.txt
[4]: https://github.com/lz4/lz4
[3]: https://android.googlesource.com/platform/external/lz4/+/HEAD/doc/lz4_Block_format.md
[2]: http://fastcompression.blogspot.com/2011/05/lz4-explained.html

The LZ4 encoding is clever:

    +-------+--------------------+----------+--------+------------------+
    | token | literal length ext | literals | offset | match length ext |
    +-------+--------------------+----------+--------+------------------+

Each block begins with a “token” byte with two 4-bit counts packed
into it, the size of the copied string and the size of the literal
string; count 15 is special, because it means further count bytes
follow.  These count bytes are added together until a byte of less
than 255 is found.  So 15 255 255 3 encodes 528, 15 4 encodes 19, 15
255 0 encodes 270, 14 encodes 14, and 15 0 encodes 15.  This encoding
of the naturals is unique, but it is not bijective because it is
self-delimiting.

It occurred to me that it would be nice to have an encoding like this
which, when used to compress an ASCII text string, yielded another
ASCII text string, like [shoco][1].  We can easily do this, at a
sacrifice to compression ratio, by using 6-bit quantities instead of
8-bit quantities, then biasing them into the printable range.  I think
the most desirable bias is 48, yielding encoding bytes
``0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmno``.

[1]: https://ed-von-schleck.github.io/shoco/

So six-bit binary 0 is encoded as '0', 1 is encoded as '1', 16 is
encoded as '@', and 63 is encoded as 'o'.  This isn’t the most
desirable encoding range but at least it avoids space and
unprintables.

When you are copying some data you need to also encode the offset of
the data to copy.  LZ4 does this with a little-endian two-byte
“offset” field, encoding an offset from 1 (the previous byte) to
65535, followed by any extension bytes for the match length.  In
printable text you could use a two-byte field encoding a 12-bit value
from 0 to 4095; if you make it little-endian, the first few values are
00, 10, 20, ... 90, :0, ;0, ... n0, o0, 01, 11, etc.  For many
purposes where you care about printability, though, the maximum total
offset distance is under 256 bytes.

In LZ4, the match length is encoded in excess -4, so 0 means to copy 4
bytes, 10 means to copy 14, and so on.  This occasionally saves a
byte, because match lengths from 15 to 18 don’t need a length
extension byte, because they’re encoded as 4-bit fields from 11 to 14.
I think this means you can’t ever flush the LZ4 compressed stream,
because the following four input bytes might be bytes that don’t occur
in the window.

In LZ4, the last block omits the “offset” field because it contains
only literals.  This means LZ4 is not self-delimiting; you need some
external framing layer to tell you where the end is.

So “9aquanauts” might be an encoding of “aquanauts”: it contains a
9-character literal “aquanauts”, and then it ends before the offset
field saying where to copy 4 bytes from.  This demonstrates that this
encoding is only minimally expansive for short strings, about as
minimal as a printable-byte-oriented encoding could be.
