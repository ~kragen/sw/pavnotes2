Lattice's iCE40 "UltraLite" super tiny FPGAs are kind of wimpy by FPGA
standards but still pretty amazing.  They have 640 (ICE40UL640) or
1248 (ICE40UL1K) 4-input LUTs, each with a D flip-flop, and on-chip 10
kHz and 48 MHz oscillators, in a 16-ball WLCSP or a 36-ball ucBGA.
The data sheet is unaccountably missing the package diagram, but the
16-ball WLCSP is 0.35-mm ball pitch and 1.409 mm x 1.409 mm x 0.35 mm
(0.7 microliters, 1.6 mg), and the 36-ball ucBGA is 0.40 mm ball pitch
and 2.5 mm x 2.5 mm x 0.40 mm.  Digi-Key has three models in stock for
US$2.80 and up.

Basic capabilities
------------------

It's "designed for ultra-low power mobile applications" and so runs on
1.2 volts with 35 microamps of standby current, but it includes
programmable 100 mA and 400 mA current sources for driving LEDs and
three programmable 24 mA current sources for driving RGB LEDs, plus
two 100kHz/400kHz/1MHz I2C ports which can be used as either master or
slave.  Example applications listed include IrDA, "Service LED",
barcode emulation, GPIO expansion, and SDIO level shifting; yet it has
56 kilobits of memory (7 kilobytes) divided into 14 EBR memory blocks.
Its I/O ports can be configured for 1.8, 2.5, or 3.3 volts, and either
single-ended or differential signaling.

Some of the current sources can be set as low as 2 mA, and it has
internal pullups on its GPIOs that can be set for roughly 3.3, 6.8,
10, or 55 kilohms.

(From here on I'll consider the 1248-gate version because the cost
difference is like 5% at Digi-Key.)

I seem to recall that SeRV fits into 200 4-input LUTs, and Smecher's
Minimax processor is about twice as big, so you can fit a whole
computer into this FPGA.

Loading configurations
----------------------

Each "PLB" (block) has a chain of 8 LUTs (chained together with carry
logic), each with a DFF, and shared block-level controls for clock
inversion, DFF enablement, and DFF set/reset.  The output of each
logic cell can be individually configured as directly coming from the
LUT or being registered through the DFF.  I think this means each cell
has 18 bits of configuration data (the 4-LUT, the carry select, and
the output select), plus three more for the block (clock phase,
enable, set/reset) for a total of 147 bits.  This would give the
ICE40UL1K 156 blocks and 22932 bits of configuration data, just under
2867 bytes.

The configuration can either be loaded from on-chip nonvolatile
configuration memory or an external SPI flash; presumably this means
you can reset the chip (by cutting power to it if nothing else) and
reload it with a new configuration over SPI.  This takes nominally 13
milliseconds (driving a master SPI or reading its internal NVCM at
33-71 MHz), or up to 53 ms at lower frequencies (7-17 MHz).  This
suggests that most of the configuration time is not occupied with
actually transferring bits because any of these numbers would imply on
the order of 900 kilobits of configuration bitstream.  You can also
make it an SPI slave to configure it at anywhere from 1-25 MHz.

2867 bytes seems astoundingly tiny as a description of a multi-CPU
computer system running on something as low-level as DFFs and lookup
tables.  I guess I've left out the routing configuration, though!  And
the datasheet doesn't document that as clearly.

[Dynamic reconfiguration through cold/warm boot is
possible](https://umarcor.github.io/warmboot/), for example by
[storing multiple bitstreams in a separate SPI
flash](https://github.com/sylefeb/Silice/tree/draft/projects/ice40-warmboot).
[Fritzsch, Hoffmann, and Bogdan's "Reduction of Bitstream Size for
Low-Cost iCE40
FPGAs"](https://easychair.org/publications/preprint_open/TPpq) says
the bitstream size is 135180 bytes for a 7680-LUT iCE40 (the iCE40HX8K
I think), so probably we should expect about 22000 bytes for a
1248-LUT device, which is a lot bigger than 2867 bytes but still
astoundingly tiny.  Fully reconfiguring that device took time closely
proportional to the bitstream size, up to 200 ms, which seems similar
to the 53 ms given above.  [This doesn't even require an external
reset, as the FPGA can initiate the warm boot
internally](https://old.reddit.com/r/FPGA/comments/tus4s9/ice40_is_it_possible_to_initiate_a_full_hardware/).

I think it might be better to [configure it as a slave,
though](https://www.latticesemi.com/-/media/LatticeSemi/Documents/ApplicationNotes/IK/FPGA-TN-02001-3-3-iCE40-Programming-Configuration.ashx?document_id=46502
"Lattice FPGA-TN-02001-3.3"), which means another chip (MCU or FPGA)
to control it.

Claire Wolf has [reverse-engineered and documented the iCE40 bitstream
format](http://bygone.clairexen.net/icestorm/format.html) but the
documentation doesn't go into much detail.

Block memory
------------

The block memory is divided into 14 separate 4-kilobit blocks, which
can be configured as "single port, pseudo dual port, or FIFO",
fundamentally organized as 256 16-bit words, with separate read and
write ports.  Moreover, read and write can be configured separately on
rising or falling clock edges.  So this potentially gives you 28 bytes
read and 28 bytes written per clock cycle.  (There's the possibility
of narrower data buses if you want.)

You can initialize the block memory as part of the FPGA configuration
and then not write to it, treating it as a ROM.

How fast is it?
---------------

Well, it has a built-in 48 MHz clock with 10% tolerance under
commercial temperature conditions and 20% under industrial, but a
built-in PLL to generate clocks at up to 275 MHz.  It takes forever
for the clock to stabilize, 100 microseconds.  It can drive and read
its LVCMOS inputs at 250 MHz at 2.5 or 3.3 volts, but only drive 155
MHz at 1.8 volts.  The global buffer clock network can run at up to
185 MHz.  Best-case pin-LUT-pin propagation latency is 9.0 ns.

This makes me think it's reasonable for a design to hit 100 MHz on
these devices and possible in some cases to hit 200 MHz.  This would
work out to 5.6 gigabytes per second into the BRAMs, 5.6 gigabytes
back out, maybe 400 megabytes per second in and out of the chip to
external sources, and 250 billion 1-bit LUT operations (hereafter "bit
operations") per second.

[Sylvain LeFebvre's Ice-V fits into about 1000 LUTs on an iCE40HX1K
IceStick including a whole
SoC](https://github.com/sylefeb/Silice/blob/draft/projects/ice-v/README.md)
and can run at 65 MHz on it.  The Ice-V actually looks super awesome,
with graphical output from a Verilator simulation.  A two-thread
barrel-processor Ice-V fits into 1240 LUTs.  It's written in Silice
("a thin abstraction above Verilog") in [a bit under 300
lines](https://github.com/sylefeb/Silice/blob/draft/projects/ice-v/CPUs/ice-v.si).

But the FPGA!  What can you do with the FPGA?
---------------------------------------------

You can clearly fill it with a CPU, but if all you want is a CPU, you
can get cheaper ones elsewhere.  A more appealing setup is to put a
CPU in part of the FPGA but use the rest of it for some kind of
superpowered peripheral the CPU can control.

### Logic simulation

Of course if you have a logic circuit design you can simulate it
faster in an FPGA than on a CPU.

### SDR

Well, I suspect you can do 8-bit wavetable synthesis with it with an
R-2R DAC at 250 megasamples per second.  That's not microwave
frequencies but it's well up into VHF.  It's so far beyond anything
I've ever dealt with in digital synthesis that I just have no idea.
You could mix it up to submillimeter frequencies and transmit a
125-MHz wide signal.

### Spread-spectrum optical

Spread-spectrum modulation and demodulation of a slower data stream
onto something like a 250MHz laser diode or infrared LED should be
doable.  But you'd probably need a pretty accurate clock, maybe an
external quartz crystal.  60 dB of coding gain should be attainable
for interactive data rates.

### Video synthesis

NTSC and PAL are about 6 MHz wide so direct digital synthesis of those
should be straightforward.  A 250 MHz dot clock on a VGA interface is
almost enough for 1920x1080 at 100 Hz, but it doesn't have enough
spare for overscan.  Suppose you go for 60 Hz and you can read in 400
megabytes of data per second (from somewhere); that's almost 7
megabytes per frame, just enough to do 24-bit color.  You can't do
that because you don't have enough pins to both read and write, but
you could probably read in enough data to do interesting things.  But
keep in mind that you only have 7 kilobytes of RAM to work with!

### Touch sensing

If we suppose that a capacitive touch sensor is in the 1-100 picofarad
range, we could maybe charge it up to 3.3 volts through an internal
50k pullup (which is more like 40k at 3.3 volts because it's not
really a resistor) with an RC time constant of 40 nanoseconds at 1 pF
and 4 microseconds at 100 pF, which would be 800 cycles at 200 MHz.
So it seems like capacitive touch sensing should be easy to achieve.
Pulling 100 pF down from 3.3 V with a 2-milliamp current source would
take 165 ns, which is way too long.

### Analog-to-digital conversion and video capture

An external RC filter and one of the differential amplifiers ought to
enable us to compare an external signal against a sigma-delta
datastream and thus digitize a signal at an equivalent PCM rate of
something like 30 Msps.  [Lattice has a reference design for this on
their
website](https://www.latticesemi.com/products/designsoftwareandip/intellectualproperty/referencedesigns/referencedesign03/simplesigmadeltaadc
"Lattice FPGA-RD-02047-1.6"); in an ICE40UP5K-SG48I it cost 99 LUTs
and ran at 75 MHz, so it would be under 10 Msps.

This should be plenty to, for example, digitize the signal from a [PAL
video
camera.](https://articulo.mercadolibre.com.ar/MLA-1143930875-camara-videovigilancia-pal-6mm-metalica-lente-intercambiable-_JM).
(I found one listed at US$26, as "Cámara Videovigilancia Pal 6mm
Metálica Lente Intercambiable"; other useful search terms may be
"analógica", "CCTV", "seguridad", "bullet", "450 TVL", "420 lines",
"BNC", "coaxial", "RCA", and "video compuesto", and many of these
cameras are listed under US$7.  People still use them for video
surveillance systems.)

However, in Lattice's reference design, they used about 9000x
oversampling instead of the more usual 8-16x, thus getting only 7.6
ksps at that rate.  They did get over 150 MHz in some other devices.

### Power switching

500 mA is enough to directly switch a relay, but I don't foresee being
willing to connect these FPGAs directly to a relay in order to save
myself a transistor at the risk of blowing up the FPGA.

### Number crunching

It doesn't have multipliers; those are reserved for the Ultra and
UltraPlus.  If we figure that each partial product bit of a carry-save
multiply has two output bits (sum and carry) and takes four input bits
(multiplier bit, multiplicand bit, carry in, sum in) it seems like it
ought to count as two bit operations, so an 8x8 multiply is 128 bit
operations.  At about 250 billion bit operations per second, it ought
to be able to handle 2 billion such multiplies per second, but with,
naively, a latency of 8 cycles.  This is not quite as high bandwidth
as my laptop's CPU, but response time is probably better.

The high cost of the multiply suggests that sparse multiplication-free
approaches to DSP have a significant speed advantage on them on this
platform.

If you are generating 1024x768 video at 60 Hz (see above about video
synthesis), you can do at most 42 8x8 multiplications per pixel.  This
is enough to do some pretty sophisticated graphics.

### Electronic simulation

In computational terms, it might be worthwhile to compile a SPICE
model (or a Modelica model) into an FPGA configuration.  You can
reasonably simulate circuits of up to a few thousand components, and
you can use Runge-Kutta.  If you're doing this on a regular laptop,
you can probably do it faster on the laptop's CPU, but the FPGA can
probably do it almost as fast.

### Music synthesis

Of course you can do the same kind of thing for physically-based
synthesis, with digital waveguides and all kinds of filters and
whatnot.

### PID control

    e = r - y
    u = Kd(e - le) + (Ki(s += e) + Kp(e))
    le = e
    
The path length between the new measurement `r` and the control output
`u` is two subtractions, a multiplication by Kd, and an addition.

![A diagram shows the path length clearly.](pid.png)

We might guess that this is about 11 cycles; at 200 MHz, that would be
about 55 ns.  What kinds of processes could you stabilize with a
tunable PID control system with 55-ns latency?  55 ns is 91 microns at
Mach 5 and 0.4 mm at orbital velocity.  In 55 ns, an object
accelerating from rest at one gee goes 15 femtometers, about a
ten-thousandth of an atomic diameter.  So this would be overkill for,
say, magnetically levitating something.

(Getting a measurement in 55 ns is probably the more difficult part of
the problem.)

### Active noise cancellation

With a couple of microphones and a speaker you ought to be able to do
active noise cancelation.  I don't know what the limitation is that
makes Bose's products cut out above a few hundred hertz but it's
probably not 55 ns or anything like that.

### Self-hosting tiny development

You almost certainly need some external memory for this, but it might
be possible to reduce the logic optimization and synthesis problem far
enough that you could synthesize a SoC design for the FPGA on itself.

Bigger iCE40s
-------------

The iCE40 I actually have is an iCE40UP5K.  It's fabbed in 40 nm and
has three times as much idle leakage current (100 microamps) but is
much larger: 5280 LUTs, 128 kibibytes of "SPRAM" SRAM in four blocks,
15 kibibytes of block RAM, eight 16-bit multiplier blocks, and two SPI
ports, and 30 or 48 balls instead of 16 or 36.  Physically it's a
little bigger: 2.15 x 2.55 mm.  And it's missing the high-current
sinks for IR LEDs on the smaller FPGA, and its I2C only goes to
400kHz.  Otherwise I think it's the same.

The datasheet for the iCE40UP5K gives not only some pin-to-pin
performance numbers (the same 9.0 ns for propagation delay through one
LUT, 16.5 ns for a 16-bit decoder, 18.0 ns for a 4-to-1 mux, 19.5 ns
for a 16-to-1 mux) but also some register-to-register numbers (40 MHz
for a 64-bit counter, 100 MHz for a 16-bit counter, 150 MHz for
pseudo-dual-port EBRAM, 50 MHz for the DSP blocks, 70 MHz for the
SPRAM).

Oddly I haven't been able to find information about which pins the
differential comparators are on in either chip, if any.  Maybe [only
the LP and HX series, not the UL and UP, have differential
comparators](https://www.latticesemi.com/-/media/LatticeSemi/Documents/ApplicationNotes/UZ/FPGA-TN-02213-1-7-Using-Differential-IO-LVDS-Sub-LVDS-iCE40-LP-HX.ashx?document_id=47960).
