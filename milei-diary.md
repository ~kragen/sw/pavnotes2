Argentina just elected as our head of state a guy who claims to be an
anarcho-capitalist (see file `weird-milei.md`).  This has never
happened before.  I thought it would be useful to record how my
expectations and observations change over time, because things that
seem obvious in retrospect aren’t always obvious at the time.

December 02023
--------------

### 02023-12-09 ###

Milei assumes power tomorrow.  Yesterday Mina and I were in a taxi
when the current president commandeered the country’s airwaves for his
farewell speech (a so-called [*cadena nacional*][0], a measure
originally established for announcing national emergencies but
commonly abused by Kirchnerist politicians to make political
speeches.)  He apologized for failing his country.

[0]: https://es.wikipedia.org/wiki/Cadena_nacional#_Argentina

The “blue” (illegal) dollar is at AR$940 × $990.  The peso has been
fluctuating; so far [this month][1] the blue dollar has ranged from
$860 × $910 to $945 × $995, while [last month][2] it ranged from $840
× $890 to $1025 × $1075.  [The previous month][7] had been much more
exciting, starting at $788 × $798 and peaking at $1050 × $1100 the day
after the elections which winnowed the presidential field down to
Massa and Milei.

[1]: https://dolarhistorico.com/cotizacion-dolar-blue/mes/diciembre-2023
[2]: https://dolarhistorico.com/cotizacion-dolar-blue/mes/noviembre-2023
[7]: https://dolarhistorico.com/cotizacion-dolar-blue/mes/octubre-2023

Travel by bus is fairly surveilled (I think you can still buy a SUBE
card without identifying yourself, and certainly mine is anonymous,
and you can still recharge SUBE anonymously at most *kioscos*, but you
cannot pay in cash on the bus) and subsidized down to $64 per trip,
with 50% discount on the second trip and 75% on third and subsequent
trips within whatever the time window is.  As part of the political
campaign, the SUBE machines recently started explaining that, without
subsidies, the price would be $700, which still seems unreasonably low
to me.  Bus companies have been surreptitiously cutting services,
placing protest signs on the sides of the buses explaining that this
is because the government isn’t paying the subsidies it promised.
Consequently, web sites that purport to tell you when the next bus
comes frequently advertise nonexistent bus runs.

Yesterday we took the bus, a taxi, and a train.  Taxi service in
Buenos Aires is $58.30 per *ficha*; the flag drop is ten *fichas*,
each 200 meters is a *ficha*, and there’s a *ficha* charge for waiting
(I think one per minute).  Generally Uber is much cheaper, and Didi is
cheaper than Uber.  But we were exhausted at *Constitución*, so the
taxi was easier.

The taxi trip cost us $3300, which is about US$3.40 at the rate above.
We took it about 7 km as the crow flies.

Milei seems to have mostly dropped the anarcho-capitalist act; his
slate of ministers is largely mainstream Macri politicians, though
presumably he’ll be a bit more radical than Macri was.  Unfortunately
he’s chosen bloodthirsty alcoholic Patricia Bullrich, Macri’s minister
of security, as his minister of security.  This is disturbing,
considering that the police forces (many? all?) answer to the minister
of security; if a coup materializes, it will likely be from the police
rather than the military.  Other choices, [according to Pagina/12:][3]

- Economy: Luis Caputo (ex-Central-Bank president under Macri)
- External relations: Diana Mondino (from Yale, supporter of
  self-determination for the Malvinas)
- Interior: Guillermo Francos
- Defense: Luis Petri
- Justice: Mariano Cúneo Libarona (professor, lawyer for Menem’s
  family, imprisoned for a month in 01997 because of AMIA)
- Super-minister of Infrastructure, unifying Transport, Public Works,
  Mining, Energy, and Communications: Guillermo Ferraro (former
  director of KPMG Peat Marwick Argentina, former official under
  Duhalde and governor Antonio Cafiero)
- Super-minister of Human Capital, unifying Social Development, Labor,
  and Education: Sandra Pettovello (politician and journalist for
  Ucedé, PRO, and Radio El Mundo)
- Health: Mario Russo (cardiologist, former secretary of health of San
  Miguel under Kirchnerist Joaquín de la Torre, and of Morón under
  Ramiro Tagliaferro of Macri’s PRO)
- Head of Cabinet: Nicolás Posse

[3]: https://www.pagina12.com.ar/690675-el-gabinete-de-javier-milei-los-ministros-y-secretarios-conf

This amounts to eliminating 11 of the 18 ministries and creating a new one.

Macri (now Milei’s ally and supporter) has been quoted in the press as
[warning in veiled terms][4] against street actions, referring to “orcs”
(“orcos”).  It’s hard to tell, but this dehumanization seems to be a
rhetorical justification for violent repression of the expected
political protests by the opposition:

> Los jóvenes no se van a quedar en casa y los orcos van a tener que
> medir muy bien cuando quieran hacer desmanes en la calle.

[4]: https://www.pagina12.com.ar/689245-macri-los-orcos-y-un-llamado-a-las-fuerzas-de-cielo

Pagina/12 seems to think this is a threat to call out brownshirt-like
unofficial paramilitary forces, rather than police, to violently
repress protests.  This seems like a plausible interpretation.

Milei spent his campaign inveighing against the “political caste”, so
it’s quite a public reversal that most of his ministers are drawn from
the political caste.

Today was a Saturday.  We walked to downtown Morón in the early
afternoon to buy some groceries at the only supermarket I’ve found
that doesn’t add shopping bags to my order without asking me and then
attempt to charge me for it.  Most of the items we bought had prices
marked on the shelves; of those, all were rung up at the correct
prices.  We bought butter (opting for La Serenísima instead of a
less-known brand whose ingredient list included food gums), sunflower
seeds, heavy cream (La Serenísima, which is adulterated with food
gums, because the supermarket has no decent brand), Coca-Cola Zero
(1.75ℓ), peanut butter (485 grams for $1900!), canned tuna, walnuts,
bleu cheese, eggs, and yerba mate.  All of this cost $12710 together.
The supermarket was calm and not crowded, though not totally empty.

The peanut butter, Coca-Cola, and sunflower seeds had no prices
posted, due to rampant inflation.  Generally, advertisements no longer
publish prices for the same reason.

Mina reports that her aunt, who is a pharmacist in a small town in the
province, has been considering refusing to serve health plans, because
they aren’t willing to pay enough.  Patients would, in many cases, be
able to get partly or fully reimbursed for out-of-network
prescriptions, if they had the dough to cough up that day.  As an
illegal alien with no health coverage, this doesn’t affect me (even if
my pharmacist were to do the same,) but I interpret it as a sign of a
society under massive stress; the systems of day-to-day survival are
breaking down under the strain.

I bought a newspaper (Clarín, $1200).  The main front-page article is
about how YPF (which is government-owned at the moment; Milei has
appointed Horacio Marín as its new CEO) just raised fuel prices by
30%, to $404 for “súper” gasoline (the normal kind) and $385 for
“súper” diesel.  These prices seem to be per liter, I don’t know,
though a photo on page 4 shows a gas-station worker raising the price
at a gas station to $443.0, which *is* per liter.  Clarín predicts “a
strong impact on inflation”.  But $404 per liter is US$1.58 per
gallon, which seems like a very low gasoline price to me.  [AAA says
that in California they average US$4.722/gallon][5] [today][8].
[California does have especially high gas taxes][6], but they’re still
only 85.38¢/gallon, so the untaxed price is still US$3.87/gallon, more
than twice the Argentine price.  I wonder what combination of export
limitations and subsidies keep the price so low here.

[5]: https://gasprices.aaa.com/?state=CA
[6]: https://en.wikipedia.org/wiki/Fuel_taxes_in_the_United_States
[8]: https://archive.fo/vfQ3U

The newspaper reports long lines at supermarkets as people rush to
stock up before the change of government, not so much for fear of
shortages as for fear of inflation.  I’ve heard rumors of this, and
certainly felt the temptation, but haven’t seen it myself.  The fear
of inflation is not unfounded; the paper says prices rose 4% in the
first week of December, which would be an annualized inflation rate of
almost 700%, a staggering figure though short of hyperinflation.

A traditional form of preserving meat here is *lengua al escabeche*,
beef tongue marinated in vinegar with oil, garlic, parsley, and chile.
I’m tempted to attempt this at the scale of some tens of kilograms, on
the theory that thus pickled, the meat will survive even if the power
goes out to the fridge.  I think beef tongue was $2400/kg last time I
looked.

Milei has promised to dramatically cut the government budget, by 15%
of the GDP (the so-called “*plan motosierra*”, “chainsaw plan”), but
Clarín says actually he wants to get 10% by rescuing the Leliq (?) of
the Central Bank and cut 5% of “primary spending” to get to a balanced
budget in 02024.  Apparently the current government budget is 19.5% of
the GDP; at the end of the last dictatorship in 01982, a currency
devaluation reduced it from 25.02% of GDP to 20.66% of GDP.

Milei predicts that the next six months will be “muy duros”, very
hard, and predicts “estanflación”, stagflation.

Milei met with Bolsonaro yesterday, even though he’s no longer the
president of Brazil.  To me, this signals [a lack of concern for human
rights][9].

[9]: https://en.wikipedia.org/wiki/Presidency_of_Jair_Bolsonaro

Mina expects a very difficult year, with more police repression of
protests, stagflation, and gradual cuts to transportation subsidies,
but policies that are much more lukewarm than his campaign rhetoric.
She doesn’t expect Milei’s advocated re-prohibition of abortion to be
successful, for example.  The vice-president-elect, Victoria
Villarruel, seems to have been marginalized; she’s from a military
family and is notorious for calling for a return of universal
conscription to get criminals off the streets, but Mina doesn’t think
this will happen.  Mina thinks that Milei probably will proceed with
his dollarization plan, but not soon; to get the necessary liquidity,
he would need to privatize state-owned enterprises (which he does plan
to do) and raid pension funds, which are now all state-owned.  She
thinks massive protests are a possibility as the economic screws
tighten, but that perhaps instead people will simply continue to
tolerate the continuation of the crisis they’re already accustomed to.

I think that Milei, whose reportedly cloned English mastiffs are named
Conan, Milton, Murray, Robert, and Lucas, is likely to attempt a
doctrinaire mass privatization and transition to capitalism.  But
capitalism depends on the willingness of private investors to invest
their money to expand productive capacity in the expectation of
profit.  Anyone who invests in Argentina during democracy knows their
profits will be confiscated the next time the Kirchnerists take power,
which will probably be in 02027 but might be much earlier; only
investments that are somehow shielded from kleptocracy, or investments
with an internal rate of return of 30% or more, will be profitable.
So I anticipate that a transition to capitalism will be marked with
massive liquidation of Argentine assets and capital flight, resulting
in a prolonged recession.  The model I keep thinking about is Russia
in the 01990s, where the GDP fell by 50% and took decades to recover.

### 02023-12-10 ###

Today Milei assumed the presidency.  I will go out later to get a
newspaper; the news web sites are full of trivia, like how he swore in
his ministers without a live TV feed, how his new [presidential
scepter][34] is [decorated with portraits of his English
mastiffs][33], and how he stopped to get out of his car and pet what
appeared to be a golden retriever on his way from Congress to the
*Casa Rosada*, the seat of the Executive Branch.  One of the top
headlines on Clarín is, “*La intimidad del poder: El primer grito de
Javier Milei y las revelaciones del* fuck you *de Cristina*”,
referring to Cristina Fernández de Kirchner, the outgoing vice
president and former president, who apparently flipped Milei off as
she walked out of the room today.

[33]: https://www.cronista.com/economia-politica/nuevo-baston-presidencial-javier-milei-agrego-un-insolito-detalle-que-desperto-la-polemica/
[34]: https://es.wikipedia.org/wiki/Bast%C3%B3n_presidencial_(Argentina)

Protestors [reportedly plan to protest on December 19 and 20][10],
responding to a threat by Milei to cut off payments (of welfare?) to
whoever protests by blocking streets.

[10]: https://www.clarin.com/politica/corta-cobra-piqueteros-responden-milei-fecha-salir-calle_0_yM4A4LfNq7.html

He’s [announced Axel Wahnish, his rabbi, as his ambassador to
Israel][11].  He also [gave a menorah to Volodymyr Zelenskyy][12], who
came to visit for the occasion; Chanukah is December 7 to 15 this
year.

[11]: https://www.clarin.com/politica/javier-milei-anuncio-israel-enviara-rabino-embajador_0_Ryq7SOAPQf.html
[12]: https://www.clarin.com/politica/metafora-javier-milei-fiesta-judia-januca_0_4T5CuWVg0g.html

Clarín quotes the end of his inaugural speech:

> También recuerdo que ese día la respuesta fue una cita del libro de
> Macabeos 3:19, que dice que ‘la victoria en la batalla no depende de
> la cantidad de soldados sino de las fuerzas que vienen del cielo.[’]
> Por lo tanto, Dios bendiga a los argentinos. Y que las fuerzas del
> cielo nos acompañen en este desafío. Muchas gracias, será difícil,
> pero lo vamos a lograr. Viva la libertad carajo.

In my translation:

> I also remember that, on that day [of his assumption of his seat in
> the legislature], the response was a quote of the book of [Maccabees
> 3:19][14], which says that “Victory in battle depends not on the
> number of soldiers but on the strength that comes from Heaven.”
> Therefore, God blesses the Argentines.  And may the strength of
> Heaven accompany us in this challenge.  Thank you very much; it will
> be difficult, but we will achieve it.  Long live liberty, goddammit.

I struggle with how to translate “carajo”; I’ve rendered it as
“goddammit”, but it’s not actually blasphemous in its origin; rather,
[it is a rude word for “penis”][13], though like “joder”, it has
entirely lost its literal sense in Argentine Spanish.  Other equally
bad translations might include:

> - Long live liberty.  Dick.
> - Long live liberty.  Fuck.
> - Long live fucking liberty.
> - Long live liberty.  Let’s roll.

[13]: https://dle.rae.es/carajo?m=form&m=form&wq=carajo
[14]: https://www.biblegateway.com/verse/en/1%20Maccabees%203%3A19

He’s already written [his first emergency executive order][15], but we
don’t know what it says yet.  Emergency executive orders ([*decretos
de necesidad y urgencia*][16], [Necessity and Urgency Decrees][17])
are, like the *cadena nacional*, a presidential power nominally
reserved for exceptional emergencies that has in fact been used
routinely, though Cristina Fernández de Kirchner in particular is
notable for using it only a few times a year instead of dozens.

[15]: https://www.clarin.com/politica/asuncion-javier-milei-presidente-listo-primer-dnu-gestion_0_5WIxiHctd9.html
[16]: https://es.wikipedia.org/wiki/Decreto_de_necesidad_y_urgencia
[17]: https://en.wikipedia.org/wiki/Necessity_and_Urgency_Decree

Initial outlines of the executive order suggest some slight variation
from the cabinet I outlined above; in particular, External Relations
will be handled by the same minister as Infrastructure, which seems
like a shockingly isolationist perspective.

Worryingly, during the inauguration, his followers were chanting
[“Policía, policía”][18] (“police, police”), reinforcing my concern
for defense of human rights.

[18]: https://www.clarin.com/politica/asuncion-javier-milei-abrazo-macri-gesto-cristina-kirchner-baston-presidencial-inesperada-arenga-policia_0_LEbnh6K3hh.html

#### The evening outing ####

It was hot during the day, so I didn’t go out until sunset, when I
donned a [Bizarrap][35] T-shirt to take a walk, hoping to pick up a
newspaper.  But no newspaper stand was still open; I had to return
home empty-handed.

[35]: https://en.wikipedia.org/wiki/Bizarrap

I walked to the plaza downtown, about 1.2 km from our house.  Though
it was dark, it was full of kids playing, young couples on dates, and
homeless people just hanging out.

I asked a young woman working in the plaza what she thought about the
election and how the day had been.  Between the tattoos on her forearm
I saw a deep purple scar across her wrist; I didn’t ask about that.
She said she’d expected that things might be tense because of the
inauguration, but that in fact everything had been perfectly calm all
day.

“More than anything I’m worried about work,” she said, in my loose
translation.  “I already have two jobs and can’t make ends meet.  My
mother and grandmother are on pensions, and I’m worried about them.
My father, too; he’s 60, so he doesn’t retire for 5 years, and he’s
working at a company Milei has said he wants to privatize.  I hope
things get better for most people, but I don’t know what will happen.”

I asked a gray-haired couple walking their excitable dogs in another
plaza.  “I went to go see the inauguration,” said the man, who
admitted to having voted for Milei, “and Milei said to expect things
to get much worse; if the [*Rodrigazo*][19] was 6, this will be 12.
That sure wasn’t what I was hoping to hear; I asked myself why I’d
bothered to come.”

[19]: https://es.wikipedia.org/wiki/Rodrigazo

I didn’t know what he was talking about with the 6 and the 12, but see
below.

The *Rodrigazo* was in 01975.  One of its surprising aspects was an
overnight devaluation of the peso by 180%; my father-in-law had just
sold a car and lost almost two thirds of the money that night to the
devaluation.  The trade unions called a 48-hour general strike,
forcing the resignation of Rodrigo, who was the Minister of the
Economy, and of José López Rega, the ultra-right-wing terrorist who
was then officially the Minister of Social Well-Being and unofficially
ran Isabel Perón’s government.  Rega fled the country, and less than a
year later, Isabel Perón was overthrown by Argentina’s last military
coup.

The woman said, “Well, after twenty years of being lied to, it’s nice
to have someone who tells the truth,” a remark strangely reminiscent
of Trump voters.

They expressed hopes that things would get better, and described their
astonishment at Cristina’s obscene gesture as she departed the
inauguration.  Then they reminisced about Menem’s presidency in the
90s, when his overvalued peso gradually hollowed out Argentina’s
industrial base, eventually leading many companies to pay wages and
other debts with [*patacones*][28] and food stamps.  Milei has often
cited Menem and especially his Minister of the Economy, Domingo
Cavallo, as inspirations.

[28]: https://en.wikipedia.org/wiki/Patac%C3%B3n_(bond)

I asked a young woman manning a *kiosco*, a convenience store, what
she thought.  “I haven’t been paying attention; I spent all day with
my family,” she said.  “*Ojalá que nos vaya bien*,” which I have a
hard time translating; a loose translation might be “I pray to God
we’re okay.”  [*Ojalá*][20], from Arabic *law šá lláh*, is a negative
form of “God willing”, used in cases where there’s not much hope.  The
expression is widely used by atheists, having been severed from its
Muslim theistic roots during the Spanish Inquisition.

“Hope and faith is all we have,” she added.

[20]: https://dle.rae.es/ojal%C3%A1

#### Notes on the inaugural speech ####

[Clarín][21] reported on the “Rodrigazo” remark, but [Pagina/12
printed the speech in full][22].  Here’s the remark:

> A su vez, el cepo cambiario, otra herencia de este Gobierno, no solo
> constituye una pesadilla social y productiva porque implica altas
> tasas de interés, bajo nivel de actividad, escaso nivel de empleo
> formal y salarios reales miserables que impulsan el aumento de
> pobres e indigentes, sino que además el sobrante de dinero en la
> economía hoy es el doble que había en la previa del Rodrigazo. Para
> tener una idea de lo que eso implica, recordemos que el Rodrigazo
> multiplicó por seis veces la tasa de inflación. Por lo que un evento
> similar significaría multiplicar la tasa de inflación por 12
> meses. Y dado que la misma viene viajando a un ritmo del 300%,
> podríamos pasar a una tasa anual del 3600. A su vez, tranquilos, que
> no termina acá, la herencia sigue.

[21]: https://www.clarin.com/economia/javier-milei-anticipo-durisimo-ajuste-denuncio-kirchnerismo-dejo-plantada-hiperinflacion_0_kDaNUSrAiW.html
[22]: https://www.pagina12.com.ar/693607-el-acto-de-asuncion-presidencial-de-javier-milei-y-su-discur

My translation:

> Furthermore, the *cepo cambiario* [foreign exchange control regime],
> another inheritance of this Administration, constitutes not only a
> social and productivity nightmare because it implies high interest
> rates, low level[s] of activity, scarcity of legal employment and
> impoverished real salaries which drive the growth of the poor and
> indigent population, but also the excess of money in the economy is
> twice what it was leading up to the *Rodrigazo*.  To have an idea of
> what that implies, let’s remember that the *Rodrigazo* multiplied
> the rate of inflation by six.  Therefore, a similar event would mean
> multiplying the rate of inflation by 12 [Pagina/12 says “meses”,
> which means “months”, but in context I think the correct word is
> “veces”, which means “times”].  And given that inflation is
> currently traveling at a speed of 300%, we could get to an annual
> rate of 3600 [percent inflation per year].  Furthermore, don't
> worry, it doesn't end here, the inheritance continues.

It’s relevant here that Alberto Fernández’s *mea culpa* speech the
other day was largely focused on blaming the “inheritance” his
administration received from Milei’s supporter [Macri, who was the
previous president (02015 to 02019)][23].

[23]: https://en.wikipedia.org/wiki/Mauricio_Macri

The *cepo cambiario* that he’s talking about is the widely flouted
official exchange rate, which is supported by requiring exporters to
immediately sell the foreign currency they receive for pesos at the
official rate, ([or rather the export-dollar rate][29], which is
AR$600, intermediate between the real exchange rate and the official
one) and by strictly limiting access to the other side of that
market — generally only importers are allowed to buy foreign currency
at the official rate.  Since the official rate is less than half of
the real rate, this operates as a heavy export tariff which is used to
subsidize imports, making it impossible for Argentine firms to compete
with foreign producers in either the domestic or foreign market.
Macri campaigned on a promise to eliminate the *cepo*, which he did,
temporarily, before reinstating it.

[29]: https://www.cronista.com/javier-milei-la-economia-que-se-viene/

I figured it was probably worth analyzing the rest of the speech; even
if it isn’t a reliable guide to the policies the dude will actually
enact, it’s the rhetoric he’s using to justify them, and so it should
tell which groups he’s trying to get support from and what kind of
behavior he’s trying to encourage among the public.

An interesting thing is that the opening of the inaugural speech was
quite optimistic, in strong contrast to the perception of my
excitable-dog-walking neighbor; perhaps unsurprisingly, it sounds sort
of like Reagan.  Here’s Pagina/12’s version:

> Señores Ministros de la Corte. Señores Gobernadores, señores
> Diputados y Senadores nacionales. Presidentes y dignatarios
> extranjeros. Argentinos. Hoy comienza una nueva era en
> Argentina. Hoy damos por terminada una larga y triste historia de
> decadencia y declive, y comenzamos el camino de la reconstrucción de
> nuestro país.
> 
> Los argentinos de manera contundente han expresado una voluntad de
> cambio que ya no tiene retorno. No hay vuelta atrás. Hoy enterramos
> décadas de fracaso, peleas intestinas y disputas sin sentido. Peleas
> que lo único que han logrado es destruir nuestro querido país y
> dejarnos en la ruina.
>
> Hoy comienza una nueva era en Argentina. Una era de paz y
> prosperidad. Una era de crecimiento y desarrollo. Una era de
> libertad y progreso. Hace 200 años, un grupo de ciudadanos
> argentinos reunidos en San Miguel de Tucumán, le dijeron al mundo
> que las Provincias Unidas del Río de la Plata no eran más una
> colonia española y que a partir de ese histórico momento seríamos
> una nación libre y soberana.

My translation:

> Honored Ministers of the Court.  Honored Governors, honored national
> Representatives and Senators.  Presidents and dignitaries from
> abroad.  Argentines.  Today begins a new era in Argentina.  Today we
> put an end to a long and sad history of decadence and decline, and
> we begin the road to the reconstruction of our country.
>
> The Argentines have firmly expressed a will to change that can no
> longer be undone.  There is no turning back.  Today we bury decades
> of failure, intestine fights [probably this is a transcription
> error], and senseless disputes.  Fights which have achieved nothing
> more than to destroy our dear country and leave us in ruins.
>
> Today begins a new era in Argentina.  An era of peace and
> prosperity.  An era of growth and development.  An era of liberty
> and progress.  200 years ago, a group of Argentine citizens who had
> met in San Miguel de Tucumán said to the world that the United
> Provinces of the Río de la Plata were no longer a Spanish colony and
> that from that historic moment we would be a free and sovereign
> nation.

Certainly his description of the past 80 years is indisputable, but it
seems unlikely that a mere election could make the kind of major
difference he’s promising — especially an election of a President who
lacks a legislative majority and who doesn’t believe in global
warming.

A particularly promising factor for me, as an illegal alien, is that
he quoted the bit of the Argentine Constitution which says that it’s
*para nuestra posteridad y para todos los hombres del mundo que
quieran habitar el suelo argentino*: “For our posterity and for all
the men of the world who wish to inhabit Argentine soil.”  To me this
reads as a symbolic rejection of the xenophobic rhetoric of Bullrich,
his bloodthirsty alcoholic Minister of Security.  And he spoke
glowingly of how Argentina received mass immigration from Europe *con
brazos abiertos*, “with open arms”.

But he also described the indigenous peoples of Argentina as
“barbarians”: *un país de bárbaros enfrascados en una guerra sin
cuartel*, “a country of barbarians bottled up in a war where no
quarter was given”.  He also cited with approval the 19th-century
Argentine president Julio Argentino Roca, best known for [his
genocidal military campaign against those peoples.][24] This attitude
seems likely to broadly prejudice the interests of immigrants to
Argentina, since most of us are not white like me, but those same
“barbarians” seeking to emigrate from Chile, Perú, Bolivia, Brazil, or
Venezuela.

[24]: https://en.wikipedia.org/wiki/Conquest_of_the_Desert

His main enemies in the speech, though, were “collectivism” (he
compared his election to the fall of the Berlin Wall), the deficit,
the printing of money, inflation, the national debt, criminals,
poverty, and above all the outgoing administration.

A thing I forgot to write down yesterday is that I anticipate a
sovereign default by Argentina within the next year, which will
probably be the largest sovereign default in history, breaking the
previous world record, which is also held by Argentina.  Now I get no
credit for this prediction, because this is the section of the
inaugural speech about it:

> *Naturalmente, a estos problemas hay que sumarle también los
> vencimientos de deuda de este año, donde los vencimientos de deudas
> en pesos son equivalentes a 90.000 millones de dólares y 25.000
> millones de dólares en moneda extranjera con organismos
> multilaterales de crédito.*
> 
> *Sin embargo, con mercados financieros cerrados y el acuerdo con el
> FMI caído por los brutales incumplimientos del gobierno saliente, el
> rollover de deuda es por demás desafiante, aún para el mítico
> cíclope.*

My translation:

> Naturally, to these problems we must also add the debt payments this
> year, where the payments due in pesos are equivalent to 90 billion
> dollars, and 25 billion dollars in foreign currency with
> multilateral credit institutions.
>
> However, with financial markets closed and the agreement with the
> IMF brought down by the brutal noncompliances of the outgoing
> administration, the debt rollover is at least challenging, even for
> the mythical Cyclops.

He took rhetorical aim at street crime:

> *En materia de seguridad, Argentina se ha convertido en un baño de
> sangre. Los delincuentes caminan libres mientras los argentinos de
> bien se encierran tras las rejas. El narcotráfico se apoderó
> lentamente de nuestras calles, a punto tal que una de las ciudades
> más importantes de nuestro país ha sido secuestrada por los narcos y
> la violencia.*

My translation:

> When it comes to security, Argentina has become a bloodbath.
> Criminals walk free while good Argentines barricade themselves up
> behind steel fences.  Drug trafficking has slowly taken over our
> streets, to the point where one of the most important cities of our
> nation has been captured by gangs and violence.

For context, [Argentina’s murder rate is about 4.3 murders per 100'000
people per year][36], lower than richer neighboring Uruguay (8.9) and
Chile (6.7) or the richer and mass-incarcerating US (6.8), but higher
than poorer neighboring Bolivia (4.0).  More to the point, it’s about
a quarter of the average murder rate in America (17.2) or [San
Bernardino (15.7)][38] and not even in the same world as Honduras
(35.1), Mexico (28.2), Brazil (21.3), St. Louis (66.1), Detroit
(39.8), Cincinnati (23.4), or Chicago (18.3).  It’s closer to San
Francisco, CA (6.4) or San Diego (2.5), and [considerably down from
02016 (5.9)][40].  [Macrotrends offers the following historical
data][41], which are from the [World Bank World Development
Indicators][44], available in [Excel][42] and [CSV][43] formats.
These numbers do seem to be in accordance with the Excel version of
the data, cells AT83357 to BN83357 on sheet “Data” (1/6).

- 02001: 8.35 murders per 100'000 people per year
- 02002: 9.42
- 02003: 7.75
- 02004: 6.05
- 02005: 5.63
- 02006: 5.35
- 02007: 5.36
- 02008: 5.89
- 02009: 6.49
- 02010: 5.80
- 02011: 6.05
- 02012: 6.31
- 02013: 7.25
- 02014: 7.54
- 02015: 6.56
- 02016: 6.01
- 02017: 5.26
- 02018: 5.37
- 02019: 5.16
- 02020: 5.37
- 02021: 4.62

So Milei’s “bloodbath” remark is completely the opposite of reality.
Street crime has spilled less and less blood since its peak a decade
ago, which was already down from its previous peak in 02002.

But theft is astoundingly rampant; in 17 years I’ve been robbed with a
screwdriver, helped the police apprehend a drunk who’d just robbed a
convenience store, had my laptop stolen from under my nose as I sat in
a restaurant, and seen cellphones stolen at least four times and a
wallet stolen once.  When George Bush’s daughter Barbara visited in
02006, someone [stole her purse and cellphone][37] as she dined in a
restaurant under Secret Service protection.  Argentina has the best
empanadas in the world, the best alfajores in the world, the best ice
cream in the world, and the best thieves in the world, at least if the
US police force is excluded.

The city he’s referring to is probably Rosario, where a friend of mine
lives.  The steel fence thing is also pretty true, although bad
Argentines also barricade themselves behind steel fences — they don’t
want to be robbed either!

[37]: https://www.theguardian.com/world/2006/nov/23/usa.julianborger
[38]: https://en.wikipedia.org/wiki/List_of_United_States_cities_by_crime_rate
[39]: https://en.wikipedia.org/wiki/Homicide_in_world_cities
[40]: https://en.wikipedia.org/wiki/Crime_in_Argentina
[41]: https://www.macrotrends.net/countries/ARG/argentina/murder-homicide-rate
[42]: https://databankfiles.worldbank.org/public/ddpext_download/WDI_EXCEL.zip
[43]: https://databank.worldbank.org/data/download/WDI_CSV.zip
[44]: https://datatopics.worldbank.org/world-development-indicators/

****

As the Peronist TV channel C5N pointed out,
Milei’s speech entirely omitted the words
*casta*, [political] caste, and *dolarización*, which were so
prominent during the campaign.  On three occasions when he did want to
blame the entire political caste for something, he instead called them
*la clase política*, the political *class*.  This curious rhetorical
change of direction is fascinating.

[36]: https://en.wikipedia.org/wiki/List_of_countries_by_intentional_homicide_rate

The bit I mentioned above that was reported about blocking streets was this:

> *Este nuevo contrato social nos propone un país distinto, un país en
> el que el Estado no dirija nuestras vidas, sino que vele por
> nuestros derechos. Un país en el que el que las hace las paga. Un
> país en el que quien corta la calle violando los derechos de sus
> conciudadanos, no recibe la asistencia de la sociedad. Puesto en
> otros términos, el que corta no cobra.*
> 
> *Un país que dentro de la ley permite todo, pero fuera de la ley no
> permite nada. Un país que contiene a quienes lo necesitan, pero no
> se deja extorsionar por aquellos que utilizan a quienes menos tienen
> para enriquecerse a ellos mismos.*

My translation:

> This new social contract offers us a different country, a country in
> which the government does not direct our lives, but stands watch
> over our rights.  A country in which he who does them [evil deeds]
> pays for them.  A country in which whoever blocks streets, violating
> the rights of their fellow citizens, receives no assistance from
> society.  In other words, he who sets up roadblocks does not get
> paid.
>
> A country where within the law everything is permitted, but outside
> the law nothing is permitted.  A country that takes care of those
> who need it, but does not permit extortion by those who use the
> poorest to enrich themselves.

This doesn’t seem too promising for the people I served when I
volunteered in the slums of Isla Maciel, many of whom had lost
children to police shootings, as well as to drugs and to killings by
drug gangs and other slum inhabitants.  Argentina doesn’t have enough
prisons to house them all, much less its thoroughly corrupt middle
class.

The reference to blocking streets is almost certainly literal; over
the last 17 years living in Argentina, I have often heard that the
only way to get grievances addressed was to block the streets.  We’re
talking about grievances such as a weeks-long power outage in the
middle of the summer, or a failure to collect garbage.  The usual
tactic is to [set a pile of rubber car tires on fire][25] in the
middle of the street; this is cheap, lasts hours, creates an opaque
barrier of smoke, is too dangerous to simply drive over, is
impractical for fire trucks to extinguish (fires in tire dumps
commonly continue to burn for months or even years), and generally is
too risky for police to touch.  The smoke also kills some of the
children who live nearby by causing respiratory problems.

[25]: https://en.wikipedia.org/wiki/Tire_fire#Use_in_protest

In other cases, it’s simply the mass of protestors filling the street
that blocks it and prevents traffic from flowing through.  This is a
daily occurrence; on any given day, there are protests filling streets
somewhere in Buenos Aires.

In notable contrast to this hardline rhetoric against poor protestors,
he also sort of promised amnesty to the Argentine political class for
their corruption, which I suppose was probably necessary to get the
support of Macri, one of the most notoriously corrupt of all:

> *En cuanto a la clase política argentina, quiero decirles que no
> venimos a perseguir a nadie. No venimos a saldar viejas vendettas ni
> a discutir espacio de poder. Nuestro proyecto no es un proyecto de
> poder. Nuestro proyecto es un proyecto de país.*

That is:

> As for the Argentine political class, I want to say to them that we
> are not coming to persecute anyone.  We are not coming to even the
> score on old vendettas, nor to dispute the space of power.  Our
> project is not a power project.  Our project is a national project.

#### Notes from news from the night ####

Perhaps the relentless vituperation against the previous
administration and, indeed, the entire previous century of Argentine
politics, was part of the motivation for Cristina’s hand gesture;
though Pagina/12 suggests that [it was a response to being yelled at
by Milei’s supporters][27].

[27]: https://www.pagina12.com.ar/693735-los-perros-en-el-baston-las-risas-con-cristina-y-el-momento-

Pagina/12 notes that the Ministry of Justice and Human Rights [has
been renamed to the Ministry of Justice][26], a purely symbolic move
which still reinforces my concern that the new administration plans to
trample on human rights — despite the incessant rhetoric about
“liberty”.  But in fact Exterior Relations does remain its own
ministry.  According to Pagina/12’s article, in which they misspelled
the word “absorbido”, the overall emergency executive order
reorganizing the executive branch is over 70 pages long.  They did not
bother to include it.

[26]: https://www.pagina12.com.ar/693759-viaje-por-el-primer-dnu-que-firmo-javier-milei

[The somewhat more reliable Cronista has also reported on this
executive order.][30] In particular, they explain that [Nicolás Posse
will be responsible for the mass privatization of state-owned
enterprises][31] like YPF, AySA, and Aerolíneas Argentinas.

[30]: https://www.cronista.com/economia-politica/a-horas-de-su-asuncion-javier-milei-firmo-su-primer-decreto-de-necesidad-y-urgencia/
[31]: https://www.cronista.com/economia-politica/milei-confirmo-quien-sera-el-funcionario-a-cargo-de-las-privatizaciones/

Wikipedia has a [handy color-coded chart of which ministers are from
which political parties][53], along with links to a page about each of
them, and a [table of foreign dignitaries who attended][54], including
heads of state Viktor Orbán, Felipe VI, Luis Lacalle Pou, Daniel
Noboa, Gabriel Boric, Santiago Peña, and Vahagn Khachaturyan.

[53]: https://en.wikipedia.org/w/index.php?title=Presidency_of_Javier_Milei&oldid=1189326077#Cabinet_members
[54]: https://en.wikipedia.org/wiki/Inauguration_of_Javier_Milei#International_leaders_and_representatives

The normal illegal (“blue”) dollar trading markets don’t open until
Monday morning, but Cronista points out that cryptocurrency markets
are open all weekend, and in those [the dollar, as represented by
stablecoins like DAI and Tether, has shot up from under $1000 on
Thursday to $1046][32], 3% up from Friday.

[32]: https://www.cronista.com/infotechnology/actualidad/dolar-cripto-hoy-cuanto-esta-tras-la-asuncion-de-javier-milei/

#### Updated predictions ####

I don’t know if I really have new information to update my predictions
with.  I already knew conditions were going to get a lot worse for at
least six months.

Knowing that Milei declared the national debt probably unpayable in
his own inaugural speech reinforces my belief that there will probably
be a sovereign default.  It’s currently unfashionable to [invade or
blockade countries to collect on their sovereign defaults][46] and
will probably remain so for the duration of Milei’s presidency, but it
seems likely that nonviolent sanctions against Argentina could make
life extremely difficult, especially since Milei wants to distance
Argentina from Russia, India, and China ([refusing to join
BRICS][52]), and even [Mercosur][47], which would make it much more
economically dependent on the IMF, World Bank, and US.

The rhetoric in his speech about crime, along with the renaming of the
Ministry of Justice and putting Bullrich in charge of the Ministry of
Security, seems to signal that the populace will be much less safe
from the police.  Though Milei admires the US, probably the level of
violent repression won’t go anywhere near as high as in the US, if
only due to lack of funds.  Even during the last dictatorship, the
police only murdered about one out of every 1000 to 3000 people, while
[in the US, one out of every 100 adults is currently imprisoned][45],
a level equivalent to the Soviet GULAG, and even more people are on
probation or parole.

But, as I noted about the SUBE card, perhaps new technologies will
offer cheaper forms of government social control than full
imprisonment, and despite Milei’s “liberty” rhetoric, maybe he’d be
strongly in favor of that.  The city is already bristling with
cameras; new AI facial-recognition algorithms could enable the police,
and other forms of organized crime such as drug-trafficking gangs, to
leverage small amounts of violence into highly effective forms of
coercion.  [IMEI scanners][48], [IMSI catchers][49], and MAC address
recorders can provide extremely cheap, invisible, highly scalable 24/7
surveillance of anyone with a cellphone.  Governments like Mexico’s
are already using them for this, for example [to punish people for
protesting in the UK][50] and [for unknown purposes in the US][51].
Together with license plate scanning and social network analysis, they
should enable organized crime groups to efficiently analyze and
neutralize networks that offer resistance to their coercion.

[45]: https://en.wikipedia.org/wiki/Incarceration_in_the_United_States
[46]: https://en.wikipedia.org/wiki/Venezuelan_crisis_of_1902%E2%80%931903
[47]: https://en.wikipedia.org/wiki/Mercosur
[48]: https://en.wikipedia.org/wiki/International_Mobile_Equipment_Identity#Law_enforcement_and_intelligence_use
[49]: https://protege.la/blog-contenido/que-son-y-como-funcionan-los-imsi-catcher/
[50]: https://privacyinternational.org/explainer/4492/how-imsi-catchers-can-be-used-protest
[51]: https://www.eff.org/es/wp/gotta-catch-em-all-understanding-how-imsi-catchers-exploit-cell-networks
[52]: https://apnews.com/article/brics-argentina-milei-mondino-e428ca065e093ccb5ecad5d19bc5963d

The proposed elimination of relatively peaceful forms of protest will
probably worsen conditions for the poorest people, who rely on them to
resolve life-threatening emergencies such as long summer power
outages.  It will probably improve conditions for the middle class in
the short term, as well as ambulance and emergency services, as
transportation becomes more reliable and predictable.  But the
repressed protests seem likely to unfold in more violent forms instead
as conditions worsen.  (Conditions are certain to worsen; Milei has
promised that.)

I expect that the blue dollar will be back up above AR$1000 this week
when Mina and I need to illegally change some more money for rent.
The total lack of rhetoric about dollarization makes me worry that the
planned criminal crackdowns might make illegal money changing more
difficult.  On the other hand, Bitcoin remains completely legal, and
likely to become more legal rather than less so, so maybe I’ll just
end up changing Bitcoin directly for pesos.

Wall Street’s enthusiasm for Argentine stocks seemed to increase after
Milei eliminated Bullrich in the first-round elections.  But I’m still
dubious about the possibility of massive foreign direct investment in
projects with a payoff time of more than a couple of years.  I think
Argentina’s only hope for economic development is still *public*
investment in increasing productive capacity.

I don’t know what to expect with respect to public transport.  Living
out here in Morón, transportation is a much bigger issue than it was
when I lived downtown.

### 02023-12-11 ###

It was hot again today.  Hotter, even.

I went out and bought a Página/12 ($600), and got a La Nación given to
me free when a café closed.  (I’d bought a cappuccino with ice
cream there for $2930.).  La Nación reprinted most of Milei’s
inaugural speech, including the bit about *peleas intestinas* (which
apparently means [“domestic squabbles”][55]), but not the part about
the *Rodrigazo* and the 12 times.

Página/12 fisked the speech, pointing out the error about the
bloodbath and some others as well.  Unfortunately Página/12 is one of
those super-partisan newspapers which is mostly devoted to
hate-raising rhetoric rather than information, similar to Fox News or
Vox.com, so the signal to noise ratio is pretty low; it’s full of
innuendoes about what things really mean, predictions about what will
result from unspecified current events, incomprehensible euphemisms or
dysphemisms, pronouns and other anaphora with no clear antecedents,
and metaphors, rather than straightforward falsifiable assertions.  La
Nación is a little better, but politically it’s much more closely
aligned with Milei, while Página/12 is a Peronist, Kirchnerist paper.

Most of the newspaper stands in Morón didn’t have any decent
newspapers, just *Crónica* and *Diario Popular*.  Those are
entertaining (at least if looking at photos of mangled dead bodies is
your idea of entertainment) but even less reliable sources of
information than Página/12.

[55]: https://dle.rae.es/intestino

Three pieces of news stand out.  The blue dollar went to $950 × $1000,
which is a move in the direction I expected but not as far as I
expected.  Milei’s administration is raising the official rate from
$385 per dollar to $650.  And they’ve [stopped accepting registrations
of planned agriculturalexports][56], because the semi-official export
tariffs are paid on the export dollar as of the date of registration.
The export dollar is currently at $600, which is the average of the
official dollar and the *dólar contado con liqui*.

The semi-official export tariffs I’m talking about are not the
difference between $600 and the true $975 value of the dollar, which
(as I explained above) operates as an additional export tariff that is
crushingly heavy on its own, but rather the so-called
[*retenciones*][57].  As I understand it, the name *retenciones*
refers to the idea that they represent withholding against other taxes
that may be owed, but in fact they are simply export tariffs.
Frequently the government changes them at harvest time to ensure that
any possible windfall profits in the agricultural sector flow to the
government rather than the farmers.

[56]: https://www.ambito.com/economia/el-gobierno-javier-milei-cerro-el-registro-anotar-exportaciones-granos-n5896630
[57]: https://es.wikipedia.org/wiki/Impuestos_a_las_exportaciones

The guy who sold me the Página/12 had long, gray hair and a long, gray
beard.  I asked him if he thought things would get better under Milei.
He closed his eyes, sighed, and shook his head sadly.  “No,” he said,
“I don’t think so.  *Ojalá que sí.  Ojalá que me equivoque.*” I hope
so; I hope I’m wrong.

Milei seems to provoke a lot of *ojalá*.

He says that to balance the budget, expenditures need to drop by 15%
of the GDP; 10% of this is supposedly from the Central Bank and 5%
from the state.  In very crude terms, this would seem to imply laying
off something like 5% of the population from government jobs, which
would be a million or two people.  It might be more, for example if
the cuts come disproportionately from personnel expenditures and less
from other parts of the government budget such as buying materials
from the private sector, or it might be less, for example if
government employees are paid more than average, or if personnel
expenses were a much smaller fraction of the government budget than in
the private sector.  But it seems like it should be in that ballpark.

It can’t be, though, because I don’t think there are millions of
government employees.  [An article from 02015][58] says that in the
ministries and “decentralized institutions”, in 02014, 69.7% of the
employees had permanent positions (*planta permanente*), working out
to 238'697 total employees out of about 342’000 total.

Anyway, the [government workers’ union][61] is threatening to strike.
I’m uneasy with the concept of a government workers’ union; given the
widely accepted sanction the government has for the use of force (for,
among other things, compelling payment of taxes), it seems dangerously
close to a protection racket or looting army.  California’s
prison-guard union has notoriously [campaigned for harsher sentences
in order to create more work for prison guards][62]; a more profound
kind of depravity is hard to imagine.  It even provoked [criticism
from the US Department of Justice][63].  Here in Argentina, the
government is a frank kleptocracy that has strangled the productive
economy for generations, so it’s hard to sympathize with the union
even if it’s only advocating higher taxes rather than higher
incarceration rates.

[58]: https://chequeado.com/el-explicador/como-emplea-el-estado-datos-y-definiciones-sobre-planta-permanente-contratados-y-monotributistas/
[61]: https://es.wikipedia.org/wiki/Asociaci%C3%B3n_Trabajadores_del_Estado
[62]: https://en.wikipedia.org/wiki/California_Correctional_Peace_Officers_Association#Lobbying
[63]: https://www.ojp.gov/ncjrs/virtual-library/abstracts/undue-influence-californias-prison-guards-union-californias

Apparently [someone named Gastón Mercanzini threw a bottle at Milei
yesterday and injured a police officer.][59] [“@wallstwolverine” and
Clarín captured video.][60]

[59]: https://www.ambito.com/politica/bullrich-se-refirio-al-botellazo-milei-el-que-las-hace-las-paga-n5896301
[60]: https://nitter.net/clarincom/status/1734198879577321714#m

The Central Bank, which Milei has smashed effigies of on live TV, has
[suspended the controlled-price foreign exchange market][64] until
further notice.  Ámbito quotes an ambiguously identified economist,
either Rocío Bisang or someone at *Portfolio Personal Inversiones*:

> sin financiamiento externo (Milei fue muy contundente en su discurso
> cuando dijo que no hay financiamiento y que el programa con el FMI
> está caído), el cepo cambiario es insostenible con una posición de
> reservas netas [muy frágil]

My translation:

> Without external financing (Milei was very vigorous in his speech
> when he said there was no financing and that the program with the
> IMF had collapsed), the *cepo cambiario* is unsustainable with [the
> Bank’s current] very fragile foreign exchange reserves position.

[64]: https://www.ambito.com/economia/dolar-bcra-limita-operaciones-el-mercado-oficial-cambios-y-necesitaran-aprobacion-previa-n5896136

### 02023-12-12 ###

The bottle-thrower is named [Gastón Ariel Mercanzini][65], and he’s a
51-year-old former Secretary of Culture from a small town in another
province, described as “ultra-Kirchnerist”.  He was drunk.  The city
police broke up a shouting match between him and the much more
numerous Milei supporters after he threw the bottle, then let him go.
The Ministry of Security is reported to be unhappy about this.

[65]: https://www.clarin.com/politica/primer-cortocircuito-nacion-ciudad-seguridad-creen-debieron-detenido-agresor-custodio-milei_0_SYXzKr3kQq.html

The supermarket price-control regime called *Precios Justos* ended on
Monday (yesterday), as planned by the previous administration, and
[massive price increases are expected][66], of 20–25% for the
previously price-controlled items.

[66]: https://www.clarin.com/economia/corset-precios-justos-empiezan-llegar-nuevas-listas-precios-supermercados-subas-25-100_0_2Q82UKDdT5.html

The power went out for a few hours this morning.  It was punishingly
hot (33° science) and remained above 25° until well into the night.

I took a walk around 23:00 and bought a Monster ($1000) and a quarter
kilo of ice cream ($1600).  As I said, Argentina has the best ice
cream in the world.  Even the small number of ice cream shops open
this late in a small suburb like ours have amazing ice cream.

The central bank has reopened the official currency-trading market
which they’d shut down yesterday.  The official dollar exchange rate
is going to be not AR$650 as reported yesterday [but AR$800,
reportedly the biggest devaluation in almost 35 years,][63] although
I’m pretty sure the crisis in 02001 tripled it almost overnight when
they dropped the peg, but apparently Salvador Vitelli says this is a
bigger jump.  Maybe he’s measuring from the end of last year when it
was AR$177.16, so this is 4.5 times higher.

The import dollar will be $940 due to an increase in the *impuesto
PAIS* to 17.5%.  The export dollar will be $860 (80% of the official
dollar plus 20% of the *contado con liqui* price) and have
*retenciones* of 15%, for non-agricultural goods, which I guess must
be on top of that $860, so effectively $748.

[63]: https://www.ambito.com/finanzas/dolar-caputo-anuncio-que-el-gobierno-llevara-el-tipo-cambio-oficial-800-n5897427

The blue dollar is up to AR$1020 × $1070, so $940 is close enough that
Argentine domestic production could maybe be competitive with foreign
imports in some areas.  The export tariff policy would still be a
stiff trade barrier protecting foreign companies from Argentine
competition.

Economist Federico Glustein predicts that the blue dollar will reach
AR$1270 in the next few days, though.

Also they’re increasing the money allocated to food stamps (*Tarjeta
Alimentar*) by 50%, and doubling the welfare payments made to poor
parents (the *Asignación Universal por Hijo*), which isn’t exactly
what I was expecting.  Specifically [AUH goes from $20661 per child
per month to $41322][74] and the food stamps go from $28600 to $42900
for one child, $44850 to $67275 for two children, or $59150 to $88725
for three or more children.  $41322+$42900 = $84222, which at $1055
per dollar is US$79.80, per month.  That’s not a lot but it’s sure a
lot more than the previous $49261.

[74]: https://www.cronista.com/economia-politica/se-duplicara-la-auh-y-la-tarjeta-alimentar-aumenta-50-a-cuanto-quedan-los-montos/

Unsurprisingly, though, they’re [cutting the subsidies for energy
and transportation][64], so the price of a bus ride will increase by
at least a factor of 10.  Unless they leave the current prices in
place, in which case the bus companies will all go bankrupt this
month, leaving us with no public transport except Uber, Didi, and
thieving taxi drivers.  No, [they’re also raising prices.][65]

[64]: https://www.ambito.com/politica/javier-milei-reunion-gabinete-luis-toto-caputo-medidas-economicas-anuncios-casa-rosada-fecha-12-diciembre
[65]: https://www.ambito.com/economia/el-gobierno-anuncio-la-reduccion-subsidios-la-energia-y-el-transporte-n5897575

They’re also [firing all government employees with less than a year of
seniority][64].

Unsurprisingly the announcement also announces plans to liberalize
imports and exports:

> Finalizada la emergencia, vamos a avanzar en la eliminación de todos
> los derechos de exportación, que consideramos un gravamen perverso
> que entorpece el desarrollo argentino.
> 
> Reemplazaremos el sistema SIRA de importaciones por un sistema
> estadístico y de información que no requerirá de la aprobación de
> licencias. Se termina así la discrecionalidad y se garantiza la
> transparencia del proceso de aprobación de las importaciones. El que
> quiera importar, ahora podrá hacerlo, y punto.

My translation:

> Once the emergency is over, we will continue to the elimination of
> all export rights (*derechos de exportación*), which we consider a
> tax that impedes Argentine progress.
>
> We will replace the SIRA system of imports with a statistical and
> information system that will not require the approval of licenses.
> Thus ends discretionarity [of import license approvals], and the
> transparency of the import approval process is guaranteed.  Whoever
> wants to import, will now be able to do so, period.

I don’t know what *derechos de exportación* and SIRA are, except by
context.  However, it seems to me that this plan is not compatible
with a fake exchange rate, and the announcement doesn’t say they plan
to eliminate the fake exchange rate, but rather raise it to $800,
bringing it closer to the real rate.  If you can import US$1000 of
whatever goods you want with US$740 worth of pesos, which is what the
$940/$1270 ratio posited above implies, then anyone who has pesos will
use them to import gold coins, Rolex watches, gem-quality sapphires,
air conditioners, or anything else that has durable resale value, and
the result will be to transfer all the pesos in Argentina from the
Argentines to the Central Bank, in exchange for its foreign reserves,
which will be sent abroad to buy Krugerrands and oil paintings.

The Argentine government has several pages explaining SIRA: [Minister
of Economy][70], [the tax agency AFIP][71], and [instructions][72].
[Cronista has an article on the planned abolition][73].

[70]: https://www.argentina.gob.ar/economia/comercio/importaciones
[71]: https://www.afip.gob.ar/sira/
[72]: https://www.argentina.gob.ar/sites/default/files/2023/02/_instructivo_-_sira_0.pdf
[73]: https://www.cronista.com/economia-politica/sira-caputo-anuncio-su-remplazo-por-un-sistema-de-informacion-y-dio-libre-acceso-a-las-importaciones/

Oh, and apparently [they did arrest Mercanzini, the drunk
bottle-thrower][66]; he’s bleeding from the head slightly in the
arrest photo, as, I imagine, was the police officer who his bottle
hit.

[66]: https://www.ambito.com/informacion-general/detuvieron-al-hombre-que-arrojo-una-botella-javier-milei-n5897036

[Economist Pablo Tigani prognosticates][67] in Ámbito Financiero:

> [P]rovocarán la destrucción de gran parte de la industria que será
> sustituída por sectores populares y medios a la agricultura[,] la
> ganadería[,] y los bancos.  Van a pulverizar el salario en pesos,
> generar un aumento del desempleo, la caída del consumo, la caída de
> la inversión, un [aumento] de la pobreza y un aumento de la
> indigencia.  Impulsarán una fuerte inflación inicial para licuar la
> deuda en pesos y van a impulsar una recesión para detener la
> inflación.
>
> Para hacer todo lo que anunciaron[,] van a tener que reprimir la
> protesta social, que puede ser de magnitudes desconocidas.

My translation:

> They will provoke the destruction of much of industry which will
> be replaced by lower-class sectors (*sectores populares*) and
> measures [?] on agriculture, ranching, and banks.  They will
> pulverize salaries in pesos, generate an increase in unemployment, a
> fall in consumption, a fall in investment, an increase in poverty,
> and an increase in indigence.  They will produce a strong initial
> inflation to liquidate the debt in pesos and they will produce a
> recession to stop the inflation.
>
> To do everything they’ve announced, they will have to repress social
> protest, which could be of magnitudes not seen before.

[67]: https://www.ambito.com/dolar-800-quita-subsidios-y-ajuste-fiscal-los-economistas-analizan-las-medidas-caputo-n5897596

I’m not sure what he means by “lower-class sectors” and “medios a la
agricultura,” but most of the rest seems like a fair set of
expectations for the government’s plan, except that investment seems
to be increasing; both Argentine ADRs in overseas markets and the
domestic stock market have been rising.

Apparently [Elon Musk congratulated Milei][68], being particularly
impressed with Milei’s statement from a few years ago, “*No hay nada
más injusto que la justicia social,*” that is, “There is nothing more
unjust than social justice.”  But Ámbito thinks Musk’s interest is in
exploiting Argentina’s lithium reserves and selling Starlink internet
service, which Enacom approved in 02021 but which isn’t yet happening,
rather than combating “social justice”.

[68]: https://www.ambito.com/economia/elon-musk-la-argentina-cuales-son-los-negocios-clave-que-espera-desarrollar-el-pais-n5896843

[Buenos Aires city Minister of Security Waldo Wolff wants to be able
to “preventively imprison” people who protest by blocking traffic
three times][69].  I’m pretty sure he’s not going to get the law he’s
asking for.

[69]: https://www.ambito.com/politica/waldo-wolff-le-propuso-patricia-bullrich-una-ley-sacar-todos-los-piquetes-n5897208

### 02023-12-13 ###

Cronista has [further reporting on the welfare changes][75].  They
also say [the devaluation of the official-rate peso exceeded all
expectations][76], and the Central Bank specifically spoke of

> *un factor nuevo e importante: el incentivo a la producción y la
> exportación y un desincentivo a continuar incrementando
> artificialmente las importaciones*

That is:

> A new and important factor: the incentive to production and
> exportation, and a disincentive to continue artificially
> increasing imports.

[75]: https://www.cronista.com/economia-politica/el-futuro-de-los-planes-sociales-impacto-de-las-medidas-de-milei-y-los-cambios-que-vienen/
[76]: https://www.cronista.com/finanzas-mercados/mas-devaluacion-tasas-menores-que-la-inflacion-y-plan-para-pagar-la-deuda-de-importadores/

I went out in the morning and bought a Monster ($1150), a Página/12
($600), a Cronista ($500), and a 500mℓ Coca-Cola Zero ($650).  The
elderly woman from whose newsstand I bought the newspapers, strikingly
beautiful with her collagen-plumped lips with bright red lipstick and
blond-dyed white hair, explained that newspapers were selling out
early these days, and expressed hope that things would get better.

I passed a store that specializes in canvas for awnings and screening
for providing outdoor shade.  I hadn’t noticed this previously, but
there’s a sign in the window, saying that sales are suspended until
further notice.  Is that new today, or did I just not notice it
before?

Downtown I passed a trash-fashion store.  Its windows announce that it
is liquidating its merchandise and closing at the end of the month.

The front pages of Crónica and Diario Popular were full of the news
about yesterday’s economic plan, and also that food prices were up
20%.  One said beef would cost $8000/kg.  I walked by a butcher shop;
different cuts of beef were listed for $4500–6500/kg.

The rain during the night reduced the temperature but increased the
dewpoint.  Fatigued, I set up the portable air conditioner to chill my
bedroom.  Hopefully this will lower the temperature and dewpoint to a
livable level, and hopefully we won’t have a second power outage.
Yesterday’s was the first outage we’ve had since we moved here a few
months ago.

Página/12 has a bright purple front page with the key ten points of
Caputo’s so-called *ajustazo*: the peso devaluation, cutting the
subsidies, welfare, etc.  The title, in 100-point capital letters: “LA
GENTE, ¡AFUERA!”  I’m not sure how to translate that.

Cronista’s front page is a little more sober.  It also has Caputo’s
economic plan on its front page, but the headline is, “*Llevan el
dólar oficial a $ 800, suben impuestos, recortan más gasto y cambian
jubilaciones*”: “They take the official dollar to $800, raise taxes,
cut expenses and change pensions.”  It also has an article about how
the IMF approves, with a photo of a smug-looking Kristalina Georgieva:
“*Tras el aval del FMI a la devaluación, el BCRA prepara salida para
la deuda y ajuste de tasas*”, “Following the guarantee of the IMF to
the devaluation, the Argentine Central Bank prepares exit for the debt
and tightening of rates.”  Apparently the IMF is now willing to
negotiate for the “reconfiguration” of the previously arranged deal
which had collapsed during the last administration.  Also the new
administration is going to try to re-enable swaps with China, a
notable contrast to Milei’s hard-line stance in the past against
dealing with China, our largest trading partner.

The IMF apparently [tweeted this official statement][77] yesterday:

> IMF Spokesperson Statement on Argentina
>
> December 12, 2023
>
> Washington, DC: Julie Kozack, Director of Communications at the
> International Monetary Fund (IMF), issued the following statement
> today:
>
> “IMF staff welcome the measures announced earlier today by
> Argentina’s new Economy Minister, Luis Caputo. These bold initial
> actions aim to significantly improve public finances in a manner
> that protects the most vulnerable in society and strengthen the
> foreign exchange regime. Their decisive implementation will help
> stabilize the economy and set the basis for more sustainable and
> private-sector led growth.
>
> IMF staff and the new Argentine authorities will work expeditiously
> in the period ahead. Following serious policy setbacks over the past
> few months, this new package provides a good foundation for further
> discussions to bring the existing Fund-supported program back on
> track.”

[77]: https://www.imf.org/en/News/Articles/2023/12/12/pr23441-imf-spokesperson-statement-on-argentina

Cronista has the Tweet printed on their page 2 as “the tweet of the
day”.  Next to it, there’s an editorial entitled “*Guerra al déficit
fiscal con un dólar a $ 800 y una tormenta casi perfecta*”: “War on
the fiscal deficit with a dollar at $800 and an almost perfect storm”.
It begins:

> *Javier Milei asumió la presidencia el domingo 10 de diciembre.
> Pero la verdadera fecha de inicio de su gestión quedará marcada a
> fuego con el discurso pronunciado ayer por su ministro de Economía,
> Luis Caputo*.

My translation:

> Javier Milei assumed the presidency on Sunday, December 10.  But the
> true beginning of his administration will be marked in letters of
> fire with the speech given yesterday by his Minister of the Economy,
> Luis Caputo.

Página/12 is less sanguine.  Their page-2 editorial begins with
accusations of corruption, under the title “*Los Caputo Boys contaron
del plan*”, “The Caputo Boys knew about the plan.”  Subtitles:

> *El equipo económico adelantó la devaluación a los banqueros*.
>
> *Al menos tres entidades recibieron la información de una corrección
> cambiaria dos días antes del anuncio de la suba del dólar oficial.*

My translation:

> The economic team gave the bankers advance information on the
> devaluation.
> 
> At least three entities received the information of a correction in
> the exchange rate two days before the announcement of the rise of
> the official dollar.

The article begins:

> *Para el mercado no parece una novedad, porque en general el sector
> financiero suele trabajar con información privilegiada, pero la
> devaluación de más del 100 por ciento que anunció el ministro de
> Economía, Luis “Toto” Caputo, la supieron antes al menos tres
> grandes banos [misspelling of “bancos”] de la Argentina.  Eso
> produjo que, en los últimos dos días, la cotización de la divisa
> oficial en las entidades se dispara hasta las 700 pesos, sólo 100
> pesos por debajo de valor que terminó anunciando el titular de
> Hacienda del gobierno de Javier Milei.  “No se animaron a ponerle al
> valor final para que no sea tan obvio”, bromeó un jugador de la
> City, en relación al adelanto aplicado por los bancos.  En Hacienda,
> en tanto, niegan que se haya charlado el plan con sectores privados,
> pero las reuniones con empresarios son vox populi desde que asumió
> Milei.*

That is:

> For the market it doesn’t seem like news, because in general the
> financial sector usually works with privilegd information, but the
> devaluation of more than 100% announced by the Minister of the
> Economy, Luis “Toto” Caputo, was previously known to at least three
> Argentine bans.  As a consequence, in the last two days, the price
> of the official currency in the banks shot up to 700 pesos, only 100
> pesos below the value that ended up being announced by the official
> of Finance of Javier Milei’s administration.  “They didn’t dare to
> put the final value on it so it wouldn’t be so obvious,” joked a
> gambler of the City, speaking of the rise applied by the banks.  In
> Finance, meanwhile, they deny that the plan had been spoken of with
> private sectors, but the meetings with businessmen are *vox populi*
> since Milei’s inauguration.

The article doesn’t offer much evidence of the accusation, nor details
on the operations of the markets.  (Is $700 the bid price, the ask
price or both?  I thought buying and selling currencies at prices
other than the official price in the MULC was illegal; are we talking
about illegal transactions?)  To me it seems like a devaluation was
extremely predictable, and the banks might have been unwilling to sell
dollars at below $700 due to uncertainty about its size rather than
certain knowledge.  But also it’s totally possible the bankers did
actually have detailed knowledge on the precise size of the
devaluation, which could have been very profitable if they were
allowed to buy dollars from people for $400, $500, $600, or even $700.

The article also asserts that this will be the only devaluation this
year, which I guess is likely since there are only three weeks left,
but another is expected between January and February.

Several other articles in Página/12 criticize the plan in very harsh
terms.

At 11:25 this morning, the blue dollar has actually receded a bit,
rather than reaching toward the predicted $1270; it’s at $1000 × $1050
now.  At 13:00, though, it’s $1065 × $1115, higher than any previous
day.  At 15:22 it’s back down to $1020 × $1070.  And that’s where it
ended the day.

INDEC’s inflation statistic, which is notoriously rigged to understate
inflation in order to let the government off easy in payments on
pensions and inflation-adjusted bonds, [says in November inflation was
12.8%][78], which is an annualized rate of 324%.  [Gasoline prices
rose 40% today][79], to $600 per liter (remember they were raised to
$404 on the 9th).  The CGT, the national federation of trade unions,
[announced it would oppose Milei’s economic plan][80], presumably by
going on strike; their announcement begins:

> *NO ES “LA CASTA”*
> 
> *EL AJUSTE LO PAGA EL PUEBLO*
>
> *El plan de ajuste fiscal y cambiario anunciado por el Gobierno
> generará una fuerte aceleración del proceso inflacionario, que
> dinamitará el poder adquisitivo de los salarios de los trabajadores
> formales e informales, trabajadores de la economía social y
> solidaria, de cuentapropistas y autónomos, así como también de
> jubilados y pensionados. Esto significa que, el anunciado ajuste, no
> ajusta a la “denominada” casta como se prometió en la campaña. El
> ajuste de Milei, una vez más, recae sobre el pueblo.*

That is:

> It’s not “the [political] caste [who is paying]”
>
> The people suffer the tightening
>
> The plan of fiscal and foreign-exchange tightening announced by the
> Administration will generate a strong acceleration of the
> inflationary process, which will dynamite the acquisitive power of
> formal [legal] and informal [illegal] workers, workers in the social
> and solidarity economy, independent and autonomous [workers], as
> well as retirees.  This means that the tightening announced does not
> tighten on the “so-called” [political] caste as promised in the
> campaign.  Milei’s tightening, once again, falls on the people.

[78]: https://www.cronista.com/economia-politica/inflacion-noviembre-2023-el-indec-da-a-conocer-el-primer-ipc-bajo-la-gestion-milei/
[80]: https://www.cronista.com/economia-politica/la-primera-advertencia-de-la-cgt-al-gobierno-no-nos-vamos-a-quedar-de-brazos-cruzados/
[79]: https://www.cronista.com/economia-politica/la-nafta-pego-otro-salto-el-precio-subio-casi-40-y-buscara-el-objetivo-de-los-800/

It continues on in this vein for a while, finishing with vague threats:

> *Sigue siendo nuestra vocación contribuir a la gobernabilidad. Para
> ello consideramos imprescindible una convocatoria al dialogo de
> parte de las autoridades gubernamentales.*
> 
> *La CGT RA no se va a quedar de brazos cruzados. Reivindicamos los
> derechos sociolaborales, el respeto a la libertad sindical y la
> negociación colectiva, el poder adquisitivo de los salarios y de las
> jubilaciones y la eliminación del impuesto a las ganancias para los
> trabajadores y trabajadoras.*
> 
> *No estamos ante un plan económico, estamos ante medidas
> desordenadas de ajuste.*

That is, in my fairly poor translation:

> It remains our calling to contribute to governability.  For this
> purpose we consider absolutely necessary a call to dialogue on the
> part of the government authorities.
>
> The CGT RA will not remain with crossed arms.  We will vindicate
> workers’ rights, respect for union freedom and collective
> bargaining, the buying power of salaries and of pensions and the
> elimination of the income tax for workers.
>
> We are not faced with an economic plan; we are faced with disordered
> tightening measures.

[Wall Street is said to be cautiously optimistic][81], especially
because of the IMF’s “unusually effusive” communiqué.  JP Morgan
predicts 60% inflation in December followed by slowing down to a
monthly average of 2.3% in 02024, which would be 31% for the year;
they also predict a 3% recession.

[81]: https://www.clarin.com/economia/cayeron-medidas-wall-street-inversores-reclaman-detalles-esperan-reformas-estructurales_0_iaHkgUyJ4r.html

If we can get out of this with only a 3% recession, that would be a
fucking miracle.  Ballparking it I’d say 12% is more likely.

Apparently [the planned removal of restrictions on imports begins in
January][82], according to communiqué A7917 of the Central Bank,
issued today.  [Many importers have unpaid debts with their overseas
suppliers][83] because they have not been allowed access to the
official foreign exchange market to buy dollars at the heavily
subsidized official rate, or rather the “import dollar” rate.  (Clarín
says [these unpaid importation debts totaled US$43 billion in
October][84].)  Communiqué A7918 of the Central Bank describes the
planned strategy for solving this problem by selling bonds with 5%
yearly coupon payments which will exempt the buyers from paying some
or all of the *impuesto PAIS* export tariff but which will ban them
from accessing the *contado con liqui* market.  They’re going to
[offer monthly interest rates of 11% to try to lure exporters to
liquidate their foreign currency sooner][87], though that will only
work as intended if inflation falls below 11%.  They’re planning to
use [a 2%-monthly crawling peg][89], which seems unrealistically
slow to me.

[82]: https://www.clarin.com/economia/banco-central-anuncio-enero-liberaran-importaciones_0_ujSFqf4Jpw.html
[83]: https://www.clarin.com/economia/plan-bandas-luis-caputo-planteo-economistas-pagar-deuda-importaciones_0_6GjulNlpCK.html
[84]: https://www.clarin.com/economia/deuda-importadores-llega-us-43000-millones-mismo-monto-deuda-fmi_0_3rlydAENgf.html
[87]: https://www.clarin.com/economia/detalles-revelo-caputo-economistas-podria-unificarse-dolar-baja-bienes-personales-nuevo-blanqueo_0_fDcuQW7sRX.html
[89]: https://www.clarin.com/economia/reunion-banqueros-bausili-ahora-plan-especial-canjear-bola-leliq_0_7mGTDJquGj.html

[The Kirchnerists tried to block the Senate from achieving a
quorum][85] and called the special session of the Senate to elect a
president pro tempore (*Presidente Provisional*) “illegal,
illegitimate, and unconstitutional”, who is [Bartolomé Abdala, a
politician from Macri’s PRO party][86].

[85]: https://www.clarin.com/politica/victoria-villarruel-consiguio-quorum-arranca-sesion-especial-designar-autoridades-senado_0_Lz52TVbpRx.html
[86]: https://es.wikipedia.org/wiki/Bartolom%C3%A9_Abdala

[Other aspects of the plan are that][87] the new unsubsidized rates
for energy will begin on January 1st, income tax (*impuesto a las
Ganancias*) will be re-established for workers earning more than 15
times the minimum wage, and the tax on personal goods ([a net worth
tax][88]) will be reduced from 1.5% yearly to 0.8% yearly.

[88]: https://www.afip.gob.ar/gananciasYBienes/bienes-personales/

[Bitcoin has had a lot of volatility in the last few days][90];
currently it’s at US$43255, almost back up to the plateau it was at
last week, which was the highest price since April 02022.  Supposedly
Argentina is one of the places where it’s most used; buying dollars
with pesos is, for most people, illegal (though widely practiced) but
buying Bitcoin with pesos is perfectly legal.

[90]: https://bitcoincharts.com/charts/bitstampUSD#rg60ztgSzm1g10zm2g25zv

[The official dollar closed at $773.97 × $832.64][91], and the Central
Bank bought US$284 million today at the new devalued price, bringing
its reserves to US$21.131 billion.

[91]: https://www.ambito.com/finanzas/dolar-hoy-cuanto-cerro-este-miercoles-13-diciembre-n5898427

### 02023-12-14 ###

It was hot today, but less so.  I went out and bought some things
today:

- Yerba mate, Canarias brand: $3199 for 500 g ($6398/kg)
- Yerba mate, CBSé brand: $1299 for 500 g ($2598/kg)
- Four all-meat hot dogs, Patyviena brand, 250g net weight: $2159 ($8636/kg)
- La Nación newspaper: $950
- El Cronista newspaper: $500
- Peanuts in the shell: $3000/kg
- salted peanuts without shells: $3000/kg also
- salted sunflower seeds without shells: $2300/kg
- Beef tongue: $4020/kg (a single tongue is just over 1 kg)
- Steaks (specifically, *bife ancho*): $5500/kg

Bloodthirsty alcoholic Minister of Security Bullrich [gave a speech
this afternoon about the new anti-protest measures they’re putting in
place][106]; among other things, she plans to prosecute people who
transport protestors to protests that block streets, organize the
protests, and fund the protests (“alcanzará al que corta, al que
transporta, al que organiza y al que financia”), [as Trudeau did in
Canada in February of 02022][103], [without even court
oversight][105], though the deputy minister of finance at the time
said [small donors were “unlikely” to be affected][104].  The ATE (the
government workers’ union) came out in advance against Bullrich’s
measures, whatever they are, and [afterwards declared them
unconstitutional][102] since they infringe on the constitutional
freedom to go on strike (articles 14 and 14bis and section 22 of
article 75).

Bullrich said:

> (...) toda persona que se quiere manifestar en la vereda no va a
> tener ningún problema. Lo que nosotros no queremos es corte de
> calles y rutas.  (...)  Habrá excepciones que tendrán que ver con
> tradiciones religiosas o eventos deportivos: muchas veces se toman
> calles para hacer maratones.

That is:

> Whoever wants to protest on the sidewalk will have no problem.  What
> we don't want is blocking of streets and highways. (...)  There will
> be exceptions that have to do with religious traditions and sporting
> events: often streets are blocked to run marathons.

She also specifically expressed her intent to prohibit picket lines by
striking workers blocking access to a company and called out the
environmental damage of tire burning.

[106]: https://www.ambito.com/politica/el-vocero-presidencial-dara-un-anuncio-las-1130-casa-rosada-n5899201

One recent social change Argentines have made which surprised me is
the wide adoption of electronic money.  Most of the people in line
ahead of me at the supermarket paid with either Mercado Pago (which
pays interest on your account balance) or credit cards.  Merchants
have adopted incentives to get people to pay with credit cards instead
of either cash or debit cards.  I don’t understand the incentive
structure that gave rise to this; it’s not fear of theft, because
Mercado Pago is far more vulnerable to theft than cash.  Whatever the
cause, it makes Argentines far more vulnerable to Trudeau-style
financial surveillance and extrajudicial retribution for suspected
involvement in protests than they were even a year ago.

Currently Argentine public opinion would run strongly counter to such
a move, but that could shift after only a few months of major protests
like those in Tahrir Square, much less an active insurgency like the
one in the 01970s.

The Sociedad Rural (the country’s main right-wing power base and
[responsible for more than half our exports][93]) [is rejecting the
emergency economic package’s hike in *retenciones*][92].  This seems
strange to me since the export dollar they get paid for their exports
goes from $600 to $860, but I guess that’s not nearly enough to
compensate for inflation.

[92]: https://www.c5n.com/economia/la-mesa-enlace-rechazo-el-aumento-retenciones-que-anuncio-caputo-no-resistimos-mas-presion-n138900
[93]: https://en.wikipedia.org/wiki/Agriculture_in_Argentina
[102]: https://www.ambito.com/politica/ate-rechaza-el-protocolo-antipiquete-y-lo-tilda-inconstitucional-n5899460
[103]: https://www.bbc.com/news/world-us-canada-60383385
[104]: https://www.cbc.ca/news/politics/emergency-bank-measures-finance-committee-1.6360769
[105]: https://financialpost.com/fp-finance/banking/trudeau-gives-banks-power-to-freeze-funds-without-court-order-in-bid-to-choke-off-protest-funding

In Rosario, the city I mentioned above which has been sort of taken
over by drug traffickers, the drug traffickers texted death threats to
the provincial governor, Pullaro; they also shot up an elementary
school, a bank, and a hospital yesterday.  The governor beefed up
security on prisons and went to go visit Bullrich.

The blue dollar is down to $940 × $990.  Glustein’s prediction of
$1270 this week (which he made two or three days ago) seems
increasingly unlikely.

Here are most of the top headlines on cronista.com today:

- Conflicto social.  [Patricia Bullrich presentó su protocolo
  antipiquetes: “El corte se termina”][94]
- Sanciones. [El Gobierno se prepara para la conflictividad en las
  calles y anuncia un protocolo antipiquetes.][95]
- Nuevo gobierno.  [Patricia Bullrich ordenó la presencialidad plena
  de los empleados del Ministerio de Seguridad: "Es insólito
  que..."][100]
- Importaciones. [Polémica por el primer bono libertario: ¿instrumento
  o estatización?][96]
- Devaluación. [Ahora, desdolarización: qué espera el mercado para el
  contado con liquidación.][97]
- Medidas. [El Lado B del ajuste: el peso sobre las jubilaciones, la
  presión tributaria y la coparticipación.][98]
- Ganancias. [Quiénes acompañarán a los libertarios para revertir la
  exención en el Congreso.][99]

[94]: https://www.cronista.com/economia-politica/patricia-bullrich-presento-su-protocolo-antipiquetes/
[95]: https://www.cronista.com/economia-politica/el-gobierno-anunciara-el-esquema-de-castigos-para-los-cortes-de-calle/
[96]: https://www.cronista.com/finanzas-mercados/el-primer-bono-libertario-genero-polemica-entre-oficialismo-y-oposicion/
[97]: https://www.cronista.com/finanzas-mercados/devaluacionahora-desdolarizacion-de-carteras-que-espera-el-mercado-para-el-dolar-ccl/
[98]: https://www.cronista.com/economia-politica/el-desagregado-del-ajuste-el-peso-sobre-las-jubilaciones-la-presion-tributaria-y-la-recomposicion-de-la-coparticipacion/
[99]: https://www.cronista.com/economia-politica/ganancias-milei-quienes-acompanaran-a-los-libertarios-para-revertir-la-exencion-en-el-congreso/
[100]: https://www.cronista.com/economia-politica/patricia-bullrich-ordeno-la-presencialidad-plena-de-los-empleados-del-ministerio-de-seguridad-es-insolito-que/

That is:

- Social conflict.  Patricia Bullrich presents her anti-protest
  policy: “Blocking roads ends here.”
- Sanctions.  The Administration prepares for conflict in the streets
  and announces an anti-protest policy.
- New administration.  Patricia Bullrich orders the employees of the
  Ministry of Security back to the office: “It’s unheard of that...”
- Imports.  Polemics over the first libertarian bond: security or
  nationalization?
- Devaluation.  Now, de-dollarization: what the *contado con
  liquidación* market expects.
- Measures.  The B Side of the tightening: the weight [probably?] on
  pensions, tax pressure, and [coparticipation][101] (redistribution
  of some federal tax revenues to provincial governments)
- Income tax.  Who will accompany the libertarians to revert the
  exemption [of wage laborers from income tax] in Congress.

[101]: https://es.wikipedia.org/wiki/Coparticipaci%C3%B3n_federal_de_impuestos

After rising earlier this week, [Argentine stocks and ADRs fell.][107]

[107]: https://www.ambito.com/economia/adrs-y-acciones-locales-del-sp-merval-suben-5-compras-oportunidad-n5899273

A big protest is planned in the Plaza de Mayo next Wednesday,
December 20.  On December 21 our next payment to the IMF is due,
US$913 million.  Reuters says we got a loan from the *Banco de
Desarrollo de América Latina y el Caribe* to make the IMF payment.

### 02023-12-20 night, morning of the 21st ###

A lot has been going on.

On Friday we went to talk to the immigration lawyer.  She was a
theater student in the 01970s during the last dictatorship, and
described how she escaped by the skin of her teeth from being vanned
away to a clandestine military detention center: during a moment of
distraction during her capture, a young conscript handed her identity
papers back to her and said, “Run and don’t stop running.  If you get
caught, I didn’t give this to you.”

Most of her business is defending criminals who are trying to avoid
getting deported.  Despite this, she lamented how the Argentine
criminal justice system is so soft on criminals.  But she said she’s
terrified of the new vice president, who comes from a military family,
and she worries that the new government will result in mass
homelessness.

She pointed out that Milei said that if he raised taxes he'd cut off
his own arm, but that now he needs to cut off three of his arms
because he’s already raised three taxes.  (The agricultural export
tariff is one; re-establishing the income tax on employees is a
second; perhaps the third is the import tax, which went from 7.5% to a
punishingly protectionist 17.5%.)  She said someone else would need to
lend him an extra arm to cut off!

Saturday was crushingly hot, and a thunderstorm in Bahía Blanca killed
13 people.  Sunday morning it reached Buenos Aires and knocked
branches off nearly every tree in the metropolitan area.

Apparently lots of pharmacies really are refusing to take health
insurance now, because the health plans are so far in arrears.

Previously unions negotiated new wages every year, then every six
months; [now they’re planning to do it every month.][119]

A protest this morning planned to cut off [the bridge between the
cities of Cipolletti and Neuquén][121] in Rio Negro Province, because
the soup kitchens (*comedores y merenderos*) were closed.  The
protestors, organized under the flag of the *Movimiento Dario
Santillán* (named after [a 21-year-old protestor murdered by the
police in 02002][122]) left the street open after an hour and a half
of negotiations with the *Gendarmería*.  The Minister of Security
trumpeted this as a triumph.

The Constitution guarantees [the rights to free expression, to
associate for useful ends, to petition the authorities, and to go on
strike.][128]

[128]: https://www.cronista.com/economia-politica/derecho-a-la-protesta-que-dice-la-constitucion-nacional/

Today, the day of the planned big protest in commemoration of the
first police killings of protestors in 02001, estimated at 30 or 40
thousand people, there’s a [new 83-page executive order
DNU-2023-70-APN-PTE repealing 300 laws][111] ([version published in
the Boletín Oficial][117], [Pagina/12 minute-by-minute updates][114],
[Ámbito Financiero’s][115], [reporting by El Cronista][116]).  The
president commandeered the national airwaves this evening (*cadena
nacional*) to [make a speech about it][112] ([video on YouTube][136]).
[Vilma Ibarra, the ex-secretary of Legal and Technical (‽), called it
a subjugation of the separation of powers between the branches of
government][118].  But [it needs to be approved by at least one of the
houses of Congress in order to remain law,][120] and it’s [unclear
whether Milei has the legislative support to make that happen][129].
In particular, they can’t approve *part* of it.

The police have been [making people get off the bus if they suspected
they were going to protests][113], but only about 30 in all, not
hundreds or thousands.  [They shoved and wrestled protestors out of
the street onto the sidewalk, apparently without causing any serious
injuries][125], despite quite a bit of brawling between protestors and
police.  They’ve only arrested two protestors, one of whom had, in
addition to protesting, just stabbed a cop.  (Watching video of the
street brawls, it’s amazing they didn’t arrest many more.)  They [put
up barricades around the *Casa Rosada*][123], the seat of the
executive branch, to protect it from protests.

[Deutsche Welte reports on today’s protests in English][108].  TN says
there [was one police officer per two protestors.][131]

[Crónica TV (if it bleeds, it leads) has an hour-long interview][109]
with the protestors banging pots and pans outside Congress and various
other places, a protest which was continuing at midnight; India’s CNN
News 18 has [live coverage of the protest from outside Congress][110]
as of 1:20 AM; as of 1:30 AM protestors have climbed to the tops of
the fences around the Congress building.  The streets are evidently
completely blocked despite the threats of cutting off welfare payments
to protestors and prosecuting people who organize protests that cut
off streets.

[108]: https://www.youtube.com/watch?v=4tWXD-tEwsk
[109]: https://www.youtube.com/watch?v=ppD1T-UhEKo
[110]: https://www.youtube.com/watch?v=SCy0KGKOijk
[111]: https://docs.google.com/viewer?embedded=true&url=https%3A%2F%2Fwww.pagina12.com.ar%2Fipad%2F%40%40%40pdfs%2Fdnudesregulacion.pdf
[112]: https://www.pagina12.com.ar/697077-a-que-hora-habla-javier-milei-en-cadena-nacional-hoy
[113]: https://www.pagina12.com.ar/697121-como-es-el-operativo-antipiquete-en-el-transporte
[114]: https://www.pagina12.com.ar/696296-javier-milei-presidente-el-ajuste-y-las-nuevas-medidas-econo
[115]: https://www.ambito.com/politica/javier-milei-dnu-desregulacion-economia-reunion-gabinete-protocolo-antipiquete-luis-toto-caputo-extraordinarias-dolar-fecha-20-diciembre
[116]: https://www.cronista.com/economia-politica/se-publico-el-decreto-con-las-mas-de-300-desregulaciones-que-dice-el-texto-de-86-hojas/
[117]: https://www.boletinoficial.gob.ar/detalleAviso/primera/301122/20231221
[118]: https://www.pagina12.com.ar/697160-las-advertencias-de-vilma-ibarra-al-super-dnu-que-anunciara-
[119]: https://www.clarin.com/politica/salarios-escenario-inedito-salto-inflacion-vienen-paritarias-mensuales_0_Lv9bHRgxa6.html
[120]: https://www.clarin.com/politica/puede-pasar-congreso-dnu-javier-milei_0_UuLv53swQO.html
[121]: https://www.clarin.com/politica/gobierno-desplego-primera-vez-protocolo-antipiquetes-evitar-corte-puente-cipolleti-neuquen_0_7q5RAqhYrC.html
[122]: https://es.wikipedia.org/wiki/Asesinatos_de_Kosteki_y_Santill%C3%A1n
[123]: https://www.clarin.com/ciudades/buenos-aires-blindada-marcha-piquetera-controles-colectivos-vallas-casa-rosada-fuerte-presencia-policial_0_i6cW2XCBdG.html
[125]: https://www.clarin.com/politica/marcha-piquetera-vivo-hora-comienzan-piquetes-mapa-cortes-20-diciembre-caba_0_CAMMDQ1YsZ.html
[129]: https://www.cronista.com/economia-politica/el-congreso-se-prepara-para-el-dnu-de-javier-milei-cuales-son-los-pasos-a-seguir/
[131]: https://youtu.be/2IZLvqKPNoU?t=7m35s
[136]: https://www.youtube.com/watch?v=nuFmtUhKTqw

As of 2 AM, the live stream from News 18 shows the streets outside of
Congress still full of protestors banging pots and pans (a so-called
*cacerolazo*).  Commenters in YouTube’s chat are making inflammatory
comments:

> - ​QUE SE QUEDEN AHI HASTA QUE SE PUDRAN
> - vayan a laburar
> - ​150% de inflación y no salían, 10 días de Milei y ahora si el país es un
>   desastre hay q sacarlo 🤣
> - ​como se nota que estos no laburan mañana
> - ​VIVA BOCA JUNIORS
> - ​LOS ORCOS QUIEREN A ARAGON
> - LIMPIEN TODO ANTES DE IRSE SORETES
> - ​🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁🚁
> - Eduardo jul​🤣CIVIL WAR🤣 entre Kirchos y Orkos🤣

The helicopter emojis may refer to the method of execution the last
military dictatorship secretly used on suspected leftists, dropping
them into the ocean from helicopters.  Much more likely, they may
refer to how protests and looting drove President Fernando de la Rúa
to resign and flee the *Casa Rosada* in a helicopter, in fear for his
life, on December 20, 02001.  “Orcos”/“Orkos” refers to Macri’s remark
likening the opposition to “orcs”.  “Boca Juniors” is a popular
football team.  Another term often seen is “kukas”, idiomatically
meaning Kirchnerists, but literally meaning “cockroaches”, just as in
[the lead-up to the Rwandan genocide][124].

According to the live stream, most of the protestors in the streets
are young, in their teens and twenties, though there are a fair number
of middle-aged and elderly people mixed in among the crowd.  Some
can’t be more than 16.  Several are waving Argentine flags.  No police
are visible in the live stream.

[124]: https://en.wikipedia.org/wiki/Rwandan_genocide#Preparation_for_genocide

The [Peronist C5N TV network is interviewing protestors at the
Congress protest,][126] simultaneously showing looped footage from
protests in La Plata, Tierra del Fuego, and somewhere else, all
presumably tonight.  Crónica is also interviewing protestors at
Congress, but there isn’t a useful link to the live stream.

[126]: https://www.youtube.com/watch?v=XKEBqDusJu0

At 2:30 AM there seems to be a garbage truck and some buses
successfully passing in the streets at the *Plaza del Congreso*, even
though there are plenty of protestors still in the streets.

At 2:45 AM the energy level of the protest seems, if anything, higher
than ever.  People are jumping up and down in unison and singing as
they bang pots and pans.  One of the protestors on top of the fences
around the Congress building is waving an Argentine flag.

Twitter seems to be broken.  TV stations are speculating about why.

At 2:51 AM the protestors are letting a number of cars through, very
slowly, but still occupying most of the street.  According to Crónica,
more and more people are arriving at the protest.

At 2:53 the Crónica signal is cutting in and out and the flashing
lights of a police car are visible.  The crowds are dissipating a bit
and more cars and buses are passing through more easily.

At 2:59 C5N is interviewing a passionate protestor, and a seller of
soft drinks weaves through the protest behind him with a tray of soft
drinks held high above his head.  Some white smoke is visible in the
background.  The crowd seems slightly calmer.

[La Nación’s TV stream is not covering the protest live,][127] instead
displaying a rerun of coverage from earlier today.

[127]: https://www.youtube.com/watch?v=vfkUsBe_uBU

At 3:15 there are still a lot of people in the street, but it seems to
me that it’s dramatically less than before.  One of the people
interviewed recently by C5N says he’s a professor and has to give
exams at 8:00 in the morning.  At 3:18 C5N is no longer showing a live
feed from Congreso.

At 3:22 Crónica reports that the protestors have set fire to a
dumpster on Rivadavia, outside Congress.  Half a dozen police
officers, without the helmets and riot shields from earlier today,
have arrived to wave Rivadavia traffic away from the burning dumpster.
At 3:25 a police car arrives.

Crónica claims they’re now the only live coverage of the protest but
that it isn’t diminishing.

At 3:30 AM the protest is still going strong.

At 3:34 AM C5N is interviewing a protestor who passionately demands
that Milei cut off his own arms.  He becomes so agitated that another
protestor intercedes, apparently concerned he will begin to hit the
reporter.

Someone has put up a banner on the Congress’s fence: *“SE VENDE. (...)
MILEI PROPIEDADES*”, which is to say, “FOR SALE.  MILEI REALTORS.”

I see some people at the top of the Congress’s front stairs; I suspect
they may be police guarding the entrance.

At 3:40 Rivadavia seems to be blocked with protestors again now.
Crónica takes a break from live coverage to loop some footage from
earlier in La Plata when an ambulance arrived at the protest to rescue
someone who’d collapsed in the protest.

Several of the people interviewed said they’d voted for Milei but were
very disappointed.  There seem to be more protestors than ever on the
fence around the Congress building, bearing a long banner of the
Argentine colors.

I can understand the disappointment to some extent, but if anything,
Milei’s departure from his campaign promises seems to have been to
carry them out slowly and perhaps incompletely.  He hasn’t yet blown
up the central bank, eliminated the peso, or reduced the state to a
minarchist night-watchman state, though the announced privatization of
all state enterprises that’s part of this decree goes a significant
distance in that direction.

At 3:53 the supposedly live feed from Crónica is a repetition of an
interview from about 20 minutes ago with a protestor whose mother is
going to be operated on soon in a public hospital for cancer, and who
says she doesn’t have money to celebrate her 13-year-old daughter’s
birthday in a week.

At 4:00 C5N ends their live commentary and starts playing a mix of
footage from earlier today.  Crónica continues replaying interviews
from earlier in the night while claiming they’re transmitting live,
with an updated timestamp in the upper right corner.  News 18 is also
replaying “live” footage from earlier.

At 4:30 C5N is again transmitting live from the protest in Congreso.
The protest seems to be almost entirely gone, with all the protestors
off the fence and only a few dozen left waiting their turn with C5N’s
microphone, blocking the street.  There are definitely some guys
standing guard at the top of the Congress steps, but they’re not in
police uniforms.  Looking back in the live stream, it seems like at
4:10 there was still a crowd shouting and waving their arms, so the
protest dispersed peacefully around 4:20.

Apparently there were *cacerolazos* blocking the streets tonight in
many neighborhoods: not just Congreso, but also Boedo, Villa Urquiza,
Lacroze, etc., as well as other cities all over the country.  Myself,
I didn’t hear any, but Morón is a pretty quiet suburb.  I went to a
café in the center of Morón and asked the waitress if there had been
unrest, but she said that while she hadn’t noticed any, she had been
inside the café all day; and I came back home shortly after sunset.

Télam, the Kirchnerist-run Argentine state news agency founded by Juan
Perón in 01945, has just uploaded [a three-minute video on the
protest][130] to YouTube.  Unfortunately subtitles are not available.
It looks like more people showed up to this spontaneous protest in
Congreso tonight than the 30 or 40 thousand expected at the planned
ones during the day.  Televisión Pública de la Argentina uploaded [a
2½ minute video of interviews][132] with protestors from various
protests and [a 3-minute video of specifically the one at
Congress][133].

[130]: https://www.youtube.com/watch?v=xRVZnW2Msic
[133]: https://www.youtube.com/watch?v=OeWYEin37lM
[132]: https://www.youtube.com/watch?v=m8e7L22BQ00

Something like 55% of the vote in the election last month was in favor
of Milei.  Almost all of the rest was for Massa.  So it seems likely
that very large parts of the population are both passionately in favor
of the protest and passionately opposed to it.

It occurs to me that, by attempting to use an executive decree to
repeal numerous laws enacted by the legislature, Milei is following
the precedent of the last dictatorship, which, as I understand it,
legislated entirely by executive decree.  This seems likely to
undermine Milei’s plans to make Argentina capitalist, since, as I said
above, capitalism depends on private investment to build productive
capacity.  If the current state of Argentine democracy permits such
liberty to executive decrees, that could discourage private investment
in Argentina, because it means there’s even less to prevent the next
president from simply expropriating the profits of private companies.
(But, as I’ve explained earlier, an enormous amount of that has been
going on already.)

But we aren’t living in 01872 anymore.  In 01872, economic
productivity was, to a very significant extent, a result of investors
buying (or paying to build) capital goods such as railroad tracks and
blast furnaces.  Today, skilled workers are more important to
productivity than capital goods.  And, in the same way, these policies
seem likely to discourage workers from taking employment in Argentina,
or staying in employment in Argentina instead of moving abroad, since
it undermines the job security that made Argentine employment more
appealing than salaries would suggest.

[Just before 7:00 in the morning News 18 is live streaming][135] from
Plaza de Mayo, which appears to be full of protestors and police,
blocking all the streets.  But something is wrong: the sun is in the
west, when it should be in the east at this hour.  So I think this is
“live” footage from yesterday’s protest.

[135]: https://www.youtube.com/watch?v=JYQx4Mnu974

### 02023-12-21 ###

I went to buy some newspapers this morning.  I asked the woman at the
newsstand what she thought about the *cacerolazos*.  “Well,” she said,
“there will always be people in favor and against.  60% of the people
voted for him, *y ahora todos tenemos que fumarlo*”, “now we all have
to put up with him.”

A Monster cost $1500; 200 mℓ of cream cost $1400.  In the butcher shop
I checked, no cut of beef has yet reached the threatened $8000/kg, but
Mina says that in the butcher shops in her parents’ neighborhood, some
have exceeded that level.

I’d love to have more capitalism in Argentina, but Milei’s policies
seem like cargo-cult capitalism: observing the forms of capitalism
without understanding what makes it go.  But then, there are several
big problems that they’ve handled better than I expected, so maybe
I’ll be positively surprised.

On TN, [Miguel Wiñazki suggests we may see a general strike now,][137]
10 days into Milei’s administration, and minority-left-party
presidential candidate Myriam Bregman apparently has already called
for one on Twitter.  Wiñazki said Milei’s administration really
started today.

[137]: https://youtu.be/eBvuZiP4-Ok?t=6m40s

The Economist summarizes the situation:

> Javier Milei, Argentina’s new right-wing president, issued an
> emergency decree designed to revive the country’s moribund
> economy. It slashed regulations in a number of industries and opened
> the door to the privatisation of various state-owned enterprises.
> Thousands took to the streets of Buenos Aires, the capital, in
> protest at the measures. Separately, Argentina auctioned off $3.7bn
> worth of government debt.

They have a [more comprehensive article from the 13th][138]
([archive.today link][146]).

[138]: https://www.economist.com/the-americas/2023/12/13/javier-milei-implements-shock-therapy-in-argentina
[146]: https://archive.fo/YZG5s

I just checked [Milei's Twitter account][139] and found that he’d
retweeted [a tweet referring to “kukarachas”][140] last night,
rhetoric which, as I said above, echoes that which led up to the
Rwandan genocide.  Slightly earlier, he retweeted [a video about how
yesterday’s daytime protests failed][141], under the rubric “🇦🇷Fracasó
la manifestación comunista para derrocar a Milei,” which is to say,
“The Communist protest to overthrow Milei failed.”  It looks like
government propaganda, but it seems to come originally from a
[partisan account][142] given to posting [Pravda-style falsified
infographics][143].  The video ends with three chilling intertitles:

- FRACASÓ EL PIQUETE (The protest failed)
- FRACASÓ EL CAOS (Chaos failed)
- TRIUNFÓ EL ORDEN (Order triumphed)

[139]: https://nitter.net/jmilei
[143]: https://nitter.net/MileiMilitancia/status/1737603280639873511#m
[142]: https://nitter.net/MileiMilitancia
[141]: https://nitter.net/JMilei/status/1737667024866668806#m
[140]: https://nitter.net/ResnicoffMartin/status/1737685121598144636#m

It turns out there’s also [a Twitter account for the Office of the
President][144] which has tweeted [the video of yesterday’s decree
speech][145]; above I also linked to a copy of it on YouTube.
Interestingly, he returned to attacking “*la casta política*” in the
speech, which was given while seated at a table surrounded by 12
members of his cabinet, mostly drawn from that very caste.

As for yesterday’s protest, the story Bullrich is telling (backed up
by TN and right-wing newspapers) is that only 3000 people showed up
for the protest, instead of the usual twenty to fifty thousand,
because welfare recipients were being threatened (by Kirchnerist
politicians) with the loss of their welfare benefits if they *didn’t*
show up to the protest.  (Theoretically, this is the kind of
corruption they’re campaigning against, but by threatening the loss of
welfare benefits if you *do* protest and cut off the street, they’re
also practicing it themselves.)  They set up an anonymous tip line for
people to call if they were experiencing such extortion, and they
report having received over ten thousand reports.  There’s [a Tweet
about this][147].

Despite Bullrich’s bloodthirsty nature and authoritarian rhetoric, I
have to admit that if what she is saying is true, her policies may
actually be, on balance, enhancing people’s liberty of expression
rather than suppressing them.  Even then, though, they may not be; it
seems to me that there would be a terrible chilling effect on
organizing protests if bus drivers knew they would be prosecuted for
transporting people there if the people then blocked the street,
intentionally or not, and if people who donated to political
organizations knew they would be prosecuted if the political
organizations then blocked streets in protest.  The fact that last
night the police evidently didn’t interfere at all with the protests
blocking the streets outside Congress is one data point on the other
side.

[144]: https://nitter.net/OPEArg/status/1737629486718267798#m
[145]: https://nitter.net/OPEArg/status/1737629486718267798#m
[147]: https://nitter.net/OPEArg/status/1736822803658146259#m

Oh, and apparently nobody noticed this last night, but the [decree
legalizes the dollar black market][148], at least until Congress
rejects it.  And [the employees of the *Banco Nación*, which is a
government-owned bank that is not the Central Bank, are marching to
the *Casa Rosada* this morning for a protest][149] against its decreed
privatization.

[148]: https://www.ambito.com/politica/chau-dolar-blue-javier-milei-dijo-que-no-es-ilegal-comprar-cuevas-n5904594
[149]: https://www.ambito.com/politica/dnu-javier-milei-marcha-empleados-del-banco-nacion-casa-rosada-n5904619

Also apparently the decree [outlaws effective labor strikes for
teachers, hospital workers, pharmacists, and several other “essential
services”][150], which seems facially unconstitutional.  [Many lawyers
agree that the decree is unconstitutional in many ways][152].

[150]: https://www.ambito.com/politica/dnu-javier-milei-limita-el-derecho-huelga-docentes-transportistas-y-personal-la-salud-n5904486
[152]: https://www.ambito.com/politica/dnu-desregulacion-la-economia-javier-milei-que-dicen-los-abogados-constitucionalistas-n5904442

The [Chamber of Commerce (and Services) is in favor of the
decree,][151] while Industriales Pymes Argentinos, a small-business
association, is more ambivalent.

[151]: https://www.ambito.com/economia/dnu-desregulacion-la-la-opinion-las-camaras-empresarias-n5904467

The removal of the so-called Rental Law, which created a great
scarcity of apartments to rent this year and last and is the reason
Mina and I had to take a fairly suboptimal place, [seems relatively
innocent][153].

[153]: https://www.ambito.com/economia/el-dnu-deroga-la-ley-alquileres-n5904251

[Right-wing Peronist politician Guillermo Moreno][154] gave a
surprising interview this afternoon on Peronist TV channel C5N.  He
thinks Milei will stay in power for three or four months, because
that’s how long Peronism will take to figure out how to respond
(though he’s not in favor of “resistance” against non-dictatorial
governments), and that Milei represents the second great “singularity”
in Argentine history, the first being Perón.

[154]: https://www.youtube.com/watch?v=WB9E1fV_KOI

An interesting aspect of Moreno’s point of view is that he seems to
honestly believe that the devaluation of the peso in the fake official
exchange rate amounts to a cut in wages, and none of C5N’s TV
presenters questioned him on this, though they argued with him quite
forcefully on other topics.  To me this seems like a
kindergarten-level misunderstanding of international economics, an
error the correction of which was the center of Adam Smith’s work 250
years ago.  Surely everyone understands by now that an overvalued
currency subsidizes imports and makes exports internationally
uncompetitive, while an undervalued currency does the opposite, right?
And that export-led industrialization is the engine that drove the
industrialization of Britain, Japan, Taiwan, Hong Kong, PRC, Korea,
Singapore, Indonesia, Malaysia, etc., while import-substitution
industrialization has failed everywhere it was tried except, arguably,
in the US?

No, apparently not.

At 21:38 Crónica shows a *cacerolazo* outside Congress again, but this
time they’re not blocking the street (because, I suppose, there are
only a few hundred people instead of last night’s thousands or tens of
thousands, though tens of thousands would easily fit into *Plaza
Congreso*).  They dance and beat drums and pots and pans in the
crosswalk, and then clear out of the crosswalk when the light changes,
in order to comply with Bullrich’s policy against cutting streets.  No
police presence is visible.

At 21:53 the protestors begin to cut off Entre Ríos street in front of
Congress.  A small number of police, without riot shields, helmets, or
apparent weapons, attempted to push the protestors a little bit, but
rapidly gave up and went with the flow.

C5N shows a massive street protest in Ushuaia, near Antarctica.

At 22:02 C5N shows the street full of thousands of protestors with
many sitting on the fence around Congress again.

Looking back in La Nación’s live stream, it seems like at 21:32 the
police in the cyan vests trying to keep the protest under control were
unarmed traffic cops, none of them apparently older than 20.  Aha, and
at 21:46 we see the traffic cops retreating from the traffic through
the cars and being replaced with regular city police, the kind who is
usually armed, but still without helmets or riot shields.

At 22:15 Crónica shows a *cacerolazo* in the plaza in Morón.  I’m
going to go and see what’s up.

****

Just to clarify, the currency-devaluation thing isn’t like the
question of whether protectionism is good or bad, where [90% of
economists agree that it’s bad][160], and, evidently, 10% think it’s
not.  No, this is the question of whether it's better for your
economy, given that you do have trade barriers, to use those barriers
to burden foreign industry and subsidize domestic industry, or to
subsidize foreign industry and burden domestic industry.  An
overvalued currency does the latter.

[160]: https://web.archive.org/web/20171207055442/https://core.ac.uk/download/pdf/6958854.pdf "Free Trade: Why Are Economists and Noneconomists So Far Apart? by William Poole, 02004, Federal Reserve Bank of St. Louis Review, September/October 02004, 86(5), pp. 1-6, citing Alston, Kearl, and Vaughan (1992), which is “Is There a Consensus among Economists in the 1990’s?” American Economic Review, May 1992, 82(2), pp. 203-29."

I went to the plaza.  About 1000 people were marching around it; once
I got within a block of it, about 23:10, I could hear the pots, pans,
and bottles.  A police officer wearing almost none of his uniform had
tied a plastic tape across the street to warn cars and protect the
protestors.  After making a full circuit around the plaza in the
street, the protestors entered the center of the plaza, yelled slogans
for a while (“Milei, basura!  Vos sos la dictadura!” and “El pueblo
unido jamás será vencido,” apparently unaware of the fact that the
people were so disunited that 57% or whatever of the vote had gone to
Milei), and then sang the National Anthem.  And then dispersed into
small groups and mostly went home, about 23:45.

[The town of Morón][157], population 122'000, is the head of [Morón
Partido][156], roughly a county or parish, population 334'000.  So
1000 protestors would be something like 0.3%–1% of the population,
which I feel is not really that much.  More surprisingly tonight's
protest at the Congress downtown [seems to have been about 3000
people][158], judging from C5N’s photo taken at 22:36.  They filled
about half a block of Entre Ríos, and they were drawn not just from
the 3 million inhabitants of the capital (those who didn't march in
their own neighborhoods, anyway) but the whole province.

[157]: https://es.wikipedia.org/wiki/Mor%C3%B3n_(Argentina)
[158]: https://nitter.net/Anonymou5____/status/1738011065726693743#m
[156]: https://es.wikipedia.org/wiki/Partido_de_Mor%C3%B3n

I got a double cheeseburger ($5450) and a Monster ($1200) and a 500mℓ
Coca-Cola Zero ($800).  I haven’t mentioned this for a while, but the
blue dollar is now $940 × $990.

At 00:48 [Crónica’s live stream][155] is showing some kind of televangelist
for the last half hour, which I guess means there aren’t any exciting
protests to show.

[155]: https://www.youtube.com/watch?v=avly0uwZzOE

C5N says that, in Córdoba, there were police brutality (chemical
weapons and rubber bullets), arrests, and injuries, though apparently
still no deaths.  [The arrests][161] were reportedly of Juan Celli,
Santiago Cabral, Rodrigo and Agustín Savoretti, Maximiliano
Ciambrella, and one unidentified (!) person, though Twitter may not be
the most reliable source here.  [It’s unclear what the arrests were
for.][162]

[161]: https://nitter.net/SoleDiazGarcia/status/1738020116648874025#m
[162]: https://www.ambito.com/ambito-nacional/cordoba-la-policia-reprimio-la-marcha-contra-el-dnu-milei-n5905379

[Entrepreneurs Marcos Galperin (founder of MercadoLibre, the local
eBay clone) and Pierpaolo Barbieri][164] expressed their support for
the decree.  [So did the AEA and the UIA,][167] the Argentine
Entrepreneurs' Association and the Argentine Industrial Union.

[164]: https://www.cronista.com/apertura/emprendedoress/dnu-de-milei-unicornios-y-emprendedores-se-unen-a-las-fuerzas-del-cielo/
[167]: https://www.lanacion.com.ar/economia/fuerte-apoyo-empresario-al-ajuste-de-milei-y-al-fin-de-amenazas-e-injerencias-indebidas-del-estado-nid21122023/

The administration has retreated a bit; instead of having the decree
become effective immediately, as was originally declared, [today it
announced that it will only go into effect on the 29th of
December][163], 8 days after publication, since when they wrote it
(over [the last year and a half!][165]) they forgot to specify a date!
This may give Congress time to reject it before it goes into effect,
something that has never happened before with an executive decree (as
I translate *Decreto de Necesidad y Urgencia* or DNU), though who
really knows what will happen?  Nothing like this has ever happened
before, and [the other political parties are signaling strong
opposition][168], as you’d expect; the *Unión Cívica Radical* is
demanding the decree be rescinded and resubmitted as a legislative
bill (a so-called *proyecto de ley*).  Macri’s handpicked successor as
Mayor of Buenos Aires, Horacio Rodríguez Larreta, says:

> La Argentina necesita reformas pero no por decreto. El instrumento
> para que esto suceda es una ley del Congreso. Tenemos que evitar que
> en 4 años otro presidente, con un DNU similar, pueda dar todo marcha
> atrás. La división de poderes es la pieza central de nuestra
> Republica.

That is:

> Argentina needs reforms, but not by decree.  The instrument for
> achieving them is a law of Congress.  We must avoid the situation
> where, in four years, another president, with a similar DNU, can
> undo everything.  The separation of powers is the central piece of
> our Republic.

It’s worth noting that after [Juan Manuel de Rosas established
Argentina’s first dictatorship in 01849][169] and was then overthrown
with help from Brazil, the Argentine Constitution was amended to
include a new form of high treason: abrogating the separation of
powers to unite the different powers in a single person — as Rosas had
done, as Caesar had done, and as Perón would, in effect, do.

The Head of the Cabinet has 10 days to send the decree for approval to
the Permanent Bicameral Commission of Congress.  La Nación has [a
guide to the process.][166]

[163]: https://www.cronista.com/economia-politica/marcha-atras-del-gobierno-se-definio-la-fecha-en-que-entrara-en-vigencia-el-dnu/
[165]: https://www.infobae.com/politica/2023/12/21/federico-sturzenegger-el-cerebro-del-dnu-revelo-cual-es-la-reforma-mas-importante-y-contesto-las-criticas/
[166]: https://www.lanacion.com.ar/politica/fuerte-rechazo-en-el-congreso-al-megadecreto-de-javier-milei-y-se-gesta-una-mayoria-dispuesta-a-nid21122023/
[168]: https://www.clarin.com/politica/oposicion-cierra-filas-presiona-milei-marcha-dnu-amenazan-invalidarlo_0_o3ZpU2cC6v.html
[169]: https://en.wikipedia.org/wiki/Juan_Manuel_de_Rosas#Ruler_of_Argentina

All in all, this seems like a dramatic de-escalation in the threat of
a popular revolt, political violence in the streets, and
Venezuela-style civil war.  And as [Javier Smaldone says:][159]

> No quería que @JMilei fuera presidente.  
> Ahora no quiero que las cosas salgan mal.  
> No quiero que todo reviente.  
> No quiero que se tenga que ir.  
> Deseo que haga un buen gobierno.
>
> No insista, soy argentino, vivo en la Argentina y tengo hijos,
> familia y amigos acá.

[159]: https://nitter.net/mis2centavos/status/1726437306809094524#m

That is:

> I didn’t want @JMilei to be president.  
> Now I don’t want things to turn out badly.  
> I don’t want everything to explode.  
> I don’t want him to have to resign.  
> I want him to run a good administration.
>
> Don’t argue with me, I’m Argentine, I live in Argentina, and I have
> children, family, and friends here.

C5N also says the CGT is planning to march on the Supreme Court on
Wednesday, which is five days from now, a full 50% of the time Milei
has been in power so far.

Wednesday is predicted to go from 19° at 4 AM to 29° at 17:00, which
is quite a bit hotter than today.  But we won't have any really hot
temperatures for a week or two, probably.

### 02023-12-22 ###

A couple of notes from today’s news.  [Legislators of Milei’s
party][170] promise to seek consensus on the decree in Congress, and
they also promise a legislative bill next week, apparently for
measures not included in the decree, [immediately after
Christmas][171].  Apparently [56% of Argentines polled by Taquion are
opposed to the decree][172], 30% in favor, and 14% neutral.  Milei,
however, [refuses to withdraw the decree in favor of a legislative
bill][174] (or bills) and says the opposition “will pay the political
cost”, which I guess means he thinks his reforms will become a lot
more popular before the next election.  Or he thinks they're more
popular than Taquion does; in a nationwide online *DC Consultores*
poll, [71.3% said they were in agreement with the decree][175],
according to [La Voz del Interior][176], a newspaper from Córdoba I
don't remember but [which was founded in 01904.][177] It doesn’t, in
particular, seem to be an ultra-right-wing mouthpiece.

[170]: https://www.cronista.com/economia-politica/diputados-la-libertad-avanza-no-descarta-rediscutir-el-dnu-de-javier-milei/
[174]: https://www.clarin.com/politica/javier-milei-descarta-cambiar-dnu-ley-advierte-oposicion-va-pagar-costo-politico-gente_0_xOktLFUuBX.html
[171]: https://www.cronista.com/economia-politica/el-gobierno-acelera-el-llamado-a-sesiones-extraordinarias-enviaria-una-ley-omnibus-despues-de-navidad/
[172]: https://www.cronista.com/economia-politica/que-opinan-los-argentinos-del-dnu-de-milei-segun-la-ultima-encuesta/
[175]: https://nitter.net/Santiago_Oria/status/1738192258644517369#m
[176]: https://www.lavoz.com.ar/politica/que-dice-la-primera-encuesta-sobre-el-dnu-de-javier-milei-y-el-protocolo-antipiquetes-de-bullrich/
[177]: https://es.wikipedia.org/wiki/La_Voz_del_Interior

56% opposed is significantly more than the 43% who voted against him
in the election, but I’m not sure what the other 13% were thinking.

Crónica TV is showing Christmas shopping prices in the Once
neighborhood.  I looked back in the stream to see if there were any
protests, but no, they were just calling out that sandwich shops are
offering sandwiches with three monthly installments, I guess so that
you can buy your Christmas dinner on credit.

October 02024
-------------

Inflation continued exploding for a while, but has now calmed down.
The exchange rate has also calmed down; [it’s currently $1145×1175 per
dollar][178], and the official rate [has caught up significantly at
$951.36×$1010][179], reducing the gap to only about 15%, the lowest
it’s been in years.

[178]: https://dolarhistorico.com/cotizacion-dolar-blue/mes/octubre-2024
[179]: https://dolarhistorico.com/cotizacion-dolar-oficial/mes/octubre-2024

With the yearly inflation adjustment a few months ago, our rent went
up from $80'000 a month to $275'500.

### 02024-10-10 ###

I just went shopping for food in my neighborhood.  Here’s the
cost breakdown:

- Can of Monster, $2000
- Jar of peanut butter, 390g, $2750
- 200 grams nutritional yeast, $7500
- 200g dried nonfat milk, $2700
- loaf of whole-wheat bread, $3750
- 3 kg chicken wings, $3750
- ½kg roast beef, ground, $4000
- one red bell pepper, $1000
- four *zapallito* zucchini squash (≈1kg), $1400
- 200g roasted shelled peanuts, $1000
- 190g of spreadable modified food starch and gelatin flavored with
  cheese, $2450
- 100g walnuts, $3000
- 50g unsweetened cacao powder, $1800
- six hot dogs (≈230g), $1700
- 190g yogurt, $1000
- 300g sunflower seeds, $900
- *Dos Corazones* brand chocolate candy, $1600

There was also a 1.5ℓ bottle of Cunnington diet cola, $1400, which I
didn't notice I hadn’t actually bought.

In total I spent about $41000 going grocery shopping for 100 minutes,
buying about 9 kg of food.  This involved hitting the grocery store,
the greengrocer, the butcher shop, and the health food store.  I was
able to get everything on my list except for the fancy mustard.

The yeast is a different brand than I usually buy, but the health food
store was out of the imported stuff.

Yesterday Congress voted on whether to override Milei’s veto on the
budget for the university system.  The lower house, *Diputados*,
sustained the veto.  As a result, today Mina’s classes at the UTN were
canceled as part of a strike.

### 02024-10-11 ###

