Hershey fonts are amazing
=========================

I’d seen Hershey fonts before (usually as thin jaggy stroke fonts,
with the singular merit of actually being stroke fonts rather than
outline fonts), but today I learned some neat things about them that I
didn’t know before.  Namely, they’re potentially very high in visual
quality (almost to the level of the Computer Modern fonts) and
outstandingly tiny (7KB per scalable ASCII font plus 223 lines of C to
process them), and they also come in a very interesting vector file
format and use a very simple drawing model.

They have some significant limitations: no kerning included, no
automatic ligatures, no curves, no filled areas, no Unicode encoding
information, no combining characters (though overstrike is certainly
possible), no colors, no stroke thickness information, no
parametrization, and no vertical escapement for CJK fonts.  The
original fonts do cover Latin, Cyrillic, Greek, mathematical and
meteorological symbols, Japanese kana, and a few Chinese characters,
but don’t even contain accents that you could overstrike.

At the same time, for many purposes, many of these are advantages
rather than disadvantages.  Because of them, support for Hershey fonts
only requires a kilobyte or two of machine code for the file format,
some way to draw a straight line, and a few kilobytes per font.

Overview in more detail
-----------------------

They’re proportional, and the **visual quality** with the right pen
width seems almost comparable to T<sub>E</sub>X’s Computer Modern
fonts:

![(sample of math text typeset with Hershey fonts in the 01970s)](hershey-sample.jpg)

As for **outstandingly tiny**, all 33 fonts in Debian’s
hershey-fonts-data /usr/share/hershey-fonts total 213kB, or 77kB as a
gzipped tar file.  That’s 2.3K compressed per scalable typeface!  Most
of them contain 96 glyphs, and there are 3287 glyphs in all, so that’s
65 bytes per glyph (comparable to a 16×32 1-bit pixel font) or 23
compressed bytes per glyph.

The **file format**, James Hurt Format, is mostly two bytes per vector
point and looks like this, with each line representing a glyph:

    3607 56I[MOMXKYLYNZO[PZRYUX RNPNYPZ ROOOXQYRY RMOOORNTMUNWOYOWPW\V_TaRbQaO`M` RSNVPV\ RSaQ`P` RRNSOUPUZV]V_ RTaS`Q_O_M`
      521 11G]KFKULXNZQ[S[VZXXYUYF
    12345 14G\KFK[ RKFTFWGXHYJYMXOWPTQKQ
     3024 54H\KFW[ RLFX[ RMFY[ RXGLZ RIFPF RUF[F RI[O[ RT[[[ RJFMH RNFMH ROFMG RVFXG RZFXG RLZJ[ RLZN[ RWZU[ RWYV[ RWYZ[

The **drawing model** is about as simple as a vector drawing format
can be; it’s a series of straight lines between integer grid points,
sometimes interrupted by lifting the pen for the next movement
(denoted by " R" in JHF, so typically each visual word represents a
polyline).

History
-------

As [Wikipedia explains][0], in 01967 Dr. Allen Vincent Hershey at the
US Naval Weapons Laboratory digitized a bunch of fonts.  He published
this as “Calligraphy for Computers” (US Naval Weapons Laboratory,
1967-08-01, NWL Report No. 2101, NTIS accession number AD-662 398), a
256-page document which is in the public domain and available [from
the Internet Archive][5] (two copies of apparently [the same scan][6])
and Google Books (a [worse scan][7] from Stanford’s library, but of
course Google works hard to keep you from saving a copy in case they
want to memory-hole it later.)

[0]: https://en.wikipedia.org/wiki/Hershey_fonts
[5]: https://archive.org/details/DTIC_AD0662398/page/n1/mode/2up
[6]: https://archive.org/details/hershey-calligraphy_for_computers/page/n1/mode/2up
[7]: https://books.google.com/books?id=qFFCAAAAIAAJ&pg=PP8

In 01976 Wolcott and Hilsenrath at the US National Bureau of Standards
published [NBS Special Publication 424][1] (the same scan available
[from the US government][36]), “A Contribution to Computer Typesetting
Techniques”, an 188-page book.  It contains 1377 of Hershey’s glyphs
in numeric form, and demonstrating specimens like the one above.

[1]: https://ia904509.us.archive.org/1/items/contributiontoco424wolc/contributiontoco424wolc.pdf "LCCN 75-619219"
[36]: https://www.govinfo.gov/content/pkg/GOVPUB-C13-a73d50f2294c93f29e5bdd9c05baf50a/pdf/GOVPUB-C13-a73d50f2294c93f29e5bdd9c05baf50a.pdf

These of course cover a much smaller repertoire than T<sub>E</sub>X’s
fonts, Unifont, or Noto.  There’s a Japanese font, but it doesn't even
cover all the Joyo Kanji.

Wolcott and Hilsenrath explain that you can use them for making
microfilm with a “COM (Computer Output on Microfilm)” device
(apparently a CRT photographed by a 35mm camera, probably with a long
exposure time) or plotting on paper, and they strongly commend
Hershey’s work:

> The determination of the location and number of points to use in
> approximating a particular character requires a rare combination of
> interests and talents. For one man to achieve the digitizing of
> literally thousands of characters requires a large measure of
> motivation, industry and fortitude — the last in nearly all of its
> dictionary connotations. ... Today there are more automatic
> techniques for digitizing oriental or even occidental alphabets but
> these systems normally involve storing much more information per
> character than is needed in Hershey’s method.

From that date until at least the mid-01980s, the NTIS provided the
fonts on magtape for duplication costs, but prohibited people from
copying the fonts in that format.  There were a variety of
publications on them in the 70s.  Patrick Michael Doyle [did his
master’s thesis at the Naval Postgraduate School in 01977][11] on
rasterizing Hershey fonts on Unix for a 200dpi VERSATEC CRT
typesetter.  He explains:

> In an attempt to improve this situation [of only having four fonts
> and no usable font editor on their PDP-11], 48 additional fonts were
> obtained from external sources.  Thirty-four of these fonts were
> already in digitized form and as a result were limited in point
> sizes available. They also required a great deal of storage in that
> form. Because the 14 Hershey fonts were available invector form
> rather than dot matrix, they were acquired in the hopes that they
> could be adapted for use in computerized typesetting in a form that
> reauired less storage. The 34 digitized fonts, for example, required
> 643 512-byte blocks of storage while the Hershey fonts, stored in
> vector form, required only 193 blocks.
>
> This thesis was directed toward finding an algorithm that would
> allow the Hershey fonts to remain in memory in vector form but
> convert them to a digitized form in any point size required by the
> user.
>
> B. INITIAL CONVERSION
>
> 1. Original Format
> 
> The vector definitions of the 14 Hershey fonts were obtained from a
> tape available through the National Bureau of Standards
> [Ref. 16]. The original tape contained approximately 360K bytes of
> data representing 8-bit EBCDIC character codes.  The tape contained
> just over 4600 card images, where each card image contained a
> character identification number, a card sequence number, and
> coordinate pairs.  As a result, the data was essentially stored as a
> stream of numbers.
> 
> Hershey’s original definitions used integers between -49 and +49 to
> represent the endpoints of his vectors, with a (50,00) coordinate
> pair representing a “lift pen” command and a (50,50) representing
> “end of character”. So that all of the coordinate pairs would fit
> into four bytes, negative values were subtracted from 100 and stored
> as two-digit numbers greater than 50 so that they could be
> differentiated from a positive integer. For example, (10,10) was
> stored as “1010” but (-10,10) was stored as “9010”.

[11]: https://apps.dtic.mil/sti/citations/ADA042291 "AD-A042 291, 173 pp."

Most of the rest of the thesis is about learning the basics of
geometric algorithms and doesn’t even reflect the best knowledge at
the time.

Wolcott and Hilsenrath clarify the coordinate system a bit (pp. 4–5,
11–12/188):

> The characters are described as uniplex, duplex, or triplex
> according to the number of parallel strokes used in the construction
> of the character. The description as simplex, complex, or gothic,
> indicates the extent to which the characters contain tapered
> segments. Three sizes of characters are available: the principal or
> normal size (21 raster units high, em=32), the indexical size (13
> raster units high, em=21), and the cartographic size (9 raster units
> high). (...)
> 
> In the Hershey system, characters are drawn by connecting lines
> between successive (x,y) coordinate pairs.  The coordinates of each
> character are given in ‘raster coordinates’, which are integers
> ranging from 49 to -49. The (x,y) coordinates for each character in
> the occidental repertory are given in Appendix A.  A useful quantity
> is the printer’s em, or the distance between the bottoms of two
> successive lines of close packed text.  The em is 32 raster units
> for characters in the principal size, and 21 raster units for the
> indexical size.
>
> The table in Appendix A [*i.e.*, the vast bulk of the book] is
> organized in the following way: The first column is the character
> number, the first pair of numbers separated by colons (:) are the
> left and right boundaries of the character in raster coordinates,
> and succeeding pairs of numbers set off by colons denote the (x,y)
> set for that character.  An (x,y) coordinate pair of (-64,0)
> indicates that the pen is lifted at that point in the character; a
> coordinate pair of (-64,-64) indicates that the end of the character
> has been reached.

I am not sure whether I have multiple sizes of the same characters or
whether I only have one of them.

Hershey seems to have mostly disappeared from the font world after
about 01972, when he published a paper called “A computer system for
scientific typography” (Comput. Graph. Image Process. 1(4): 373-385
(1972)), though in 01986 he published [a 20-page paper from the Naval
Postgraduate School][2] about computational fluid dynamics, suggesting
he may have gone to teach at NPS.  It looks like it uses Hershey fonts
for the math, but I’m not sure.  However, see below about “Hershey
1995”!

[2]: https://archive.org/details/potentialgradien00herspdf/mode/2up

On 01986-04-01 the Usenet Font Consortium, a mailing list represented
by Pete Holzmann of Octopus Enterprises (Cupertino, CA), posted
Hershey’s data to mod.sources in a “JHF” format created by James Hurt
of Cognition, Inc. (Billerica, MA), according to [AVHershey-OTF][4]
and [HERSHEY.DOC from the GhostScript distribution][8].  This includes
the reassuring notice:

> The font data in this distribution may be converted into any other
> format *EXCEPT* the format distributed by the U.S. NTIS (which
> organization holds the rights to the distribution and use of the
> font data in that particular format). Not that anybody would really
> *want* to use their format... each point is described in eight bytes
> as "xxx yyy:", where xxx and yyy are the coordinate values as ASCII
> numbers.
>
> *PLEASE* be reassured: The legal implications of NTIS' attempt to
> control a particular form of the Hershey Fonts *are*
> troubling. HOWEVER: We have been endlessly and repeatedly assured by
> NTIS that they do not care what we do with our version of the font
> data, they do not want to know about it, they understand that we are
> distributing this information all over the world, etc etc etc... but
> because it isn't in their *exact* distribution format, they just
> don't care!!! So go ahead and use the data with a clear conscience!
> (If you feel bad about it, take a smaller deduction for something on
> your taxes next week...)

[4]: https://github.com/scruss/AVHershey-OTF
[8]: https://web.mit.edu/ghostscript/www/Hershey.htm

Note that this 8-bytes-per-point format is not the same as the
4-bytes-per-point format described in Doyle’s thesis!  Apparently the
NTIS changed formats at some point.  Maybe they got sick of explaining
that 90 really meant -10.

The comments in [GNU plotutils 2.6][24] file `libplot/g_her_glyph.c`
elaborate a bit:

> Dr. Hershey digitized the glyphs c. 1967, at what is now the
> U.S. Naval Surface Weapons Center in Dahlgren, Virginia.  For many
> years he distributed copies of the glyphs, and his typographic
> software, on magnetic tape.  Over 120 copies of the tape were
> distributed.  There have been many other distributions of the
> glyphs.  In the 1970's they were incorporated, not always with
> attribution, in several commercial plotting packages.  They were
> first freely distributed in 1985(?), by being posted to Usenet (to
> vol. 4 of mod.sources) by Pete Holzmann `<pete@xc.org>`, then at
> Octopus Enterprises.  In the 1980's the glyphs were incorporated in
> at least two freeware plotting programs, Nelson Beebe's PLOT79 and
> Tim Pearson's PGPLOT.  The latter is still available (see
> <http://www.astro.caltech.edu/~tjp/pgplot/>)

The file contains 4400 Hershey-font glyphs and a number of other
glyphs in Hershey format: Thomas Wolff’s symbol glyphs, Nelson Beebe’s
glyphs, Robert Maier’s glyphs, and some modified versions of the
Hershey glyphs: inverted glyphs like ¡ and ¿, dotless i, etc.  Also,
it has accents!  Acute, grave, diaeresis (misspelled), circumflex,
tilde, ring, etc.

Digging a bit more on Holzmann, [“The Last Bit: Missionaries Lag No
More”][42] says,

> Today, we’re suggesting that missionaries lag no more. Why? (...)
> maybe it’s because certain pioneers started orgs like CrossConnect
> (the OLD CrossConnect — the one that started xc.org — the Pete
> Holzmann and Jonathan Marsden crowd), JAARs, and more.

xc.org is still CrossConnect.  The [Paraclete Mission Group has a page
on him][43] with a photo:

> Pete Holzmann is an advisor in information technology to Christian
> leaders in mission, churches, philanthropy, and the marketplace. He
> joined Paraclete after having spent over a decade as a Silicon
> Valley computer consultant

Perhaps it would be worth contacting him.

[42]: https://brigada.org/2016/09/25_19030
[43]: https://www.paraclete.net/associate/pete-holzmann/

Since then, I think I have seen these fonts used in Ghostscript, and
on my system there are also copies bundled into R and Inkscape.  It
also seems to be used in other free software like [plotutils][24],
GMU’s NASA-funded [GrADS GIS software][9] which only supports other
fonts [as of version 2.1][10] and UIUC’s [Visual Molecular Dynamics
(VMD)][16], as well as proprietary software like [Turbo Pascal 4 and
up][38] and the Interactive Data Language.  To my knowledge, no legal
trouble has ensued, and it’s not clear on what grounds the NTIS would
sue, given that US government work is statutorily not subject to US
copyright, fonts are statutorily not subject to US copyright, and
databases of factual data are not subject to US copyright due to the
*Feist* decision.  Moreover, [the tapes are no longer available][23].

[9]: http://cola.gmu.edu/grads/
[10]: https://www.cpc.ncep.noaa.gov/products/outreach/workshops/CDPW38/4_Thursday/1_Morning/Adams.pdf
[16]: https://www.ks.uiuc.edu/Research/vmd/
[23]: https://github.com/scruss/python-hershey
[24]: https://www.gnu.org/software/plotutils/
[38]: https://lwn.net/Articles/655601/

In 02003–02006 [David M. MacMillan and Rollande Krandall wrote a
GFDL-licensed book about the Hershey fonts][26] which has since
disappeared from the Web at that URL.  The Archive unfortunately only
has the table of contents.  MacMillan later [wrote an explanation
about the book][32] and [moved it to a new domain][27], where it
appears to be as complete as it ever was, and relicensed it under
CC-BY-SA.  It contains, in particular, a [skeletal biography of
Hershey][28], [a summary of the 01967 paper][29], and a [summary of the
NORC encoding described in Doyle’s thesis][30].

He’s also published [a copy of the original Holzmann Usenet
posts][33] containing shar files.

It also contains a rather astonishing bibliographic entry:

> [Hershey 1995] Hershey, Dr. Allen V[incent]. “[Cartography and
> Typography with True BASIC.][31]” Monterey, CA: US Naval Postgraduate
> School, September 1995. NPS-09-95-003. US NTIS stock number
> ADA299505. Cited in [gp241].

This lists Hershey as a “Research Affiliate” of the NPS.  He must have
been 85 years old when he wrote this, and it has some amusing notes in
the Introduction:

> The Macintosh computer has some neat features. Files are collected
> in folders. Each file has an icon, (...) At the Naval Postgraduate
> School there are Macintosh Quadras and at Kinkos there are Power
> Macintoshes. Unfortunately these computers refuse to accept
> Microsoft BASIC. The probability that they can accept True BASIC has
> been the source of motivation for the present investigation.
>
> The absence of interchangeability between machines and programs is
> pathetic.

He evidently invented his own equivalent of the JHF encoding:

> For home computers each coordinate is expressed by a single byte
> with a bias of 64.  Each datum is a two-byte word with the following
> format:
>
>     Byte  Interpretation
>        1  X-coordinate
>        2  Y-coordinate
>
> This is a two-fold compression of the data [relative to the decimal
> NORC encoding, described later].  The end-of-line word is 0,64 and
> the end-of-character word is 0,0. From the biased data it is
> necessary only to subtract 64 to obtain the unbiased data.

Also, his own vector graphics format, with one instruction per line.
A substantial part of the 88 pages of the report are listings of BASIC
programs; another substantial part is handwritten input files in
coding forms for his page description languages, and the typeset
results.

[26]: https://web.archive.org/web/20060620084824/http://www.circuitousroot.com/notebooks/typography/hershey/index.html "Exploring Dr. Hershey’s Typography"
[27]: https://www.galleyrack.com/hershey/old-hershey-webpages/index.html
[28]: https://www.galleyrack.com/hershey/old-hershey-webpages/biography/index.html
[29]: https://www.galleyrack.com/hershey/old-hershey-webpages/calligraphy/index.html
[30]: https://www.galleyrack.com/hershey/old-hershey-webpages/glyph-grid/index.html
[31]: https://core.ac.uk/download/pdf/36717536.pdf
[32]: https://circuitousroot.com/artifice/letters/hershey/index.html
[33]: https://www.galleyrack.com/hershey/holzmann-USENET-distribution/index.html

In 02015, Frank Grießhammer of Adobe [gave a speech at TypeCon][19]
which has been [recorded on YouTube][34] promising to make an OpenType
version of the Hershey fonts, and has forked [Kamal Mostafa’s C
library][20], which he has since done [some work on][37], and [Stewart
C. Russell’s Python library][21], but doesn’t seem to have delivered
the promised fonts.  Russell has, though, in his [AVHershey-OTF][4]
project.

[19]: https://lwn.net/Articles/654819/
[37]: https://github.com/kamalmostafa/hershey-fonts/compare/master...frankrolf:hershey-fonts:master
[34]: https://www.youtube.com/watch?v=xQNHAWrR_eg
[21]: https://github.com/frankrolf/python-hershey
[20]: https://github.com/frankrolf/hershey-fonts

Existing software
-----------------

[Kamal Mostafa wrote a tiny GPL C library][18a] (223 lines of code) to
make Hershey fonts available in Debian and integrate them into Gnuplot
and [OpenLase][17].  There’s an [R package for inspecting the stroke
endpoints][12] which includes things like animated rendering.  Jari
Komppa (Sol_HSA), “looking for some decent free vector fonts that are
not truetype / freetype based”, wrote [a library in C++ for feeding
them to OpenGL][15].  And there’s a [library for Proce55ing.][35] And
[Laurence D’Oliveiro wrote a Python library][39] which can convert
them to PostScript Type 3 fonts.

[12]: https://coolbutuseless.github.io/package/hershey/
[15]: https://solhsa.com/hershey/
[17]: https://www.youtube.com/watch?v=8y04_yhh-ZM
[18a]: http://www.whence.com/hershey-fonts/
[35]: https://github.com/ixd-hof/HersheyFont
[39]: https://gitlab.com/ldo/hersheypy

Mostafa provides a somewhat reorganized JHF setup and a [GPLed C
interface][18] whose beautifully parsimonious but fairly
memory-optimized API could be cruelly abbreviated as follows:

[18]: https://github.com/kamalmostafa/hershey-fonts/blob/master/libhersheyfont/hersheyfont.h

    struct hershey_font {
      struct hershey_glyph glyphs[256];  /* indexed by ASCII value */
    } *hershey_font_load(const char *fontname),
      *hershey_jhf_font_load(const char *jhffile);

    struct hershey_glyph {
      unsigned glyphnum;
      unsigned short width, npaths;
      struct hershey_path {
        struct hershey_path *next;
        unsigned short nverts;
        struct hershey_vertex { short x, y; } verts[];
      } *paths;
    } *hershey_font_glyph(struct hershey_font *hf, unsigned char c);

    void hershey_font_free(struct hershey_font *hf);

The default Debian setup compiles the 223-line implementation to 2077
bytes of amd64 code (text segment size in the libhersheyfont.a).
Compiling it for ARM Thumb2 with -Os I get 1152 bytes.

The JHF file format
-------------------

Hershey’s judicious and artistic choice of points is a major factor in
the astounding compactness of these letterforms, as Wolcott and
Hilsenrath said, but the JHF file format I showed examples of above is
another major factor; this single 37-byte line of text:

    12345 14G\KFK[ RKFTFWGXHYJYMXOWPTQKQ

represents 12 coordinate pairs and 10 lines.  What appears to be an
archived copy of Holzmann’s original mod.sources post documents the
format as follows:

    The top line and bottom line, which are normally used to define vertical
            spacing, are not given. Maybe somebody can determine appropriate
            values for these!

    The left line and right line, which are used to define horizontal spacing,
            are provided with each character in the database.

    Format of Hershey glyphs:

    5 bytes - glyphnumber
    3 bytes - length of data  length in 16-bit words including left&right numbers
    1 byte  - x value of left margin
    1 byte  - x value of right margin
    (length*2)-2 bytes      - stroke data

    left&right margins and stroke data are biased by the value of the letter 'R'
    Subtract the letter 'R' to get the data.

    e.g. if the data byte is 'R', the data is 0
         if the data byte is 'T', the data is +2
         if the data byte is 'J', the data is -8

    and so on...

    The coordinate system is x-y, with the origin (0,0) in the center of the
    glyph.  X increases to the right and y increases *down*.

    The stroke data is pairs of bytes, one byte for x followed by one byte for y.

    An 'R' in the stroke data indicates a 'lift pen and move' instruction.

The glyphnumber “12345” in the above is plainly garbage, but the rest
fits.

The letter 'R' in ASCII is 82, so the -49 to +49 range of the tape
data Doyle used corresponds to 33 ('!') to 131, which would be four
characters after the last ASCII character, DEL (delete).  So with
printable ASCII this can represent coordinates from -50 (space) to
+45, which I guess is close enough.

The original data [was hard-wrapped at 72 columns][13], which Kamal
Mostafa wrote a [Python script to unwrap][22].  It’s possible to have
8 bytes of stroke data that could be read as a glyph number and size,
if you had four points whose (x, y) was in the right range of large
negative numbers, but I don’t think it happened; in any case, the
three-digit length should tell you if it’s happened, which is what
Mostafa’s script uses.

[13]: https://coolbutuseless.github.io/package/hershey/articles/hershey-font-format.html
[22]: https://github.com/kamalmostafa/hershey-fonts/blob/master/tools/hershey-jhf-fix-linebreaks.py

Paul Bourke wrote [an exploration of the JHF format in 01997][14]
accompanied with a visualization which has been used by others.  He
explains:

> The structure is basically as follows: each character consists of a
> number 1->4000 (not all used) in column 0:4, the number of vertices
> in columns 5:7, the left hand position in column 8, the right hand
> position in column 9, and finally the vertices in single character
> pairs. All coordinates are given relative to the ascii value of
> 'R'. If the coordinate value is " R" that indicates a pen up
> operation.
> 
> As an example consider the 8th symbol
> 
>     8  9MWOMOV RUMUV ROQUQ
> 
> It has 9 coordinate pairs (this includes the left and right position).  
> The left position is 'M' - 'R' = -5  
> The right position is 'W' - 'R' = 5  
> The first coordinate is "OM" = (-3,-5)  
> The second coordinate is "OV" = (-3,4)  
> Raise the pen " R"  
> Move to "UM" = (3,-5)  
> Draw to "UV" = (3,4)  
> Raise the pen " R"  
> Move to "OQ" = (-3,-1)  
> Draw to "UQ" = (3,-1)  
> Drawing this out on a piece of paper will reveal it represents an 'H'.   

[14]: https://paulbourke.net/dataformats/hershey/

This glyph doesn’t appear in Mostafa’s version of the Hershey fonts,
but [Reinholtz’s version][25] seems to have 1597 lines which have already been
unwrapped.

[25]: https://github.com/reinholtz/hershey-fonts-with-unicode/blob/master/hersheyfonts.txt

Wolcott and Hilsenrath obliquely mention a vector file format for
entire pages (p. 6, 12/188):

> While in principle it would be possible to do this [plot a page]
> on-line with a main frame [sic] computer, in practice it is more
> convenient to generate an intermediate file on magnetic tape, and
> then to use this magnetic tape to drive the graphic output
> device. (...) The speed of a pen plotter is one to two characters
> per second depending on the size and complexity of the character.

They do explain that there is a “programmable mini-computer” built
into their COM, which I suppose means they have some flexibility in
designing their own file formats.  But they don’t talk at all about
those formats!

A minimal C implementation permits deep embedding
-------------------------------------------------

I wrote [a tiny library called Smolhershey to read JHF fonts][40].
The main `sh_show` subroutine just calls a user-defined callback for
each line segment and contains 14 lines of C; for efficient access, it
indexes an array of `u8` pointers to the beginnings of JHF lines, plus
one to the end of the file, and it updates a notion of “cursor” at the
beginning and end of each glyph, so calling it repeatedly with no
intervening code is sufficient to set a line of text.  The pointer
array is set up by a second subroutine, `sh_load_font`, which scans
over a memory array looking for newlines.  The two subroutines total
under 1K of code; compiled with size optimization for Thumb, they
total under 200 bytes.

[40]: http://canonical.org/~kragen/sw/dev/smolhershey.md.html

Possible things to do
---------------------

There are several things I’d like to do with Hershey fonts that I
haven’t done yet.

### Kerning ###

I want to precompute some kerning tables.  The basic idea is that each
character has a left class and a right class, drawn from a small
number of classes such as 16 or 32, and when `sh_show` starts to draw
a character, it combines the previous character’s right class (stored
in the graphics context) with the current character’s left class to
form a table index, and then looks up the kern in the kerning table to
figure out how much to adjust currentpoint.  Like maybe six lines of
code and 12 machine instructions:

    sh_kern kern = gc->kern;
    if (kern) {
        int kidx = gc->lastclass << 4 | kern->leftclass[glyph_index];
        cp.x += kern->kerns[kidx];
        gc->lastclass = kern->rightclass[glyph_index];
    }

Admittedly this is significant in the context of 37 lines of code and
80 machine instructions for my entire Smolhershey library, and that
doesn’t count the complexity of loading the tables, which might have
different lengths, especially for a font with a lot of glyphs.

### Curve guessing ###

The Hershey fonts have quite good visual quality at small sizes, but
at large sizes, the polygonal quality of the paths becomes
objectionable.

### Converting stroke fonts to outline fonts ###

TrueType only support outline fonts, so if you want to make a TrueType
version of any stroke font (including Hershey fonts) you need to
generate outlines from it.

You can convert any stroke font to an outline font by stroking the
font paths; the simplest way is to flatten the paths to segments (as
Hershey already did by hand in the case of Hershey fonts; but some
stroke font formats do support curves),create a capsule (a rectangle
with semicircles on its ends) for each line segment, and then trace
the outline of the union of all the capsules.  PostScript and derived
systems also offer other kinds of line caps and line joins, with
miters and miter limits and whatnot.  And, of course, just as when
rendering directly to pixels, the line width is a free variable here;
you can pick different stroke weights.

The capsule is the Minkowski sum of a circle and a line segment; you
can generalize this, as Metafont does, by taking the Minkowski sum
with an arbitrary pen-nib shape, which permits the line width of each
segment to vary according to its angle, providing, for example,
vertical stress.

More generally, you can vary the widths of segments according to
arbitrary algorithms, including varying continuously along a segment.

A goofy idea I’ve been thinking about is to compute an SDF of the pure
stroke font and threshold it to get the font outline.  You can add
other stuff to the SDF (noise, gradients) to perturb the resulting
outline.  To stress strokes at a certain angle, compute the SDF in a
stretched coordinate system.  Maybe X-displacement weighs half as much
as Y-displacement, for example.

### More elaborate ways to render to pixels ###

Computing an SDF might be useful for rendering to pixels as well, and
there are some interesting things you can do there.  Instead of
computing a single stretched SDF, for example, compute three stretched
SDFs whose level sets around a point are ellipses tilted at different
angles.

This is appealing because you can precompute the SDF arrays for a
particular glyph, and then, by interpolating and extrapolating between
these three SDFs, you can rasterize an arbitrary stroke weight with an
arbitrary stress angle.

You can compute (a good approximation of) each initial SDF as follows:

- make a pixel buffer to hold a conservatively approximated lower
  bound on the SDF,
- fill it with ∞ (a trivially correct lower bound),
- render a two-pixel antialiased line through it along the stroke’s
  path, overwriting the ∞ for those two pixels with the actual
  distance to the stroke; and then
- successively relax the distance field by finding a pixel whose lower
  bound can be improved because a nearby pixel’s lower bound has
  recently been improved; we can limit each pixel to being improved
  once if we order the pixels to relax in a heap implementing a
  priority queue by their new lower bound.

This is almost the same algorithm [I used for skeletonization in
circlegrid][41].  It’s linear in the number of pixels if the heap
operations are constant-time, but it has a constant factor of
neighborhood size, because the same pixel may get moved up the heap
once by each of its neighbors.

[41]: http://canonical.org/~kragen/sw/dev3/circlegrid.html
