Consider this example one-line PEG, using a syntax which defines each
nonterminal inline where it’s first used.  `s` is “sum”, `t` is
“term”, `v` is “variable”, `n` is “number", and `op` is “operator”.
For simplicity we assume there is no whitespace.

    <s: <t: <v: [a-z]+> | <n: [0-9]+> | "(" <s> ")"> (<op: "+" | "-"> <t>)*>

(See file `monkey-paw-pattern-rewriting.md` for more detail.)

This is sufficient to explain how to convert an expression like

    1-xo-(y-2)

into a tagged tree like this:

    s(t(n("1"))
      op("-") t(v("xo"))
      op("-") t("(" s(t(v("y")) op("-") t(n("2"))) ")"))

Or, in an XMLisher format, though I think this is less readable:

    <s><t><n>1</n></t>
       <op>-</op><t><v>xo</v></t>
       <op>-</op><t>(<s><t><v>y</v><op>-</op><t><n>2</n></t></s>)</t>
    </s>

Or, in the Jevko-like notation used in file
`labeled-tree-programming-language.md`:

    s {
        t { n {1} }
        op {-} t { v {xo} }
        op {-} t { {(} s { t { v {y} } op {-} t { n {2} } } {)} }
    }

In a sense, the byte offsets where each successfully parsed
nonterminal began and ended, along with the nonterminal type, give us
all the information we need to process the syntax tree efficiently.

Coincidentally, this is precisely the information that must be
recorded in the memo table to get Packrat’s linear parsing time
guarantee.  To use a memo-table entry, we need to be able to look up,
given a nonterminal and a starting position in the input, the ending
position of that nonterminal in the input, or the fact that the
nonterminal fails when started at that position.

> For error reporting, it is also often useful to know how far ahead
> the parsing of that nonterminal looked in the input, but it’s not
> necessary to get Packrat to work.  That information also helps with
> updating the memo table after incremental changes in the input.  For
> example, in the case above, if the character after the `2` were
> changed to a `3`:
> 
>     1-xo-(y-23
> 
> the `n` nonterminal would now parse `23` rather than `2`; but since
> that is the end of input, the overall grammar would only be able to
> parse `1-xo`.  So the parsing of the `2` by `n` depends on the
> character following it, and, in general, PEGs support arbitrary
> lookahead.  For error reporting, it would be useful to know that it
> was expecting a digit, an operator, or a right parenthesis instead
> of EOF.  Similarly, if we change the string to
>
>     1-xo@-(y-2)
> 
> it’s useful to know that parsing failed when it encountered `@`,
> expecting a letter (in `v`), an operator (in `s`), or end of input.
>
> However, this plays no role in what follows.

Numbering the input characters:

    1-xo-(y-2)
    0123456789

we have the following memo table of ending positions, assuming we only
memoize nonterminals and (perhaps unrealistically) terminals:

    0 { s {10}
        t {1}
        v {fail}
        n {1}    }
    1 { op {2}
        + {fail}
        - {2}    }
    2 { t {4}
        v {4} }
    4 { op {5}
        + {fail}
        - {6}    }
    5 { t {10}
        v {fail}
        n {fail}
        ( {6}    }
    6 { s {9}
        t {7}
        v {7} }
    7 { op {8}
        + {fail}
        - {8}    }
    8 { t {9}
        v {fail}
        n {9}    }
    9 { op {fail}
        + {fail}
        - {fail}
        ) {10}   }
    10 { op {fail}
         + {fail}
         - {fail}  }

That’s 32 entries; for strings under 64KiB we can plausibly store each
offset in 16 bits, so each entry (containing two offsets and a
nonterminal identifier) can plausibly occupy 64 bits, and an entire
hash table containing this data could fit in 256 bytes.  Not that
inspiring for a 10-character string, in a sense, particularly one
representing an AST with 7 nodes, three subtractions and four
leafnodes.  However, as described below, it turns out this memory
overhead can be mostly eliminated.

An interesting thing here is that if we repeat the parse of the body
of `t` starting at 5:

    <v: [a-z]+> | <n: [0-9]+> | "(" <s> ")"

we don’t need to descend into any of its callees because they’re all
in the memo table.  So we could iterate over the child nodes of `t` in
constant time per child (i.e., time bounded by the size of the
definition of `t`) using just the memo table and the definition of
`t`.

Generally the information about which type of node the parse tree
child is (is it a `v` or an `n` or what?) gives us the semantic
information we need for whatever kind of processing we need.  In cases
where it doesn’t — for example, if we were to define the grammar as

    <s: <t: [a-z]+ | [0-9]+ | "(" <s> ")"> ("+" <t> | "-" <t>)*>

so that all the children of `s` are either terminals or `t`
nodes — it’s straightforward to insert additional nonterminals to add
the desired semantic information, as I did above.

In a sense, this imposes a sort of “virtual DOM” on top of the input
text.  Initially we parse the text to populate the memo table;
thenceforth, by replaying parts of the parse selectively, we can
iterate over the parse tree with a tree node iterator supporting these
operations in time that is constant for a given grammar (because in
the worst case they take time proportional to the definition of the
production):

- `getNextSibling(p: Node) -> Optional[Node]`: returns the node following
  `p` nested under the same parent, or nil if `p` is its parent’s last
  child; this is analogous to `.nextSibling` in the HTML DOM.
- `getFirstChild(p: Node) -> Optional[Node]`: returns the first child
  node of `p`, or nil if `p` has no children; this is analogous to
  `.firstChild` in the HTML DOM.
- `getNonterminal(p: Node) -> Optional[Nonterminal]`: returns the
  nonterminal that parsed `p`, or nil if it does not correspond to a
  nonterminal; this is analogous to `.tagName` in the HTML DOM.
- `getSpan(p: Node) -> tuple[Textpos, Textpos]`: returns the start and
  end positions of the text corresponding to `p`.

The internal representation of the `Node` is a pair of pointers, one
into the input text and one into the parent production.  This
combination of constant-space representation with constant-time
iteration is enabled by entering every backtrackable alternative or
iteration separately into the memo table, so that backtracking is
never necessary during the replay; we simply iterate over the
possibilities, looking each one up in the memo table until we get to
one that didn't fail.

Given these operations, it’s straightforward to write a function like
this:

    def firstChildTagged(p: Node, n: Nonterminal) -> Optional[Node]:
        q = getFirstChild(p)
        while q is not None:
            if getNonterminal(q) is n:
                return q
            q = getNextSibling(q)

        return None

This function will not necessarily run in constant time, because a
node can have an arbitrarily large number of children, but I think it
will be rare for it to be too slow.

Yedidia and Chong 02021 (“Fast Incremental PEG Parsing”,
doi:10.1145/3486608.3486900) points out that, if you memoize long runs
of repetitions in Kleene closure, you can reliably get fast parsing
even if you don’t memoize short and simple productions, because you
can bound the work required by an unmemoized parse; this allows you to
only memoize productions that required more than some threshold amount
of work, which can eliminate the vast majority of entries from the
memo table.  This technique should make it possible to support the
interface described above with minimal memory overhead beyond that of
the raw input text, because only the top few levels of the parse tree
are materialized in the memo table, with nodes further from the root
materialized on demand in bounded time.
