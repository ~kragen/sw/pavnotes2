This is not really a new idea, being implicit
in Prolog DCGs, but the problem of parsing can
be viewed as a constraint satisfaction problem
where you’re looking for a sequence of
production steps that could produce a given
string of output.  This view of the problem
offers some interesting alternatives:

- If you only constrain the string length but
  not its contents, you can generate strings of
  a given length, which are potentially useful
  for testing.  (This came out of some thinking
  about shape grammars; see file
  `shape-grammars.md`.)
- This also admits formulations where you know
  some substrings of the string to parse but
  not the whole thing.  This is potentially
  useful for error recovery as well as artistic
  purposes.
- Solving the constraint satisfaction problem
  bottom up rather than top down also gives you
  a way to generate short random strings, which
  is useful for getting feedback on a grammar
  design.
- If you measure the probabilities of each
  nondeterministic choice in the grammar on a
  training corpus, you can use those
  probabilities to either generate sentences
  similar to the training corpus or to guess
  about corrupted text for purposes of error
  recovery.
