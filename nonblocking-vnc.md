I’m working on a simple VNC server in C, which I think I should be
able to write in a few hundred lines of code; my Golang version is 278
lines of code.  The idea is to make it easy to write a GUI C program
which you can connect to over the VNC protocol with zero or more
clients and which can do computation in the background when it isn’t
having to respond to VNC protocol messages.  I don’t want to require
multithreading, so the VNC library needs to provide socket file
descriptors back to the C program to watch for events on, while the Go
program can simply rely on goroutines.

I want to do this as part of Yeso.  Right now one problem is that Yeso
doesn’t have a way to notify the client program about changes in its
file descriptor list; the VNC server needs to be able to inform it of
new clients.  And some of the time the VNC library will need to tell
the client program about file descriptors that need watching when they
become writable, not just readable.

Right now the `yw_fds` function only provides *readable* file
descriptors to the client program.  But VNC may involve writing a
large amount of data to a socket, potentially larger than the socket’s
send buffer.  In this situation, unless we ensure our writes are
nonblocking, a slow client could potentially hang the server and
prevent it from doing background computation or servicing other
concurrent clients.  Such nonblocking writes require some way to get
notified when some data is drained from the buffer, allowing us to
send more.

(Actually, looking at file `yeso-vnc.md`, maybe I can get by without
changing Yeso’s `yw_fds` interface, because maybe regular Yeso
programs won’t be using this interface at all.)

To ensure the writes are nonblocking without internally requiring
unlimited buffer space, we need some way to “save our place” and come
back to the writing task later.  Without multithreading, this requires
putting resumption logic at each possible resumption point, which
suggests minimizing the number of possible resumption points.

My minimal Golang VNC server only sends ProtocolVersion,
security-type, ServerInit, and framebuffer update messages.  Of these
four, the first three are sent at most once, and they are bounded in
size; the fourth can be sent an unlimited number of times and contain
an unlimited amount of data each time.  So, if we preallocate enough
buffer space for the first three and the header of the fourth, we can
keep the resumption points down to one: framebuffer update
resumptions.

A framebuffer update can contain an arbitrary number of rectangles,
each of which covers some arbitrary area of the framebuffer.  But,
initially, we can keep the framebuffer update simple, sending only a
single rectangle covering the whole screen.  Then our resumption state
consists only of (1) whether we’re in the middle of a framebuffer
update and (2) at what pixel position.  I guess we might need (3) the
byte offset in that pixel, since the sockets layer doesn’t know we’re
sending pixels.

It might also be important to avoid overwriting the framebuffer we’re
reading from if we want to avoid screen update tearing.  However, this
may require allocating as much as one framebuffer per client if we are
to guarantee progress.  Consider the case where you have N
framebuffers locked because clients are partway through writing them,
and another client requests a framebuffer update.  If you add it as
another reader to one of the N framebuffers, then a constant stream of
such clients will keep that framebuffer from ever being freed up, and
if you add it to a currently unlocked framebuffer, now you have N+1,
and if the app wants to start drawing it now needs N+2 framebuffers.
And, if we have to allocate a framebuffer of memory per client, maybe
we should just allocate a big enough network output buffer to hold an
entire framebuffer’s worth of pixels, simplifying the network sending
code.

(Allocating two framebuffers per client instead of one would make it
possible to send partial screen updates.)

Nonblocking reads are a slightly different problem: we may have not
yet read enough data to finish reading a whole message.  Indeed, ahead
of time we may not know how big that is; the SetEncodings and
ClientCutText client-to-server messages can in theory be arbitrarily
large.  So in theory we need to be able to resume reading those after
consuming data, as well.  But for things like SetPixelFormat and
KeyEvent, it’s fine to just restart the reading from the beginning if
there’s incomplete data in our input buffer.  And probably in practice
that’s true for the others too if the input buffer isn’t tiny compared
to the framebuffer, because copying megabytes of text is unusual
enough that handling it well isn’t exactly a high priority.  (The VNC
protocol already handles it badly by sending it to every VNC server
you’re connected to!)

Anyway, this means that maybe we don’t have to save resumption states
for reads, but restarting a parse probably is a thing we need to be
able to do.

Maybe the right set of operations for such nonblocking reads is:

- commit(buf): discards all characters before the read pointer;
- read(buf): char option: returns a character from the buffer, if any,
  and advances the read pointer, or, if no character is available,
  returns None and “aborts the transaction”, returning the read
  pointer to the position of the last commit, unreading all the
  characters; and
- fill(buf, socket): reads as much data from the socket into buf as
  will fit.

The idea is that when you start trying to read a thing, you may run
out of data, which aborts the read and in which case you’ll return
failure.  (Maybe there should be an explicit abort() instead, which
would make this interface useful for some other kinds of parsing too.)
But if you get enough data to read the thing, you’ll call commit().
And when you get signaled that the socket is readable, you call fill()
and then retry the reading operation.  If you abort on a full input
buffer, either you need to grow the buffer or signal some kind of
higher-level error, maybe closing the connection or something.

Maybe read() should take a length parameter, for performance reasons.
But generally the server-to-client messages are tiny, and the inlined
implementation of the one-byte version is probably something like
this anyway, similar to getchar():

    cmp rdi, rsi       # end of data so far received?
    je short 1f        # jump to failure case if so
    mov al, [rdi]      # otherwise, load byte and
    inc rdi            # increment read pointer

That seems like a pretty acceptable performance overhead.
