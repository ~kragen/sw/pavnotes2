Alabaster is only slightly soluble in water normally; different
sources give solubilities of 0.2 to 3 grams per 100 mℓ.  This is
inconveniently low for electrolysis.  How would you make a stronger
alabaster electrolyte?

Because its dissolution is exothermic it has retrograde solubility,
decreasing with increasing temperature, so you can't take the normal
route of heating up the solvent to dissolve more of it.  However, it
looks like the solubility actually reaches a maximum at 50°, with the
solubility at 0° and 100° being slightly lower and about equal.

Shukla and Kumar published a paper in 02008 (10.1021/je800465f) that
gave its solubility in pure water at 35° as 0.0148 mol/kg (which I
calculate as 2.01 g/kg on an anhydrous basis, 0.201 wt%) at pH 7, but
increasing its solubility to a maximum of 0.0523 mol/kg (7.12 g/kg)
with 2.8165 mol/kg of NaCl, which I calculate as 165 g/kg.  This
improves further to 0.0561 mol/kg of alabaster at pH 2.5 and
2.6327 mol/kg of NaCl.

The CRC handbook says it's soluble in "ammonium salts, sodium
thiosulfate & glycerine".  Wikipedia says it's "slightly soluble" in
glycerine.

It makes some sense that ammonia might be able to complex the calcium,
as it does with copper, cobalt, chromium, nickel, zinc, silver, gold,
and platinum-group metals, but I haven't found any claims that it
actually does do this with calcium or any other alkaline earth metal.

[An open-access paper last year by Taherdangkoo et al.][0] summarizes
published data on alabaster's solubility in [a really awkward
four-megabyte Excel spreadsheet][1].  The 9-page paper mentions
increasing solubility with increased pressure (up to 1000 bar) but
doesn't actually say anything else about how to get higher solubility;
for that you have to look through the tables.

Their "dataset 9" from Bock (1961) confirms the Shukla and Kumar
result mentioned above, except one order of magnitude lower, with
0.773 g/kg of alabaster dissolved in 25° water with 2.58 mol/kg NaCl.

Their "dataset 10" from Dickson (1963) was the one that tried high
pressures.  At 6 bar and 105° Dickson was only able to dissolve
0.0687 wt% of anhydrite in water, but by boosting it to 1000 bar he
got to 0.188 wt%.  Note that this is still lower than the usual STP
solubility; not sure what's up with that.

Their "dataset 13" from Ostroff and Metler (1966) reaches very similar
results, but an order of magnitude off in the opposite direction
(maximum solubility of 7.67 g/100 g at 28° with 2.86 molal NaCl).  (I
suspect Taherdangkoo et al. have just been careless with their units.)
Ostroff and Metler were also trying magnesium chloride, and were able
to dissolve 8.22 g/100 g (probably actually 0.822 g/100 g) at 50° with
2.356 molal NaCl and 0.202 molal MgCl₂; plausibly they could have
dissolved much more if they'd kept going to higher MgCl₂
concentrations.  A potential advantage of MgCl₂ is that you can *get*
higher concentrations than with NaCl because its solubility is so much
higher, and of course magnesium sulfate is very water-soluble.  NaCl
stopped being helpful above about 2.5 molal, though.

Their "dataset 39" from Tian et al. (2012) gets even slightly higher
solubilities by using other salts: up to 0.063 mol/kg with sal
ammoniac (at 1.5938 mol/kg sal ammoniac and 70°, though even at 35°
they dissolved 0.0624 mol/kg with that amount of sal ammoniac), and
0.0685 with the nitrate.  (This is not the high solubility "in ammonia
salts" the CRC handbook had led me to hope for.)  A third data series
is again labeled as sal ammoniac, but with radically different
numbers; I suspect this is mislabeled and should be a mixture of the
chloride and the sulfate.

Their "dataset 41" from Shukla et al. (2018) gets higher solubilities
still, dissolving up to 0.1176 mol/kg of alabaster at 30°, using
2.6028 mol/kg NaCl and 10% ethylammonium lactate.

[0]: https://mdpi-res.com/d_attachment/data/data-07-00140/article_deploy/data-07-00140.pdf?version=1665916239 "Taherdangkoo, Tian, Sadighi, Meng, Yang and Butscher, 02022, 10.3390/data7100140"
[1]: https://data.mendeley.com/datasets/phmrfngcxn/1

What about the glycerine thing?  I can't find any information.

I have this vague idea that what's really going on here is that ionic
calcium doesn't really like to dissolve in water, sort of like
iron(III) (ferric) ions, and you need to persuade it really hard,
which is why fluorspar and calcium citrate are insoluble.  This makes
me wonder if you could maybe use EDTA or some similar ligand to make
it more soluble.
