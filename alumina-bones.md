Bones and teeth are resilient to impacts despite being made of brittle
hydroxyapatite because the hydroxyapatite crystals are thinner than
the critical scale for flaw-insensitivity, and they’re bound together
by a protein matrix which is hard enough to transfer loads effectively
but not so hard that it propagates cracks between them.  The teeth on
the radulae of *Patella vulgata*, the common limpet, are even stronger
because they use goethite instead of hydroxyapatite.

Sapphire is the best material, so obviously the thing to try is sapphire
nanocrystals.  But you need platy or acicular morphology for the trick
to work; in a concrete made of near-unity-aspect-ratio particles, the
cement is necessarily the weak link.  And acicular morphology (as used
in bones and limpet teeth, as well as, for example, glass-fiber
reinforced plastic, carbon-fiber reinforced plastic,
chopped-basalt-fiber-reinforced cement, fiberglass-reinforced plaster,
horsehair-reinforced plaster, etc.) is far preferable, supposing crude
bulk fabrication techniques, because getting platy crystals to pack
together is difficult.

Mullite (3Al₂O₃·2SiO₂ or 2Al₂O₃·SiO₂) is conveniently acicular, but it
isn’t sapphire.  But [Iga and Murase reported in 01995][0]:

> Aluminum ammonium sulfate (AA) solution (0.1 mol/l) and ammonium
> hydrogen carbonate (AHC) solution (1.2 mol/l) were allowed to react
> at room temperature [35° to 60°]. The precipitates formed were kept
> with the mother liquor in a closed vessel and aged at 100 °C [for 24
> hours]. Acicular-shaped crystalline particles of Al(NH4)CO3(OH)2,
> (AACH), were obtained after the elongated aging period. Acicular
> α-alumina particles were obtained through the thermal decomposition
> at 1300 °C out of the AACH particles which were previously treated
> with a small amount of tetraethoxysilane (about 1.6 wt. % of SiO2).

[0]: https://www.cambridge.org/core/journals/journal-of-materials-research/article/abs/preparation-of-acicular-particles-of-alumina/83447F1124E3596B075496C8837C6B2F (Journal of Materials Research, Volume 10, Issue 7, July 1995, pp. 1579 - 1581, DOI 10.1557/JMR.1995.1579 )

That’s ammonium alum and salt of hartshorn, and they got sapphire
whiskers out of it, specifically for the purpose of making composite
ceramics actually.  The initial aluminum ammonium carbonate
precipitate was amorphous (promising for use on its own as a binder
for ceramics) and the TEM micrographs seem to show aspect ratios on
the order of 10 with particle sizes on the order of 1μm and jagged
ends, looking to be quite the inhalation hazard for humans.  Aging in
sulfuric acid made larger crystals, ammonia made smaller ones.  The
tetraethoxysilane was necessary to preserve the acicular shape through
pyrolysis to sapphire.

Discouragingly, [Iga and Murase’s work seems to have gone almost
unnoticed][1]; nobody seems to have reported even a simple
replication, despite the obviously immense virtue of resilient
sapphire-whisker composites, if they can be synthesized.

[1]: https://scholar.google.com/scholar?cites=3202356705630527529&as_sdt=2005&sciodt=0,5&hl=es

