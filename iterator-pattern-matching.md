Pattern matching on sum types doesn’t require bounds checking, so
range iteration constructs can avoid bounds checking too, without
requiring clever compilers or taking advantage of undefined behavior.

Looping over an array in C and pseudo-assembly
----------------------------------------------

Consider this nearly trivial C code:

    int t = 0;
    for (int i = 0; i != n; i++) if (a[i].open) t += a[i].b;

If you were to compile this straightforwardly to an assembly-like
register code, it might look something like this:

            r0 <- 0                 ; initialize t to immediate 0
            r1 <- 0                 ; initialize i to 0
        1:  goto 1f if r1 == r2     ; exit loop if i == n
            r3 <- r1 << 5           ; compute i << 5 to index into a
            r3 <- *(u16*)(r4+r3+6)  ; load .open contents from memory
            goto 2f if r3 == 0      ; skip body of if statement if not open
            r3 <- r1 << 5
            r3 <- *(s32*)(r4+r3+12) ; load .b contents from memory
            r0 <- r0 + r3
        2:  r1 <- r1 + 1            ; i++
            goto 1b                 ; back to top of loop
        1:  ;; end of loop, code continues here

Hopefully the pseudo-assembly here is sufficiently clear.  A crucial
thing here is that there’s no bounds check on the array indexing; it’s
entirely possible that `a` is smaller than `n`, in which case the
program will probably not work properly.  To solve this problem you
can insert runtime bounds checks:

            r0 <- 0
            r1 <- 0
        1:  goto 1f if r1 == r2
            goto 3f if r1 u>= r5    ; unsigned compare to length in r5
            r3 <- r1 << 5
            r3 <- *(u16*)(r4+r3+6)
            goto 2f if r3 == 0
            goto 3f if r1 u>= r5    ; unsigned compare to length in r5
            r3 <- r1 << 5
            r3 <- *(s32*)(r4+r3+12)
            r0 <- r0 + r3
        2:  r1 <- r1 + 1
            goto 1b
        1:

This probably makes the loop run about 15% more instructions per
iteration, though those bounds-checking instructions might get
overlapped.  A slightly smarter compiler can eliminate the common
`r4 + r1 << 5` subexpression and strength-reduce it, as well as
eliminating the duplicate check, which results in something like this:

            r0 <- 0
            r1 <- 0
            r6 <- r4                ; initial pointer
        1:  goto 1f if r1 == r2
            goto 3f if r1 u>= r5
            r3 <- *(u16*)(r6+6)     ; index off pointer
            goto 2f if r3 == 0
            r3 <- *(s32*)(r6+12)    ; index off pointer
            r0 <- r0 + r3
        2:  r1 <- r1 + 1
            r6 <- r6 + 32           ; increment pointer
            goto 1b
        1:

Now our bounds-check instruction is probably closer to 20% extra work
in the inner loop, depending on how often the branch within the loop
is taken.  And we’re just using simple register+offset addressing
instead of the less-frequently-supported base+index+offset.  `r1` is
now only being used to terminate the loop and to possibly jump to the
bounds-checking error handler.

A compiler that manages to hoist the bounds-check instruction out of
the loop (which causes the failure to time-travel to before the loop)
and also rewrite the termination condition in terms of the iteration
pointer can generate something like this:

            r0 <- 0
            goto 3f if r2 u> r5     ; verify that [0, n) is within array bounds
            r1 <- r4
            r3 <- r2 << 5           ; compute array size in bytes
            r1 <- r1 + r3           ; calculate end pointer
            r6 <- r4
        1:  goto 1f if r6 == r1     ; compare iterator in r6 against end in r1
            r3 <- *(u16*)(r6+6)
            goto 2f if r3 == 0
            r3 <- *(s32*)(r6+12)
            r0 <- r0 + r3
        2:  r6 <- r6 + 32
            goto 1b
        1:

And then a standard basic block reordering transformation, not
dependent on any of the foregoing and totally implementable just as a
standard way to compile `for` and `while` loops, allows us to move the
unconditional jump out of the loop:

            r0 <- 0
            goto 3f if r2 u> r5
            r1 <- r4
            r3 <- r2 << 5
            r1 <- r1 + r3
            r6 <- r4
            goto 1f                   ; unconditional jump to loop condition
        4:  r3 <- *(u16*)(r6+6)
            goto 2f if r3 == 0
            r3 <- *(s32*)(r6+12)
            r0 <- r0 + r3
        2:  r6 <- r6 + 32
        1:  goto 4b if r6 != r1       ; loop test is now reversed in sense
        1:

So now our loop is 6 instructions instead of 11 and probably as good
as you could write it by hand.  But it took a fair bit of compiler
complexity to get there, plus undefined behavior for the array bounds
(violating) read.

The same thing in OCaml
-----------------------

Consider instead this OCaml code:

    type coso = Rec of int * int * bool * int * int64 * int64

    let rec myloop (t : int) : coso list -> int = function
      | [] -> t
      | Rec(_, _, open_, b, _, _)::cs -> myloop (if open_ then t + b else t) cs

This does the same thing, but iterating over a linked list rather than
an array.  For amd64 `ocamlopt` compiles it as follows (Intel syntax):

    0000000000014c60 <camlExampleloop__myloop_83>:
       14c60:        sub rsp, 8                 ; allocate stack (unused)
       14c64:    5:  cmp r15, [r14]
       14c67:        jbe 1f                     ; invoke GC if necessary
       14c69:    3:  cmp rbx, 1                 ; Is the list argument nil?
       14c6d:        je 2f                      ; return if so
       14c6f:        mov rdi, [rbx]             ; load list item (Rec pointer)
       14c72:        mov rsi, [rdi+0x10]        ; load bool open_ at offset 0x10
       14c76:        cmp rsi, 1                 ; is it false (1)?  If so,
       14c7a:        je 4f                      ; go to next list item
       14c7c:        mov rdi, [rdi+0x18]        ; If not, load field b (0x18)
       14c80:        lea rax, [rax+rdi*1-1]     ; Add tagged int to rax (t)
       14c85:    4:  mov rbx, [rbx+0x8]         ; Go to next list item
       14c89:        jmp 5b                     ; tail recursion
       14c8b:        nop
       14c8c:    2:  add rsp, 8                 ; deallocate stack space
       14c90:        ret                        ; return t (rax)
       14c91:    1:  call 32658 <caml_call_gc>  ; (not really needed here)
       14c96:        jmp 3b                     ; 14c69, continue after GC

This inner loop is 12 instructions rather than 6 or even 11.  The two
conditionals are each a cmp/je instruction pair rather than a
goto-if-equal instruction; they get fused into goto-if-equal macro-ops
in the CPU’s instruction-decoding frontend.  There’s an extra `mov
rdi, [rbx]` in the loop because OCaml puts pointers to structs in its
list rather than the actual structs.  Let’s compare this inner loop
side by side with our 11-pseudo-instruction version above:

    5:  cmp r15, [r14]
        jbe 1f
    3:  cmp rbx, 1              1:  goto 1f if r1 == r2
        je 2f
        mov rdi, [rbx]              goto 3f if r1 u>= r5
                                    r3 <- r1 << 5
        mov rsi, [rdi+0x10]         r3 <- *(u16*)(r4+r3+6)
        cmp rsi, 1                  goto 2f if r3 == 0
        je 4f
        mov rdi, [rdi+0x18]         goto 3f if r1 u>= r5
                                    r3 <- r1 << 5
                                    r3 <- *(s32*)(r4+r3+12)
        lea rax, [rax+rdi*1-1]      r0 <- r0 + r3
    4:  mov rbx, [rbx+0x8]      2:  r1 <- r1 + 1
        jmp 5b                      goto 1b

The first thing that’s obvious is that `ocamlopt` is pretty stupid
indeed, although it does do tail-call elimination.  It hasn’t rotated
the loop to avoid the wasteful unconditional jump back to the top.
It’s allocating an 8-byte stack frame that it never uses.  It’s
inserted a garbage collection check inside our inner loop even though
it’s unnecessary because there’s no allocation inside the loop.  Fully
6 of the 17 non-nop instructions in this compiled subroutine are
useless.  (Although they’re not on exhibit here, it does in fact do
things like constant folding and commutativity and associativity of
integer math, though; it isn’t a completely nonoptimizing compiler.)

But not only is OCaml doing work we aren’t, we’re also doing work
OCaml isn’t.  We can see that the compiled OCaml doesn’t have anything
corresponding to our bounds checking; it only has the loop termination
test.  But OCaml is a safe language, unlike C; it guarantees no memory
errors.  OCaml also isn’t having to do bit shifts.

By virtue of proving at compile time that our list argument is of list
type, it can depend on the fact that it’s either nil (represented
evidently as 1) or a pointer to two fields.  And by virtue of proving
at compile time that the list items are of type `coso`, it can depend
on them having the proper number of fields.  So the only type or
bounds tests made at run time are those that direct the control flow.
We also introduce local variable names for the fields we’re going to
use in the pattern match, so there’s no need to eliminate a common
subexpression there either, though CSE is actually a thing `ocamlopt`
can do.

This means that the OCaml code is actually closer to the
assembly-language code generated than the C code is, enabling the
compiler to get good results with less work.

Looping over an array with ranges as views
------------------------------------------

In C, though not in the OCaml implementation, different variable types
can have different sizes, another way OCaml is closer to assembly than
C is.  C++11 added range-based for loops, which let us write our
original loop in a function like this one:

    int myloop(cosos a)
    {
      int t = 0;
      for (auto& c : a) if (c.open) t += c.b;
      return t;
    }

G++ compiled this function to the following humming code:

    0000000000000000 <myloop(cosos)>:
       0:        xor eax, eax                ; initialize t (return value) to 0
       2:     3: cmp rdi, rsi                ; compare iterator pointer to limit
       5:        je 1f                       ; exit loop if equal
       7:        cmp BYTE PTR [rdi+0x8], 0   ; check bool field c.open
       b:        je 2f                       ; if false, skip addition
       d:        add eax, [rdi+0xc]          ; load 32-bit int c.b and add to t
      10:     2: add rdi, 0x20               ; advance pointer to next struct
      14:        jmp 3b                      ; unconditional jump to loop top
      16:     1: ret                         ; return t

(Surprisingly, it didn’t do the loop rotation transformation, which
would be important in a case like this.  I supplied `-Os`, but it
doesn’t do it with `-O` either.)

For this to work, the class `cosos` was defined as follows, defining
the `begin()` and `end()` methods used by range-based for loops:

    struct cosos {
      coso *begin_, *end_;
      coso *begin() { return begin_; }
      coso *end() { return end_; }
    };

Inlining these method definitions, passing the entire object in the
`rdi` and `rsi` registers, and observing that the `begin_` field was
not used after initializing the iterator and could therefore be
overwritten with the iterator.

What’s interesting is that the structure of the loop eliminates the
question of index validity.  If the range was valid (which C++ doesn’t
guarantee) and we’re inside the loop, `c` references a valid object;
outside the loop, it’s out of scope, just like the `open_` and `b`
variables in our OCaml code, so code that tries to reference it will
not compile.  Since the iterable object is itself providing the
indices, it is unnecessary to verify them inside the loop, or even to
reify them.

However, although a simple `for` loop like this can express many
algorithms on arrays and other sequences, it is not adequate to
express all of them.  It would be nice to be able to express arbitrary
array computations efficiently, not just the simplest ones.

We could imagine Wadler-style “views” of range objects which allow us
to treat them semantically like OCaml’s lists: either they are Nil or
they are Cons(first, rest), where “rest” is a range of the same type
representing the rest of the sequence being iterated over.  For “input
ranges”, Alexandrescu’s version of Stepanov and Lee’s “input
iterators”, you need a linear type system in which the
pattern-matching operation invalidates the original iterator, but the
other kinds of ranges can be handled using conventional type systems.

Such range objects are in general adequately expressive.

To get efficient code using range objects, you probably do need to
specialize your code for the range type at compile time.  If you’re
doing virtual method dispatch and multiple memory accesses to figure
out how to advance your pointer and whether it’s hit the end, code
like this example loop is just going to unavoidably be a lot slower.
I haven’t actually managed to convince G++ to do such a thing, but
without any saving and restoring of registers, a G++ virtual method
call looks like this:

    mov rax, [rdi]    ; load vtable pointer from object rdi points to
    call [rax+8]      ; call second method in vtable with that object as self

A termination-test method that could be invoked thus might look like
this:

    mov rax, [rdi+0x10]  ; load end pointer
    cmp [rdi+0x8], rax   ; compare to current pointer
    setne al             ; convert result to legible 0 or 1 in rax
    ret

Then the caller would have to do something like this:

    test rax, rax
    je 1f

So just the loop’s termination test would involve 8 instructions, more
than the entire loop does with specialization.  And that doesn't even
increment the pointer!

Minimal-overhead fully dynamic ranges
-------------------------------------

If you couldn’t use specialization, so you were going to pay some kind
of dynamic dispatch overhead per item to iterate over a range, you’d
like the operations of termination testing, iterator advancing, and
item-reference obtaining to share a single dispatch, because that’s
the common case.  It would be reasonable for iterating over a range
more than once to require an extra bookmark-saving operation.

The minimum amount of overhead to invoke a dynamically determined
subroutine would be something like this:

    call r14

Just an indirect call to a get-next subroutine through a register.
For the get-next subroutine itself to be efficient, it probably needs
to not have to reload its data from memory, so we probably want that
data to be in registers.  In fact, since we’re probably calling it
repeatedly in a loop where we’re doing other things, we probably want
its data to be in *call-preserved* registers, like RBX, RBP, and
R12–R15.  Otherwise we have to reload that data ourselves before
calling it, unless we’ve managed to avoid calling any standard
subroutines on a given iteration.

So let’s say we allocate R12 and R13 to the iterator’s private state
data.  If it wants to have more private state data, it can store it in
memory, but we’re giving it two registers.  And let’s say it returns a
pointer to the next iteratee in R9, or null if there is no next
iteratee.  Then I think our get-next subroutine for our
32-byte-per-item array looks like this:

        cmp r12, r13          ; compare current pointer to end pointer
        je 1f                 ; if equal, take end-of-array branch
        mov r9, r12           ; return current pointer, but
        add r12, 32           ; increment it for the next time, and
        ret                   ; return
    1:  xor r9, r9            ; in end-of-array case, return 0
        ret

Then the callsite looks something like this:

    1: call r14               ; invoke get-next subroutine
       test r9, r9            ; did it return a null pointer?
       je 1f                  ; if so, exit loop
       cmp byte ptr [r9+8], 0 ; a[i].open?
       je 1b
       add eax, [r9+12]       ; t += a[i].b
    2: jmp 1b                 ; jump back to top of loop
    1: pop r14                ; restore callee-saved registers used by iterator
       pop r13
       pop r12
       ret

The setup for it of course needs to save R12, R13, and R14 and load
them up with iterator state.

The C++ specialized version above had 7 instructions in its inner
loop, of which it would execute 6 or 7 per iteration.  This version
also has 7 instructions in its inner loop, of which it executes 5 or 7
per iteration, but one of them is a call which usually runs another 5
instructions (or occasionally 4, but with a mispredicted branch), so
in total we have 10 or 12 instructions, about a 1.4 to 2.0 factor of
increased work.  As predicted, this is unavoidably a lot slower, but
not ridiculously so.

It would be better to write the inner loop in this rotated form,
saving an inner-loop instruction:

       jmp 1f                  ; jump into the middle of the loop
    3: cmp byte ptr [r9+8], 0
       je 1f
       add eax, [r9+12]
    1: call r14
       test r9, r9
       jne 3b

But that is an unfair advantage over the C++ output above.

A fairer advantage, on amd64 and ARM32 anyway, would be to add to the
contract of the get-next subroutine that it should indicate its
success or failure with the CPU’s zero flag ZF, leaving it set to
indicate the end of iteration or clear to indicate that an item was
provided.  This simplifies get-next to this, which probably has no
significant speed difference:

        cmp r12, r13
        je 1f
        mov r9, r12
        add r12, 32
    1:  ret

And the calling loop to this, with 5 instructions in the loop:

    3: cmp byte ptr [r9+8], 0
       je 1f
       add eax, [r9+12]
    1: call r14
       jne 3b

(This would allow us to iterate over integers rather than pointers,
which might be desirable in some cases.)

By supplying a different function pointer, we can get our structs in
this same loop in some other way.  For example, here’s a get-next
which walks an intrusive linked list of structs:

        test r12, r12        ; is this a null pointer?
        je 1f
        mov r9, r12
        mov r12, [r12 + 32]  ; follow next-link after end of struct
    1:  ret

And here’s one that walks a non-intrusive linked list of structs
that’s structured like OCaml’s:

        cmp r12, 1
        je 1f
        mov r9, [r12]
        mov r12, [r12 + 8]
    1:  ret

On this Ryzen 5 3500U a simple non-dynamic scalar loop summing an
array as follows takes about 530ps per array item:

            .align 8
        2:  add rax, [rdi]
            add rdi, 8
        1:  cmp rdi, rsi
            jne 2b

And using this dynamic iteration mechanism slows it down to about
1400ps.  So it’s slightly under a nanosecond of overhead per
iteration, but that’s still a 2.7× slowdown for a simple enough loop.

Such an iterator object might be stored in a struct containing the two
context variables to put in R12 and R13, the get-next subroutine
pointer, a save-bookmark subroutine pointer, and a destructor.  So
preparing to iterate over one might look like this:

        push r12
        push r13
        push r14
        mov r12, [rdi]
        mov r13, [rdi + 8]
        mov r14, [rdi + 12]

In cases like the ones we’re considering here, where the two context
variables contain all the relevant data, the save-bookmark and
destructor subroutines might just say

        ret

but in other cases the save-bookmark subroutine might heap-allocate
something, copy data R12 or R13 points to into it, and point R12 or
R13 at that new data, and the destructor might then deallocate it.

This destructor/bookmarksaver approach is not super harmonious with
the desired pattern-matching semantics of the source language, unless
it uses a linear type system.  If we’re willing to instead depend on
garbage collection, we could simply decree that R12 and R13 are
pointers treated by the GC like any others, so we can safely copy them
in the same circumstances we could safely copy any other pointer.
Conceptually then the get-next subroutine in R14 is destructuring the
iterator represented by {R12, R13, R14}, and either returning Nil (in
ZF) or a Cons of the car in R9 and the cdr in {R12, R13, R14}.  Or
maybe just {R12, R13}.

If you want to switch from one iterator to another, for example for a
nested loop or other algorithm that uses multiple iterators, you might
end up having to do something like this:

        mov [rsp + 64], r12
        mov [rsp + 72], r13
        ; Maybe we can assume the get-next function pointer in R14
        ; doesn’t change, so we don’t have to save it.
        mov r12, [rsp + 16]
        mov r13, [rsp + 24]
        mov r14, [rsp + 32]

This context switch is definitely heavier-weight than the typical case
of loading a pointer to an object:

        mov rdi, [rsp + 16]

But I think the difference is likely to be repaid after very few calls
on the iterator.

