Henry Baker’s COMFY compilers implement an interesting compositional
approach to control flow.  I was thinking it might be worthwhile to
implement it directly in an interpreted bytecode, so I explored a few
different ways to do it.  I can’t find a way it would be better than
the traditional bytecode representation with conditional jumps, except
in unusual cases.  It seems like each of the alternatives would
usually be slower and require more space for the bytecode than the
traditional approach.  This probably generalizes to directly
implementing it in hardware, too.

This is the record of that fruitless exploration.

COMFY still seems like a great way to do *compilation* of control
flow, though, and it would be fine for a tree-walking interpreter too.

Introduction to COMFY: go, yes, no, actions, jumps, and tests
-------------------------------------------------------------

Henry Baker’s COMFY-65 and COMFY-80 compiler build up the control-flow
graph of a subroutine algebraically from simpler control-flow graphs.
Each graph has one entry point, which I will call **go**, and two
exits, which I will call **yes** and **no**, one or both of which may
be unused.  (Baker sometimes calls them “win” and “lose” and sometimes
“succeed” and “fail”.)

The atomic graphs are single instructions, categorized into
**actions**, **jumps**, and **tests**.  Actions are ordinary
instructions which do something and then proceed on to the next
instruction (yes), jumps are execution-terminating instructions such
as returning from a subroutine, and tests are conditional branches,
which can either proceed on to the next instruction (yes) or transfer
control elsewhere (no).

Compilation normally proceeds backwards from the end of memory, so
normally the yes and no addresses are known ahead of time, and the go
address is returned by the compilation.  So, whenever COMFY compiles
one of these atomic units, it does it with some memory address for no
and another for yes.  If the place that it’s compiling the atomic unit
is not immediately before its yes, it inserts an unconditional branch
instruction to branch to the yes.

COMFY’s compositional operators: not, if, and while
---------------------------------------------------

A control-flow graph in COMFY is built up *algebraically* from atomic
actions, jumps, and tests, using the primitive compositional operators
not, if, and while.  Each of these composes one or more graphs with
one entry, go, and two exits, yes and no, into a new graph with one
entry and two exits.

When t = **not** q, q.go = t.go, q.yes = t.no, and q.no = t.yes.
That is:

    not q:
        go  = q.go
        yes = q.no
        no  = q.yes

When t = **if** q c a, c and a have the same exits as t, but their
entries are wired up to the exits of q, and q’s entry is the entry to
the whole composed graph.  In this way the control-flow decision made
by q determines whether c or a runs.  That is:

    if q c a:
        go    = q.go
        q.yes = c.go
        q.no  = a.go
        yes   = c.yes = 
                a.yes
        no    = c.no  = 
                a.no

Given an action **nop** that simply succeeds, ordinary statement
sequencing **seq** x y can be rewritten as if x y nop, which doubles
as logical conjunction, and longer sequences seq x y ...xs can be
rewritten as seq x (seq y ...xs); logical disjunction **alt** x y is
analogously if x (not nop) y or not (seq (not x) (not y)), and alt x y
...xs is alt x (alt y ...xs).

In **while** q b, q’s no exits the loop successfully, but its yes runs
an iteration of the loop body.  Early exits from the loop body cause
the whole loop to fail, and the entry to the whole loop is q’s entry.
That is:

    while q b:
        go    = q.go = 
                b.yes
        q.yes = b.go
        yes   = q.no
        no    = b.no

Compiling this necessarily requires compiling either q or b first,
before the entry point to the other is known; Baker handles this by
first compiling an unconditional jump, then compiling b before it,
then compiling q before b, then finally backpatching
(“forwardpatching”?) the unconditional jump so that it jumps to the
beginning of q.

It is worth noting that in this form Baker’s “while” supports not only
early exits (C’s break) but also Python’s eccentric “while-else”
construct.

An infinite loop **loop** b can be simply rewritten to while nop b.

Baker’s compilers special-case all of the rewritable operators I
described above, I suppose to avoid unnecessary nops.  He also
compiles 5 s as seq s s s s s, and analogously for any value of 5, a
construct that is especially useful on the 6502.

Bytecode implementation: pushno, popno, fail, and b; or try, popno, fail, and b
-------------------------------------------------------------------------------

Suppose we want to implement these control-flow operators not in a
compiler but in a bytecode interpreter, in terms of bytecode
operations that should be executed when entering or exiting one of
these compositional operators, or crossing between their children.

Since ordinary sequencing continually increments the yes address while
leaving the no address unchanged, the interpreter should have a
“no-stack”, a stack of positions at which to continue execution after
a test fails.  Then the code to implement if q c a is as follows:

        pushno 1f
        {{compile q}}
        popno
        {{compile c}}
        b 2f
    1:  {{compile a}}
    2:

Here “pushno” pushes an address onto the no-stack (in this case the
address where the alternate a begins), popno removes it (so that
failure in either c or a will go to whatever the previous failure
handler was), and b unconditionally transfers control to the given
label, in this case 2.

In the simplest case, this is slightly more code than the standard
approach:

        {{compile q}}
        jz 1f
        {{compile c}}
        b 2f
    1:  {{compile a}}
    2:

But in cases where the condition is a logical conjunction of two or
more clauses, the COMFY approach is probably denser because it does
not need to repeat the address of the alternate.

not q is similarly simple:

        pushno 1f
        {{compile q}}
        popno
        fail
    1:

Here **fail** simply unconditionally transfers control to the top of
the no-stack.  So if q fails, it transfers control to label 1, but if
it succeeds, then fail transfers control to whatever the outer no
context is.

Three bytecode instructions and a label is a lot heavier weight than
the usual approach to logical negation.  An alternate approach would
be to compile the condition *somewhere else* using a “try” instruction
which more literally swaps the “yes” and “no” addresses: it pushes the
current next instruction address on the no-stack and transfers control
elsewhere:

        try 1f
        ...

    1:  {{compile q}}
        popno
        fail

This is just as heavyweight but more nonlocal (and thus possibly
requiring more bits to encode the jump offset).

(In most cases you’d instead want to compile a negation by reversing
its “yes” and “no”, or rewriting if (not q) c a to if q a c, or
reversing the sense of a test.)

We can do if q c a in the same style:

        try 1f
        {{compile a}}
    2:  ...

    1:  {{compile q}}
        popno
        {{compile c}}
        b 2b

while q b in the pushno style becomes:

    1:  pushno 2f
        {{compile q}}
        popno
        {{compile b}}
        b 1b
    2:

Or alternately:

        b 1f
    2:  {{compile b}}
    1:  pushno 2b
        {{compile (not q)}}

Again, that’s slightly heavier weight than the usual way to do a while
loop:

    1:  {{compile q}}
        jz 2f
        {{compile b}}
        b 1b
    2:

And, again, it can be rewritten in the “try” style:

    1:  try 2f
        ...

    2:  {{compile q}}
        popno
        {{compile b}}
        b 1b

This doesn’t actually save you any instructions over the traditional
approach, though, because you still have all the same conditional
tests as before, they just don’t have an address in them, and you
additionally need this popno instruction, which may eat up a slot in
your instruction encoding and also adds bulk to your code.  In cases
where you have multiple conditional branches to the same place, this
approach might be more compact.

So, although this approach is definitely workable, I don’t see an
advantage for it over the traditional approach in any of these cases.
In fact, it’s probably a little worse.

Unification with subroutine call: call, ret, b, and popno
---------------------------------------------------------

The no-stack is a stack of addresses to which code can transfer
control.  So, often, is the call stack.  We can unify them.  Our
“fail” becomes “ret”, our “try” becomes “call”, and “popno” becomes an
instruction that pops the call stack to skip the rest of the caller.
Then we no longer need a separate no-stack, all our conditional test
instructions just become conditional returns, and only call and b
instructions contain addresses.

This is conceptually simpler than the try/popno/b instruction approach
described above, it requires only one new instruction (popno) rather
than three (try/popno/fail or pushno/popno/fail), and it would
slightly simplify memory allocation; but it doesn’t seem to have any
particular advantage in speed or code density.

And it’s probably slightly worse at code density than the
pushno/popno/fail approach, which in turn is probably slightly worse
than the traditional jne/jge approach, which is conceptually simpler
and also doesn’t need runtime space for a no-stack.

### Eliminating unconditional branches: call, ret, and popno ###

It *would* be possible to eliminate the b instruction as well by
introducing a spurious address on the stack to be unconditionally
discarded at jump targets; we transform if q c a as follows:

        call 1f                 call 1f       
        {{compile a}}           {{compile a}} 
                                call 2f        ; insert spurious no
    2:  ...                 2:  popno          ; discard spurious no
                       -->                    
    1:  {{compile q}}       1:  {{compile q}} 
        popno                   popno         
        {{compile c}}           {{compile c}} 
        b 2b                    call 2b       
    
and while q b as follows:

                                call 1f      
    1:  call 2f             1:  popno        
        ...                     call 2f      
                       -->      ...          
                            
    2:  {{compile q}}       2:  {{compile q}}
        popno                   popno        
        {{compile b}}           {{compile b}}
        b 1b                    call 1b      

but in this form it seems like a bad tradeoff, bulking up the bytecode
further and adding additional interpretation time.  

### Explicit return address pushing: jal, pushj, popj, and ret ###

If the call instruction stores the return address in a “link register”
instead of pushing it onto a stack, this gets slightly less annoying,
at the expense of adding a single-instruction preamble to every
subroutine.  The control structures above transform as follows; if a q
c:

        call 1f                 jal 1f         ; jump and link
        {{compile a}}           {{compile a}} 
    2:  ...                 2:  ...
                       -->                    
    1:  {{compile q}}       1:  pushj          ; push link register on jumpstack
        popno                   {{compile q}} 
        {{compile c}}           popj           ; pop jumpstack
        b 2b                    {{compile c}}
                                jal 2b       

while q b:

    1:  call 2f             1:  jal 2f
        ...            -->      ...          
                            2:  pushj
    2:  {{compile q}}           {{compile q}}
        popno                   popj         
        {{compile b}}           {{compile b}}
        b 1b                    jal 1b       

But, although this is not quite as bad as the pure call form, it’s
still pretty bad: instead of a simple conditional jump out of the
loop — or even a pushno, a conditional test, and a popno — now we have
a jal, a pushj, a conditional test, and a popj.  And now every
subroutine needs to begin with a pushj.
