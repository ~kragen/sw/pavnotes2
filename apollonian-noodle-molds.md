Dry spaghetti can be close-packed in the pattern of the [hexagonal
close-packing of circles, which occupies 90.7% of space][1], in the
limit of infinitely long spaghetti.  If carefully alternated with the
correct smaller size of spaghetti, in theory it should be able to
reach a higher density, as the smaller circles fill in the space
between the larger circles as in the [Apollonian gasket][0].  In the
limit of more sizes of spaghetti, they fill all of space.

[0]: https://en.wikipedia.org/wiki/Apollonian_gasket
[1]: https://en.wikipedia.org/wiki/Circle_packing#Densest_packing

If you wet a finite number of sizes of dry spaghetti with cement and
stack it up this way, the cement fills the spaces between the circles,
hardening into roughly triangular cement rods running parallel to the
spaghetti.  If the cement is refractory, like the phosphates of
magnesium and aluminum (see file `more-precipitation-ideas.md` among
others), you can burn out the spaghetti and leave only the cement.
This would make a trabecular structure sort of like wood or trabecular
bone.

Such a trabecular structure would be much stronger and stiffer than an
unordered foam of the same material and porosity, and it would also
present much less resistance to fluid flow parallel to its spaghetti
axis.  Low fluid resistance is important in catalyst-support
applications like automotive catalytic converters, for which you’d
need to use a high-temperature cement.  Even with only a single size
of noodle, 90% porosity should be attainable, which is quite high by
the standards of open-cell foams.

In the limit where the spaghetti strands are actually touching,
though, there are no connections among the parallel cement rods; the
line of contact between adjacent pieces of spaghetti cuts the cement
rods off from one another.  If the spaghetti isn’t perfectly rigid,
[these lines of contact expand into Hertzian contact planes][2].  So,
rather than forming a solid object when the spaghetti is burned out,
they would collapse into a pile of rubble.  In practice, this would
probably make a solid object that was rather weak because of
imperfections in the spaghetti packing.

[2]: https://en.wikipedia.org/wiki/Contact_mechanics#Contact_between_two_cylinders_with_parallel_axes

If the strands of spaghetti, rather than being perfectly cylindrical,
had periodic “necks” at which they narrowed down to a smaller diameter
before returning to their original diameter — like strings of sausage
links, streptococcus, or some kinds of algae, or the inverse of rebar
or bamboo, which have ridges instead — those necks would leave space
for strong transverse links between adjacent cement rods, making a
much stronger matrix in exchange for a very small amount of extra
mass.

For rigidity and strength, ideally the grooves forming the “necks”
wouldn’t pass around the spaghetti strands in a circular fashion,
which would make the transverse connections perpendicular to the main
cement rods, because in that case a shear stress trying to move the
cement rods parallel to one another would be a pure bending and
flexion stress on the transverse connections.  If, instead, the
grooves for the transverse connections are at an oblique angle, the
resulting transverse connections will be put into tension or
compression with such shear stresses, making the overall matrix
enormously stronger.

One simple way to make such transverse connections might be to make
one or more helical grooves down each spaghetti strand.  Another would
be to make the spaghetti strands themselves by twisting together two
or three soft cylinders, such as cooked spaghetti, forming a rod with
two or three helical grooves, though approaching 90% porosity with
that structure would likely be very difficult, because the twisted
pair or triple wouldn’t be a very good approximation to a cylinder.

If lower porosity and higher strength is desirable, instead of a
helical groove, you could use a helical ridge, perhaps made by winding
a string around a cylinder.  Instead of isolated cement rods connected
by occasional transverse connections, like trabecular wood, this would
give rise to isolated circular tunnels connected by occasional holes
in the walls.

Making the spaghetti out of some kind of foam rather than a solid
organic material such as semolina would have several benefits: it
would require much less overall material, it would not rupture the
cement matrix by expanding if removed by burnout, and it would be
easier to cut the grooves if cutting were the method chosen.

Using a thermoplastic (such as wax, starch, sugar, or polyethylene)
rather than gluten (which is a thermoset) would also help to avoid
such ruptures, because the thermoplastic would be able to flow along
the passages that are being formed.

For some purposes, it might not be necessary to remove the noodles; if
they are left in, you have a fiber-reinforced composite, and some
possible noodle materials could provide enormously higher toughness
than the pure cement.  For example, mild steel, polyethylene, cotton,
wood fibers, sawdust, coffee grounds, spent yerba mate, cowpies, and
so on.  But that starts to get into a different family of material
structures entirely, reinforced cements.
