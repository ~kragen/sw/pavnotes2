Let’s treat a directory of Markdown files as a Wiki.  We adopt a
convention for turning phrases into filenames, something that
includes, for example, the mapping

    turning phrases into filenames → turning-phrases-into-filenames.md

Emacs scans the text as you’re writing it and highlights any phrases
that correspond to existing Markdown files or existing outlink names.
There’s an “add link” key which turns the most recent such phrase into
a link, or, if the region is active, turns the region into such a
link.  There’s also an “add outlink” key, like my C-M-]
insert-markdown-footnote-link, which adds a link from the previous
word to an external URL (from the clipboard), or if your cursor is
immediately after such a link, extends it to include another word to
the left.  When you save, any new outlinks are added to the Wiki’s
central outlink glossary file.

A “follow link” key (suggested when you add an internal link) opens
the linked Markdown file in Emacs, even if it doesn’t exist.  So
creating a page for “turning phrases into filenames” might involve the
following keystrokes:

    C-SPC                                    ; set-mark-command
    t u r n i n g SPC p h r a s e s SPC      ; self-insert-command
    i n t o SPC f i l e n a m e s            ; self-insert-command
    C-x C-x                                  ; exchange-point-and-mark
    C-return                                 ; markdown-wiki-make-link
    C-tab                                    ; markdown-follow-wiki-link

It turns out there’s a markdown-follow-wiki-link command already which
seems to be designed for something like this, but by default it looks
for markdown-regex-wiki-link, which says:

> This matches typical bracketed [[WikiLinks]] as well as ’aliased’
> wiki links of the form [[PageName|link text]].

And those aren’t in Markdown!  There’s a markdown-follow-link-at-point
which maybe works (I haven’t tried it on a nonexistent file yet) but
isn't by default on a key.

It’s too bad there isn’t a mark-word-backwards or mark-sexp-backwards
command analogous to C-M-SPC, mark-sexp.  That would streamline the
envisioned interaction.

