In [a recent YouTube video about his unfortunately uneconomic
concentrating solar power electricity scheme][0], [Sergiy Yurko][1]
described several interesting and promising improvements on
regenerator-like seasonal thermal stores.  He proposes building an
“artificial dirt mountain” of 10.2 million cubic meters of soil, four
times the volume of the (obviously feasible) Pyramid of Cheops, but
only 120 m tall, meanwhile laying metal pipes through it filled with
heat-transfer oil.

[0]: https://youtu.be/Kdg70S3TFCs "’Solar power doesn’t depend on night, weather, season. It eliminates the main drawback of solar panel’, 18m41s, uploaded 2023-03-24"
[1]: https://www.youtube.com/@sergiyyurko8668

His inspiration is the well-known seasonal thermal store at [Drake
Landing Solar Community][2] in Okotoks, Alberta.  The so-called
“borehole thermal energy storage (BTES)” store is a 35-meter-diameter
field containing 144 vertical 37-meter-deep boreholes with closed
loops of 110-mm-diameter plastic pipe to exchange heat with the soil,
[which conducts heat][3] at 2.0 W/m/K and has a volumetric heat
capacity of 2.3 MJ/m³/K, but also loses significant heat to
groundwater convection, resulting in efficiencies generally below 30%
and always below 54%, because the water table in the area is only
about 3–6 meters below the surface.  The soil can reach 80°, so the
convection can be substantial.

[2]: https://www.dlsc.ca/
[3]: https://www.researchgate.net/publication/292186784_Numerical_Modeling_of_a_Soil-Borehole_Thermal_Energy_Storage_System "Catolico, Ge, and McCartney, doi 10.2136/vzj2015.05.0078, CC BY-NC-ND"

The artificial-mountain approach of course avoids the groundwater
problem.

One of the interesting things Yurko proposes which I haven’t seen
before, but which turns out to come from Drake Landing design, is that
he proposes to keep the thermal gradient of his regenerator radial,
rather than running from one end to the other.  Thus the hottest part
of the regenerator is always in the middle, rather than at the outside
boundary, greatly reducing the heat loss through the outer insulation
of the system into the environment.  (For a cold reservoir used for
cooling, you would want to keep the coldest part in the middle
instead.)

The Catolico, Ge, and McCartney paper I linked above explains:

> The BTES system is comprised of 24 series of six connected U-tube
> pipes that extend from the center to the outer boundary in a radial
> direction (Fig. 2).  During charging, heated fluid is circulated into
> the boreholes at the center of the array first, then progressively
> through boreholes toward the boundary.  In this case [sic], the
> center of the array reaches the greatest temperatures.  During the
> winter discharging months, cold water is injected into the outer
> boundary pipes, is heated by the thermal energy stored in the soil,
> and then is extracted from the center pipes.  The warm water is
> pumped to the homes and heat is transferred to the homes through
> forced-air-fan coil units.  The system is bounded above by an
> insulation layer and hydraulic barrier that prevent heat and fluid
> exchange with the surface (Fig. 2) [sic].

The [thermal diffusivity][4] of the soil from Catolico et al.’s paper
would be 2.0 W/m/K/(2.3 MJ/m³/K), which works out to 8.7e-7 m²/s; I
think that this means that, absent convection, the scale of heat
transfer is about:

- 0.9 mm in 1 second;
- 56 mm in 1 hour;
- 270 mm in 1 day;
- 730 mm in 1 week;
- 1.5 m in 1 month;
- 3.7 m in 6 months;
- 5.2 m in 1 year.

[4]: https://en.wikipedia.org/wiki/Thermal_diffusivity

Pretty much all soil will be in this neighborhood.

The boreholes are about 3 meters apart; Catolico et al. give the
dimensions of the field as 30 meters by 30 meters, while Yurko says
it’s 35 meters.

(I suspect that if you grouted the soil around the outside of the
Drake Landing field to a depth of nearly the depth of the boreholes,
strongly enough to drastically reduce groundwater convection, you
could get higher efficiencies.)

At a smaller scale, I think you could do something similar in three
dimensions instead of two by feeding hot heat-transfer fluid (*fluido
caloportador* in Spanish, a delightful phrase) through a fat pipe into
the center of a spherical regenerator when heating up the regenerator,
and sucking it out through that fat pipe when drawing heat from it.  A
spherical regenerator poses a greater risk that some of the
heat-storage medium won’t get adequate fluid flow; I think that, by
alternating spherical shells of fine-grained material and of
coarse-grained material, this can be avoided.  The heat-transfer fluid
pressure gradient in the coarse-grained material will be minimal
because of the larger pores of that part of the medium, so flow in
that layer will be tangential rather than radial, evening out whatever
inequalities in pressure would have built up in the preceding layer of
fine-grained material.  The outermost shell would be made of
coarse-grained material to enable the cold-fluid pipe to be connected
on only one side.

Yurko also points out that by injecting heat at different layers of
such a radial-gradient heat reservoir (or drawing it off), you can use
various different temperatures.  If you have a reservoir that is at
280° in the center, but only want 40° heat for heating your
greenhouses, he points out, you can draw that 40° from the outermost
layers of the reservoir; in this way you diminish the thermal gradient
in the outermost layers of insulation.  Similarly, if the middle of
your mountain is 400°, but clouds mean you can only heat your oil to
300°, you can run the oil through only the outer parts of your
mountain that are still below 300°.

(Yurko proposes that the resulting electricity will cost 5¢/kWh,
apparently unaware that solar PV is half that most places these days,
especially the “sunny desert climate” he proposes building it in; this
is why I say his scheme is uneconomic as a source of electricity.)

Yurko proposes to transfer 1.6 petajoules in and out seasonally to the
inner 7.1 million m³ of soil, assuming 1 W/m/K for the thermal
conductivity and 0.5 kWh/K/m³ = 1.8 MJ/m³/K.  Both of these numbers
are a bit lower than the Catolico et al. figures from Drake Landing,
but probably about right given that the soil would be above the water
table.  He seems to be proposing a seasonal swing of about 125°, from
about 170° to about 295°.

Me, I’m more interested in small-scale regenerator applications like
home pottery kilns.
