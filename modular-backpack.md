In file `packing-list.md` I was surprised to realize that my small
backpack (300mm x 400mm x 150mm, 18l) was itself 475g, 14% of its
weight when full to 3.5kg with its 64 or so separate items.  And
generally all that weight is only useful as a backpack; when I'm in a
fixed place I can't use the backpack parts for non-backpack purposes.
Ultralight backpackers start as low as 2kg, 4.5kg with food and water.

So here's my design outline of a backpack that size that I think
should weigh about 150g, about a third as much, while improving
waterproofness, comfort, and convenience, and also serving some
non-backpack uses.

Conceptual backpack crap framework
----------------------------------

Backpacks contain power cables, USB chargers, food, water, blankets,
clothes, bags, fuel, stoves, cooking utensils, etc.; hereinafter
"crap".

The backpack has several functions: it supports my load of crap
without tying up my hands, it protects my back from sharp and
irregular hard crap, it keeps my crap organized so I can access it
easily, it protects it from the sun and rain, it protects it to some
extent from other crap, it hides my crap from people who I don't want
to have it or critique it, it physically impedes other people's access
to the crap, and it puts its weight on my shoulders (and, ideally, my
hips) to minimize the effort required, and it cushions shocks.

Crap support: 1.6g of nylon netting
-----------------------------------

The supporting function is in some sense the easiest: in theory I
could loop a fishing line through a hole in each piece of crap and
through some kind of harness at the top of my back, and as long as
they weighed less than twice the fishing line rating, they would be
fine.  [Monofilament nylon seems to be rated about 500MPa][1], so 5kg,
50N, 25N per side, only requires 0.25 mm five-pound line, maybe 500mm
of it, total 25mm³.  At 1.1g/cc that's 28mg.  A 28mg backpack would be
four orders of magnitude better.

[1]: https://www.beadkraft.com/reference-monofilament

More practically you probably need at least some kind of netting for
crap that don't have appropriate holes.  0.2mm monofilament nylon
fishing line does exist and should be good for 1.6kg, and nylon
normally has about 60% elongation at break (and about 3GPa Young's
modulus), so even pretty extreme shocks won't break it.  If your
netting bag is the size above, well, that's 0.45m², 4500cm².  If your
netting holes are square and 20mm across, that's 40mm of line per 4cm²
hole, 45m of line, weighing 1.6g.  In theory the 900 mm of perimeter
of the base means it's supported by 45 lines and can thus withstand a
load of 72 kg, which seems like a pretty reasonable amount of
containment for 3.5 to 10 kg of crap.

It would be nice for the netting bag to unfold into a person-sized
hammock, but that probably requires more like 200m of monofilament
line (7g) plus some kind of shock absorber.  With finer netting to
keep out mosquitoes, it could work as a mesh tent instead.  But given
the low weight of this part of the backpack, duplicating it is not a
big deal.

Crap subdivision: 60g of plastic in 4 large bags and 16 small ones
------------------------------------------------------------------

Just a netting bag tied to a harness would let all kinds of crap like
toenail clippers fall out and get lost.  Typically I already put my
crap in smaller bags inside the backpack, though.  This largely
eliminates the need for the backpack itself to perform this
containment.  With three levels of bagging, the backpack can have four
large main bags, each containing four small bags, each containing four
pieces of crap, at worst I have to fish 12 things out of bags to find
the right one; on average it's 7.5 if I just pick at random.

The reusable HDPE bags we've had imposed on us in supermarkets here
are about 10 liters and, because they're about 30 microns thick, about
7.2 grams, and they really do hold up to reuse a lot better than the
cheaper bags that are half this weight.  Something like this could
work for the large main bags, though you might be able to do a little
better with a better material like aluminized mylar, we should
probably figure on 30g or so.  Ideally these four large main bags
would all extend the full 400mm height of the backpack so you can
access any one without pulling the others out first, and they would
each be waterproof, perhaps sealed with replaceable sticky tape.

These are nominally 150mm x 150mm x 400mm, though there should be some
variation.  They open on the long side to permit independent removal
of any of four small inner bags, which are nominally 150mm x 150mm x
100mm.

The 16 or so inner small bags are both smaller area and potentially
thinner plastic film or fine netting, for example pantyhose.  I think
they're probably about another 30g, just under 2g each.

In practice such a simple and uniform design is surely quite
suboptimal.  You'd want to have a large bag on top of the others,
horizontally across the top, so you can access the most frequently
needed items without removing any large main bags.  Each bag should be
a different size, and at least one small bag should be rigid and
padded.  You'd want an extra spare large main bag or two.  The large
main bags should be easy to hang from things when they're out of the
pack.

Crap protection with the backboard: 30g of mostly plastic foam
--------------------------------------------------------------

Some kind of padding is needed between the crap and my back, in case
there's hard and sharp crap, and it needs to be somewhat resistant to
punctures and abrasion.  Ideally this "back plate" or "backboard"
would also be somewhat rigid to help transfer the weight of the crap
to my hips rather than my shoulders, like an internal frame.  This
side of the parallelepiped is 0.12m².  A block of plastic foam might
work well; building-insulation expanded polystyrene foam is about
10kg/m³, which means that a 15mm section is 150g/m², 18g.  It needs to
be protected from abrasion on both sides; on the human side it needs
to be soft, while on the backpack side it needs to be
puncture-resistant and stiff.

In file `material-observations` in Dernocua, I found 45g/m² nonwoven
polyester *friselina* fabric for 16.5¢/m², which could work well to
protect the human side from abrasion for another 5.4g plus adhesive.
(As mentioned in file `packing-list.md` I have some *friselina*
shopping bags that are closer to 100g/m², and similar blank bags seem
to cost about 57¢/m².)

The backpack side needs something harder, and ideally fluted
vertically to add stiffness without impeding sliding the four large
main bags in and out.  A sheet of PET or HIPS would be ideal unless
you're going to do a fluted fiberglass layup on the styrofoam surface.
500 microns, about the thickness of a coke bottle, would probably be
fine and 50g/m², thus 6.0g if the fluting isn't that extreme.

A different way to make a similar backboard would be to get premade
corrugated fiberglass composite roofing sheet, make a mold with some
cloth and a plastic bag, and squirt it full of expanding polyurethane
foam.

This backboard (18g+5.4g+6g = 29.4g), when disconnected from the netting and
thus the rest of the backpack, serves as a pelvis rest for sitting or
sleeping.  The hard side goes down on the ground to reduce damage from
sticks and rocks.

A different alternative would be to have a lighter, more rigid frame
over which a soft cloth mesh is stretched, like an Aeron chair, so
your back is in contact with the mesh rather than foam.  Then the hard
face would just be hard.  I think it might be possible to make this
lighter.

The rain cover: 28g to hide crap
--------------------------------

Nylon doesn't do that well in ultraviolet, and a second layer of
waterproofing is desirable, along with some semblance of security and
not looking like trash.  For this purpose, there's a sort of opaque,
waterproof bag that fits over the top of the netting bag and fastens
at the bottom, or vice versa.  It also has pockets in it for
frequently accessed and rarely stolen items such as pencils, safety
pins, and extra bags.  It has attractive art printed on it.

I think plausibly the best way to do this is to dye-sublimate the art
onto some *friselina*, glue it to a space blanket (according to file
`packing-list.md`, that's US$1.55/m² and 18μm thick, so 18g/m²), cut
it to a pattern, and hem and sew appropriately.  The cover is the
same shape and size as the netting bag, 0.45m², so this is 8g of space
blanket plus 20g of *friselina* for 28g total, not counting pockets,
which can probably be made of lighter-weight netting.

To make the cover *really* waterproof, so that crap inside stays dry
even if the backpack is floating down a river, it would probably need
to be sewn to the harness and later waterproofed.  But that isn't my
plan.

The harness: 25g
----------------

You need two shoulder straps.  A belt would also be desirable.  These
cannot be fishing lines; they must distribute weight across plenty of
skin to avoid pressure injuries.  Normally the shoulder straps have
padding sewn into them, but I think a better approach may be to make
them soft knit cloth tubes.

At low weight this alone is adequate, but at higher weights it may be
necessary to put padding inside them.  This padding can be maxi pads,
socks, underwear, towels, steel wool, toilet paper, etc.  The soft
cloth isn't waterproof, so these items either need individual
waterproof bags, or you need to store them elsewhere before it rains
or you sweat.  By sewing overlapping cloths like the fly of
tighty-whiteys, these items are accessible even while the backpack is
on your back.

I think we can expect 200g/m² from cloths like this, but each shoulder
strap is maybe 400mm long and 160mm wide, so they total 0.128m² and
25.6g.  To cut down on this weight and improve adjustability, you can
replace the lower half of each strap with a couple of loops to lace to
the desired length with shoelaces or similar.  (A similar lace across
the front holds the straps near your neck.).  This reduces the
shoulder harness to some 18g, and when the backpack is apart, the
laces can be used for other purposes, such as guying a tarp or hanging
laundry.

Plausibly a belt is also a win; aside from supporting the backpack
itself on your hips, thus putting less strain on your back, it can put
some of your crap in hip bags where it's completely hip-supported and
easily accessible without taking off the pack.  So let's say 25g as a
guess.

It's also useful to be able to leave the main backpack behind and take
only the belt.

Closures: 5g
------------

How should you open and close all these pockets and containers?  Roll
up an opening and tie down the ends?  Plastic zippers?  Metal zippers?
Plastic buckles?

I think that plausibly buttons and buttonholes are the best answer.  I
haven't weighed them but I think a button or three weighs less than a
zipper, even a plastic one, and buttons offer reconfigurability just
by virtue of adding more buttonholes.  I am guessing you'll end up
with 5g of buttons.

The shell with all its pockets can plausibly be unbuttoned to hang
from a convenient spot and provide easy access to them all when you
aren't using the backpack.

Alternatively you could lace more things together.

Alternatives
------------

Well, so that's an outline of a 150-gram 18-liter backpack, which is
also only about 200 cc.

There are things called "ground sheets" ultralight backpackers use to
sleep on damp ground while staying dry, made of tyvek or something
called polycryo, which is even lighter.  They might be worthwhile
looking into as alternative materials for things like bags and the
backboard.  Also, Kapton.

Dyneema is six times as strong as nylon, so in theory you can use one
sixth as much.  Probably using it for buttons and buttonhole
reinforcement would be more worthwhile than trying to use it
everywhere.

A single backpack liner might save some weight by allowing the four
large main bags to be lighter-weight mesh.  And it would be
interesting to see if something like aluminized mylar can save most of
that weight.

[The thinnest aluminized mylar I'm finding][-1] is AR$1118 for 10
sheets, each 1m x 0.7m of 30μm mylar, which is almost twice as thick
as a space blanket.  This is quite expensive at US$3.29/m².  But it's
much cheaper than [Kapton][-i]: 1m x 55cm of 150-micron thickness for
AR$15000: US$56/m².

[-1]: https://articulo.mercadolibre.com.ar/MLA-930493209-lamina-mylar-30-micrones-1mx70cm-reflectante-x10-metanoia-_JM
[-i]: https://articulo.mercadolibre.com.ar/MLA-1286236754-lamina-de-kapton-1mtr-x-55-cm-_JM

[Fake tyvek is sold for roofing][1+i] at about AR$9600 per 30m² roll
(US$19.80, 66¢/m²); in this case it's DTI Wichi Tecno Roofing nonwoven
waterproof polypropylene vapor barrier described as "espesor de 55gr",
which I suspect is 55g/m².  Real tyvek is HDPE and vapor-permeable.

[1+i]: https://articulo.mercadolibre.com.ar/MLA-841421814-wichi-tecno-150-x-20-30m2-membrana-hidrofuga-tipo-tyvek-_JM

55g/m² is only a bit heavier than *friselina*, and might be lighter
than *friselina* plus a space blanket.  *Friselina* can be melted
together to make seams; I suspect the same is true of tyvek.
Low-temperature hot-melt glue is another plausible way to make seams
in both cases.  Even other adhesives might work, though tyvek is
reluctant to adhere.

The rain cover of either type of material could conceivably double as
a tent fly and/or ground sheet, like a tarp.  But it probably needs to
be significantly bigger than is minimally necessary for a backpack.

Common tarps are woven polyethylene fibers sandwiched between
polyethylene sheets.  The blue "light-duty" type is 130-150 microns
and thus 130-150 g/m².  [A 4m x 6m one][1-i] is offered for AR$10319
(US$21.40, 89¢/m²), though the sellers implausibly claim it to be made
of raffia.

[1-i]: https://articulo.mercadolibre.com.ar/MLA-781982158-cobertor-lona-plastica-multiuso-4-x-6-metros-color-azul-_JM

The smallest traditional tents I see at Easy are some 600mm long when
folded up and stored, so it might be a good idea to make the backpack
that tall in order to be able to contain one of them without it
sticking out.

Another possibly relevant construction material is the thin
closed-cell polyethylene foam underlayment used for pergo floors,
which provides flexible padding and waterproofness at low weight.
I've seen someone use it as a blanket when sleeping on the street.

Hierarchy or tiering
--------------------

It probably makes sense to carry the most critical items in a hip belt
(or money belt, or in pockets) so that you don't lose access to them
whenever you take the backpack off.  Some packs have "belt panels"
that the belt threads through which are basically fanny packs.  A
kilogram and a liter are reasonable numbers to shoot for, for a fanny
pack.  (Other names include bumbag, belt bag, waist bag, moon bag, or
belly bag, WP tells me; it also says Ötzi was wearing a fanny pack
when he died, and that fashion designers have decided to rehabilitate
fanny packs.)  That leaves a lot less weight for the actual backpack
part.
