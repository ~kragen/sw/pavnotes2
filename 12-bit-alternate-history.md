C really wants your computer to be byte-oriented; it is very much a
child of the PDP-11 and thence the IBM 360, and the onslaught of
360-like microcomputers in the 80s cemented its win.

Intel’s first CPU, the 4-bit 4004, was designed for doing BCD
operations one digit at a time in pocket calculators for Busicom.
Their concurrent but mostly unrelated 8008 mostly implemented the
instruction set from CTC’s Datapoint 2200 “programmable terminal”, but
also supported BCD arithmetic, perhaps with a view to 4004-like
applications.  So a 6-bit, 9-bit, or 18-bit architecture was probably
unlikely; the 2200, though bit-serial, had to be architecturally 8-bit
because even then, terminals commonly spoke 7-bit ASCII with a parity
bit, though perhaps a bit more commonly without one.

4 bits is a bit small for BCD calculator registers, wasting most of
your transistors on control, though I guess Saturn survived that way
until the 90s.

But maybe 12 bits could have won.  The [12-bit PDP-8 fit onto the
4000-transistor Intersil 6100][0], and that was in CMOS, which
requires extra transistors compared to NMOS that’s about the same size
as the RCA 1801 (CMOS, 5000 transistors) and the Intel 8008 (NMOS I
think, and 3500).

[0]: https://en.wikipedia.org/wiki/Transistor_count

But the 6502 and 8080 could address a lot more memory than a PDP-8,
and presumably that dead-end aspect of the PDP-8 is why we got Data
General and, presumably as a result, the PDP-11.

Maybe a sufficiently inspired extended -8 could have supported larger
memory spaces in a useful way without going to a larger accumulator
and ALU; the Smalltalk-80 virtual machine, perhaps inspired by the
Nova, had four “base registers” instead of the PDP-8’s two.

On the -8, direct memory references were by a 7-bit index field in the
instruction word, via either a 6502-like zero page or the page the
program counter was on, roughly corresponding to globals and an
ARM-like literal constant pool; on the -8 that second mode was also
used for local variables.

Smalltalk separated local variable accesses from constant-pool accesses, so it 
could support recursion, and also added accesses to slots in the receiver.
You could imagine a PDP-8-like machine that could operate on 3 BCD digits at a 
time, but also access more memory by having such stack-frame and auxiliary base 
pointer registers, and extending them to be widerp
20 or 24 bits, say.

12-bit would also have allowed octal, which was a bit awkward on
8-bitters: is the highest 16-bit address 377377, or is it 177777,
despite containing neither the byte 177 nor the impossible-in-8-bits
777?

12 bits is a perhaps uncomfortably wide character set for English,
using 50% more expensive memory than 8-bit bytes, but Japan might have
adopted it early: it’s big enough for the joyo kanji.  And for more
compressed English text, you could pack two SIXBIT characters per
12-bit word.
