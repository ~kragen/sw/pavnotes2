Hammers are among the oldest human tools; like levers, inclined
planes, movable pulleys, or hydraulic cylinders, they amplify the
force exerted by applying it to a larger load over a shorter distance.
The difference, and the reason Galileo omitted the hammer from his
list of the six simple machines in *Le Meccaniche* in 01600, is that
in the hammer the input and output forces are exerted at different
times, connected only by kinetic energy, which wouldn't be discovered
until Emilie du Chatelet (published in 01756).

A phenomenon I think is underappreciated, except perhaps in baseball,
is how a hammer can launch the thing it hits at a higher velocity than
thee hammer itself.

As is well known, an elastic collision between a large object and a
small object can accelerate the smaller object to a higher speed,
because both momentum and energy are conserved.  Here *v*'₀ is not the
derivative but the new velocity of the object of mass *m*₀ after the
collision; supposing object of mass *m*₁ is previously stationary:

> *m*₀*v*₀ = *m*₀*v*'₀ + *m*₁*v*'₁  (1: conservation of momentum)  
> ½*m*₀*v*₀² = ½*m*₀*v*'₀² + ½*m*₁*v*'₁²  (2: conservation of energy)  
> *m*₀*v*₀² = *m*₀*v*'₀² + *m*₁*v*'₁²  (3: 2 without common factor)  
> *m*₀*v*₀² - *m*₀*v*'₀² = *m*₁*v*'₁²  (4: 3 with *m*₀ moved to one side)  
> *m*₀(*v*₀² - *v*'₀²)/*m*₁ = *v*'₁²  (5: 4 solved for *v*₁²)  
> *m*₀(*v*₀ - *v*'₀)/*m*₁ = *v*'₁  (6: 1 solved for *v*'₁)  
> √(*m*₀/*m*₁)√(*v*₀² - *v*'₀²) = *v*'₁  (7: 5 solved for *v*'₁)  
> √(*m*₀/*m*₁)√(*v*₀² - *v*'₀²) = *m*₀(*v*₀ - *v*'₀)/*m*₁  (8: 6 substituted in 7)  
> √(*v*₀² - *v*'₀²) = √(*m*₀/*m*₁)(*v*₀ - *v*'₀)  (9: 8 without common factor)  
> *v*₀² - *v*'₀² = (*m*₀/*m*₁)(*v*₀² - 2*v*₀*v*'₀ - *v*'₀²)  (10: 9 squaring both sides)  
> (*m*₀/*m*₁ - 1)*v*'₀² + 2(*m*₀/*m*₁)*v*₀*v*'₀ + (1 - *m*₀/*m*₁)*v*₀² = 0  (11: 10 moved all to one side)  

At this point we can use the quadratic formula to calculate *v*'₀ and
thus *v*'₁.

Hmm, I must have fucked that up.  [Wikipedia says][0] that the
solution is, in the variables above:

> *v*'₀ = (*m*₀ - *m*₁)*v*₀/(*m*₀ + *m*₁)  
> *v*'₁ = 2*m*₀*v*₀/(*m*₀ + *m*₁)

[0]: https://en.wikipedia.org/wiki/Elastic_collision#One-dimensional_Newtonian

I'll postpone trying to figure out what my error was for the time
being.  These solutions do seem to check out:

    In [2]: m0, m1, v0, v1, v0p, v1p = sympy.symbols('m0 m1 v0 v1 v0p v1p')
    In [8]: (((m0 - m1)*v0 / (m0 + m1)) * m0 + 2*m0*v0/(m0 + m1) * m1).simplify()
    Out[8]: m0*v0
    In [9]: (((m0 - m1)*v0 / (m0 + m1))**2 * m0 + (2*m0*v0/(m0 + m1))**2 * m1).simplify()
    Out[9]: m0*v0**2
    
Oh, [Hyperphysics has the derivation][1], which starts as above but continues:

> *m*₀(*v*₀ - *v*'₀)(*v*₀ + *v*'₀) = *m*₁*v*'₁²  (12: 5 with difference of squares)  
> *m*₀(*v*₀ - *v*'₀) = *m*₁*v*'₁  (13: 6 rearranged)  
> *v*₀ + *v*'₀ = *v*'₁  (14: 12 divided by 13)  
> *v*'₀ = *v*'₁ - *v*₀  (15: 14 solved for *v*'₀)  
> *m*₀(*v*₀ - *v*'₁ + *v*₀) = *m*₁*v*'₁  (16: 15 substituted into 13)  

At this point I got lost for a while.

> 2*m*₀*v*₀ = (*m*₁ + *m*₀)*v*'₁  (17: 16 with each velocity moved to its own side)  
> *v*'₁ = 2*m*₀*v*₀/(*m*₁ + *m*₀)  (18: 17 solved for *v*'₁)  
> *m*₀(*v*₀ - *v*'₀) = *m*₁*(*v*₀ + *v*'₀)  (19: 14 substituted into 13)  
> *m*₀*v*₀ - *m*₀*v*'₀ = *m*₁*v*₀ + *m*₁*v*'₀  (20: 19 distributed)  
> (*m*₀-*m*₁)*v*₀ = (*m*₀+*m*₁)*v*'₀  (21: 20 with each velocity on its own side)  
> *v*'₀ = (*m*₀-*m*₁)*v*₀/(*m*₀*+*m*₁)  (22: 21 solved for *v*'₀)  

[1]: http://hyperphysics.phy-astr.gsu.edu/hbase/elacol2.html#c2

So the initially stationary object goes faster by a factor
2*m*₀/(*m*₀ + *m*₁), which is 1 if the masses are equal, approaches 0
when the mass of the impactor approaches 0, and approaches 2 when its
own mass approaches 0.  It would seem that at either extreme the
kinetic energy imparted to the initially stationary object
approaches 0.  When the masses are equal, all the kinetic energy is
transferred from the impactor to the target, so we have a sort of
unavoidable tradeoff between speedup and "efficiency", if we think of
energy left in the impactor as "lost": the higher the output speed,
the less energy is transferred.

If you wanted to transfer nearly all of the energy into smaller,
faster objects, you could collide an initial large impactor with a
sequence of smaller objects, each taking a fraction of the initial
impactor's energy.  With hard materials, the distance from one impact
to the next could be pretty short, submillimeter for macroscopic
objects.

If we divide the numerator and denominator by *m*₀ then the speedup is
2/(1 + *m*₁/*m*₀).

If we arbitrarily decide *m*₁/*m*₀ = 1/*e* = 0.37, this is about 1.46.
A chain of four such impacts in sequence, without the multiple-impact
trick above, with objects of relative masses 1, 0.37, 0.14, 0.050, and
0.018 should give relative velocities 1, 1.5, 2.1, 3.1, 4.6 and
relative energies 1, 0.79, 0.62, 0.49, 0.38.  If instead we have a
chain of 8 impacts reducing down to the same mass, the mass reduction
factor is 0.607 per step, so the masses are 1, 0.607, 0.368, 0.224,
0.136, 0.082, 0.050, 0.030, and 0.018; the relative velocities are 1,
1.24, 1.55, 1.93, 2.40, 2.99, 3.72, 4.62, and 5.76; and the energies
1, 0.940, 0.884, 0.831, 0.781, 0.735, 0.691, 0.649, and 0.611.  So you
have 61% of the energy of the initial impactor in 1.8% of its mass.

Suppose you're willing to accept lower efficiency in exchange for a
higher speedup.  For example, 2 grams at 400 m/s, 160 J, starting from
an initial 200 gram impactor; how fast would it have to be going?
With 100% efficiency it would be 40 m/s.  With a chain of 8 such
impacts the mass reduction factor per step is 0.56, so the speedup per
step is 1.28, 7.3 after 8 impacts, so the 200-gram impactor is going
55 m/s, 300 J.  Accelerating it to this speed over 100 mm would
require an average of 3kN, the weight of 310 kg.

Supposing the initial 200-gram impactor is reacting against another
200-gram reaction mass, and the other objects in the chain are 112,
63, 35, 20, 11, 6.2, 3.5, and 1.9 grams, the whole apparatus weighs
652 grams plus overhead, small enough to be handheld.  If each mass is
made of 7.9 g/cc steel in the shape of a cylinder whose diameter is
three times its height, its volume is 7.1 times the cube of its
height, and its mass 56 grams/cm³ times the cube of height; this gives
them heights of 15.3 mm, 12.6 mm, 10.4 mm, 8.6 mm, 7.1 mm, 5.8 mm, 4.8 mm,
4.0 mm, and 3.3 mm, for a total length of 72 mm plus the initial
spaces between them, and a total diameter of 46 mm.

If we take the speed of sound in steel as 6 km/s, the time for the
shock wave from the impact to ring back and forth through the
15-mm-long weight ten times might be 50 microseconds.  Over shorter
periods of time we can't really treat it as a lumped mass; we have to
consider the propagation of sound waves through it.  Unfortunately
this is a pretty long period of time considering that the Young's
modulus of steel is only 200 GPa and all the kinetic energy that will
ultimately be transferred to the 112-gram mass (274 J) must first be
converted into compressive strain.  200 GPa per strain times the
cross-sectional area of 11.2 cm² is 220 MN per strain or 220 kN per
millistrain, which at 12.6 mm is 220 kN per 12.6 microns or 18
kN/micron, disregarding for the time being the fact that we're pushing
against the accelerating mass of the thing and not a vise jaw or
something.  The spring energy of ½kx² then gives us 0.9 J for 10
microns, 8 J for 30 microns, 90 J for 100 microns, and 150 J for 130
microns.

At 130 microns of deformation, the 112-gram mass would be exerting 2.3
meganewtons of force and accelerating at 2 million gees.  At that
acceleration it will have lost contact with the larger mass in only
3.5 microseconds, an order of magnitude too fast, probably wasting
most of the energy of the collision in vibrational modes of the two
masses.

Probably this can be improved by giving the masses a flatter aspect
ratio and convex contact surfaces, but I'm not sure if it can be
improved *enough*.

Note that this is only a problem at high speeds and large scales like
this.  If the initial impactor is moving only 1 m/s, like a normal
hammer, the problem goes away.  It also goes away if we scale down, so
that instead of 200 grams, 112 grams, 63 grams, etc., the masses are
200 mg, 112 mg, 63 mg, etc., even at the same 55-400 m/s.

An apparent solution is to put springs on the ends of the masses; in
the limit, the masses become hollow lenticular shells, functioning
like pairs of Belleville washers, but I don't think we have to go that
far.  The springs are quite stiff, as springs go; in the case of the
first impact, it is sufficient to extend it from some 5 microseconds
and 100 microns up to, say, 50 microseconds, so we're looking for a
spring constant 100x lower, 200x to be safe: 100N/micron.  So we
expect a peak on the order of 2.5mm of spring compression and 250 kN
and on the order of 70 microseconds of contact time.

However, this becomes impossible as we move to the smaller masses,
because almost the same elastic deformation energy has to get packed
into a smaller and smaller mass.  Proof resilience is ½σ²/E; if we
suppose for music wire σ = 2.5 GPa and E as above is 200 GPa, we get
16 MJ/m³, which means we need 19 cc of music wire to hold 300 J,
weighing 150 g.  This also gives us a maximum speed music wire can
launch itself at under its own strain energy: 2.0 J/g, which is the
same energy as 63 m/s.

So if we can't put enough elastic energy into steel to reach our
desired speeds, what can we do?  Maybe we can use air springs between
the masses to give them elastic collisions; each mass can end in a
cylinder that slips over a piston in the next one.  The compression
and decompression ought to be fairly close to adiabatic and thus
efficient, and a hole or two in the side of the cylinder ought to be
sufficient to clearly separate the elastic "collision" stage of the
motion from the almost lossless free-motion stage.  This is crucial to
allow full conversion of the compression energy in each stage to the
kinetic energy of the mass before energy starts transferring to the
next stage.

The cylinder wall needs to be thick enough not to burst, but the part
of the cylinder under compression can be maybe quite short, and thus
supported by the end of the cylinder.  Also, instead of each mass
containing its own cylinder, several of the same diameter can perhaps
be fit as sliding pistons into a larger, non-moving cylinder, so the
cylinder can be thick and heavy without slowing down the masses.  (A
much narrower, very compliant metal spring set into a tiny bore in the
center of each piston can return them to their home positions.)
Transitions between cylinder diameters can be achieved with a piston
turned with a shoulder.

Such a design with only two masses is currently used in an Olympic
sport.

It isn't necessary to have the masses entirely enclosed in a single
continuous cylinder; only the air-spring zones where they transfer
energy by compressing air need to be enclosed.  This permits
suspending the masses with something like a Sarrus linkage and homing
them via external springs, avoiding the need to drill piston faces for
homing springs.

For use as a hand tool, probably only much lower velocities are
needed; the outer casing of the hammer can strike against the striking
target with only ordinary hammer force, bringing the later stages
gently to a stop via their homing springs, while the primary impactor
continues to slide freely within the hammer's head, a few tens of
milliseconds thereafter striking the second mass and starting the
rapid chain of impacts which then drives the final-stage impactor into
the face of the hammer at much higher velocity.  This requires that
the face of the hammer be lightweight and able to swing free from the
hammer's outer casing when the final impactor strikes it.

Non-fully-enclosed pistons sticking also potentially permits more than
one input or output piston on the same mass, potentially enabling the
higher efficiencies suggested earlier.  If we use as above a mass
ratio of 0.607, 6.0% of the energy stays in the impactor, which is
thus traveling at 24% of its original speed.  If we arrange for it to
strike a second mass once it has launched the first one, we can
recover most of this remaining energy, and perhaps arrange for this
second mass to strike the next stage in line at the same time, for
example by being longer or traveling faster than the already-moving
mass.  It's probably kind of a waste if it's only going 24% as fast as
its partner, because velocity amplification is the whole point of what
we're doing here, but there's no way we can get it up to going just as
fast with elastic collisions; the asymptotic limit would be twice the
speed of the primary impactor, which is only 48% of the speed the
primary impactor has originally.

However, once we're faffing around with sticking pistons into fixed
cylinders, we're no longer really in momentum-conserving territory,
because we can transfer momentum to the cylinders and possibly thereby
into the Earth.  This would work by making the output piston of each
mass wider than the input piston of the next mass, and possibly than
its own, and boring the cylinders to match.  The hot compressed air in
the wide input cylinder is pushing forward partly on the narrow oytput
piston but mostly on the cylinder shoulder, so the reaction force on
the input mass is much greater than the force on the output mass,
which therefore moves a greater distance while the cylinder is under
pressure.  By this means the final mass ends up with nearly all the
input energy but only a tiny fraction of the momentum of the primary
"impactor".  This is the same principle used in the pumps of waterjet
cutters to boost the pressure stage by stage, but in this case driven
by kinetic energy rather than directly by the pressure of the previous
stage.
