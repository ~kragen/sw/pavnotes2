I bought some aluminum foil and a dish towel (“balerina”) today,
02022-11-09; see file `material-observations-02022.md`.  Here are some
calculations about the pricing and expected performance of different
possible ways to make sandwich panels with a styrofoam coare.

Fibrous reinforcement
---------------------

Here I considered aluminum foil, nonwoven dish towel, fiberglass
window screen, kraft paper, jute burlap, reflective mylar, friselina,
single-face corrugated cardboard, bidirectional woven fiberglass
cloth, unidirectional fiberglass cloth, and masonite as possible face
sheet stiffeners.

4 m × 28 cm × 19 g/m² for US$1.38 is about 21.3 g of aluminum and thus
a price of US$64.80/kg of aluminum or US$1.23/m².  I have seen
aluminum foil on MercadoLibre of presumably the same 7-μm thickness
for 50¢/m².

The dish towel’s 34¢ for 18.99 g and 370 mm × 400 mm gives
US$17.90/kg, or US$2.30/m².

Then I checked MercadoLibre to find out what other fibrous stiffening
materials I could apply to the surface of a sandwich panel.

[Agroplastic-brand fiberglass window screen][ws] on MercadoLibre costs
AR$586.51 per meter at 1.5-meter widths, which works out to
US$1.35/m².

[ws]: https://articulo.mercadolibre.com.ar/MLA-1129758448-tela-tejido-mosquitero-fibra-de-vidrio-150cm-venta-x-metro-_JM

[Kraft paper for wrapping][pk] costs AR$2189 for 40 sheets of 850 mm ×
1200 mm at 80 g/m², which works out to US$7.55, 19¢/m², and
US$2.31/kg.  Mina tells me gray “sulfite paper” is even cheaper:
AR$4666.81 for a roll of 12 kg, US$16.09, US$1.34/kg (“bobina papel
sulfito copsi”), probably the same thickness as the kraft paper but
thinner.

[pk]: https://articulo.mercadolibre.com.ar/MLA-880642294-40-papel-madera-kraft-85x120cm-embalaje-bolsas-pliegos-_JM

[Jute burlap][jb] costs AR$589 per meter at 1 meter width and “270–300
grams”, I assume per square meter.  This works out to US$2.03/m² and
US$7/kg.  I’m astounded that it costs twice as much as five layers of
kraft paper.

[jb]: https://articulo.mercadolibre.com.ar/MLA-909325831-tela-arpillera-de-yute-comun-precio-x-metro-deco-manualidad-_JM

[Reflective mylar for indoor cultivation][rm] costs AR$1830 for 10 m ×
600 mm × 100 μm.  If we assume 1.4 g/cc for mylar, that’s 140 g/m², so
we get US$6.31, US$1.05/m², and US$7.50/kg.  I was thinking that this
stuff came with adhesive backing but apparently not.

[rm]: https://articulo.mercadolibre.com.ar/MLA-920054453-papel-mylar-reflectante-x-10-metros-100-micrones-indoor-_JM

[A 100-meter roll of *friselina*][rf] nonwoven polyester, “15 grams”
(per m² I suppose), 66 cm wide, costs AR$1390, which works out to
US$4.79, 7.3¢/m², and US$4.84/kg.

[rf]: https://articulo.mercadolibre.com.ar/MLA-618730179-cubrecamillas-rollos-x-100-metros-fiselina-o-friselina--_JM

[A roll of single-face corrugated cardboard][rc] costs AR$3606 for
1.2 m × 25 m and weighs supposedly 8 kg.  This works out to US$12.40,
41¢/m², US$1.55/kg, and 270 g/m².  This is the lowest price so far per
kg if it’s correct.  This product is also available at Easy; I wonder
if it would also be cheaper there.  Single-face corrugated cardboard
is like two-thirds of single-wall corrugated cardboard: it’s missing
the other face.

[rc]: https://articulo.mercadolibre.com.ar/MLA-1127970036-oferta-rollo-carton-corrugado-12m-x-25mts-_JM

[220g/m² woven fiberglass cloth for epoxy layups][wfc] costs
AR$699/m², which works out to US$2.41/m² or US$11.00/kg.  This doesn't
say whether it's E-glass, S-glass, or something else; the brand is
“Riversoul”.

[wfc]: https://articulo.mercadolibre.com.ar/MLA-790695673-tela-fibra-de-vidrio-tejido-220-grsmt2-x-1-mt2--_JM

[No-brand 300g/m² unidirectional E-glass fiberglass cloth for epoxy
layups][udc] costs AR$2154/kg, which works out to US$7.43/kg (cheaper
than the mystery-meat cloth above!) and US$2.29/m².

[udc]: https://articulo.mercadolibre.com.ar/MLA-1144103840-tejido-e-glass-vidrio-unidireccional-300grm2-x-kg-_JM

[Fiplasto-brand chapadur, masonite, high-density fiberboard, or
hardboard][cd] sells for AR$2052 per 1.22 m × 2.44 m (i.e., 4'×8') at
3mm thickness.  If we figure it’s about 1.2 g/cc that’s probably about
10.7 kg of masonite.  That works out to US$7.08, US$2.38/m²,
3.6 kg/m², and 66¢/kg.

[cd]: https://articulo.mercadolibre.com.ar/MLA-880267026-placa-chapadur-habano-segunda-cal-3mm-122-x-244-embalajes-_JM

Mina tells me contact paper comes in a 10 m × 400 mm roll for AR$1990
(US$6.86, US$1.71/m²), but we don’t know how much it costs.

### Tradeoffs ###

Of the above reinforcements, several are basically just cellulose:
masonite, cardboard, burlap, and kraft paper; for use as
sandwich-panel faces we can treat them all as just different ways to
buy cellulose to smash onto sandwich-panel faces.  Given that,
probably the relevant observations are that masonite is the cheapest
per kg (66¢/kg but US$2.38/m² because it’s 3600g/m²), and recycled
sulfite paper is the cheapest per m² (less than 19¢/m² but US$1.34/kg
because it’s 80g/m²).

Crudely we might guess that all these purified cellulose products have
a [Young’s modulus][ymet] on the order of 10 GPa.  E-glass, by
contrast, is supposed to have a Young’s modulus of 72 GPa, so smearing
7 grams of cellulose across some surface should give you the same
rigidity as smearing just 1 gram of E-glass, or 2 grams if they’re in
an omnidirectional fiber mat or woven roving.  So, in terms of
rigidity, US$1.55/kg of cellulose is equivalent to about US$6/kg for
planar glass fiber or, where applicable, US$11/kg for unidirectional
glass fiber.  And [aluminum][alym] has about the same modulus as E-glass.
Mylar (polyethylene terephthalate) is only about 2–2.7 GPa.

[ymet]: https://www.engineeringtoolbox.com/young-modulus-d_417.html
[alym]: https://www.mit.edu/~6.777/matprops/aluminum.htm

So if our objective is to keep a sandwich panel from buckling cheaply,
probably fiberglass or cardboard are about equally good, but masonite
is much better when we need the strength it has.  However, masonite
costs several times as much as the styrofoam (which is probably on the
order of US$1/m²).

Stuccoing instead
-----------------

Thinking further, if we glue whatever reinforcement to the styrofoam
with a coat of brittle but hard adhesive, it might be practical to
stucco the styrofoam with enough adhesive to provide more stiffness
than the reinforcement.  According to file
`desiccant-climate-control.md` slaked lime costs 12¢/kg at retail and
plaster of Paris 40¢/kg, and according to file `sodium-silicate.md`
portland cement costs 10.2¢/kg, hydraulic slaked lime 3.5¢/kg, and
sodium silicate about US$3/kg (of the solid), and construction sand
costs 5¢/kg.  According to file `material-sourcing.md` I bought the
dirty beach sand I have here for 6.2¢/kg three months ago.  Quartz
sand has a Young’s modulus of about 90 GPa.  The Young’s modulus of
cured portland cement (calcium silicate hydrates) [ranges from 100 GPa
to 300 GPa][csh], with the highest values occurring with high
proportions of calcium, so probably in a portland-cement mortar, the
quartz sand will be the thing that deforms, and the whole mortar will
have roughly the Young’s modulus of quartz.

[csh]: https://hal.archives-ouvertes.fr/hal-00475195/PDF/Plassard.pdf "Cédric Plassard, Eric Lesniewska, Isabelle Pochard, André Nonat. Intrinsic Elastic Properties of Calcium Silicate Hydrates by Nanoindentation. 12th International Congress on the Chemistry of Cement, Jul 2007, Montreal, Canada. pp.44. hal-00475195" 

(As a side note, since according to file
`material-observations-02022.md`, styrofoam costs US$4.30/kg and up at
retail, sodium silicate is evidently actually cheaper at retail than
styrofoam, and certainly a great deal more convenient to carry home on
the bus.  This suggests that foaming up waterglass with heat in a mold
between foil sheets might be a competitive way to make insulating
panels.  Maybe adding a little calcium phosphate or calcium carbonate
to the waterglass would improve its fire resistance.  But file
`material-observations.md` says the waterglass foam I was able to make
had a density around 61 mg/cc, i.e., 61 kg/m³, about five times denser
than styrofoam; and I think the wholesale cost of waterglass is
enormously lower, and its rigidity and strength are higher.)

Much higher values could be achieved with sapphire sand (Young’s
modulus 377 GPa), especially with a harder binder, but it is more
expensive.  It isn’t *expensive* but nothing is as cheap as quartz
sand.  According to file `material-sourcing.md` it costs about
US$2.10/kg, and silicon carbide (twice as stiff again) costs
US$6.38/kg.

Further investigation suggests that typical Young’s modulus for cured
Portland cement is actually closer to 15–50 GPa, lower than the values
given above for the pure alite and belite because of porosity, so in
fact higher sand loading probably increases rigidity rather than
decreasing it.

### Polymer-modified cement for stuccoing ###

Suppose, and this is maybe reaching a bit, that this stuff actually
sticks to styrofoam.  A mortar mix of 25% portland cement, 15% water,
and 60% of this shitty beach sand ought to cost about 6.3¢/kg, and so
wouldn’t reach US$1/m² until 16 kg/m²; assuming 2.4 g/cc density for
the cured mortar we have a 6.6-mm layer (or two 3.3-mm layers, one on
each side of a sandwich board).

Evidently [stuccoing a styrofoam wall with a cement mix and fiberglass
mesh is a common thing to do][ehcs] and [so is covering styrofoam in
plaster of Paris][ehpp].

[ehcs]: https://www.ehow.com/how_7888766_cement-styrofoam-wall.html
[ehpp]: https://www.ehow.com/how_5673370_seal-styrofoam.html

If the cement doesn’t stick to styrofoam naturally, maybe mixing latex
paint into it would help; [US patent 4,559,263 from Dow suggests Dow
styrene butadiene latex][latepat], which [BF Goodrich’s US patent
3,002,940][goodrich] suggests can be used as a latex paint as an
alternative to acrylates.  [Mallard Creek Polymers][pci] has a press
release where they explain that they offer both “all-acrylic polymer
emulsions” and, for specialty applications needing “outstanding water
resistance and binding strength” as well as “very high filler
loading”, “styrene–butadiene (SBR) latex”; they also explain [that the
first latex paints, from Dow, were styrene-butadiene latexes][mcp].
The Dow patent also suggests that acrylate latexes should work fine
too.  [Other papers suggest using surfactants like sulfonol][ops] to
improve adhesion between styrofoam and concrete, but I think latex
paint is a pretty damn fine surfactant too.

This plastic can of latex paint I bought today weighs 1368 g, so it’s
probably about 1250 g of paint.  It cost US$6.55, so that’s about
US$5/kg.  If I replaced 20% of the water with this paint, the mix
would be 3% paint, adding a cost of about 15¢/kg, roughly quadrupling
the overall cost.  But really only the layer immediately in contact
with the styrofoam would need any such modification.

[Quikrete sells a Foam Coating product][qkfc] billed as a
“polymer-modified, fiber-reinforced Portland cement based rigid
coating” to stick better to styrofoam.  The MSDS is unenlightening as
to which polymers are used.

[qkfc]: https://www.quikrete.com/productlines/foamcoatingpro.asp

[Pacific Northwest National Lab’s guide to stuccoing over
styrofoam][pnsf] suggests using “polymer modified (PM) or traditional
cement stucco” and “lath” (evidently meaning chicken wire).

[pnsf]: https://basc.pnnl.gov/resource-guides/stucco-over-rigid-foam-insulation#edit-group-description

[latexpat]: https://patents.google.com/patent/US4559263A/en
[goodrich]: https://patents.google.com/patent/US3002940A/en
[pci]: https://www.pcimag.com/articles/101891-all-acrylic-polymer-emulsions-and-styrene-butadiene-latex
[mcp]: https://www.mcpolymers.com/library/styrene-butadiene-latex
[ops]: https://www.sciencedirect.com/science/article/abs/pii/S0958946504000101

### Stuccoing should help a huge amount ###

Suppose we have 16 kg/m² of portland-cemented sand, 3.3 mm thick on
each side of the styrofoam, with a Young’s modulus of 90 GPa.  3.3 mm
is 0.0033 m²/m, and multiplying that by 90 GPa gives us 297 MN/(m/m)
of deformation or 297 N/(μm/m), or rather 297 N per meter of wall
width per (μm per meter of height).  If this is on the surface of a
40-mm-thick slab its distance from the neutral axis is 20 mm and so
this works out to a moment of 5.94 newton meters per meter of wall per
(μm per meter) of deformation.

Because I still haven’t derived the Young’s modulus of the styrofoam
itself from last night’s Euler-buckling observations, I’m just going
to use the [2.7 compressive, 5.0 MPa tensile numbers from this 02015
paper][chhs].  The 20 mm on the compressed side of the neutral axis,
0.02 m²/m, will then require 0.054 N per meter to compress it by one
micron per meter; its average distance from the neutral axis is 10 mm,
and so this works out to a moment of 0.0054 N m per meter.

[chhs]: https://www.researchgate.net/publication/271225927_Static_and_dynamic_mechanical_properties_of_expanded_polystyrene "Static and dynamic mechanical properties of expanded polystyrene, March 02015, Materials and Design 69(4) DOI:10.1016/j.matdes.2014.12.024, Chen, Hao, Hughes, and Shi"

That is, unless I’ve miscalculated, 3.3 mm of concrete stucco, costing
about US$4/m², should be enough to make the styrofoam more than a
thousand times as flexurally rigid.  But it increases its weight from
500 g/m² to 16.5 kg/m² and its cost to US$6/m².

With that in mind, it might be better to try for the thinnest feasible
coating, like, maybe 0.3 mm, the thickness of three grains of sand.
This should increase the flexural rigidity by about two orders of
magnitude, only roughly quadruple the weight (to about 2 kg/m²), and
only increase the materials cost from about US$2/m² to about
US$2.40/m².  And it should be about as rigid as if I’d used 3-mm
masonite, because the cement material has about 10 times the Young’s
modulus of cellulose.

(Actually, because the styrofoam can shear as well as compressing, all
this extra rigidity in the surface of the sandwich panel will probably
be wasted from the perspective of flexural rigidity.  [The shear
modulus of styrofoam][smsf] is even lower than its Young’s modulus.)

[smsf]: https://mdpi-res.com/d_attachment/polymers/polymers-12-00047/article_deploy/polymers-12-00047.pdf?version=1577681868 "Measurement of the Shear Properties of Extruded Polystyrene Foam by In-Plane Shear and Asymmetric Four-Point Bending Tests, Yoshihara and Maruta.  Note that they were testing XPS rather than the EPS I have here."

To get a consistent thin coat, it still might be useful to apply it
papier-mâché style by wetting paper or window screen in the stucco mix
and laying the stuccoed paper on the styrofoam surface.  Thinner
fibrous reinforcement like fiberglass window screen might be more
promising than cellulose alternatives if they allow you to get a
thinner layer of stucco.

You can also use portland cement without any sand in it, which gives a
smoother surface though probably not higher rigidity.  Normally you
don’t do this in part because sand is cheaper, but in this case
plausibly most of the cost of the material will come from the latex,
and the large particle size of the sand may be disadvantageous.

If the stucco has microcracks in it, it will not have much rigidity in
tension, just whatever comes from whatever fibrous reinforcement
bridges the cracks.  If the styrofoam can be elastically deformed so
that the face being stuccoed is convex, then relaxed once the stucco
has set somewhat, it could be pretensioned to keep both stucco faces
under compression, which would dramatically improve their rigidity.
However, this technique probably will not last forever; the styrofoam
will creep and gas will diffuse out of its cells, so eventually it
will no longer be under tension, and the stucco will no longer be
under compression.  Post-tensioning it with cables might work better.

Other candidate adhesives
-------------------------

Aside from the portland cement, plaster of Paris, quicklime,
waterglass, and latex paint mentioned above, candidate adhesives
include spray adhesive, acetic silicone, epoxy resin, doublestick
tape, and exotic cements like magnesium oxychloride, geopolymers, or
phosphates of zinc, magnesium, or aluminum.

[Spray adhesive][aas] costs AR$2300 for a 305-gram spray can, US$7.93,
US$26/kg.  This could be useful for sticking mylar to styrofoam if it
doesn’t eat the styrofoam.  Given that its solvents include dimethyl
ether, dichloromethane, hexane, and something called GLP, I think it
probably would.  If you can spray it on the mylar, let it dry, and
then stick it to the styrofoam (or stucco), it might be okay.

[aas]: https://articulo.mercadolibre.com.ar/MLA-1144605018-pegamento-adhesivo-aerosol-spray-permanente-tekbond-305-grs-_JM

[Contact cement][cccc] costs AR$1749 per liter, US$6.01 at AR$291/US$,
and I think this is rubber cement.  It’s recommended for things like
gluing leather to cloth.  Probably its solvent would dissolve
styrofoam; it's specifically advertised as “with toluene” and [with
toluene and acetone][ccta].

[cccc]: https://articulo.mercadolibre.com.ar/MLA-843291333-cemento-de-contacto-adhesivo-pega-fuerte-x-1-litro-_JM
[ccta]: https://articulo.mercadolibre.com.ar/MLA-1122163633-cemento-de-contacto-kekol-k-2024-400gr-adhesivo-para-madera-_JM

As noted in file `material-observations-02022.md`, *Sinteplast
Recuplast Interior Mate* white latex paint at 400g/m² seems to be a
viable option, but it’s more expensive than the styrofoam it’s gluing
together; I’ll have to see if it still works when applied much more
sparsely.

Ethyl acetate (or, rather, nail polish diluent, with some other
ingredients as well) eats holes in the stuff and makes it stick to
things.  I ought to be able to dilute the stuff to the right
concentration in ethanol, spray it onto a styrofoam surface to make it
just barely gummy, and then press that surface against another
styrofoam surface.  It wouldn’t have to be pure ethyl acetate; any
kind of nail polish remover, and many paint thinners (typically
largely toluene and methanol), turpentine substitutes (“aguarrás”),
citrus terpenes, etc., ought to work.  [One article claims even
kerosene or diesel fuel will work][eolo], and many others say
[gasoline will][smds], though apparently Abbie Hoffman disagrees.  See
file `nonpolar-solvents.md`.

[eolo]: https://essentialoils.org/news/notes/what-are-you-proving-when-you-dissolve-styrofoam-with-lemon-oil
[smds]: http://www.sciencemadness.org/talk/viewthread.php?tid=65708

Sugar water might be an option too, perhaps with a little blue vitriol
or borax to keep the ants away.
