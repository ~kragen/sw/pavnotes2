I managed to find [a page on the ARM developer website that hasn't
been deleted yet][0]: "Table 3.1 of the Cortex-M0 Technical Reference
Manual r0p0", which lists the entire Cortex-M0 instruction set, with
instruction timings with zero wait states.  The Cortex-M cores *only*
support Thumb or Thumb2 instructions apparently:

> The processor implements the ARMv6-M Thumb instruction set,
> including a number of 32-bit [Thumb-2] instructions... The ARMv6-M
> instruction set comprises:
>
> - all of the 16-bit Thumb instructions from ARMv7-M excluding CBZ, CBNZ and IT
> - the 32-bit Thumb instructions BL, DMB, DSB, ISB, MRS and MSR.

The cbz and cbnz instructions are fused compare-branch pairs, and are
in the Cortex-M3 along with it\*.

The Cortex-M0 instruction set
-----------------------------

Here's the table, with 86 instructions and 61 distinct mnemonics.  I
think this would be a lot more informative with the actual instruction
encodings.

## Register-register moves (4) ##

* movs Rd, #imm: 1 cycle (8-bit immediate)
* movs Rd, Rm: 1 cycle (low to low)
* mov Rd, Rm: 1 cycle (any to any, except I guess not pc)
* mov pc, Rm: 3 cycles

(mvn is in the logical section.)

## Addition (9) ##

* adds Rd, Rn, #imm: 1 cycle (3-bit immediate)
* adds Rd, Rd, #imm: 1 cycle (8-bit immediate)
* adds Rd, Rn, Rm: 1 cycle (all registers low)
* add Rd, Rn, Rm: 1 cycle (any to any, except I guess not pc)
* add pc, pc, Rm: 3 cycles
* adcs Rd, Rd, Rm: 1 cycle (any to any?)
* add sp, sp, #imm: 1 cycle (deallocating stack)
* add Rd, sp, #imm: 1 cycle (indexing off sp)
* adr Rd, label: 1 cycle (indexing off pc)

## Subtraction (6) ##

* subs Rd, Rn, Rm: 1 cycle (low and low)
* subs Rd, Rn, #imm: 1 cycle (3-bit immediate)
* subs Rd, Rd, #imm: 1 cycle (8-bit immediate)
* sbcs Rd, Rd, Rm: 1 cycle
* sub sp, sp, #imm: 1 cycle (allocating stack)
* rsbs Rd, Rn, #0: 1 cycle (reverse subtract from 0 to negate)

## Logical and multiplication (13) ##
  
* ands Rd, Rd, Rm: 1 cycle
* eors Rd, Rd, Rm: 1 cycle
* orrs Rd, Rd, Rm: 1 cycle
* bics Rd, Rd, Rm: 1 cycle (abjunction)
* mvns Rd, Rm: 1 cycle (move "negated", i.e., notted)

* lsls Rd, Rm, #imm: 1 cycle (constant shift)
* lsls Rd, Rd, Rs: 1 cycle

* lsrs Rd, Rm, #imm: 1 cycle
* lsrs Rd, Rd, Rs: 1 cycle
* asrs Rd, Rm, #imm: 1 cycle (arithmetic shift by constant)
* asrs Rd, Rd, Rs: 1 cycle
* rors Rd, Rd, Rs: 1 cycle

* muls Rd, Rm, Rd: 1 or 32 cycles (presumably depending on if you have
  a multiplier; note the absence of an instruction to get the other
  half of the product)

Note the delightful contrast with a multibyte bit shift on an AVR8.

## Data format conversion (7) ##

* sxtb Rd, Rm: 1 cycle (sex a byte)
* uxtb Rd, Rm: 1 cycle (zero-extend a byte I guess?)
* sxth Rd, Rm: 1 cycle (sex a halfword)
* uxth Rd, Rm: 1 cycle ([zero-extend a halfword][53])
* rev Rd, Rm: 1 cycle (byteswap)
* rev16 Rd, Rm: 1 cycle (middle-endian nuxi swap)
* revsh Rd, Rm: 1 cycle (reverse "signed bottom half word"???)

[53]: https://stackoverflow.com/questions/72689539/understanding-uxth-instruction-in-arm

## Memory access (24) ##

* ldrb Rd, [Rn, #imm]: 2 cycles
* ldrb Rd, [Rn, Rm]: 2 cycles
* ldrsb Rd, [Rn, Rm]: 2 cycles

* ldrh Rd, [Rn, #imm]: 2 cycles
* ldrh Rd, [Rn, Rm]: 2 cycles
* ldrsh Rd, [Rn, Rm]: 2 cycles

* ldr Rd, label: 2 cycles (pc-relative)
* ldr Rd, [Rn, #imm]: 2 cycles
* ldr Rd, [Rn, Rm]: 2 cycles
* ldr Rd, [SP, #imm]: 2 cycles (indexing off sp)

Note the distinct lack of postincrement and preincrement addressing
modes, index register shifts, or combinations of more than two
addends!  Also the sign-extending loads don't have constant offsets,
but I think that's actually a limitation in regular ARM.

* strb Rd, [Rn, #imm]: 2 cycles
* strb Rd, [Rn, Rm]: 2 cycles

* strh Rd, [Rn, #imm]: 2 cycles
* strh Rd, [Rn, Rm]: 2 cycles

* str Rd, [Rn, #imm]: 2 cycles
* str Rd, [Rn, Rm]: 2 cycles
* str Rd, [sp, #imm]: 2 cycles

Of course PC-relative stores are omitted.

* ldm Rn!, {low register list}: 1+N cycles ("excluding base"; N is number of registers)
* ldm Rn, {low register list}: 1+N cycles ("including base")
* stm Rn!, {low register list}: 1+N cycles
* push {low register list}: 1+N cycles
* push {low register list, LR}: 1+N cycles
* pop {low register list}: 1+N cycles
* pop {low register list, pc}: 4+N cycles (N includes pc)

## Comparisons to set flags (4) ##

* cmp Rn, Rm: 1 cycle
* cmp Rn, #imm: 1 cycle
* cmn Rn, Rm: 1 cycle (compare negative; is this bitwise or twos-complement?)
* tst Rn, Rm: 1 cycle (does bitwise AND rather than subtraction)

## Control flow (12) ##

* b*cc* label: 1 cycle if branch not taken, 3 if takem
* b label: 3 cycles
* bl *label*: 4 cycles
* bx Rm: 3 cycles (same as mov to pc?)
* blx Rm: 3 cycles

Note that conditional branches are the only conditional instructions,
since it\* are omitted.

* svc #imm: unknown (this is swi, but handled differently)
* cpsid i: 1 cycle (disable interrupts)
* cpsie i: 1 cycle (enable interrupts)
* bkpt #imm: unknown
* wfe: 2 cycles (wait for event)
* wfi: 2 cycles (wait for interrupt)

## Miscellaneous crap (7) ##

* mrs Rd, special-register: 4 cycles
* msr special-register, Rn: 4 cycles
* sev: 1 cycle ("send event")
* nop: 1 cycle
* yield: 1 cycle (what even is this, it says it's a nop)
* isb: 4 cycles ("instruction synchronization")
* dmb: 4 cycles ("data memory")
* dsb: 4 cycles ("data synchronization")

[0]: https://developer.arm.com/documentation/ddi0432/c/CHDCICDF

Notes
-----

[The Cortes-M0 is about 12'000 gates][1] and so probably about 50'000
transistors not including memory.  But it reaches almost one
DMIPS/MHz, while the original ARM (27'000 transistors; see file
`barrel-shifter-datapath.md`) reached only about half that.

[1]: https://community.arm.com/support-forums/f/architectures-and-processors-forum/5176/arm-cortex-m0-details

The implemented instruction set is rather disappointing compared to
the normal ARM set, above all ergonomically.  And it's not nearly as
small and simple as RV32IC, though it does at least have part of a
multiply.


