I was thinking about a human-readable instruction set (see file
`ascii-isa.md`) for cross-platform frugal programming.  Basically my
thought was that [uxn](https://100r.co/site/uxn.html) was the first
attempt at frugal “write once, run anywhere” that was [good enough to
criticize](http://found.ward.bay.wiki.org/view/good-enough-to-criticize).

So this is my attempt at sketching a low-level “cross-platform
assembly” language that is compact enough and fast enough both to
assemble and to run on a wide range of machines that it’s a viable
vehicle for cross-platform frugal programming. The objective is to be
able to compile a program from a high-level language to a compact
xpasm representation once, and then run it efficiently on at least
i386, amd64, ARM7, aarch64, RV32G, RV64G, DragonBall, AVR8, 8088, Z80,
and 6502, using a fast and small xpasm-to-native-code compiler.

“High-level language” means something like C++, Pascal, Scheme, BASIC,
or OCaml.

“Compact xpasm representation” means that the xpasm representation
shouldn’t be more than three times the size of native code compiled
for any target platform with a C compiler.

“Efficiently” means with a slowdown of less than 10×, ideally less
than 2×, compared to code compiled for the same platform with a C
compiler, Also, the native code generated from the xpasm shouldn’t be
too humongous, or the platforms with 16-bit memory addressing will
only be able to run very limited programs.

“Fast and small” means that the xpasm-to-native-code compiler should
be 8 kibibytes or less and should require less than 16 instructions
per byte of xpasm code.

Uxn
-----

Uxn’s page explains:

> The Uxn ecosystem is a little personal computing stack, created to
> host tools and games, programmable in its own unique assembly
> language.
> 
> It was designed with an implementation-first mindset with a focus on
> creating portable graphical applications, the distribution of Uxn
> projects is akin to sharing game roms for any classic console
> emulator.

And the [Hundred Rabbits
Philosophy](https://100r.co/site/philosophy.html) explains:

> We build simple tools to tackle specific tasks. We release builds
> and documentation to support a wide range of platforms, from native
> applications to terminal tools. We target 20 years old hardware as
> to encourage recyclism and discourage the consumption of fashionable
> electronics.

(And also because they are suffused with noble anti-bourgeois
sentiment.)  And in [their notes about working offgrid
efficiently](https://100r.co/site/working_offgrid_efficiently.html#power),
they say:

> Computers are generally power-sucking vampires. Choosing different
> software, operating systems, or working from machines with a lower
> draw (ARM) or even throttling the CPU, are some of the many things
> we do to lower our power requirements. The way that software is
> built has a substantial impact on the power consumption of a system,
> it is shocking how cpu-intensive modern programs can be.
> 
> Choosing software designed for low-end PCs is a good solution, it is
> also possible to throttle processes on your machine by using a
> ‘throttling controller’. The basic idea is like the throttle in a
> car, it allows to set the rate at which your system will operate and
> consume power.

And later:

> Software has a big impact on productivity, they need to be reliable
> and fast.

Devine gave a [really great talk about the path that led to
uxn](https://100r.co/site/weathering_software_winter.html) explaining
these ideas in more detail, and giving a wonderful overview of the
history of fictious machines, exploring the NES, Smalltalk, Oberon,
Pascal, Plan9, Java, Inferno, the Commodore 64, Kolmogorov complexity,
Chifir, Subleq OISC, Thue, FRACTRAN, combinatorial logic, Brainfuck,
the J1 Forth processor, CHIP-8, Ruby, Photoshop, the ZX Spectrum,
munificent, the archival problem, and so on.  I think everyone should
read it.

And I find these principles very inspiring, except for the
anti-bourgeois stuff.  I’d love to have a little personal computing
stack to host portable graphical tools and games that is built to
lower my power requirements by not being cpu-intensive so my computer
can be less of a power-sucking vampire, while boosting my productivity
by being reliable and fast.

However, uxn is not frugal with CPU, or fast, nor is it good for
programmer productivity, and it’s not all that simple. It’s designed
for interpretation, which inherently means you’re throwing away 90% or
95% of even a single-threaded CPU, and it’s not designed as a
compilation target, which means that you’re stuck programming at the
assembly-language level.  This is pretty fucking fun, but that’s
because it makes easy things hard, not hard things easy; the offgrid
notes lionize Oulipo:

> In the 60s, there was a group of French writers who would create
> works using constrained writing techniques, like replacing every
> noun in a text with the seventh noun after it in a dictionary. They
> did this to inspire ideas and creativity. Living and working aboard
> a sailboat was for us a constraint, it liberated our imagination by
> eliminating possibilities.

As a result of the efficiency problems, the [platforms with complete uxn
implementations](https://github.com/hundredrabbits/awesome-uxn#emulators)
are the luxurious ones: Microsoft Windows, Linux, AmigaOS, the Essence
operating system, anything that can run SDL or Lua and Love2D, Gameboy
Advance, Nintendo 64, Nintendo DS, Playdate, and the Nook eReader: all
32-bit or 64-bit platforms with RAM of 4 mebibytes or more, except
that the GBA only has 384 kibibytes, and there were Amigas with as
little as 512K (though I don’t know if they can run uxn).  More
limited machines like the Gameboy, MS-DOS, and the ZX Spectrum do not
have complete uxn implementations.

There are a much larger number of partial uxn implementations.  Part
of the reason for this is that, in order to get acceptable
performance, the Varvara fictious machine that uxn is part of [has
complex virtual peripherals](https://wiki.xxiivv.com/site/varvara.html)
with “hardware” screen bitplanes, sprites with transparency, a synth
with ADSR envelopes, etc.  But all of these peripherals are an extra
hurdle for implementations to clear and, more importantly from my
viewpoint, extra opportunities for incompatibility.

A more efficient fictious machine would let you implement sprite
compositing and sound synthesis in the code you’re running on it, so
each platform implementation doesn’t have to reimplement it.  This
also provides more flexibility.

With an environment that makes hard things easy instead of making easy
things hard, you can easily do things on a Gameboy or an HP 200LX or
HP 48GX that would normally be very difficult without a much bigger
and more power-hungry computer, and maybe each implementation for a
new platform doesn’t have to reimplement wavetable synthesis and sprites.

In the talk I linked above, Linvega says:

> We built this little system that is tailored to host the games we
> make. It’ll be alien to anyone else, and that’s okay, we are not
> trying to sell you on this idea, what we want is for everyone to try
> and make their own personal computer instead of piggybacking on
> someone else’s idea that’s cluttered with artifacts.

Well, okay, so here’s my attempt to try and make my own personal
computer, one that really is frugal with CPU without sacrificing
portability, one that makes hard things easy instead of making easy
things hard.

A cross-platform assembly language rather than a binary instruction set
-----------------------------------------------------------------------

In file `ascii-isa.md` I explored the possibilities of a hardware
instruction set that would also be human-readable as ASCII. But, in
particular with respect to jump destinations, I couldn’t come up with
a solution that would be both human-readable and implementable
efficiently in hardware without a separate linker/loader pass that
looks at every instruction.  And there were other compromises between
direct hardware implementability and simplicity of generating
efficient code for a different target architecture.  And if you have a
separate linker/loader pass, why not just use something like a
traditional assembler?  Just, one that implements a
machine-independent assembly language, instead of a normal one.  It
might not be quite as fast as strlen() but maybe it could be close.

### Basic design outline ###

The xpasm fictious machine is a byte-addressed 32-bit
register-oriented Harvard machine with a three-address code, designed
so that a very simpleminded cross-assembler can convert each xpasm
instruction into one or more target-machine instructions.

The xpasm language is line-oriented, fairly free-form within each line
(skipping over whitespace is fairly cheap), with one instruction per
line with a rich CISCy set of addressing modes; the 16 registers look
like (predefined) variables named a, b, c, d, e, f, g, h, i, j, k, l,
m, n, o, and p, while data labels must be introduced with `&`. (XXX
that’s a bad choice because it’s an infix operator.) The syntax is pop
infix, but without arbitrary nesting of expressions.  For example, `b
& c` or `a + e` are valid expressions, but `a + b + e` is not.

Limited nested control flow operators are available because it is
shorter to write `if cond {...}` than to write `if cond 1f ... 1:`,
the xpasm-targeting compiler doesn’t have to allocate label names or
numbers, the xpasm-to-native compiler doesn’t have to waste memory on
labels that will not be referenced again, and it doesn’t impair
readability — in fact, it improves it.

Data-transfer instructions are `dest = src`, and computational
instructions are `dest = src1 op src2` or `dest = op src`. As in C,
the `op`s span the set of integer operations commonly implemented in
hardware. (I may do floating point at some point, but not yet.)
Control flow is provided by `:label`, `go label`, `if cond label`, `if
cond {`, `loop {`, `while cond`, `}`, `call label`, `call *src`, and
`ret`. The semantics of `while cond` are unusual; it means `if (!cond)
break`, because in assembly you generally need several instructions
within the loop before you can evaluate the continuation condition.

Operands can be decimal integer constants, data labels preceded with
`&`, code labels preceded with `&&`, registers, or memory
references. Memory references are denoted by `[]` containing an
expression giving the memory address; it can contain a constant (data
label preceded with `&` or integer) and one or two registers, all
separated by `+` signs. The last register can be preceded by a scale,
which must be 2, 4, or 8. So these are legitimate operands:

    c           # register c
    &foo        # the data label foo
    &&foo       # the address of program label foo
    [&foo]      # the data in memory at label foo
    [c]         # the data in memory where register c points
    [&foo+c]    # the data in memory c bytes after foo
    [&foo+2c]   # similarly, but 2*c bytes
    [4+c]       # the data 4 bytes after where c points
    [ 4 + c ]   # same, whitespace is ignored
    [&foo+b+4c] # combining two registers, one with a scale

I’d also thought about spelling these with locutions like `*c`,
`*&foo`, `foo[]`, `foo[c]`, `c[4]`, `c.foo`, `b.foo.4c`, and so on.

An unresolved problem here is how to handle byte-wide accesses.

### Unresolved problems around subroutine calls ###

I’m not sure how to handle call and return in a reasonable way. I’d
like them to usually use whatever the native call and return
instructions are so they get reasonable performance, but on 64-bit
platforms those will push 64 bits on the stack (and usually require
the stack pointer to be 64-bit aligned), while on 16-bit platforms
they will push 16 bits on the stack.  But I’d like to be able to use
stack allocation for local variables and saved registers, so I want to
expose the stack pointer to xpasm programs.

Another problem with the above is that you probably need at least a
minimal prologue for subroutine entry points, because on platforms
like RISC-V that use a link register, non-leaf subroutines need to
save the link register on the stack.  But you don’t want to do that at
every label.

A third problem is that register-window architectures like SPARC will
have totally different parameter-passing behavior if you use their
native call and return instructions.  But I don’t really care that
much about SPARC.

Possibly it makes the right solution is to make subroutine invocation
a magical *deus ex machina*.  You invoke a call operation with zero or
more register-sized arguments and a callee; it clobbers some registers
and puts zero or more register-sized arguments in other registers.
Your subroutine header specifies how many arguments you take and how
big your stack frame is.  The loader handles the rest.

Ben Sittler suggests the alternative of storing the return addresses
on a separate return stack which is not in linear memory, thus
avoiding the necessity to either expose native return addresses to
user code or to translate native return addresses to user-visible
return addresses at call time and back at return time.  For
multithreading this requires defining operations to create a return
stack of a given capacity and to switch stacks, and for longjmp() you
need to be able to reset the stack pointer to an earlier value.

### The xpasm size penalty is about 2× over compiler-generated i386 code ###

One drawback of this is that, traditionally, textual assembly language
is considerably bulkier than the corresponding machine code, and
especially than bytecode.  Let’s look at this subroutine, which
recursively performs substitution on a term given a list of
replacements for variables, from file
`term-rewriting-micro-interpreter.md` in Dernocua:

    (define (subst t env)
      (if (var? t) (cdr (assoc t env))
          (if (pair? t) (cons (subst (car t) env) (subst (cdr t) env))
              t)))

That’s 4 lines and 139 bytes.  I implemented this in i386 assembly in
Qfitzah and put some effort into shrinking it. Here’s the source code:

    proc subst
            jnvar %al, 1f           # if t is not a var, jump ahead; otherwise
            do assq                 # look it up with assq (inheriting args), then
            cdr %eax                # we take its cdr, then
            ret                     # return it
    1:      jpair %al, 2f           # if t is not a pair, we just return it
            ret                     # (it’s already in %eax)
    2:      push %eax               # we must preserve argument t across a call
            push %ecx               # and env too.
            cdr %eax                # get (cdr t) for that recursive call,
            do subst                # which inherits env, but might clobber args;
            xchg %eax, %edx         # socking away its return value in %edx,
            pop %ecx                # restoring env for the second recursive call,
            pop %eax                # and also t, before
            push %edx               # saving the first subst return value on the stack;
            car %eax                # what we want to subst now is (car t)
            do subst                # so now our substed car is in %eax,
            pop %ecx                # and our substed cdr in %ecx, so we can
            jmp cons                # tail-call cons and return the result

This is 19 lines and 1239 bytes.  Through some macro trickery, it
assembles to the following 20 instructions:

     80481b7:	a8 02                	test   $0x2,%al
     80481b9:	74 09                	je     0x80481c4
     80481bb:	e8 23 00 00 00       	call   0x80481e3
     80481c0:	8b 40 04             	mov    0x4(%eax),%eax
     80481c3:	c3                   	ret
     80481c4:	a8 03                	test   $0x3,%al
     80481c6:	74 01                	je     0x80481c9
     80481c8:	c3                   	ret
     80481c9:	50                   	push   %eax
     80481ca:	51                   	push   %ecx
     80481cb:	8b 40 04             	mov    0x4(%eax),%eax
     80481ce:	ff 55 0c             	call   *0xc(%ebp)
     80481d1:	92                   	xchg   %eax,%edx
     80481d2:	59                   	pop    %ecx
     80481d3:	58                   	pop    %eax
     80481d4:	52                   	push   %edx
     80481d5:	8b 00                	mov    (%eax),%eax
     80481d7:	ff 55 0c             	call   *0xc(%ebp)
     80481da:	59                   	pop    %ecx
     80481db:	e9 74 ff ff ff       	jmp    0x8048154

(+ 5 (- #xdb #xb7)) = 41 bytes, about 30 times more compact than the
assembly source code and 3.4 times more compact than the Scheme.
Compactness-oriented output from a compiler might be something like
this, omitting both the comments and the hex dump:

    subst:test $2,%al
    je 1f
    call assq
    mov 4(%eax),%eax
    ret
    1:test $3,%al
    je 2f
    ret
    2:push %eax
    push %ecx
    mov 4(%eax),%eax
    call *12(%ebp)
    xchg %eax,%edx
    pop %ecx
    pop %eax
    push %edx
    mov (%eax),%eax
    call *12(%ebp)
    pop %ecx
    jmp cons

That’s 225 bytes, 18% of the size of the original, but still 5.5 times
the size of the i386 code. compress(1) reduces it to 171 bytes, and
the LZW compression used by compress(1) is simple and fast enough that
you could very reasonably implement it on even a very limited machine,
but that’s still 4.2 times the size of the i386 code.

With the xpasm syntax outlined above, it might look like this:

    :subst
    b=a&2
    if b!=0{
    call assq
    a=[a+4]
    ret
    }
    b=a&3
    if b!=0{
    ret
    }
    p=p-8
    [p]=a
    [p+4]=c
    a=[a+4]
    call subst
    d=a
    a=[p]
    c=[p+4]
    [p]=d
    a=[a]
    call subst
    c=[p]
    p=p+8
    go cons

This is less verbose (and more readable) than the AT&T syntax above:
167 bytes that compress to 132 with compress(1). This is still a 4.1×
or 3.2× blowup over the handwritten native code, though.

On the other hand, my objective here was to get xpasm to be less than
3× the size of code generated by a C compiler, not code generated by
tricky assembler macros to be as small as possible. So I wrote a
version of `subst` in C, which, omitting the relevant definitions, is:

    oop __attribute__((fastcall))
    subst(oop t, oop env)
    {
      if (is_var(t)) return cdr(assoc(t, env));
      if (is_pair(t)) return cons(subst(car(t), env), subst(cdr(t), env));
      return t;
    }

Then I compiled it (183 bytes of C, incidentally) with GCC 9.4.0 with
`gcc -c -fomit-frame-pointer -fno-pic -m32 -Os -I
/usr/i686-linux-gnu/include qfitzah-subst.c` and got the following:

    00000000 <subst>:
       0:	f3 0f 1e fb          	endbr32 
       4:	56                   	push   %esi
       5:	53                   	push   %ebx
       6:	89 cb                	mov    %ecx,%ebx
       8:	83 ec 14             	sub    $0x14,%esp
       b:	80 e1 02             	and    $0x2,%cl
       e:	74 0b                	je     1b <subst+0x1b>
      10:	89 d9                	mov    %ebx,%ecx
      12:	e8 fc ff ff ff       	call   13 <subst+0x13>
      17:	8b 18                	mov    (%eax),%ebx
      19:	eb 2b                	jmp    46 <subst+0x46>
      1b:	f6 c3 03             	test   $0x3,%bl
      1e:	75 26                	jne    46 <subst+0x46>
      20:	8b 0b                	mov    (%ebx),%ecx
      22:	89 54 24 0c          	mov    %edx,0xc(%esp)
      26:	e8 fc ff ff ff       	call   27 <subst+0x27>
      2b:	8b 54 24 0c          	mov    0xc(%esp),%edx
      2f:	8b 0b                	mov    (%ebx),%ecx
      31:	89 c6                	mov    %eax,%esi
      33:	e8 fc ff ff ff       	call   34 <subst+0x34>
      38:	83 c4 14             	add    $0x14,%esp
      3b:	89 f2                	mov    %esi,%edx
      3d:	5b                   	pop    %ebx
      3e:	89 c1                	mov    %eax,%ecx
      40:	5e                   	pop    %esi
      41:	e9 fc ff ff ff       	jmp    42 <subst+0x42>
      46:	83 c4 14             	add    $0x14,%esp
      49:	89 d8                	mov    %ebx,%eax
      4b:	5b                   	pop    %ebx
      4c:	5e                   	pop    %esi
      4d:	c3                   	ret    

That’s 78 bytes of machine code, so the xpasm above is larger by only
2.1× uncompressed or 1.7× compressed.

The i386 is pretty good on code density, although Thumb2, RV32C, and
RV64C do usually beat it by a little.

### Block/string instructions ###

A lot of programs have a hot inner loop that copies a block of memory
(memcpy) or sets it to a constant byte, usually 0 (memset).  Graphical
programs commonly have an inner loop of copying, masking, or
alpha-compositing two-dimensional blocks of pixels.  Especially on
weak machines like the 6502 where the xpasm performance penalty is
most signficant, providing such a capability as a primitive xpasm
“instruction” might significantly extend the range of systems it can
express efficiently.

[Smalltalk’s graphics kernel][sgc] was originally built around a
[BitBlt
operation](http://bildungsgueter.de/Smalltalk/Pages/MVCTutorial/Pages/BitBlt.htm)
which takes a “halftoneForm” mask which has a bit set for the pixels
to affect (like the Cray vector mask register, but tiled, and
originally limited to 16x16), a “combinationRule” ROP which gives a
4-bit truth table for how source and destination bits confine, and
source and dest and clipping rects.  This allows you to amortize
Smalltalk’s interpretation overhead over computations of an enormous
number of parallel pixels. Wikipedia [has an example of how to use
this for shaped sprites][wpss], even in higher color depths, by ANDing
the background with a stencil before ORing in the image. Win32 aped
the interface.

[sgc]: https://www.tech-insider.org/star/research/acrobat/8108-b.pdf "The Smalltalk Graphics Kernel, by Daniel H H Ingalls, BYTE, August 01981, pp. 168 et seq."
[wpss]: https://en.wikipedia.org/wiki/Bit_blit#Example_of_a_masked_blit_implementation

How would you go about validating these ideas?
-------------------------------------------------------------

Probably some of the ideas above are good and some are bad; how would
you tell which is which?  What would a minimal viable xpasm look like?

You probably need loaders (JIT compilers) for at least a couple of
different platforms, ideally different enough to flush out assumptions
that will be big problems in some circumstances, and some way to
measure or at least approximate their performance.

One direction to go is file `programming-junk.md`.
