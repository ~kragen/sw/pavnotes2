Can we construct a rhyme scheme in which, as the length of the poem
approaches infinity, each of an infinite number of rhymes appears an
infinite number of times?

The sum of an infinite geometric series is finite.  The standard
example is that 1 + ½ + ¼ + ⅛ + ... approaches 2, or ½ + ¼ + ⅛ +
... approaches 1.  This immediately suggests a skeletal packing scheme
like this:

    _ A A _
    A _ _ _  1
    _ _ _ A  2
    _ _ _ _
    A _ _ _  4
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ A  8
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _

That is, we divide our poem into quatrains.  In the first quatrain in
which rhyme A appears, it appears twice, occupying ½ of the quatrain,
while the other half is taken up by rhymes that have appeared
previously.  In the next quatrain, labeled 1, it appears once, thus
occuping ¼ of the quatrain.  This is as seldom as it can appear in a
single quatrain, so it appears once in the next two quatrains, labeled
2, thus occupying ⅛ of them, and in the same way once in the next four
quatrains, once in the next eight, and so on.  In this way it appears
an infinite number of times, but only O(log N) times in N lines of the
poem.

Suppose we introduce a second rhyme in the second quatrain.  Do we get
conflicts?

    _ A A _
    A B B _  1
    B _ _ A  2
    _ _ _ B
    A _ _ _  4
    B _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ A  8
    _ _ _ B
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _

No, because each B occurs directly "below" (four lines after) an A,
and in the original skeleton no A is below another A.  And we can add
a third rhyme in the same way:

    _ A A _
    A B B _  1
    B C C A  2
    C _ _ B
    A _ _ C  4
    B _ _ _
    C _ _ _
    _ _ _ _
    _ _ _ A  8
    _ _ _ B
    _ _ _ C
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _
    _ _ _ _

But from this point we are evidently going to run into problems,
because both ends of the fifth quatrain are already full.  We
front-loaded its occurrence of A (the fifth in the poem) and now we
are out of space.  We can correct this by moving that A further down
(considerations of symmetry suggest moving it past the centerline) and
similarly in the 8 group, giving us room for a D.  You can never have
too much D, and this poem can now contain infinite D, and likewise E:

    _ A A _
    A B B _  1
    B C C A  2
    C D D B
    D E E C  4
    E _ _ D
    A _ _ E
    B _ _ _
    C _ _ _  8
    D _ _ _
    E _ _ _
    _ _ _ A
    _ _ _ B
    _ _ _ C
    _ _ _ D
    _ _ _ E

But this still runs out of steam at E, and it seems clear that any
such "vertical repetition" scheme will sooner or later collide with
its beginning; as long as you have a finite number of columns, sooner
or later you collide with your starting point.  This means it doesn't
matter what your original pattern is; you can't introduce an infinite
number of new repetitions of it at regular intervals.

You can still fill out the pattern in an ad-hoc fashion:

    _ A A _
    A B B _  1
    B C C A  2
    C D D B
    D E E C  4
    E F F D
    A G F E
    B G H F
    C H H I  8
    D G I F
    E J J K
    K I H A
    K J L B
    L G M C
    K L M D
    N I H E

But it would be more interesting to have some kind of systematic
approach that gives you some kind of upper bound on how long you'd
have to wait for the next repetition, and still starts out with each
new rhyme appearing frequently for a while before gradually becoming
less frequent.

An approach that seems promising is the traversal of the Sierpinski
triangle as in <http://canonical.org/~kragen/sw/dev3/siercal>.  Each
day in that calendar is adjacent to exactly three other days: the
previous day, the next day, and some other day, which I'll call the
"reflection".

Two thirds of the time the reflection is precisely two days earlier or
later, because you're at the first or last day inside a three-day
triangle.  But when you're in a central day, the reflection can be
arbitrarily far away.

Two thirds of those central days are in either the first or last
three-day triangle within a nine-day week, in which case the
reflection is also within the same nine-day week, either six days
earlier or later (it's the other central day).  But when you're in a
central three-day triangle, the reflection is in some other week, and
can be arbitrarily far away.

In two thirds of those cases, the other week is in the same 27-day
month, because you're in either the first or last week of the month.
But the other one third of the time, the other week is in some other
month, and can be arbitrarily far away.

Similarly, in the first and third month of an 81-day season, the
reflection is in the same season; in the first and third season of a
243-day cycle, the reflection is in the same cycle; in the first and
third cycle of a biennial, the reflection is in the same biennial,
etc.

This seems somehow suggestive, but it isn't obvious to me how to make
the leap.

Another way to get a similar sort of sequence is reflected binary Gray
code: t ^ t >> 1, which gives you a sequence that changes in only one
bit every time t increments.  You can see which one with t ^ t >> 1 ^
t - 1 ^ t - 1 >> 1:

    >>> [t ^ t >> 1 ^ t - 1 ^ t - 1 >> 1 for t in range(16)]
    [0, 1, 2, 1, 4, 1, 2, 1, 8, 1, 2, 1, 4, 1, 2, 1]

This doesn't seem very promising either.
