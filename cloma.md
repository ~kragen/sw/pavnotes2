I just found out that apparently in Mexico you can buy [ephedra
tablets][0] over the counter at Wal-Mart.  They’re “made in the USA”
apparently, brand “Cloma Pharma Laboratories”, brand name “Black
Spider 25”, with supposedly 25 mg of ephedrine per capsule (plus
caffeine, white willow bark extract, yohimbine, ginseng, synephrene,
phenylethylamine, cayenne pepper, etc.)  They also sell other sports
supplements such as “Red Wasp”, “China White”, and “Cocodrene”, all
three of which also contain ephedra.  Interestingly, [Red Wasp also
shows up on sites in Russia][1], [Brazil][2], and Ukraine.  Aside from
Wal-Mart and Sears, it’s mostly stocked in sports supplements shops as
a weight-loss “supplement”.

In the US itself, apparently *Ephedra sinica* seeds are legal still,
just not the plant.

This all seems terribly irrational to me.  Let the methheads smoke
themselves to death if they want to, but let the rest of the
population have effective decongestants, please.

[0]: https://www.walmart.com.mx/ip/cuidado-deportivo/pre-entrenamiento-cloma-pharma-black-spider-100-capsulas-cloma-pharma-black-spider/00085961325225
[1]: https://kupiprotein.ru/zhiroszhigateli/1396-red-wasp-25-75-kapsul-cloma-pharma.html
[2]: https://www.americanas.com.br/produto/74352787/red-wasp-25-termogenico-importado-75-capsulas-cloma-pharma
